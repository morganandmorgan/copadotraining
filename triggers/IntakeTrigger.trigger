/**
 * Intake Trigger
 * @description Trigger for Intake SObject.
 * @author Jeff Watson
 * @date 9/4/2019
 */

trigger IntakeTrigger on Intake__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Intake__c')) return;
    fflib_SObjectDomain.triggerHandler(IntakeDomain.class);

}