trigger MatterTransfer_Trigger on Matter_Transfer__c (before insert, after insert) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Matter_Transfer__c')) return;

    fflib_SObjectDomain.triggerHandler(MatterTransfer_Domain.class);
}