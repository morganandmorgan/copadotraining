trigger Users on User (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    // Only process if trigger is enabled via Trigger Controls custom object
    //Trigger_Control__c triggerControls = TriggerControlsSelector.selectDefaultControls();
    //if(triggerControls == null || triggerControls.All_Triggers__c == true) {
        fflib_SObjectDomain.triggerHandler(mmcommon_Users.class);
    //}
}