trigger PayableCreditNoteTrigger on c2g__codaPurchaseCreditNote__c (before update, after update) {
    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('c2g__codaTransactionLineItem__c')) return;

    fflib_SObjectDomain.triggerHandler(PayableCreditNoteDomain.class);
}