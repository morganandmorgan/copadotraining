trigger DocsFileInfoTrigger on litify_docs__File_Info__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_docs__File_Info__c')) return;
    fflib_SObjectDomain.triggerHandler(DocsFileInfoDomain.class);
} //trigger