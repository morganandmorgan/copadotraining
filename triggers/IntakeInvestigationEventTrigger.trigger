trigger IntakeInvestigationEventTrigger on IntakeInvestigationEvent__c (before insert, after insert, after update, after delete) {

    // Only process if trigger is enabled via Trigger Controls custom object
    //Trigger_Control__c triggerControls = TriggerControlsSelector.selectDefaultControls();
    //if(triggerControls == null || (triggerControls.All_Triggers__c == true && triggerControls.Intake_Investigations__c == true)) {
        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                IntakeInvestigationCopyParent_TrigAct copyParentFieldsHandler = new IntakeInvestigationCopyParent_TrigAct();
                if (copyParentFieldsHandler.shouldRun()) {
                    copyParentFieldsHandler.doAction();
                }
            }
        } else if (Trigger.isAfter) {
            if (Trigger.isInsert) {
                IntakeInvestigationEventRollup_TrigAct rollupHandler = new IntakeInvestigationEventRollup_TrigAct();
                if (rollupHandler.shouldRun()) {
                    rollupHandler.doAction();
                }
            } else if (Trigger.isUpdate) {
                IntakeInvestigationEventRollup_TrigAct rollupHandler = new IntakeInvestigationEventRollup_TrigAct();
                if (rollupHandler.shouldRun()) {
                    rollupHandler.doAction();
                }
            } else if (Trigger.isDelete) {
                IntakeInvestigationEventRollup_TrigAct rollupHandler = new IntakeInvestigationEventRollup_TrigAct();
                if (rollupHandler.shouldRun()) {
                    rollupHandler.doAction();
                }
            }
        }
    //}
}