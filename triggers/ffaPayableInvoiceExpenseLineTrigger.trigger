trigger ffaPayableInvoiceExpenseLineTrigger on c2g__codaPurchaseInvoiceExpenseLineItem__c (before insert) {

    Set<Id> matterIds = new Set<Id>();

    //check to see if any of the related matters are closed, if so throw an error:
    for(c2g__codaPurchaseInvoiceExpenseLineItem__c pinExpLine : Trigger.new){
        if(pinExpLine.Matter__c != null){
            matterIds.add(pinExpLine.Matter__c);
        }
    }
    Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>(
        [SELECT Id, Finance_Status__c, Name, ReferenceNumber__c FROM litify_pm__Matter__c WHERE Id in :matterIds]
    );
    
    for(c2g__codaPurchaseInvoiceExpenseLineItem__c pinExpLine : Trigger.new){
        if(pinExpLine.Matter__c != null){
            if(matterMap != null && matterMap.containsKey(pinExpLine.Matter__c) && matterMap.get(pinExpLine.Matter__c).Finance_Status__c == 'Closed'){
                pinExpLine.addError('The following Matter has a Finance Status of Closed and cannot accept any new expense lines - '+ matterMap.get(pinExpLine.Matter__c).Name);
            }
        }
    }
}