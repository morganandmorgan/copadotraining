/**
 * Event Trigger
 * @description Trigger for Event SObject.
 * @author Jeff Watson
 * @date 2/21/2019
 */

//trigger Event_Trigger on Event (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
trigger Event_Trigger on Event (before insert, before update, after insert, after update, after delete) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Event')) return;

    // Todo: This needs to be refactored into a new domain class called Event_Domain.
    InvestigationEventTriggerHandler handler = new InvestigationEventTriggerHandler();
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            handler.beforeInsertHandler(Trigger.new);
        } else if (Trigger.isUpdate) {
            handler.beforeUpdateHandler(Trigger.oldMap, Trigger.newMap);
        }
    } else if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            handler.afterInsertHandler(Trigger.new);
        } else if (Trigger.isUpdate) {
            handler.afterUpdateHandler(Trigger.old, Trigger.new);
        } else if (Trigger.isDelete) {
            handler.afterDeleteHandler(Trigger.old);
        }
    }
}