/**
 * Litify Matter Team Member Trigger
 * @description Trigger for Litify Matter Team Member SObject.
 * @author Jeff Watson
 * @date 2/21/2019
 */
 
trigger MatterTeamMember_Trigger on litify_pm__Matter_Team_Member__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_pm__Matter_Team_Member__c')) return;
    fflib_SObjectDomain.triggerHandler(MatterTeamMember_Domain.class);
}