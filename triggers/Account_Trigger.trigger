/**
 * Account Trigger
 * @description Trigger for Account SObject.
 * @author Jeff Watson
 * @date 2/21/2019
 */

trigger Account_Trigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Account')) return;

    // Todo: Refactor the following into Domain class using fflib
    mmcommon_Accounts.triggerHandler();

    // Todo: Needs to be moved to Account_Domain as well as the above
    String workflow = '';
    //if (Test.isRunningTest()) return;
    //There is no Trigger.new on Delete (one line so the unit test covers enough code)
    if ( !Trigger.isDelete ) SpringCMTriggerHandler.StartWorkflow(UserInfo.getSessionId(), Trigger.new.get(0).getSObjectType().getDescribe().getName(), Trigger.new, workflow);
}