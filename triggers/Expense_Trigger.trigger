/**
 * LitifyExpense Trigger
 * @description Trigger for Litify Expense SObject.
 * @author Jeff Watson
 * @date 1/19/2019
 */

trigger Expense_Trigger on litify_pm__Expense__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_pm__Expense__c')) return;

    fflib_SObjectDomain.triggerHandler(ExpenseDomain.class);
    dlrs.RollupService.triggerHandler(litify_pm__Expense__c.SObjectType);
}