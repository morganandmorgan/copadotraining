trigger LitifyPMReferrals on litify_pm__Referral__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Only process if trigger is enabled via Trigger Controls custom object
    //Trigger_Control__c triggerControls = TriggerControlsSelector.selectDefaultControls();
    //if(triggerControls == null || (triggerControls.All_Triggers__c == true && triggerControls.Litify_Referrals__c == true)) {
        fflib_SObjectDomain.triggerHandler(mmlitifylrn_Referrals.class);
    //}
}