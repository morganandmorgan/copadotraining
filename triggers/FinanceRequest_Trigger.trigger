trigger FinanceRequest_Trigger on Finance_Request__c (before update) {
    if (Trigger_Helper.isDisabled('Finance_Request__c')) return;

    fflib_SObjectDomain.triggerHandler(FinanceRequest_Domain.class);
}