trigger Payment_Trigger on c2g__codaPayment__c (after update, after insert) {
        
    System.debug('Payment_Trigger');

    PaymentProcessingStatusUpdater handler = null;
    handler=new PaymentProcessingStatusUpdater();
    
    handler.handlePaymentTrigger(Trigger.new, Trigger.oldMap, Trigger.isInsert);
        
}