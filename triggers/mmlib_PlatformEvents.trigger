trigger mmlib_PlatformEvents on mmlib_Event__e 
    (after insert) 
{
    mmlib_PlatformEventsDistributor.triggerHandler();
}