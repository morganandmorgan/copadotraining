/*==================================================================================
Name            : PaymentMediaDetailTrigger
Author          : CLD Partners
Created Date    : June 2018
Description     : Trigger to set PIN Lookup and EXP Report Lookup
==================================================================================*/
trigger PaymentMediaDetailTrigger on c2g__codaPaymentMediaDetail__c (before insert, before update) {

	Set<String> pinNames = new Set<String>();
	Set<Id> pmsIds = new Set<Id>();
	Set<Id> pinIds = new Set<Id>();
	Set<Id> financeRequestIds = new Set<Id>();

	List<c2g__codaPurchaseInvoice__c> pinUpdateList = new List<c2g__codaPurchaseInvoice__c>();
	List<litify_pm__Expense__c> expUpdateList = new List<litify_pm__Expense__c>();
	
	Map<String, c2g__codaPurchaseInvoice__c> pinMap = new Map<String, c2g__codaPurchaseInvoice__c>();
	Map<Id, litify_pm__Expense__c> expPinMap = new Map<Id, litify_pm__Expense__c>();
	Map<Id, litify_pm__Expense__c> expMap = new Map<Id, litify_pm__Expense__c>();

	for (c2g__codaPaymentMediaDetail__c pmd : Trigger.new) {
		pinNames.add(pmd.c2g__DocumentNumber__c);
		pmsIds.add(pmd.c2g__PaymentMediaSummary__c);
	}


	Map<Id, c2g__codaPaymentMediaSummary__c> pmsMap = new Map<Id, c2g__codaPaymentMediaSummary__c>([
		SELECT Id, c2g__PaymentReference__c FROM c2g__codaPaymentMediaSummary__c WHERE Id in : pmsIds]);
	for (c2g__codaPurchaseInvoice__c pin : [SELECT Id, Finance_Request__c, Name FROM c2g__codaPurchaseInvoice__c WHERE Name in :pinNames]) {
		pinMap.put(pin.Name, pin);
		pinIds.add(pin.Id);
	}

	//query the expenses that need to have the check number applied:
	for(litify_pm__Expense__c exp : [SELECT Id, Create_Check_Number__c, Payable_Invoice__c FROM litify_pm__Expense__c WHERE Payable_Invoice__c != null AND Payable_Invoice__c in : pinIds]){
		expPinMap.put(exp.Payable_Invoice__c, exp);
	}

	//loop through and make updates
	for (c2g__codaPaymentMediaDetail__c pmd : Trigger.new) {
		c2g__codaPurchaseInvoice__c pin = pinMap.containsKey(pmd.c2g__DocumentNumber__c) ? pinMap.get(pmd.c2g__DocumentNumber__c) : null;
		pmd.Payable_Invoice__c = pin != null ? pin.Id : pmd.Payable_Invoice__c;

		//set the vendor payment lookup
		if(pin != null){
			pin.Vendor_Payment__c = pmsMap.containsKey(pmd.c2g__PaymentMediaSummary__c) ?  pmsMap.get(pmd.c2g__PaymentMediaSummary__c).Id : null;
			pinUpdateList.add(pin);

			//set the check number on the expnese 
			if(expPinMap.containsKey(pin.Id) && pmsMap.containsKey(pmd.c2g__PaymentMediaSummary__c)){
				litify_pm__Expense__c exp = expPinMap.get(pin.Id);
				exp.Create_Check_Number__c = pmsMap.get(pmd.c2g__PaymentMediaSummary__c).c2g__PaymentReference__c;
				expMap.put(exp.Id, exp);
			}
		}
	}

	update pinUpdateList;
	update expMap.values();
}