trigger AuditReportCardTrigger on Audit_Report_Card__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Audit_Report_Card__c')) return;
    fflib_SObjectDomain.triggerHandler(AuditReportCardDomain.class);
} //trigger