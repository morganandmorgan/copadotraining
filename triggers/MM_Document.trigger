trigger MM_Document on MM_Document__c (after insert, after update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('MM_Document__c')) return;
    
    // Todo: This needs to be refactored into a domain class.
    if (Trigger.isInsert) {
        List<Document_Uploaded__e> eventsToPublish = new List<Document_Uploaded__e>();
        for (MM_Document__c mmDoc : Trigger.new) {
            eventsToPublish.add(new Document_Uploaded__e(MatterId__c = mmDoc.Matter__c));
        }
        EventBus.publish(eventsToPublish);

    } else if (Trigger.isUpdate) {
        for (MM_Document__c mmDoc : Trigger.New) {
            boolean updateDocNameorType = false;
            MM_Document__c oldDoc = Trigger.oldMap.get(mmDoc.Id);
            if (oldDoc.Document_Name__c != mmDoc.Document_Name__c) {
                updateDocNameorType = true;
            } else if (oldDoc.Document_Type__c != mmDoc.Document_Type__c) {
                updateDocNameorType = true;
            }
            if (updateDocNameorType) {
                UpdateSpringCMDocumentName.sendDocnametoSpringCM(mmDoc.Id, mmDoc.Document_Name__c, mmDoc.Document_Type__c);
            }
        }
    }
}