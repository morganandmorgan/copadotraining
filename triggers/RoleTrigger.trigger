/**
 * RoleTrigger
 * @description Trigger for litify_pm__Role__c.
 * @author Jeff Watson
 * @date 1/18/2019
 */
 
trigger RoleTrigger on litify_pm__Role__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_pm__Role__c')) return;
    
    fflib_SObjectDomain.triggerHandler(RoleDomain.class);
}