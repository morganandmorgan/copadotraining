trigger LitifyPMMatterTeamMembers on litify_pm__Matter_Team_Member__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/* ****

  This trigger/class calls a domain that runs redundant code.  Code that is also being removed from the other domain: MatterTeamMember_Domain
  See comments in the other domain about removing the scheduling of SyncMatterTeamMembersToLawsuitQueuable


    // Only process if trigger is enabled via Trigger Controls custom object
    //Trigger_Control__c triggerControls = TriggerControlsSelector.selectDefaultControls();
    //if(triggerControls == null || (triggerControls.All_Triggers__c == true && triggerControls.Litify_Team_Members__c == true)) {
        // Check for Global Trigger control
        if (Trigger_Helper.isDisabled('litify_pm__Matter_Team_Member__c')) return;
        fflib_SObjectDomain.triggerHandler(mmmatter_MatterTeamMembers.class);
    //} 


  This trigger/class needs deleted.


**** */    
}