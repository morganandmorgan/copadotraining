trigger MarketingTrackingInfos on Marketing_Tracking_Info__c
    (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
    fflib_SObjectDomain.triggerHandler(mmmarketing_TrackingInformations.class);
}