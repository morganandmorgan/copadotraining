/**
 * FeeGoalAllocation_Trigger
 * @description Trigger for Fee Goal Allocation SObject.
 * @author Jeff Watson
 * @date 2/13/2019
 */

trigger FeeGoalAllocation_Trigger on Fee_Goal_Allocation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(!System.isFuture()) {
        fflib_SObjectDomain.triggerHandler(FeeGoalAllocation_Domain.class);
    }
}