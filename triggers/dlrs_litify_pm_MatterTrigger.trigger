/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/

trigger dlrs_litify_pm_MatterTrigger on litify_pm__Matter__c (before delete, before insert, before update, after delete, after insert, after undelete, after update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_pm__Matter__c')) return;

    dlrs.RollupService.triggerHandler(litify_pm__Matter__c.SObjectType);
}