/**
 * Litify Matter Trigger
 * @description Trigger for Litify Matter SObject.
 * @author Jeff Watson
 * @date 2/21/2019
 */

trigger Matter_Trigger on litify_pm__Matter__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_pm__Matter__c')) return;

    if (Trigger.isAfter && Trigger.isInsert) {
        if (Label.Matter_Trigger_Switch != null && Label.Matter_Trigger_Switch.equalsIgnorecase('true')) {
            MatterHandler.onAfterInsert(Trigger.new);
        }
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
        MatterHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }

    fflib_SObjectDomain.triggerHandler(mmmatter_Matters.class);
    fflib_SObjectDomain.triggerHandler(Matter_Domain.class);

    try {
        String org = UserInfo.getUserName().substringAfterLast('.');
        //is this production?
        if (org == 'com') {
            et4ae5.triggerUtility.automate('litify_pm__Matter__c');
        }
    } catch (Exception e) {
        //we don't want this to break the trigger
    } //try/catch
}