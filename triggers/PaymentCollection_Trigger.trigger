/*============================================================================
Name            : PaymentCollection_Trigger
Author          : CLD
Created Date    : Jun 2019
Description     : trigger on Payment_Collection__c
=============================================================================*/
trigger PaymentCollection_Trigger on Payment_Collection__c (before insert) {

    fflib_SObjectDomain.triggerHandler(PaymentCollection_Domain.class);

}