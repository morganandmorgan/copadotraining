trigger PaymentsPlusErrorLog_Trigger on c2g__PaymentsPlusErrorLog__c (after insert) {

    PaymentProcessingStatusUpdater handler = null;
    handler=new PaymentProcessingStatusUpdater();

    List<c2g__PaymentsPlusErrorLog__c> paymentErrorLogRecords = null;
    paymentErrorLogRecords=(List<c2g__PaymentsPlusErrorLog__c>) Trigger.New;

    handler.handlePaymentErrorLogTrigger(paymentErrorLogRecords);
    
}