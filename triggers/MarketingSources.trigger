trigger MarketingSources on Marketing_Source__c
    (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
    fflib_SObjectDomain.triggerHandler(mmmarketing_Sources.class);
}