trigger IntakeTriggeredSends on Intake__c (after insert,after update) {

  // Check for Global Trigger control
  if (Trigger_Helper.isDisabled('Intake__c')) return;

  //we don't want this to run more than once per thread
  if ( !IntakeDomain.suppressTriggeredSend() ) {
    try {
      et4ae5.triggerUtility.automate('Intake__c');
    } catch (Exception e) {
      //we don't want this to break all the other intake triggers        
    } //try/catch
  }
} //class