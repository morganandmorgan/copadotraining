trigger PaymentSummary_Trigger on c2g__codaPaymentAccountLineItem__c (after update) {

    if (Trigger_Helper.isDisabled('c2g__codaPaymentAccountLineItem__c')) return;

    fflib_SObjectDomain.triggerHandler(PaymentSummary_Domain.class);

}