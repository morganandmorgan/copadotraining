/**
 * IncomingDarkHourTrigger
 * @description Incoming Dark Hour Trigger.
 * @author 360 Degrees
 *
 * @author Jeff Watson
 * Renamed trigger to match naming convention, formatted.  This also needs to have logic moved into a Domain class.
 */

trigger TdcTswMessage_Trigger on tdc_tsw__Message__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    tdc_tsw__Message__c[] lstMessage = Trigger.new;
    if (trigger.isInsert && trigger.isAfter) {
        IncomingDarkHourTriggerHandler.getIncomingMessage(lstMessage);
    }
}