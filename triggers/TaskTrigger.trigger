/**
 * Task Trigger
 * @description Trigger for Task SObject.
 * @author Jeff Watson
 * @date 2/21/2019
 */

trigger TaskTrigger on Task (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Task')) return;
    fflib_SObjectDomain.triggerHandler(TaskDomain.class);
}