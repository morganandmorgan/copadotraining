trigger PaymentStatusUpdateEvent_Trigger on Payment_Status_Update__e (after insert) {
    //Monitor/listener/poll client for payment status updated events.

    System.debug('PaymentStatusUpdateEvent_Trigger:  Payment update event received/created.');

    PaymentProcessingStatusUpdater handler = null;
    handler=new PaymentProcessingStatusUpdater();

    handler.handlePaymentStatusUpdateEventTrigger(Trigger.new);    

}