/*============================================================================
Name            : TransactionLineItem_Trigger
Author          : CLD
Created Date    : Sep 2018
Description     : trigger on c2g__codaTransactionLineItem__c
=============================================================================*/
trigger TransactionLineItem_Trigger on c2g__codaTransactionLineItem__c (before insert, after insert) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('c2g__codaTransactionLineItem__c')) return;

    fflib_SObjectDomain.triggerHandler(TransactionLineItem_Domain.class);
}