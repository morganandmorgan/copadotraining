trigger IncidentInvestigationEventTrigger on IncidentInvestigationEvent__c (after update) {

    // Only process if trigger is enabled via Trigger Controls custom object
    //Trigger_Control__c triggerControls = TriggerControlsSelector.selectDefaultControls();
    //if(triggerControls == null || (triggerControls.All_Triggers__c == true && triggerControls.Incident_Investigations__c == true)) {
        if (Trigger.isAfter) {
            if (Trigger.isUpdate) {
                IncidentInvestigationUpdateEvent_TrigAct updateChildrenAction = new IncidentInvestigationUpdateEvent_TrigAct();
                if (updateChildrenAction.shouldRun()) {
                    updateChildrenAction.doAction();
                }
            }
        }
    //}
}