trigger mmlawsuit_Lawsuits on Lawsuit__c
    ( after delete, after insert, after update, before delete, before insert, before update )
{
    if (Trigger_Helper.isDisabled('Lawsuit__c')) return;
    fflib_SObjectDomain.triggerHandler(mmlawsuit_Lawsuits.class);
}