trigger AuditRuleTrigger on Audit_Rule__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Audit_Rule__c')) return;
    fflib_SObjectDomain.triggerHandler(AuditRuleDomain.class);
} //trigger