/*============================================================================
Name            : TrustTransaction_Trigger
Author          : CLD
Created Date    : May 2019
Description     : trigger on Trust_Transaction__c
=============================================================================*/
trigger TrustTransaction_Trigger on Trust_Transaction__c (after insert, after update) {
    
    // Check for Global Trigger control
    //if (!Trigger_Helper.shouldRun()) return;

    fflib_SObjectDomain.triggerHandler(TrustTransaction_Domain.class);
}