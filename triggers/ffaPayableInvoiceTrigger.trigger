trigger ffaPayableInvoiceTrigger on c2g__codaPurchaseInvoice__c (before update, after update) {

	// Check for Global Trigger control
	if (Trigger_Helper.isDisabled('c2g__codaPurchaseInvoice__c')) return;

	// Todo: This needs to be refactored into a new domain class called PurchaseInvoice_Domain.
	if(trigger.isBefore){
		//call the creation of litify expenses
		ffaPayableInvoiceTriggerHandler.createLitifyExpense(trigger.new, trigger.oldMap);

		//validate against the matter's trust balance
		ffaPayableInvoiceTriggerHandler.validateTrustBalance(trigger.new, trigger.oldMap);
	}

	if(trigger.isAfter){
		//set the check memo if applicable
		ffaPayableInvoiceTriggerHandler.setCheckMemo(trigger.new, trigger.oldMap);	
	}
}