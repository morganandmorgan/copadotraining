trigger mmffa_Deposits on Deposit__c (before delete, before insert, before update, after delete, after insert, after undelete, after update) {

    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('Deposit__c')) return;

    // Todo: This needs to be refactored into a new domain class called Deposit_Domain.
    fflib_SObjectDomain.triggerHandler(mmffa_Deposits.class);
    //dlrs.RollupService.triggerHandler(Deposit__c.SObjectType);
}