trigger DocuSignStatusTrigger on dsfs__DocuSign_Status__c (before insert) {

    // Only process if trigger is enabled via Trigger Controls custom object
    //Trigger_Control__c triggerControls = TriggerControlsSelector.selectDefaultControls();
    //if(triggerControls == null || triggerControls.All_Triggers__c == true && triggerControls.Docusign_Status__c == true) {
        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                DocuSignStatusObtainUserInfo_TrigAct obtainUserInfoHandler = new DocuSignStatusObtainUserInfo_TrigAct();
                if (obtainUserInfoHandler.shouldRun()) {
                    obtainUserInfoHandler.doAction();
                }
            }
        }
    //}
}