trigger NegotiationTrigger on litify_pm__Negotiation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Check for Global Trigger control
    if (Trigger_Helper.isDisabled('litify_pm__Negotiation__c')) return;

    fflib_SObjectDomain.triggerHandler(NegotiationDomain.class);
}