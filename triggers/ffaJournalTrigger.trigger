/*============================================================================
Name            : ffaJournalTrigger
Author          : CLD
Created Date    : July 2018
Description     : trigger on c2g__codaJournal__c
=============================================================================*/
trigger ffaJournalTrigger on c2g__codaJournal__c (before update) {

	//call the creation of litify expenses
	ffaJournalTriggerHandler.createLitifyExpense(trigger.new, trigger.oldMap);
	
}