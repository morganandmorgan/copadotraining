trigger PaymentMediaSummaryTrigger on c2g__codaPaymentMediaSummary__c (before insert, before update) {

	List<litify_pm__Expense__c> expenseUpdateList = new List<litify_pm__Expense__c>();
	Map<Id, String> checkNumberMap = new Map<Id, String>(); //key is finance request Id / value is the check number
	Map<Id, Id> payableInvoiceMap = new Map<Id, Id>(); 
	Map<Id,Id> financeRequestMap = new Map<Id,Id>();
	

    
	for(c2g__codaPaymentMediaSummary__c pms : [SELECT 
		Id, c2g__PaymentReference__c, 
			(SELECT Id, Payable_Invoice__r.Finance_Request__c, Payable_Invoice__c
			FROM c2g__PaymentMediaDetails__r 
			WHERE Payable_Invoice__c != null
			ORDER BY Payable_Invoice__r.Name desc) 
		FROM c2g__codaPaymentMediaSummary__c
		WHERE Id in :Trigger.new]){
		
		for(c2g__codaPaymentMediaDetail__c pmd : pms.c2g__PaymentMediaDetails__r)
        {
            // Pull the updated Summary as this may contain the check number.
            c2g__codaPaymentMediaSummary__c newSummary = Trigger.newMap.get(pms.Id);

			if(pmd.Payable_Invoice__c != null && !payableInvoiceMap.containsKey(pms.Id)){
				payableInvoiceMap.put(pms.Id, pmd.Payable_Invoice__c);
			}

			if(pmd.Payable_Invoice__r.Finance_Request__c != null){
				financeRequestMap.put(pms.Id, pmd.Payable_Invoice__r.Finance_Request__c);	
			}

			if(newSummary.c2g__PaymentReference__c != null){
				checkNumberMap.put(pmd.Payable_Invoice__c, newSummary.c2g__PaymentReference__c);	
			}
		}
	}

	for(litify_pm__Expense__c exp : [SELECT Id, Payable_Invoice__c, Create_Check_Number__c FROM litify_pm__Expense__c WHERE Payable_Invoice__c in : checkNumberMap.keyset()]){
		exp.Create_Check_Number__c = checkNumberMap.get(exp.Payable_Invoice__c);
		expenseUpdateList.add(exp);
	}

	for(c2g__codaPaymentMediaSummary__c pms : Trigger.new){
		pms.Finance_Request__c = financeRequestMap.containsKey(pms.Id) ? financeRequestMap.get(pms.Id) : pms.Finance_Request__c;
	}

	//populate the primary PIN field:
	for(c2g__codaPaymentMediaSummary__c pms : Trigger.new){
		pms.Primary_Payable_Invoice__c = payableInvoiceMap.containsKey(pms.Id) ? payableInvoiceMap.get(pms.Id) : pms.Primary_Payable_Invoice__c;
	}

	//update other objects
	update expenseUpdateList;
}