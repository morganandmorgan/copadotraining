<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Morgan &amp; Morgan</label>
    <tabs>standard-Account</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-report</tabs>
    <tabs>Intake__c</tabs>
    <tabs>Questionnaire__c</tabs>
    <tabs>Case_Qualify_Criteria__c</tabs>
    <tabs>Address_by_Zip_Code__c</tabs>
    <tabs>Lawsuit__c</tabs>
    <tabs>Approval_Assignment__c</tabs>
    <tabs>Incident__c</tabs>
    <tabs>DICE_Queue__c</tabs>
    <tabs>ReferralRule__c</tabs>
    <tabs>tdc_tsw__Message__c</tabs>
    <tabs>et4ae5__Configuration__c</tabs>
    <tabs>AOB_Intake_Wizard</tabs>
    <tabs>Twilio_Account_Mapping__c</tabs>
    <tabs>c2g__codaJournalLineItem__c</tabs>
    <tabs>LiveIvrSearching</tabs>
    <tabs>Qualtrics__c</tabs>
    <tabs>Audit_Error_Log__c</tabs>
    <tabs>litify_docs__Input__c</tabs>
    <tabs>litify_docs__Template__c</tabs>
    <tabs>litify_docs__Packet__c</tabs>
    <tabs>litify_docs__File_Info__c</tabs>
    <tabs>Lien_LOP__c</tabs>
</CustomApplication>
