<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-AppLauncher</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-AppLauncher</tabs>
    <tabs>Intake__c</tabs>
    <tabs>Questionnaire__c</tabs>
    <tabs>Case_Qualify_Criteria__c</tabs>
    <tabs>Address_by_Zip_Code__c</tabs>
    <tabs>Lawsuit__c</tabs>
</CustomApplication>
