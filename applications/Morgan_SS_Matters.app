<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Matter_Layout_Social_Security</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#115099</headerColor>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>true</shouldOverrideOrgTheme>
    </brand>
    <description>The app for handling all of the Morgan &amp; Morgan Social Security Matters</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Morgan SS Matters</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Social_Security</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Social_Security</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Social_Security</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Social_Security</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M Matter Management</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default2</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Litify Matter Management Testing</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default2</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>M&amp;M Matter Management</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>PI_Matters_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>M&amp;M Matter Management - PI</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Administrative</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Blasting</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Cast_Iron_Pipes</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Civil_Rights</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Medical_Malpractice</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Nursing_Home</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Personal_Injury</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.PIP</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Premises_Liability</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Product_Liability</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Matter_Layout_Full_Litify_Matters</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>litify_pm__Matter__c</pageOrSobjectType>
        <recordType>litify_pm__Matter__c.Workers_Comp</recordType>
        <type>Flexipage</type>
        <profile>M&amp;M System Administrator Modified</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>M&amp;M Accounting</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Case_Staff_Homepage</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>M&amp;M FP Matter Management App</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default2</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>M&amp;M Social Security CCC Consultant</profile>
    </profileActionOverrides>
    <tabs>standard-home</tabs>
    <tabs>Multi_Matter_Upload</tabs>
    <tabs>standard-Task</tabs>
    <tabs>litify_pm__Matter__c</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-File</tabs>
    <tabs>Intake__c</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Event</tabs>
    <tabs>litify_pm__Default_Matter_Team__c</tabs>
    <tabs>SpringCMEos__SpringCM_Viewer</tabs>
    <tabs>Twilio_Account_Mapping__c</tabs>
    <tabs>c2g__codaJournalLineItem__c</tabs>
    <tabs>LiveIvrSearching</tabs>
    <tabs>Qualtrics__c</tabs>
    <tabs>litify_docs__Input__c</tabs>
    <tabs>litify_docs__Template__c</tabs>
    <tabs>litify_docs__Packet__c</tabs>
    <tabs>litify_docs__File_Info__c</tabs>
    <tabs>Lien_LOP__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Morgan_SS_Matters_UtilityBar</utilityBar>
</CustomApplication>
