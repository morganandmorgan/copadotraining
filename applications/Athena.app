<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>true</shouldOverrideOrgTheme>
    </brand>
    <description>Athena App for managing experts, knowledge, and other content for Morgan &amp; Morgan</description>
    <formFactors>Small</formFactors>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>true</isNavPersonalizationDisabled>
    <label>Athena</label>
    <navType>Standard</navType>
    <tabs>standard-Account</tabs>
    <tabs>Expert__c</tabs>
    <tabs>Lien_LOP__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Athena_UtilityBar</utilityBar>
</CustomApplication>
