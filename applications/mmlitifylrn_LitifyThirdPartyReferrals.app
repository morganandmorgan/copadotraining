<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>The application responsible for managing the third party, non-Morgan &amp; Morgan, intake referrals to the Litify Referral Network.</description>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Litify Third Party Referrals</label>
    <tabs>litify_pm__Firm__c</tabs>
    <tabs>mmlitifylrn_Credentials_Management</tabs>
    <tabs>ReferralRule__c</tabs>
    <tabs>litify_pm__Referral__c</tabs>
    <tabs>Intake__c</tabs>
    <tabs>Twilio_Account_Mapping__c</tabs>
    <tabs>c2g__codaJournalLineItem__c</tabs>
    <tabs>LiveIvrSearching</tabs>
    <tabs>Qualtrics__c</tabs>
    <tabs>litify_docs__Input__c</tabs>
    <tabs>litify_docs__Template__c</tabs>
    <tabs>litify_docs__Packet__c</tabs>
    <tabs>litify_docs__File_Info__c</tabs>
    <tabs>Lien_LOP__c</tabs>
</CustomApplication>
