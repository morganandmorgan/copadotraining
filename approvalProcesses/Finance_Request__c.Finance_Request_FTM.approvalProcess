<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>true</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <type>allInternalUsers</type>
    </allowedSubmitters>
    <allowedSubmitters>
        <type>creator</type>
    </allowedSubmitters>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Name</field>
        <field>Check_Amount__c</field>
        <field>Expense_Type__c</field>
        <field>Matter_Reference_Number__c</field>
        <field>Owner</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <approvalActions>
            <action>
                <name>Update_Attorney_Approved_Date</name>
                <type>FieldUpdate</type>
            </action>
            <action>
                <name>Update_Status_Attorney_Approved</name>
                <type>FieldUpdate</type>
            </action>
        </approvalActions>
        <assignedApprover>
            <approver>
                <name>Approving_Attorney__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>If the check is greater than $500 ($2000 for Insurance Dispute), or it is a Close Matter request, it will go to the assigned attorney first for approval. Matters where assigned attorney = Mark Nation will go to Finance.</description>
        <entryCriteria>
            <formula>AND(
 Matter__r.litify_pm__Principal_Attorney__r.Id &lt;&gt; &quot;005o0000003UsRI&quot;,
OR(
(Matter__r.Litigation__c = &quot;Insurance Dispute&quot;
&amp;&amp;
Check_Amount__c &gt; 2000.00),
(Matter__r.Litigation__c &lt;&gt; &quot;Insurance Dispute&quot;
&amp;&amp;
Check_Amount__c &gt; 500.00),
RecordType.DeveloperName = &quot;Close_Matter&quot;
),
$User.Id &lt;&gt; Approving_Attorney__r.Id
)</formula>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Check Request &gt; 500 OR Close Matter Request</label>
        <name>Check_Request_500</name>
        <rejectionActions>
            <action>
                <name>Reject_Request</name>
                <type>FieldUpdate</type>
            </action>
        </rejectionActions>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <approvalActions>
            <action>
                <name>Set_Status_Complete</name>
                <type>FieldUpdate</type>
            </action>
        </approvalActions>
        <assignedApprover>
            <approver>
                <name>Finance_FTM</name>
                <type>queue</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>Send Finance request over to FTM Finance for approval</description>
        <label>FTM - Finance Approvers</label>
        <name>FTM_Finance_Approvers</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
        <rejectionActions>
            <action>
                <name>Reject_Request</name>
                <type>FieldUpdate</type>
            </action>
        </rejectionActions>
    </approvalStep>
    <description>All Finance Requests for Fort Myers (minus Social Security)</description>
    <emailTemplate>Internal_Notifications/Finance_Request_New_Approval</emailTemplate>
    <enableMobileDeviceAccess>true</enableMobileDeviceAccess>
    <entryCriteria>
        <formula>AND(
OR(
ISPICKVAL(Request_Status__c, &quot;Draft&quot;),
ISPICKVAL(Request_Status__c, &quot;Rejected&quot;)
),
Matter__r.AssignedToMMBusiness__r.Name=&quot;Morgan &amp; Morgan Fort Myers PLLC&quot; 
)</formula>
    </entryCriteria>
    <finalApprovalActions>
        <action>
            <name>Finance_Request_Send_Approval_Email</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Finance_Request_Update_Approval_Date</name>
            <type>FieldUpdate</type>
        </action>
    </finalApprovalActions>
    <finalApprovalRecordLock>false</finalApprovalRecordLock>
    <finalRejectionActions>
        <action>
            <name>Finance_Request_Send_Approval_Email</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Finance_Request_Update_Rejected_Date</name>
            <type>FieldUpdate</type>
        </action>
    </finalRejectionActions>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <initialSubmissionActions>
        <action>
            <name>Set_Status_In_Process</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Set_Submitted_by_UserName</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Set_Submitted_Date_Today</name>
            <type>FieldUpdate</type>
        </action>
    </initialSubmissionActions>
    <label>Finance Request - FTM</label>
    <nextAutomatedApprover>
        <useApproverFieldOfRecordOwner>false</useApproverFieldOfRecordOwner>
        <userHierarchyField>Manager</userHierarchyField>
    </nextAutomatedApprover>
    <recallActions>
        <action>
            <name>Delete_Submitted_Date</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Update_Status_Draft</name>
            <type>FieldUpdate</type>
        </action>
    </recallActions>
    <recordEditability>AdminOrCurrentApprover</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
