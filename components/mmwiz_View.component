<apex:component controller="mmwiz_WizardExtension">

    <apex:attribute name="questionnaireName" description="The name of the questionnaire to view." type="String" required="true"/>
    <apex:attribute name="suppressQuestionnairHeader" description="Supplying 'true' hides the questionnaire's overview text." type="Boolean" required="false" default="false"/>

    <!-- =============== Header Section ========================================================= -->
        <apex:slds />

        <!-- Get a copy of JQuery -->
        <apex:includeScript value="https://code.jquery.com/jquery-3.2.1.min.js" />
        <apex:includeScript value="{! urlfor($Resource.Morgan, 'Morgan/scripts/mm-jquery-geturlvar.js') }" />
        <apex:includeScript value="{! $Resource.mmlib_js_Utils }" loadOnReady="true"/>

        <apex:includeScript value="{! urlfor($Resource.mm_chosen_js_v01, 'chosen.js') }" />
        <apex:stylesheet value="{! urlfor($Resource.mm_chosen_js_v01, 'chosen.min.css') }" />

        <script type="text/javascript">
            var pageUrl = '{! JSENCODE($CurrentPage.URL) }';
            var urlQuestionnaireToken = '{! JSENCODE(questionnaireName) }';
            var urlPageToken = '{! JSENCODE($CurrentPage.parameters.page) }';
            var urlDebugMode = '{! JSENCODE($CurrentPage.parameters.debug) }';
            var initSessionGuid = '{! JSENCODE($CurrentPage.parameters.CallVariable6) }';
            var sessionGuid = '{! JSENCODE($CurrentPage.parameters.CallVariable6) }';
            var suppressQuestionnairHeader = '{! suppressQuestionnairHeader }';
        </script>
    <!-- =============== /Header Section ========================================================= -->
    <!-- =============== Body Section ========================================================= -->

        <!-- REQUIRED SLDS WRAPPER -->
        <div class="slds-scope">

            <!-- MASTHEAD -->
            <!--
            < p class="slds-text-heading--label slds-m-bottom--small">__Questionnaire Title__< / p >
             -->
            <!-- / MASTHEAD -->

            <!-- PAGE HEADER -->
            <div class="slds-page-header">

                <!-- PAGE HEADER TOP ROW -->
                <div class="slds-grid">

                    <!-- PAGE HEADER / ROW 1 / COLUMN 1 -->
                    <div class="slds-col slds-has-flexi-truncate">

                        <!-- HEADING AREA -->
                        <!-- MEDIA OBJECT = FIGURE + BODY -->
                        <div class="slds-media slds-no-space slds-grow">
                            <div class="slds-media__figure">
                                <c:mmwiz_SvgSprite xlinkHref="{!URLFOR($Asset.SLDS, 'assets/icons/standard-sprite/svg/symbols.svg#task')}"
                                    svgClass="slds-icon slds-icon-standard-user" />
                            </div>

                            <div class="slds-media__body">
                                <p class="slds-text-title--caps slds-line-height--reset">Questionnaire</p>
                                <h1 id="currentQuestionnaireTitle" class="slds-page-header__title slds-m-right--small slds-align-middle" title=""></h1>
                            </div>
                        </div>
                        <!-- / MEDIA OBJECT -->
                        <!-- HEADING AREA -->

                    </div>
                    <!-- / PAGE HEADER / ROW 1 / COLUMN 1 -->

                    <!-- PAGE HEADER / ROW 1 / COLUMN 2 -->
                    <!--
                    <div class="slds-col slds-no-flex slds-grid slds-align-top">
                        <div class="slds-button-group" role="group">
                            <button class="slds-button slds-button--neutral">
                                More
                            </button>
                        </div>
                    </div>
                     -->
                    <!-- / PAGE HEADER / ROW 1 / COLUMN 2 -->

                </div>
                <!-- / PAGE HEADER TOP ROW -->
                <!--
                <apex:outputPanel layout="none" rendered="{! not(isBlank(currentQuestionnaireModel.displayText)) }" >
                -->
                <!-- PAGE HEADER DETAIL ROW -->
                <ul class="slds-grid slds-page-header__detail-row" style="display:{! if(suppressQuestionnairHeader, 'none', '') }">

                    <!-- PAGE HEADER / ROW 2 / COLUMN 1 -->
                    <li>
                        <p class="slds-text-title slds-truncate slds-m-bottom--xx-small" title="Overview">Overview</p>
                        <!--
                        <p class="slds-text-body--large" title="{! currentQuestionnaireModel.displayText }">{! currentQuestionnaireModel.displayText }</p>
                        -->
                        <p id="currentQuestionnaireOverview" class="slds-text-body--large" title=""></p>
                    </li>
                </ul>
                <!-- / PAGE HEADER DETAIL ROW -->
                <!--
                </apex:outputPanel>
                -->
            </div>
            <!-- / PAGE HEADER -->


            <!-- ************************************************************************************************************ -->

            <!-- PRIMARY CONTENT WRAPPER -->
            <div class="myapp">

                <!-- RELATED LIST CARDS-->
                <div class="slds-grid slds-m-top--large">

                    <!-- QUESTIONS CARD -->
                    <div class="slds-col slds-col-rule--right slds-p-right--large slds-size--5-of-12">

                        <article class="slds-card slds-card--narrow slds-grid--align-spread mm-question-board">

                            <!-- CARD HEADER -->
                            <div class="slds-card__header slds-grid">

                                <header class="slds-media slds-media--center slds-has-flexi-truncate">
                                    <div class="slds-media__figure">
                                        <c:mmwiz_SvgSprite xlinkHref="{!URLFOR($Asset.SLDS, 'assets/icons/standard-sprite/svg/symbols.svg#question_feed')}"
                                                        svgClass="slds-icon slds-icon-standard-lead slds-icon--small" />
                                    </div>

                                    <div class="slds-media__body slds-truncate">
                                        <h2 class="slds-text-heading--small">Questions</h2>
                                    </div>
                                </header>
                            </div>
                            <!-- / CARD HEADER -->

                            <!-- CARD BODY -->
                            <div class="slds-card__body">
                                <div class="slds-card__body--inner">

                                    <!-- QUESTION 1 TILE -->

                                    <div class="slds-tile slds-tile--board">

                                    <form class="slds-form--stacked" id="questions-form">


                                    </form>

                                    <!-- / QUESTION 1 TILE -->
                                    </div>
                                </div>

                            </div>
                            <!-- / CARD BODY -->

                            <!-- CARD FOOTER -->
                            <div class="slds-card__footer">
                                <div class="slds-button-group" role="group">
                                    <form id="buttons_form">
	                                    <button class="slds-button  slds-button--neutral" id="page-questions-btn-1">Previous</button>
	                                    <button class="slds-button  slds-button--neutral" id="page-questions-btn-2">Next</button>
        	                            <button class="slds-button  slds-button--neutral" id="page-questions-btn-3">Submit</button>
            	                        <button class="slds-button  slds-button--neutral" id="page-questions-btn-4">Reset Form</button>
                                    </form>
                                </div>
                            </div>
                            <!-- / CARD FOOTER -->
                        </article>

                        <!-- ************************************************************************************************************ -->

                    </div>
                    <!-- / QUESTIONS CARD -->

                    <!-- DISPLAY TEXT CARD -->
                    <div class="slds-col slds-p-left--large slds-p-right--large slds-size--7-of-12">
                        <article class="slds-card slds-card--narrow" style="height:100%;">
                            <!-- CARD HEADER -->
                            <div class="slds-card__header slds-grid">

                                <header class="slds-media slds-media--center slds-has-flexi-truncate">
                                    <div class="slds-media__figure">
                                        <c:mmwiz_SvgSprite xlinkHref="{!URLFOR($Asset.SLDS, 'assets/icons/standard-sprite/svg/symbols.svg#solution')}"
                                                        svgClass="slds-icon slds-icon-standard-lead slds-icon--small" />
                                    </div>

                                    <div class="slds-media__body slds-truncate">
                                        <h2 class="slds-text-heading--small">Response Actions</h2>
                                    </div>
                                </header>
                            </div>
                            <!-- / CARD HEADER -->
                            <div class="slds-card__body">
                                <div class="slds-card__body--inner">
                                    <div class="slds-tile">
                                        <div class="slds-tile__detail slds-text-body--large">
                                            <div id="question-text-response"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- / DISPLAY TEXT CARD -->


                </div>
                <!-- / RELATED LIST CARDS -->

            </div>
            <!-- / PRIMARY CONTENT WRAPPER -->

            <!-- ************************************************************************************************************ -->

            <!-- FOOTER -->
            <footer role="contentinfo" class="slds-p-around--large">
                <!-- LAYOUT GRID -->
                <div class="slds-grid slds-grid--align-spread">
                
                <!-- <p class="slds-col">__Questionnaire Title__</p> -->
                </div>
                <!-- / LAYOUT GRID -->
            </footer>
            <!-- / FOOTER -->

            <!-- ************************************************************************************************************ -->
            <div id="questionTemplate" style="display:none;">
                    <!-- QUESTION TYPE RADIO BUTTON FIELDSET -->
                    <fieldset class="slds-form-element" id="<!-- questionModel.token -->">
                        <legend class="slds-form-element__legend slds-form-element__label">
                            <p class="slds-text-heading--small"><!-- questionModel.displayText --></p>
                        </legend>
                        <span class="error_msg slds-text-color_error"></span>
                        <div class="slds-form-element__control">
                            <!-- answer template goes here -->
                        </div>
                    </fieldset>
                    <!-- QUESTION TYPE RADIO BUTTON FIELDSET -->
            </div>
            <div id="answerTemplate_radio" style="display:none;">
                    <!-- ANSWER SPAN FIELDSET -->
                    <span class="slds-radio">
                        <input type="radio" id="<!-- answerModel.token -->" name="<!-- questionModel.token  -->" checked="" />
                        <label class="slds-radio__label" for="<!-- questionModel.token  -->">
                            <span class="slds-radio--faux"></span>
                            <span class="slds-form-element__label"><!-- answerModel.displayText --></span>
                        </label>
                    </span>
                    <!-- ANSWER SPAN FIELDSET -->
            </div>
            <div id="answerTemplate_picklist" style="display:none;">
                    <!-- ANSWER SPAN FIELDSET -->
                    <span class="slds-select_container">
                        <select class="slds-select" id="<!-- questionModel.token  -->" name="<!-- questionModel.token  -->">
                            <option value="<!-- answerModel.token -->"><!-- answerModel.displayText --></option>
                        </select>
                    </span>
                    <!-- ANSWER SPAN FIELDSET -->
            </div>

            <!-- ************************************************************************************************************ -->


            </div>
            <!-- / REQUIRED SLDS WRAPPER -->

            <div class="debugInfo" style="display:none;">
                Debug Info:
                <ul>
                </ul>
            </div>

            <c:mmwiz_JavascriptRemoting />

            <apex:includeScript value="{! $Resource.mmwiz }" loadOnReady="true"/>

    <!-- =============== /Body Section ========================================================= -->

</apex:component>