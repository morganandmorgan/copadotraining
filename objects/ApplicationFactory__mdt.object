<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>The records are used to setup the Application Factory</description>
    <fields>
        <fullName>ClassToInject__c</fullName>
        <description>The Apex class that will be created by the factory.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>The Apex class that will be created by the factory.</inlineHelpText>
        <label>Class To Inject</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Interface__c</fullName>
        <description>The interface that would be input for a ServiceFactory reference.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The interface that would be input for a ServiceFactory reference.</inlineHelpText>
        <label>Interface</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SObjectType__c</fullName>
        <description>The SObject Type -- aka SObject API name.  This is required for Domain, Unit Of Work and Selector types.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The SObject Type -- aka SObject API name.  This is required for Domain, Unit Of Work and Selector types.</inlineHelpText>
        <label>SObject Type</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SortOrder__c</fullName>
        <description>For UnitOfWork entries, you must supply the sort order that a SObject should be processed</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>For UnitOfWork entries, you must supply the sort order that a SObject should be processed</inlineHelpText>
        <label>Sort Order</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>The factory type that this record defines.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>The factory type that this record defines.</inlineHelpText>
        <label>Type</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Domain</fullName>
                    <default>true</default>
                    <label>Domain</label>
                </value>
                <value>
                    <fullName>Selector</fullName>
                    <default>false</default>
                    <label>Selector</label>
                </value>
                <value>
                    <fullName>Service</fullName>
                    <default>false</default>
                    <label>Service</label>
                </value>
                <value>
                    <fullName>UnitOfWork</fullName>
                    <default>false</default>
                    <label>Unit Of Work</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Application Factory</label>
    <listViews>
        <fullName>All</fullName>
        <columns>DeveloperName</columns>
        <columns>Type__c</columns>
        <columns>SObjectType__c</columns>
        <columns>Interface__c</columns>
        <columns>ClassToInject__c</columns>
        <columns>SortOrder__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <pluralLabel>Application Factories</pluralLabel>
    <validationRules>
        <fullName>Domain_ClassToInject_MustBIConstructable</fullName>
        <active>true</active>
        <description>The Domain class to inject should be the fflib_SObjectDomain.IConstructable Constructor inner class of the Domain object.</description>
        <errorConditionFormula>AND( RIGHT( ClassToInject__c, 12 ) != &apos;.Constructor&apos;, TEXT( Type__c ) == &apos;Domain&apos; )</errorConditionFormula>
        <errorDisplayField>ClassToInject__c</errorDisplayField>
        <errorMessage>The class to inject should be the fflib_SObjectDomain.IConstructable Constructor inner class of the Domain object.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Domain_ClassToInject_Required</fullName>
        <active>true</active>
        <description>Validates that the Class To Inject field is present if the type is Domain</description>
        <errorConditionFormula>AND( ISBLANK( ClassToInject__c ), TEXT( Type__c ) == &apos;Domain&apos; )</errorConditionFormula>
        <errorDisplayField>ClassToInject__c</errorDisplayField>
        <errorMessage>Required for type &quot;Domain&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Domain_Interface_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Domain</description>
        <errorConditionFormula>AND( NOT(ISBLANK(  Interface__c  )), TEXT( Type__c ) == &apos;Domain&apos; )</errorConditionFormula>
        <errorDisplayField>Interface__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Domain&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Domain_SObject_Type_Required</fullName>
        <active>true</active>
        <description>Validates that the Sobject Type field is present if the type is Domain</description>
        <errorConditionFormula>AND( ISBLANK( SObjectType__c ), TEXT( Type__c ) == &apos;Domain&apos; )</errorConditionFormula>
        <errorDisplayField>SObjectType__c</errorDisplayField>
        <errorMessage>Required for type &quot;Domain&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Domain_SortOrder_Field_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Domain</description>
        <errorConditionFormula>AND( NOT(ISBLANK(  SortOrder__c  )), TEXT( Type__c ) == &apos;Domain&apos; )</errorConditionFormula>
        <errorDisplayField>SortOrder__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Domain&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Selector_ClassToInject_Required</fullName>
        <active>true</active>
        <description>Validates that the Class To Inject field is present if the type is Selector</description>
        <errorConditionFormula>AND( ISBLANK( ClassToInject__c ), TEXT( Type__c ) == &apos;Selector&apos; )</errorConditionFormula>
        <errorDisplayField>ClassToInject__c</errorDisplayField>
        <errorMessage>Required for type &quot;Selector&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Selector_Interface_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Selector</description>
        <errorConditionFormula>AND( NOT(ISBLANK( Interface__c )), TEXT( Type__c ) == &apos;Selector&apos; )</errorConditionFormula>
        <errorDisplayField>Interface__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Selector&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Selector_SObjectType_Required</fullName>
        <active>true</active>
        <description>Validates that the SObject Type field is present if the type is Selector</description>
        <errorConditionFormula>AND( ISBLANK(  SObjectType__c  ), TEXT( Type__c ) == &apos;Selector&apos; )</errorConditionFormula>
        <errorDisplayField>SObjectType__c</errorDisplayField>
        <errorMessage>Required for type &quot;Selector&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Selector_SortOrder_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Selector</description>
        <errorConditionFormula>AND( NOT(ISBLANK(  SortOrder__c  )), TEXT( Type__c ) == &apos;Selector&apos; )</errorConditionFormula>
        <errorDisplayField>SortOrder__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Selector&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Service_ClassToInject_Required</fullName>
        <active>true</active>
        <description>Validates that the Class To Inject field is present if the type is Service</description>
        <errorConditionFormula>AND( ISBLANK( ClassToInject__c ), TEXT( Type__c ) == &apos;Service&apos; )</errorConditionFormula>
        <errorDisplayField>ClassToInject__c</errorDisplayField>
        <errorMessage>Required for type &quot;Service&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Service_Interface_Required</fullName>
        <active>true</active>
        <description>Validates that the Interface field is present if the type is Service</description>
        <errorConditionFormula>AND( ISBLANK( Interface__c  ), TEXT( Type__c ) == &apos;Service&apos; )</errorConditionFormula>
        <errorDisplayField>Interface__c</errorDisplayField>
        <errorMessage>Required for type &quot;Service&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Service_SObjectType_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Service</description>
        <errorConditionFormula>AND( NOT(ISBLANK( SObjectType__c )), TEXT( Type__c ) == &apos;Service&apos; )</errorConditionFormula>
        <errorDisplayField>SObjectType__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Service&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Service_SortOrder_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Service</description>
        <errorConditionFormula>AND( NOT(ISBLANK( SortOrder__c )), TEXT( Type__c ) == &apos;Service&apos; )</errorConditionFormula>
        <errorDisplayField>SortOrder__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Service&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Unit_Of_Work_ClassToInject_ShouldBeEmpty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Unit Of Work</description>
        <errorConditionFormula>AND( NOT(ISBLANK( ClassToInject__c )), TEXT( Type__c ) == &apos;UnitOfWork&apos; )</errorConditionFormula>
        <errorDisplayField>ClassToInject__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Unit Of Work&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Unit_Of_Work_Interface_Should_Be_Empty</fullName>
        <active>true</active>
        <description>Validates that non-required fields are empty for type Unit Of Work</description>
        <errorConditionFormula>AND( NOT(ISBLANK( Interface__c )), TEXT( Type__c ) == &apos;UnitOfWork&apos; )</errorConditionFormula>
        <errorDisplayField>Interface__c</errorDisplayField>
        <errorMessage>Field should be blank for type &quot;Unit Of Work&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Unit_Of_Work_Sobject_Type_Required</fullName>
        <active>true</active>
        <description>Validates that the Sobject Type field is present if the type is Unit Of Work</description>
        <errorConditionFormula>AND( ISBLANK( SObjectType__c ), TEXT( Type__c ) == &apos;UnitOfWork&apos; )</errorConditionFormula>
        <errorDisplayField>SObjectType__c</errorDisplayField>
        <errorMessage>Required for type &quot;Unit Of Work&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Unit_Of_Work_SortOrder_Required</fullName>
        <active>true</active>
        <description>Validates that the Sort Order field is present if the type is Unit Of Work</description>
        <errorConditionFormula>AND( OR( ISBLANK( SortOrder__c ),SortOrder__c &lt; 0.0000001), TEXT( Type__c ) == &apos;UnitOfWork&apos; )</errorConditionFormula>
        <errorDisplayField>SortOrder__c</errorDisplayField>
        <errorMessage>Required for type &quot;Unit Of Work&quot; and cannot be &quot;0&quot; or negative.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
