<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Contains the data for generating and executing question, answer, and action wizards.  LIVR Tags</description>
    <fields>
        <fullName>Case_Type__c</fullName>
        <description>used to tell flow which Case type flow they should go to</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Case Type</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Information presented to the user detailing the purpose or usage of this record.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Information presented to the user detailing the purpose or usage of this record.</inlineHelpText>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DisplayText__c</fullName>
        <description>Specifies the text that is displayed to the user. Carriage returns and HTML markup are allowed.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Specifies the text that is displayed to the user. Carriage returns and HTML markup are allowed.</inlineHelpText>
        <label>Display Text</label>
        <length>131072</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>IsActive__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Specifies whether the record should be used.  If not checked (false), the item will continue to be displayed to the user but not implemented by the business logic.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Specifies whether the record should be used.  If not checked (false), the item will continue to be displayed to the user but not implemented by the business logic.</inlineHelpText>
        <label>Is Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsDeleted__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates this item is virtually deleted.  If checked (true), the item will not be displayed to the user nor implemented by the business logic.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Indicates this item is virtually deleted.  If checked (true), the item will not be displayed to the user nor implemented by the business logic.</inlineHelpText>
        <label>Is Deleted</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Litigation__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Litigation</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SerializedData__c</fullName>
        <description>JSON formatted lists specifying the items that are related to this record.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>JSON formatted lists specifying the items that are related to this record.</inlineHelpText>
        <label>Serialized Data</label>
        <length>131072</length>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>The string representation of the model Apex class.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>The string representation of the model Apex class.</inlineHelpText>
        <label>Type</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Wizard</label>
    <listViews>
        <fullName>All</fullName>
        <columns>MasterLabel</columns>
        <columns>DeveloperName</columns>
        <columns>IsActive__c</columns>
        <columns>Type__c</columns>
        <columns>Description__c</columns>
        <columns>SerializedData__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>View</fullName>
        <columns>Type__c</columns>
        <columns>MasterLabel</columns>
        <columns>DeveloperName</columns>
        <columns>NamespacePrefix</columns>
        <columns>IsActive__c</columns>
        <filterScope>Everything</filterScope>
        <label>View</label>
        <language>en_US</language>
    </listViews>
    <pluralLabel>Wizards</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
