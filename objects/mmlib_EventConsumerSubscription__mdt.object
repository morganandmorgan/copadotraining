<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>The records are used to setup the subscription to various platform events.</description>
    <fields>
        <fullName>Consumer__c</fullName>
        <description>The name of the Apex Class that implements mmlib_IEventConsumer that the subscription is operating on behalf of.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The name of the Apex Class that implements mmlib_IEventConsumer that the subscription is operating on behalf of.</inlineHelpText>
        <label>Consumer</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EventSObjectCategory__c</fullName>
        <description>The API name of the related SObject that this subscription is for (Example: &quot;Account&quot;, or &quot;Intake__c&quot;).  It could also represent a non-SObject, arbitrary, category ( Example: &quot;Apollo&quot; or &quot;ClientProfile )..  This helps to filter subscriptions better.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The API name of the related SObject that this subscription is for (Example: &quot;Account&quot;, or &quot;Intake__c&quot;).  It could also represent a non-SObject, arbitrary, category ( Example: &quot;Apollo&quot; or &quot;ClientProfile )..  This helps to filter subscriptions better.</inlineHelpText>
        <label>Event SObject Category</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Event__c</fullName>
        <description>The specific event that this subscription is listening for.  Example: &quot;UserCreation&quot;</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The specific event that this subscription is listening for.  Example: &quot;UserCreation&quot;</inlineHelpText>
        <label>Event</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsActive__c</fullName>
        <defaultValue>true</defaultValue>
        <description>If checked, then this subscription is active and an event of matching this description will be distributed to the consumer specified.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>If checked, then this subscription is active and an event of matching this description will be distributed to the consumer specified.</inlineHelpText>
        <label>Is Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MatcherRule__c</fullName>
        <defaultValue>&quot;Match Platform Event Bus and Related SObject and Event Name&quot;</defaultValue>
        <description>This determines which fields of the subscription that must match the event in order for the event to be distributed to the subscriber.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>This determines which fields of the subscription that must match the event in order for the event to be distributed to the subscriber.</inlineHelpText>
        <label>Matcher Rule</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>MatchPlatformEventBusAndRelatedSObjectAndEventName</fullName>
                    <default>true</default>
                    <label>Match Platform Event Bus and Related SObject and Event Name</label>
                </value>
                <value>
                    <fullName>MatchPlatformEventBusAndRelatedSObjectOnly</fullName>
                    <default>false</default>
                    <label>Match Platform Event Bus and Related SObject Only</label>
                </value>
                <value>
                    <fullName>MatchPlatformEventBusandEventName</fullName>
                    <default>false</default>
                    <label>Match Platform Event Bus and Event Name</label>
                </value>
                <value>
                    <fullName>MatchPlatformEventBusOnly</fullName>
                    <default>false</default>
                    <label>Match Platform Event Bus Only</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>RelatedPlatformEventBus__c</fullName>
        <defaultValue>&quot;mmlib_Event__e&quot;</defaultValue>
        <description>This is the platform event bus related that this subscription relates to.  i.e.  mmlib_Event__e</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>This is the platform event bus related that this subscription relates to.  i.e.  mmlib_Event__e</inlineHelpText>
        <label>Related Platform Event Bus</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>mmlib_Event__e</fullName>
                    <default>true</default>
                    <label>mmlib_Event__e</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Event Consumer Subscription</label>
    <listViews>
        <fullName>All</fullName>
        <columns>MasterLabel</columns>
        <columns>DeveloperName</columns>
        <columns>Consumer__c</columns>
        <columns>Event__c</columns>
        <columns>EventSObjectCategory__c</columns>
        <columns>IsActive__c</columns>
        <columns>MatcherRule__c</columns>
        <columns>RelatedPlatformEventBus__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <pluralLabel>Event Consumer Subscriptions</pluralLabel>
    <validationRules>
        <fullName>Label_Should_Be_Combination</fullName>
        <active>false</active>
        <description>The record label should be the combination of the &lt;Related Platform Event Bus (without the &quot;__e&quot; suffix)&gt;_Event SObject Category (without the &quot;__c&quot; suffix - if present)&gt;_&lt;Event&gt;_&lt;Consumer&gt;</description>
        <errorConditionFormula>MasterLabel != LEFT( SUBSTITUTE( TEXT(RelatedPlatformEventBus__c), &apos;__e&apos;, &apos;&apos;) &amp; SUBSTITUTE(EventSObjectCategory__c, &apos;__e&apos;, &apos;&apos;) &amp; &apos;_&apos; &amp; Event__c &amp; &apos;_&apos; &amp; Consumer__c, 40)</errorConditionFormula>
        <errorDisplayField>MasterLabel</errorDisplayField>
        <errorMessage>The record label should be the combination of the &lt;Related Platform Event Bus (without the &quot;__e&quot; suffix)&gt;_Event SObject Category (without the &quot;__c&quot; suffix - if present)&gt;_&lt;Event&gt;_&lt;Consumer&gt;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Name_Should_Be_Combination</fullName>
        <active>false</active>
        <description>The record name should be the combination of the &lt;Related Platform Event Bus (without the &quot;__e&quot; suffix)&gt;_Event SObject Category (without the &quot;__c&quot; suffix - if present)&gt;_&lt;Event&gt;_&lt;Consumer&gt;</description>
        <errorConditionFormula>DeveloperName != LEFT( SUBSTITUTE( TEXT(RelatedPlatformEventBus__c), &apos;__e&apos;, &apos;&apos;) &amp; SUBSTITUTE(EventSObjectCategory__c, &apos;__e&apos;, &apos;&apos;) &amp; &apos;_&apos; &amp; Event__c &amp; &apos;_&apos; &amp; Consumer__c, 40)</errorConditionFormula>
        <errorDisplayField>DeveloperName</errorDisplayField>
        <errorMessage>The record name should be the combination of the &lt;Related Platform Event Bus (without the &quot;__e&quot; suffix)&gt;_Event SObject Category (without the &quot;__c&quot; suffix - if present)&gt;_&lt;Event&gt;_&lt;Consumer&gt;</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
