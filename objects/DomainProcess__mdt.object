<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>This custom metadata type is responsible for controlling the dependency injection of domain criteria and actions.</description>
    <fields>
        <fullName>ClassToInject__c</fullName>
        <description>This is the Apex class that will be injected into the process execution.  If the domain process type is &quot;Criteria&quot;, then the class has to implement mmlib_ICriteria or mmlib_ICriteriaWithExistingRecords.  If the domain process type is &quot;Action&quot;, then the class has to implement mmlib_IAction.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>This is the Apex class that will be injected into the process execution.</inlineHelpText>
        <label>Class To Inject</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Brief description of why this is present.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Brief description of why this is present.</inlineHelpText>
        <label>Description</label>
        <required>true</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>DomainMethodToken__c</fullName>
        <description>In a process context of Domain Method Execution, you will need to provide a Domain Method Token -- some unique string that will be supplied to the dependency injection process from the domain class method.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>In a process context of Domain Method Execution, you will need to provide a Domain Method Token -- some unique string that will be supplied to the dependency injection process from the domain class method.</inlineHelpText>
        <label>Domain Method Token</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExecuteAsynchronous__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, then the action will be executed in Queueable context and not the primary execution thread.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>If checked, then the action will be executed in Queueable context and not the primary execution thread.</inlineHelpText>
        <label>Execute Asynchronous</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsActive__c</fullName>
        <defaultValue>true</defaultValue>
        <description>If this is unchecked, this criteria or action will be ignored by the process.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>If this is unchecked, this criteria or action will be ignored by the process.</inlineHelpText>
        <label>Is Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>LogicalInverse__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, then the logical inverse of the associated criteria will be used for the criteria.  Basically, instead of the criteria retaining the records, those records will be filtered out of the working set.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>If checked, then the logical inverse of the associated criteria will be used for the criteria.  Basically, instead of the criteria retaining the records, those records will be filtered out of the working set.</inlineHelpText>
        <label>Logical Inverse</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>OrderOfExecution__c</fullName>
        <description>Within a process context and &quot;trigger operation&quot;/&quot;domain method token&quot;, you will have one or more criteria and one or more actions.  This allows you to specify what the execution order is.  If more than one sequence of criteria and actions are required, then designate this by using decimal numbers.  i.e. 1.1-criteria, 1.2-critera, 1.3-action, 2.1-criteria, 2.2-action</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Within a process context and &quot;trigger operation&quot;/&quot;domain method token&quot;, you will have one or more criteria and one or more actions.  This allows you to specify what the execution order is.</inlineHelpText>
        <label>Order Of Execution</label>
        <precision>4</precision>
        <required>true</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PreventRecursive__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, then the execution of this action a second time in the trigger execution context is prevented.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>If checked, then the execution of this action a second time in the trigger execution context is prevented.</inlineHelpText>
        <label>Prevent Recursive</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ProcessContext__c</fullName>
        <description>Represents what type of process will be initiating the execution of the criteria and actions.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Represents what type of process will be initiating the execution of the criteria and actions.</inlineHelpText>
        <label>Process Context</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>DomainMethodExecution</fullName>
                    <default>true</default>
                    <label>Domain Method Execution</label>
                </value>
                <value>
                    <fullName>TriggerExecution</fullName>
                    <default>false</default>
                    <label>Trigger Execution</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>RelatedDomain__c</fullName>
        <description>The &quot;domain&quot; that this process arrangement is related to.  Usually this is the related SObject API name, especially in a Trigger Execution, but it is not limited to just that.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>The &quot;domain&quot; that this process arrangement is related to.  Usually this is the related SObject API name, especially in a Trigger Execution, but it is not limited to just that.</inlineHelpText>
        <label>Related Domain</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TriggerOperation__c</fullName>
        <description>For a process context of &quot;trigger execution&quot;, you need to specify which trigger operation this criteria or action would respond to.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>For a process context of &quot;trigger execution&quot;, you need to specify which trigger operation this criteria or action would respond to.</inlineHelpText>
        <label>Trigger Operation</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>AfterDelete</fullName>
                    <default>true</default>
                    <label>After Delete</label>
                </value>
                <value>
                    <fullName>AfterInsert</fullName>
                    <default>false</default>
                    <label>After Insert</label>
                </value>
                <value>
                    <fullName>AfterUndelete</fullName>
                    <default>false</default>
                    <label>After Undelete</label>
                </value>
                <value>
                    <fullName>AfterUpdate</fullName>
                    <default>false</default>
                    <label>After Update</label>
                </value>
                <value>
                    <fullName>BeforeDelete</fullName>
                    <default>false</default>
                    <label>Before Delete</label>
                </value>
                <value>
                    <fullName>BeforeInsert</fullName>
                    <default>false</default>
                    <label>Before Insert</label>
                </value>
                <value>
                    <fullName>BeforeUpdate</fullName>
                    <default>false</default>
                    <label>Before Update</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>Domain processes are composed of criteria and actions -- which type is this entry??</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Domain processes are composed of criteria and actions -- which type is this entry??</inlineHelpText>
        <label>Type</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Action</fullName>
                    <default>false</default>
                    <label>Action</label>
                </value>
                <value>
                    <fullName>Criteria</fullName>
                    <default>false</default>
                    <label>Criteria</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Domain Process</label>
    <listViews>
        <fullName>All</fullName>
        <columns>MasterLabel</columns>
        <columns>IsActive__c</columns>
        <columns>ProcessContext__c</columns>
        <columns>RelatedDomain__c</columns>
        <columns>TriggerOperation__c</columns>
        <columns>DomainMethodToken__c</columns>
        <columns>Type__c</columns>
        <columns>OrderOfExecution__c</columns>
        <columns>ClassToInject__c</columns>
        <columns>ExecuteAsynchronous__c</columns>
        <columns>PreventRecursive__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <pluralLabel>Domain Processes</pluralLabel>
    <validationRules>
        <fullName>DomainMethodTokenValidOnlyOnProcessDME</fullName>
        <active>true</active>
        <description>DomainMethodToken__c can only be populated for ProcessContext__c = &quot;DomainMethodExecution&quot;</description>
        <errorConditionFormula>not( isblank( DomainMethodToken__c ) ) &amp;&amp; text( ProcessContext__c  ) != &quot;DomainMethodExecution&quot;</errorConditionFormula>
        <errorDisplayField>DomainMethodToken__c</errorDisplayField>
        <errorMessage>The &quot;Domain Method Token&quot; should only be populated when the process context is &quot;Domain Method Execution&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ExecuteAsynchronousValidOnlyOnTypeAction</fullName>
        <active>true</active>
        <description>ExecuteAsynchronous__c can only be active for Type__c = &quot;Action&quot;</description>
        <errorConditionFormula>TEXT(Type__c) != &quot;Action&quot; &amp;&amp;  ExecuteAsynchronous__c</errorConditionFormula>
        <errorDisplayField>ExecuteAsynchronous__c</errorDisplayField>
        <errorMessage>&quot;Execute Asynchronous&quot; can only be true when Type is &quot;Action&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>LogicalInverseValidOnlyOnTypeCriteria</fullName>
        <active>true</active>
        <description>The LogicalInverse__c field is only valid when the type is Criteria.</description>
        <errorConditionFormula>TEXT(Type__c) != &quot;Criteria&quot; &amp;&amp; LogicalInverse__c</errorConditionFormula>
        <errorDisplayField>LogicalInverse__c</errorDisplayField>
        <errorMessage>&quot;Logical Inverse&quot; can only be true when Type is &quot;Criteria&quot;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>PreventRecursiveValidOnlyOnProcessTE</fullName>
        <active>true</active>
        <description>PreventRecursive__c can only be active for ProcessContext__c = &quot;TriggerExecution&quot;</description>
        <errorConditionFormula>PreventRecursive__c &amp;&amp; text( ProcessContext__c ) != &quot;TriggerExecution&quot;</errorConditionFormula>
        <errorDisplayField>PreventRecursive__c</errorDisplayField>
        <errorMessage>&quot;Prevent Recursive&quot; is only valid when the process context is &quot;Trigger Execution&quot;</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
