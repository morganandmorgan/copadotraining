<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>Resolution_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Resolution_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>litify_pm__Resolution_Date__c</fields>
        <fields>litify_pm__Insurance__c</fields>
        <fields>litify_pm__Settlement_Verdict_Amount__c</fields>
        <fields>Matter_Name__c</fields>
        <label>Resolution Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Case_Manager_Credited__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case Manager Credited</label>
        <referenceTo>User</referenceTo>
        <relationshipName>CaseManagerResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Finance_Request__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Connects the Resolution to a Finance Request so that when users are updating a Closing Statement they can Update the Resolution as well.</description>
        <externalId>false</externalId>
        <label>Finance Request</label>
        <referenceTo>Finance_Request__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>Resolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Insurance_Display_Name__c</fullName>
        <externalId>false</externalId>
        <formula>litify_pm__Insurance__r.Insurance_Display__c</formula>
        <label>Insurance Display Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Matter_Name__c</fullName>
        <externalId>false</externalId>
        <formula>litify_pm__Matter__r.Record_Name__c</formula>
        <label>Matter Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Amount_Due_to_Client__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Amount Due to Client</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Attorney_Credited__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Attorney Credited</label>
        <referenceTo>User</referenceTo>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Contingency_Fee_Rate__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contingency Fee Rate (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>litify_pm__Days_to_Resolve__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(
AND(
NOT(ISBLANK(litify_pm__Matter__c)),
NOT(ISBLANK(litify_pm__Matter__r.litify_pm__Open_Date__c)),
NOT(ISBLANK(litify_pm__Resolution_Date__c))),

litify_pm__Resolution_Date__c-litify_pm__Matter__r.litify_pm__Open_Date__c, null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Days to Resolve</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Fees_Due_to_Others__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fees Due to Others</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Gross_Attorney_Fee__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Gross Attorney Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Insurance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Insurance</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>litify_pm__Insurance__c.litify_pm__Matter__c</field>
                <operation>equals</operation>
                <valueField>$Source.litify_pm__Matter__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>litify_pm__Insurance__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Matter__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Matter</label>
        <referenceTo>litify_pm__Matter__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Negotiation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Negotiation</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>litify_pm__Negotiation__c.litify_pm__Matter__c</field>
                <operation>equals</operation>
                <valueField>$Source.litify_pm__Matter__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>litify_pm__Negotiation__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Net_Attorney_Fee__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Net Attorney Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Payor_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payor Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Attorney</fullName>
                    <default>false</default>
                    <label>Attorney</label>
                </value>
                <value>
                    <fullName>Defendant</fullName>
                    <default>false</default>
                    <label>Defendant</label>
                </value>
                <value>
                    <fullName>Insurance</fullName>
                    <default>false</default>
                    <label>Insurance</label>
                </value>
                <value>
                    <fullName>Third-Party Administrator (TPA)</fullName>
                    <default>false</default>
                    <label>Third-Party Administrator (TPA)</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>litify_pm__Payor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payor</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Resolutions (Payor)</relationshipLabel>
        <relationshipName>LitifyResolutionsPayor</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Reason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reason</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>litify_pm__Resolution_Type__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Calls not being returned</fullName>
                    <default>false</default>
                    <label>Calls not being returned</label>
                </value>
                <value>
                    <fullName>Cannot locate defendant</fullName>
                    <default>false</default>
                    <label>Cannot locate defendant</label>
                </value>
                <value>
                    <fullName>Client did not treat</fullName>
                    <default>false</default>
                    <label>Client did not treat</label>
                </value>
                <value>
                    <fullName>Client missing</fullName>
                    <default>false</default>
                    <label>Client missing</label>
                </value>
                <value>
                    <fullName>Client Paid Correctly</fullName>
                    <default>false</default>
                    <label>Client Paid Correctly</label>
                </value>
                <value>
                    <fullName>Client unavailable or deceased</fullName>
                    <default>false</default>
                    <label>Client unavailable or deceased</label>
                </value>
                <value>
                    <fullName>Conflict of Interest</fullName>
                    <default>false</default>
                    <label>Conflict of Interest</label>
                </value>
                <value>
                    <fullName>Decided not to pursue claim</fullName>
                    <default>false</default>
                    <label>Decided not to pursue claim</label>
                </value>
                <value>
                    <fullName>Decided to accept prior offer</fullName>
                    <default>false</default>
                    <label>Decided to accept prior offer</label>
                </value>
                <value>
                    <fullName>Defendant did not respond</fullName>
                    <default>false</default>
                    <label>Defendant did not respond</label>
                </value>
                <value>
                    <fullName>Did not wish to treat</fullName>
                    <default>false</default>
                    <label>Did not wish to treat</label>
                </value>
                <value>
                    <fullName>Dissatisfied with handling of case</fullName>
                    <default>false</default>
                    <label>Dissatisfied with handling of case</label>
                </value>
                <value>
                    <fullName>Employees not polite</fullName>
                    <default>false</default>
                    <label>Employees not polite</label>
                </value>
                <value>
                    <fullName>Firm Withdrew from case</fullName>
                    <default>false</default>
                    <label>Firm Withdrew from case</label>
                </value>
                <value>
                    <fullName>Injuries too minor or unrelated</fullName>
                    <default>false</default>
                    <label>Injuries too minor or unrelated</label>
                </value>
                <value>
                    <fullName>Liability issues</fullName>
                    <default>false</default>
                    <label>Liability issues</label>
                </value>
                <value>
                    <fullName>Low offer</fullName>
                    <default>false</default>
                    <label>Low offer</label>
                </value>
                <value>
                    <fullName>Negative review of medical records</fullName>
                    <default>false</default>
                    <label>Negative review of medical records</label>
                </value>
                <value>
                    <fullName>No insurance or assets</fullName>
                    <default>false</default>
                    <label>No insurance or assets</label>
                </value>
                <value>
                    <fullName>Not in Ref Atty County (PIP cases)</fullName>
                    <default>false</default>
                    <label>Not in Ref Atty County (PIP cases)</label>
                </value>
                <value>
                    <fullName>Not notified of change of assistant</fullName>
                    <default>false</default>
                    <label>Not notified of change of assistant</label>
                </value>
                <value>
                    <fullName>Offer too low</fullName>
                    <default>false</default>
                    <label>Offer too low</label>
                </value>
                <value>
                    <fullName>PIP benefits exhausted (PIP cases)</fullName>
                    <default>false</default>
                    <label>PIP benefits exhausted (PIP cases)</label>
                </value>
                <value>
                    <fullName>PIP claim already filed (PIP cases)</fullName>
                    <default>false</default>
                    <label>PIP claim already filed (PIP cases)</label>
                </value>
                <value>
                    <fullName>Property damage not resolved</fullName>
                    <default>false</default>
                    <label>Property damage not resolved</label>
                </value>
                <value>
                    <fullName>Reasons unrelated to firm/case</fullName>
                    <default>false</default>
                    <label>Reasons unrelated to firm/case</label>
                </value>
                <value>
                    <fullName>Too difficult to speak to attorney</fullName>
                    <default>false</default>
                    <label>Too difficult to speak to attorney</label>
                </value>
                <value>
                    <fullName>Uncooperative client</fullName>
                    <default>false</default>
                    <label>Uncooperative client</label>
                </value>
                <value>
                    <fullName>Upset that case is being referred.</fullName>
                    <default>false</default>
                    <label>Upset that case is being referred.</label>
                </value>
                <value>
                    <fullName>Unknown</fullName>
                    <default>false</default>
                    <label>Unknown</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>litify_pm__Resolution_Date__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resolution Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>litify_pm__Resolution_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resolution Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Arbitration</fullName>
                    <default>false</default>
                    <label>Arbitration</label>
                </value>
                <value>
                    <fullName>Mediation</fullName>
                    <default>false</default>
                    <label>Mediation</label>
                </value>
                <value>
                    <fullName>Negotiation - Filed Suit</fullName>
                    <default>false</default>
                    <label>Negotiation - Filed Suit</label>
                </value>
                <value>
                    <fullName>Negotiation - Not Filed</fullName>
                    <default>false</default>
                    <label>Negotiation - Not Filed</label>
                </value>
                <value>
                    <fullName>Negotiation - Trial Date Set</fullName>
                    <default>false</default>
                    <label>Negotiation - Trial Date Set</label>
                </value>
                <value>
                    <fullName>Verdict - Plaintiff</fullName>
                    <default>false</default>
                    <label>Verdict - Plaintiff</label>
                </value>
                <value>
                    <fullName>Verdict - Defense</fullName>
                    <default>false</default>
                    <label>Verdict - Defense</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>litify_pm__Resolved_by__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resolved by</label>
        <referenceTo>User</referenceTo>
        <relationshipName>LitifyResolutionsResolvedBy</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Settlement_Verdict_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Settlement/Verdict Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Total_Damages__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Damages</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Total_Expenses__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Expenses</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Resolution</label>
    <listViews>
        <fullName>litify_pm__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>RES-{00000}</displayFormat>
        <label>Resolution Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Resolutions</pluralLabel>
    <recordTypes>
        <fullName>litify_pm__Resolution</fullName>
        <active>true</active>
        <compactLayoutAssignment>Resolution_Compact_Layout</compactLayoutAssignment>
        <label>Resolution</label>
        <picklistValues>
            <picklist>litify_pm__Payor_Type__c</picklist>
            <values>
                <fullName>Attorney</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Defendant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Insurance</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Third-Party Administrator %28TPA%29</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>litify_pm__Reason__c</picklist>
            <values>
                <fullName>Calls not being returned</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Cannot locate defendant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client Paid Correctly</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client did not treat</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client missing</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client unavailable or deceased</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Conflict of Interest</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Decided not to pursue claim</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Decided to accept prior offer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Defendant did not respond</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Did not wish to treat</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dissatisfied with handling of case</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Employees not polite</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Firm Withdrew from case</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Injuries too minor or unrelated</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Liability issues</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low offer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negative review of medical records</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>No insurance or assets</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not in Ref Atty County %28PIP cases%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not notified of change of assistant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Offer too low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>PIP benefits exhausted %28PIP cases%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>PIP claim already filed %28PIP cases%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Property damage not resolved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Reasons unrelated to firm%2Fcase</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Too difficult to speak to attorney</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Uncooperative client</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Unknown</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Upset that case is being referred%2E</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>litify_pm__Resolution_Type__c</picklist>
            <values>
                <fullName>Arbitration</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mediation</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negotiation - Filed Suit</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negotiation - Not Filed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negotiation - Trial Date Set</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Verdict - Defense</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Verdict - Plaintiff</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <excludedStandardButtons>New</excludedStandardButtons>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
