<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>150.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <groupingSortProperties/>
            <header>My Active Matters</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#54C254</indicatorMiddleColor>
            <report>unfiled$public/Active_Presuit</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Total Active</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>true</expandOthers>
            <groupingSortProperties/>
            <header>Total Active Missing Pre-suit Info</header>
            <legendPosition>Bottom</legendPosition>
            <report>Company_Dashboard_Reports_Lightning/My_Medical_Management_Compliance_1F2</report>
            <showPercentage>false</showPercentage>
            <showTotal>false</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>15.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <groupingSortProperties/>
            <header>My New Matters This Month</header>
            <indicatorBreakpoint1>5.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>10.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Case_Manager_Operations/Matters_Created_This_Month</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Total this Month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingSortProperties/>
            <header>My New Matters This Year by Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Case_Manager_Operations/Number_of_New_Matters_This_Year_By_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total by Month</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>My Aging Matters</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Case_Manager_Operations/X6_8_10_Group_Active_Cases</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>6 / 8 / 10 Months</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Status contains &quot;Demand&quot;</footer>
            <groupingSortProperties/>
            <header>My Demand Matters</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Case_Manager_Operations/Matters_in_Demand_Status</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total in Demand</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*All statuses that contain &quot;Demand&quot;</footer>
            <groupingSortProperties/>
            <header>% Active Cases in Demand</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Case_Manager_Operations/percent_in_demand</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>Demand vs Other</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>NPS__c.score__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>5 being highest</footer>
            <groupingSortProperties/>
            <header>Last 30 Days Ask Nicely</header>
            <indicatorBreakpoint1>3.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>4.5</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average Survey Score</metricLabel>
            <report>Case_Manager_Operations/Last_30_Days_Ask_Nicely</report>
            <showRange>false</showRange>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingSortProperties/>
            <header>My Turned Down Matters This Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Case_Manager_Operations/My_Turned_Down_Matters_This_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>TD&apos;s By Status</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingSortProperties/>
            <header>My Turn Down Matters This Year</header>
            <legendPosition>Bottom</legendPosition>
            <report>Case_Manager_Operations/This_Year_Turn_Downs_Matters_by_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>TD&apos;s by Month</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>jdiaz@forthepeople.com</runningUser>
    <textColor>#000000</textColor>
    <title>Case Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
