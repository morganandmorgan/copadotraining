import { LightningElement, track, api } from 'lwc';
import getRecords from '@salesforce/apex/QueryRecord.Query';

export default class QueryRecordField extends LightningElement
{
    @track data = [];
    @track error;

    @track results = [];
    @track term = '';
    @track show = false;
    @track loading = false;
    @track empty = false;

    @api selected = '';


    async handleOnSelect(evt)
    {
        var index = evt.currentTarget.dataset.value;
        var matter = this.results[index];

        this.selected = matter.Id;
        
        this.show = false;
        this.loading = false;
        this.empty = false;

        this.term = matter.ReferenceNumber__c + ' - ' + matter.litify_pm__Display_Name__c;
        this.template.querySelector('lightning-input').value = this.term
    }

    async handleOnEnter(evt)
    {
        if (evt.keyCode == 13 && evt.target.value.length > 0)
        {
            console.log("User pressed Enter");

            this.show = false;
            this.loading = true;

            this.term = evt.target.value.toLowerCase();
            console.log("Searching with '" + this.term + "'");

            this.data = await this.pullData(this.term);

            console.log("Found " + this.data.length + " record(s)");

            this.results = await this.siftData(this.data, this.term);
            this.results = await this.sortData(this.results, this.term);
            
            this.results = this.results.slice(0, 100);

            this.empty = this.results.length < 1;
            this.loading = false;
            this.show = true;
        }
    }

    async handleOnChange(evt)
    {
        console.log("User modified input field value");

        if (evt.target.value.length < 3) {
            console.log("Term is less than 3 characters long. Hiding listbox...");
            this.show = false;
            this.loading = false;

            return;
        }

        if (evt.target.value.length === 3 && evt.target.value !== this.term) {

            this.show = false;
            this.loading = true;

            
            this.term = evt.target.value.toLowerCase();
            console.log("Searching with '" + this.term + "'");

            this.data = await this.pullData(this.term);
            console.log("Found " + this.data.length + " record(s)");

        }
        else
            this.term = evt.target.value.toLowerCase();
        
        this.results = await this.siftData(this.data, this.term);
        this.results = await this.sortData(this.results, this.term);
        this.results = this.results.slice(0, 100);

        this.loading = false;
        this.show = true;
        this.empty = this.results.length < 1;
    }

    async pullData(term)
    {
        try {
            const result = await getRecords({ term: term });
            
            this.error = undefined;
            return result;
        }
        catch (error) {
            this.error = error;
            return undefined;
        }
    }

    async sortData(data, term)
    {
        return [...data].sort((a, b) => {
            var itemA = (a.ReferenceNumber__c + ' - ' + a.litify_pm__Display_Name__c).toLowerCase();
            var itemB = (b.ReferenceNumber__c + ' - ' + b.litify_pm__Display_Name__c).toLowerCase();

            return itemA.indexOf(term) <= itemB.indexOf(term) ? -1 : 1;
        });
    }

    async siftData(data, term)
    {
        return data.filter((item) => {
            var itemText = (item.ReferenceNumber__c + ' - ' + item.litify_pm__Display_Name__c).toLowerCase();
            return itemText.indexOf(term) !== -1;
        });
    }
}