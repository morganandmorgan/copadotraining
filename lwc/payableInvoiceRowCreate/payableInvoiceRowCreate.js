/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getMatterDetails from '@salesforce/apex/createPayableInvoiceController.getMatterDetails'


export default class PayableInvoiceRowCreate extends LightningElement {

    @api index = 0
    @api lineNumber
    @api wrapperObject = {}
    @api glaIdDefault
    @api matterIdDefault
    @api expenseTypeIdDefault
    @api lineNumberDefault
    @api dim1IdDefault
    @api dim2IdDefault
    @api dim3IdDefault
    @api dim4IdDefault
    @api lineDescDefault
    @track updatedInternally = false;

    @track updatedWrapper = {}

    // @api updateRows(Wrapper) {
    //     // const inputFields = this.template.querySelectorAll(
    //     //     'lightning-input-field'
    //     // );
    //     // if (inputFields) {
    //     //     inputFields.forEach(field => {
    //     //         field.reset();
    //     //     });
    //     // }
        
    //     this.updatedInternally = true
    //     console.log('***Wrapper', Wrapper)
    //     this.wrapperObject = Wrapper

    //     //set the value that is displayed in the UI
    //     this.matterIdDefault = Wrapper.matterId
    //     this.glaIdDefault = Wrapper.glaId
    //     this.expenseTypeIdDefault = Wrapper.expenseTypeId
    //     this.dim1IdDefault = Wrapper.dim1Id
    //     this.dim2IdDefault = Wrapper.dim2Id
    //     this.dim3IdDefault = Wrapper.dim3Id
    //     this.dim4IdDefault = Wrapper.dim4Id
    //     this.lineDescDefault = Wrapper.lineDescription
    //     console.log('***matterIdDefault', this.matterIdDefault)
    //     console.log('***dim1IdDefault', this.dim1IdDefault)
    //     console.log('***dim2IdDefault', this.dim2IdDefault)

    //     //set the value that is stored in the collection in the line component
    //     this.updatedWrapper.matterId = this.matterIdDefault
    //     this.updatedWrapper.dim1Id = this.dim1IdDefault
    //     this.updatedWrapper.dim2Id = this.dim2IdDefault
    //     this.updatedWrapper.dim3Id = this.dim3IdDefault
    //     this.updatedWrapper.dim4Id = this.dim4IdDefault
    //     this.updatedWrapper.lineDescription = this.lineDescDefault
    //     console.log('***updatedWrapper.matterId', this.updatedWrapper.matterId)
    //     console.log('***updatedWrapper.dim1Id', this.updatedWrapper.dim1Id)
    //     console.log('***updatedWrapper.dim2Id', this.updatedWrapper.dim2Id)

    // }

    //@track expenseTypeId;
    @api
    setDefaultMatter(headerMatterId) {
        if (this.matterIdDefault === null || this.matterIdDefault === '' || this.matterIdDefault === undefined) {
            //set the value that is displayed in the UI
            this.matterIdDefault = headerMatterId
            //set the value that is stored in the collection in the line component
            this.updatedWrapper.matterId = headerMatterId
            this.updatedWrapper.dim1Id = this.dim1IdDefault
            this.updatedWrapper.dim2Id = this.dim2IdDefault
            this.updatedWrapper.dim3Id = this.dim3IdDefault
            this.updatedWrapper.dim4Id = this.dim4IdDefault
            this.updatedWrapper.lineDescription = this.lineDescDefault

            //dispatch event to update collection stored in parent component
            this.dispatchColumnDataChange('matterId')
            console.log('setDefaultMatter - updatedWrapper: ' + this.updatedWrapper)
            console.log('setDefaultMatter - matterIdDefault = ' + headerMatterId)
        }
    }
    @api
    setDefaultExpenseType(accountDefaultExpType) {
        if (this.expenseTypeIdDefault === null || this.expenseTypeIdDefault === '' || this.expenseTypeIdDefault === undefined) {
            this.expenseTypeIdDefault = accountDefaultExpType
            this.updatedWrapper.expenseTypeId = accountDefaultExpType
            this.dispatchColumnDataChange('expenseTypeId')
            console.log('setDefaultMatter - expenseTypeIdDefault = ' + accountDefaultExpType)
        }

    }
    @api
    setLineNumber(accountDefaultLineNumber) {
        if (this.lineNumberDefault === null || this.lineNumberDefault === '' || this.lineNumberDefault === undefined) {
            this.lineNumberDefault = this.index += 1;
            //this.updatedWrapper.expenseTypeId = accountDefaultLineNumber
            // this.dispatchColumnDataChange('expenseTypeId')
            console.log('setDefaultLineItem- lineNumberDefault = ' + accountDefaultLineNumber)
        }

    }
    @api
    setDefaultGLA(accountDefaultGLA) {
        if (this.glaIdDefault === null || this.glaIdDefault === '' || this.glaIdDefault === undefined) {
            this.glaIdDefault = accountDefaultGLA
            this.updatedWrapper.glaId = accountDefaultGLA
            this.dispatchColumnDataChange('glaId')
            console.log('setDefaultMatter - glaIdDefault = ' + accountDefaultGLA)
        }

    }
    @api
    setDefaultDim1(matterDefaultDim1) {
        if (this.dim1IdDefault === null || this.dim1IdDefault === '' || this.dim1IdDefault === undefined) {
            this.dim1IdDefault = matterDefaultDim1
            this.updatedWrapper.dim1Id = matterDefaultDim1
            this.dispatchColumnDataChange('dim1Id')
            console.log('setDefaultDim1 - dim1IdDefault = ' + matterDefaultDim1)
        }

    }
    @api
    setDefaultDim2(matterDefaultDim2) {
        if (this.dim2IdDefault === null || this.dim2IdDefault === '' || this.dim2IdDefault === undefined) {
            this.dim2IdDefault = matterDefaultDim2
            this.updatedWrapper.dim2Id = matterDefaultDim2
            this.dispatchColumnDataChange('dim2Id')
            console.log('setDefaultDim2 - dim2IdDefault = ' + matterDefaultDim2)
        }

    }
    @api
    setDefaultDim3(matterDefaultDim3) {
        if (this.dim3IdDefault === null || this.dim3IdDefault === '' || this.dim3IdDefault === undefined) {
            this.dim3IdDefault = matterDefaultDim3
            this.updatedWrapper.dim3Id = matterDefaultDim3
            this.dispatchColumnDataChange('dim3Id')
            console.log('setDefaultDim3 - dim3IdDefault = ' + matterDefaultDim3)
        }

    }
    @api
    setDefaultDim4(matterDefaultDim4) {
        if (this.dim4IdDefault === null || this.dim4IdDefault === '' || this.dim4IdDefault === undefined) {
            this.dim4IdDefault = matterDefaultDim4
            this.updatedWrapper.dim4Id = matterDefaultDim4
            this.dispatchColumnDataChange('dim4Id')
            console.log('setDefaultDim4 - dim4IdDefault = ' + matterDefaultDim4)
        }

    }

    disconnectedCallback() {
        console.log('***delete', this.wrapperObject)
    }

   
    handleGlaSelect(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.glaId = event.target.value;
        }
        this.dispatchColumnDataChange('glaId')
    }
    handleMatterSelect(event) {

        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined && event.target.value !== '') {
            this.updatedWrapper.matterId = event.target.value;
        }
        console.log('ID: ' + this.updatedWrapper.matterId);
        this.updatedInternally = true;
        //default from the matter
        getMatterDetails({ matterId: this.updatedWrapper.matterId })
            .then(result => {
                this.dim1IdDefault = result.dim1;
                this.dim2IdDefault = result.dim2;
                this.dim3IdDefault = result.dim3;
                this.dim4IdDefault = result.dim4;

                this.setDefaultMatter(this.updatedWrapper.matterId)

                if (result.Error !== undefined && result.Error !== null && result.Error !== 'undefined' && result.Error !== '') {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error:',
                            message: result.Error,
                            variant: 'error',
                            mode: 'sticky'
                        })


                    );
                    if (!result.Error.includes('has been financially closed')) {
                        console.log('DISABLED');
                        this.dispatchDisableSave(false);
                    }
                    else
                        this.dispatchDisableSave(true);
                }
                else {
                    this.dispatchDisableSave(false);
                }



            })
            .catch(error => {
                this.error = error;
                console.log('this.error = ' + this.error)
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: this.error.message,
                        variant: 'error'
                    })
                );
            });
        this.dispatchColumnDataChange('matterId')
    }
    handleExpenseTypeSelect(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.expenseTypeId = event.target.value;
        }
        this.dispatchColumnDataChange('expenseTypeId')
    }
    handleAmountChange(event) {
        this.updatedWrapper.lineAmount = event.target.value;
        this.dispatchColumnDataChange('lineAmount')
    }
    handleDim1Select(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.dim1Id = event.target.value;
        }
        this.dispatchColumnDataChange('dim1Id')
    }
    handleDim2Select(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.dim2Id = event.target.value;
        }
        this.dispatchColumnDataChange('dim2Id')
    }
    handleDim3Select(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.dim3Id = event.target.value;
        }
        this.dispatchColumnDataChange('dim3Id')
    }
    handleDim4Select(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.dim4Id = event.target.value;
        }
        this.dispatchColumnDataChange('dim4Id')
    }
    handleLineDescSelect(event) {
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== undefined) {
            this.updatedWrapper.lineDescription = event.target.value;
        }
        this.dispatchColumnDataChange('lineDescription')
    }

    handleRowDelete(event) {
        this.dispatchRowDelete(event.detail);
    }

    //DISPATCHER METHODS
    dispatchRowDelete() {
        let rowDeleteEvent = new CustomEvent('rowdelete', {
            detail: this.index,
            bubbles: true,
            cancelable: true
        });
        this.dispatchEvent(rowDeleteEvent);
    }
    dispatchColumnDataChange(typeOfChange) {

        let payload = {
            wrapperRow: { wrapperData: this.updatedWrapper, index: this.index, columnDataChanged: typeOfChange }
        };
        //create a new event to dispatch (note the name must be all lowercase with no special characters)
        let columnDataChangeEvent = new CustomEvent('columndatachange', {
            detail: payload,
            bubbles: true,
            cancelable: true
        });
        this.dispatchEvent(columnDataChangeEvent);
    }


    dispatchDisableSave(disableSave) {
        let payload = {
            disableSave
        };
        //create a new event to dispatch (note the name must be all lowercase with no special characters)
        let disableSaveButtonEvent = new CustomEvent('disablesave', {
            detail: payload,
            bubbles: true,
            cancelable: true
        });
        this.dispatchEvent(disableSaveButtonEvent);
    }

}