/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getMatterDetails from '@salesforce/apex/depositCreateController.getMatterDetails';

export default class DepositCreateRow extends LightningElement {
    @api index = 0
    @api disableLimitsChild = false
    @api disableUnallocatedChild = false
    @track amountDefault
    @track hardCostDefault
    @track softCostDefault
    @track allocatedFeeDefault
    @track unallocatedCostDefault
    @track allocatedFee
    @track unallocatedCost
    @api wrapperObject = {}

    @track updatedWrapper = {}

    @track defaultlimit = true

    @track totalCostLimit;
    limitTotal = 0;
    
    @api
    resetLimits(){
        this.hardCostDefault = null
        this.softCostDefault = null
        this.totalCostLimit = null
        this.limitTotal = null
        console.log('resetLimits - hardCostDefault = ' + this.hardCostDefault)
        console.log('resetLimits - softCostDefault = ' + this.softCostDefault)
    }
    @api
    resetUnallocated(){
        this.allocatedFeeDefault = null
        this.unallocatedCostDefault = null
        this.limitTotal = null
        console.log('resetLimits - hardCostDefault = ' + this.hardCostDefault)
        console.log('resetLimits - softCostDefault = ' + this.softCostDefault)
    }
    handleFormReset() {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
    @api
    resetRow(){
        this.handleFormReset()
        this.resetLimits()
        this.resetUnallocated()
        this.amountDefault = 0.00
    }
    calculateTotalLimits(){
        let hcLimit = this.updatedWrapper.hardCostLimit !== '' && this.updatedWrapper.hardCostLimit !== null && this.updatedWrapper.hardCostLimit !== undefined ? this.updatedWrapper.hardCostLimit : 0.00
        let scLimit = this.updatedWrapper.softCostLimit !== '' && this.updatedWrapper.softCostLimit !== null && this.updatedWrapper.softCostLimit !== undefined ? this.updatedWrapper.softCostLimit : 0.00
        let afValue = this.updatedWrapper.allocatedFee !== '' && this.updatedWrapper.allocatedFee !== null && this.updatedWrapper.allocatedFee !== undefined ? this.updatedWrapper.allocatedFee : 0.00
        let ucValue = this.updatedWrapper.unallocatedCost !== '' && this.updatedWrapper.unallocatedCost !== null && this.updatedWrapper.unallocatedCost !== undefined ? this.updatedWrapper.unallocatedCost : 0.00
        
        this.totalCostLimit = parseFloat(hcLimit) + parseFloat(scLimit)
        this.limitTotal = parseFloat(hcLimit) + parseFloat(scLimit) + parseFloat(afValue) + parseFloat(ucValue)

        //dispatch a warning if the amount is less than the limits
        if(this.limitTotal > this.updatedWrapper.depositAmount){
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Warning:',
                    message: 'The allocation limits exceed the amount of the deposit',
                    variant: 'warning'
                })
            );

            //disable the save button:
            this.dispatchDisableSave(true)
        }
        else{
            //enable save
            this.dispatchDisableSave(false)
        }

    }
    dispatchDisableSave(disableSave){
        let payload = {
            disableSave
        };
        //create a new event to dispatch (note the name must be all lowercase with no special characters)
        let disableSaveButtonEvent = new CustomEvent('disablesave', {
            detail: payload,
            bubbles: true,
            cancelable: true
        });
        this.dispatchEvent(disableSaveButtonEvent);
    }
    
    handleMatterSelect( event ) { 
        //console.log('handleMatterSelect - event.detail = '+JSON.stringify(event.detail));
        console.log('handleMatterSelect - event.target.value = '+event.target.value);

        let selectedId = ''
        if(event.target.value !== null && event.target.value !== 'null' && event.target.value !== 'undefined'){
            selectedId = event.target.value;
        } 
        this.updatedWrapper.matterId = selectedId;

        //validate matter details (company is a matche etc)
        //default from the matter
        getMatterDetails({matterId : selectedId})
        .then(result => {
            if(result.Error !== undefined && result.Error !== null && result.Error !== 'undefined' && result.Error !== ''){
                console.log('Error : '+ JSON.stringify(result.Error));
                this.disableSave = true
                this.dispatchDisableSave(this.disableSave);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: result.Error,
                        variant: 'error',
                        mode: 'sticky'
                    })
                );
            }
            else{
                this.disableSave = false
                this.dispatchDisableSave(this.disableSave);
            }
        })
        .catch(error => {
            this.error = error;
            console.log('this.error = '+ this.error)
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error:',
                    message: this.error.body.message,
                    variant: 'error'
                })
            );
        });

        this.dispatchColumnDataChange('matterId')
    }
    handleAmountChange(event) {
        this.updatedWrapper.depositAmount = event.target.value;
        this.dispatchColumnDataChange('depositAmount')
    }
    handleCheckNumberChange(event) {
        this.updatedWrapper.checkNumber = event.target.value;
        this.dispatchColumnDataChange('checkNumber')
    }
    handleHCLimitChange(event) {
        this.hardCostDefault = event.target.value;
        
        this.updatedWrapper.hardCostLimit = event.target.value;
        this.dispatchColumnDataChange('hardCostLimit')
        this.calculateTotalLimits()
    }
    handleSCLimitChange(event) {
        this.softCostDefault = event.target.value;

        this.updatedWrapper.softCostLimit = event.target.value;
        this.dispatchColumnDataChange('softCostLimit')
        this.calculateTotalLimits()
    }
    handleAllocatedFeeChange(event){
        this.allocatedFeeDefault = event.target.value;
        
        this.updatedWrapper.allocatedFee = event.target.value;
        this.dispatchColumnDataChange('allocatedFee')
        this.calculateTotalLimits()
    }
    handleUnallocatedCostChange(event){
        this.unallocatedCostDefault = event.target.value;
        
        this.updatedWrapper.unallocatedCost = event.target.value;
        this.dispatchColumnDataChange('unallocatedCost')
        this.calculateTotalLimits()
    }
    dispatchColumnDataChange(typeOfChange){

        let payload = {
            wrapperRow: { wrapperData: this.updatedWrapper, index: this.index, columnDataChanged : typeOfChange}
        };
        //create a new event to dispatch (note the name must be all lowercase with no special characters)
        let columnDataChangeEvent = new CustomEvent('columndatachange', {
            detail: payload,
            bubbles: true,
            cancelable: true
        });
        this.dispatchEvent(columnDataChangeEvent);
    }

    handleRowDelete(event) {
        this.dispatchRowDelete(event.detail);
    }
    dispatchRowDelete() {
        let rowDeleteEvent = new CustomEvent('rowdelete', {
            detail: this.index,
            bubbles: true,
            cancelable: true
        });
        this.dispatchEvent(rowDeleteEvent);
    }
}