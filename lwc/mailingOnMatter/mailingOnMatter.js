import { LightningElement, api, track, wire} from 'lwc';
import getDocData from '@salesforce/apex/MailingOnMatterController.getDocData';
import getRoleData from '@salesforce/apex/MailingOnMatterController.getRoleData';
import sendMail from '@salesforce/apex/MailingOnMatterController.sendMail';
import getMailTypeOptions from '@salesforce/apex/MailingOnMatterController.getMailTypeOptions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
 /* eslint-disable no-console */
 /* eslint-disable no-alert */

export default class MailingOnMatter extends LightningElement {
    @api recordId;
    
    @track matterId;
    @track resultMessage;

    @track onDocuments = 1;
    @track onRoles = 0;
    @track docSortBy = 'lastModDate';
    @track docSortDir = 'asc';

    connectedCallback() {
        if (this.recordId == null) {
            this.matterId = 'a1n8A000000TAurQAG'; //CP
            //this.matterId = 'a1n8A000000TC67'; //Litify
        } else {
            this.matterId = this.recordId;
        }
        this.resultMessage = 'matterId :' + JSON.stringify(this.matterId);
    } //connectedCallback

    /* Document Selection */

    @track docColumns = [
        { label: 'Name', fieldName: 'name', sortable : true, type : 'text' },
        { label: 'Category', fieldName: 'category', sortable : true, type : 'text' },
        { label: 'Sub Category', fieldName: 'subCategory', sortable : true, type : 'text' },
        { label: 'To', fieldName: 'docTo', sortable : true, type : 'text'},
        { label: 'From', fieldName: 'docFrom', sortable : true, type : 'text' },
        { label: 'Created Date', fieldName: 'createdDate', sortable : true, type : 'text' },
        { label: 'Created By', fieldName: 'createdBy', sortable : true, type : 'text'},
        { label: 'Last Mod Dat', fieldName: 'lastModDate', sortable : true, type : 'text' },
        { label: 'Last Mod By', fieldName: 'lastModBy', sortable : true, type : 'text' }
    ];

    @track docData;
    @track allDocData;
    @track preSelectedDocs = [];
    @track selectedDocs = [];

    @wire(getDocData, {matterId: '$matterId'})
    wiredDocs(results) {
        if (results.data) {
            this.docData = results.data;
            this.allDocData = results.data;
            this.sortDocs(this.docSortBy, this.docSortDir);
        }
    } //wiredDocs


    docSort(event) {
        let fieldName = event.detail.fieldName;
        let sortDirection = event.detail.sortDirection;
        this.docSortBy = fieldName;
        this.docSortDir = sortDirection;
        this.sortDocs(fieldName, sortDirection);
     } //docSort

     sortDocs(fieldName, sortDirection) {
        let sortResult = Object.assign([], this.docData);
        this.docData = sortResult.sort(function(a,b) {

            if ( fieldName.includes("Date") ) {
                if (sortDirection == 'asc') return new Date(b[fieldName]) - new Date(a[fieldName])
                else return new Date(a[fieldName]) - new Date(b[fieldName]);
            } else {
                if (a[fieldName] < b[fieldName])
                    return sortDirection === 'asc' ? -1 : 1;
                else if (a[fieldName] > b[fieldName])
                    return sortDirection === 'asc' ? 1 : -1;
                else return 0;
            }

        })
      } //sortDocs


    @track docSearch;
    searchChange(event) {
        let filter = event.target.value;

        let regex = new RegExp(filter, "i");
        // filter checks each row, constructs new array where function returns true
        let filteredData = this.allDocData.filter(row=>regex.test(row.name) );

        this.docData = [...filteredData];
    } //searchChange


    nextClick() {
        //we can't get this once the template is hidden
        let docTable = this.getElementByName('lightning-datatable','documents');
        this.selectedDocs = docTable.getSelectedRows();

        if (this.selectedDocs.length != 1) {
            if (this.selectedDocs.length == 0) {
                this.displayMessage('Document Validation', 'You must select a document to continue.','warning');
            } else {
                this.displayMessage('Document Validation', 'You can only select one document.','warning');
            }
        } else {
            this.onDocuments = 0;
            this.onRoles = 1;
        }
    } //nextClick

    /* Role Selection */

    @track roleColumns = [
        { label: 'Role', fieldName: 'role', editable: false },
        { label: 'Name', fieldName: 'name', editable: true },
        { label: 'ATTN', fieldName: 'attn', editable: true },
        { label: 'Shipping Street', fieldName: 'shippingStreet', editable: true },
        { label: 'City', fieldName: 'shippingCity', editable: true },
        { label: 'State', fieldName: 'shippingState', editable: true},
        { label: 'Postal Code', fieldName: 'shippingPostalCode', editable: true},
        { label: 'Return Envelope', fieldName: 'returnEnvelope', editable: true, type: 'boolean'},
    ];

    @track roleData; // Roles = Recipients
    @track draftRoles = [];
    @track savedRoles = [];

    @wire(getRoleData, {matterId: '$matterId'})
    wiredRoles(result) {
        if (result.data) {
            this.roleData = new Array(); //we have to build this from scratch so the array is not fixed length, as returned from apex
            for (const role of result.data) {
                this.roleData.push(role);
            }
        }
    } //wiredRoles

    previousClick() {
        this.onDocuments = 1;
        this.onRoles = 0;
        this.preSelectedDocs = [this.selectedDocs[0].id];
    } //previousClick

    @track newRecipientIndex = 1; //this is so we can generate unique id's for new recipients
    addRecipientClick() {
        let newRecipientId = 'newId' + String(this.newRecipientIndex);
        let newRecipientName = 'new recipient ' + String(this.newRecipientIndex);
        this.newRecipientIndex++;
        //this has to be done this way for the component to refresh
        this.roleData = [...this.roleData, {id:newRecipientId, name:newRecipientName, attn:'', shippingStreet:'', shippingCity:'', shippingState:'', shippingPostalCode:'', role:''}];
    } //addRecipientClick


    getElementByName(type, name) {

        let elements = this.template.querySelectorAll(type);

        for (const element of elements) {
            if (element.name == name) {
                return element
            }
        }

    } //getElementByName

    @track roleEdits = 0;

    handleSave(event) {
        this.savedRoles = event.detail.draftValues;
        this.displayMessage('Recipient Edits', 'Saved.','success');
        this.roleEdits = 0;
    }

    roleChange(event) {
        this.roleEdits = 1;
    }

    @track mailTypeOptions;
    @track mailType = 'First Class';
    @wire(getMailTypeOptions)
    wiredMailTypeOptions(results) {
        if (results.data) {
            this.mailTypeOptions = results.data;
        }
    } //wiredMailTypeOptions


    applyRoleEdits (selectedRoles) {
        //this hack will let us edit any fields that Apex made read-only
        selectedRoles = JSON.parse(JSON.stringify(selectedRoles));

        //we need to apply the edits to the selectedRoles, because the LWC datatable behaves weird
        for (const roleEdit of this.savedRoles) {
            let index = null;
            //find the role this edit applies to
            for (let i=0; i < selectedRoles.length; i++) {
                if (selectedRoles[i].id == roleEdit.id) {
                    index = i;
                    break;
                }
            } //for selectedRoles

            //if we found it, apply the edit(s)
            if (index != null) {
                if (roleEdit.name) selectedRoles[index].name = roleEdit.name;
                if (roleEdit.attn) selectedRoles[index].attn = roleEdit.attn;
                if (roleEdit.shippingStreet) selectedRoles[index].shippingStreet = roleEdit.shippingStreet;
                if (roleEdit.shippingCity) selectedRoles[index].shippingCity = roleEdit.shippingCity;
                if (roleEdit.shippingState) selectedRoles[index].shippingState = roleEdit.shippingState;
                if (roleEdit.shippingPostalCode) selectedRoles[index].shippingPostalCode = roleEdit.shippingPostalCode;
                if (roleEdit.returnEnvelope) selectedRoles[index].returnEnvelope = roleEdit.returnEnvelope;
            }
        } //for roleEdit

        return selectedRoles;

    } //applyRoleEdits


    @track spinCount = 0;
    completeClick() {

        if (this.roleEdits) {
            this.displayMessage('Recipient Validation', 'You must save your changes before continuing.','warning');
            return;
        }

        let roleTable = this.getElementByName('lightning-datatable','roles');
        let selectedRoles = roleTable.getSelectedRows();

        if (selectedRoles.length == 0) {
            this.displayMessage('Recipient Validation', 'You must provide at least one recipient.','warning');
            return;
        }

        selectedRoles = this.applyRoleEdits(selectedRoles);

        for (const role of selectedRoles) {
            if (role.name == '' || role.shippingStreet == '' || role.shippingCity == '' || role.shippingState == '' || role.shippingPostalCode == '') {
                    this.displayMessage('Recipient Validation', 'Selected recipients must have complete addresses.','warning');
                    return;
            } //if blank address
        } //for roles

        let mte = this.getElementByName('lightning-combobox','mailType');

        this.spinCount++;
        sendMail({matterId: this.matterId, jsDocuments : JSON.stringify(this.selectedDocs), jsRoles : JSON.stringify(selectedRoles), mailType : mte.value})
        .then(result => {
            this.sendData = result;
            //alert('sendMail success.');
            this.displayMessage('Mail Request Sent','Successful','success');
            this.resultMessage = this.sendData[0];

            //reset the form
            this.onDocuments = 1;
            this.onRoles = 0;
            this.preSelectedDocs = [];
            this.docSearch = ''; //clear the search
            this.docData = [...this.allDocData]; //display everything again

            this.docSortBy = 'lastModDate';
            this.docSortDir = 'asc';
            this.sortDocs(this.docSortBy, this.docSortDir);
    
            this.spinCount--;
        })
        .catch(error => {
            this.error = error;
            //alert('sendMail error: "' + JSON.stringify(error) + '"');
            this.displayMessage('Mail Request Error', JSON.stringify(error),'error');
            this.spinCount--;
        });

    } //completeClick


    displayMessage(title,message,variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }

}