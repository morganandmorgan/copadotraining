import { LightningElement, api, track, wire } from 'lwc';
import getMatterData from '@salesforce/apex/QuickHipaaController.getMatterData';
import sendMail from '@salesforce/apex/QuickHipaaController.sendMail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class QuickHipaa extends LightningElement {

    @api recordId;
    
    @track matterId;
    @track resultMessage;

    connectedCallback() {
        if (this.recordId == null) {
            //this.matterId = 'a1n8A000000TAurQAG'; //CP
            this.matterId = 'a1n1J000001gf9dQAA'; //Litify
            //this.matterId = '123notFoundABC';
        } else {
            this.matterId = this.recordId;
        }
        this.resultMessage = 'matterId :' + JSON.stringify(this.matterId);
    } //connectedCallback

    @track matterData = {'name':''};

    @wire(getMatterData, {matterId: '$matterId'})
    wiredMatter({ error, data}) {
        if (data) {
            this.matterData = data;
            //alert('wiredMatter data: ' + JSON.stringify(this.matterData) );
        } else if (error) {
            alert('wiredMatter error: ' + JSON.stringify(error) );
        }
    } //wiredMatter

    getElementByName(type, name) {
        let elements = this.template.querySelectorAll(type);

        for (const element of elements) {
            if (element.name == name) {
                return element
            }
        }
    } //getElementByName

    @track spinCount = 0;
    sendClick() {
        let matterData = {};
        matterData.id = this.matterId;

        let e = this.getElementByName('lightning-input','name');
        matterData.name = e.value;

        e = this.getElementByName('lightning-input','street');
        matterData.street = e.value;

        e = this.getElementByName('lightning-input','city');
        matterData.city = e.value;

        e = this.getElementByName('lightning-input','state');
        matterData.state = e.value;

        e = this.getElementByName('lightning-input','postalCode');
        matterData.postalCode = e.value;

        if (matterData.name == '' || matterData.street == '' || matterData.city == '' || matterData.state == '' || matterData.postalCode == '') {
            this.displayMessage('Recipient Validation', 'Recipients must have complete address.','warning');
            return;
        } //if blank address

        this.spinCount++;
        sendMail({jsMatterData : JSON.stringify(matterData)})
        .then(result => {

            if (result == 'Success') {
                this.displayMessage('HIPAA Mail Request Sent', result,'success');
                this.done++;
            } else {
                this.displayMessage('HIPAA Mail NOT Sent', result, 'warning');
            }
   
            this.spinCount--;

        })
        .catch(error => {
            this.error = error;
            //alert('sendMail error: "' + JSON.stringify(error) + '"');
            this.displayMessage('HIPAA Mail Request Error', JSON.stringify(error),'error');
            this.spinCount--;
        });

    } //sendClick

    @track done = 0;

    closeClick() {
        const closeQA = new CustomEvent('close');
        this.dispatchEvent(closeQA);
    } //closeClick


    displayMessage(title,message,variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }    

} //class