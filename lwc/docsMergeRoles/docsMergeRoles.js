import { LightningElement, api, track, wire } from 'lwc';
import getTemplates from '@salesforce/apex/DocsMergeRolesController.getTemplates';
import getRoles from '@salesforce/apex/DocsMergeRolesController.getRoles';
import getTeamMembers from '@salesforce/apex/DocsMergeRolesController.getTeamMembers';
import doMerge from '@salesforce/apex/DocsMergeRolesController.doMerge';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
 /* eslint-disable no-console */
 /* eslint-disable no-alert */

const roleColumns = [
    { label: 'Name', fieldName: 'Name__c' },
    { label: 'Category', fieldName: 'litify_pm__Role__c' },
    { label: 'Sub Category', fieldName: 'litify_pm__Subtype__c' },
    { label: 'Additional Traits', fieldName: 'litify_pm__Additional_Trait__c' }
];

export default class DocsMergeRoles extends LightningElement {
    @api recordId;
    
    @track matterId;

    connectedCallback() {
        if (this.recordId == null) {
            this.matterId = 'a1n2g0000000vFfAAI';
        } else {
            this.matterId = this.recordId;
        }
        this.resultMessage = 'matterId :' + JSON.stringify(this.matterId);
    } //connectedCallback

    /* Template Choice */

    @track templateOptions;
    @track templateChoice;

    @wire(getTemplates)
    wiredTemplates(result) {
        if (result.data) {
            this.templateOptions = result.data;
        }
    } //wiredTemplates

    templateChange(event) {
        this.templateChoice = event.detail.value;
    }

    /* Roles Choice(s) */

    @track roleColumns = roleColumns;
    @track roleData;
    @track selectedRoles = [];
    @track error;

    @wire(getRoles, {matterId: '$matterId'})
    wiredRoles(result) {
        if (result.data) {
            this.roleData = result.data;
        }
    } //wiredRoles


    /* Team Member Choice */

    @track teamMemberOptions;
    @track teamMemberChoice;

    @wire(getTeamMembers, {matterId: '$matterId'})
    wiredTeamMembers(result) {
        if (result.data) {
            this.teamMemberOptions = result.data;
        }
    } //wiredTeamMembers

    teamMemberChange(event) {
        this.teamMemberChoice = event.detail.value;
    }

    /* Merge Button */

    @track mergeData;
    @track resultMessage;

    @track spinCount = 0;
    mergeClick() {

        //validation
        let allValid = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        if (!allValid) {
            this.mergeMessage('Invaid Input','Please select missing information.','error');
            return;
        }

        let selectedRoles = this.template.querySelector('lightning-datatable').getSelectedRows();
        if (selectedRoles.length == 0) {
            this.mergeMessage('Invaid Input','Please select at least one role.','error');
            return;
        }

        //pre-processing
        this.spinCount++;

        let roleIds = [];
        for (let i = 0; i < selectedRoles.length; i++){
            roleIds.push(selectedRoles[i].Id);
        }

        let sourceIds = {
            litify_docs__Template__c:this.templateChoice,
            litify_pm__Matter__c:this.matterId,
            litify_pm__Matter_Team_Member__c:this.teamMemberChoice
        };
          
        //merge       
        doMerge({sourceIdMap : JSON.stringify(sourceIds), roleIdSet : JSON.stringify(roleIds)})
        .then(result => {
            this.mergeData = result;
            //alert('doMerge success.');
            this.mergeMessage('Merge Complete','Successful','success');
            this.resultMessage = this.mergeData[0];
            this.spinCount--;
        })
        .catch(error => {
            this.error = error;
            //alert('doMerge error: "' + JSON.stringify(error) + '"');
            this.mergeMessage('Merge Error', JSON.stringify(error),'error');
        });

    } //mergeClick


    mergeMessage(title,message,variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }

}