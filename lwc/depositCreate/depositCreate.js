/* eslint-disable no-useless-concat */
/* eslint-disable no-unused-vars */
/* eslint-disable radix */
/* eslint-disable no-console */

//Import standard lightning functions
import { LightningElement, track, wire, api } from 'lwc'
import { getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//Import Apex Methods we'll need:
import addDepositRow from '@salesforce/apex/depositCreateController.addDepositRow'
import getRecordTypeOptions from '@salesforce/apex/depositCreateController.getRecordTypeOptions';
import getBankAccountDefaults from '@salesforce/apex/depositCreateController.getBankAccountDefaults';
import saveDeposits from '@salesforce/apex/depositCreateController.saveDeposits';
import getBankAccountDetails from '@salesforce/apex/depositCreateController.getBankAccountDetails';
// import checkEligibleExpenseTotal from '@salesforce/apex/depositCreateController.checkEligibleExpenseTotal';
// import checkMatterFinancialStatus from '@salesforce/apex/depositCreateController.checkMatterFinancialStatus';



//Constants
const FIELDS = [
    'c2g__codaBankAccount__c.Id',
    'c2g__codaBankAccount__c.Name'
];


//TODO: Update component search for matter to be based on "matter record name"

let i = 0
// eslint-disable-next-line no-unused-vars

export default class depositCreate extends LightningElement {

    //TODO: Track the total amount of all the added deposits

    @api recordId;

    //Tracked Variables
    @track defaultCostAccount = ''; //TODO: set these with an apex method to get the company defaults.
    @track defaultTrustAccount = '';
    @track defaultOperatingAccount = '';

    @track depositWrappers = [] //deposit row data to send to apex controller
    @track rtItems = [];
    @track rtMap = {};

    @track totalAmount = 0; //this tracks the total amount added on the page

    @track bankAccount;
    @track bankAccountId;

    @track selectedRecordType = '';
    @track error;
    @track showSpinner = false;
    @track disableSave = false;

    @track numberOfRowsToAdd = 1;
    keyInt = 0;
    disableLimits = true;
    @track disableUnallocated = true;
    spinnerMessage = 'Please Wait'
    firstLoad = true

    //Component Init
    constructor() {
        super();

        //load the rows
        if (this.firstLoad === true) {
            this.handleAddRow();
        }
        this.firstLoad = false
    }
    // get today's date in apporpriate string
    get todayDate() {
        this.today = new Date();
        this.dd = String(this.today.getDate()).padStart(2, '0');
        this.mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
        this.yyyy = this.today.getFullYear();
        this.today = this.yyyy + '-' + this.mm + '-' + this.dd;
        return this.today
    }
    //fectch the current bank account record
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    bankAccountRecord({ error, data }) {
        if (error) {
            this.disableSave = true
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading contact',
                    message: 'Error retrieving bank account defaults',
                    variant: 'error',
                    mode: 'sticky'
                }),
            );
        } else if (data) {
            this.spinnerMessage = 'Loading Bank Account Defaults'
            this.showSpinner = true

            this.bankAccount = data;
            this.bankAccountId = this.bankAccount.fields.Id.value;
            this.getBankAccountDefaultValues()
            this.getRTOptions()
        }
    }
    getRTOptions() {
        getRecordTypeOptions({ bankAccountId: this.bankAccountId })
            .then(result => {
                //create array with elements which has been retrieved controller
                //here value will be Id and label of combobox will be Name

                for (i = 0; i < result.length; i++) {

                    //store the record type map for later use
                    let rtId = result[i].Id
                    let rtName = result[i].Name
                    this.rtMap[rtId] = rtName
                    // console.log('getRecordTypeOptions - rtId = '+ rtId)
                    // console.log('getRecordTypeOptions - rtName = '+ rtName)

                    //store the picklist items
                    this.rtItems = [...this.rtItems, { value: result[i].Id, label: result[i].Name }];
                }
                //if the size of the options is 1 then set the selection to that value
                console.log('getRecordTypeOptions - this.rtItems.length = ' + this.rtItems)
                if (this.rtItems.length === 1) {
                    this.selectedRecordType = result[0].Id

                    //if that item is cost or operating, update the disabled flags:
                    if (result[0].Name === 'Trust Payout - Costs' || result[0].Name === 'Operating') {
                        this.disableLimits = false
                    }
                    if (result[0].Name === 'Operating') {
                        this.disableUnallocated = false
                    }
                }

            })
            .catch(error => {
                this.error = error;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: 'Error retrieving the bank account defaults',
                        variant: 'error'
                    })
                );
                this.spinnerMessage = 'Please wait'
                this.showSpinner = false
                console.log('this.error = ' + this.error)
            });

    }
    //Methods
    getBankAccountDefaultValues() {
        console.log('record Id = ' + this.bankAccountId)
        getBankAccountDefaults({ bankAccountId: this.bankAccountId })
            .then(result => {
                this.defaultCostAccount = result.Cost
                this.defaultOperatingAccount = result.Operating
                this.defaultTrustAccount = result.Trust

                if (result.Error !== undefined && result.Error !== null && result.Error !== 'undefined' && result.Error !== '') {
                    console.log('Error : ' + JSON.stringify(result.Error));

                    this.disableSave = true
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error:',
                            message: 'Error validating bank account selections',
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                }

                this.spinnerMessage = 'Please wait'
                this.showSpinner = false
            })
            .catch(error => {
                this.error = error;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: 'Error retrieving the bank account defaults' + this.error.body.message,
                        variant: 'error'
                    })
                );
                this.spinnerMessage = 'Please wait'
                this.showSpinner = false
                // console.log('this.error = '+ this.error)
            });
    }
    validateBankAccountSelection(event) {
        let selectedId = ''
        if (event.target.value !== null && event.target.value !== 'null' && event.target.value !== 'undefined') {
            selectedId = event.target.value;
        }
        //check if the bank account is valid
        getBankAccountDetails({ bankAcctId: event.target.value })
            .then(result => {
                if (result.Error !== undefined && result.Error !== null && result.Error !== 'undefined' && result.Error !== '') {
                    console.log('Error : ' + JSON.stringify(result.Error));
                    this.disableSave = true
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error:',
                            message: result.Error,
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                }
                else {
                    this.disableSave = false
                }
            })
            .catch(error => {
                this.error = error;
                console.log('this.error = ' + this.error)
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: this.error.message,
                        variant: 'error'
                    })
                );
            });
    }
    handleDisableSave(event) {
        this.disableSave = event.detail.disableSave;
    }
    handleAddRow() {
        // console.log('this.numberOfRowsToAdd = '+ this.numberOfRowsToAdd)
        addDepositRow({ depositList: this.depositWrappers, numberToAdd: this.numberOfRowsToAdd, keyValue: this.keyInt })
            .then(result => {
                this.depositWrappers = (result);
                this.keyInt++
                // console.log('this.keyInt = '+ this.keyInt)
                // console.log('this.depositWrappers = '+ JSON.stringify(this.depositWrappers))
            })
            .catch(error => {
                this.error = error;
                // console.log('this.error = '+ this.error)
            });
    }
    handleRowDelete(event) {
        console.log('handleRowDelete: index = ' + JSON.stringify(event.detail));
        let clonedWrappers = JSON.parse(JSON.stringify(this.depositWrappers))

        clonedWrappers.splice(event.detail, 1);
        this.depositWrappers = clonedWrappers;
        console.log('new depositWrappers: ' + JSON.stringify(this.depositWrappers));
    }

    handleCostBankAccountChange(event) {
        this.defaultCostAccount = event.target.value;
        this.validateBankAccountSelection(event);
    }
    handleOperatingBankAccountChange(event) {
        this.defaultOperatingAccount = event.target.value;
        this.validateBankAccountSelection(event);
    }
    handleTrustBankAccountChange(event) {
        this.defaultTrustAccount = event.target.value;
        this.validateBankAccountSelection(event);
    }
    handleNumberOfRowsChange(event) {
        this.numberOfRowsToAdd = event.target.value;
    }
    handleRecordTypeChange(event) {
        this.selectedRecordType = event.target.value;
        let selectedRT = this.selectedRecordType
        // console.log('handleRecordTypeChange - rtMap = '+ JSON.stringify(this.rtMap))
        // console.log('handleRecordTypeChange - selectedRT = '+ selectedRT)
        // console.log(this.rtMap.hasOwnProperty(selectedRT))

        let recordTypeName = this.rtMap.hasOwnProperty(selectedRT) ? this.rtMap[selectedRT] : '';

        // console.log('handleRecordTypeChange - recordTypeName = '+ recordTypeName)
        if (recordTypeName === 'Trust Payout - Costs' || recordTypeName === 'Operating') {
            this.disableLimits = false
        }
        else {
            //reset the amounts
            let childComponents = this.template.querySelectorAll('c-deposit-create-row')
            // console.log('childComponents = '+ JSON.stringify(childComponents))
            childComponents.forEach(e => { e.resetLimits() })

            this.disableLimits = true
        }

        if (recordTypeName === 'Operating') {
            this.disableUnallocated = false
            let childComponents = this.template.querySelectorAll('c-deposit-create-row')
            // console.log('childComponents = '+ JSON.stringify(childComponents))
            childComponents.forEach(e => { e.resetUnallocated() })
        }
    }
    handleColumnDataChange(event) {
        // console.log('handleColumnDataChange - event.detail = '+JSON.stringify(event.detail));
        // console.log('handleColumnDataChange - this.depositWrappers BEFORE UPDATE = '+ JSON.stringify(this.depositWrappers));

        // eslint-disable-next-line radix
        let varIndex = parseInt(event.detail.wrapperRow.index)

        let clonedWrappers = JSON.parse(JSON.stringify(this.depositWrappers))

        // console.log('handleColumnDataChange - event.detail.wrapperRow.columnDataChanged = '+ event.detail.wrapperRow.columnDataChanged);

        if (event.detail.wrapperRow.columnDataChanged === 'matterId') {
            //clonedWrappers[varIndex].matterId = Object.assign({}, event.detail.wrapperRow.wrapperData).matterId;
            clonedWrappers[varIndex].matterId = event.detail.wrapperRow.wrapperData.matterId;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'depositAmount') {
            clonedWrappers[varIndex].depositAmount = event.detail.wrapperRow.wrapperData.depositAmount;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'checkNumber') {
            clonedWrappers[varIndex].checkNumber = event.detail.wrapperRow.wrapperData.checkNumber;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'hardCostLimit') {
            clonedWrappers[varIndex].hardCostLimit = event.detail.wrapperRow.wrapperData.hardCostLimit;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'softCostLimit') {
            clonedWrappers[varIndex].softCostLimit = event.detail.wrapperRow.wrapperData.softCostLimit;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'allocatedFee') {
            clonedWrappers[varIndex].allocatedFee = event.detail.wrapperRow.wrapperData.allocatedFee;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'unallocatedCost') {
            clonedWrappers[varIndex].unallocatedCost = event.detail.wrapperRow.wrapperData.unallocatedCost;
        }

        this.depositWrappers = clonedWrappers;
        this.calculateTotal()

        // console.log('handleColumnDataChange - this.depositWrappers AFTER UPDATE = '+ JSON.stringify(this.depositWrappers));
    }
    calculateTotal() {
        let currentWrapper;
        this.totalAmount = 0.00;
        for (currentWrapper of this.depositWrappers) {
            this.totalAmount += parseFloat(currentWrapper.depositAmount)
        }
    }
    resetLines() {
        let newWrappers = []
        this.depositWrappers = newWrappers;
        this.keyInt = 0;
        this.numberOfRowsToAdd = 1;
        this.totalAmount = 0;
        this.handleAddRow();
        //reset the amounts
        let childComponents = this.template.querySelectorAll('c-deposit-create-row')
        // console.log('childComponents = '+ JSON.stringify(childComponents))
        childComponents.forEach(e => { e.resetRow() })
    }
    handleSubmit(event) {
        event.preventDefault();       // stop the form from submitting

        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        const allValidComboBoxes = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        console.log('selectedRecordType: -' + this.selectedRecordType + '-');
        if (this.selectedRecordType === undefined || (this.selectedRecordType !== undefined && this.selectedRecordType.length === 0)) {
            alert('Deposit Type must be entered!');
            return;
        }

        console.log('event.detail.fields = ' + JSON.stringify(event.detail.fields));
        this.showSpinner = true;
        this.handleSaveDeposits(event)
    }
    handleSaveDeposits(event) {
        saveDeposits(
            { headerFields: event.detail.fields, depositWrapperList: this.depositWrappers, selectedRecordType: this.selectedRecordType })
            .then(result => {
                console.log('save result = ' + result)

                //handle success
                if (result.isSuccess) {
                    //reset the lines form
                    this.resetLines()
                    this.showSpinner = false;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success:',
                            message: 'Successfully created deposits and kicked off allocation batch',
                            variant: 'success',
                        })
                    );
                }
                //handle validation errors
                if (result.hasValidationError) {
                    this.showSpinner = false;
                    const validationToastMessage = new ShowToastEvent({
                        title: 'Warning: ',
                        message: result.validationMessage,
                        variant: 'warning',
                        mode: 'sticky'
                    });
                    this.dispatchEvent(validationToastMessage);
                }
            })
            .catch(error => {
                this.error = error;
                this.showSpinner = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: this.error.body.message,
                        variant: 'error',
                        mode: 'sticky'
                    })
                );
                console.log('this.error = ' + this.error)
            });
    }
}