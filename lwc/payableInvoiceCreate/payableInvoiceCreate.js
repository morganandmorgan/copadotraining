/* eslint-disable no-useless-concat */
/* eslint-disable no-unused-vars */
/* eslint-disable radix */
/* eslint-disable no-console */

//Import standard lightning functions
import { LightningElement, track, wire, api } from 'lwc'
import { getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//Import Apex Methods we'll need:
import addPinRow from '@salesforce/apex/createPayableInvoiceController.addPinRow'
import savePins from '@salesforce/apex/createPayableInvoiceController.savePins'
import getMatterDetails from '@salesforce/apex/createPayableInvoiceController.getMatterDetails'
import getAccountVerifiedInfo from '@salesforce/apex/createPayableInvoiceController.getAccountVerifiedInfo'
import getAccountDefaultExpenseTypeId from '@salesforce/apex/createPayableInvoiceController.getAccountDefaultExpenseTypeId'
import checkCurrentCompany from '@salesforce/apex/createPayableInvoiceController.checkCurrentCompany'

//Constants
const BANK_FIELDS = [
    'c2g__codaBankAccount__c.Id',
    'c2g__codaBankAccount__c.Name'
];

let i = 0
// eslint-disable-next-line no-unused-vars

export default class depositCreate extends LightningElement {

    @api recordId;

    //Tracked Variables
    @track defaultGLA = ''; //comes from the selected Account
    @track defaultExpenseType = ''; //comes from the selected Account

    @track pinLineWrappers = [] //pin row data to send to apex controller

    @track totalAmount = 0; //this tracks the total amount added on the page

    @track bankAccount;
    @track bankAccountId;

    @track vendorAccountId;
    @track pinMatterId;
    @track glaId;
    @track expenseTypeId;
    @track lineDescription;
    @track payeeType;
    @track autoPost = true;

    @track error;
    @track showSpinner = false;
    @track disableSave = false;

    @track glaIdDefaultParent
    @track matterIdDefaultParent
    @track expenseTypeIdDefaultParent
    @track dim1IdDefaultParent
    @track dim2IdDefaultParent
    @track dim3IdDefaultParent
    @track dim4IdDefaultParent
    @track lineDescIdDefaultParent

    @track invoiceDateDefault
    @track dueDateDefault

    numberOfRowsToAdd = 1;
    keyInt = 1;
    spinnerMessage = 'Please Wait'

    //Component Init
    constructor() {
        super();
        this.handleAddRow();
    }

    connectedCallback() {
        this.autoPost = true;
    }

    // get today's date in apporpriate string
    get todayDate() {
        this.today = new Date();
        this.dd = String(this.today.getDate()).padStart(2, '0');
        this.mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
        this.yyyy = this.today.getFullYear();
        this.today = this.yyyy + '-' + this.mm + '-' + this.dd;
        return this.today
    }

    // // get today's date in apporpriate string for Due Date
    // get todayDate_Due() {
    //     this.today = new Date();
    //     this.dd = String(this.today.getDate()).padStart(2, '0');
    //     this.mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
    //     this.yyyy = this.today.getFullYear();
    //     this.today = this.yyyy + '-' + this.mm + '-' + this.dd;
    //     return this.today
    // }

    //fectch the current bank account record
    @wire(getRecord, { recordId: '$recordId', fields: BANK_FIELDS })
    bankAccountRecord({ error, data }) {
        if (error) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading contact',
                    message: 'Error retrieving bank account default',
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.spinnerMessage = 'Loading Bank Account Defaults'
            this.showSpinner = true
            this.bankAccount = data;
            this.bankAccountId = this.bankAccount.fields.Id.value;
            console.log('this.bankAccount = ' + this.bankAccount);

            //check the current company
            this.validatePageLoad();
        }
    }

    validatePageLoad() {
        checkCurrentCompany({ bankAccountId: this.bankAccountId })
            .then(result => {
                if (result.Error !== undefined && result.Error !== null && result.Error !== 'undefined' && result.Error !== '') {
                    console.log('Error : ' + JSON.stringify(result.Error));

                    this.disableSave = true

                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error:',
                            message: result.Error,
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                }
                this.showSpinner = false
            })
            .catch(error => {
                this.error = error;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: 'Error validating page load - ' + this.error.body.message,
                        variant: 'error'
                    })
                );
                this.showSpinner = false
                // console.log('this.error = '+ this.error)
            });
    }

    //HANDLE METHODS
    handleAddRow() {
        console.log('this.numberOfRowsToAdd = ' + this.numberOfRowsToAdd)
        let self = this
        addPinRow({ pinLineWrappers: this.pinLineWrappers, numberToAdd: this.numberOfRowsToAdd, keyValue: this.keyInt })
        .then(result => {
                this.pinLineWrappers = JSON.parse(JSON.stringify(result));
                // this.keyInt+= Number(this.numberOfRowsToAdd)
                console.log('this.pinLineWrappers = ', this.pinLineWrappers);
                console.log('this.numberOfRowsToAdd = ' + this.numberOfRowsToAdd, this.keyInt);
                for (let count = this.keyInt - 1; count < this.pinLineWrappers.length; count++)
                {
                    let newRecord = this.pinLineWrappers[count];
                    if (this.glaId !== undefined)
                        newRecord.glaId = this.glaId;
                    if (this.expenseTypeId !== undefined)
                        newRecord.expenseTypeId = this.expenseTypeId;
                    console.log('newRecord: ' + JSON.stringify(newRecord));
                   
                }
                
                this.keyInt+= Number(this.numberOfRowsToAdd)

                console.log('this.keyInt = ' + this.keyInt);
                // console.log('this.pinLineWrappers = '+ JSON.stringify(this.pinLineWrappers))


            })
            .catch(error => {
                this.error = error;
                console.log('handleAddRow - this.error = ' + this.error)
            });
    }
    handleNumberOfRowsChange(event) {
        this.numberOfRowsToAdd = event.target.value;
    }
    handleAutoPostChange(event) {
        this.autoPost = event.target.value;
    }
    handleDisableSave(event) {
        this.disableSave = event.detail.disableSave;
    }

    handleGLASelect(event) {
        //set the variable
        this.glaId = event.target.value;
        let self = this;
        let childComponents = this.template.querySelectorAll('c-payable-invoice-row-create')
        childComponents.forEach(c => {
            c.setDefaultGLA(self.glaId)
        })

    }

    handleExpenseTypeSelect(event) {
        //set the variable
        this.expenseTypeId = event.target.value;
        let self = this;
        let childComponents = this.template.querySelectorAll('c-payable-invoice-row-create')
        childComponents.forEach(c => {
            c.setDefaultExpenseType(self.expenseTypeId)
        })

    }

    
    /*
    handlePinMatterSelect(event) {
        //set the variable
        this.pinMatterId = event.target.value;
        if (this.pinMatterId !== undefined) {
            //default from the matter
            getMatterDetails({ matterId: this.pinMatterId })
                .then(result => {
                    if (this.payeeType === 'Client') {
                        this.vendorAccountId = result.client;
                        console.log('this.vendorAccountId = ' + this.vendorAccountId)
                    }
                    
                    // this.pinLineWrappers.forEach(r => {
                    //     r.dim1Id = result.dim1
                    //     r.dim2Id = result.dim2
                    //     r.dim3Id = result.dim3
                    //     r.dim4Id = result.dim4
                    // })

                    //set the matter lookup on the line items:
                    let childComponents = this.template.querySelectorAll('c-payable-invoice-row-create')
                    childComponents.forEach(c => {
                        c.setDefaultMatter(this.pinMatterId)
                    })

                    if (result.Error !== undefined && result.Error !== null && result.Error !== undefined && result.Error !== '') {
                        console.log('Error : ' + JSON.stringify(result.Error));
                        this.disableSave = true
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Error:',
                                message: result.Error,
                                variant: 'error',
                                mode: 'sticky'
                            })
                        );
                    }
                    else {
                        this.disableSave = false
                    }
                })
                .catch(error => {
                    this.error = error;
                    console.log('this.error = ' + this.error)
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error:',
                            message: this.error.message,
                            variant: 'error'
                        })
                    );
                });
        }
    }
    */
    handlePayeeTypeChange(event) {
        //set the payee variable
        this.payeeType = event.target.value;

        //default the account field if appropriate
        if (this.payeeType === 'Client' && this.pinMatterId !== null && this.pinMatterId !== '') {
            getMatterDetails({ matterId: this.pinMatterId })
                .then(result => {
                    console.log('result = ' + this.vendorAccountId)
                    this.vendorAccountId = result.client;
                    console.log('this.vendorAccountId = ' + this.vendorAccountId)
                })
                .catch(error => {
                    this.error = error;
                    console.log('this.error = ' + this.error)
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error:',
                            message: this.error.body.message,
                            variant: 'error'
                        })
                    );
                });
        }

    }
    handleVendorAccountChange(event) {
        this.vendorAccountId = this.payeeType = event.target.value;
        console.log('Vendor Account ID : ' + this.vendorAccountId);

        //call method to get the gla default
        getAccountVerifiedInfo({ accountId: this.vendorAccountId })
            .then(result => {
                //this.glaIdDefaultParent = (result);
                //console.log('this.glaIdDefaultParent = ' + this.glaIdDefaultParent)
                console.log('result.Approved_Vendor__c = ' + result.Approved_Vendor__c)
                console.log('result.Vendor_Verification__c = ' + result.Vendor_Verification__c)
            
                console.log('result = ' + result)
                if ((result.Approved_Vendor__c == false  && result.Vendor_Verification__c !== 'Verified') || (result.Approved_Vendor__c == false  && result.Vendor_Verification__c == null) ) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Warning:',
                            message: result.Name + ' is not a verified vendor!',
                            variant: 'warning',
                            mode: 'sticky'
                        })
                    );
                }
            })
            .catch(error => {
                this.error = error;
                console.log('this.error = ' + this.error)
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: this.error.message,
                        variant: 'error'
                    })
                );
            });
    }
    handleRowDelete(event) {
        console.log('handleRowDelete: index = ' + JSON.stringify(event.detail));
        let clonedWrappers = JSON.parse(JSON.stringify(this.pinLineWrappers))

        clonedWrappers.splice(event.detail, 1);

        let lineNum = 1;
        for(let wrapper of clonedWrappers)
        {
            wrapper.key = lineNum++;
        }
        this.keyInt = lineNum;

        this.pinLineWrappers = clonedWrappers;

        console.log('new pinLineWrappers: ' + JSON.stringify(this.pinLineWrappers));
        console.log('***lineNum: ' + lineNum);
        console.log('***keyInt: ' + this.keyInt);
        
    }

    handleColumnDataChange(event) {
        console.log('event.detail = ' + JSON.stringify(event.detail));

        let varIndex = parseInt(event.detail.wrapperRow.index)

        let clonedWrappers = JSON.parse(JSON.stringify(this.pinLineWrappers))

        if (event.detail.wrapperRow.columnDataChanged === 'matterId') {
            clonedWrappers[varIndex].matterId = event.detail.wrapperRow.wrapperData.matterId;
            clonedWrappers[varIndex].dim1Id = event.detail.wrapperRow.wrapperData.dim1Id;
            clonedWrappers[varIndex].dim2Id = event.detail.wrapperRow.wrapperData.dim2Id;
            clonedWrappers[varIndex].dim3Id = event.detail.wrapperRow.wrapperData.dim3Id;
            clonedWrappers[varIndex].dim4Id = event.detail.wrapperRow.wrapperData.dim4Id;
            clonedWrappers[varIndex].lineDescription = event.detail.wrapperRow.wrapperData.lineDescription;

        }
        else if (event.detail.wrapperRow.columnDataChanged === 'glaId') {
            clonedWrappers[varIndex].glaId = event.detail.wrapperRow.wrapperData.glaId;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'lineAmount') {
            clonedWrappers[varIndex].lineAmount = event.detail.wrapperRow.wrapperData.lineAmount;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'expenseTypeId') {
            console.log('Hmmmmm... ' + event.detail.wrapperRow.wrapperData.expenseTypeId);
            clonedWrappers[varIndex].expenseTypeId = event.detail.wrapperRow.wrapperData.expenseTypeId;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'dim1Id') {
            clonedWrappers[varIndex].dim1Id = event.detail.wrapperRow.wrapperData.dim1Id;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'dim2Id') {
            clonedWrappers[varIndex].dim2Id = event.detail.wrapperRow.wrapperData.dim2Id;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'dim3Id') {
            clonedWrappers[varIndex].dim3Id = event.detail.wrapperRow.wrapperData.dim3Id;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'dim4Id') {
            clonedWrappers[varIndex].dim4Id = event.detail.wrapperRow.wrapperData.dim4Id;
        }
        else if (event.detail.wrapperRow.columnDataChanged === 'lineDescription') {
            clonedWrappers[varIndex].lineDescription = event.detail.wrapperRow.wrapperData.lineDescription;
            console.log('***Description... ' + event.detail.wrapperRow.wrapperData.lineDescription);
        }

        this.pinLineWrappers = clonedWrappers;
        this.calculateTotal();
    }
    handleSubmit(event) {
        event.preventDefault();       // stop the form from submitting
        console.log('event.detail.fields = ' + JSON.stringify(event.detail.fields));

        this.showSpinner = true;

        // Do some validation...
        let errorFlag = false;
        let self = this;
        this.pinLineWrappers.forEach(function (pinLine, index) {
            if (pinLine.glaId === undefined || (pinLine.glaId !== undefined && pinLine.glaId.length === 0)) {
                self.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: 'GLA is not set on line ' + (index + 1),
                        variant: 'error'
                    })
                );
                errorFlag = true;

            }
            
            if (pinLine.expenseTypeId === undefined && pinLine.Create_Litify_Expenses__c === true || (pinLine.expenseTypeId !== undefined && pinLine.expenseTypeId.length === 0 && pinLine.Create_Litify_Expenses__c === true)) {
                self.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: 'Expense Type is not set on line ' + (index + 1),
                        variant: 'error'
                    })
                );

                errorFlag = true;
            }
            // REMOVED 3/2/20 - Validation is now happening on the PIN Line through a validation rule
            // if (pinLine.matterId === undefined || (pinLine.matterId !== undefined && pinLine.matterId.length === 0)) {
            //     self.dispatchEvent(
            //         new ShowToastEvent({
            //             title: 'Error:',
            //             message: 'Matter is not set on line ' + (index + 1),
            //             variant: 'error'
            //         })
            //     );

            //     errorFlag = true;
            // }
        })
        if (errorFlag) {
            this.showSpinner = false;
            return;
        }
        // add save call here
        this.handleSavePayableInvoice(event);
    }

    resetLines() {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
                if(field.fieldName == 'c2g__DueDate__c'){
                    field.value = this.todayDate
                }
            });
        }

        let newWrappers = []
        this.pinLineWrappers = newWrappers
        this.keyInt = 1
        this.totalAmount = 0.00
        this.numberOfRowsToAdd = 1
        this.glaIdDefaultParent = null
        this.matterIdDefaultParent = null
        this.expenseTypeIdDefaultParent = null
        this.dim1IdDefaultParent = null
        this.dim2IdDefaultParent = null
        this.dim3IdDefaultParent = null
        this.dim4IdDefaultParent = null
        this.lineDescIdDefaultParent = ''
        this.handleAddRow();
    }

    calculateTotal() {
        let currentWrapper;
        this.totalAmount = 0.00;
        for (currentWrapper of this.pinLineWrappers) {
            this.totalAmount += parseFloat(currentWrapper.lineAmount)
        }
    }
    handleSavePayableInvoice(event) {
        savePins(
            { headerFields: event.detail.fields, pinLineWrapperList: this.pinLineWrappers, bankAccountId: this.bankAccountId, autoPost: this.autoPost })
            .then(result => {
                console.log('save result = ' + result)

                //handle success
                if (result.isSuccess) {
                    //reset the lines form
                    this.resetLines()
                    this.showSpinner = false;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success:',
                            message: result.sucessMessage,
                            variant: 'success',
                        })
                    );
                }
                //handle validation errors
                if (result.hasValidationError) {
                    this.showSpinner = false;
                    const validationToastMessage = new ShowToastEvent({
                        title: 'Warning: ',
                        message: result.validationMessage,
                        variant: 'warning',
                        mode: 'sticky'
                    });
                    this.dispatchEvent(validationToastMessage);
                }
            })
            .catch(error => {
                this.error = error;
                this.showSpinner = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error:',
                        message: this.error.body.message,
                        variant: 'error',
                        mode: 'sticky'
                    })
                );
                console.log('this.error = ', this.error)
            });
    }
}