<apex:page showHeader="true"
    standardStylesheets="false"
    sidebar="true"
    applyHtmlTag="true"
    applyBodyTag="false"
    docType="html-5.0"
    standardController="Account"
    extensions="mmgmap_VerifyAddressExtension">

<html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<head>
    <title>{! Account.Name} -- Verify Addresses</title>
    <apex:slds />
    <apex:includeScript value="https://code.jquery.com/jquery-3.2.1.min.js" />

    <style type="text/css">
        [contentEditable=true]:empty:not(:focus):before
        {
            content:attr(placeholder-text)
        }
    </style>

    <script src="{!SourceApiString}" async="" defer=""></script>

    <script type="text/javascript">
        function pageLoad()
        {
            initAutocomplete();
            placeBillingAddressIntoSearchTextbox();

            if ($("#addressFromUser").val())
            {
                searchForAddress();
            }
        }
    </script>

    <script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete;

        var assignmentMatrix =
        {
            street_number: ['streetNum', 'long_name'],
            route: ['streetName', 'short_name'],
            locality: ['city', 'long_name'],
            administrative_area_level_1: ['state', 'short_name'],
            postal_code: ['postalCode', 'long_name']
        };

        var address =
        {
            data:
            {
                streetNum: '',
                streetName: '',
                street: '',
                city: '',
                state: '',
                postalCode: ''
            },
            isEmpty: true
        };

        function initAutocomplete()
        {
            autocomplete =
                new google.maps.places.Autocomplete(
                    (document.getElementById('addressFromUser')),
                    {types: ['address']});

            // When the user selects an address from the dropdown ...
            autocomplete.addListener('place_changed', selectAddressHandler);
        }

        function selectAddressHandler()
        {
            var place = autocomplete.getPlace();

            if (!place) { return; }

            for (var i = 0; i < place.address_components.length; i++)
            {
                var comp = place.address_components[i];
                var am = assignmentMatrix[comp['types'][0]];

                if (am)
                {
                    address.data[am[0]] = comp[am[1]];
                    address.isEmpty = false;
                };
            }

            address.data['street'] = address.data['streetNum'];
            if (0 < address.data['street'].length)
            {
                address.data['street'] += ' ';
            }
            address.data['street'] += address.data['streetName'];

            $('#lookup-street').text(address.data['street']);
            $('#lookup-city').text(address.data['city']);
            $('#lookup-state').text(address.data['state']);
            $('#lookup-postalCode').text(address.data['postalCode']);
        }

        function applyToCheckedAddresses()
        {
            if (!address.isEmpty)
            {
                $('.mmAddressCheckbox:checked').each(
                    function(){

                        var fields = ['street', 'city', 'state', 'postalCode'];
                        var refTo = $(this).attr('refTo');

                        for (var i = 0; i < fields.length; i++)
                        {
                            var f = fields[i];
                            var inputName = refTo + '-' + f;

                            $('[id$=' + inputName + ']').val(address.data[f]);
                        }
                    }
                );
            }
        }

        function clearCheckedAddresses()
        {
            $('.mmAddressCheckbox:checked').each(
                function(){

                    var fields = ['street', 'city', 'state', 'postalCode'];
                    var refTo = $(this).attr('refTo');

                    for (var i = 0; i < fields.length; i++)
                    {
                        var f = fields[i];
                        var inputName = refTo + '-' + f;

                        $('[id$=' + inputName + ']').val('');
                    }
                }
            );
        }

        function clearAddressSearch()
        {
            $("#addressFromUser").val('');
            $("#lookup-street").text('');
            $("#lookup-city").text('');
            $("#lookup-state").text('');
            $("#lookup-postalCode").text('');
            for (var k in address.data) { address.data[k] = '' };
            address.isEmpty = true;
        }

        function placeBillingAddressIntoSearchTextbox()
        {
            var billAddress = $("[id$=bill-street]").val();
            billAddress = appendTextWithSpacer(billAddress, $("[id$=bill-city]").val(), ' ');
            billAddress = appendTextWithSpacer(billAddress, $("[id$=bill-state]").val(), ' ');
            billAddress = appendTextWithSpacer(billAddress, $("[id$=bill-postalCode]").val(), ' ');

            if (0 < billAddress.length)
            {
                $("#addressFromUser").val(billAddress);
            }
        }

        function appendTextWithSpacer(targetString, stringToAppend, spacer)
        {
            if (!spacer) { spacer = ' '; };
            if (targetString && targetString.length < 1 && stringToAppend) { return stringToAppend; };
            if (stringToAppend && stringToAppend.length < 1) { return targetString; };
            return targetString + spacer + stringToAppend;
        }
    </script>
</head>

<body class="slds-scope">

    <div class="slds-page-header" role="banner">
    <div class="slds-grid">
        <div class="slds-col slds-has-flexi-truncate">
            <span class="slds-icon_container slds-icon-standard-location">
                <svg class="slds-icon slds-icon--medium" aria-hidden="true">
                    <use xlink:href="{! URLFOR($Asset.SLDS, '/assets/icons/standard-sprite/svg/symbols.svg#location')}"></use>
                </svg>
            </span>
            <span>Verify Addresses for Account</span>
            <div class="slds-page-header__title slds-truncate" title="My Accounts">{! Account.Name }</div>
        </div>
    </div>
    </div>

    <div class="slds-panel slds-grid slds-grid--vertical slds-nowrap">
        <div class="slds-form--stacked slds-grow slds-scrollable--y">

            <div class="slds-panel__section">
                <h3 class="slds-text-heading--small slds-m-bottom--medium">Account Detail</h3>
                <div class="slds-form-element slds-hint-parent slds-has-divider--bottom">
                    <div class="slds-grid">
                        <div class="slds-col">
                            <span class="slds-form-element__label">Name: </span>
                            <span class="slds-form-element__control">
                                <span class="slds-form-element__static">{! Account.Name }</span>
                            </span>
                            <br/>
                            <span class="slds-form-element__label">Phone: </span>
                            <span class="slds-form-element__control">
                                <span class="slds-form-element__static">{! Account.Phone }</span>
                            </span>
                            <br/>
                            <span class="slds-form-element__label">Email: </span>
                            <span class="slds-form-element__control">
                                <span class="slds-form-element__static">{! Account.PersonEmail }</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <apex:form >
                <div class="slds-panel__section slds-has-divider--bottom">
                    <h3 class="slds-text-heading--small slds-m-bottom--medium">Edit Address Details</h3>
                    <div class="slds-media">
                        <div class="slds-media__body">
                            <div class="slds-col">
                                <div class="slds-box">
                                    <input type="text" placeholder="Enter the address" id="addressFromUser" value="" style="width:500px;"></input>
                                    <button type="submit" class="slds-button slds-button--neutral" onclick="clearAddressSearch(); return false;">Reset Search</button><br/>
                                    <br/>
                                    <div class="slds-form-element__label" id="lookup-street" contentEditable="true" placeholder-text="street"></div><br/>
                                    <div class="slds-form-element__label" id="lookup-city" contentEditable="true" placeholder-text="city"></div><br/>
                                    <div class="slds-form-element__label" id="lookup-state" contentEditable="true" placeholder-text="state"></div><br/>
                                    <div class="slds-form-element__label" id="lookup-postalCode" contentEditable="true" placeholder-text="postal code"></div><br/>
                                    <br/>
                                    <button type="submit" class="slds-button slds-button--neutral" onclick="applyToCheckedAddresses(); return false;">Apply To Checked</button>
                                    <button type="submit" class="slds-button slds-button--neutral" onclick="clearCheckedAddresses(); return false;">Clear Checked</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slds-media">
                        <div class="slds-media__body">
                            <div class="slds-button-group slds-m-top--small mm_AddressesDiv" role="group">
                                <div class="slds-col">
                                    <div class="slds-box">
                                        <div class="slds-form-element__control">
                                            <span class="slds-checkbox">
                                                <input type="checkbox" class="mmAddressCheckbox" id="BillingCheckbox" refTo="bill" checked=""/>
                                                <label class="slds-checkbox__label" for="BillingCheckbox">
                                                    <span class="slds-checkbox_faux"></span>
                                                    <span class="slds-form-element__label">Physical Address</span><br/>
                                                </label>
                                            </span>
                                            <apex:inputText id="bill-street" value="{! Account.BillingStreet}" html-placeholder="street"/><br/>
                                            <apex:inputText id="bill-city" value="{! Account.BillingCity}" html-placeholder="city"/><br/>
                                            <apex:inputText id="bill-state" value="{! Account.BillingState}" html-placeholder="state"/><br/>
                                            <apex:inputText id="bill-postalCode" value="{! Account.BillingPostalCode}" html-placeholder="postal code"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-col">
                                    <div class="slds-box">
                                        <div class="slds-form-element__control">
                                            <span class="slds-checkbox">
                                                <input type="checkbox" class="mmAddressCheckbox" id="PersonMailingCheckbox" refTo="mail" checked=""/>
                                                <label class="slds-checkbox__label" for="PersonMailingCheckbox">
                                                    <span class="slds-checkbox_faux"></span>
                                                    <span class="slds-form-element__label">Mailing Address</span>
                                                </label>
                                            </span>
                                            <apex:inputText id="mail-street" value="{! Account.PersonMailingStreet}" html-placeholder="street"/><br/>
                                            <apex:inputText id="mail-city" value="{! Account.PersonMailingCity}" html-placeholder="city"/><br/>
                                            <apex:inputText id="mail-state" value="{! Account.PersonMailingState}" html-placeholder="state"/><br/>
                                            <apex:inputText id="mail-postalCode" value="{! Account.PersonMailingPostalCode}" html-placeholder="postal code"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-col">
                                    <div class="slds-box">
                                        <div class="slds-form-element__control">
                                            <span class="slds-checkbox">
                                                <input type="checkbox" class="mmAddressCheckbox" id="PersonOtherCheckbox" refTo="other" checked=""/>
                                                <label class="slds-checkbox__label" for="PersonOtherCheckbox">
                                                    <span class="slds-checkbox_faux"></span>
                                                    <span class="slds-form-element__label">Other Address</span>
                                                </label>
                                            </span>
                                            <apex:inputText id="other-street" value="{! Account.PersonOtherStreet}" html-placeholder="street"/><br/>
                                            <apex:inputText id="other-city" value="{! Account.PersonOtherCity}" html-placeholder="city"/><br/>
                                            <apex:inputText id="other-state" value="{! Account.PersonOtherState}" html-placeholder="state"/><br/>
                                            <apex:inputText id="other-postalCode" value="{! Account.PersonOtherPostalCode}" html-placeholder="postal code"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-col">
                                    <div class="slds-box">
                                        <div class="slds-form-element__control">
                                            <span class="slds-checkbox">
                                                <input type="checkbox" class="mmAddressCheckbox" id="ShippingCheckbox" refTo="ship" checked=""/>
                                                <label class="slds-checkbox__label" for="ShippingCheckbox">
                                                    <span class="slds-checkbox_faux"></span>
                                                    <span class="slds-form-element__label">Shipping Address</span>
                                                </label>
                                            </span>
                                            <apex:inputText id="ship-street" value="{! Account.ShippingStreet}" html-placeholder="street"/><br/>
                                            <apex:inputText id="ship-city" value="{! Account.ShippingCity}" html-placeholder="city"/><br/>
                                            <apex:inputText id="ship-state" value="{! Account.ShippingState}" html-placeholder="state"/><br/>
                                            <apex:inputText id="ship-postalCode" value="{! Account.ShippingPostalCode}" html-placeholder="postal code"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slds-panel__section slds-has-divider--bottom">
                    <div class="slds-media">
                        <div class="slds-media__body">
                            <div class="slds-button-group slds-m-top--small" role="group">
                                <apex:commandButton styleClass="slds-button slds-button--neutral" value="Cancel" action="{! cancel}"/>
                                <apex:commandButton styleClass="slds-button slds-button--brand" value="Submit" action="{! save}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </apex:form>

        </div>
    </div>

</body>
<script type="text/javascript"> (function() { pageLoad(); })(); </script>
</html>
</apex:page>