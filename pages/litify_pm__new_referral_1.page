<apex:page standardController="litify_pm__Referral__c" showHeader="true" sidebar="true" extensions="litify_pm.ReferralWizardController">
    <apex:pageMessages />
    <apex:form >
        <apex:outputPanel rendered="{!!is_lightning}">
            <apex:sectionHeader subtitle="Create a new Referral" title="Step 1 of 4"/>        
            <apex:pageBlock title="Referral Information" mode="edit">
                <apex:pageBlockSection title="Client Information" columns="1">
                    <apex:inputField value="{!referral.litify_pm__Client_First_Name__c}"/>
                    <apex:inputField value="{!referral.litify_pm__Client_Last_Name__c}"/>
                    <apex:inputField value="{!referral.litify_pm__Client_City__c}"/>
                    <apex:inputField value="{!referral.litify_pm__Client_State__c}"/>
                    <apex:inputField value="{!referral.litify_pm__Client_email__c}"/>
                    <apex:inputField value="{!referral.litify_pm__Client_phone__c}"/>
                </apex:pageBlockSection>
                <apex:pageBlockSection title="Case Details" columns="1">
                    <apex:inputField value="{!referral.litify_pm__Case_Type__c}"/>
                    <apex:inputField value="{!referral.litify_pm__External_Ref_Number__c}"/>
                    <apex:inputField value="{!referral.litify_pm__Description__c}" style="width:90%"/>
                    <apex:inputField value="{!referral.litify_pm__Incident_date__c}"/>
                </apex:pageBlockSection>
                <apex:pageBlockButtons location="bottom">
                    <apex:commandButton action="{!view}" value="Cancel" immediate="true"/>
                    <apex:commandButton action="{!step2}" value="Next" />
                </apex:pageBlockButtons>
            </apex:pageBlock>        
        </apex:outputPanel>
        <apex:outputPanel rendered="{!is_lightning}">
            <apex:stylesheet value="{!URLFOR($Resource.litify_pm__slds, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
            <div class="litify slds">
                <ul>
                    <li>
                        <div class="slds-text-heading--large slds-align--absolute-center">
                            Create New Referral
                        </div>
                    </li>
                    <li class="slds-text-heading--label slds-align--absolute-center">
                        Step 1 of 4
                    </li>
                    <div class="slds-p-top--medium"></div>
                    <li class="slds-has-divider--top">
                        <div class="slds-form--stacked">
                            <div class="slds-p-top--x-small"></div>
                            <div>
                                <div class="slds-section__title">Client Information</div>
                                <div class="slds-p-top--medium"></div>
                                <div class="slds-grid">
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">First Name</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Client_First_Name__c}" styleClass="slds-input"/>
                                        </div>
                                    </div>
                                    <div class="slds-m-horizontal--medium"></div>
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">Last Name</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Client_Last_Name__c}" styleClass="slds-input"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-p-top--x-small"></div>
                                <div class="slds-grid">
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">City</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Client_City__c}" styleClass="slds-input"/>
                                        </div>
                                    </div>
                                    <div class="slds-m-horizontal--medium"></div>
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">State</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Client_State__c}" styleClass="slds-select"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-p-top--x-small"></div>
                                <div class="slds-grid">
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">Email</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Client_email__c}" styleClass="slds-input"/>
                                        </div>
                                    </div>
                                    <div class="slds-m-horizontal--medium"></div>
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">Phone</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Client_phone__c}" styleClass="slds-input"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-p-top--x-large"></div>
                            <div>
                                <div class="slds-section__title">Case Information</div>
                                <div class="slds-p-top--medium"></div>
                                <div class="slds-grid">
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">Case Type</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Case_Type__c}" style="width:48%" styleClass="slds-lookup__search-input"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-p-top--x-small"></div>
                                <div class="slds-grid">
                                    <div class="slds-col">
                                        <div class="slds-form-element">
                                            <label class="slds-form-element__label">Case Details</label>
                                            <div class="slds-form-element__control">
                                                <apex:inputField value="{!referral.litify_pm__Description__c}" styleClass="slds-textarea"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-p-top--x-small"></div>
                                <div class="slds-grid">
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">Your Reference Number</label>
                                        <div class="slds-form-element__control">
                                            <input type="text" class="slds-input"/>
                                        </div>
                                    </div>
                                    <div class="slds-m-horizontal--medium"></div>
                                    <div class="slds-col">
                                        <label class="slds-form-element__label">Incident Date</label>
                                        <div class="slds-form-element__control">
                                            <apex:inputField value="{!referral.litify_pm__Incident_date__c}" style="width:80%" styleClass="slds-input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-p-top--x-large"></div>
                            <div>
                                <div class="slds-section__title">Case Location</div>
                                <div class="slds-p-top--medium"></div>
                                <div class="slds-form-element">
                                    <div class="slds-form-element__control">
                                        <label class="slds-checkbox">
                                            <input type="checkbox" checked="true"/>
                                            <span class="slds-checkbox--faux"></span>
                                            <span class="slds-form-element__label">Use the same location as the client</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slds-p-top--medium"></div>
                    </li>
                    <li class="slds-has-divider--top slds-align--right">
                        <div class="slds-p-top--x-small"></div>
                        <div class="slds-grid slds-grid--align-end">
                            <apex:commandButton action="{!step2}" value="Continue" styleClass="slds-button slds-button--brand" />
                            <apex:commandButton action="{!view}" value="Cancel" immediate="true" styleClass="slds-button slds-button--neutral" />
                        </div>
                    </li>
                </ul>
                <div class="slds-p-top--x-small"></div>
            </div>
        </apex:outputPanel>
    </apex:form>
</apex:page>