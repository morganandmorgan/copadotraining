<apex:page controller="mmsysadmin_UserAccessDetailsController" setup="true">
<!--
  <apex:stylesheet value="{!URLFOR($Resource.jQuery, 'css/smoothness/jquery-ui-1.8.16.custom.css')}"/>
 -->
  <apex:stylesheet value="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" />
  <apex:stylesheet value="{!URLFOR($Resource.mmsysadmin, 'css/useraccessdetails.css')}"/>

<!--
  <apex:includeScript value="{!URLFOR($Resource.jQuery, 'js/jquery-1.6.2.min.js')}"/>
  <apex:includeScript value="{!URLFOR($Resource.jQuery, 'js/jquery-ui-1.8.16.custom.min.js')}"/>
 -->
  <apex:includeScript value="https://code.jquery.com/jquery-3.2.1.min.js" />
  <apex:includeScript value="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" />
  <apex:includeScript value="{!URLFOR($Resource.mmsysadmin, 'scripts/useraccessdetails.js')}"/>

  <apex:form >
    <apex:outputPanel layout="none">
      <apex:sectionHeader title="User Access Details" subtitle="{!userFullName}"/>

      <apex:pageBlock title="User Permissions">
        <c:mmsysadmin_PermsetAccessTable tableClass="userPermTable"
                              rows="{!permsetInfo}"
                              headings="{!userPermLabels}"
                              enabledCols="{!userPermStatus}">
          <div style="width: 21px">
            <apex:image value="/img/checkbox_checked.gif" alt="Checked" styleClass="checkImg"
                        title="Checked" rendered="{!userPerms[row.Id][heading.apiName]}"/>
          </div>
        </c:mmsysadmin_PermsetAccessTable>
      </apex:pageBlock>

      <apex:pageBlock title="Object Information">
        <apex:pageBlockButtons location="top">
          <!-- We don't actually have any buttons, but this gives us an easy way to include a legend -->
          <b>Legend:</b>
          <span class="permissionscreate_true">Create</span>,
          <span class="permissionsread_true" style="color: white">Read</span>,
          <span class="permissionsedit_true">Edit</span>,
          <span class="permissionsdelete_true">Delete</span>,
          <span class="permissionsviewallrecords_true">View All Records</span>,
          <span class="permissionsmodifyallrecords_true">Modify All Records</span>
        </apex:pageBlockButtons>

        <c:mmsysadmin_PermsetAccessTable tableClass="objectPermTable" columnClass="objectColumn"
                              rows="{!permsetInfo}"
                              headings="{!objectLabels}"
                              enabledCols="{!objectPermStatus}">
          <table class="objectPermDetail" cellpadding="0" cellspacing="0">
            <tr>
              <apex:repeat var="perm" value="{!objectPermFieldNames}">
                <td class="obj_{!heading.apiName}">
                  <div class="objectPermField {!perm}_{!objectPerms[row.Id][heading.apiName][perm]}">
                    &nbsp;
                  </div>
                </td>
              </apex:repeat>
            </tr>
          </table>
        </c:mmsysadmin_PermsetAccessTable>

        <!-- Defines a JavaScript function we can call to reload the FLS table -->
        <apex:actionFunction action="{!loadFls}" name="loadFls" rerender="flsTable"
                             oncomplete="drawCanvas(event);">
          <apex:param name="flsObjectType" assignTo="{!flsObjectType}" value=""/>
          <apex:param name="eventTarget" value=""/>
        </apex:actionFunction>

        <apex:outputPanel layout="block" styleClass="flsInfo" rendered="{!NOT(ISNULL(flsObjectType))}">
          <!-- Gives us someplace to draw our cool, attention-getting polygon -->
          <div id="canvasDiv" style="height: 24px">
            <canvas height="24px" class="fls-canvas"/>
          </div>

          <apex:outputPanel id="flsTable" layout="block" styleClass="flsTableContainer">
            <div class="flsTableContainerHeading">Field Level Security: {!flsObjectType}</div>
            <c:mmsysadmin_PermsetAccessTable tableClass="flsTable" columnClass="flsColumn"
                                  rows="{!permsetInfo}"
                                  headings="{!fieldLabels}"
                                  enabledCols="{!fieldStatus}">
              <table class="flsDetail" cellpadding="0" cellspacing="0">
                <tr>
                  <apex:repeat var="perm" value="{!flsPermFieldNames}">
                    <td class="fls_{!heading.apiName}">
                      <div class="flsCell {!perm}_{!fls[row.Id][heading.apiName][perm]}">&nbsp;</div>
                    </td>
                  </apex:repeat>
                </tr>
              </table>
            </c:mmsysadmin_PermsetAccessTable>
          </apex:outputPanel>
        </apex:outputPanel>
      </apex:pageBlock>

      <apex:pageBlock title="Apex Classes">
        <c:mmsysadmin_PermsetAccessTable tableClass="apexClassTable" rows="{!permsetInfo}" headings="{!classNames}"
                              enabledCols="{!classStatus}">
          <div style="width: 21px">
            <apex:image value="/img/checkbox_checked.gif" alt="Checked" styleClass="checkImg"
                        title="Checked" rendered="{!apexClasses[row.Id][heading.apiName]}"/>
          </div>
        </c:mmsysadmin_PermsetAccessTable>
      </apex:pageBlock>

      <apex:pageBlock title="Visual Force Pages">
        <c:mmsysadmin_PermsetAccessTable tableClass="vfPageTable" rows="{!permsetInfo}" headings="{!pageNames}"
                              enabledCols="{!pageStatus}">
          <div style="width: 21px">
            <apex:image value="/img/checkbox_checked.gif" alt="Checked" styleClass="checkImg"
                        title="Checked" rendered="{!pages[row.Id][heading.apiName]}"/>
          </div>
        </c:mmsysadmin_PermsetAccessTable>
      </apex:pageBlock>
    </apex:outputPanel>
  </apex:form>
</apex:page>