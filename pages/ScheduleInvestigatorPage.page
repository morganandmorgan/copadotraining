<apex:page sidebar="true" controller="ScheduleInvestigatorCtlr" showHeader="true" docType="html-5.0">

    <apex:stylesheet value="{!URLFOR($Resource.Morgan, 'Morgan/css/sumoselect.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Morgan, 'Morgan/css/multiple-select.css')}"/>
    <apex:stylesheet value="//cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css" />
    <apex:stylesheet value="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css"/>
    <apex:stylesheet value="{!URLFOR($Resource.FontAwesome, 'font-awesome-4.6.3/css/font-awesome.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Morgan, 'Morgan/css/scheduleinvestigator.css')}"/>

    <apex:includeScript value="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPI73i0kmEP9lsrjgXXN8knBL8gp91r90"/>
    <apex:includeScript value="{!URLFOR($Resource.Morgan, 'Morgan/scripts/jquerymin.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Morgan, 'Morgan/scripts/jquerysumoselect.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Morgan, 'Morgan/scripts/multiple-select.js')}"/>
    <apex:includescript value="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" />
    <apex:includeScript value="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"/>
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" />
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"/>
    <apex:includeScript value="{!URLFOR($Resource.Morgan, 'Morgan/scripts/scheduleinvestigator.js')}"/>
    <script>
        ScheduleInvestigator.initVisualforceValues({
                signupTimeFullDay: '{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_Full__c))}',
                signupTimeFiveIntakes: '{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_5__c))}',
                signupTimeFourIntakes: '{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_4__c))}',
                signupTimeThreeIntakes: '{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_3__c))}',
                signupTimeTwoIntakes: '{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_2__c))}',
                signupTimeOneIntake: '{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_1__c))}',
                caseTypeOptCatastrophicId: '{!caseTypeSelectOptionCatastrophicIdentifier}',
                getCityOptionsRemoteActionName: '{!$RemoteAction.ScheduleInvestigatorCtlr.getCityOptions}',
                delayedSignupReasonTime: '{!$Setup.ScheduleInvestigatorSettings__c.Delayed_Signup_Reason_Time__c}'
            });
    </script>
    
    <apex:form >
        <!-- Case Detail Section -->
        <apex:pageMessages id="pageMessages"/>
            
        <apex:pageBlock title="Case Detail" >
            
            <apex:pageBlockSection columns="1">
                <apex:inputText value="{!selectedSlot}" id="userSlotSelected" style="display:none"/>
                 
                <apex:pageBlockSectionItem rendered="{!AND(incidentInstance.primaryIntakeClientName != null, incidentInstance.primaryIntakeClientName != '')}">
                    <apex:outputLabel value="Client Name" for="clientName"/>
                    <apex:outputText id="clientName" value="{!incidentInstance.primaryIntakeClientName}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!OR(incidentInstance.primaryIntakeClientName = null, incidentInstance.primaryIntakeClientName = '')}">
                    <apex:outputLabel value="{!$ObjectType.Incident__c.fields.Name.Label}" for="incidentName"/>
                    <apex:outputText id="incidentName" value="{!incidentInstance.incidentName}"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Case Type"/>
                    <apex:repeat value="{!caseTypes}" var="eachCaseType">
                        <apex:outputText value="{!eachCaseType}"/><br/>
                    </apex:repeat>
                </apex:pageBlockSectionItem>
                
            </apex:pageBlockSection>
            
            
        </apex:pageBlock>    


        <!-- Investigation Location Section -->
        <apex:pageBlock title="Investigation Location" id="investigationLocation"  >
        
            <apex:pageBlockSection collapsible="true" columns="2" id="locationSection">
            
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Location" for="locationAddress"/>
                    <apex:selectList id="locationAddress" value="{!selectedLocation}" size="1">
                        <apex:selectOptions value="{!investigationLocationOptions}"/>
                        <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem />
                
                <apex:pageBlockSectionItem >
                    <apex:outputPanel layout="block">
                        <apex:repeat value="{!investigationLocationMap}" var="locationId" >
                            <apex:variable var="addr" value="{!investigationLocationMap[locationId]}"/>
                            <div data-addresskey="{!addr.addressKey}" class="investigationAddress" style="display: none;">
                                <table class="addressTable">
                                    <tr>
                                        <td class="leftColumn"><apex:outputLabel value="Location Address"/></td>
                                        <td class="rightColumn">
                                            <apex:outputText value="{!addr.investigationLocationName}" styleClass="addressName"/><br/>
                                            <apex:outputText value="{!addr.investigationLocationStreet1}" styleClass="addressStreet"/><br/>
                                            <apex:outputText value="{!addr.investigationLocationCity}" styleClass="addressCity"/><br/>
                                            <apex:outputText value="{!addr.investigationLocationState}" styleClass="addressState"/><br/>
                                            <apex:outputText value="{!addr.investigationLocationZipcode}" styleClass="addressZipcode"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </apex:repeat>
                        
                        <div id="ManualAddress" data-addresskey="Add New Location" class="investigationAddress" style="display:none;">
                            
                            <table class="addressTable">
                            
                                <tr>
                                    <td class="leftColumn"></td>
                                    <td class="rightColumn"></td>
                                </tr>
                                
                                <tr>
                                    <td class="leftColumn"><apex:outputLabel value="Location Name" for="manualLocation"/></td>
                                    <td class="rightColumn">
                                        <apex:inputText id="manualLocation" value="{!incidentInstance.manualAddress.investigationLocationName}" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="leftColumn"><apex:outputLabel value="Street Address 1" for="manualStreet1"/></td>
                                    <td class="rightColumn">
                                        <apex:inputText id="manualStreet1" value="{!incidentInstance.manualAddress.investigationLocationStreet1}" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="leftColumn"><apex:outputLabel value="Street Address 2" for="manualStreet2"/></td>
                                    <td class="rightColumn">
                                        <apex:inputText id="manualStreet2" value="{!incidentInstance.manualAddress.investigationLocationStreet2}" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="leftColumn"><apex:outputLabel value="State" for="manualState"/></td>
                                    <td class="rightColumn">
                                        <apex:selectList id="manualState" value="{!incidentInstance.manualAddress.investigationLocationState}" size="1" multiselect="false" onchange="ScheduleInvestigator.getCityOptions();">
                                            <apex:selectOptions value="{!stateSelectOptions}"/>
                                        </apex:selectList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="leftColumn"><apex:outputLabel value="City" for="manualCity"/></td>
                                    <td class="rightColumn">
                                        <apex:inputHidden id="manualCityHidden" value="{!incidentInstance.manualAddress.investigationLocationCity}"/>
                                        <select id="manualCity">
                                            <option value="">--None--</option>
                                        </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="leftColumn"><apex:outputLabel value="Zip Code" for="manualZip"/></td>
                                    <td class="rightColumn">
                                        <apex:inputText id="manualZip" value="{!incidentInstance.manualAddress.investigationLocationZipcode}" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="leftColumn"></td>
                                    <td class="rightColumn">
                                        <apex:commandButton value="Save Address" onclick="ScheduleInvestigator.saveAddressClick(); return false;"/>
                                        <apex:commandButton value="Cancel" oncomplete="ScheduleInvestigator.cancelAddress()"/>
                                        <apex:actionFunction action="{!filterInvestigators}" name="saveAddressAction" oncomplete="ScheduleInvestigator.saveAddressComplete()" reRender="investigators-List, pageMessages"/>
                                    </td>
                                </tr>
                                
                            </table>
                            
                        </div>

                        <div id="ManualDisplayAddress" style="display:none;">
                            <table class="addressTable">
                                <tr>
                                    <td class="leftColumn">Location Address</td>
                                    <td class="rightColumn" id="displayAddress"></td>
                                </tr>
                                <tr>
                                    <td class="leftColumn"></td>
                                    <td class="rightColumn">
                                        <apex:commandButton value="Edit New Location" onclick="ScheduleInvestigator.editAddressClick(); return false;"/>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <div id="mapContainer">
                        <div id="map"></div>
                        <br/>
                        <a target="_blank" id="mapDirections" href="" style="color:blue;">Get Directions</a>
                    </div>
                </apex:pageBlockSectionItem>

            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <!-- Case Matters Section -->
        <apex:pageBlock title="Case Matters">
        
            <div>
                <table id="caseMatters" width="100%">
                    <thead>
                        <tr>
                            <th><input id="checkAllAttendees" type="checkbox"/></th>
                            <th>Client Name</th> 
                            <th>Injured Party Name</th>
                            <th>Status</th>
                            <th>Status Detail</th>
                            <th>Case Type</th>
                        </tr>
                    </thead>
                    
                    <tbody id="attendeeChkbox">
                        <apex:repeat value="{!incidentInstance.attendees}" var="attendee">
                            <tr>
                                <td><apex:inputCheckbox value="{!attendee.selected}" styleClass="individualAttendeeSelection"/></td>
                                <td>{!attendee.clientName}</td>
                                <td>{!attendee.injuredPartyName}</td>
                                <td>{!attendee.status}</td>
                                <td>{!attendee.statusDetail}</td>
                                <td>
                                    <apex:selectList styleClass="caseTypeSelection" value="{!attendee.selectedCaseTypes}" multiselect="true">
                                        <apex:selectOptions value="{!attendee.availableCaseTypeOptions}"/>
                                    </apex:selectList>
                                </td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </div>
            
        </apex:pageBlock>
        
        <!-- Addtional Information Section -->
        <apex:pageBlock title="Additional Information">
        
            <apex:pageBlockSection columns="1">
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Additional Case Notes" for="additionalNotes"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:inputTextarea id="additionalNotes" value="{!additionalNotes}" html-placeholder="Enter any additional notes here" cols="80" rows="10" style="resize:none;"/>
                </apex:pageBlockSectionItem>

            </apex:pageBlockSection>
        </apex:pageBlock>

        <!-- Investigators Requirements Section -->
        <apex:pageBlock title="Investigator Requirements">
            
            <apex:pageBlockSection columns="2">
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Spoken Languages" for="languageId"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Skills" for="skillsList"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:actionRegion >
                        <apex:selectList styleClass="LanguageList" id="languageId" value="{!selectedLanguage}" size="4" multiselect="true" style="width: 9% !important">
                            <apex:selectOptions value="{!languageChoices}"/>
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                        </apex:selectList>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
            
                <apex:pageBlockSectionItem >
                    <apex:actionRegion >
                        <apex:selectList styleClass="SkillList" id="skillsList" value="{!selectedSkill}" size="4" multiselect="true" style="width: 9% !important">
                            <apex:selectOptions value="{!skillChoices}"/>
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                        </apex:selectList>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock title="Scheduled Date And Time">
            <apex:pageBlockSection >
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Scheduled Date" for="scheduledDate"/>
                    <apex:actionRegion >
                        <apex:inputText value="{!scheduledDate}" id="scheduledDate"> 
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages" oncomplete="ScheduleInvestigator.changeScheduledDateCallback();"/>
                        </apex:inputText>
                    </apex:actionRegion>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem labelStyleClass="catastrophicDetail" dataStyleClass="catastrophicDetail">
                    <apex:pageMessage severity="info" strength="1" summary="{!$Label.CatastrophicCaseTimeliness}"/>
                </apex:pageBlockSectionItem>
                
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Time Required" for="reqTimeValue"/>
                    <apex:outputText id="reqTimeValue" value="{!VALUE(TEXT($Setup.ScheduleInvestigatorSettings__c.Signup_Time_1__c))}" styleClass="reqTimeValue"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem labelStyleClass="catastrophicDetail" dataStyleClass="catastrophicDetail">
                    <apex:outputLabel value="Reason for Delayed Signup" for="catastrophicInjuryValueOption"/>
                    <apex:outputPanel layout="block">
                        <div id="delayedSignupRequiredInput">
                            <div id="delayedSignupRequiredBlock"></div>
                            <apex:selectList id="catastrophicInjuryValueOption" value="{!reasonForDelayedSignup}" size="1">
                                <apex:selectOptions value="{!reasonsForDelayedSignupList}"/>
                            </apex:selectList>
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Event Start Time" id="startTimeLabel" style="display:none" for="eventStartTime"/>
                    <div id='StartTimeSelect'></div>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:actionRegion >
                <apex:inputHidden value="{!timeRequired}" id="timeSlotRequired"> 
                    <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                </apex:inputHidden>
            </apex:actionRegion>
        </apex:pageBlock>
        
        
        
        <!-- Investigators Section -->
        <apex:pageBlock title="Investigators">
            <div>
                <span class="filterOverride">
                    <apex:actionRegion >
                        <apex:inputCheckbox value="{!isLocationEnabled}" rendered="{!$Setup.ScheduleInvestigatorPermissions__c.Can_Override_Location__c}">Location
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                        </apex:inputCheckbox>
                    </apex:actionRegion>
                </span>
                <span class="filterOverride">
                    <apex:actionRegion >
                        <apex:inputCheckbox value="{!isLanguagesEnabled}" rendered="{!$Setup.ScheduleInvestigatorPermissions__c.Can_Override_Language__c}">Language
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                        </apex:inputCheckbox>
                    </apex:actionRegion>
                </span>
                <span class="filterOverride">
                    <apex:actionRegion >
                        <apex:inputCheckbox value="{!isSkillEnabled}" rendered="{!$Setup.ScheduleInvestigatorPermissions__c.Can_Override_Skills__c}">Skill
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                        </apex:inputCheckbox>
                    </apex:actionRegion>
                </span>
                <span class="filterOverride">
                    <apex:actionRegion >
                        <apex:inputCheckbox value="{!isTimeEnabled}" rendered="{!$Setup.ScheduleInvestigatorPermissions__c.Can_Override_Time__c}">Time
                            <apex:actionSupport event="onchange" action="{!filterInvestigators}" reRender="investigators-List, pageMessages"/>
                        </apex:inputCheckbox>
                    </apex:actionRegion>
                </span>
            </div>

            <apex:outputPanel layout="block" id="investigators-List">
                
                <table id="investigators-section" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <apex:outputPanel layout="none" rendered="{!NOT(isLocationEnabled)}">
                                <th>Territory</th>
                            </apex:outputPanel>
                            <th>Time from last appt</th>
                            <th>Time to next appt</th>
                            <th class="colInvestigatorTimeslot">Available Timeslot</th>
                            <th>Notifications</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <apex:repeat value="{!territoryUsersAvailableSlots}" var="userId" >
                            <apex:variable var="userAvailSlots" value="{!territoryUsersAvailableSlots[userId]}"/>    
                            <apex:repeat value="{!userAvailSlots}" var="eachSlot">
                                <tr data-hour="{!eachSlot.timeFrameHour}" data-minute="{!eachSlot.timeFrameMinute}">
                                    <td><input type="radio" name="selectedInv" id="selectedInv_{!eachSlot.slotNumber}" value="{!userId}-{!eachSlot.timeFrameHour}.{!eachSlot.timeFrameMinute}" onClick="javascript:ScheduleInvestigator.investigatorSelected('{!eachSlot.timeFrameHour}.{!eachSlot.timeFrameMinute}')"/></td>
                                    
                                    <td>{!eachSlot.Investigator.Name}</td>

                                    <apex:outputPanel layout="none" rendered="{!NOT(isLocationEnabled)}">
                                        <td>{!eachSlot.Investigator.Territory__c}</td>
                                    </apex:outputPanel>
                                    
                                    <td>
                                        <span class="distance" data-previousaddress="{!eachSlot.addressOfPriorAppointment}" data-nextaddress="{!investigationLocationMap[selectedLocation].investigationLocationStreet1}, {!investigationLocationMap[selectedLocation].investigationLocationCity}, {!investigationLocationMap[selectedLocation].investigationLocationState}, {!investigationLocationMap[selectedLocation].investigationLocationZipcode}"></span>
                                    </td>
                                    
                                    <td>
                                        <span class="distance" data-previousaddress="{!investigationLocationMap[selectedLocation].investigationLocationStreet1}, {!investigationLocationMap[selectedLocation].investigationLocationCity}, {!investigationLocationMap[selectedLocation].investigationLocationState}, {!investigationLocationMap[selectedLocation].investigationLocationZipcode}" data-nextaddress="{!eachSlot.addressOfNextAppointment}"></span>
                                    </td>
                                    
                                    <td data-order="{!eachSlot.timeFrameHour}.{!eachSlot.timeFrameMinute}">{!eachSlot.timeFrameHour}.{!eachSlot.timeFrameMinute}</td>
                                    
                                    <td>
                                        <apex:variable var="notification" value="{!userNotifications[userId]}"/>
                                        <apex:outputPanel layout="block" rendered="{!OR(notification.isNotificationEnabled, AND(eachSlot.hasConflict, NOT(isTimeEnabled)))}" style="white-space: pre;">
                                            <apex:outputText value="Doesn't meet the following requirements"/>
                                            <br/>
                                            <apex:outputText value="{!notification.locationNotification & notification.languageNotification & notification.skillNotification}" rendered="{!notification.isNotificationEnabled}"/>
                                            <apex:outputText value="-time" rendered="{!AND(eachSlot.hasConflict, NOT(isTimeEnabled))}"/>
                                        </apex:outputPanel>
                                    </td>
                                </tr>    
                            </apex:repeat>
                            
                        </apex:repeat>
                       
                    </tbody>
                </table>
                <script>
                    ScheduleInvestigator.applyDataTable();
                </script>
            </apex:outputPanel>
            <apex:pageBlockButtons location="bottom">
                <apex:outputPanel >
                    <button type="button" class="btn schedulerAction" onclick="ScheduleInvestigator.scheduleClick(this); return false;" id="scheduleEvent">Schedule</button>
                    <button type="button" class="btn schedulerAction" onclick="ScheduleInvestigator.cancelClick(this); return false;" id="cancelEvent">Cancel</button>

                    <apex:actionFunction name="saveEvent" action="{!saveEvent}" reRender="pageMessages" oncomplete="ScheduleInvestigator.saveEventCallback();"/>
                    <apex:actionFunction name="cancel" action="{!cancelSchedule}" immediate="true"/>
                </apex:outputPanel>
            </apex:pageBlockButtons>
        </apex:pageBlock>
    </apex:form>
    
</apex:page>