<apex:page standardController="Audit_Rule__c" extensions="AuditRuleToolController" lightningStylesheets="true">
<apex:outputPanel rendered="{!(userTheme!='Theme3')}">
    <apex:slds />
</apex:outputPanel>

<apex:form >

    <apex:pageMessages escape="false"/>

    <apex:pageBlock id="RuleDetails" title="Audit Rule">
        <apex:pageBlockSection id="auditRuleSection">
            <!-- html-disabled requires a parameter, but it is ignored and alwasy true -->
            <apex:inputField value="{!theRule.auditRule.name}" style="width:50ch;" rendered="{!(!hasFlags)}"/>
            <apex:inputField value="{!theRule.auditRule.name}" style="width:50ch;" rendered="{!hasFlags}" html-disabled="html ignores this"/>
            <apex:inputField value="{!theRule.auditRule.isActive__c}"/>

            <apex:inputField value="{!theRule.auditRule.Description__c}" style="width:80em; height:4em;" rendered="{!(!hasFlags)}"/>
            <apex:inputField value="{!theRule.auditRule.Description__c}" style="width:80em; height:4em;" rendered="{!hasFlags}" html-disabled="html ignores this"/>
            <apex:inputField value="{!theRule.auditRule.Points__c}" style="width:6ch;" rendered="{!(!hasFlags)}"/>
            <apex:inputField value="{!theRule.auditRule.Points__c}" style="width:6ch;" rendered="{!hasFlags}" html-disabled="but visualforce requires it"/>

            <apex:selectList value="{!theRule.auditRule.Object__c}" size="1" disabled="{!hasFlags}">
                <apex:actionSupport event="onchange" reRender="dateTimeFields,userFields,chooseFields"/>
                <apex:selectOptions value="{!baseObjects}"/>
            </apex:selectList>
            <apex:selectList id="userFields" value="{!theRule.auditRule.Related_Flagged_User__c}" size="1" disabled="{!hasFlags}">
                <apex:selectOptions value="{!userFields}"/>
            </apex:selectList>

            <apex:inputField value="{!theRule.auditRule.Rule_Type__c}" rendered="{!(!hasFlags)}">
                <apex:actionSupport event="onchange" reRender="auditRuleSection"/>
            </apex:inputField>
            <apex:inputField value="{!theRule.auditRule.Rule_Type__c}" rendered="{!hasFlags}" html-disabled="true"/>
            &nbsp;

            <apex:selectList id="dateTimeFields" value="{!theRule.auditRule.Custom_Scheduled_Date_Field__c}" size="1" rendered="{!(theRule.auditRule.Rule_Type__c=='Scheduled')}" disabled="{!hasFlags}">
                <apex:selectOptions value="{!dateTimeFields}"/>
            </apex:selectList>
            <apex:inputField value="{!theRule.auditRule.Days__c}" style="width:4ch;" rendered="{!(theRule.auditRule.Rule_Type__c=='Scheduled' && !hasFlags)}"/>
            <apex:inputField value="{!theRule.auditRule.Days__c}" style="width:4ch;" rendered="{!(theRule.auditRule.Rule_Type__c=='Scheduled' && hasFlags)}" html-disabled="true"/>

            <apex:inputField value="{!theRule.auditRule.Scheduled_Flag_Notification__c}" rendered="{!theRule.auditRule.Rule_Type__c=='Scheduled'}">
                <apex:actionSupport event="onchange" rerender="auditRuleSection"/>                
            </apex:inputField>

            <apex:inputField value="{!theRule.auditRule.Scheduled_Flag_Notification_Days__c}" style="width:4ch;" rendered="{!(theRule.auditRule.Rule_Type__c=='Scheduled' && theRule.auditRule.Scheduled_Flag_Notification__c==true)}"/>

            <apex:inputField value="{!theRule.auditRule.Scheduled_Flag_Notification_Message__c}" style="width:60em; height:4em;" rendered="{!(theRule.auditRule.Rule_Type__c=='Scheduled' && theRule.auditRule.Scheduled_Flag_Notification__c==true)}"/>
        </apex:pageBlockSection>    

        <br/>
        
        <apex:pageBlockSection >
            <apex:repeat value="{!dynamicFields}" var="f">
                <apex:inputField value="{!theRule.auditRule[f.fieldPath]}" required="{!OR(f.required, f.dbrequired)}" rendered="{!(!hasFlags)}"/>
                <apex:inputField value="{!theRule.auditRule[f.fieldPath]}" required="{!OR(f.required, f.dbrequired)}" rendered="{!hasFlags}" html-disabled="true"/>
            </apex:repeat>
        </apex:pageBlockSection>

        <hr/>

        <apex:outputPanel id="chooseFields" rendered="{!(!hasFlags)}">
        <table>
            <tr>
                <td>
                    <apex:outputPanel rendered="{!(theRule.auditRule.Object__c!='')}">
                        <b>{!theRule.auditRule.Object__c}.</b>&nbsp;
                        <apex:selectList value="{!baseField}" size="1">
                            <apex:actionSupport event="onchange" action="{!selectBaseField}" reRender="chooseFields"/>
                            <apex:selectOptions value="{!baseFields}"/>
                        </apex:selectList>&nbsp;
                    </apex:outputPanel>
                    <br/>
                    <apex:outputPanel rendered="{!(baseRelatedField!='')}">
                        <div style="padding: 5px 5px 5px 0px;">
                            <b>{!baseRelatedField}.</b>&nbsp;
                            <apex:selectList value="{!relatedField}" size="1">
                                <apex:actionSupport event="onchange" action="{!selectRelatedField}" reRender="chooseFields"/>
                                <apex:selectOptions value="{!relatedFields}"/>
                            </apex:selectList>&nbsp;
                        </div>
                    </apex:outputPanel>
                </td>
                <td>
                    <apex:outputPanel rendered="{!(theRule.auditRule.Object__c!='')}">
                        <apex:inputText value="{!baseFieldRendered}" size="60"/>&nbsp;
                        <apex:commandButton action="{!copyBaseFieldToNextArgument}" value="Copy to next argument"/>
                    </apex:outputPanel>
                    <br/>
                    <apex:outputPanel rendered="{!(baseRelatedField!='')}">
                        <div style="padding-top: 2px;">
                            <apex:inputText value="{!relatedFieldRendered}" size="60"/>&nbsp;
                            <apex:commandButton action="{!copyRelatedFieldToNextArgument}" value="Copy to next argument"/>
                        </div>
                    </apex:outputPanel>
                </td>
            </tr>
        </table>
        </apex:outputPanel>

        <apex:pageBlockSection id="expressionBlock" columns="1">
            <apex:variable var="index" value="{!0}"/>
            <apex:pageBlockTable value="{!theRule.expressions}" var="expression">
                <apex:column headerValue="Argument 1">
                    <apex:inputText value="{!expression.argument1}" size="40" disabled="{!hasFlags}"/>
                </apex:column>
                <apex:column headerValue="Operator">
                    <apex:selectList value="{!expression.operator}" size="1" disabled="{!hasFlags}">
                        <apex:selectOption itemValue="" itemLabel=""/>
                        <apex:selectOption itemValue="=" itemLabel="="/>
                        <apex:selectOption itemValue="<>" itemLabel="<>"/>
                        <apex:selectOption itemValue=">" itemLabel=">"/>
                        <apex:selectOption itemValue=">=" itemLabel=">="/>
                        <apex:selectOption itemValue="<" itemLabel="<"/>
                        <apex:selectOption itemValue="<=" itemLabel="<="/>
                        <apex:selectOption itemValue="contains" itemLabel="contains"/>
                        <apex:selectOption itemValue="does_not_contain" itemLabel="does_not_contain"/>
                        <apex:selectOption itemValue="contains_only" itemLabel="contains_only"/>
                    </apex:selectList> 
                </apex:column>
                <apex:column headerValue="Percent">
                    <apex:inputText value="{!expression.percent}" size="3" rendered="{!(!hasFlags)}"/>
                    <apex:outputPanel rendered="{!hasFlags}">
                        <input type="text" value="{!expression.percent}" size="3" disabled="true"/>
                    </apex:outputPanel>
                </apex:column>
                <apex:column headerValue="Argument 2">
                    <apex:inputText value="{!expression.argument2}" size="40" disabled="{!hasFlags}"/>
                </apex:column>
                <apex:column headerValue="Date Modifier">
                    <apex:selectList value="{!expression.dateModifier}" size="1" disabled="{!hasFlags}">
                        <apex:selectOption itemValue="" itemLabel="" itemEscaped="false"/>
                        <apex:selectOption itemValue="days_ago" itemLabel="days_ago" itemEscaped="false"/>
                        <apex:selectOption itemValue="months_ago" itemLabel="months_ago" itemEscaped="false"/>
                        <apex:selectOption itemValue="years_ago" itemLabel="years_ago" itemEscaped="false"/>
                    </apex:selectList> 
                </apex:column>
                <apex:column headerValue="Delete">
                    <apex:commandButton action="{!deleteExpression}" value="  -  " reRender="expressionBlock" disabled="{!hasFlags}">
                        <apex:param name="delId" value="{!index}" assignTo="{!deleteIndex}"/>
                    </apex:commandButton>
                    <apex:variable var="index" value="{!index+1}"/>
                </apex:column>
            </apex:pageBlockTable>

            <apex:outputPanel style="float:right;">
                <apex:commandButton action="{!addExpression}" value="Add Expression" reRender="expressionBlock" disabled="{!hasFlags}"/>
            </apex:outputPanel>

        </apex:pageBlockSection>

        <table width="100%">
            <tr>
                <td align="left">
                    <apex:commandButton action="{!testRule}" value="Test"/>&nbsp;
                    <apex:commandButton action="{!saveRule}" value="Save"/>
                </td>
                <td align="right"><apex:commandButton action="{!cloneRule}" value="Clone" disabled="{!(theRule.auditRule.id='')}"/></td>
            </tr>
        </table>

    </apex:pageBlock>

    <br/>

    <apex:pageBlock title="AuditFlagScheduled">
        <p>Apex class that will process any flags scheduled for today.</p>
        <apex:commandButton action="{!scheduleJob}"  value="Schedule Job"/>
    </apex:pageBlock>

</apex:form>
</apex:page>