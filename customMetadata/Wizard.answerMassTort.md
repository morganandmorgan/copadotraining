<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerMassTort</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Mass Tort</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransferToQueue5741&quot;],
&quot;tags&quot;:[&quot;Drugs&quot;,&quot;Medications&quot;,&quot;Medical Devices&quot;,&quot;9/11&quot;,&quot;Terrorist Attack&quot;,&quot;Porter Ranch&quot;,&quot;Mosaic Fertilizer&quot;,&quot;Energy Drinks&quot;,&quot;Breast Implants&quot;,&quot;Dicamba&quot;,&quot;Round Up&quot;,&quot;Weed Killer&quot;, &quot;talcum&quot;, &quot;Risperdal&quot;, &quot;Abilify&quot;, &quot;Xarelto&quot;, &quot;implants&quot;,&quot;essure&quot;,&quot;bair Hugger&quot;,&quot;hip implant&quot;, &quot;knee implant&quot;,&quot;invokana&quot;,&quot;Low T&quot;,&quot;VW recalls&quot;,&quot;Hernia Mesh&quot;,&quot;Surgical mesh&quot;,&quot;Actos&quot;,&quot;Nexium&quot;,&quot;Protonix&quot;,&quot;Prevacid&quot;,&quot;Zofran&quot;,&quot;Mesh&quot;,&quot;3M Earplugs&quot;, &quot;3M&quot;,&quot;Earplugs&quot;,&quot;Prilosec&quot;,&quot;Mesh&quot;,&quot;medication&quot;,&quot;knee replacement&quot;,&quot;hip replacement&quot;]}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
