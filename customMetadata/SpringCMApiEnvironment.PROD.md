<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PROD</label>
    <protected>false</protected>
    <values>
        <field>AuthEndpoint__c</field>
        <value xsi:type="xsd:string">https://auth.springcm.com/api/v201606/</value>
    </values>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">6c2e12b2-b053-4b06-aea8-776d51934f55</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">f8e78c7276704e9bb3e810dc24bfa6d0t6cLMSHkD5zLrVQRCevcrSvXEbbIWbFKNzpCUpMwbkVJByfrsxg1uqhbLEHv08Olzo81qO7WS9BH6lWQ5tZi4OahSN1Pib5c</value>
    </values>
</CustomMetadata>
