<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>questionWhichStateDoYouLiveIn</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Which state do you live in?</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{
    &quot;childTokens&quot;: [
        &quot;answerStateFlorida&quot;,
        &quot;answerStateGeorgia&quot;,
        &quot;answerStateNorthCarolina&quot;,
        &quot;answerStateSouthCarolina&quot;,
        &quot;answerStateVirginia&quot;,
        &quot;answerStateNewYork&quot;
    ],
    &quot;actionTokens&quot;: [
        &quot;actionGenerateRecordForQA&quot;
    ]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_QuestionModel</value>
    </values>
</CustomMetadata>
