<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Admins</label>
    <protected>false</protected>
    <values>
        <field>Profile_Name__c</field>
        <value xsi:type="xsd:string">M&amp;M System Administrator Modified</value>
    </values>
    <values>
        <field>Redirect_URL__c</field>
        <value xsi:type="xsd:string">/apex/liveivr</value>
    </values>
    <values>
        <field>Role_Name__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
