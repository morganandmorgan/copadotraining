<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CTM_Injury_dot_com</label>
    <protected>false</protected>
    <values>
        <field>CTM_Account_Id__c</field>
        <value xsi:type="xsd:string">126689</value>
    </values>
    <values>
        <field>CTM_Auth_Named_Credential__c</field>
        <value xsi:type="xsd:string">CTM_Injury</value>
    </values>
    <values>
        <field>Domain_Value__c</field>
        <value xsi:type="xsd:string">www.injury.com</value>
    </values>
</CustomMetadata>
