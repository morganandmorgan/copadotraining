<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Morgan &amp; Morgan Kentucky PLLC</label>
    <protected>false</protected>
    <values>
        <field>Company_Name__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan Kentucky PLLC</value>
    </values>
    <values>
        <field>Interest_Rate__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
</CustomMetadata>
