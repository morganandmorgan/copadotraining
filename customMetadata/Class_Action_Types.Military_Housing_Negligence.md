<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Military Housing Negligence</label>
    <protected>false</protected>
    <values>
        <field>Assigned_Attorney__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Case_Type__c</field>
        <value xsi:type="xsd:string">All Potential</value>
    </values>
    <values>
        <field>CA_Flow__c</field>
        <value xsi:type="xsd:string">All Potential</value>
    </values>
    <values>
        <field>CA_Member_Status__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Name__c</field>
        <value xsi:type="xsd:string">Military Housing Negligence</value>
    </values>
    <values>
        <field>CA_Q1_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Q2_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Q3_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Q4_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Q5_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Q6_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CA_Status__c</field>
        <value xsi:type="xsd:string">Under Review</value>
    </values>
    <values>
        <field>CQC_Bypass__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Case_Manager__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>PL_Group__c</field>
        <value xsi:type="xsd:string">Group 3</value>
    </values>
</CustomMetadata>
