<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>questionLIVR2</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">List of answers for:  What is the reason for the call</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">What is the reason for the call?</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;childTokens&quot;:[&quot;answerAccounting_Calls&quot;,
&quot;answerADA_American_Disability_Act&quot;, 
&quot;answerMesothelioma_Asbestos&quot;, 
&quot;answerBusiness_Law&quot;, 
&quot;answerCallsForTimMorgan&quot;, 
&quot;answerClient_Satisfaction&quot;, 
&quot;answerComplex_Litigation&quot;, 
&quot;answerCompliments&quot;, 
&quot;answerCriminal&quot;, 
&quot;answerEmployment_Law&quot;, 
&quot;answerEscalation_Calls&quot;, 
&quot;answerForfeiture&quot;, 
&quot;answerHurricane&quot;, 
&quot;answerImmigration&quot;, 
&quot;answerInsurance_Dispute&quot;, 
&quot;answerJBMGovernor&quot;, 
&quot;answerInternet_Leads&quot;, 
&quot;answerLabor&quot;, 
&quot;answerLookforEmployment&quot;, 
&quot;answerMarketing_Calls&quot;, 
&quot;answerMassTort&quot;, 
&quot;answerMediaCalls&quot;, 
&quot;answerCast_Iron_Pipes&quot;, 
&quot;answerCivil_Rights&quot;, 
&quot;answerFCRA&quot;, 
&quot;answerMedical_Malpractice&quot;, 
&quot;answerMarijuana&quot;, 
&quot;answerMesothelioma_Asbestos&quot;, 
&quot;answerMikeReese&quot;, 
&quot;answerNursing_Home&quot;, 
&quot;answerPersonal_Injury_Cases&quot;, 
&quot;answerPremise_Liability_Cases&quot;, 
&quot;answerProduct_Liability&quot;, 
&quot;answerSocial_Security&quot;, 
&quot;answerReferrals&quot;, 
&quot;answerSellProduct&quot;, 
&quot;answerSSRobertHiggens&quot;, 
&quot;answerSSCollections&quot;, 
&quot;answerSSScottBates&quot;,&quot;answerTCPA&quot;, 
&quot;answertrafficking&quot;, 
&quot;answerTCPA&quot;, 
&quot;answerVA_or_Veteran_s_Benefits_Agent_Ora&quot;, 
&quot;answerWarranty&quot;, 
&quot;answerWhistleblower&quot;, 
&quot;answerWorkers_Compensation&quot;, 
&quot;answerFoodPoisoning2&quot;, 
&quot;answerAutoAccident2&quot;,
&quot;answerNegligentSecurity2&quot;, 
&quot;answerGeneralInjury2&quot;,
&quot;answerSlipandFall2&quot;, 
&quot;answerTakata2&quot;,
&quot;answerAnimalIncident2&quot;],
&quot;actionTokens&quot;:[&quot;actionGenerateRecordForQA&quot;],
&quot;questionType&quot;:&quot;typeahead&quot;}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_QuestionModel</value>
    </values>
</CustomMetadata>
