<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Labor Default Mail-HF</label>
    <protected>true</protected>
    <values>
        <field>Default_Task_Owner_Id__c</field>
        <value xsi:type="xsd:string">005o0000003UsO7</value>
    </values>
    <values>
        <field>Handling_Firm__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan</value>
    </values>
    <values>
        <field>Intake_Record_Type_API_Name__c</field>
        <value xsi:type="xsd:string">Labor</value>
    </values>
</CustomMetadata>
