<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prestonsburg - Criminal</label>
    <protected>false</protected>
    <values>
        <field>Assigned_Office_Location__c</field>
        <value xsi:type="xsd:string">Prestonsburg</value>
    </values>
    <values>
        <field>Assigned_to_MM_Business__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan Kentucky PLLC</value>
    </values>
    <values>
        <field>Case_Type__c</field>
        <value xsi:type="xsd:string">Criminal</value>
    </values>
    <values>
        <field>Matter_Plan__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Orbit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parent_Office_Name__c</field>
        <value xsi:type="xsd:string">Lexington</value>
    </values>
</CustomMetadata>
