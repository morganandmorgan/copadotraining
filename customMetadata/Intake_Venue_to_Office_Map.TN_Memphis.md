<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TN - Memphis</label>
    <protected>false</protected>
    <values>
        <field>Case_Type_Suppression__c</field>
        <value xsi:type="xsd:string">Cast Iron Pipes</value>
    </values>
    <values>
        <field>Office_Account_Id__c</field>
        <value xsi:type="xsd:string">001o000000tkgYF</value>
    </values>
    <values>
        <field>Venue__c</field>
        <value xsi:type="xsd:string">TN - Memphis</value>
    </values>
</CustomMetadata>
