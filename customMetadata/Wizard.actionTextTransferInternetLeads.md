<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferInternetLeads</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Internet Leads</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">x5736
&lt;br/&gt;
&lt;br/&gt;
Callers stating that they received a call from one of our agents regarding a new case or a &lt;b&gt;lead&lt;/b&gt; they submitted:
&lt;br/&gt;
&lt;br/&gt;
First, look them up in SF.
&lt;br/&gt;
&lt;br/&gt;
If their Status/ Status Detail says: &quot;lead-Needs to be Qualified&quot; or &quot;Lead-Attorney Requested more Information&quot;- tx to x5736 from 8am-6pm M-F. 
&lt;br/&gt;
&lt;br/&gt;
From hours of 8PM-10PM, 7 days a week: If the call back is regarding ONLY an existing Financial case and status is Lead-Attorney Requested more information, WARM transfer to IST x5737.
&lt;br/&gt;
&lt;b&gt;&lt;u&gt;For any other Statuses, send to the proper Department&lt;/u&gt;&lt;/b&gt;
&lt;br/&gt;
&lt;br/&gt;
&lt;b&gt; REMEMBER: If the case status is Converted, ALWAYS transfer to the Case staff assigned ONLY &lt;/b&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
