<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerAutoAccident2</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:type="xsd:string">Automobile Accident</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Auto Accident (Testing)</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:type="xsd:string">Personal Injury</value>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionFlowForTesting&quot;],
&quot;tags&quot;:[&quot;Automobile&quot;,&quot;Accident&quot;,&quot;injuries&quot;,&quot;injury&quot;,&quot;auto accident&quot;,&quot;car accident&quot;,&quot;hit and run&quot;,&quot;motorcycle&quot;,&quot;pedestrian&quot;,&quot;bus&quot;,&quot;cyclist&quot;,&quot;bicycle&quot;,&quot;truck&quot;,&quot;semi&quot;,&quot;car&quot;,&quot;burn&quot;]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
