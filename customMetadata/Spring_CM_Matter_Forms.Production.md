<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Production</label>
    <protected>false</protected>
    <values>
        <field>APIDownlaod__c</field>
        <value xsi:type="xsd:string">https://apidownloadna21.springcm.com/v201411/documents/</value>
    </values>
    <values>
        <field>Advanced_Search__c</field>
        <value xsi:type="xsd:string">https://na21.springcm.com/atlas/Search/Search.aspx?aid=19112&amp;nav=false</value>
    </values>
    <values>
        <field>Multi_Matter_Upload_URL__c</field>
        <value xsi:type="xsd:string">https://na21.springcm.com/atlas/Forms/UpdateFormDoc.aspx?aid=19112&amp;FormUid=4f9b5fad-782d-e811-9c15-ac162d889bd0</value>
    </values>
    <values>
        <field>Multi_Merge_URL__c</field>
        <value xsi:type="xsd:string">https://na21.springcm.com/atlas/Forms/UpdateFormDoc.aspx?aid=19112&amp;FormUid=b7f561c8-172e-e811-9c15-ac162d889bd0</value>
    </values>
    <values>
        <field>UploadUrl__c</field>
        <value xsi:type="xsd:string">https://na21.springcm.com/atlas/Forms/UpdateFormDoc.aspx?aid=19112&amp;FormUid=9e79fbd6-701f-e911-9c1d-ac162d889bd1</value>
    </values>
</CustomMetadata>
