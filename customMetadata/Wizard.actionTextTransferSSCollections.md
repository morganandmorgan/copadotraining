<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferSSCollections</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Social Security Collections Department</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;u&gt;&lt;b&gt;Social Security Collections Department Call Instructions:&lt;/b&gt;&lt;/u&gt;
&lt;br/&gt;
&lt;br/&gt;

&lt;li&gt;Caller states they have received a bill from our SS department and they want to make a payment, transfer to x5756&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;If the client is callling to dispute the charges, transfer to SS LIVR English x5744  Spanish x5732&quot;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
