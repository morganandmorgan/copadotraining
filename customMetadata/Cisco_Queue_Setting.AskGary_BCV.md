<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AskGary - BCV</label>
    <protected>true</protected>
    <values>
        <field>Cisco_Queue__c</field>
        <value xsi:type="xsd:string">5708</value>
    </values>
    <values>
        <field>County__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>GeneratingAttorney__c</field>
        <value xsi:type="xsd:string">bvigness@forthepeople.com</value>
    </values>
    <values>
        <field>Handling_Firm__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan</value>
    </values>
    <values>
        <field>Marketing_Source__c</field>
        <value xsi:type="xsd:string">AskGary - BCV</value>
    </values>
    <values>
        <field>Venue__c</field>
        <value xsi:type="xsd:string">FL - Fort Myers</value>
    </values>
</CustomMetadata>
