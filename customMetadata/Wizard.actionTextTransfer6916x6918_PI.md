<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransfer6916x6918 PI</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">ADA; Meso; Med Mal; Nursing; PI; Premise; Products; Workers Comp</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English - x6916  Spanish - x6918
&lt;br/&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Automobile Accident-&lt;/b&gt; Injuries caused by a motorized vehicle.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Bed Bugs-&lt;/b&gt; Injuries sustained by a bloodsucking bug. These bugs are usually found in mattresses in hotels.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Children&apos;s Rights-&lt;/b&gt; Injuries to a minor while they were in supervised activity (school, camp, day care, etc.).&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Fire Injury-&lt;/b&gt; Burn injuries that occurred n buildings, homes, apartments. &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Food Poisoning-&lt;/b&gt; Injuries sustained by food eaten in any place/location/restaurant etc.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Maritime/Admiralty-&lt;/b&gt; Injuries on or near a body of water from a jet ski, on a dock, while being a passenger in a cruise ship or while working inside a cruise ship, etc.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Rape &amp; Molestation-&lt;/b&gt; Injuries of sexual nature to an adult or a child. &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Salon-&lt;/b&gt; Injuries caused by eyebrow waxing, pedicure or manicure, etc. &lt;/li&gt;

An injury caused by some type of negligence or unsafe, or defective condition on someone&apos;s property or at a place of business.&lt;br/&gt;&lt;br/&gt;
&lt;b&gt;Animal Incident- &lt;/b&gt;Injuries caused by an animal such as a dog, cat, zoo/farm animal or any domestic animal.&lt;br/&gt;
&lt;b&gt;General Injury-&lt;/b&gt; An injury caused by a defective condition on the premises such as: falling objects, injuries while on a train or aircraft etc. 
&lt;b&gt;Negligent Security- &lt;/b&gt;Direct or indirect act of aggression which causes injury by failing to provide security to protect against crime likely to occur on the premises.&lt;br/&gt;
&lt;b&gt;Slip &amp; Fall-&lt;/b&gt; Injuries caused by a slippery object or substance not properly handled by a liable party.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
