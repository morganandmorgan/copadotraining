<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Settings</label>
    <protected>false</protected>
    <values>
        <field>Insert_Endpoint__c</field>
        <value xsi:type="xsd:string">https://www.virtualacd.biz/intelliqueue/system/listloader/ListLoader.cfm</value>
    </values>
    <values>
        <field>Remove_Endpoint__c</field>
        <value xsi:type="xsd:string">https://www.virtualacd.biz/intelliqueue/system/listloader/LeadRemove.cfm</value>
    </values>
    <values>
        <field>Security_Key__c</field>
        <value xsi:type="xsd:string">950c42063573c03bf87b1e4c11abe3eb</value>
    </values>
</CustomMetadata>
