<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerProduct Liability</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Product Liability</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer6916x6918_Product_Liab&quot;],
&quot;tags&quot;:[&quot;defective product&quot;,&quot;injuries due to a defective product&quot;,&quot;manufacturing defects&quot;,&quot;design defects&quot;,&quot;defects&quot;,&quot;failure to warn&quot;,&quot;warning label &quot;,&quot;injury&quot;,&quot;product fails&quot;,&quot;product breaks&quot;,&quot;distributor&quot;,&quot;product recall&quot;,&quot;e-cig cases&quot;,&quot;samsung cases&quot;,&quot;ikea dresser&quot;,&quot;sorin stockert 3T heater cooler&quot;,&quot;heater cooler device&quot;,&quot;made in china&quot;,&quot;Carbon monoxide poisoning&quot;,&quot;vehicle recalls&quot;,&quot;airbags&quot;,&quot;airbag&quot;,&quot;air bag&quot;,&quot;air bags&quot;,&quot;takata&quot;,&quot;continental&quot;,&quot;continental automative&quot;,&quot;car seats&quot;,&quot;tire blowouts&quot;,&quot;seat belt defects&quot;,&quot;seat collapse defects&quot;,&quot;x-lite guardrail injuries&quot;,&quot;guardrail&quot;,&quot;vehicle fire cases&quot;,&quot;roll over cases&quot;,&quot;product malfunction&quot;,&quot;malfunction&quot;]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
