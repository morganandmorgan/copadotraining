<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransfer6967 Med Mal</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">ADA; Meso; Med Mal; Nursing; PI; Premise; Products; Workers Comp</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;li&gt;&lt;b&gt;Medical-&lt;/b&gt; Injuries caused by the negligence of a doctor, hospital staff or medical professional (home health aide, licensed health provider, RN, LPN, EMT/ Paramedic, Pharmacist, etc.). &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Dental- &lt;/b&gt;Injuries due to negligence from a dentist or orthodontist.&lt;/li&gt;
&lt;br/&gt;
&lt;b&gt;Check the Med Mal queue to see if there are agents on Idle. If so, send to x6967 from 8am-8pm Mon.-Sat. and Sundays 8am-6pm. &lt;/b&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
