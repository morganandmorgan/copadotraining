<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Does User Have CCC Related Profile?</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Does the user have one of the CCC related profiles?</value>
    </values>
    <values>
        <field>RuleDefinition__c</field>
        <value xsi:type="xsd:string">{ 
&quot;segmentList&quot; : [ { 
&quot;ruleType&quot; : &quot;mmbrms_UserInProfilesSpecified&quot;, 
&quot;ruleSetupJson&quot; : &quot;{\&quot;profilesSpecifiedList\&quot; : [ \&quot;M&amp;M CCC\&quot;,\&quot;M&amp;M Case Processor\&quot;]}&quot; 
} ], 
&quot;defaultResponse&quot; : true 
}</value>
    </values>
</CustomMetadata>
