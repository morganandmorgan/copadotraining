<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CQC Account</label>
    <protected>false</protected>
    <values>
        <field>App__c</field>
        <value xsi:type="xsd:string">CQC</value>
    </values>
    <values>
        <field>Target_Object_Fields__c</field>
        <value xsi:type="xsd:string">firstname,
lastname,
billingcity,
billingstate,
description,
nps_comment__pc,
email_indexed__c,
sub_type__c,
toggle_flag__c,
additional_contact_phone__c,
personassistantphone,
personhomephone,
personmobilephone,
phone,
personotherphone,
additional_business_phone__c,
cp_legacyid_legal_entity_sk__c,
cp_legacyid_office_sk__c,
date_of_marriage__c,
date_of_separation__c,
account_name_dba__c,
address_formatted__c,
c2g__averagedaystopay__c,
c2g__codaaccountspayablecontrol__c,
c2g__codaaccountsreceivablecontrol__c,
c2g__codaaccounttradingcurrency__c,
c2g__codaallowdeleteinuse__c,
c2g__codabankaccountname__c,
c2g__codabankaccountnumber__c,
c2g__codabankaccountreference__c,
c2g__codabankcity__c,
c2g__codabankcountry__c,
c2g__codabankfax__c,
c2g__codabankibannumber__c,
c2g__codabankname__c,
c2g__codabankphone__c,
c2g__codabanksortcode__c,
c2g__codabankstateprovince__c,
c2g__codabankstreet__c,
c2g__codabankswiftnumber__c,
c2g__codabankzippostalcode__c,
c2g__codabasedate1__c,
c2g__codabasedate2__c,
c2g__codabasedate3__c,
c2g__codabasedate4__c,
c2g__codabillingaddressisvalid__c,
c2g__codabillingmethod__c,
c2g__codacreditagency__c,
c2g__codacreditlimit__c,
c2g__codacreditlimitenabled__c,
c2g__codacreditlimitreviewed__c,
c2g__codacreditmanager__c,
c2g__codacreditrating__c,
c2g__codacreditratingreviewed__c,
c2g__codacreditreference__c,
c2g__codacreditstatus__c,
c2g__codadaysoffset1__c,
c2g__codadaysoffset2__c,
c2g__codadaysoffset3__c,
c2g__codadaysoffset4__c,
c2g__codadefaultexpenseaccount__c,
c2g__codadescription1__c,
c2g__codadescription2__c,
c2g__codadescription3__c,
c2g__codadescription4__c,
c2g__codadimension1__c,
c2g__codadimension2__c,
c2g__codadimension3__c,
c2g__codadimension4__c,
c2g__codadiscount1__c,
c2g__codadiscount2__c,
c2g__codadiscount3__c,
c2g__codadiscount4__c,
c2g__codaeccountrycode__c,
c2g__codaentityusecode__c,
c2g__codaexemptioncertificate__c,
c2g__codaexternalid__c,
c2g__codafederallyreportable1099__c,
c2g__codafinancecontact__c,
c2g__codaincometaxtype__c,
c2g__codainputvatcode__c,
c2g__codaintercompanyaccount__c,
c2g__codainvoiceemail__c,
c2g__codamergeid__c,
c2g__codaoutputvatcode__c,
c2g__codapaymentmethod__c,
c2g__codareportingcode__c,
c2g__codasalestaxstatus__c,
c2g__codashippingaddressisvalid__c,
c2g__codataxcalculationmethod__c,
c2g__codataxcode1__c,
c2g__codataxcode2__c,
c2g__codataxcode3__c,
c2g__codataxpayeridentificationnumber__c,
c2g__codaunitofwork__c,
c2g__codavalidatedbillingcity__c,
c2g__codavalidatedbillingcountry__c,
c2g__codavalidatedbillingpostcode__c,
c2g__codavalidatedbillingstate__c,
c2g__codavalidatedbillingstreet__c,
c2g__codavalidatedshippingcity__c,
c2g__codavalidatedshippingcountry__c,
c2g__codavalidatedshippingpostcode__c,
c2g__codavalidatedshippingstate__c,
c2g__codavalidatedshippingstreet__c,
c2g__codavatregistrationnumber__c,
c2g__codavatstatus__c,
c2g__collectionsonhold__c,
c2g__collectionsonholdreason__c,
c2g__dayssalesoutstanding__c,
cloudingoagent__bar__c,
cloudingoagent__bas__c,
cloudingoagent__bav__c,
cloudingoagent__brdi__c,
cloudingoagent__btz__c,
cloudingoagent__sar__c,
cloudingoagent__sas__c,
cloudingoagent__sav__c,
cloudingoagent__srdi__c,
cloudingoagent__stz__c,
comments__c,
default_expense_type__c,
dob_ssa__c,
email_search__c,
ffa_company__c,
ffaci__currencyculture__c,
ffbf__accountparticulars__c,
ffbf__bankbic__c,
ffbf__paymentcode__c,
ffbf__paymentcountryiso__c,
ffbf__paymentpriority__c,
ffbf__paymentroutingmethod__c,
fferpcore__exemptioncertificate__c,
fferpcore__isbillingaddressvalidated__c,
fferpcore__isshippingaddressvalidated__c,
fferpcore__materializedbillingaddressvalidated__c,
fferpcore__materializedshippingaddressvalidated__c,
fferpcore__outputvatcode__c,
fferpcore__salestaxstatus__c,
fferpcore__taxcode1__c,
fferpcore__taxcode2__c,
fferpcore__taxcode3__c,
fferpcore__taxcountrycode__c,
fferpcore__validatedbillingcity__c,
fferpcore__validatedbillingcountry__c,
fferpcore__validatedbillingpostalcode__c,
fferpcore__validatedbillingstate__c,
fferpcore__validatedbillingstreet__c,
fferpcore__validatedshippingcity__c,
fferpcore__validatedshippingcountry__c,
fferpcore__validatedshippingpostalcode__c,
fferpcore__validatedshippingstate__c,
fferpcore__validatedshippingstreet__c,
fferpcore__vatregistrationnumber__c,
fferpcore__vatstatus__c,
ffex__legalname__c,
flow_alpha_group__c,
flow_name_city__c,
full_name_phone__c,
parent_business_account__c,
phone_mobile_number__c,
phone_search__c,
phone_unformatted_formula__c,
physical_location__c,
qbdialer__closedate__c,
qbdialer__closescore__c,
qbdialer__dials__c,
qbdialer__lastcalltime__c,
qbdialer__responsetime__c,
qbdialer__timezonesidkey__c,
tdc_tsw__last_sender_number__c,
tdc_tsw__sms_opt_out__c</value>
    </values>
    <values>
        <field>Target_Object__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
