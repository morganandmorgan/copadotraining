<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransfer5706 Hurricane</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Hurricane Damage</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Transfer to x5706
&lt;br/&gt;
&lt;br/&gt;
People who have suffered huge losses from Hurricane Irma and the insurance companies have denied or low balled their claims adding more grief to this devastating disaster. 
&lt;BR/&gt;
&lt;BR/&gt;
&lt;LI&gt;Calls Regarding FEMA claims, disputes or denials, please inform the caller that: &quot;We are not able to assist in regards to your FEMA claim. We can get your information in regards to your claim with your insurance company to see if you may have a case.&quot; &lt;/LI&gt;
&lt;BR/&gt;
&lt;LI&gt;If a caller asks: &quot;Who are the crooks and predators that are referenced in our TV/ Radio Ad?&quot;  Please say: &quot;This is referring to contractors and others that get victims to assign their benefits over, for more information to see if you may have a case with our firm, let me transfer you over to the specialized department that will be able to assist you with your free consultation.&quot; &lt;/LI&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
