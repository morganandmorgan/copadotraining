<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransfer5745x5748 Forfeiture</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Business Law; Complex Lit; Employment Law; Forfeiture; Immigration; Insurance Dispute; VA; Warranty</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English - x5745  Spanish - x5748
&lt;br/&gt;
&lt;br/&gt;
When local, state, or federal police take (aka &quot;&quot;seize&quot;&quot;) a citizen&apos;s property or assets because of ANY of the following reasons:
&lt;br/&gt;-Police believe the property or assets were or will be used to commit or facility criminal activity&lt;br/&gt;
-Police believes the property or assets were purchased from proceeds of the criminal activity</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
