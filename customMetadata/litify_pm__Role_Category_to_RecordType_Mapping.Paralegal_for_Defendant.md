<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Paralegal for Defendant</label>
    <protected>false</protected>
    <values>
        <field>litify_pm__Category__c</field>
        <value xsi:type="xsd:string">Paralegal for Defendant</value>
    </values>
    <values>
        <field>litify_pm__RecordType_Name__c</field>
        <value xsi:type="xsd:string">Paralegal_for_Defendant</value>
    </values>
</CustomMetadata>
