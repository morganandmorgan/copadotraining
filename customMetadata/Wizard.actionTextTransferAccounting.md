<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferAccounting</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Accounting Calls</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;b&gt;Accounting Call Instructions:&lt;/b&gt;
&lt;br/&gt;
&lt;br/&gt;
&lt;span style=&quot;color: red;&quot;&gt;Accounting Department &lt;b&gt;NEVER&lt;/b&gt; takes calls from clients!!&lt;/span&gt;
&lt;br/&gt;

&lt;li&gt;Calls requesting case-related W-9’s go to the paralegal on file. W-9’s that are NOT case-related go to Melissa Nuckoles’ voicemail. To transfer to VM, press * then x5141&lt;/li&gt;
&lt;br/&gt;

&lt;li&gt;Calls regarding Wire Transfers from SunTrust Bank get consulted to Bernardo Garcia x5107 (usually it is Lewanna from SunTrust that calls for Bernardo).&lt;/li&gt;
&lt;br/&gt;

&lt;li&gt;Calls from check cashing/banking institutions to verify funds on a check, first confirm which location the check is drawn on, and then WARM TRANSFER to the proper person on the verification list below:&lt;/li&gt;

&lt;br/&gt;
&lt;br/&gt;

&lt;span style=&quot;color: red;&quot;&gt; CHECK VERIFICATION List: (Always warm transfer the call!!):&lt;/span&gt;
&lt;br/&gt;

&lt;li&gt;Orlando Trust Account - 1st Monserrat Capistran x5066, 2nd Lorissa Thompson x5232, 3rd Melissa Nuckoles x5141, 4th email ORL-Accounting&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Fort Myers Trust Account - Lauren Hutchinson x2127, Emily Ruiz x2152&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Tampa Trust Account - Denise Rees x3136, Aida Torrens x3082, Nicole Garcia x3080, Patti Patterson x3759,  Georgina George x3255, Imani Shabazz x3195&lt;/li&gt;
&lt;br/&gt; 
&lt;li&gt;Jacksonville Trust Account - 1st Jessica Dills x6169, 2nd Adam Poblacion x6137, 3rd Matt Brown x6129&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Memphis Trust Account - 1st Mike Harrell x1874, 2nd Brian Nason x1804&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Nashville Trust Account - Tracy Morkunas x4903&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Atlanta Trust Account - Jessica Wood x4027, LaShonna Norwood x4421, Octavia McClendon x4303, Svetlana Shchukin x4350, Vickie Lofton x4407. If you can’t reach anyone, email ATL-Accounting&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Lexington Trust Account - Jessica Polchinski x7810, Rebecca Brown x7861&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Mississippi Trust Account - 1st Courtney Reeves x7337, 2nd Hope Matheson x7348, 3rd Aida Torrens x3082&lt;/li&gt;
&lt;br/&gt;
&lt;br/&gt;

&lt;span style=&quot;color: red;&quot;&gt;Other Call Types&lt;/span&gt;


&lt;br/&gt;
&lt;li&gt;Calls regarding buying/purchasing equipment (i.e. copy machines, postage machines), supplies, coffee services, etc. get transferred to Hwa Salomone’s voicemail. To transfer to her VM, press * then the x5058&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Calls regarding outstanding invoices for client-related services do not get put through to accounting. You must transfer the call to the attorney’s office that used their services.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Calls regarding NON-client related services invoices (i.e. parking invoices, rent, electric, etc.) transfer to Melissa Nuckoles voicemail. To transfer to her VM, press * then x5141&lt;/li&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
