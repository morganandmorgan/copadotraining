<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferEscalations</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Escalated Calls</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;LI&gt;Client calls back and you notice it’s regarding a TD so you deliver the TD script.  Caller will not take the TD and demands to speak to someone that can tell them why: Warm transfer to IST x5737&lt;/LI&gt;
&lt;br/&gt;

&lt;LI&gt;If the caller insists on speaking to someone and IST is not available, warm transfer to Client Satisfaction x5746&lt;/LI&gt;
&lt;br/&gt;

&lt;LI&gt;Client complaints for case staff or not getting a call back (after attempting the case staff), these calls go to Client Satisfaction/Services x5746 &lt;/LI&gt;
&lt;br/&gt;

&lt;LI&gt;If the client wants to cancel their services with the firm or cancel their appointment, warm transfer to IST x5737&lt;/LI&gt;
&lt;BR/&gt;

If the client demands to speak to an attorney: &quot;I completely understand; in order to ensure that you speak to the proper legal team, I need to transfer you over to our case professional, they are trained by the attorneys to answer your questions and screen the cases for the attorneys. What type of case are you calling in about?&quot; (Once they inform you what case they&apos;re calling about) - &quot;Allow me to transfer you so they can assist you further.&quot; 
&lt;br/&gt;
&lt;br/&gt;

&lt;LI&gt;If it&apos;s a sensitive case (i.e. Rape case, Death Cases, etc.) or a Whistleblower Case, please get with your TL for instructions.&lt;/LI&gt;
&lt;br/&gt;

&lt;LI&gt;Any other cases, explain to the client that this is the process for the free consultation and unfortunately we will not be able to simply tx. to an Attorney.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
