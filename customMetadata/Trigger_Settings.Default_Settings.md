<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Settings</label>
    <protected>false</protected>
    <values>
        <field>Accounts__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Docusign_Status__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_All_Triggers__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Expense_Enable_Summaries__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Expense_Minutes_Back_To_Process__c</field>
        <value xsi:type="xsd:double">-720.0</value>
    </values>
    <values>
        <field>Incident_Investigations__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Intake_Enable_TOG_Integration__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Intake_Investigations__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Intakes__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Investigations__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Litify_Expenses__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Litify_Matters__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Litify_Referrals__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Litify_Roles__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Litify_Team_Members__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Matter_Compute_SOL__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Matter_Task_Histories__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Matter_Update_Deceased__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Spring_Accounts__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
