<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UserChanged_Lawsuit</label>
    <protected>false</protected>
    <values>
        <field>Consumer__c</field>
        <value xsi:type="xsd:string">mmlawsuit_PlatformEventConsumer</value>
    </values>
    <values>
        <field>EventSObjectCategory__c</field>
        <value xsi:type="xsd:string">User</value>
    </values>
    <values>
        <field>Event__c</field>
        <value xsi:type="xsd:string">UsersChanged</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>MatcherRule__c</field>
        <value xsi:type="xsd:string">MatchPlatformEventBusAndRelatedSObjectAndEventName</value>
    </values>
    <values>
        <field>RelatedPlatformEventBus__c</field>
        <value xsi:type="xsd:string">mmlib_Event__e</value>
    </values>
</CustomMetadata>
