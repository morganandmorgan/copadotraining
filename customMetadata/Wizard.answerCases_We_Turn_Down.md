<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerCases We Turn Down</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Cases we turn down</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTurnDown&quot;],
&quot;tags&quot;:[&quot;Cigarettes/Tobacco&quot;,
&quot;Bankruptcy&quot;,
&quot;False Information&quot;,
&quot;Family Law&quot;,
&quot;Foreclosure&quot;,
&quot;Landlord Tenant&quot;,
&quot;Political Issues&quot;,
&quot;Taxes IRS&quot;,
&quot;Unemployment&quot;,
&quot;Black Farmers&quot;,
&quot;Loan Modification&quot;,
&quot;American Spirits Cigarettes&quot;,
&quot;DCF cases&quot;,
&quot;department of children and family&quot;,
&quot;suing the government&quot;,
&quot;suing the president&quot;,
&quot;suing the congress&quot;,
&quot;defamation&quot;,
&quot;BP Oil Spills&quot;,
&quot;Chinese Drywall&quot;,
&quot;Lumber Liquidator floors&quot;,
&quot;Mini Cooper&quot;,
&quot;Purina Dog Chow&quot;,
&quot;Domestic Violence&quot;,&quot;Child Support&quot;,&quot;Stalking&quot;]}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
