<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransfer6945x6948 Business Law</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Business Law; Complex Lit; Employment Law; Forfeiture; Immigration; Insurance Dispute; VA; Warranty</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English &amp;  Spanish - x5576
&lt;br/&gt;
&lt;br/&gt;
&lt;b&gt;Contract Dispute-&lt;/b&gt;  When any party in a contract has a disagreement regarding any of the contract terms or definitions, or when a party a fails to perform a duty or promise that they agreed to in the contract. &lt;br/&gt;
&lt;b&gt;Professional malpractice/legal malpractice-&lt;/b&gt; Negligence by an Attorney, Accountant, Architect, Engineers or a Real Estate Agent, etc. This can include when one of these professionals fails to perform a task or function that lead to the client suffering monetary damage.&lt;br/&gt;
&lt;b&gt;Real Estate-&lt;/b&gt; Issues dealing with a real estate transaction or property sale/purchase&lt;br/&gt;
&lt;b&gt;Securities-&lt;/b&gt;Investment loss from stocks, bonds, options, mutual funds, currencies, annuities, real estate investment trusts (REITs), pensions, IRAs, 401Ks and/or any other type of investment.&lt;br/&gt;
&lt;b&gt;Wills, Estates and Probate-&lt;/b&gt; An inheritance under a t rust or will with claims for breach of fiduciary duty, fraud, lack of capacity, undue influence or other misconduct.&lt;br/&gt;
&lt;b&gt;Construction-&lt;/b&gt; Cases involving home repair, home remodel, or new single family home construction that resulted in a breach of contract. Construction repairs paid for and promise, yet never performed or not performed on time. &lt;br/&gt; 
&lt;b&gt;Intellectual Property-&lt;/b&gt; 
Patent: A government authority or license for the sole right to exclude others from making, using, or selling an invention. &lt;br/&gt;
Trademark: A symbol, word, or words legally registered or established by use as representing a company or product. &lt;br/&gt;
Copyright: The exclusive legal right, given to an originator to print, publish, perform, film, or record literary, artistic, or musical material, and to authorize others to do the same. &lt;br/&gt;
Trade Secret: A formula, practice, process, design, instrument, pattern of information which is not generally known by others, and by which a business can obtain an economic advantage over competitors or customers. Example of well known trade secrets include the formula for Coca-Cola.&lt;br/&gt;
&lt;b&gt;Solicitation for Legal Services-&lt;/b&gt; An Attorney who solicits prospective clients for legal services illegally by not using advertisements and instead contacting the PC by phone, email or in person.
&lt;b&gt;Forfeiture&lt;/b&gt; When local, state, or federal police take (aka &quot;seize&quot;) a citizen&apos;s property or assets because of ANY of the following reasons:
        -Police believe the property or assets were or will be used to commit or facility criminal activity
        -Police believes the property or assets were purchased from proceeds of the criminal activity</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
