<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>damaged_house_state</label>
    <protected>false</protected>
    <values>
        <field>RequestField__c</field>
        <value xsi:type="xsd:string">damaged_house_state</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SObjectFieldName__c</field>
        <value xsi:type="xsd:string">Damaged_House_State__c</value>
    </values>
    <values>
        <field>SObjectName__c</field>
        <value xsi:type="xsd:string">Intake__c</value>
    </values>
</CustomMetadata>
