<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Does User Have Specific Role Of Admin?</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>RuleDefinition__c</field>
        <value xsi:type="xsd:string">{
  &quot;segmentList&quot; : [ {
    &quot;ruleType&quot; : &quot;mmbrms_UserInRolesSpecified&quot;,
    &quot;ruleSetupJson&quot; : &quot;{\&quot;rolesSpecifiedList\&quot; : [ \&quot;admin\&quot; ]}&quot;
  } ],
  &quot;defaultResponse&quot; : true
}</value>
    </values>
</CustomMetadata>
