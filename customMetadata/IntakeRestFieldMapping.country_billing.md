<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>country_billing</label>
    <protected>false</protected>
    <values>
        <field>RequestField__c</field>
        <value xsi:type="xsd:string">country_billing</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SObjectFieldName__c</field>
        <value xsi:type="xsd:string">BillingCountry</value>
    </values>
    <values>
        <field>SObjectName__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
