<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>UnitOfWork_litify_pm_Referral_c</label>
    <protected>false</protected>
    <values>
        <field>ClassToInject__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Interface__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SObjectType__c</field>
        <value xsi:type="xsd:string">litify_pm__Referral__c</value>
    </values>
    <values>
        <field>SortOrder__c</field>
        <value xsi:type="xsd:double">400.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">UnitOfWork</value>
    </values>
</CustomMetadata>
