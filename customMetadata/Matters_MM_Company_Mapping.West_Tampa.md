<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>West Tampa</label>
    <protected>false</protected>
    <values>
        <field>Assigned_Office_Location__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Assigned_to_MM_Business__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan Tampa P.A.</value>
    </values>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Matter_Plan__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Orbit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parent_Office_Name__c</field>
        <value xsi:type="xsd:string">Tampa</value>
    </values>
</CustomMetadata>
