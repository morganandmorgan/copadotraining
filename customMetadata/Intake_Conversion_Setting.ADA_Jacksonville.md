<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ADA - Jacksonville</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Assigned_Office_Location__c</field>
        <value xsi:type="xsd:string">Jacksonville</value>
    </values>
    <values>
        <field>Create_Lawsuit__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Create_Matter__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Handling_Firm__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan</value>
    </values>
    <values>
        <field>Indicator_Field__c</field>
        <value xsi:type="xsd:string">Y</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:type="xsd:string">ADA - American Disability Act</value>
    </values>
    <values>
        <field>Venue__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
