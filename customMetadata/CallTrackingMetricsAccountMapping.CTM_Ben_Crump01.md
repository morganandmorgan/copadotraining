<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CTM_Ben_Crump01</label>
    <protected>false</protected>
    <values>
        <field>CTM_Account_Id__c</field>
        <value xsi:type="xsd:string">125243</value>
    </values>
    <values>
        <field>CTM_Auth_Named_Credential__c</field>
        <value xsi:type="xsd:string">CTM_Ben_Crump</value>
    </values>
    <values>
        <field>Domain_Value__c</field>
        <value xsi:type="xsd:string">www.bencrump.com</value>
    </values>
</CustomMetadata>
