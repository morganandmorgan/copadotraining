<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTurnDown</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Auto TD - Do not transfer, follow TD scripts.
&lt;br/&gt;
&lt;br/&gt;
&lt;b&gt;Cigarettes/Tobacco-&lt;/b&gt; Anyone calling regarding injuries/cancer diagnosis due to consuming tobacco products.&lt;br/&gt;
&lt;b&gt;Bankruptcy-&lt;/b&gt; Filing for bankruptcy or issues with a current bankruptcy case. Legal process where people are able to find relief from their debts and start over against.&lt;br/&gt;
&lt;b&gt;Family Law-&lt;/b&gt; Includes issues with DCF, Divorce, Child Support and custody, alimony, property and debt division, and restraining orders.&lt;br/&gt;
&lt;b&gt;Foreclosure-&lt;/b&gt; Allows a lender to recover the amount owed on a defaulted loan by selling or taking ownership (repossession) of the property securing the loan.&lt;br/&gt;
&lt;b&gt;Landlord/Tenant Dispute(as long as it does not involve injuries)-&lt;/b&gt; Issues with landlords over charging, evictions, living condition (no injuries). &lt;br/&gt;
&lt;b&gt;Loan Modification&lt;/b&gt; Homeowners facing major financial hardship that could lead to a foreclosure may work with a lender to work out a plan or restructuring their mortgage loan so the person (borrower) can afford the payments. &lt;br/&gt;
&lt;b&gt;Political Issues-&lt;/b&gt; Suing Government/President and Congress.&lt;br/&gt;
&lt;b&gt;Taxes/IRS-&lt;/b&gt; Issues where the person has a claim regarding their taxes or want assistance in filing their taxes or issues with the IRS.&lt;br/&gt;
&lt;b&gt;Unemployment Denied-&lt;/b&gt; Issues with receiving unemployment benefits.&lt;br/&gt;
&lt;b&gt;Black Farmers-&lt;/b&gt; A class Action that Morgan &amp; Morgan handled and is now closed. This was a case against the U.S. Department of Agriculture for racial discrimination against African American Farmers.&lt;br/&gt;
&lt;b&gt;BP Oil Spill-&lt;/b&gt; Spill in the Gulf of Mexico that effected many businesses and people.&lt;br/&gt;
&lt;b&gt;Chinese Drywall-&lt;/b&gt; Refers to an environmental health issue involving defective drywall manufactured in China, imported to the U.S. and used in residential construction between 2001 and 2009.&lt;br/&gt;
&lt;b&gt;Lumber Liquidator floors-&lt;/b&gt; Claims against a flooring company, where an investigation tested several lumber liquidator Chinese-made laminate flooring products and found that nearly all of the samples contained high levels of a chemical that can cause many health problems.&lt;br/&gt;
&lt;b&gt;Mini Cooper-&lt;/b&gt; Closed class action that alleged that certain mini cooper models suffered a defect within their timing chain.&lt;br/&gt;
&lt;b&gt;Purina/Dog Chow-&lt;/b&gt; Class Action suit that claims Purina is responsible for thousands of dog deaths.
Please read this script to the caller: 
“I’m sorry to hear you are going through this situation and I thank you for giving Morgan &amp; Morgan a chance to look into your case.  However this specific matter falls out of our firms practice area. I want to be a part of your solution so I would like to provide you with the state bar so they can refer you to an attorney that can possibly assist further. Do you have something to write with?....“</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
