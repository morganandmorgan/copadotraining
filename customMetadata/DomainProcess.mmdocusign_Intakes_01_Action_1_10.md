<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>mmdocusign_Intakes_01_Action_1_10</label>
    <protected>false</protected>
    <values>
        <field>ClassToInject__c</field>
        <value xsi:type="xsd:string">mmdocusign_IntakeTemplateAssgnmntAction</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">This will default the intake&apos;s DocusignTemplateID__c and Docusign_Template_ID_Invst__c fields</value>
    </values>
    <values>
        <field>DomainMethodToken__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ExecuteAsynchronous__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LogicalInverse__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>OrderOfExecution__c</field>
        <value xsi:type="xsd:double">1.1</value>
    </values>
    <values>
        <field>PreventRecursive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ProcessContext__c</field>
        <value xsi:type="xsd:string">TriggerExecution</value>
    </values>
    <values>
        <field>RelatedDomain__c</field>
        <value xsi:type="xsd:string">Intake__c</value>
    </values>
    <values>
        <field>TriggerOperation__c</field>
        <value xsi:type="xsd:string">BeforeInsert</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Action</value>
    </values>
</CustomMetadata>
