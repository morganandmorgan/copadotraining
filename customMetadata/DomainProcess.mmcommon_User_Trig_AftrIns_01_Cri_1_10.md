<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>mmcommon_User_Trig_AftrIns_01_Cri_1_10</label>
    <protected>false</protected>
    <values>
        <field>ClassToInject__c</field>
        <value xsi:type="xsd:string">mmcommon_UserIsActiveChangedCriteria</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Domain criteria used to determine if the User is active in order to assign holidays to that user.</value>
    </values>
    <values>
        <field>DomainMethodToken__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ExecuteAsynchronous__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LogicalInverse__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>OrderOfExecution__c</field>
        <value xsi:type="xsd:double">1.1</value>
    </values>
    <values>
        <field>PreventRecursive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ProcessContext__c</field>
        <value xsi:type="xsd:string">TriggerExecution</value>
    </values>
    <values>
        <field>RelatedDomain__c</field>
        <value xsi:type="xsd:string">User</value>
    </values>
    <values>
        <field>TriggerOperation__c</field>
        <value xsi:type="xsd:string">AfterInsert</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Criteria</value>
    </values>
</CustomMetadata>
