<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerInsurance Dispute</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Insurance Dispute</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer6945x6948_Ins_Dispute&quot;],
&quot;tags&quot;:[&quot;First party&quot;,&quot;insurance&quot;,&quot;1st party&quot;,&quot;first party&quot;, &quot;property&quot;,&quot;auto&quot;,&quot;life&quot;,&quot;accidental&quot;,&quot;contractor&quot;,&quot;health&quot;,&quot;critical illness&quot;,&quot;storage&quot;,&quot;vehicle value&quot;,&quot;diminished&quot;,&quot;DIV&quot;,&quot;auto damage&quot;,&quot;dispute&quot;,&quot;low ball&quot;,&quot;short term disability&quot;,&quot;long term disability&quot;,&quot;gap insurance&quot;,&quot;mold&quot;,&quot;sinkhole&quot;,&quot;flood&quot;,&quot;flood damage&quot;,&quot;car&quot;,&quot;blasting&quot;,&quot;quarry&quot;]}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
