<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerPremise Liability Cases</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Premise Liability Cases (AI, GI, NS, SF)</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer5716x5718_Premise&quot;],
&quot;tags&quot;:[&quot;Animal&quot;,&quot;General&quot;,&quot;Negligent Security&quot;,&quot;Slip &amp; Fall&quot;,&quot;Dog&quot;,&quot;Slip and Fall&quot;,&quot;Slip&quot;,&quot;Fall&quot;,&quot;aggression&quot;,&quot;intentional&quot;,&quot;Train injuries&quot;,&quot;Air Craft injuries&quot;,&quot;falling&quot;,&quot;security&quot;,&quot;injuries&quot;,&quot;injury&quot;]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
