<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Domain_Marketing_Tracking_Info__c</label>
    <protected>false</protected>
    <values>
        <field>ClassToInject__c</field>
        <value xsi:type="xsd:string">mmmarketing_TrackingInformations.Constructor</value>
    </values>
    <values>
        <field>Interface__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SObjectType__c</field>
        <value xsi:type="xsd:string">Marketing_Tracking_Info__c</value>
    </values>
    <values>
        <field>SortOrder__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Domain</value>
    </values>
</CustomMetadata>
