<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferSellProduct</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Sell Product Hwa</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Direct the caller to email Hwa Salomone at hsalomone@forthepeople.com
&lt;br/&gt;
&lt;br/&gt;
Inquiries wanting to rent us office space, finance software, provide cleaning service, sell us a product or service 
&lt;br/&gt;
&lt;br/&gt;
If the caller mentions or specifically asks to speak with Hwa, transfer to x5058</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
