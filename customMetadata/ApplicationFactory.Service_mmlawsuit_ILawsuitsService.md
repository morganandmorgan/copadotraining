<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Service_mmlawsuit_ILawsuitsService</label>
    <protected>false</protected>
    <values>
        <field>ClassToInject__c</field>
        <value xsi:type="xsd:string">mmlawsuit_LawsuitsServiceImpl</value>
    </values>
    <values>
        <field>Interface__c</field>
        <value xsi:type="xsd:string">mmlawsuit_ILawsuitsService</value>
    </values>
    <values>
        <field>SObjectType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SortOrder__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Service</value>
    </values>
</CustomMetadata>
