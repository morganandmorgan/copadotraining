<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferToQueue5741 Mass Tort</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Mass Tort</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Mass Tort x5741
&lt;br/&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;General Drug-&lt;/b&gt; Injuries due to a medication.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Medical Device/implant- &lt;/b&gt; Injuries due to a medical device or implant inserted.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;9/11- &lt;/b&gt; Allows victims of terrorist acts that occurred on U.S. soil to sue the foreign governments responsible.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Porter Ranch-&lt;/b&gt; Porter ranch residents who have been suffering from the health complications caused by a massive gas blowout at a Southern California Gas co. facility.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Cast Iron Pipes- &lt;/b&gt; Calls re: homes built before 1975 and suffered water damage from pipe issues.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Mosaic Fertilizer Plant- &lt;/b&gt; A massive sinkhole opened up beneath the Mosaic company&apos;s fertilizer plant.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Energy Drinks- &lt;/b&gt;Injuries sustained by consuming energy drinks.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Round up- &lt;/b&gt;A weed-killer that has been linked to causing cancer.&lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Dicamba-&lt;/b&gt; Dicamba is a highly volatile herbicide with a propensity to drift or volatize onto off-site locations. Farmers use a Herbicide Roundup to destroy weeds and unwanted vegetation. Dicamba has caused weeds to be more resistant &quot;immune&quot; to Roundup  and now over 2/3rds of all farmers have &quot;super weeds.&quot; &lt;/li&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;Talcum Powder-&lt;/b&gt; Since 1971, more than 20 studies have linked talc powder to ovarian cancer. Johnson &amp; Johnson, Baby Powder and Shower-to-Shower products knew about the ovarian cancer risk since 1982 but failed to warn women.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
