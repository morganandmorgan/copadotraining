<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TEST</label>
    <protected>false</protected>
    <values>
        <field>AuthEndpoint__c</field>
        <value xsi:type="xsd:string">https://authuat.springcm.com/api/v201606/</value>
    </values>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">00000000-0000-0000-0000-000000000000</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">0000000000000000</value>
    </values>
</CustomMetadata>
