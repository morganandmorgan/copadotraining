<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferToMarketing</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Marketing Calls</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Please have the person send an email to Marketing-inquiries@forthepeople.com
&lt;br/&gt;
&lt;br/&gt;
&lt;b&gt;DO NOT&lt;/b&gt; Transfer to Karine, she is no longer in charge of Marketing Inquiries.
&lt;br/&gt;
&lt;br/&gt;
A Marketing Call is when someone is calling wanting to know who manages our websites (forthepeople.com, classaction.com, hurricanelawyer.com, etc.)
Also, questions, information or website inquiries related to google ads, social media, someone wanting us to advertise on their network.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
