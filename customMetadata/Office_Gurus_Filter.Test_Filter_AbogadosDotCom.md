<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test Filter (Abogados.com)</label>
    <protected>false</protected>
    <values>
        <field>Campaign_Id__c</field>
        <value xsi:type="xsd:string">129871</value>
    </values>
    <values>
        <field>Filter__c</field>
        <value xsi:type="xsd:string">SELECT Id, Status__c, Status_Detail__c, Number_Outbound_Dials_Status__c, Marketing_Source__c, Marketing_Sub_source__c, Sent_To_TOG__c FROM Intake__c WHERE Status__c = &apos;Lead&apos; AND Status_Detail__c = &apos;Needs to be Qualified&apos; AND Number_Outbound_Dials_Status__c &lt;= 6 AND Marketing_Source__c = &apos;abogados.com&apos; ORDER BY Number_Outbound_Dials_Status__c ASC, CreatedDate DESC</value>
    </values>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">400.0</value>
    </values>
</CustomMetadata>
