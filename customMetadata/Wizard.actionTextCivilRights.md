<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextCivilRights</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English Transfer to X6916   24/7  |  Spanish Transfer to X6918  24/7
&lt;br/&gt; 
&lt;br/&gt;
Individuals who have not been allowed their full legal, social or economic rights as American citizens. 
&lt;br/&gt;
- &lt;b&gt;Civil Discrimination&lt;/b&gt; – Individuals who are discriminated against in public for being of a protected class – religion, gender, ethnicity etc.
- &lt;b&gt;Jail&lt;/b&gt; – Injuries to inmates due to mistreatment or abuse by jail staff. 
- &lt;b&gt;Police Brutality&lt;/b&gt; - Individuals who are injured mentally or physically due to excessive force or treatment from law enforcement.
- &lt;b&gt;Wrongful Imprisonment&lt;/b&gt; – Individuals incarcerated for crimes they did not commit.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
