<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Trigger Settings</label>
    <protected>false</protected>
    <values>
        <field>litify_pm__runAllAccount_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllAttachment_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllContact_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllContentDocumentLink_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllContentDocument_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllContentVersion_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllEvent_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllIntake_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllLitifyDoc_Trigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__runAllMatterStageActivity_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllMatterTeamMember_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllMatter_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllQuestionnaire_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllTask_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__runAllTransaction_Trigger__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>litify_pm__setMatterTeamBasedOnPrincipalAttorney__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>litify_pm__skipEmailAccountMatchingForReferralToInt__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
