<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CME Report</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:type="xsd:string">Product Liability</value>
    </values>
    <values>
        <field>Document_Link__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Is_Witness_Statement__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Trigger_on_edit__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
