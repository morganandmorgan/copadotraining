<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferSSScottBates</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Scott Bates Social Security Calls</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;b&gt;We are never to transfer any SS client calls to Scott Bates or his Voicemail. &lt;/b&gt;
&lt;br/&gt;
&lt;br/&gt;

&lt;li&gt;If a current client calls regarding a SS case, search in Salesforce and transfer to the assigned Case Developer/ Manager. &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;If no Case developer is assigned and you are not sure where to transfer, you can transfer to x2515 &lt;/li&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
