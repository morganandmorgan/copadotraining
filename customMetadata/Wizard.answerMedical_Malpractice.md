<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerMedical Malpractice</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Medical Malpractice</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer6967_Med_Mal&quot;],
&quot;tags&quot;:[&quot;Doctor&quot;,&quot;Nurse&quot;,&quot;Hospital&quot;,&quot;Medical professional&quot;,&quot;home health aide&quot;,&quot;licensed health care&quot;,&quot;RN&quot;,&quot;LPN&quot;,&quot;Hospice&quot;,&quot;EMT&quot;,&quot;Paramedic&quot;,&quot;Pharmacy Negligence&quot;,&quot;Vaccinations&quot;,&quot;HIPPA violation&quot;, &quot;medical malpractice&quot;,&quot;misdiagnosis&quot;,&quot;dentist&quot;,&quot;birth&quot;,&quot;birth injury&quot;,&quot;pregnancy&quot;]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
