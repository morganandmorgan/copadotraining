<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerBusiness Law</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Business Law</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransferToQueue6945x6948BusLaw&quot;],
&quot;tags&quot;:[&quot;Contract&quot;,&quot;business&quot;,&quot;dispute&quot;,&quot;Legal Malpractice&quot;,&quot;Real estate&quot;,&quot;securities&quot;,&quot;Wills&quot;,&quot;Estate&quot;,&quot;probate&quot;,&quot;Solicitation&quot;,&quot;legal services&quot;,&quot;IRA&quot;,&quot;401K&quot;,&quot;Accountant&quot;,&quot;malpractice&quot;,&quot;pension&quot;,&quot;trusts&quot;,&quot;copyright&quot;,&quot;trademark&quot;,&quot;patent&quot;,&quot;trade secrets&quot;,&quot;construction cases&quot;,&quot;home repairs&quot;,&quot;repairs of property&quot;,&quot;purchase of property&quot;,&quot;utility bills&quot;,&quot;home repair&quot;,&quot;HOA&quot;,&quot;homeowners association&quot;,&quot;monetary loss&quot;,&quot;eminent domain&quot;,&quot;time share&quot;,&quot;condo&quot;,&quot;pet injury&quot;,&quot;intellectual property&quot;,&quot;professional malpractice&quot;,&quot;utilities&quot;,&quot;Woodbridge Scheme&quot;,&quot;forfeiture&quot;,&quot;seize&quot;,&quot;federal police&quot;,&quot;local police&quot;,&quot;state police&quot;,&quot;property taken&quot;,&quot;assets&quot;]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
