<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerPersonal Injury Cases</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Personal Injury Cases</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer6916x6918_PI&quot;],
&quot;tags&quot;:[&quot;Automobile&quot;,&quot;Accident&quot;,&quot;Bed Bugs&quot;,&quot;Children&apos;s Rights&quot;,&quot;Bullying&quot;, &quot;Cyber Bullying&quot;,&quot;Fire Injury&quot;,&quot;Food Poisoning&quot;,&quot;Maritime&quot; ,&quot;Admiralty&quot;, &quot;Jones Act&quot;,&quot;Cruise Ship cases&quot;,&quot;Rape&quot;, &quot;Molestation&quot;, &quot;Salon&quot;,&quot;injuries&quot;,&quot;wrongful death&quot;,&quot;sexual harassment&quot;,&quot;injury&quot;,&quot;auto accident&quot;,&quot;assault&quot;,&quot;Animal&quot;,&quot;General&quot;,&quot;Negligent Security&quot;,&quot;Slip &amp; Fall&quot;,&quot;Dog&quot;,&quot;Slip and Fall&quot;,&quot;Slip&quot;,&quot;Fall&quot;,&quot;aggression&quot;,&quot;intentional&quot;,&quot;Train injuries&quot;,&quot;Air Craft injuries&quot;,&quot;falling&quot;,&quot;security&quot;,&quot;injuries&quot;,&quot;injury&quot;,&quot;burn&quot;,&quot;car&quot;,&quot;waxing&quot;,&quot;trip&quot;]
}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
