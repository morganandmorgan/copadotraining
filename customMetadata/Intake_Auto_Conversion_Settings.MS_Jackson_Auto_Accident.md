<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MS - Jackson Auto Accident</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:type="xsd:string">Automobile Accident</value>
    </values>
    <values>
        <field>Treatment_Coordinator_UserID__c</field>
        <value xsi:type="xsd:string">005o0000003UsvU</value>
    </values>
    <values>
        <field>Venue__c</field>
        <value xsi:type="xsd:string">MS - Jackson</value>
    </values>
</CustomMetadata>
