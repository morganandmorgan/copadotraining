<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferToLookforEmployment</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Looking for employment</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;b&gt;Person calling is seeking employment opportunities with the Firm:&lt;/b&gt;
&lt;br/&gt;
&lt;br/&gt;
Ask: “Have you Applied for a position already?
&lt;br/&gt;
&lt;br/&gt;
&lt;li&gt;Yes - Ask which office they applied for and transfer to the appropriate HR&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;No - Direct them to www.forthepeople.com to our employment section and they can apply online.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
