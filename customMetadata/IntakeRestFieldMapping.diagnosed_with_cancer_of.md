<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>diagnosed_with_cancer_of</label>
    <protected>false</protected>
    <values>
        <field>RequestField__c</field>
        <value xsi:type="xsd:string">diagnosed_with_cancer_of</value>
    </values>
    <values>
        <field>Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SObjectFieldName__c</field>
        <value xsi:type="xsd:string">Diagnosed_with_Cancer__c</value>
    </values>
    <values>
        <field>SObjectName__c</field>
        <value xsi:type="xsd:string">Intake__c</value>
    </values>
</CustomMetadata>
