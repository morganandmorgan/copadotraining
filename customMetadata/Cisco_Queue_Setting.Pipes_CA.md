<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pipes CA</label>
    <protected>true</protected>
    <values>
        <field>Cisco_Queue__c</field>
        <value xsi:type="xsd:string">5707</value>
    </values>
    <values>
        <field>County__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>GeneratingAttorney__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Handling_Firm__c</field>
        <value xsi:type="xsd:string">Morgan &amp; Morgan</value>
    </values>
    <values>
        <field>Marketing_Source__c</field>
        <value xsi:type="xsd:string">classaction.com</value>
    </values>
    <values>
        <field>Venue__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
