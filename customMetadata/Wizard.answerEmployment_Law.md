<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerEmployment Law</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Employment Law (Wage and Hour)</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer6945x6948_Empl_Law&quot;],
&quot;tags&quot;:[&quot;Wages&quot;,&quot;not paid&quot;,&quot;overtime&quot;,&quot;hours worked&quot;,&quot;back wages&quot;,&quot;underpaid&quot;,&quot;owed wages&quot;,&quot;vacation pay&quot;,&quot;OT&quot;,&quot;Bonus&quot;,&quot;commission&quot;,&quot;salary&quot;, &quot;Employer&quot;,&quot;Tips&quot;, &quot;sick days&quot;,&quot;PTO&quot;,&quot;Best &amp; Brightest&quot;,&quot;Best and Brightest&quot;, &quot;Florida Teacher&quot;,&quot;FL Teacher&quot;,&quot;Teacher&quot;]}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
