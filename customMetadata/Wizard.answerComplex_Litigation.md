<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>answerComplex Litigation</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">Complex Litigation</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:type="xsd:string">{&quot;actionTokens&quot;:[&quot;actionTextTransfer5745x5748_Complex_Lit&quot;],
&quot;tags&quot;:[&quot;Class Action&quot;,&quot;group lawsuit&quot;,&quot;Construction products&quot;,&quot;air conditioning coil&quot;,&quot;amtrak derailment&quot;,&quot;Valspar Paint&quot;,&quot;Atchison Chemical Spill&quot;,&quot;Beats Headphones&quot;,&quot;Commemorative/Bullion coin&quot;,&quot;Cosmetics&quot;,&quot;Keratin Hair&quot;,&quot;Data Breaches&quot;,&quot;Security Breach&quot;,&quot;Atchison chemical spill&quot;,&quot;Aloe&quot;,&quot;Alma legend&quot;,&quot;UCF security breach&quot;,&quot;Yahoo breach&quot;,&quot;Swedish medical center seucirty breach&quot;,&quot;Experian data breach&quot;,&quot;IHG Hotels data breach&quot;,&quot;Yahoo&quot;,&quot;Saks Fifth Avenue&quot;,&quot;HeathNow data Breach&quot;,&quot;Deceptive Advertisements&quot;,&quot;JC Penny&quot;,&quot;Kohls&quot;,&quot;Sears&quot;,&quot;Macys&quot;,&quot;For Profit School&quot;,&quot;Milk Fixing Pricing&quot;,&quot;HP Printers&quot;,&quot;Lead Poisoning cases in Indiana&quot;,&quot;Oil Burning cars&quot;,&quot;Youtube over charging&quot;,&quot;Rush Card Cases&quot;,&quot;washing machines exploding&quot;,&quot;Wells Fargo&quot;,&quot;Equifax&quot;,&quot;Aetna HIV Data Leak&quot;,&quot;Experian Data Breach&quot;,&quot;CA Wild Fires&quot;,&quot;California fires&quot;,&quot;Burkolderia Cepacia&quot;,&quot;b cepacia&quot;,&quot;KB home&quot;,&quot;bb&amp;t lockout&quot;,&quot;bb &amp; t lockout&quot;,&quot;bb&amp; t lockout&quot;,&quot;w2 breach&quot;,&quot;Farmers Insurance&quot;,&quot;K coverage&quot;,&quot;Natera&quot;,&quot;genetic testing&quot;,&quot;Florida Virtual School&quot;,&quot;NY outpatient center&quot;,&quot;St. Petersburg Surgery endoscopy&quot;, &quot;grindr&quot;,&quot;HIV status&quot;,&quot;Panera leak&quot;,&quot;saks 5th Ave &amp; Lord Taylor&quot;,&quot;lord taylor&quot;,&quot;saks fifth&quot;]}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_AnswerModel</value>
    </values>
</CustomMetadata>
