<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TWILIO_CLASSACTION_TV01</label>
    <protected>false</protected>
    <values>
        <field>Domain_Value__c</field>
        <value xsi:type="xsd:string">www.classaction.com</value>
    </values>
    <values>
        <field>Phone_Number__c</field>
        <value xsi:type="xsd:string">+1 (800) 888-1234</value>
    </values>
    <values>
        <field>Source_Value__c</field>
        <value xsi:type="xsd:string">TV</value>
    </values>
    <values>
        <field>Twilio_Account_Id__c</field>
        <value xsi:type="xsd:string">AC223a3cdfb8b2afda02486bcfd9357e7f</value>
    </values>
    <values>
        <field>Twilio_Auth_Named_Credential__c</field>
        <value xsi:type="xsd:string">Twilio_Master</value>
    </values>
</CustomMetadata>
