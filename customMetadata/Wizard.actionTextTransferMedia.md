<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferMedia</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Media Calls</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">&lt;u&gt;&lt;b&gt;Media call instructions:&lt;/b&gt;&lt;/u&gt;
&lt;br/&gt;
Called from the TV News, TV stations, and papers are considered media calls.
&lt;br/&gt;
&lt;br/&gt;
Direct the caller to media@forthepeople.com for any of the requests below: 
&lt;br/&gt;
&lt;br/&gt;
&lt;li&gt;PR Calls (Interview Requests, Sponsorship Requests, Media Inquiries) &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Anyone wanting an interview with John Morgan.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Media is calling to speak to someone or an attorney in regards to a specific case in the media. &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Media is calling to speak to a specific person.&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Media is calling but you are not sure who the assigned attorney is to the case. &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Media is calling in regards to General inquiries NOT pertaining to a specific case (i.e. Medical Marijuana, Politics, Legal Opinions about a current case)&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Media calls where you are not sure if it is in regards to a specific case.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
