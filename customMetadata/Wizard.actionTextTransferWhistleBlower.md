<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferWhistleBlower</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Whistleblower</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English x6916  Spanish x6918&lt;br/&gt;
 &lt;br/&gt;
Anyone who believes they have a claim that involves fraud against the US government - Medicare &amp; Medicaid fraud, tax evasion, government contractor fraud, securities fraud or other fraud against the US government. &lt;br/&gt;
  &lt;br/&gt;
If the caller does not want to talk to an intake please transfer the call to David Reign Ext. 2202 &lt;br/&gt;
  &lt;br/&gt;
These are sensitive calls, and we must handle all of these calls like we do all calls, with professionalism, extreme sensitivity and with care.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
