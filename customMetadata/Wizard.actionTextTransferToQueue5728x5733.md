<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferToQueue5728x5733 SS</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Social Security</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English - x5728  Spanish - x5733
&lt;br/&gt;
&lt;br/&gt;
When a person wants to file for social security disability or received a denial when they applied for disability.
&lt;br/&gt;&lt;br/&gt;
&lt;li&gt;&lt;b&gt;For converted cases please transfer to the assigned case developer &lt;/li&gt;
&lt;BR/&gt;
&lt;li&gt;&lt;b&gt;For cases that are retainer sent please transfer to X5735&lt;/b&gt; &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;&lt;b&gt;For all other Social Security Cases&lt;/b&gt; &lt;/li&gt;
&lt;li&gt;&lt;b&gt;English x5278     Spanish X5733&lt;/b&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
