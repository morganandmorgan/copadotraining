<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransferTimMorgan</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Calls for Tim Morgan</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">“Tim is out of the office today, what is it that you need assistance with in order to get you to someone who can assist you today?”
&lt;br/&gt;
&lt;br/&gt;

Find out what the caller needs and transfer to the appropriate person or department.
&lt;br/&gt;
&lt;br/&gt;

&lt;U&gt;&lt;b&gt;Calls that can be transferred to Tim’s VM:&lt;/b&gt;&lt;/U&gt;
&lt;br/&gt;
&lt;br/&gt;
&lt;li&gt;Charlie Christ&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Medical Marijuana&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Callers wanting to speak to a Morgan&lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Fund Raising Questions/Calls &lt;/li&gt;
&lt;br/&gt;
&lt;li&gt;Caller wants to know about the Morgan Dogs&lt;/li&gt;
&lt;br/&gt;
&lt;b&gt;To transfer to his VM, press the * and then his x5225&lt;/b&gt;</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
