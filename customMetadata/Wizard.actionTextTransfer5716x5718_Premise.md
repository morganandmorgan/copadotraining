<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>actionTextTransfer5716x5718 Premise</label>
    <protected>false</protected>
    <values>
        <field>Case_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">ADA; Meso; Med Mal; Nursing; PI; Premise; Products; Workers Comp</value>
    </values>
    <values>
        <field>DisplayText__c</field>
        <value xsi:type="xsd:string">English - x5716  Spanish - x5718
&lt;br/&gt;
&lt;br/&gt;
An injury caused by some type of negligence or unsafe, or defective condition on someone&apos;s property or at a place of business.&lt;br/&gt;&lt;br/&gt;
&lt;LI&gt;&lt;b&gt;Animal Incident- &lt;/b&gt;Injuries caused by an animal such as a dog, cat, zoo/farm animal or any domestic animal.&lt;/LI&gt;&lt;br/&gt;
&lt;LI&gt;&lt;b&gt;General Injury-&lt;/b&gt; An injury caused by a defective condition on the premises such as: falling objects, injuries while on a train or aircraft etc.&lt;/LI&gt;&lt;br/&gt;
&lt;LI&gt;&lt;b&gt;Negligent Security- &lt;/b&gt;Direct or indirect act of aggression which causes injury by failing to provide security to protect against crime likely to occur on the premises.&lt;/LI&gt;&lt;br/&gt;
&lt;LI&gt;&lt;b&gt;Slip &amp; Falls-&lt;/b&gt; Injuries caused by a slipper object or substance not properly handled by a liable party.</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsDeleted__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Litigation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SerializedData__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">mmwiz_ActionDisplayTextModel</value>
    </values>
</CustomMetadata>
