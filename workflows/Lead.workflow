<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Unconverted_Lead_Notification_1_Hour</fullName>
        <description>Unconverted Lead Notification - 1 Hour</description>
        <protected>false</protected>
        <recipients>
            <recipient>SFDC_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Unconverted_Lead_1_Hour</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Company_name</fullName>
        <field>Company</field>
        <name>Clear Company name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Clear Company name from Hubspot leads</fullName>
        <actions>
            <name>Clear_Company_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNULL(Company))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Unconverted Leads Email Dov</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If lead is unconverted, send email to Dov in one hour</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Unconverted_Lead_Notification_1_Hour</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
