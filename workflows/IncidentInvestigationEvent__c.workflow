<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CCC_Investigation_Scheduled_Investigator_NL</fullName>
        <ccEmails>Investigations@NationLaw.com</ccEmails>
        <description>CCC - Investigation Scheduled - Investigator - NL</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>investigations@nationlaw.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EmailAlertsInvestigationInvestigatorNLF</template>
    </alerts>
    <alerts>
        <fullName>Email_for_Prior_Investigator_when_Reassigned</fullName>
        <description>CCC - Email for Prior Investigator when Reassigned</description>
        <protected>false</protected>
        <recipients>
            <field>Prior_Record_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>investigations@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Reassigned_IIE</template>
    </alerts>
    <alerts>
        <fullName>Investigation_Cancelled_Investigator</fullName>
        <description>CCC - Investigation Cancelled - Investigator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>investigations@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Canceled_Investigator</template>
    </alerts>
    <alerts>
        <fullName>Investigation_Scheduled_Investigator</fullName>
        <ccEmails>investigations@forthepeople.com</ccEmails>
        <description>CCC - Investigation Scheduled - Investigator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>investigations@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Investigation_Investigator</template>
    </alerts>
    <rules>
        <fullName>CCC - Investigation - Investigator Changed</fullName>
        <actions>
            <name>Investigation_Scheduled_Investigator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Investigation_Scheduled_Investigator</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Sends notification to the new Investigator that they have been assigned. Should avoid firing in conjunction with the IncidentInvestigationEvent being set to &quot;Scheduled&quot;.</description>
        <formula>AND(   NOT(ISNEW()),   OR(     NOT(ISCHANGED(Investigation_Status__c)),     NOT(ISPICKVAL(Investigation_Status__c, &quot;Scheduled&quot;))   ),   ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Investigation - Reassigned Email for Investigators</fullName>
        <actions>
            <name>Email_for_Prior_Investigator_when_Reassigned</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Investigation_Scheduled_Investigator</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Sends notification to an investigator that they were removed from an investigation</description>
        <formula>ISCHANGED(Prior_Record_Owner__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Investigation Cancelled - Investigator</fullName>
        <actions>
            <name>Investigation_Cancelled_Investigator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Investigation_Canceled_Client_Intake_Agent_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IncidentInvestigationEvent__c.Investigation_Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Rescheduled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Investigation Scheduled - Investigator</fullName>
        <actions>
            <name>Investigation_Scheduled_Investigator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Investigation_Scheduled_Investigator</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IncidentInvestigationEvent__c.Investigation_Status__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>IncidentInvestigationEvent__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Investigation Scheduled - Investigator - NL</fullName>
        <actions>
            <name>CCC_Investigation_Scheduled_Investigator_NL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Investigation_Scheduled_Investigator_NL</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IncidentInvestigationEvent__c.Investigation_Status__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>IncidentInvestigationEvent__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <description>Nation Law</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>CCC_Email_Investigation_Scheduled_Investigator</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Email - Investigation Scheduled - Investigator</subject>
    </tasks>
    <tasks>
        <fullName>CCC_Email_Investigation_Scheduled_Investigator_NL</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Email - Investigation Scheduled - Investigator - NL</subject>
    </tasks>
    <tasks>
        <fullName>CCC_Investigation_Canceled_Client_Intake_Agent_Email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC - Investigation Canceled - Client-Intake-Agent Email</subject>
    </tasks>
</Workflow>
