<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>litify_pm__Update_Investigated_At_field</fullName>
        <field>litify_pm__Investigated_at__c</field>
        <formula>litify_pm__Investigated_At__c</formula>
        <name>Update Investigated At field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>litify_pm__Referral__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>litify_pm__Update_Signed_Up_At_field</fullName>
        <field>litify_pm__Signed_up_at__c</field>
        <formula>litify_pm__Signed_Up_At__c</formula>
        <name>Update Signed Up At field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>litify_pm__Referral__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>litify_pm__Update Investigated At And Signed Up At Fields</fullName>
        <actions>
            <name>litify_pm__Update_Investigated_At_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>litify_pm__Update_Signed_Up_At_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),OR(ISCHANGED( litify_pm__Investigated_At__c ),ISCHANGED( litify_pm__Signed_Up_At__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
