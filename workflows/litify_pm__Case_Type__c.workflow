<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Test_Trigger_WF</fullName>
        <field>Test_trigger_wf__c</field>
        <literalValue>1</literalValue>
        <name>Update Test Trigger WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Test Duplicate</fullName>
        <actions>
            <name>Test_WF_Rule</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Case_Type__c.Test_trigger_wf__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Test_WF_Rule</fullName>
        <assignedTo>ksadler@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Test WF Rule</subject>
    </tasks>
</Workflow>
