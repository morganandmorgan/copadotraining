<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Finance_Request_Send_Approval_Email</fullName>
        <description>Finance Request - Send Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Finance_Request_Approval_Result</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Owner_FTM</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_FTM</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - FTM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_KTY_Finance</fullName>
        <description>Assign to Kentucky Finance Approvers</description>
        <field>OwnerId</field>
        <lookupValue>Finance_Kentucky</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - KTY Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_MEM_Finance</fullName>
        <description>Assign to Memphis Finance Approvers.</description>
        <field>OwnerId</field>
        <lookupValue>Finance_MEM</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - MEM Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_MIS_Finance</fullName>
        <description>Assign to Mississippi Finance Approvers.</description>
        <field>OwnerId</field>
        <lookupValue>Finance_MIS</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - MIS Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_MT_CA</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_MT_CA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - MT/CA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_NSH_Finance</fullName>
        <description>Assign to Nashville Finance Approvers</description>
        <field>OwnerId</field>
        <lookupValue>Finance_NSH</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - NSH Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_Philly</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_Pennsylvania</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - Philly</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_Social_Finance_Approvers</fullName>
        <description>Assign owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>Finance_Approvers_Social</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner - Social Finance Approvers</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_Submitted_Date</fullName>
        <field>Submitted_Date__c</field>
        <name>Delete Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Request_Mark_In_Process</fullName>
        <field>Request_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Finance Request - Mark In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Request_Update_Approval_Date</fullName>
        <description>When final approval, sf will update the approved date field</description>
        <field>Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Finance Request - Update Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Request_Update_Rejected_Date</fullName>
        <field>Request_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Finance Request - Update Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Request</fullName>
        <field>Request_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Complete</fullName>
        <description>When approved, update the status to approved</description>
        <field>Request_Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Set Status - Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_In_Process</fullName>
        <description>Setting the status to In Process</description>
        <field>Request_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Set Status - In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_Date_Today</fullName>
        <description>Used to set the submitted date for finance request check requests to today</description>
        <field>Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>Set Submitted Date Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_by_UserName</fullName>
        <field>Submitted_By_User_Name__c</field>
        <formula>$User.Username</formula>
        <name>Set Submitted by UserName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Attorney_Approved_Date</fullName>
        <field>Attorney_Approval_Date__c</field>
        <formula>Today()</formula>
        <name>Update Attorney Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Attorney_Approved</fullName>
        <description>Used so that items become avail in list view for finance team</description>
        <field>Request_Status__c</field>
        <literalValue>In Process - Attorney Approved</literalValue>
        <name>Update Status - Attorney Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Draft</fullName>
        <field>Request_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Status - Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
