<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>litify_pm__Update_Duration</fullName>
        <field>litify_pm__Duration__c</field>
        <formula>((litify_pm__End_Time__c - litify_pm__Start_Time__c)*24)
+
(((((litify_pm__End_Time__c - litify_pm__Start_Time__c)*24)-((litify_pm__End_Time__c - litify_pm__Start_Time__c)*24))*60)/100)</formula>
        <name>Update Duration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>litify_pm__Update_Rate</fullName>
        <field>litify_pm__Rate__c</field>
        <formula>litify_pm__Team_Member__r.litify_pm__Billable_Hourly_Rate__c</formula>
        <name>Update Rate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>litify_pm__Calculate Rate for Time Entry</fullName>
        <actions>
            <name>litify_pm__Update_Rate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(litify_pm__Team_Member__r.litify_pm__Billable_Hourly_Rate__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>litify_pm__Start Time End Time Equal Duration</fullName>
        <actions>
            <name>litify_pm__Update_Duration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(ISNEW(),ISCHANGED(litify_pm__End_Time__c), ISCHANGED(litify_pm__Start_Time__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
