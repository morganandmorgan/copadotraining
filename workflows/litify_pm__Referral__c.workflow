<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_1_hour_notification_to_Jake_Non_sync_referral</fullName>
        <description>Email 1 hour notification to Jake - Non sync referral</description>
        <protected>false</protected>
        <recipients>
            <recipient>jsternberger@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rlamont@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tchurch@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Referral_Not_Synced_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>litify_pm__Set_Record_Type_Incoming</fullName>
        <field>RecordTypeId</field>
        <lookupValue>litify_pm__Incoming_Referral</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type Incoming</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>litify_pm__Set_Record_Type_Outgoing</fullName>
        <field>RecordTypeId</field>
        <lookupValue>litify_pm__Outgoing_Referral</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type Outgoing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Referral Not Synced Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Referral__c.litify_pm__LRN_Created_At__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send email to Jake after 1 hour</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_1_hour_notification_to_Jake_Non_sync_referral</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Litify Referral Record Type To Incoming</fullName>
        <actions>
            <name>litify_pm__Set_Record_Type_Incoming</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Litify&apos;s Referral Record Type To Incoming</description>
        <formula>AND( RecordType.DeveloperName &lt;&gt; &quot;mmlitifylrn_ThirdPartyOutgoingReferral&quot;, RecordType.DeveloperName &lt;&gt; &quot;OutgoingReferral&quot;, litify_pm__Originating_Firm__r.litify_pm__ExternalId__c &lt;&gt; $Setup.litify_pm__Public_Setup__c.litify_pm__litify_organization_id__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Litify Referral Record Type To Outgoing</fullName>
        <actions>
            <name>litify_pm__Set_Record_Type_Outgoing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Litify&apos;s Referral Record Type To Outgoing</description>
        <formula>AND( RecordType.DeveloperName &lt;&gt; &quot;mmlitifylrn_ThirdPartyOutgoingReferral&quot;
   , litify_pm__Originating_Firm__r.litify_pm__ExternalId__c = $Setup.litify_pm__Public_Setup__c.litify_pm__litify_organization_id__c
   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>litify_pm__Set Record Type Incoming</fullName>
        <actions>
            <name>litify_pm__Set_Record_Type_Incoming</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>litify_pm__Originating_Firm__r.litify_pm__ExternalId__c  &lt;&gt;  $Setup.litify_pm__Public_Setup__c.litify_pm__litify_organization_id__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>litify_pm__Set Record Type Outgoing</fullName>
        <actions>
            <name>litify_pm__Set_Record_Type_Outgoing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>litify_pm__Originating_Firm__r.litify_pm__ExternalId__c  =  $Setup.litify_pm__Public_Setup__c.litify_pm__litify_organization_id__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
