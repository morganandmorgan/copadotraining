<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Total_Attorney_Fee_on_Fee_Recovery</fullName>
        <description>IF( 
Fee__c = 0.3333,Settlement_Amount__c/3, 
IF( 
NOT(ISBLANK(Fee__c)),Settlement_Amount__c * Fee__c, 
IF( 
NOT(ISBLANK(Fee_Amount_if_not_percent__c)),Fee_Amount_if_not_percent__c, 
0 
)))</description>
        <field>Total_Attorney_Fees__c</field>
        <formula>IF( 
Fee__c = 0.3333,Settlement_Amount__c/3, 
IF( 
NOT(ISBLANK(Fee__c)),Settlement_Amount__c * Fee__c, 
IF( 
NOT(ISBLANK(Fee_Amount_if_not_percent__c)),Fee_Amount_if_not_percent__c, 
0 
)))</formula>
        <name>Set Total Attorney Fee on Fee &amp; Recovery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_reporting_stage_value</fullName>
        <description>Update for reporting</description>
        <field>Stage_When_Settled__c</field>
        <formula>Matter__r.Reporting_Matter_Stage__c</formula>
        <name>Set reporting stage value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Reporting Stage on Fee %26 Recovery Record for Reporting</fullName>
        <actions>
            <name>Set_reporting_stage_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates record w reporting stage</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Total Attorney Fee</fullName>
        <actions>
            <name>Set_Total_Attorney_Fee_on_Fee_Recovery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets total attorney fees when edited</description>
        <formula>OR( ISNEW(), ISCHANGED(Fee__c), ISCHANGED(Fee_Amount_if_not_percent__c), ISCHANGED(Settlement_Amount__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
