<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Fee_Goal_Amount_Has_Exceeded_100_Email_Notification</fullName>
        <description>Fee Goal Amount Has Exceeded 100% Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>cruben@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sdavidovitz@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Goal_Amount_Exceeds_110</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_employee_name</fullName>
        <description>Sets employee&apos;s name</description>
        <field>Employee_name__c</field>
        <formula>Employee__r.FirstName &amp;&quot; &quot;&amp; Employee__r.LastName</formula>
        <name>Set employee name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fee Goal - Send Congrats Packet</fullName>
        <actions>
            <name>Congrats_Packet_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fee_Goal__c.Congrats_Packet_Sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Creates a task to indicate that a congrats packet was sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fee Goal Amount Percent Greater than 110%25 Notification</fullName>
        <actions>
            <name>Fee_Goal_Amount_Has_Exceeded_100_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Fee_Goal_Amount_Exceeds_110</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fee_Goal__c.Percentage_of_Goal_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>110</value>
        </criteriaItems>
        <description>Sends a notification to admin staff, to alert that the fee goal has exceeded 110% of the Goal Amount.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set employee name on fee goal</fullName>
        <actions>
            <name>Set_employee_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fee_Goal__c.Employee__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Congrats_Packet_Sent</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The congrats packet was sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Congrats Packet Sent</subject>
    </tasks>
    <tasks>
        <fullName>Fee_Goal_Amount_Exceeds_110</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The fee goal amount has exceeded 110%!</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Fee Goal Amount Exceeds 110%</subject>
    </tasks>
</Workflow>
