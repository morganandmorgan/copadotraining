<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CCC_CAT_Investigation_Canceled_SME_Star_Attorney</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <description>CCC - CAT Investigation Canceled - SME/Star/Attorney</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>CCC_Supervisor</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>investigations@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Canceled_Investigation_VarRolesIIE</template>
    </alerts>
    <alerts>
        <fullName>CCC_Investigation_Scheduled_Client_NL</fullName>
        <description>CCC - Investigation Scheduled - Client NL</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>investigations@nationlaw.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Investigation_Client_NLF</template>
    </alerts>
    <alerts>
        <fullName>Email_Case_Manager_Investigation_Status_Change</fullName>
        <description>Email Case Manager - Investigation Status Change</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Case_Manager_Notification_for_Investigation_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>Investigation_Cancelled_Intake_Agent</fullName>
        <ccEmails>investigations@forthepeople.com</ccEmails>
        <description>CCC - Investigation Cancelled - Intake Agent</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>investigations@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Canceled_Investigation_VarRolesIIE</template>
    </alerts>
    <alerts>
        <fullName>Investigation_Scheduled_Client</fullName>
        <description>CCC - Investigation Scheduled - Client</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>johnbmorgan@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Investigation_Client</template>
    </alerts>
    <alerts>
        <fullName>Investigation_Scheduled_Intake_Agent</fullName>
        <description>CCC - Investigation Scheduled - Intake Agent</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>investigations@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Investigation_Var_Roles_IIE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Case_Manager_Email</fullName>
        <description>Sets email address</description>
        <field>Case_Manager_Email__c</field>
        <formula>Intake__r.Case_Manager_Name__r.Email</formula>
        <name>Set Case Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CCC - Investigation Canceled - Client-Intake-Agent Email</fullName>
        <actions>
            <name>Investigation_Cancelled_Intake_Agent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IntakeInvestigationEvent__c.Investigation_Status__c</field>
            <operation>equals</operation>
            <value>Canceled,Rescheduled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Investigation Scheduled - Client-Intake Agent Email</fullName>
        <actions>
            <name>Investigation_Scheduled_Client</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Investigation_Scheduled_Intake_Agent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IntakeInvestigationEvent__c.Investigation_Status__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>IntakeInvestigationEvent__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <description>only MM handling firm since client gets email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Investigation Scheduled - Client-Intake Agent Email - NL</fullName>
        <actions>
            <name>CCC_Investigation_Scheduled_Client_NL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Investigation_Scheduled_Intake_Agent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IntakeInvestigationEvent__c.Investigation_Status__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>IntakeInvestigationEvent__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <description>Nation Law investigation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Manager Email on Investigation Status Change</fullName>
        <actions>
            <name>Email_Case_Manager_Investigation_Status_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends email on statuschange</description>
        <formula>OR(  AND(ISPICKVAL(Investigation_Status__c, &quot;Scheduled&quot;),ISCHANGED(Case_Manager_Email__c),PRIORVALUE(Case_Manager_Email__c) = &quot;&quot;),  AND(ISCHANGED(Investigation_Status__c), NOT(ISPICKVAL(Investigation_Status__c,&quot;Completed&quot;)))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Case Manager Email</fullName>
        <actions>
            <name>Set_Case_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets email on record</description>
        <formula>Case_Manager_Email__c &lt;&gt; Intake__r.Case_Manager_Name__r.Email</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
