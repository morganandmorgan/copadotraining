<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Agent_Send_JUUL_Questionnaire_from_Matter</fullName>
        <description>Agent - Send JUUL Questionnaire from Matter</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/RR_JUUL_Your_Injury_Questionnaire_Staff_Matter</template>
    </alerts>
    <alerts>
        <fullName>Agent_Send_PFS_Questionnaire_from_Matter</fullName>
        <description>Agent - Send PFS Questionnaire from Matter</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PFS_Questionnaire_Email_from_Matter</template>
    </alerts>
    <alerts>
        <fullName>Agent_Send_Questionnaire_from_Matter</fullName>
        <description>Agent - Send Questionnaire from Matter</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/RR_Your_Injury_Questionnaire_Staff_Matter</template>
    </alerts>
    <alerts>
        <fullName>Cancer_Cross_screening_Email_Alert</fullName>
        <description>Cancer Cross-screening Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>contact@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Cancer_Cross_Screening_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Paralegal_when_matter_is_updated_to_Litigation</fullName>
        <description>Notify Paralegal when matter is updated to Litigation</description>
        <protected>false</protected>
        <recipients>
            <field>Litigation_Paralegal__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Paralegal_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Principal_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Principal_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>litify_pm__Principal_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>notifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Matter_in_Ligation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Pre_Trial_Follow_up_Email_Alert</fullName>
        <description>Pre-Trial Follow-up Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Trial_Partner_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Trial_Partner__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>litify_pm__Principal_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sdavidovitz@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Pre_trial_Follow_up</template>
    </alerts>
    <alerts>
        <fullName>RoundUp_Plaintiffs_Information_Sheet</fullName>
        <description>RoundUp Plaintiffs Information Sheet</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>roundup@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Q_Round_Up_Plaintiffs_Information_Sheet</template>
    </alerts>
    <alerts>
        <fullName>SOC_Change_of_Attorney</fullName>
        <description>SOC - Change of Attorney</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SOC_Change_of_Attorney</template>
    </alerts>
    <alerts>
        <fullName>SOC_Hearing_Request_Notice</fullName>
        <description>SOC - Hearing Request Notice</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SOC_Hearing_Request_Notice</template>
    </alerts>
    <alerts>
        <fullName>SOC_Initial_Claim_Notice</fullName>
        <description>SOC - Initial Claim Notice</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SOC_Initial_Claim_Notice</template>
    </alerts>
    <alerts>
        <fullName>SOC_Recon_Notice</fullName>
        <description>SOC - Recon Notice</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SOC_Recon_Notice</template>
    </alerts>
    <alerts>
        <fullName>SS_File_Fee_Petition</fullName>
        <description>SS - File Fee Petition</description>
        <protected>false</protected>
        <recipients>
            <recipient>berlisramirez@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmalpica@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SS_Matter_Emails/Email_Alerts_Fee_Petition</template>
    </alerts>
    <alerts>
        <fullName>SS_Notify_Owner_of_New_Matter</fullName>
        <description>SS - Notify Owner of New Matter</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SS_Matter_Emails/Email_Alert_New_Matter</template>
    </alerts>
    <alerts>
        <fullName>Trial_Follow_up</fullName>
        <description>Trial Follow-up</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Trial_Partner_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Trial_Partner__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>litify_pm__Principal_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sdavidovitz@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Trial_Follow_up</template>
    </alerts>
    <alerts>
        <fullName>Trial_Partner_Assigned_Email_Alert</fullName>
        <description>Trial Partner Assigned Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Trial_Partner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sdavidovitz@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Trial_Partner_Assigned_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>WC_SocSec_Cross_screening_Email_Alert</fullName>
        <description>WC - SocSec Cross-screening Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>contact@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Cross_screening_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_Delete_Minor_to_Display_Name</fullName>
        <field>litify_pm__Display_Name__c</field>
        <formula>IF(
				Client_is_Minor__c=FALSE,
				litify_pm__Client__r.LastName&amp;&quot;, &quot;&amp;litify_pm__Client__r.FirstName&amp;&quot; vs. SSA&quot;,
				IF(
								AND(
								Client_is_Minor__c=TRUE,
								ISBLANK(Injured_Party__c)
								),
								litify_pm__Client__r.LastName&amp;&quot;, &quot;&amp;litify_pm__Client__r.FirstName&amp;&quot; vs. SSA (Minor)&quot;,
			 Injured_Party__r.LastName&amp;&quot;, &quot;&amp;Injured_Party__r.FirstName&amp;&quot; vs. SSA (Minor)&quot; 
				)
)</formula>
        <name>Add/Delete &quot;(Minor)&quot; to Display Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Prior_Principal_Attorney_ID</fullName>
        <field>Prior_Principal_Attorney__c</field>
        <formula>PRIORVALUE(litify_pm__Principal_Attorney__c)</formula>
        <name>Capture Prior Principal Attorney ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fetch_SSN_from_Account</fullName>
        <description>Populates the Social Security Number Search field with the value from the same named Account field.</description>
        <field>SocialSecurityNumberSearch__c</field>
        <formula>litify_pm__Client__r.SocialSecurityNumberSearch__c</formula>
        <name>Fetch SSN from Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Litigation_Status_Date_Update</fullName>
        <description>adds today&apos;s date to the Litigation Status Date so we only send an email once for when litigation is started</description>
        <field>Litigation_Status_Date__c</field>
        <formula>NOW()</formula>
        <name>Litigation Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PFS_Field_Update</fullName>
        <description>Triggered when a user sets Date PFS Filed.</description>
        <field>PFS__c</field>
        <literalValue>Yes</literalValue>
        <name>PFS Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateMatterReferenceNumber</fullName>
        <description>Populates the litify_pm__Matter__c.ReferenceNumber__c field with a the &quot;intake number&quot; from the litify_pm__Matter__c.Intake__r.Name field.</description>
        <field>ReferenceNumber__c</field>
        <formula>SUBSTITUTE(Intake__r.Name, &quot;INT-&quot;, &quot;&quot;)</formula>
        <name>Populate Matter Reference Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SOC_Client_age_at_conversion</fullName>
        <description>For use in minor cases. Records the client&apos;s age when the matter is converted.</description>
        <field>Client_age_at_conversion__c</field>
        <formula>litify_pm__Client__r.Client_s_Age__c</formula>
        <name>SOC - Client age at conversion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Client_Email_on_Matter</fullName>
        <field>Client_Email__c</field>
        <formula>litify_pm__Client__r.PersonContact.Email</formula>
        <name>Set Client Email on Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_Date</fullName>
        <field>litify_pm__Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Document_Folder_Indicator_to_N_when</fullName>
        <description>Updates the indicator to N which is used in utility to suppress user creation of SpringCM document folders</description>
        <field>Document_Folder_Indicator__c</field>
        <literalValue>N</literalValue>
        <name>Set Document Folder Indicator to N when</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Evaluation_Date_Matter</fullName>
        <field>Evaluation_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Evaluation Date - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Filed_Date_Matter</fullName>
        <field>litify_pm__Filed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Filed Date - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Finance_Closed_Date</fullName>
        <field>Finance_Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Finance Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Template</fullName>
        <description>Sets the first SMS Template</description>
        <field>Treatment_SMS_Next_Template__c</field>
        <formula>IF(ISPICKVAL(litify_pm__Client__r.Language__c,&quot;Spanish&quot;),&quot;15 Day Treatment Matter - Spanish&quot;,&quot;15 Day Treatment Matter&quot;)</formula>
        <name>Set Initial Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Treatment_SMS_Date</fullName>
        <description>Sets initial treatment SMS date 15 days after Retainer Received</description>
        <field>Treatment_SMS_Next_Date__c</field>
        <formula>DATEVALUE(Intake__r.Retainer_Received__c)+15</formula>
        <name>Set Initial Treatment SMS Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Letter_Date</fullName>
        <field>Last_Letter_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Last Letter Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Treatment_Date_to_Today</fullName>
        <field>Last_Treatment_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Last Treatment Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MRI_Completed_Date_Matter</fullName>
        <field>MRI_Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Set MRI Completed Date - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_RO_Pre_Suit</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>Pre Suit</literalValue>
        <name>Set Matter Stage RO - Pre Suit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_Closed</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>Closed</literalValue>
        <name>Set Matter Stage Read Only - Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_DCD</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>DCD</literalValue>
        <name>Set Matter Stage Read Only - DCD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_Filing</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>Filing</literalValue>
        <name>Set Matter Stage Read Only - Filing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_Intake</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>Intake</literalValue>
        <name>Set Matter Stage Read Only - Intake</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_Litigation</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>Litigation</literalValue>
        <name>Set Matter Stage RO - Litigation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_New_Matter</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>New Matter</literalValue>
        <name>Set Matter Stage Read Only - New Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Matter_Stage_Read_Only_Settlement</fullName>
        <field>Matter_Stage_Read_Only__c</field>
        <literalValue>Settlement</literalValue>
        <name>Set Matter Stage Read Only - Settlement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Mediation_Comp_Date_Matter</fullName>
        <field>Mediation_Comp_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Mediation Comp Date - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Mobile_Phone</fullName>
        <field>Client_Mobile_Phone__c</field>
        <formula>litify_pm__Client__r.PersonContact.MobilePhone</formula>
        <name>Set Mobile Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Phone_Number</fullName>
        <field>Client_Phone__c</field>
        <formula>litify_pm__Client__r.Phone</formula>
        <name>Set Phone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Positive_MRI_Finding_Date_Matter</fullName>
        <field>Positive_MRI_Finding_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Positive MRI Finding Date - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Litigation_Date</fullName>
        <field>Stage_to_Litigation_Date__c</field>
        <formula>Today()</formula>
        <name>Set Stage to Litigation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Appeal</fullName>
        <field>Status_to_Appeal__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Appeal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Demand_Matter</fullName>
        <field>Status_to_Demand__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Demand - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Investigation</fullName>
        <field>Status_to_Investigation__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Investigation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_MMI</fullName>
        <field>Status_to_MMI__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to MMI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_New_Case</fullName>
        <field>Status_to_New_Case__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to New Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Pending_Litigation</fullName>
        <field>Status_to_Pending_Litigation__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Pending Litigation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Referred_Out</fullName>
        <field>Status_to_Referred_Out__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Referred Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Settled</fullName>
        <field>Status_to_Settled__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Settled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Treating</fullName>
        <field>Status_to_Treating__c</field>
        <formula>TODAY()</formula>
        <name>Set Status to Treating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Treatment_SMS_Trigger</fullName>
        <field>Treatment_SMS_Trigger__c</field>
        <literalValue>1</literalValue>
        <name>Set Treatment SMS Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Turn_Down_Status_Date_Matter</fullName>
        <field>Turn_Down_Status_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Turn Down Status Date - Matter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Cat_Case_Confirmed</fullName>
        <field>Cat_Case_Confirmed__c</field>
        <literalValue>1</literalValue>
        <name>Update Cat-Case Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Name_SCC_Search</fullName>
        <field>Client_Name_SCC_Search__c</field>
        <formula>litify_pm__Client__r.FirstName &amp; &quot; &quot; &amp; litify_pm__Client__r.LastName</formula>
        <name>Update Client Name SCC Search</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Display_Name</fullName>
        <field>litify_pm__Display_Name__c</field>
        <formula>litify_pm__Client__r.LastName&amp;&quot;, &quot;&amp;litify_pm__Client__r.FirstName&amp; 
IF(Litigation__c=&quot;Social Security&quot;,&quot; vs SSA&quot;,
&quot;&quot;
)</formula>
        <name>Update Display Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Injured_Party_Deceased</fullName>
        <field>Injured_Party_Deceased__c</field>
        <literalValue>1</literalValue>
        <name>Update Injured Party Deceased</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Matter_Last_Modified_Date_User</fullName>
        <field>Last_Modified_Date_User__c</field>
        <formula>NOW()</formula>
        <name>Update Matter.Last Modified Date (User)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Lawsuit_Filed</fullName>
        <description>updates status to Lawsuit Filed</description>
        <field>litify_pm__Status__c</field>
        <literalValue>Lawsuit Filed</literalValue>
        <name>Update Status - Lawsuit Filed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Pending_Litigation</fullName>
        <field>litify_pm__Status__c</field>
        <literalValue>Pending Litigation</literalValue>
        <name>Update Status - Pending Litigation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>litify_pm__Update_Display_Name</fullName>
        <field>litify_pm__Display_Name__c</field>
        <formula>IF(
Not(
Isblank(litify_pm__Case_Type__r.Name)),

litify_pm__Client__r.Name&amp;&quot; Matter &quot;&amp;TEXT(MONTH(DATEVALUE(CreatedDate)))&amp;&quot;/&quot;&amp;TEXT(Year(DATEVALUE(CreatedDate)))&amp;


&quot; (&quot;&amp;litify_pm__Case_Type__r.Name&amp;&quot;)&quot;,

litify_pm__Client__r.Name&amp;&quot; Matter &quot;&amp;TEXT(MONTH(DATEVALUE(CreatedDate)))&amp;&quot;/&quot;&amp;TEXT(Year(DATEVALUE(CreatedDate))))</formula>
        <name>Update Display Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>litify_pm__Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>litify_pm__Non_Billable_Matter</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Post_Case_Survey</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_43dS5jOOVUrkZHD&amp;u=UR_4TwknbVRQHBmtUh&amp;t=OC_1LZ0q5pVoqmiiVd&amp;b=morganmorgan</endpointUrl>
        <fields>Case_Manager_Email__c</fields>
        <fields>Case_Manager_Name__c</fields>
        <fields>Case_Manager__c</fields>
        <fields>Client_Email__c</fields>
        <fields>Client_First_Name__c</fields>
        <fields>Client_Language__c</fields>
        <fields>Client_Last_Name__c</fields>
        <fields>Client_Mobile_Phone__c</fields>
        <fields>Client_Phone__c</fields>
        <fields>Id</fields>
        <fields>Intake__c</fields>
        <fields>Principal_Attorney_Email__c</fields>
        <fields>Principal_Attorney_Name__c</fields>
        <fields>ReferenceNumber__c</fields>
        <fields>Reporting_Case_Type__c</fields>
        <fields>Reporting_Office__c</fields>
        <fields>SMS_Opt_out__c</fields>
        <fields>litify_pm__Client__c</fields>
        <fields>litify_pm__Principal_Attorney__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>dstella@forthepeople.com</integrationUser>
        <name>Post Case Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>X45_Day_Client_Satisfaction_SMS_Survey</fullName>
        <apiVersion>48.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_9N3xlaNrsdsUZ49&amp;u=UR_4TwknbVRQHBmtUh&amp;t=OC_3DchVjobQHRFbD2&amp;b=morganmorgan</endpointUrl>
        <fields>Case_Manager_Email__c</fields>
        <fields>Case_Manager_Name__c</fields>
        <fields>Case_Manager_Phone__c</fields>
        <fields>Case_Manager__c</fields>
        <fields>Client_Email__c</fields>
        <fields>Client_First_Name__c</fields>
        <fields>Client_Language__c</fields>
        <fields>Client_Last_Name__c</fields>
        <fields>Client_Mobile_Phone__c</fields>
        <fields>Client_Phone__c</fields>
        <fields>Id</fields>
        <fields>Intake__c</fields>
        <fields>Principal_Attorney_Email__c</fields>
        <fields>Principal_Attorney_Name__c</fields>
        <fields>ReferenceNumber__c</fields>
        <fields>Reporting_Case_Type__c</fields>
        <fields>Reporting_Office__c</fields>
        <fields>SMS_Opt_out__c</fields>
        <fields>litify_pm__Client__c</fields>
        <fields>litify_pm__Principal_Attorney__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>dstella@forthepeople.com</integrationUser>
        <name>45 Day Client Satisfaction SMS Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>X45_Day_Client_Satisfaction_Survey</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_9N3xlaNrsdsUZ49&amp;u=UR_4TwknbVRQHBmtUh&amp;t=OC_2CE1O8ALG77dYbT&amp;b=morganmorgan</endpointUrl>
        <fields>Case_Manager_Email__c</fields>
        <fields>Case_Manager_Name__c</fields>
        <fields>Case_Manager__c</fields>
        <fields>Client_Email__c</fields>
        <fields>Client_First_Name__c</fields>
        <fields>Client_Language__c</fields>
        <fields>Client_Last_Name__c</fields>
        <fields>Client_Mobile_Phone__c</fields>
        <fields>Client_Phone__c</fields>
        <fields>Id</fields>
        <fields>Intake__c</fields>
        <fields>Principal_Attorney_Email__c</fields>
        <fields>Principal_Attorney_Name__c</fields>
        <fields>ReferenceNumber__c</fields>
        <fields>Reporting_Case_Type__c</fields>
        <fields>Reporting_Office__c</fields>
        <fields>SMS_Opt_out__c</fields>
        <fields>litify_pm__Client__c</fields>
        <fields>litify_pm__Principal_Attorney__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>dstella@forthepeople.com</integrationUser>
        <name>45 Day Client Satisfaction Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>X90_Day_Client_Satisfaction_Survey</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_9N3xlaNrsdsUZ49&amp;u=UR_4TwknbVRQHBmtUh&amp;t=OC_2CE1O8ALG77dYbT&amp;b=morganmorgan</endpointUrl>
        <fields>Case_Manager_Email__c</fields>
        <fields>Case_Manager_Name__c</fields>
        <fields>Case_Manager__c</fields>
        <fields>Client_Email__c</fields>
        <fields>Client_First_Name__c</fields>
        <fields>Client_Language__c</fields>
        <fields>Client_Last_Name__c</fields>
        <fields>Client_Mobile_Phone__c</fields>
        <fields>Client_Phone__c</fields>
        <fields>Id</fields>
        <fields>Intake__c</fields>
        <fields>Principal_Attorney_Email__c</fields>
        <fields>Principal_Attorney_Name__c</fields>
        <fields>ReferenceNumber__c</fields>
        <fields>Reporting_Case_Type__c</fields>
        <fields>Reporting_Office__c</fields>
        <fields>SMS_Opt_out__c</fields>
        <fields>litify_pm__Client__c</fields>
        <fields>litify_pm__Principal_Attorney__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>dstella@forthepeople.com</integrationUser>
        <name>90 Day Client Satisfaction Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Capture Prior Principal Attorney</fullName>
        <actions>
            <name>Capture_Prior_Principal_Attorney_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(litify_pm__Principal_Attorney__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Client SMS Reply Indicates Treatment</fullName>
        <actions>
            <name>Set_Last_Treatment_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Treatment_SMS_Latest_Reply__c</field>
            <operation>equals</operation>
            <value>1,Y,Yes</value>
        </criteriaItems>
        <description>Update Matter when client signals continued treatment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy SSN from Account</fullName>
        <actions>
            <name>Fetch_SSN_from_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For any CRUD operation, retrieve the SSN value from the Account and populate the Social Security Number Search field.</description>
        <formula>OR( 				AND( 								ISBLANK(SocialSecurityNumberSearch__c), 								NOT(ISBLANK(litify_pm__Client__r.SocialSecurityNumberSearch__c)) 				), 				ISNEW(), 				ISCHANGED(litify_pm__Client__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Determine If Matter Reference Number Needs Populating</fullName>
        <actions>
            <name>PopulateMatterReferenceNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rules determines if the litify_pm__Matter__c.ReferenceNumber__c field needs populating when the litify_pm__Matter__c record is created.</description>
        <formula>NOT( ISNULL( Intake__c ) ) &amp;&amp; ISBLANK( ReferenceNumber__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification to Paralegal when Stage %3D Litigation</fullName>
        <actions>
            <name>Notify_Paralegal_when_matter_is_updated_to_Litigation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Litigation_Status_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Matter_Stage__c</field>
            <operation>equals</operation>
            <value>Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Filed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Stage_to_Litigation_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>notEqual</operation>
            <value>Turn Down,Referred Out,Settled,Appeal</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Finance_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Litigation_Status_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send email to paralegal when stage = Litigation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mass Tort - Med Mal Cross Screening Notification</fullName>
        <active>true</active>
        <description>Sends email notification to Workers Comp clients, to offer an opportunity to assist them with a potential Social Security case.</description>
        <formula>AND(
RecordType.Name = &quot;Mass Tort&quot;,
ISBLANK(Turn_Down_Indicator__c),
OR(
ISPICKVAL(Intake__r.Case_Type__c, &quot;Losartan&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Amlodipine&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Irbesartan&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Hydrochlorothiazide&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Xarelto&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Talcum Powder&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Roundup&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Victoza&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Pradaxa&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Taxotere&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Januvia&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Docetaxel&quot;),
ISPICKVAL(Intake__r.Case_Type__c, &quot;Valsartan&quot;)
), 
NOT(Injured_Party_Deceased__c),
Intake__r.Client_Email_txt__c &lt;&gt; &quot;none@none.com&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cancer_Cross_screening_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Cross_screening_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.Converted_Notification_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Qualtrics - 45 Day Survey - English</fullName>
        <active>true</active>
        <description>Triggers the 45 Day Survey [English] to go out to the client.</description>
        <formula>AND(
 ISPICKVAL(litify_pm__Client__r.Language__c, &quot;English&quot;),
 NOT(ISPICKVAL(Intake__r.Litigation__c, &quot;Social Security&quot;)),
 NOT(ISPICKVAL(Intake__r.Litigation__c, &quot;Workers Compensation&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;PIP&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;Boy Scouts&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;Childhood Sex Abuse&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;JUUL&quot;)),
 ISPICKVAL(Intake__r.Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
 Intake__r.Assigned_Office__r.Name &lt;&gt; &quot;Nation Law&quot;,
 NOT(ISPICKVAL(Intake__r.What_type_of_Class_Action_case_is_it__c, &quot;Equifax Breach&quot;)),
 NOT(ISBLANK( litify_pm__Open_Date__c )),
 TODAY()-litify_pm__Open_Date__c &lt;= 45,
 litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;&quot;,
 litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;none@none.com&quot;,
 litify_pm__Client__r.PersonContact.HasOptedOutOfEmail = False,
 ISBLANK(Turn_Down_Indicator__c),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Closed&quot;)),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Settled&quot;)),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Referred Out&quot;)),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Duplicate&quot;))
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X45_Day_Client_Satisfaction_SMS_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X45_Day_Client_Satisfaction_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X45_Day_Survey_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Open_Date__c</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Qualtrics - 45 Day Survey - Spanish</fullName>
        <active>true</active>
        <description>Triggers the 45 Day Survey [Spanish] to go out to the client.</description>
        <formula>AND(
 ISPICKVAL(litify_pm__Client__r.Language__c, &quot;Spanish&quot;),
 NOT(ISPICKVAL(Intake__r.Litigation__c, &quot;Social Security&quot;)),
 NOT(ISPICKVAL(Intake__r.Litigation__c, &quot;Workers Compensation&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;PIP&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;Boy Scouts&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;Childhood Sex Abuse&quot;)),
 NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;JUUL&quot;)),
 ISPICKVAL(Intake__r.Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
 Intake__r.Assigned_Office__r.Name &lt;&gt; &quot;Nation Law&quot;,
 NOT(ISPICKVAL(Intake__r.What_type_of_Class_Action_case_is_it__c, &quot;Equifax Breach&quot;)),
 NOT(ISBLANK( litify_pm__Open_Date__c )),
 TODAY()-litify_pm__Open_Date__c &lt;= 45,
 litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;&quot;,
 litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;none@none.com&quot;,
 litify_pm__Client__r.PersonContact.HasOptedOutOfEmail = False,
 ISBLANK(Turn_Down_Indicator__c),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Closed&quot;)),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Settled&quot;)),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Referred Out&quot;)),
 NOT(CONTAINS( Reporting_Case_Status__c , &quot;Duplicate&quot;))
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X45_Day_Client_Satisfaction_SMS_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X45_Day_Client_Satisfaction_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X45_Day_Survey_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Open_Date__c</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Qualtrics - 90 Day Survey - English</fullName>
        <active>true</active>
        <description>Triggers the 90 Day Survey [English] to go out to the client.</description>
        <formula>AND( OR( ISPICKVAL(litify_pm__Client__r.Language__c, &quot;English&quot;), ISPICKVAL(litify_pm__Client__r.Language__c, &quot;&quot;) ),   ISPICKVAL(Intake__r.Litigation__c, &quot;Workers Compensation&quot;),   NOT(ISBLANK( litify_pm__Open_Date__c )),  TODAY()-litify_pm__Open_Date__c &lt;= 90,  ISPICKVAL(Intake__r.Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),  Intake__r.Assigned_Office__r.Name  &lt;&gt; &quot;Nation Law&quot;, litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;&quot;,   litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;none@none.com&quot;,   litify_pm__Client__r.PersonContact.HasOptedOutOfEmail = False,   ISBLANK(Turn_Down_Indicator__c),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Closed&quot;)),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Settled&quot;)),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Referred Out&quot;)),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Duplicate&quot;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X45_Day_Client_Satisfaction_SMS_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X90_Day_Client_Satisfaction_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X90_Day_Survey_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Open_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Qualtrics - 90 Day Survey - Spanish</fullName>
        <active>true</active>
        <description>Triggers the 90 Day Survey [Spanish] to go out to the client.</description>
        <formula>AND(   ISPICKVAL(litify_pm__Client__r.Language__c, &quot;Spanish&quot;),   ISPICKVAL(Intake__r.Litigation__c, &quot;Workers Compensation&quot;),   ISPICKVAL(Intake__r.Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),   Intake__r.Assigned_Office__r.Name &lt;&gt; &quot;Nation Law&quot;, NOT(ISBLANK( litify_pm__Open_Date__c )),  TODAY()-litify_pm__Open_Date__c &lt;= 90,  litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;&quot;,   litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;none@none.com&quot;,   litify_pm__Client__r.PersonContact.HasOptedOutOfEmail = False,   ISBLANK(Turn_Down_Indicator__c),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Closed&quot;)),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Settled&quot;)),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Referred Out&quot;)),   NOT(CONTAINS( Reporting_Case_Status__c , &quot;Duplicate&quot;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X45_Day_Client_Satisfaction_SMS_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X90_Day_Client_Satisfaction_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>X90_Day_Survey_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Open_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Qualtrics - Post Case Survey</fullName>
        <active>true</active>
        <description>Triggers the closing survey to go out to the client.</description>
        <formula>AND(
NOT(ISBLANK( Earliest_Attorney_Fee_Amount__c )),
NOT(ISBLANK( Resolution_Date__c )),
TODAY()-Resolution_Date__c &lt;= 7,
litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;&quot;,
litify_pm__Client__r.PersonContact.Email &lt;&gt; &quot;none@none.com&quot;,
litify_pm__Client__r.PersonContact.HasOptedOutOfEmail = False,
litify_pm__Client__r.SMS_Text_Automated_Communication_OptOut__c = False,
ISPICKVAL(Intake__r.Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
Intake__r.Assigned_Office__r.Name &lt;&gt; &quot;Nation Law&quot;,
NOT(ISPICKVAL(Intake__r.Litigation__c, &quot;Workers Compensation&quot;)),
NOT(ISPICKVAL(Intake__r.Case_Type__c, &quot;PIP&quot;)),
ISBLANK(Turn_Down_Indicator__c)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Post_Case_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>Closing_Survey_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.Resolution_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SMS - Recurring Matter Treatment</fullName>
        <active>true</active>
        <description>Sets triggers for recurring treatment SMS</description>
        <formula>IF( AND( $Profile.Name=&quot;M&amp;M System Administrator Modified&quot;, NOT(Treatment_SMS_Trigger__c),  NOT(litify_pm__Client__r.tdc_tsw__SMS_Opt_out__c),  NOT(litify_pm__Client__r.PersonContact.tdc_tsw__SMS_Opt_out__c),  NOT(Treatment_SMS_Opt_Out__c),  NOT(ISPICKVAL(Intake__r.Injured_party_deceased__c,&quot;Yes&quot;)),  Injured_Party_Deceased__c=FALSE,  LOWER(Reporting_Matter_Stage__c)&lt;&gt;&quot;closed&quot;, OR( LOWER(Reporting_Law_Type__c)=&quot;personal injury&quot;, LOWER(Reporting_Law_Type__c)=&quot;premises liability&quot;, LOWER(Reporting_Law_Type__c)=&quot;litigation&quot;, LOWER(Reporting_Law_Type__c)=&quot;admiralty&quot; ), NOT(ISPICKVAL(litify_pm__Status__c,&quot;Settled&quot;)),  NOT(ISPICKVAL(litify_pm__Status__c,&quot;Turned Down&quot;)),  NOT(ISPICKVAL(litify_pm__Status__c,&quot;Referred Out&quot;)),  NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;burn&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;product liability&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;civil rights&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;pip suit&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;sexual assault&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;rape&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;molestation&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;food poisoning&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;products liability&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;dog bite&quot;)), RecordType.DeveloperName&lt;&gt;&quot;Administrative&quot;, NOT(ISPICKVAL(No_Treatment_Reason_PL__c,&quot;Dog Bite&quot;)), NOT(ISPICKVAL(No_Treatment_Reason_PL__c,&quot;Fracture&quot;)), NOT(ISPICKVAL(No_Treatment_Reason_PL__c,&quot;Other part of body injury/scarring&quot;)) ),  True,  False  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Treatment_SMS_Trigger</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.Treatment_SMS_Next_Date__c</offsetFromField>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SMS - Set Matter Treatment Date</fullName>
        <actions>
            <name>Set_Initial_Template</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Initial_Treatment_SMS_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the initial treatment SMS date, sets schedule for all recurring dates</description>
        <formula>IF( AND( $Profile.Name=&quot;M&amp;M System Administrator Modified&quot;, TODAY()-DATEVALUE(Intake__r.Retainer_Received__c)&lt;= 14, NOT(litify_pm__Client__r.tdc_tsw__SMS_Opt_out__c), NOT(litify_pm__Client__r.PersonContact.tdc_tsw__SMS_Opt_out__c), NOT(Treatment_SMS_Opt_Out__c), NOT(ISPICKVAL(Intake__r.Injured_party_deceased__c,&quot;Yes&quot;)), Injured_Party_Deceased__c=FALSE, LOWER(Reporting_Matter_Stage__c)&lt;&gt;&quot;closed&quot;, OR( LOWER(Reporting_Law_Type__c)=&quot;personal injury&quot;, LOWER(Reporting_Law_Type__c)=&quot;premises liability&quot;, LOWER(Reporting_Law_Type__c)=&quot;litigation&quot;, LOWER(Reporting_Law_Type__c)=&quot;admiralty&quot; ), NOT(ISPICKVAL(litify_pm__Status__c,&quot;Settled&quot;)), NOT(ISPICKVAL(litify_pm__Status__c,&quot;Turned Down&quot;)), NOT(ISPICKVAL(litify_pm__Status__c,&quot;Referred Out&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;burn&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;product liability&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;civil rights&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;pip suit&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;sexual assault&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;rape&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;molestation&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;food poisoning&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;products liability&quot;)), NOT(CONTAINS(LOWER(Reporting_Case_Type__c),&quot;dog bite&quot;)), RecordType.DeveloperName&lt;&gt;&quot;Administrative&quot;, NOT(ISPICKVAL(No_Treatment_Reason_PL__c,&quot;Dog Bite&quot;)), NOT(ISPICKVAL(No_Treatment_Reason_PL__c,&quot;Fracture&quot;)), NOT(ISPICKVAL(No_Treatment_Reason_PL__c,&quot;Other part of body injury/scarring&quot;)) ), True, False )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SOC - %22%28Minor%29%22 Display Name Update</fullName>
        <actions>
            <name>Add_Delete_Minor_to_Display_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 				RecordType.DeveloperName=&quot;Social_Security&quot;, 				OR( 								AND( 												Client_is_Minor__c=FALSE, 												UPPER(RIGHT(litify_pm__Display_Name__c,8))=&quot; (MINOR)&quot; 								), 								AND( 												Client_is_Minor__c=TRUE, 												UPPER(RIGHT(litify_pm__Display_Name__c,7))&lt;&gt;&quot;(MINOR)&quot; 								) 				) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SOC - Client age at conversion time</fullName>
        <actions>
            <name>SOC_Client_age_at_conversion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <description>Records the client&apos;s age at the time the matter is converted.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SOC - Update Display Name when Client changes</fullName>
        <actions>
            <name>Update_Display_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 				 				RecordType.Name=&quot;Social Security&quot;, 				 				OR( 								 								ISBLANK(litify_pm__Display_Name__c), 								 								ISCHANGED(litify_pm__Client__c) 				 				)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SS - 120 Day RFR Status Check Up</fullName>
        <actions>
            <name>X120_Day_RFR_Status_Check</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>If a decision letter has not been returned within 120 days of the RFR Filing date, create a task to Follow up on the status of the case</description>
        <formula>AND(ISBLANK(X2nd_Denial_Date__c),NOT(ISBLANK(RFR_Filed__c)),TODAY() - RFR_Filed__c &gt;= 120,CONTAINS( RecordType.DeveloperName, &quot;Social_Security&quot; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS - 30 Day RFR Follow Up</fullName>
        <actions>
            <name>X30_Day_RFR_Follow_UP</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>If the RFR packet has not been received (the RFR Packet received date is empty) 30 days after the RFR packet sent date, then create a task to follow up on the RFR due that day</description>
        <formula>AND(ISBLANK(RFR_Packet_Received__c),NOT(ISBLANK(RFR_Packet_Sent__c)),TODAY() - RFR_Packet_Sent__c &gt;= 30,CONTAINS( RecordType.DeveloperName, &quot;Social_Security&quot; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS - Fee Petition</fullName>
        <actions>
            <name>Fee_Petition_For_Matter</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Fee_Agreement_Status__c</field>
            <operation>equals</operation>
            <value>Not Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS - Hearing Date Tasks</fullName>
        <actions>
            <name>Pull_ERE_for_Initial_Hearing</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Submit_Final_ERE_Upload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Hearing_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS - Post Initial Hearing ERE Check</fullName>
        <actions>
            <name>X150_Day_Post_Hearing_ERE_Chec</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X90_Day_Post_Hearing_ERE_Chec</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Hearing_Attended__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <description>After the Hearing Attended date is filled in, create 90 day and 150 day post hearing ere check.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS - Time Based RFH FU%27s</fullName>
        <active>false</active>
        <description>If the RFH packed has not been received 30 &amp; 45days after the RFH sent date, then create a task to 30  &amp; 45 Day FU on RFH due TODAY</description>
        <formula>ISBLANK(RFH_Packet_Received__c) &amp;&amp; NOT(ISBLANK(RFH_Packet_Sent__c)) &amp;&amp; CONTAINS( RecordType.DeveloperName, &quot;Social_Security&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X30_Day_Follow_up_on_RFH</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.RFH_Packet_Sent__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>X45_Day_Follow_up_on_RFH</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.RFH_Packet_Sent__c</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Cat-Case Confirmed</fullName>
        <actions>
            <name>Update_Cat_Case_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Injured_Party_Deceased__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Cat_Case_Confirmed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Phone on Matter</fullName>
        <actions>
            <name>Set_Client_Email_on_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Mobile_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Phone_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets phone number for searching</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Closed Date</fullName>
        <actions>
            <name>Set_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Matter_Stage__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Document Folder Indicator to N when Status %3D Duplicate</fullName>
        <actions>
            <name>Set_Document_Folder_Indicator_to_N_when</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Status__c</field>
            <operation>equals</operation>
            <value>Duplicate</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Evaluation Date - Matter</fullName>
        <actions>
            <name>Set_Evaluation_Date_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Evaluation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Ortho_Eval__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Filed Date - Matter</fullName>
        <actions>
            <name>Set_Filed_Date_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Filed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>Lawsuit Filed</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>Litigation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Finance Closed Date</fullName>
        <actions>
            <name>Set_Finance_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Finance_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Finance_Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Injured Party Deceased</fullName>
        <actions>
            <name>Update_Injured_Party_Deceased</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Deceased_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Last Letter Date</fullName>
        <actions>
            <name>Set_Last_Letter_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Sub_Status__c</field>
            <operation>equals</operation>
            <value>Letter 1 Complete,Letter 2 Complete,Letter 3 Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MRI Completed Date - Matter</fullName>
        <actions>
            <name>Set_MRI_Completed_Date_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.MRI_Completed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.MRI_Completed__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - Closed</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;Closed&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - DCD</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_DCD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;DCD&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - Filing</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_Filing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;Filing&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - Intake</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;Intake&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - Litigation</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_Litigation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set to litigation for reports</description>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;Litigation&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - New Matter</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_New_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;New Matter&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - Pre Suit</fullName>
        <actions>
            <name>Set_Matter_Stage_RO_Pre_Suit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;Pre Suit&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stage Read Only - Settlement</fullName>
        <actions>
            <name>Set_Matter_Stage_Read_Only_Settlement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>litify_pm__Matter_Stage_Activity__r.Name = &quot;Settlement&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Mediation Comp Date - Matter</fullName>
        <actions>
            <name>Set_Mediation_Comp_Date_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Mediation_Comp__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Mediation_Comp_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Positive MRI Finding Date - Matter</fullName>
        <actions>
            <name>Set_Positive_MRI_Finding_Date_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Positive_MRI_Finding_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Positive_MRI_Finding__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage to Litigation Date</fullName>
        <actions>
            <name>Set_Stage_to_Litigation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Matter_Stage__c</field>
            <operation>equals</operation>
            <value>Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Stage_to_Litigation_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Appeal</fullName>
        <actions>
            <name>Set_Status_to_Appeal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>startsWith</operation>
            <value>Appeal</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Appeal__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Demand - Matter</fullName>
        <actions>
            <name>Set_Status_to_Demand_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Demand__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>startsWith</operation>
            <value>Demand</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Investigation</fullName>
        <actions>
            <name>Set_Status_to_Investigation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>Investigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Investigation__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to MMI</fullName>
        <actions>
            <name>Set_Status_to_MMI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>MMI</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_MMI__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to New Case</fullName>
        <actions>
            <name>Set_Status_to_New_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>New Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_New_Case__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Pending Litigation</fullName>
        <actions>
            <name>Set_Status_to_Pending_Litigation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Pending_Litigation__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>Pending Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Referred Out</fullName>
        <actions>
            <name>Set_Status_to_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Referred_Out__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Status__c</field>
            <operation>startsWith</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Settled</fullName>
        <actions>
            <name>Set_Status_to_Settled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>startsWith</operation>
            <value>Settled</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Settled__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Treating</fullName>
        <actions>
            <name>Set_Status_to_Treating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Reporting_Case_Status__c</field>
            <operation>equals</operation>
            <value>Treating</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.Status_to_Treating__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Turn Down Status Date - Matter</fullName>
        <actions>
            <name>Set_Turn_Down_Status_Date_Matter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNULL(Turn_Down_Status_Date__c) &amp;&amp; (CONTAINS(UPPER(Reporting_Case_Status__c),&quot;FIRE&quot;) || CONTAINS(UPPER(Reporting_Case_Status__c),&quot;TURN&quot;) || CONTAINS(UPPER(Reporting_Case_Status__c),&quot;WITHD&quot;) ) &amp;&amp; RecordTypeId &lt;&gt; &quot;0121J000000uH0FQAU&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Trial Date is Set</fullName>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Trial_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Gross_Recovery__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Status__c</field>
            <operation>notEqual</operation>
            <value>Closed,Settled,Turned Down,Referred Out,Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Trial_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Trial_Follow_up</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.Trial_Notification_Date__c</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Pre_Trial_Follow_up_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Trial_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Pre_Trial_Follow_up_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Trial_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Pre_Trial_Follow_up_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Trial_Date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Pre_Trial_Follow_up_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.litify_pm__Trial_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Trial Partner Assigned to Case Email</fullName>
        <actions>
            <name>Trial_Partner_Assigned_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Trial_Partner_Assigned_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the Trial Partner when one is assigned to the matter.</description>
        <formula>ISCHANGED( Trial_Partner__c )  &amp;&amp;  litify_pm__Trial_Date__c  &gt;= TODAY()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Client Name SCC Search</fullName>
        <actions>
            <name>Update_Client_Name_SCC_Search</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Client_Name__c &lt;&gt; Client_Name_SCC_Search__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PFS</fullName>
        <actions>
            <name>PFS_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.Date_PFS_Filed__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates PFS field when Date PFS Filed is set.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>User Modifies Matter</fullName>
        <actions>
            <name>Update_Matter_Last_Modified_Date_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fires whenever a non-admin user modifies a matter record</description>
        <formula>$Profile.Name&lt;&gt;&quot;M&amp;M System Administrator Modified&quot;  &amp;&amp; 
$Profile.Name&lt;&gt;&quot;M&amp;M Accounting&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Workers Comp - Social Security Cross Screening Notification</fullName>
        <active>true</active>
        <description>Sends email notification to Workers Comp clients, to offer an opportunity to assist them with a potential Social Security case.</description>
        <formula>RecordType.Name = &quot;Workers Compensation&quot;
&amp;&amp;
ISBLANK(Turn_Down_Indicator__c)
&amp;&amp;
NOT(Injured_Party_Deceased__c)
&amp;&amp;
Intake__r.Client_Email_txt__c &lt;&gt; &quot;none@none.com&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>WC_SocSec_Cross_screening_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Cross_screening_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>litify_pm__Matter__c.Converted_Notification_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>litify_pm__Non-Billable Matter Record Type</fullName>
        <actions>
            <name>litify_pm__Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Matter__c.litify_pm__Billable_Matter__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>litify_pm__Update Matter Display Label</fullName>
        <actions>
            <name>litify_pm__Update_Display_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Deprecated - No longer in use as of 19.9001</description>
        <formula>OR( ISNEW(), ISCHANGED( litify_pm__Client__c), ISCHANGED( litify_pm__Case_Type__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Closing_Survey_Sent</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Closing Survey Sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Closing Survey Sent</subject>
    </tasks>
    <tasks>
        <fullName>Cross_screening_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Used for tracking if cross-screening email was sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Cross-screening Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Fee_Petition_For_Matter</fullName>
        <assignedTo>cmanick@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Fee Petition For Matter</subject>
    </tasks>
    <tasks>
        <fullName>Pull_ERE_for_Initial_Hearing</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>litify_pm__Matter__c.Hearing_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Pull ERE for Initial Hearing</subject>
    </tasks>
    <tasks>
        <fullName>Send_30_Day_FU_on_Client_Cost_Due</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>litify_pm__Matter__c.Statement_of_Cost_Sent__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send 30 Day FU on Client Cost Due</subject>
    </tasks>
    <tasks>
        <fullName>Send_sixty_Day_FU_on_Client_Cost_Due</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>60</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>litify_pm__Matter__c.Statement_of_Cost_Sent__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send 60 Day FU on Client Cost Due</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Final_ERE_Upload</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>-7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>litify_pm__Matter__c.Hearing_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Final ERE Upload</subject>
    </tasks>
    <tasks>
        <fullName>Trial_Partner_Assigned_Email_Sent</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The Trial Partner notification email was sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Trial Partner Assigned - Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>X120_Day_RFR_Status_Check</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>120 Day RFR Status Check</subject>
    </tasks>
    <tasks>
        <fullName>X120_Day_RFR_Status_Check_Up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>120 Day RFR Status Check Up</subject>
    </tasks>
    <tasks>
        <fullName>X150_Day_Post_Hearing_ERE_Chec</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>150</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>litify_pm__Matter__c.Hearing_Attended__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>150 Day Post Hearing ERE Check</subject>
    </tasks>
    <tasks>
        <fullName>X30_Day_Follow_up_on_RFH</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>30 Day Follow up on RFH</subject>
    </tasks>
    <tasks>
        <fullName>X30_Day_RFR_Follow_UP</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>30 Day RFR Follow UP</subject>
    </tasks>
    <tasks>
        <fullName>X45_Day_Follow_up_on_RFH</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>45</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>45 Day Follow up on RFH</subject>
    </tasks>
    <tasks>
        <fullName>X45_Day_Survey_Sent</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>45 Day Survey Sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>45 Day Survey Sent</subject>
    </tasks>
    <tasks>
        <fullName>X90_Day_Post_Hearing_ERE_Chec</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>litify_pm__Matter__c.Hearing_Attended__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>90 Day Post Hearing ERE Check</subject>
    </tasks>
    <tasks>
        <fullName>X90_Day_Survey_Sent</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>90 Day Survey Sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>90 Day Survey Sent</subject>
    </tasks>
</Workflow>
