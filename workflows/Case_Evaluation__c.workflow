<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Step_Status_Alert</fullName>
        <description>Approval Step Status Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>gbosseler@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>msimmons@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Case_Evaluation_Approval_Outcome_Notification</template>
    </alerts>
    <alerts>
        <fullName>Approval_Step_Status_Alert_TEST</fullName>
        <description>Approval Step Status Alert - TEST</description>
        <protected>false</protected>
        <recipients>
            <recipient>ksadler@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Case_Evaluation_Approval_Outcome_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Approval_Date</fullName>
        <field>Approved_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Update Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Rejected_Date</fullName>
        <field>Rejected_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_for_Approval_Date</fullName>
        <field>Submitted_for_Approval_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Update Submitted for Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_for_Approval_Status</fullName>
        <field>Status__c</field>
        <literalValue>Submitted For Approval</literalValue>
        <name>Update Submitted for Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
