<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Send_to_Flow</fullName>
        <description>Sends duplicate item to flow for processing to create conflict if one exists.</description>
        <field>Send_to_Flow__c</field>
        <literalValue>1</literalValue>
        <name>Send to Flow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Duplicate Record Item is Intake</fullName>
        <actions>
            <name>Send_to_Flow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DuplicateRecordItem.Conflict_Source_has_Intake__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets send to flow if conflict source record has intake associated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TST</fullName>
        <active>false</active>
        <criteriaItems>
            <field>DuplicateRecordItem.Conflict_Source_has_Intake__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets send to flow if conflict source record has intake associated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
