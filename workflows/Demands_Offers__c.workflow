<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Status_Update_Email</fullName>
        <description>Send Status Update Email - Demand/Offer Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Demands_Offers_Approval_Result</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Description_of_Incident</fullName>
        <description>Sets from matter</description>
        <field>Description_of_Incident__c</field>
        <formula>Matter__r.Description_of_Incident__c</formula>
        <name>Set Description of Incident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LCP_Information</fullName>
        <description>Concatenates LCP Info</description>
        <field>LCP_Details__c</field>
        <formula>&quot;No LCP Approved: &quot;&amp;BR()&amp;IF(Matter__r.No_LCP_Approved__c,&quot;True&quot;,&quot;False&quot;)&amp;BR()&amp; 
BR()&amp;
&quot;No LCP Reason: &quot;&amp;BR()&amp;Matter__r.No_LCP_Reason__c</formula>
        <name>Set LCP Information</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Demand_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Demand Approved</literalValue>
        <name>Set Status - Demand Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Offer_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Offer Approved</literalValue>
        <name>Set Status - Offer Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Offer_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Offer Rejected</literalValue>
        <name>Set Status - Offer Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_Demand_Rejected</fullName>
        <description>Sets status to rejected</description>
        <field>Approval_Status__c</field>
        <literalValue>Demand Rejected</literalValue>
        <name>Set status - Demand Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_Draft</fullName>
        <description>Recall sets to draft</description>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Demand_in_Process</fullName>
        <description>Set approval status to demand in process</description>
        <field>Approval_Status__c</field>
        <literalValue>Demand Approval in Process</literalValue>
        <name>Update Status - Demand in Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_Offer_in_Process</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Offer Approval in Process</literalValue>
        <name>Update Status - Offer in Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Fields on Demand %26 Offer</fullName>
        <actions>
            <name>Set_Description_of_Incident</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_LCP_Information</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Demands_Offers__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set fields on demand/offers for email template etc</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
