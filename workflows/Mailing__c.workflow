<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Mailing_Outbound_Message</fullName>
        <apiVersion>48.0</apiVersion>
        <description>Sends Mailing object data to service bus</description>
        <endpointUrl>https://prod-62.eastus2.logic.azure.com:443/workflows/215daf2852d24f1d82b929f3b44d88a4/triggers/manual/paths/invoke?api-version=2016-10-01&amp;sp=%2Ftriggers%2Fmanual%2Frun&amp;sv=1.0&amp;sig=LjYH_iFBcge1vZq0Tb8IdHlDmcAVfwXDUbmwRvi6jtA</endpointUrl>
        <fields>Attn__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>Document_Source__c</fields>
        <fields>Documents__c</fields>
        <fields>Error_Code__c</fields>
        <fields>Id</fields>
        <fields>Mailing_Job_ID__c</fields>
        <fields>Matter_Law_Type__c</fields>
        <fields>Matter_Number__c</fields>
        <fields>Matter__c</fields>
        <fields>Name</fields>
        <fields>Office_Code__c</fields>
        <fields>OwnerId</fields>
        <fields>Recipient_City__c</fields>
        <fields>Recipient_Name__c</fields>
        <fields>Recipient_Postal_Code__c</fields>
        <fields>Recipient_State__c</fields>
        <fields>Recipient_Street__c</fields>
        <fields>Return_Envelope__c</fields>
        <fields>Status__c</fields>
        <fields>SystemModstamp</fields>
        <fields>Tracking_Number__c</fields>
        <fields>Type__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>ksadler@forthepeople.com</integrationUser>
        <name>Mailing Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Mailing Outbound Message - Pending</fullName>
        <actions>
            <name>Mailing_Outbound_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mailing__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <description>Sends outbound message to the mailing service bus for processing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
