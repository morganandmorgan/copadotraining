<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Audit_Flag_Set_Status_to_In_Progress</fullName>
        <description>When ownership is changed, set the status to in progress</description>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Audit Flag - Set Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Audit Flag - Set tatus in Progress when Accepted</fullName>
        <actions>
            <name>Audit_Flag_Set_Status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update to in progress when record owner is changed from queue</description>
        <formula>ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
