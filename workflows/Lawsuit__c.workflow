<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Case_Manager_Case_posted_to_CP</fullName>
        <description>Email Case Manager - Case posted to CP</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Lawsuit_Posted_to_Client_Profiles</template>
    </alerts>
    <alerts>
        <fullName>Send_AN_Alert</fullName>
        <ccEmails>clientsatisfaction@forthepeople.com</ccEmails>
        <description>Send AN Alert</description>
        <protected>false</protected>
        <senderAddress>clientsatisfaction@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Ask_Nicely_Low_Score</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Lawsuit_External_ID</fullName>
        <description>Populates the Lawsuit__c.External_ID__c field with a seed value from the External_ID_Seed__c</description>
        <field>External_ID__c</field>
        <formula>External_Id_Seed__c</formula>
        <name>Populate Lawsuit External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Manager_Email_on_Lawsuit</fullName>
        <description>Sets email address on lawsuit</description>
        <field>Case_Manager_Email__c</field>
        <formula>Intake__r.Case_Manager_Name__r.Email</formula>
        <name>Set Case Manager Email on Lawsuit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Client_Email_on_Lawsuit</fullName>
        <description>Set email on lawsuit</description>
        <field>Client_Email__c</field>
        <formula>AccountId__r.PersonContact.Email</formula>
        <name>Set Client Email on Lawsuit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Demand_Date</fullName>
        <field>Demand_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Demand Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Evaluation_Date</fullName>
        <field>Evaluation_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Evaluation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Template</fullName>
        <description>Defines initial template to use</description>
        <field>Next_Treatment_SMS_Template__c</field>
        <formula>IF(
ISPICKVAL(AccountId__r.Language__c,&quot;Spanish&quot;), &quot;15 Day Treatment - Spanish&quot;,
&quot;15 Day Treatment&quot;
)</formula>
        <name>Set Initial Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lawsuit_Filed_Date</fullName>
        <field>Lawsuit_Filed_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Lawsuit Filed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MRI_Completed_Date</fullName>
        <field>MRI_Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Set MRI Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Mediation_Comp_Date</fullName>
        <field>Mediation_Comp_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Mediation Comp Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Next_Treatment_SMS_Date</fullName>
        <description>Sets initial treatment sms 15 day</description>
        <field>Next_Treatment_SMS_Date__c</field>
        <formula>DATEVALUE(Intake__r.Retainer_Received__c) + 15</formula>
        <name>Set Initial Treatment SMS Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Litigation_Date</fullName>
        <field>Pending_Litigation_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Pending Litigation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Positive_MRI_Finding_Date</fullName>
        <field>Positive_MRI_Finding_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Positive MRI Finding Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Treatment_SMS_Trigger</fullName>
        <description>Updates checkbox to trigger process builder</description>
        <field>Trigger_Treatment_SMS__c</field>
        <literalValue>1</literalValue>
        <name>Set Treatment SMS Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Turn_Down_Status_Date</fullName>
        <field>Turn_Down_Status_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Turn Down Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_To_Be_Posted</fullName>
        <field>Status__c</field>
        <literalValue>To Be Posted</literalValue>
        <name>Update Status - To Be Posted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>ATO_Webhook</fullName>
        <apiVersion>36.0</apiVersion>
        <endpointUrl>http://66.35.88.140:34567/LawsuitNotificationService.asmx</endpointUrl>
        <fields>AccountId__c</fields>
        <fields>Case_Subtype__c</fields>
        <fields>Case_Type__c</fields>
        <fields>Id</fields>
        <fields>Intake__c</fields>
        <fields>Litigation__c</fields>
        <fields>Name</fields>
        <fields>Status__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>ryan@forthepeople.com</integrationUser>
        <name>ATO_Webhook</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>ATO_Webhook</fullName>
        <actions>
            <name>ATO_Webhook</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.Status__c</field>
            <operation>equals</operation>
            <value>To Be Posted</value>
        </criteriaItems>
        <description>Send lawsuit.id to ATO service for importing case</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ask Nicely Low Score Notification</fullName>
        <actions>
            <name>Send_AN_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.Latest_AN_Score__c</field>
            <operation>lessOrEqual</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Latest_AN_Survey__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <description>Updated to not include SS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Determine If Lawsuit External ID Needs Populating</fullName>
        <actions>
            <name>Populate_Lawsuit_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule determines if the Lawsuit__c.External_ID__c field needs populating when the Lawsuit__c record is created.</description>
        <formula>ISBLANK( External_ID__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lawsuit - Update Case Manager Email</fullName>
        <actions>
            <name>Set_Case_Manager_Email_on_Lawsuit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates lawsuit with case manager email address</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lawsuit Posted - Notify Case Manager</fullName>
        <actions>
            <name>Email_Case_Manager_Case_posted_to_CP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.Status__c</field>
            <operation>equals</operation>
            <value>Posted</value>
        </criteriaItems>
        <description>Notify case manager of lawsuit posting to CP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Review for TPA</fullName>
        <actions>
            <name>Review_Completed_Case</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lawsuit__c.Status__c</field>
            <operation>equals</operation>
            <value>Questionnaire,Review</value>
        </criteriaItems>
        <description>Converted cases ready for review by tpa</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SMS - Send SMS Lawsuit Treatment</fullName>
        <active>true</active>
        <description>KS Deactivated legacy *this is handled on matter* 
Rule to check box to true to fire process builder for SMS</description>
        <formula>IF(
  AND(
    NOT(AccountId__r.tdc_tsw__SMS_Opt_out__c),
    NOT(AccountId__r.PersonContact.tdc_tsw__SMS_Opt_out__c),
    NOT(Treatment_SMS_Opt_Out__c),
    NOT(CP_File_Closed__c),
    NOT(Trigger_Treatment_SMS__c),
    NOT(ISBLANK(Next_Treatment_SMS_Date__c)),
   OR(
    AND(NOT(ISBLANK(CP_File_Open_Date__c)),ISPICKVAL(CP_Active_Case_Indicator__c,&quot;Y&quot;)),
    ISPICKVAL(CP_Active_Case_Indicator__c,&quot;N&quot;)
    ),
    NOT(Intake__r.Catastrophic__c),
    NOT(ISPICKVAL(Intake__r.Injured_party_deceased__c, &quot;Yes&quot;)),
    CP_Server__c &lt;&gt; &quot;NLF&quot;,
    OR(
      ISPICKVAL(Intake__r.Litigation__c, &quot;Personal Injury&quot;),
      ISPICKVAL(Intake__r.Litigation__c, &quot;Premises Liability&quot;)
    ),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;demand&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;settled&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;suit filed&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;litigation&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;trial&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;turn&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;dismissed&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;withdraw&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;eeoc&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;pending service&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;lien-fired&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;fired us&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;referred&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;appeal&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;lien&quot;))
  ),
  True,
  False
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Treatment_SMS_Trigger</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lawsuit__c.Next_Treatment_SMS_Date__c</offsetFromField>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SMS - Set Lawsuit Treatment Date</fullName>
        <actions>
            <name>Set_Initial_Template</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Next_Treatment_SMS_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets the first day trigger date</description>
        <formula>IF(
  AND(
    NOT(AccountId__r.tdc_tsw__SMS_Opt_out__c),
    NOT(AccountId__r.PersonContact.tdc_tsw__SMS_Opt_out__c),
    NOT(Treatment_SMS_Opt_Out__c),
    OR(
    AND(NOT(ISBLANK(CP_File_Open_Date__c)),ISPICKVAL(CP_Active_Case_Indicator__c,&quot;Y&quot;)),
    ISPICKVAL(CP_Active_Case_Indicator__c,&quot;N&quot;)
    ),
    NOT(CP_File_Closed__c),
    NOT(ISBLANK(Intake__r.Retainer_Received__c)),
    TODAY() - DATEVALUE(Intake__r.Retainer_Received__c) &lt;= 14,
    NOT(Intake__r.Catastrophic__c),
    NOT(ISPICKVAL(Intake__r.Injured_party_deceased__c, &quot;Yes&quot;)),
    OR(
      ISPICKVAL(Intake__r.Litigation__c, &quot;Personal Injury&quot;),
      ISPICKVAL(Intake__r.Litigation__c, &quot;Premises Liability&quot;)
    ),
    CP_Server__c &lt;&gt; &quot;NLF&quot;,
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;demand&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;settled&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;suit filed&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;litigation&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;trial&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;turn&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;dismissed&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;withdraw&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;eeoc&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;pending service&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;lien-fired&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;fired us&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;referred&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;appeal&quot;)),
    NOT(CONTAINS(LOWER(CP_Status__c), &quot;lien&quot;))
  ),
  True,
  False
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Email on Lawsuit</fullName>
        <actions>
            <name>Set_Client_Email_on_Lawsuit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>On edit, save client email</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Demand Date</fullName>
        <actions>
            <name>Set_Demand_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.CP_Status__c</field>
            <operation>equals</operation>
            <value>Demand</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Demand_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Lawsuit: CP Status == Demand</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Evaluation Date</fullName>
        <actions>
            <name>Set_Evaluation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lawsuit__c.Evaluation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Ortho_Eval__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Pain_Management_Eval__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Lawsuit Filed Date</fullName>
        <actions>
            <name>Set_Lawsuit_Filed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.CP_Status__c</field>
            <operation>equals</operation>
            <value>Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Lawsuit_Filed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set Lawsuit Filed Date when CP Status == Litigation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MRI Completed Date</fullName>
        <actions>
            <name>Set_MRI_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.MRI_Completed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.MRI_Completed__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MT Cases To Be Posted</fullName>
        <actions>
            <name>Update_Status_To_Be_Posted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.Status__c</field>
            <operation>equals</operation>
            <value>Review</value>
        </criteriaItems>
        <description>auto set To  Be Posted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Mediation Comp Date</fullName>
        <actions>
            <name>Set_Mediation_Comp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.Mediation_Comp__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Mediation_Comp_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Pending Litigation Date</fullName>
        <actions>
            <name>Set_Pending_Litigation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.CP_Status__c</field>
            <operation>equals</operation>
            <value>Pending Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Pending_Litigation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Lawsuit: CP Status == Pending Litigation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Positive MRI Finding Date</fullName>
        <actions>
            <name>Set_Positive_MRI_Finding_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lawsuit__c.Positive_MRI_Finding_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lawsuit__c.Positive_MRI_Finding__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Turn Down Status Date</fullName>
        <actions>
            <name>Set_Turn_Down_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNULL(Turn_Down_Status_Date__c) &amp;&amp; (CONTAINS(UPPER(CP_Status__c),&quot;FIRE&quot;) || CONTAINS(UPPER(CP_Status__c),&quot;TURN&quot;) || CONTAINS(UPPER(CP_Status__c),&quot;WITHD&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>test apex call</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lawsuit__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>asdf</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Review_Completed_Case</fullName>
        <assignedTo>angel@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Review questionnaire, and change lawsuit stage to &quot;posted&quot;.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review Completed Case</subject>
    </tasks>
</Workflow>
