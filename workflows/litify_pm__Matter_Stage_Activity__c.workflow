<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Stage_Name_Search</fullName>
        <description>updates for searching</description>
        <field>Stage_Search_Name__c</field>
        <formula>Name&amp;&quot;-&quot;&amp;
IF(
NOT(ISBLANK(litify_pm__Matter__r.Intake__c)),
SUBSTITUTE(litify_pm__Matter__r.Intake__r.Name, &quot;INT-&quot;, &quot;&quot;),
litify_pm__Matter__r.CP_Case_Number__c
)</formula>
        <name>Update Stage Name Search</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Stage Search Text</fullName>
        <actions>
            <name>Update_Stage_Name_Search</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates Stage Search Text for better look up search functionality</description>
        <formula>NOT(ISBLANK(litify_pm__Matter__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
