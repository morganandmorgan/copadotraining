<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ATL_Cat_Case_Turn_Down_Email_Notification</fullName>
        <ccEmails>JDMiller@ForThePeople.com, ATL-NewCase@ForThePeople.com</ccEmails>
        <description>ATL Cat Case Turn Down Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>msimmons@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Abogados_Autoresponder_Email_Alert</fullName>
        <description>Abogados Autoresponder Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Abogados_Autoresponder</template>
    </alerts>
    <alerts>
        <fullName>Agent_Send_JUUL_Questionnaire_from_Intake</fullName>
        <description>Agent - Send JUUL Questionnaire from Intake</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/RR_JUUL_Your_Injury_Questionnaire_Staff_Intake</template>
    </alerts>
    <alerts>
        <fullName>Agent_Send_Questionnaire_from_Intake</fullName>
        <description>Agent - Send Questionnaire from Intake</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/RR_Your_Injury_Questionnaire_Staff_Intake</template>
    </alerts>
    <alerts>
        <fullName>Asbestos_Notification_Email</fullName>
        <description>Asbestos Notification Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>mleininger@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Assigned_Attorney_Email</fullName>
        <description>CCC - Assigned Attorney Email</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Case_Developer_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Case_Manager_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Person_Assigned_to_Case</template>
    </alerts>
    <alerts>
        <fullName>BTG_Welcome_Email</fullName>
        <description>BTG Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@businesstrialgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/BTG_Welcome_Email2</template>
    </alerts>
    <alerts>
        <fullName>Belviq_Book_Email_Intake</fullName>
        <description>Belviq Book Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCC_Intake_Emails/Belviq_Information_request</template>
    </alerts>
    <alerts>
        <fullName>Birth_Injury_Book_Email_Intake</fullName>
        <description>Birth Injury Book Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCC_Intake_Emails/Birth_Injury_Information_request</template>
    </alerts>
    <alerts>
        <fullName>CA_CLG_Gas_Explosion_Email_Alert</fullName>
        <description>CA - CLG - Gas Explosion Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rrocha@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/CA_CLG_Gas_Explosion_Welcome_Letter</template>
    </alerts>
    <alerts>
        <fullName>CCC_Auto_TD_Class_Action_Email</fullName>
        <description>CCC - Auto TD -Class Action - Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>notifications@classaction.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/CCC_Auto_TD_Class_Action</template>
    </alerts>
    <alerts>
        <fullName>CCC_Auto_TD_M_M_Email</fullName>
        <description>CCC - Auto TD -M&amp;M - Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>notifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/CCC_Auto_TD_M_M</template>
    </alerts>
    <alerts>
        <fullName>CCC_CAT_SUIP_Notify_Sean_S</fullName>
        <description>CCC - CAT SUIP Notify Sean S</description>
        <protected>false</protected>
        <recipients>
            <recipient>sshaughnessy@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Case_Manager_Developer_Email</fullName>
        <description>CCC -  Case Manager / Developer Email</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Developer_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Case_Manager_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Person_Assigned_to_Case</template>
    </alerts>
    <alerts>
        <fullName>CCC_Cat_Notify_Assigned_Atty_Converted_Case</fullName>
        <description>CCC - Cat - Notify Assigned Atty Converted Case (also included on WC conversions)</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Catastrophic_Case_Out_of_Area_Referred_Out</fullName>
        <description>CCC - Catastrophic Case Out of Area Referred Out</description>
        <protected>false</protected>
        <recipients>
            <recipient>bgainsford@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jsternberger@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tchurch@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wgreen@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Catastrophic_Case_Scheduled_for_Sign_Up_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Scheduled for Sign Up - No Atty</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Catastrophic_Case_Scheduled_for_Sign_Up_No_Copy_Mike_Morgan_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Scheduled for Sign Up - No Copy Mike Morgan - No Atty</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Catastrophic_Case_Under_Review_Turned_Down_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Under Review/Turned Down - No Atty</description>
        <protected>false</protected>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Under Review/Turned Down - No Copy Mike Morgan - No Atty</description>
        <protected>false</protected>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Mike_Morgen_Email_Alert</fullName>
        <description>CCC - Mike Morgen Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Scheduled for Sign Up</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Scheduled for Sign Up - No Atty</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up_No_Copy_Mike_Morgan</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Scheduled for Sign Up - No Copy Mike Morgan</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up_No_Copy_Mike_Morgan_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@forthepeople.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Scheduled for Sign Up - No Copy Mike Morgan - No Atty</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Under Review/Turned Down</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Atty</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Under Review/Turned Down - No Atty</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Under Review/Turned Down - No Copy Mike Morgan</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan_No_Att</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <ccEmails>bhirt@nationlaw.com</ccEmails>
        <description>CCC -Nation- Catastrophic Case Under Review/Turned Down - No Copy Mike Morgan - No Atty</description>
        <protected>false</protected>
        <recipients>
            <recipient>mnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>npanagakis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Case_already_signed_CCC_not_calling_PC_Please_set_status_to_Converted</fullName>
        <description>Case already signed - CCC not calling PC. Please set status to Converted.</description>
        <protected>false</protected>
        <recipients>
            <recipient>kdavis@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>trojas@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Attorneys_Portal_Related/Already_Signed_Change_Status_Converted</template>
    </alerts>
    <alerts>
        <fullName>Cast_Iron_Pipes_Auto_Emails_Nation_Cases</fullName>
        <description>Cast Iron Pipes Auto Emails (Nation Cases)</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pipes@nationlaw.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>et4ae5__ExactTarget/CastIronPipesConvertedEmail</template>
    </alerts>
    <alerts>
        <fullName>Cast_Iron_Pipes_Auto_Emails_South_FL_only</fullName>
        <description>Cast Iron Pipes Auto Emails (South FL only)</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cipcases@weillawfirm.net</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>et4ae5__ExactTarget/WeilSnyderCastIronPipesConvertedEmail</template>
    </alerts>
    <alerts>
        <fullName>Cat_Qualified_Turn_Down_Notification</fullName>
        <description>Cat Qualified Turn Down Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Catastrophic_Case_Scheduled_for_Sign_Up</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Scheduled for Sign Up</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Catastrophic_Case_Scheduled_for_Sign_Up_No_Mike_Morgan</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Scheduled for Sign Up - No Copy Mike Morgan</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Catastrophic_Case_Turned_Down</fullName>
        <description>CCC - Catastrophic Case Turned Down</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Catastrophic_Case_Under_Review_Turned_Down</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Under Review/Turned Down</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Catastrophic_Case_Under_Review_Turned_Down_No_Mike_Morgan</fullName>
        <ccEmails>star-matter@forthepeople.com</ccEmails>
        <ccEmails>CCC-Supervisors@ForThePeople.com</ccEmails>
        <description>CCC - Catastrophic Case Under Review/Turned Down - No Copy Mike Morgan</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Child_Victim_Auto_TD_CA_Email</fullName>
        <description>Child Victim - Auto TD -CA - Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>notifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/CSA_TD</template>
    </alerts>
    <alerts>
        <fullName>Class_Action_Referral_Notification_to_Dov</fullName>
        <description>Class Action Referral Notification to Dov</description>
        <protected>false</protected>
        <recipients>
            <recipient>jsternberger@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mleininger@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sdavidovitz@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>notifications@classaction.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Notify_Non_Litify_Referral</template>
    </alerts>
    <alerts>
        <fullName>Conflict_Email_to_Conflict_Team</fullName>
        <description>Conflict Email to Conflict Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>Under_Review_Team</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Conflict_email</template>
    </alerts>
    <alerts>
        <fullName>Dicamba_Attorney_Welcome_Email</fullName>
        <description>Dicamba Attorney Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rrocha@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_Dicamba_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_CCC_SUIP_10_Day</fullName>
        <description>Drip Email - CCC SUIP - 10 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_10Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_CCC_SUIP_14_Day</fullName>
        <description>Drip Email - CCC SUIP - 14 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_14Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_CCC_SUIP_20_Day</fullName>
        <description>Drip Email - CCC SUIP - 20 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_20Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_CCC_SUIP_5_Day</fullName>
        <description>Drip Email - CCC SUIP - 5 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_5Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_MT_Retainer_Sent_10_Day</fullName>
        <description>Drip Email - MT Retainer Sent - 10 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mtintake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_10Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_MT_Retainer_Sent_14_Day</fullName>
        <description>Drip Email - MT Retainer Sent - 14 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mtintake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_14Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_MT_Retainer_Sent_20_Day</fullName>
        <description>Drip Email - MT Retainer Sent - 20 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mtintake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_20Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_MT_Retainer_Sent_5_Day</fullName>
        <description>Drip Email - MT Retainer Sent - 5 Day</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mtintake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RSContact_5Days</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_RR_1_Hour</fullName>
        <description>Drip Email - RR 1 Hour</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RR_TD_Client_Email_V1</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_RR_1_Hour_With_Link</fullName>
        <description>Drip Email - RR 1 Hour - With Link</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RR_1_HR_Your_Injury_Questionnaire</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_RR_1_Hour_With_Link_Follow_Up</fullName>
        <description>Drip Email - RR 1 Hour - With Link - Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RR_1_HR_Client_Email_W_Questionnaire_Link</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_RR_4_Hour_With_Link</fullName>
        <description>Drip Email - RR 4 Hour - With Link</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RR_4_HR_Client_Email_W_Questionnaire_Link</template>
    </alerts>
    <alerts>
        <fullName>Drip_Email_Turn_Down_1_Hour</fullName>
        <description>Drip Email - Turn Down 1 Hour</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RR_TD_Client_Email_V3</template>
    </alerts>
    <alerts>
        <fullName>Drip_Labor_Labor_TCPA_Act_now_you_may_be_entitled_to_compensation_DAY_5</fullName>
        <description>Drip - Labor - Labor/TCPA - Act now; you may be entitled to compensation - DAY 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>intake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Labor_Labor_TCPA_Act_Now_Day_5</template>
    </alerts>
    <alerts>
        <fullName>Drip_Labor_Labor_TCPA_Don_t_miss_your_chance_We_want_to_help_you_DAY_20</fullName>
        <description>Drip - Labor - Labor/TCPA - Don’t miss your chance! We want to help you. - DAY 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>intake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Labor_Labor_TCPA_Don_t_miss_your_change_Day_20</template>
    </alerts>
    <alerts>
        <fullName>Drip_Labor_Labor_TCPA_Time_is_running_out_DAY_14</fullName>
        <description>Drip - Labor - Labor/TCPA - Time is running out - DAY 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>intake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Labor_Labor_TCPA_Time_is_Running_Out_Day_14</template>
    </alerts>
    <alerts>
        <fullName>Drip_Labor_Labor_TCPA_Urgent_We_have_not_begun_working_on_your_potential_claim_y</fullName>
        <description>Drip - Labor - Labor/TCPA - Urgent: We have not begun working on your potential claim yet - DAY 10</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>intake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Labor_Labor_TCPA_Urgent_Day_10</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_CLG_Act_now_you_may_be_entitled_to_compensation_DAY_5</fullName>
        <description>Drip - MT - CLG - Act now; you may be entitled to compensation - DAY 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_CLG_Act_Now_Day_5</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_CLG_Don_t_miss_your_chance_We_want_to_help_you_DAY_20</fullName>
        <description>Drip - MT - CLG - Don’t miss your chance! We want to help you. - DAY 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_CLG_Don_t_Miss_Chance_Day_20</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_CLG_Time_is_running_out_DAY_14</fullName>
        <description>Drip - MT - CLG - Time is running out - DAY 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_CLG_Time_is_Running_Out_Day_14</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_CLG_Urgent_We_have_not_begun_working_on_your_potential_claim_yet_DAY_10</fullName>
        <description>Drip - MT - CLG - Urgent: We have not begun working on your potential claim yet - DAY 10</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_CLG_No_Work_Started_Day_10</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_EDL_Act_now_you_may_be_entitled_to_compensation_DAY_5</fullName>
        <description>Drip - MT - EDL - Act now; you may be entitled to compensation - DAY 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_EDL_Act_Now_Day_5</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_EDL_Don_t_miss_your_chance_We_want_to_help_you_DAY_20</fullName>
        <description>Drip - MT - EDL - Don’t miss your chance! We want to help you. - DAY 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_EDL_Don_t_Miss_Chance_Day_20</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_EDL_Time_is_running_out_DAY_14</fullName>
        <description>Drip - MT - EDL - Time is running out - DAY 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_EDL_Time_is_Running_Out_Day_14</template>
    </alerts>
    <alerts>
        <fullName>Drip_MT_EDL_Urgent_We_have_not_begun_working_on_your_potential_claim_yet_DAY_10</fullName>
        <description>Drip - MT - EDL - Urgent: We have not begun working on your potential claim yet - DAY 10</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_EDL_No_Work_Started_Day_10</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_CNF_More_Info_Needed_Day_1</fullName>
        <description>Drip - SS - More Info Needed - Day 1</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_1</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_CNF_More_Info_Needed_Day_10</fullName>
        <ccEmails>socialsecurity@forthepeople.com</ccEmails>
        <description>Drip - SS - CNF – More Info Needed - Day 10</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecuritybenefits@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_10_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_CNF_More_Info_Needed_Day_21</fullName>
        <ccEmails>socialsecurity@forthepeople.com</ccEmails>
        <description>Drip - SS - CNF – More Info Needed - Day 21</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecuritybenefits@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_21_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_CNF_More_Info_Needed_Day_7</fullName>
        <ccEmails>socialsecurity@forthepeople.com</ccEmails>
        <description>Drip - SS - CNF – More Info Needed - Day 7</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecuritybenefits@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_7_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_CNF_Need_FCQ_Day_1</fullName>
        <description>Drip - SS - CNF – Need FCQ – Day 1</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_Need_FCQ_Day_1_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_DLI_Day_1</fullName>
        <description>Drip - SS - Need DLI - Day 1</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_DLI_Day_1_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_DLI_Day_10</fullName>
        <description>Drip - SS - Need DLI - Day 10</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_DLI_Day_10_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_DLI_Day_5</fullName>
        <description>Drip - SS - Need DLI - Day 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_DLI_Day_5_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_Denial_Letter_Day_1</fullName>
        <description>Drip - SS -  Need Denial Letter - Day 1</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_Denial_Letter_Day_1_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_Denial_Letter_Day_14</fullName>
        <description>Drip - SS - Need Denial Letter - Day 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_Denial_Letter_Day_14_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_Denial_Letter_Day_7</fullName>
        <description>Drip - SS - Need Denial Letter - Day 7</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_Denial_Letter_Day_7_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_FCQ_Day_14</fullName>
        <description>Drip - SS - Need FCQ - Day 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_FCQ_Day_14_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Need_FCQ_Day_7</fullName>
        <description>Drip - SS - Need FCQ - Day 7</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Need_FCQ_Day_7_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Paper_Retainer_Day_1</fullName>
        <description>Drip - SS - Paper Retainer – Day 1</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Paper_Retainer_Day_1</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Paper_Retainer_Day_10</fullName>
        <description>Drip - SS - Paper Retainer – Day 10</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Paper_Retainer_Day_10</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Paper_Retainer_Day_14</fullName>
        <description>Drip - SS - Paper Retainer – Day 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Paper_Retainer_Day_14</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Paper_Retainer_Day_20</fullName>
        <description>Drip - SS - Paper Retainer – Day 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Paper_Retainer_Day_20</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Paper_Retainer_Day_5</fullName>
        <description>Drip - SS - Paper Retainer – Day 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Paper_Retainer_Day_5</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Unable_to_Contact_Day_1</fullName>
        <description>Drip - SS - Unable to Contact Day - 2</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_1</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Unable_to_Contact_Day_10</fullName>
        <description>Drip - SS - Unable to Contact Day - 14</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_14</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Unable_to_Contact_Day_14</fullName>
        <description>Drip - SS - Unable to Contact Day - 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_5_v2</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Unable_to_Contact_Day_30</fullName>
        <description>Drip - SS - Unable to Contact Day - 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_10</template>
    </alerts>
    <alerts>
        <fullName>Drip_SS_Unable_to_Contact_Day_5</fullName>
        <description>Drip - SS - Unable to Contact Day - 3</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_5</template>
    </alerts>
    <alerts>
        <fullName>Elmiron_Book_Email_Intake</fullName>
        <description>Elmiron Book Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCC_Intake_Emails/Elmiron_Information_request</template>
    </alerts>
    <alerts>
        <fullName>Email_4_Hour_Retainer_Received</fullName>
        <description>Email: 4 Hour Retainer Received</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/RR_TD_Client_Email_V1RR_TD_Client_Email_V2</template>
    </alerts>
    <alerts>
        <fullName>Email_Client_First_Staff_Conversation_Follow_Up_Email</fullName>
        <description>Email Client - First Staff Conversation Follow Up Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_Post_Sign_Up</template>
    </alerts>
    <alerts>
        <fullName>Email_Client_First_Staff_Conversation_Follow_Up_Email_No_Video_Link</fullName>
        <description>Email Client - First Staff Conversation Follow Up Email (No Video Link)</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_Post_Sign_Up_No_Link</template>
    </alerts>
    <alerts>
        <fullName>Email_Client_First_Staff_Conversation_No_Treatment</fullName>
        <description>Email Client - First Staff Conversation (No Treatment)</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_No_Treatment</template>
    </alerts>
    <alerts>
        <fullName>Email_Mail_Out_Agent</fullName>
        <description>Email Mail Out Agent</description>
        <protected>false</protected>
        <recipients>
            <field>Mail_Out_Agent_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Social_Security_Templates/Mail_Out_Notification_Application_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_Mail_Out_Agent_Status_Changed_from_Retainer_Received</fullName>
        <description>Email Mail Out Agent - Status Changed from Retainer Received</description>
        <protected>false</protected>
        <recipients>
            <field>Mail_Out_Agent_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Mail_Out_Notification_Status_Changed_From_Retainer_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_Mark_Nation_on_Case_Convert</fullName>
        <description>Email Mark Nation on Case Convert</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Person_Assigned_to_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_for_Approval_Under_Review</fullName>
        <description>Labor - Email Notification for Approval - Under Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>Labor_Approvers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cespy@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>laborandemployment@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>FCRA_How_To_Dispute_Inaccuracies</fullName>
        <description>FCRA - How To Dispute Inaccuracies</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>creditlawyers@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/FCRA_How_To_Dispute_Inaccuracies</template>
    </alerts>
    <alerts>
        <fullName>FCRA_How_To_Get_Credit_Report</fullName>
        <description>FCRA - How To Get Credit Report</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>creditlawyers@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/FCRA_How_To_Get_Credit_Report</template>
    </alerts>
    <alerts>
        <fullName>Generating_Attorney_Email</fullName>
        <description>Generating Attorney Email</description>
        <protected>false</protected>
        <recipients>
            <field>Generating_Attorney__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Generating_Attorney</template>
    </alerts>
    <alerts>
        <fullName>Keith_Mitnik_Welcome_Email</fullName>
        <description>Keith Mitnik Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Automated_Customer_Emails/Keith_Mitnik_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>Labor_30_Day_TD_Alert</fullName>
        <description>Labor 30 Day TD Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>jenniferr@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Labor_30_Day_TD_Email_to_Client</template>
    </alerts>
    <alerts>
        <fullName>Labor_TD_Alert</fullName>
        <description>Labor TD Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>jenniferr@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Labor_30_Day_TD_Email_to_Client</template>
    </alerts>
    <alerts>
        <fullName>MT_Monat_Welcome_Email</fullName>
        <description>MT - Monat Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_Monat_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>MT_Wildfire_Welcome_Email</fullName>
        <description>MT - Wildfire Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlit@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mass_Tort_Email/MT_Wildfire_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>Meso_Book_Email_Intake</fullName>
        <description>Meso Book Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Meso_Book</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Email</fullName>
        <description>CCC - New Case Email</description>
        <protected>false</protected>
        <recipients>
            <field>New_Case_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Email_SUIP</fullName>
        <description>CCC - New Case Email - SUIP</description>
        <protected>false</protected>
        <recipients>
            <field>New_Case_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Investigation_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>SS_English_Adult_Mental_FCQ_Email_Alert</fullName>
        <description>SS - English, Adult Mental FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_English_Adult_Mental_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_English_Adult_Mental_and_Physical_FCQ_Email_Alert</fullName>
        <description>SS - English, Adult Mental and Physical FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_English_Adult_Mental_and_Physical_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_English_Adult_Physical_FCQ_Email_Alert</fullName>
        <description>SS - English, Adult Physical FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_English_Adult_Physical_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_English_Minor_FCQ_Email_Alert</fullName>
        <description>SS - English, Minor FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_English_Minor_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_More_Info_Needed_Day_20</fullName>
        <description>SS – More Info Needed - Day 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_21</template>
    </alerts>
    <alerts>
        <fullName>SS_More_Info_Needed_Day_3</fullName>
        <description>SS – More Info Needed - Day 3</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_10</template>
    </alerts>
    <alerts>
        <fullName>SS_More_Info_Needed_Day_5</fullName>
        <description>SS – More Info Needed - Day 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_10_v2</template>
    </alerts>
    <alerts>
        <fullName>SS_Retainer_received_Needs_App_Day_2</fullName>
        <description>SS – Retainer received - Needs App - Day 2</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_CNF_More_Info_Needed_Day_1_v2</template>
    </alerts>
    <alerts>
        <fullName>SS_Retainer_received_Needs_App_Day_20</fullName>
        <description>SS – Retainer received - Needs App - Day 20</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_14_v2</template>
    </alerts>
    <alerts>
        <fullName>SS_Retainer_received_Needs_App_Day_3</fullName>
        <description>SS – Retainer received - Needs App - Day 3</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_1_v2</template>
    </alerts>
    <alerts>
        <fullName>SS_Retainer_received_Needs_App_Day_5</fullName>
        <description>SS – Retainer received - Needs App - Day 5</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>socialsecurity@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/SS_Unable_to_Contact_Day_10_v2</template>
    </alerts>
    <alerts>
        <fullName>SS_Spanish_Adult_Mental_FCQ_Email_Alert</fullName>
        <description>SS - Spanish, Adult Mental FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_Spanish_Adult_Mental_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_Spanish_Adult_Mental_and_Physical_FCQ_Email_Alert</fullName>
        <description>SS - Spanish, Adult Mental and Physical FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_Spanish_Adult_Mental_and_Physical_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_Spanish_Adult_Physical_FCQ_Email_Alert</fullName>
        <description>SS - Spanish, Adult Physical FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_Spanish_Adult_Physical_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>SS_Spanish_Minor_FCQ_Email_Alert</fullName>
        <description>SS - Spanish, Minor FCQ Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>soc-fcq@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/SS_Spanish_Minor_FCQ_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Email_to_Attorneys</fullName>
        <description>CCC - Send Approval Email to Attorneys</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Send_First_Conversation_Atty</fullName>
        <description>Send First Conversation (Atty)</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_Attorney_Version</template>
    </alerts>
    <alerts>
        <fullName>Send_First_Conversation_Atty_No_Treatment</fullName>
        <description>Send First Conversation (Atty) - No Treatment</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_Attorney_Version_No</template>
    </alerts>
    <alerts>
        <fullName>Send_First_Conversation_Atty_No_Video_Link</fullName>
        <description>Send First Conversation (Atty) - No Video Link</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_Attorney_Version_No_Link</template>
    </alerts>
    <alerts>
        <fullName>Send_Intake_Details</fullName>
        <ccEmails>leadsteam@gosimon.com</ccEmails>
        <description>Send Intake Details</description>
        <protected>false</protected>
        <recipients>
            <recipient>referrals@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Social_Security_Approval_to_Attorney</fullName>
        <ccEmails>ssapprovals@forthepeople.com</ccEmails>
        <description>Social Security Approval to Attorney</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Social_Security_Templates/Social_Security_Attorney_Review</template>
    </alerts>
    <alerts>
        <fullName>Social_Security_FCQ_Notification</fullName>
        <ccEmails>soc-fcq@forthepeople.com</ccEmails>
        <description>Social Security FCQ Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Social_Security_Templates/Social_Security_Intake_FCQ_Notification</template>
    </alerts>
    <alerts>
        <fullName>Social_Security_Internal_Generating_Lead</fullName>
        <ccEmails>socialsecuritybenefits@forthepeople.com</ccEmails>
        <description>Social Security - Internal Generating Lead</description>
        <protected>false</protected>
        <senderAddress>asolis@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Social_Security_Templates/Social_Security_Internal_Generating_Lead</template>
    </alerts>
    <alerts>
        <fullName>Testing_SMS_Timed_Trigger</fullName>
        <description>Testing SMS Timed Trigger</description>
        <protected>false</protected>
        <recipients>
            <recipient>jstandberry@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mleininger@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tpalmer@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Testing_SMS_Trigger_Delete_After_Testing_Is_Complete</template>
    </alerts>
    <alerts>
        <fullName>Trucking_Attorney_Alert_Under_Review_Turn_Down</fullName>
        <description>Trucking Attorney Alert - Under Review/Turn Down</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney_6__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>Turn_Down_Email_General_Drug_Device</fullName>
        <description>Turn Down Email General Drug/Device</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mtintake@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/CCC_MT_Auto_TD_M_M</template>
    </alerts>
    <alerts>
        <fullName>Ultra_Catastrophic_Case_Under_Review_Turned_Down</fullName>
        <ccEmails>rlamont@forthepeople.com</ccEmails>
        <description>CCC - Ultra Catastrophic Case status change</description>
        <protected>false</protected>
        <recipients>
            <recipient>aflury@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bgainsford@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dfalkenstein@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mattmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>reuven@forthepeople.com.mm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tchurch@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yapfelbaum@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yithayakumar@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Alerts_Ultra_Cat_Case_Var_Roles</template>
    </alerts>
    <alerts>
        <fullName>VA_Turndown_Email</fullName>
        <description>VA - Turndown Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>veteransclaims@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>VA_emails/X5_VA_TD_letter_x</template>
    </alerts>
    <alerts>
        <fullName>VA_Welcome_Email</fullName>
        <description>VA - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>veteransclaims@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>VA_emails/X1_VA_Welcome_x</template>
    </alerts>
    <alerts>
        <fullName>VA_Welcome_Follow_up_Email</fullName>
        <description>VA - Welcome Follow-up Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>veteransclaims@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>VA_emails/X2_VA_Welcome_follow_up_x</template>
    </alerts>
    <alerts>
        <fullName>Wage_Hour_TD_Alert</fullName>
        <description>Wage &amp; Hour TD Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>jenniferr@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Wage_Hour_Turn_Down_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Web_Form_Submission_Auto_Responder_CA</fullName>
        <description>Web Form Submission Auto Responder - CA</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@classaction.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Web_Form_Auto_Responder_ClassAction</template>
    </alerts>
    <alerts>
        <fullName>Web_Form_Submission_Auto_Responder_FTP</fullName>
        <description>Web Form Submission Auto Responder - FTP</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/Web_Form_Auto_Responder_FTP</template>
    </alerts>
    <alerts>
        <fullName>Whistleblower_Welcome_Email</fullName>
        <description>Whistleblower Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Automated_Customer_Emails/Whistleblower_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>X24_Hour_Call_Left_Voicemail</fullName>
        <description>24-Hour Call - Left Voicemail</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/Email_to_Client_Left_Voicemail</template>
    </alerts>
    <alerts>
        <fullName>Zantac_Book_Email_Intake</fullName>
        <description>Zantac Book Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCC_Intake_Emails/Zantac_Information_request</template>
    </alerts>
    <alerts>
        <fullName>Zostavax_Attorney_Welcome_Email</fullName>
        <description>Zostavax Attorney Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_txt__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>complexlitteam@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automated_Customer_Emails/MT_Zostavax_Welcome_Email_V2</template>
    </alerts>
    <alerts>
        <fullName>Zostavax_Questionnaire_Complete_to_Case_Staff</fullName>
        <description>Zostavax Questionnaire Complete to Case Staff</description>
        <protected>false</protected>
        <recipients>
            <recipient>lleon@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>complexlitteam@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mass_Tort/Zostavax_Case_Details_Questionnaire</template>
    </alerts>
    <fieldUpdates>
        <fullName>Attorney_Approved</fullName>
        <field>Status_Detail__c</field>
        <literalValue>Atty Approved</literalValue>
        <name>Attorney Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Attorney_TD</fullName>
        <field>Status_Detail__c</field>
        <literalValue>Atty - TD</literalValue>
        <name>Attorney - TD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bird_Under_Review</fullName>
        <field>Status__c</field>
        <literalValue>Under Review</literalValue>
        <name>Bird - Under Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIP_SFL_Docusign_ID_Update</fullName>
        <field>DocusignTemplateID__c</field>
        <formula>&quot;f0d703b1-f73f-4ee2-b161-0f52af4ec6fd&quot;</formula>
        <name>CIP SFL Docusign ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIP_SFL_Docusign_Invest_ID_Update</fullName>
        <field>Docusign_Template_ID_Invst__c</field>
        <formula>&quot;ec9fa247-e98e-4148-a31e-443cf3259ac4&quot;</formula>
        <name>CIP SFL Docusign Invest-ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_First_Last_Name_Field_Update</fullName>
        <field>Client_First_Last_Name__c</field>
        <formula>Client__r.FirstName &amp; &quot; &quot; &amp; Client__r.LastName</formula>
        <name>Client First Last Name Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>If_Retainer_Sent_is_Blank</fullName>
        <field>Retainer_Sent__c</field>
        <formula>BLANKVALUE( Retainer_Sent__c , now())</formula>
        <name>If Retainer Sent is Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Intake_Update_Client_Email</fullName>
        <field>Client_Email_txt__c</field>
        <formula>Client__r.PersonContact.Email</formula>
        <name>Intake - Update Client Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Correct_Contact_Update</fullName>
        <field>Last_Correct_Contact__c</field>
        <formula>IF(ISBLANK(Last_Correct_Contact__c),now(),Last_Correct_Contact__c)</formula>
        <name>Last Correct Contact Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Status_Update_Date</fullName>
        <field>Last_Status_Update_Date__c</field>
        <formula>NOW()</formula>
        <name>Last Status Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Qualified_Expired_Rescreen_Update</fullName>
        <description>Updates the status detail field to &quot;Qualified Expired Rescreen&quot;</description>
        <field>Status_Detail__c</field>
        <literalValue>Qualified Expired - Rescreen</literalValue>
        <name>Qualified Expired Rescreen Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Retainer_Received_Update</fullName>
        <field>Retainer_Received__c</field>
        <formula>BLANKVALUE( Retainer_Received__c , now())</formula>
        <name>Retainer Received Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Retainer_Sent_Update</fullName>
        <field>Retainer_Sent__c</field>
        <formula>BLANKVALUE( Retainer_Sent__c , now())</formula>
        <name>Retainer Sent Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Review_Complete_TD_Call_Pending</fullName>
        <description>Labor Turn Down rule after status has changed from Under Review, and client does not have an email.</description>
        <field>Status_Detail__c</field>
        <literalValue>TD by Attorney Review - TD Call Pending</literalValue>
        <name>Attorney Review Complete TD Call Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SS_No_Contact_Needs_Mail</fullName>
        <description>Changes Needs Application Filed to No Contact – Needs Mail</description>
        <field>Status_Detail__c</field>
        <literalValue>No Contact – Needs Mail</literalValue>
        <name>SS - No Contact – Needs Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SS_No_Contact_Turn_Down</fullName>
        <description>Changes status to Turn Down when there&apos;s a failure to make contact with the client.</description>
        <field>Status__c</field>
        <literalValue>Turn Down</literalValue>
        <name>SS - No Contact – Turn Down</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUIP_date_set_if_null</fullName>
        <field>Sign_Up_In_Progress_Date__c</field>
        <formula>BLANKVALUE( Sign_Up_In_Progress_Date__c , now())</formula>
        <name>SUIP date set if null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cast_Iron_Pipes_Litigation</fullName>
        <description>Sets litigation type to Insurance Dispute</description>
        <field>Litigation__c</field>
        <literalValue>Insurance Dispute</literalValue>
        <name>Set Cast Iron Pipes Litigation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contact_ID_on_Intake</fullName>
        <field>Contact_ID__c</field>
        <formula>CASESAFEID(Client__r.PersonContactId)</formula>
        <name>Set Contact ID on Intake</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_First_Staff_Attempt_Date</fullName>
        <description>Set First Staff Attempt Date to stop No 24 hour attempt from being updated</description>
        <field>First_Staff_Attempt_Date__c</field>
        <formula>NOW()</formula>
        <name>Set First Staff Attempt Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_First_Staff_Conversation_Date</fullName>
        <description>Updates First Staff Date</description>
        <field>First_Staff_Conversation_Date__c</field>
        <formula>NOW()</formula>
        <name>Set First Staff Conversation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Input_Channel_Hubspot</fullName>
        <description>Sets input channel to hubspot</description>
        <field>InputChannel__c</field>
        <literalValue>HubSpot</literalValue>
        <name>Set Input Channel - Hubspot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Input_Channel_infra_intakes</fullName>
        <description>Sets Input Channel to infra-intakes</description>
        <field>InputChannel__c</field>
        <literalValue>infra-intakes</literalValue>
        <name>Set Input Channel - infra-intakes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MailOut_Agent_to_Null</fullName>
        <field>Mail_Out_Agent_Name__c</field>
        <name>Set MailOut Agent to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Mail_Sent_Date_to_Null</fullName>
        <field>Mail_Sent_Date__c</field>
        <name>Set Mail Sent Date to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Marketing_Sub_source_to_Injury_com</fullName>
        <field>Marketing_Sub_source__c</field>
        <literalValue>injury.com</literalValue>
        <name>Set Marketing Sub source to injury.com</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Morgan_Morgan_Handling_Firm</fullName>
        <description>Sets handling firm if role meets criteria</description>
        <field>Handling_Firm__c</field>
        <literalValue>Morgan &amp; Morgan</literalValue>
        <name>Set Morgan &amp; Morgan Handling Firm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_No_24_Hour_Attempt</fullName>
        <description>Updates if criteria still = true</description>
        <field>No_Staff_Call_Attempt_24_hours__c</field>
        <literalValue>1</literalValue>
        <name>Set No 24 Hour Attempt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_No_24_Hour_Convo</fullName>
        <description>Updates if no convo in 24 hours</description>
        <field>No_Staff_Convo_in_24_Hours__c</field>
        <literalValue>1</literalValue>
        <name>Set No 24 Hour Convo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Qualified_Date_Time</fullName>
        <description>Sets Qualified Date/Time = Now</description>
        <field>Qualified_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Set Qualified Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Qualified_Date_Time_to_Null</fullName>
        <field>Qualified_Date_Time__c</field>
        <name>Set Qualified Date/Time to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Referred_Out_Date</fullName>
        <description>Sets Referred Out Date/Time to NOW()</description>
        <field>Referred_Out_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Referred Out Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Retainer_Received_date_to_Nul</fullName>
        <field>Retainer_Received__c</field>
        <name>Set Retainer Received date to Nul</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Retainer_Sent_Date_to_Null</fullName>
        <field>Retainer_Sent__c</field>
        <name>Set Retainer Sent Date to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SUIP_Date</fullName>
        <description>Captures the current time as the Sign Up In Progress date</description>
        <field>Sign_Up_In_Progress_Date__c</field>
        <formula>BLANKVALUE(Sign_Up_In_Progress_Date__c,NOW())</formula>
        <name>Set SUIP Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Staff_Assigned_Date</fullName>
        <description>Sets staff assigned date to trigger additional workflow that monitors 24 hour convo/attempts</description>
        <field>Staff_Assigned_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Staff Assigned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Detail_to_Exhausted</fullName>
        <description>Sets status detail to Exhausted</description>
        <field>Status_Detail__c</field>
        <literalValue>Exhausted</literalValue>
        <name>Set Status Detail to Exhausted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Turn_Down</fullName>
        <description>Sets status to Turn Down</description>
        <field>Status__c</field>
        <literalValue>Turn Down</literalValue>
        <name>Set Status - Turn Down</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_TD_Reason_Attorney_not_interested</fullName>
        <description>Sets TD reason for Attorney not interested</description>
        <field>Turn_Down_Reason__c</field>
        <literalValue>Attorney not interested</literalValue>
        <name>Set TD Reason - Attorney not interested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_UR_Exit_Date_to_Null</fullName>
        <field>Under_Review_Exit_Date__c</field>
        <name>Set UR Exit Date to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Under_Review_Date_Time</fullName>
        <field>Under_Review_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Under Review Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Venue_to_Nationwide</fullName>
        <field>Venue__c</field>
        <literalValue>Nationwide</literalValue>
        <name>Set Venue to Nationwide</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Signed_Up_By_Attorney_Update</fullName>
        <description>Updates status detail field to &quot;Signed Up By Attorney.&quot;</description>
        <field>Status_Detail__c</field>
        <literalValue>Signed Up By Attorney</literalValue>
        <name>Signed Up By Attorney Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Social_Security_Retainer_Sent_Date</fullName>
        <description>Updates the Retainer Sent field.</description>
        <field>Retainer_Sent__c</field>
        <formula>NOW()</formula>
        <name>Social Security - Retainer Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Social_Security_Update_Mail_Sent_Date</fullName>
        <description>Updates mail sent date</description>
        <field>Mail_Sent_Date__c</field>
        <formula>NOW()</formula>
        <name>Social Security - Update Mail Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TD_by_Attorney_Review_Email_Sent</fullName>
        <description>TD by Attorney Review - Email Sent</description>
        <field>Status_Detail__c</field>
        <literalValue>TD by Attorney Review - Email Sent</literalValue>
        <name>TD by Attorney Review - Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Turn_Down_Date_Updated</fullName>
        <field>Turn_Down_Date__c</field>
        <formula>BLANKVALUE( Turn_Down_Date__c , now())</formula>
        <name>Turn Down Date Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UR_Reason_Bird</fullName>
        <field>Under_Review_Reason__c</field>
        <literalValue>Requires SME Decision</literalValue>
        <name>UR Reason - Bird</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Attorney_Approval_Date</fullName>
        <description>Date that Attorney approves/rejects intake</description>
        <field>Attorney_Approved__c</field>
        <formula>NOW()</formula>
        <name>Update Attorney Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CQC_Bypass_TD</fullName>
        <field>CQC_Bypass__c</field>
        <literalValue>Turn Down</literalValue>
        <name>Update CQC Bypass TD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Converted_Date_Today</fullName>
        <description>When status = converted, set date</description>
        <field>Converted_Date__c</field>
        <formula>Now()</formula>
        <name>Update Converted Date - Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email</fullName>
        <field>Client_Email_txt__c</field>
        <formula>Client__r.PersonContact.Email</formula>
        <name>Update Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FCQ_Date</fullName>
        <description>Date that indicates when Intake was placed under FCQ status</description>
        <field>FCQ_Date__c</field>
        <formula>Now()</formula>
        <name>Update FCQ Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FCQ_Date_1</fullName>
        <description>Updated the FCQ date when Status = Under Review and Status Detail = FCQ</description>
        <field>FCQ_Date__c</field>
        <formula>Now()</formula>
        <name>Update FCQ Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IP_Deceased_Yes</fullName>
        <field>Injured_party_deceased__c</field>
        <literalValue>Yes</literalValue>
        <name>Update - IP Deceased - Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Intake_Completed_Date</fullName>
        <description>Now()</description>
        <field>Intake_Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Intake Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Intake_Receive_Date</fullName>
        <field>Intake_Received_Date__c</field>
        <formula>Now()</formula>
        <name>Update Intake Receive Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Correct_Contact</fullName>
        <field>Last_Correct_Contact__c</field>
        <formula>NOW()</formula>
        <name>Update Last Correct Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Disposition_DNC</fullName>
        <description>Updates the last disposition field to &quot;DNC: Attorney Handling.&quot;</description>
        <field>Last_Disposition__c</field>
        <formula>&quot;DNC: Attorney Handling&quot;</formula>
        <name>Update - Last Disposition - DNC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Marketing_Source_to_Classaction</fullName>
        <description>Update marketing source</description>
        <field>Marketing_Source__c</field>
        <literalValue>classaction.com</literalValue>
        <name>Update Marketing Source to Classaction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Marketing_sub_source</fullName>
        <field>Marketing_Sub_source__c</field>
        <literalValue>injury.com</literalValue>
        <name>Update Marketing sub source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OB_Calls_Status_to_0</fullName>
        <description>Update OB Calls - Status to 0 when status changed</description>
        <field>Number_Outbound_Dials_Status__c</field>
        <formula>0</formula>
        <name>Update OB Calls - Status to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_On_Hold_Date</fullName>
        <description>Today ()</description>
        <field>On_Hold_Date__c</field>
        <formula>NOW()</formula>
        <name>Update On Hold Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_on_Intake_from_Client_Accou</fullName>
        <description>coping the phone # from the Client account to the Intake</description>
        <field>Phone__c</field>
        <formula>BLANKVALUE(Client__r.PersonContact.MobilePhone, Client__r.PersonContact.HomePhone)</formula>
        <name>Update Phone on Intake from Client Accou</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Social_Security</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Social_Security</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type - Social Security</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_CCC</fullName>
        <description>Updates to CCC when Integration User Hubspot is creator</description>
        <field>RecordTypeId</field>
        <lookupValue>CCC</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to CCC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Labor</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Labor</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Labor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referring_Office_PICOM</fullName>
        <description>Updates Referring Office w/ &quot;PICOM&quot;</description>
        <field>Referring_Office__c</field>
        <formula>&quot;PersonalInjury.com&quot;</formula>
        <name>Update Referring Office - PICOM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Detail</fullName>
        <field>Status_Detail__c</field>
        <literalValue>Needs to be Qualified</literalValue>
        <name>Update Status Detail - Needs to be Qual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Detail_1</fullName>
        <field>Status_Detail__c</field>
        <literalValue>Send email</literalValue>
        <name>Update Status Detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Detail_Null</fullName>
        <description>Sets Status Detail value to null.</description>
        <field>Status_Detail__c</field>
        <name>Update Status Detail Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Detail_Pending_Referral</fullName>
        <field>Status_Detail__c</field>
        <literalValue>Pending Referral</literalValue>
        <name>Update Status Detail - Pending Referral</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Detail_TD_by_Criteria</fullName>
        <field>Status_Detail__c</field>
        <literalValue>TD by Criteria - TD Complete</literalValue>
        <name>Update Status Detail - TD by Criteria -</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Lead</fullName>
        <field>Status__c</field>
        <literalValue>Lead</literalValue>
        <name>Update Status - Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Referred_Out</fullName>
        <field>Status__c</field>
        <literalValue>Referred Out</literalValue>
        <name>Update Status - Referred Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Retainer_Received</fullName>
        <description>Updates the status to Retainer Received.</description>
        <field>Status__c</field>
        <literalValue>Retainer Received</literalValue>
        <name>Update Status - Retainer Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Turned_Down</fullName>
        <field>Status__c</field>
        <literalValue>Turn Down</literalValue>
        <name>Update Status - Turned Down</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Suppress_from_DICE</fullName>
        <field>Suppress_From_Dice__c</field>
        <literalValue>1</literalValue>
        <name>Update Suppress from DICE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_TD_Attorney_Review_Call_Complete</fullName>
        <field>Status_Detail__c</field>
        <literalValue>TD by Attorney Review - TD Call Complete</literalValue>
        <name>Update TD Attorney Review Call Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Under_Review_Exit_Date_Time</fullName>
        <description>Updated with NOW()</description>
        <field>Under_Review_Exit_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Under Review Exit Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VA_F_U_Email_Sent</fullName>
        <field>Status_Detail__c</field>
        <literalValue>F/U package sent - email</literalValue>
        <name>VA - F/U Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VA_Package_sent_email</fullName>
        <description>Sets the status detail when the VA package email is sent.</description>
        <field>Status_Detail__c</field>
        <literalValue>Package sent - email</literalValue>
        <name>VA - Package sent - email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webform_default_caller_is_injured_party</fullName>
        <description>Is caller the injured party field should = Yes.</description>
        <field>Caller_is_Injured_Party__c</field>
        <literalValue>Yes</literalValue>
        <name>Webform default caller is injured party</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>CCC_Turn_Down_Survey</fullName>
        <apiVersion>48.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_0e6HgHcE54u1EUt&amp;u=UR_4TwknbVRQHBmtUh&amp;t=OC_12PZiRTOijGttfA&amp;b=morganmorgan</endpointUrl>
        <fields>Approving_Attorney_1__c</fields>
        <fields>Approving_Attorney_2__c</fields>
        <fields>Approving_Attorney_3__c</fields>
        <fields>Approving_Attorney_4__c</fields>
        <fields>Approving_Attorney_5__c</fields>
        <fields>Approving_Attorney_6__c</fields>
        <fields>Case_Type__c</fields>
        <fields>Client_Email_txt__c</fields>
        <fields>Client_First_Last_Name__c</fields>
        <fields>Client__c</fields>
        <fields>Id</fields>
        <fields>Intake_Agent_Name__c</fields>
        <fields>Intake_Name__c</fields>
        <fields>Litigation__c</fields>
        <fields>Status_Detail__c</fields>
        <fields>Status__c</fields>
        <fields>Turn_Down_Date__c</fields>
        <fields>Turn_Down_Reason_Notes__c</fields>
        <fields>Turn_Down_Reason__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>integrationuser@forthepeople.com</integrationUser>
        <name>CCC Turn Down Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>DicambaAlert</fullName>
        <apiVersion>40.0</apiVersion>
        <description>Message to Zapier for Dicamba status change</description>
        <endpointUrl>https://hooks.zapier.com/hooks/catch/315925/rupbcq/</endpointUrl>
        <fields>Id</fields>
        <fields>Intake_Name__c</fields>
        <fields>Marketing_Source__c</fields>
        <fields>Marketing_Sub_source__c</fields>
        <fields>Name</fields>
        <fields>Status__c</fields>
        <fields>Turn_Down_Reason__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>ryan@forthepeople.com</integrationUser>
        <name>DicambaAlert</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>LOOPlus_Intake_TCPA_Welcome_Packet</fullName>
        <apiVersion>36.0</apiVersion>
        <description>Outbound Message for the TCPA Welcome Packet which is auto-emailed to the client 3 days after status is &quot;converted&quot;</description>
        <endpointUrl>https://apps.drawloop.com/package/111</endpointUrl>
        <fields>Drawloop_Next_TCPA_Welcome__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>shorn@forthepeople.com</integrationUser>
        <name>LOOPlus - Intake TCPA Welcome Packet</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>MesoAlert</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Message to Zapier for Meso status change</description>
        <endpointUrl>https://hooks.zapier.com/hooks/catch/315925/1bc7ji/</endpointUrl>
        <fields>Id</fields>
        <fields>Intake_Name__c</fields>
        <fields>Marketing_Source__c</fields>
        <fields>Name</fields>
        <fields>Status__c</fields>
        <fields>Turn_Down_Reason__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>ryan@forthepeople.com</integrationUser>
        <name>MesoAlert</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>ATL-Cat Case Turn Down Email Notification</fullName>
        <actions>
            <name>ATL_Cat_Case_Turn_Down_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Qualified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>GA - Atlanta</value>
        </criteriaItems>
        <description>Email notification for ATL Cat case that is marked TD</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Abogados - Autoresponder</fullName>
        <actions>
            <name>Abogados_Autoresponder_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>abogados.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.CreatedByWebform__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asbestos Email Notifications on Status Change</fullName>
        <actions>
            <name>Asbestos_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify Reuven and Dov on status change</description>
        <formula>AND (ISPICKVAL(Case_Type__c,&quot;Asbestos&quot;), ISCHANGED(Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BTG Auto-Responder</fullName>
        <actions>
            <name>BTG_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Auto email sent to client - BTG</description>
        <formula>ISPICKVAL(Marketing_Source__c,&quot;BTG.com&quot;) &amp;&amp; ISPICKVAL(InputChannel__c,&quot;infra-intakes&quot;) &amp;&amp; NOT(ISBLANK(Client_Email_txt__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Ben Crump - Non Litify Referrals Notification</fullName>
        <actions>
            <name>Class_Action_Referral_Notification_to_Dov</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Jake

CA.com 
classaction.com 
ClassAction 911 Leads 
ClassAction FL Sinkhole Leads 
ClassAction TCPA Leads 
ClassAction Wage &amp; Hour Leads 
ClassAction Xarelto Leads</description>
        <formula>ISPICKVAL(Status__c, &quot;Referred Out&quot;)  &amp;&amp;    ISPICKVAL(Marketing_Source__c,&quot;BenCrump.com&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CA - CLG - Gas Explosion Welcome Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>General Class Action</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.What_type_of_Class_Action_case_is_it__c</field>
            <operation>contains</operation>
            <value>Gas Explosion</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Retainer_Received__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Complex Lit - Class Action - CLG - Gas Explosion Welcome Letter</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CA_CLG_Gas_Explosion_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Welcome_Letter_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Intake__c.Retainer_Received__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CCC - 30-day Turn Down Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Wage &amp; Hour</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>If Under Review for more than 30 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_Turn_Down</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_TD_Reason_Attorney_not_interested</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Turn_Down_Date_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.Under_Review_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CCC - Attorney Assigned</fullName>
        <actions>
            <name>Assigned_Attorney_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Alerts_Person_Assigned_to_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>CCC or Complex Litiigation - Attorney Assigned</description>
        <formula>AND(
Assigned_Attorney__r.Email &lt;&gt; &quot;steve@knechtlaws.com&quot;,
Assigned_Attorney__r.Email &lt;&gt; &quot;cases@knechtgroup.com&quot;,
Assigned_Attorney__r.Email &lt;&gt; &quot;rweil@weillawfirm.net&quot;,
OR(
AND(
ISNEW(),
NOT(ISBLANK(Assigned_Attorney__c))),
ISCHANGED(Assigned_Attorney__c)),
OR(
RecordType.DeveloperName = &quot;CCC&quot;,
RecordType.DeveloperName = &quot;Complex Litigation&quot;,
ISPICKVAL(Case_Type__c, &quot;Cast Iron Pipes&quot;)),
OR(
AND(
ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
NOT(ISPICKVAL(Case_Type__c, &quot;Wage &amp; Hour&quot;))),
ISPICKVAL(Handling_Firm__c, &quot;Nation Law&quot;)),
NOT(ISPICKVAL(Case_Type__c, &quot;PIP&quot;)),
NOT(ISPICKVAL(Type_of_Policy__c, &quot;Diminished Value&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Auto TD -Class Action</fullName>
        <actions>
            <name>CCC_Auto_TD_Class_Action_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Client_Turn_Down_Notification</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3 AND 4 AND 5) or (3 AND 6))</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>classaction.com,classactionsnews.com,CA.com,pipelawsuit.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>TD by Agent - Automatic TD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>contains</operation>
            <value>HubSpot,infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>FTP Wash Amtrak Crash</value>
        </criteriaItems>
        <description>Auto Turn down email sent to client for Class Action</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Auto TD -M%26M</fullName>
        <actions>
            <name>CCC_Auto_TD_M_M_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Client_Turn_Down_Notification</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3 AND 4 AND 5) or (3 AND 6))</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>notEqual</operation>
            <value>classaction.com,classactionsnews.com,CA.com,pipelawsuit.com,nationlaw.com,abogados.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>TD by Agent - Automatic TD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>contains</operation>
            <value>HubSpot,infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>FTP Wash Amtrak Crash</value>
        </criteriaItems>
        <description>Auto Turn down email sent to client for MM / Non-CA cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - CAT SUIP Orlando Notify Sean S</fullName>
        <actions>
            <name>CCC_CAT_SUIP_Notify_Sean_S</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>FL - Orlando</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Personal Injury,Premises Liability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>notEqual</operation>
            <value>Lake</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>notEqual</operation>
            <value>Marion</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>notEqual</operation>
            <value>Sumter</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>notEqual</operation>
            <value>Levy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>notEqual</operation>
            <value>Volusia</value>
        </criteriaItems>
        <description>All orlando venue intakes that go SUIP notify Sean s</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Case Manager Assigned</fullName>
        <actions>
            <name>CCC_Case_Manager_Developer_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Alerts_Person_Assigned_to_Case</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>CCC or Complex Litigation - Case Manager Assigned</description>
        <formula>AND(   OR(     AND(       ISNEW(),       NOT(ISBLANK(Case_Manager_Name__c))     ),     ISCHANGED(Case_Manager_Name__c)   ),   OR(     RecordType.DeveloperName = &quot;CCC&quot;,     RecordType.DeveloperName = &quot;Complex Litigation&quot;,     ISPICKVAL(Case_Type__c, &quot;Cast Iron Pipes&quot;)   ),   OR(     AND(       ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),       NOT(ISPICKVAL(Case_Type__c, &quot;Wage &amp; Hour&quot;))     ),     ISPICKVAL(Handling_Firm__c, &quot;Nation Law&quot;)   ),   NOT(ISPICKVAL(Case_Type__c, &quot;PIP&quot;)),   NOT(ISPICKVAL(Type_of_Policy__c, &quot;Diminished Value&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Cat - Converted Notify Atty</fullName>
        <actions>
            <name>CCC_Cat_Notify_Assigned_Atty_Converted_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends assigned atty notification that their intake has been converted - Modified to also include any workers comp</description>
        <formula>OR( Catastrophic__c, ISPICKVAL(Litigation__c,&quot;Workers Compensation&quot;) ) &amp;&amp; TEXT(PRIORVALUE(Status__c))&lt;&gt;&quot;Converted&quot; &amp;&amp; ISPICKVAL(Status__c,&quot;Converted&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Notification - TD</fullName>
        <actions>
            <name>Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic
(CCC,Complex Litigation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Notification - TD - No Atty</fullName>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic - NO ATTY
(CCC,Complex Litigation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Notification - TD - WC%2FPL</fullName>
        <actions>
            <name>Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Catastrophic_Case_Under_Review_Turned_Down_No_Mike_Morgan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic - INCLUDES WC/PL
(CCC,Complex Litigation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Notification - TD - WC%2FPL - No Atty</fullName>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic - INCLUDES WC/PL - NO ATTY EMAIL
(CCC,Complex Litigation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Notification - UR</fullName>
        <actions>
            <name>Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic
(CCC,Complex Litigation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Notification - UR - WC%2FPL</fullName>
        <actions>
            <name>Catastrophic_Case_Under_Review_Turned_Down_No_Mike_Morgan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic - INCLUDES WC/PL
(CCC,Complex Litigation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Package Sent</fullName>
        <actions>
            <name>Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Attorney Approval Email - Catastrophic - NON WC, PL
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),     ISPICKVAL(Status_Detail__c, &quot;SMS&quot;),    ISPICKVAL(Status_Detail__c, &quot;SMS sent&quot;)   ),   NOT(ISPICKVAL(Litigation__c, &quot;Product Liability&quot;)),   NOT(ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)),   OR(     ISBLANK(Under_Review_Date__c),     Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;),     Isblank(Assigned_Attorney__c)   ),   ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Package Sent - No Atty</fullName>
        <actions>
            <name>CCC_Catastrophic_Case_Scheduled_for_Sign_Up_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Attorney Approval Email - Catastrophic - NON WC, PL - NO ATTY
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),     ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   NOT(ISPICKVAL(Litigation__c, &quot;Product Liability&quot;)),   NOT(ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)),   OR(     NOT(ISBLANK(Under_Review_Date__c)),     AND(       NOT(Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;)),       NOT(Isblank(Assigned_Attorney__c))     )   ),   ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Package Sent - WC%2FPL</fullName>
        <actions>
            <name>Catastrophic_Case_Scheduled_for_Sign_Up_No_Mike_Morgan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Attorney Approval Email - Catastrophic - INCLUDES WC, PL 
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),     ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   OR(     ISPICKVAL(Litigation__c, &quot;Product Liability&quot;),     ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)   ),   OR(     ISBLANK(Under_Review_Date__c),     Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;),     Isblank(Assigned_Attorney__c)   ),   ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Catastrophic Case - Package Sent - WC%2FPL - No Atty</fullName>
        <actions>
            <name>CCC_Catastrophic_Case_Scheduled_for_Sign_Up_No_Copy_Mike_Morgan_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Attorney Approval Email - Catastrophic - INCLUDES WC, PL - NO ATTY
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),     ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   OR(     ISPICKVAL(Litigation__c, &quot;Product Liability&quot;),     ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)   ),   OR(     NOT(ISBLANK(Under_Review_Date__c)),     AND(       NOT(Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;)),       NOT(Isblank(Assigned_Attorney__c))     )   ),   ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Generating %26 Assigned Attorney Status Change Email</fullName>
        <actions>
            <name>Generating_Attorney_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Generating_Atty_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Anytime an intake status is updated, notify the generating attorney</description>
        <formula>ISCHANGED(Status__c)   &amp;&amp;  NOT(ISBLANK(Generating_Attorney__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Mike Morgan Email Alerts</fullName>
        <actions>
            <name>CCC_Mike_Morgen_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If Cat Auto OR Cat WC OR Cat Kentucky
Email Mike Morgan</description>
        <formula>Catastrophic__c &amp;&amp; OR( ISPICKVAL(Litigation__c,&quot;Workers Compensation&quot;), ISPICKVAL(Case_Type__c,&quot;Automobile Accident&quot;), State_of_incident__c = &quot;KY&quot; ) &amp;&amp; OR( ISNEW(), ISCHANGED(Status__c) ) &amp;&amp; OR( ISPICKVAL(Status__c, &quot;Turn Down&quot;), ISPICKVAL(Status__c, &quot;Under Review&quot;), ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC - New Case - SUIP</fullName>
        <actions>
            <name>New_Case_Email_SUIP</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_New_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send New case alert that a case has been scheduled for investigation 
(CCC, Complex Lit)</description>
        <formula>ISPICKVAL(Status__c,&quot;Sign Up In Progress&quot;)  &amp;&amp;  ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;)  &amp;&amp; OR (RecordType.Name = &quot;CCC&quot;, RecordType.Name = &quot;Complex Litigation&quot;)  &amp;&amp;  OR(  ISBLANK(Assigned_Attorney__c),  Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - New Case Retainer Received Esign%2FMail Out</fullName>
        <actions>
            <name>New_Case_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_New_Case_Retainer_Receive</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send email to new case when signed up
(CCC, Complex Lit)</description>
        <formula>AND(
  ISPICKVAL(Status__c, &quot;Retainer Received&quot;),
  OR(
    ISPICKVAL(Case_Type__c, &quot;Cast Iron Pipes&quot;),
    AND(
      OR(
        ISPICKVAL(Status_Detail__c, &quot;Esign&quot;),
        ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)
      ),
      OR(
        RecordType.Name = &quot;CCC&quot;,
        RecordType.Name = &quot;Complex Litigation&quot;
      )
    )
  ),
  Assigned_Attorney__r.Email &lt;&gt; &quot;steve@knechtlaws.com&quot;,
  OR(
    ISBLANK(Assigned_Attorney__c),
    Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Non Cat - Attorney Approval</fullName>
        <actions>
            <name>Send_Approval_Email_to_Attorneys</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Alerts_Case_Status_Various_Roles</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3 AND ( 2 or(4 and 5)) and 6) OR (1 AND 2 AND 6 AND 7)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation,Consumer Protection,Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>notEqual</operation>
            <value>Nationwide,Out of Area,Out of Country</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Ben Crump</value>
        </criteriaItems>
        <description>Send Attorney Approval Email - (CCC, Complex Lit, Consumer Protection, Mass Tort)
Updated to Handle Ben Crump Cat and Non Cat UR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC - Ultra-Catastrophic Case -</fullName>
        <actions>
            <name>Ultra_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Ultra_Cat</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Anytime an intake status is updated, notify the Ultra Cat Team</description>
        <formula>ISCHANGED(Status__c)   &amp;&amp;     ISPICKVAL(Severity__c, &quot;Ultra Catastrophic&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Notification - TD</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <description>Send Attorney Approval Email - Catastrophic
(CCC, Complex Lit)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Notification - TD - No Atty</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <description>*Nation Law* Send Attorney Approval Email - Catastrophic - NO ATTY
(CCC, Complex Lit)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Notification - TD - WC%2FPL</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <description>*Nation* Send Attorney Approval Email - Catastrophic - INCLUDES WC/PL
(CCC, Complex Lit)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Notification - TD - WC%2FPL - No Atty</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan_No_Att</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <description>*Nation* Send Attorney Approval Email - Catastrophic - INCLUDES WC/PL - NO ATTY EMAIL  (CCC, Complex Lit)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Notification - UR</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>*Nation* Send Attorney Approval Email - Catastrophic
(CCC, Complex Lit)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Notification - UR - WC%2FPL</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Under_Review_Turned_Down_No_Copy_Mike_Morgan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Catastrophic_Case_Under_Review_Turned_Down</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC,Complex Litigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Product Liability,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>*Nation* Send Attorney Approval Email - Catastrophic - INCLUDES WC/PL
(CCC, Complex Lit)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Package Sent</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>*Nation* Send Attorney Approval Email - Catastrophic - NON WC, PL
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),         ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   NOT(ISPICKVAL(Litigation__c, &quot;Product Liability&quot;)),   NOT(ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)),   OR(     ISBLANK(Under_Review_Date__c),     Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;),     Isblank(Assigned_Attorney__c)   ),   ISPICKVAL(Handling_Firm__c, &quot;Nation Law&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Package Sent - No Atty</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>*Nation* Send Attorney Approval Email - Catastrophic - NON WC, PL - NO ATTY
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),         ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   NOT(ISPICKVAL(Litigation__c, &quot;Product Liability&quot;)),   NOT(ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)),   OR(     NOT(ISBLANK(Under_Review_Date__c)),     AND(       NOT(Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;)),       NOT(Isblank(Assigned_Attorney__c))     )   ),   ISPICKVAL(Handling_Firm__c, &quot;Nation Law&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Package Sent - WC%2FPL</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up_No_Copy_Mike_Morgan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>*Nation* Send Attorney Approval Email - Catastrophic - INCLUDES WC, PL
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),         ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   OR(     ISPICKVAL(Litigation__c, &quot;Product Liability&quot;),     ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)   ),   OR(     ISBLANK(Under_Review_Date__c),     Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;),     Isblank(Assigned_Attorney__c)   ),   ISPICKVAL(Handling_Firm__c, &quot;Nation Law&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCC -Nation- Catastrophic Case - Package Sent - WC%2FPL - No Atty</fullName>
        <actions>
            <name>CCC_Nation_Catastrophic_Case_Scheduled_for_Sign_Up_No_Copy_Mike_Morgan_No_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Attorney Approval Email - Catastrophic - INCLUDES WC, PL - NO ATTY
(CCC, Complex Lit)</description>
        <formula>AND(   ISPICKVAL(Status__c, &quot;Sign Up In Progress&quot;),   OR(     RecordType.Name = &quot;CCC&quot;,     RecordType.Name = &quot;Complex Litigation&quot;   ),   Catastrophic__c,   OR(     ISPICKVAL(Status_Detail__c, &quot;Mailout Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Esign Sent&quot;),     ISPICKVAL(Status_Detail__c, &quot;Investigation Scheduled&quot;),         ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)   ),   OR(     ISPICKVAL(Litigation__c, &quot;Product Liability&quot;),     ISPICKVAL(Litigation__c, &quot;Workers Compensation&quot;)   ),   OR(     NOT(ISBLANK(Under_Review_Date__c)),     AND(       NOT(Contains(Assigned_Attorney__r.Account.Name, &quot;Legacy&quot;)),       NOT(Isblank(Assigned_Attorney__c))     )   ),   ISPICKVAL(Handling_Firm__c, &quot;Nation Law&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CIP South FL Dousign</fullName>
        <actions>
            <name>CIP_SFL_Docusign_ID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CIP_SFL_Docusign_Invest_ID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>FL - Fort Myers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>equals</operation>
            <value>Broward</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>equals</operation>
            <value>Palm Beach</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>equals</operation>
            <value>Monroe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>equals</operation>
            <value>Miami-Dade</value>
        </criteriaItems>
        <description>Sets the Docusign ID for South FL CIP cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CP4 Fuel Injection Pump Web Lead Set to Referred Out</fullName>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>CP4 Bosch Fuel Pump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>ForThePeople CP4 Fuel Injection Pump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Processor Email Alert to Convert Intake</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>Internal Attorney</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>Attorney Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Do not call - already signed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs to be Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received</value>
        </criteriaItems>
        <description>Email alert sent to Case Processors to set status to Converted, when an intake is created via the attorney portal and is already signed up.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_already_signed_CCC_not_calling_PC_Please_set_status_to_Converted</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cast Iron Pipes Auto Emails %28Nation Cases%29</fullName>
        <actions>
            <name>Cast_Iron_Pipes_Auto_Emails_Nation_Cases</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Cast_Iron_Pipes_Auto_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Contract_Venue__c</field>
            <operation>notEqual</operation>
            <value>FL - Miami</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.State_of_incident__c</field>
            <operation>equals</operation>
            <value>FL</value>
        </criteriaItems>
        <description>Sends alert to the client when a case has been converted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cast Iron Pipes Auto Emails %28South FL only%29</fullName>
        <actions>
            <name>Cast_Iron_Pipes_Auto_Emails_South_FL_only</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Cast_Iron_Pipes_Auto_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Contract_Venue__c</field>
            <operation>equals</operation>
            <value>FL - Miami</value>
        </criteriaItems>
        <description>Sends alert to the client when a South Florida case has been converted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cast Iron Pipes Litigation Update</fullName>
        <actions>
            <name>Set_Cast_Iron_Pipes_Litigation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes</value>
        </criteriaItems>
        <description>Updates the litigation type for cast iron pipes cases to Insurance Dispute, if the litigation type is originally set to Mass Tort.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cat Case Turn Down Email Notification</fullName>
        <actions>
            <name>Cat_Qualified_Turn_Down_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Alerts_Case_Status_Various_Roles</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Qualified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <description>Email notification for a Cat case that is marked TD</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Catastrophic Out of Area Cases</fullName>
        <actions>
            <name>CCC_Catastrophic_Case_Out_of_Area_Referred_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Alerts_Case_Status_Various_Roles</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Personal Injury,Premises Liability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>Out of Area,CA - Los Angeles,MA - Boston,NJ - New Jersey,NY - New York</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends an email notification to Jake, Tyler, Bret and Will Green for PI/Prem cases that are referred out.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Child Victim NJ Web Lead Set to Referred Out</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Rape &amp; Molestation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>Classaction NJ Child Victims Act</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>State: Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>Age: Over 18 (Adult)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>Where did this happen: Home</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>State:Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>Where did this happen: Family Members House</value>
        </criteriaItems>
        <description>Auto set status to referred out for Child Victims Act</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Child Victim NJ Web Lead Set to TD</fullName>
        <actions>
            <name>Child_Victim_Auto_TD_CA_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Status_Detail_TD_by_Criteria</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Turned_Down</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND (5 OR 6 OR 7 OR 8 OR 9)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Rape &amp; Molestation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out,Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>Classaction NJ Child Victims Act</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>State: Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Age: Over 18 (Adult)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Where did this happen: Home</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>State:Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Where did this happen: Family Members House</value>
        </criteriaItems>
        <description>Auto set status to referred out for Child Victims Act</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Child Victim Web Lead Set to Referred Out</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Rape &amp; Molestation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>contains</operation>
            <value>Child Victims Act</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>State: Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>Age: Over 18 (Adult)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>Where did this happen: Home</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>State:Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>notContain</operation>
            <value>Where did this happen: Family Members House</value>
        </criteriaItems>
        <description>Auto set status to referred out for Child Victims Act</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Child Victim Web Lead Set to TD</fullName>
        <actions>
            <name>Child_Victim_Auto_TD_CA_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Status_Detail_TD_by_Criteria</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Turned_Down</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND (5 OR 6 OR 7 OR 8 OR 9)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Rape &amp; Molestation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out,Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>contains</operation>
            <value>Child Victims Act</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>State: Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Age: Over 18 (Adult)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Where did this happen: Home</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>State:Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Where did this happen: Family Members House</value>
        </criteriaItems>
        <description>Auto set status to referred out for Child Victims Act</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Clergy PA Web Lead Set to Referred Out</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Rape &amp; Molestation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>classaction Clergy Abuse PA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <description>Auto set status to referred out for Clergy PA</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Conflict Notification to UR Team</fullName>
        <actions>
            <name>Conflict_Email_to_Conflict_Team</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Conflict_Status__c</field>
            <operation>equals</operation>
            <value>Pending Conflict</value>
        </criteriaItems>
        <description>This rule triggers on &quot;Conflicts Status&quot; and email the Conflict Team via a Public Group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DicambaAlert</fullName>
        <actions>
            <name>DicambaAlert</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>DicambaAlert webhook to Zapier</description>
        <formula>OR( AND( ISCHANGED(Case_Type__c), ISPICKVAL(Case_Type__c, &quot;Dicamba&quot;), PRIORVALUE(Case_Type__c) &lt;&gt; &quot;Dicamba&quot; ), AND( ISNEW(), ISPICKVAL(Case_Type__c, &quot;Dicamba&quot;)), AND(ISPICKVAL(Case_Type__c, &quot;Dicamba&quot;), ISCHANGED(Status__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Draw Loop Automated Email TCPA Welcome Packet</fullName>
        <actions>
            <name>LOOPlus_Intake_TCPA_Welcome_Packet</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>TCPA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <description>This WF sends out an automated DDP packet via email to the client 3 days after the status has been set to converted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LOOPlus_Intake_TCPA_Welcome_Packet</name>
                <type>OutboundMessage</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - CCC Sign Up In Progress</fullName>
        <active>true</active>
        <description>Drip emails for sign up in progress for (CCC, Complex Lit)</description>
        <formula>AND( ISPICKVAL(Status__c,&quot;Sign Up In Progress&quot;), OR( ISPICKVAL(Status_Detail__c,&quot;Esign Sent&quot;),ISPICKVAL(Status_Detail__c,&quot;Mailout Sent&quot;) ), OR( RecordType.DeveloperName = &quot;CCC&quot;, RecordType.DeveloperName = &quot;Complex_Litigation&quot; ), NOT(ISPICKVAL(Litigation__c,&quot;Veteran Disability&quot;)), OR(ISPICKVAL(Client__r.Language__c,&quot;English&quot;),ISPICKVAL(Client__r.Language__c,&quot;Spanish&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_CCC_SUIP_5_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X5_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_CCC_SUIP_20_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X20_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_CCC_SUIP_10_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X10_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_CCC_SUIP_14_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X14_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - Labor - Labor%2FTCPA Lawsuit</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Consumer Protection</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <description>Sends email based on Labor Lits</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Labor_Labor_TCPA_Act_now_you_may_be_entitled_to_compensation_DAY_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Labor_Labor_TCPA_Time_is_running_out_DAY_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Labor_Labor_TCPA_Don_t_miss_your_chance_We_want_to_help_you_DAY_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Labor_Labor_TCPA_Urgent_We_have_not_begun_working_on_your_potential_claim_y</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - MT - Energy Drink Lawsuit</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Energy Drink</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>energydrinklawsuit.com</value>
        </criteriaItems>
        <description>Sends email based on EDL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_EDL_Time_is_running_out_DAY_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_EDL_Act_now_you_may_be_entitled_to_compensation_DAY_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_EDL_Don_t_miss_your_chance_We_want_to_help_you_DAY_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_EDL_Urgent_We_have_not_begun_working_on_your_potential_claim_yet_DAY_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - MT - Energy Drink Lawsuit %28CLG%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Energy Drink</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <description>Sends email based on CLG</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_CLG_Don_t_miss_your_chance_We_want_to_help_you_DAY_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_CLG_Act_now_you_may_be_entitled_to_compensation_DAY_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_CLG_Time_is_running_out_DAY_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_MT_CLG_Urgent_We_have_not_begun_working_on_your_potential_claim_yet_DAY_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - MT - Energy Drink Lawsuit %28Create task%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Energy Drink</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>energydrinklawsuit.com</value>
        </criteriaItems>
        <description>Create Task for when the email is sent out</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Drip Email - MT Retainer Sent</fullName>
        <active>true</active>
        <description>Drip emails for sign up in progress for Mass Tort</description>
        <formula>AND( ISPICKVAL(Status__c,&quot;Retainer Sent&quot;), RecordType.DeveloperName = &quot;Mass_Tort&quot;,  OR(ISPICKVAL(Client__r.Language__c,&quot;English&quot;),ISPICKVAL(Client__r.Language__c,&quot;Spanish&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_MT_Retainer_Sent_5_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X5_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_MT_Retainer_Sent_10_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X10_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_MT_Retainer_Sent_14_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X14_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_MT_Retainer_Sent_20_Day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>X20_Day_SUIP_Reminder_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - PI Morgan RR to TD</fullName>
        <active>false</active>
        <description>only Morgan PI/Prem gets email</description>
        <formula>AND(
  ISPICKVAL(Status__c, &quot;Turn Down&quot;),
  NOT(ISBLANK(Retainer_Received__c)),
  ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
  NOT(ISPICKVAL(Marketing_Source__c, &quot;BenCrump.com&quot;)),
  NOT(ISPICKVAL(Marketing_Source__c, &quot;PersonalInjury.com&quot;)),
  NOT(ISPICKVAL(Marketing_Source__c, &quot;PICOM&quot;)),
  OR(
    ISPICKVAL(Litigation__c, &quot;Premises Liability&quot;),
    ISPICKVAL(Litigation__c, &quot;Personal Injury&quot;)
  ),
  OR(
    ISPICKVAL(Client__r.Language__c, &quot;English&quot;),
    ISPICKVAL(Client__r.Language__c, &quot;&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_Turn_Down_1_Hour</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_1_Hour_TD_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - PI Morgan Retainer Received - Contains Link</fullName>
        <actions>
            <name>Drip_Email_RR_1_Hour_With_Link</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_1_Hour_Retainer_Received_with_Link</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>only Morgan PI/Prem gets email with link to questionnaire.

MUST BE UPDATED WHEN CASE TYPE QUESTIONNAIRE READY</description>
        <formula>AND(
  ISPICKVAL(Status__c, &quot;Retainer Received&quot;),
  ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
  OR(
  ISPICKVAL(Status_Detail__c, &quot;Esign&quot;),
   ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)
   ),
  OR(
    ISPICKVAL(Case_Type__c, &quot;Automobile Accident&quot;),
    ISPICKVAL(Case_Type__c, &quot;General Injury&quot;),
    ISPICKVAL(Case_Type__c, &quot;Slip and Fall&quot;),
    ISPICKVAL(Case_Type__c, &quot;Animal Incident&quot;)
  ),
  OR(
    ISPICKVAL(Litigation__c, &quot;Premises Liability&quot;),
    ISPICKVAL(Litigation__c, &quot;Personal Injury&quot;)
  ),
  OR(
    ISPICKVAL(Client__r.Language__c, &quot;English&quot;),
    ISPICKVAL(Client__r.Language__c, &quot;&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_RR_4_Hour_With_Link</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_4_Hour_Retainer_Received_with_Link</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - PI Morgan Retainer Received - Contains Link - 1 HR Follow Up</fullName>
        <active>true</active>
        <description>1 HR Follow Up Email - Only Morgan PI/Prem gets email with link to questionnaire.

MUST BE UPDATED WHEN CASE TYPE QUESTIONNAIRE READY</description>
        <formula>AND(
  ISPICKVAL(Status__c, &quot;Retainer Received&quot;),
  ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
  OR(
  ISPICKVAL(Status_Detail__c, &quot;Esign&quot;),
   ISPICKVAL(Status_Detail__c, &quot;SMS&quot;)
   ),
  OR(
    ISPICKVAL(Case_Type__c, &quot;Automobile Accident&quot;),
    ISPICKVAL(Case_Type__c, &quot;General Injury&quot;),
    ISPICKVAL(Case_Type__c, &quot;Slip and Fall&quot;),
    ISPICKVAL(Case_Type__c, &quot;Animal Incident&quot;)
  ),
  OR(
    ISPICKVAL(Litigation__c, &quot;Premises Liability&quot;),
    ISPICKVAL(Litigation__c, &quot;Personal Injury&quot;)
  ),
  OR(
    ISPICKVAL(Client__r.Language__c, &quot;English&quot;),
    ISPICKVAL(Client__r.Language__c, &quot;&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_RR_1_Hour_With_Link_Follow_Up</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_1_Hour_Retainer_Received_with_Link_Follow_Up</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - PI Morgan Retainer Received - No Link</fullName>
        <active>true</active>
        <description>only Morgan PI/Prem gets email WITHOUT link to questionnaire.

Add when go live:   NOT(ISPICKVAL(Case_Type__c,&quot;Automobile Accident&quot;)),

MUST BE UPDATED WHEN CASE TYPE QUESTIONNAIRE READY</description>
        <formula>AND(
  ISPICKVAL(Status__c, &quot;Retainer Received&quot;),
  ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),
  NOT(ISPICKVAL(Case_Type__c, &quot;Automobile Accident&quot;)),
  NOT(ISPICKVAL(Case_Type__c, &quot;Slip and Fall&quot;)),
  NOT(ISPICKVAL(Case_Type__c, &quot;General Injury&quot;)),
  NOT(ISPICKVAL(Case_Type__c, &quot;Animal Incident&quot;)),
  OR(
    ISPICKVAL(Litigation__c, &quot;Premises Liability&quot;),
    ISPICKVAL(Litigation__c, &quot;Personal Injury&quot;)
  ),
  OR(
    ISPICKVAL(Client__r.Language__c, &quot;English&quot;),
    ISPICKVAL(Client__r.Language__c, &quot;&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_Email_RR_1_Hour</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_1_Hour_Retainer_Received</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_4_Hour_Retainer_Received</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_4_Hour_Retainer_Received</name>
                <type>Task</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Need DLI</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Date of Last Injury (DLI)</value>
        </criteriaItems>
        <description>SS Send drip emails if status detail = Need DLI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_DLI_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_DLI_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_DLI_Day_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Need Denial Letter</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs Denial Letter</value>
        </criteriaItems>
        <description>SS Send drip emails if status detail = Need Denial Letter</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_Denial_Letter_Day_7</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_Denial_Letter_Day_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_Denial_Letter_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Need FCQ</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>FCQ</value>
        </criteriaItems>
        <description>SS Send drip emails if status detail = Need FCQ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_FCQ_Day_7</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Need_FCQ_Day_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Need More Info</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs More Information</value>
        </criteriaItems>
        <description>SS Send drip emails if status detail = Need more info</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_CNF_More_Info_Needed_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_CNF_More_Info_Needed_Day_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_CNF_More_Info_Needed_Day_21</name>
                <type>Alert</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_CNF_More_Info_Needed_Day_7</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Need More Info  Update</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs More Information</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Attorney Requested More Information</value>
        </criteriaItems>
        <description>SS Send drip emails if status detail = Need more info</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_CNF_More_Info_Needed_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SS_More_Info_Needed_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SS_More_Info_Needed_Day_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SS_More_Info_Needed_Day_3</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Paper Retainer</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Mail Sent,New Application - Mail Sent,Open Claim - Mail Sent</value>
        </criteriaItems>
        <description>SS Send drip emails if status = Paper Retainer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Paper Retainer Update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>New Application - Mail Sent,Open Claim - Mail Sent</value>
        </criteriaItems>
        <description>SS Send drip emails if status = Paper Retainer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Paper_Retainer_Day_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Retainer Received - Needs App</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent,Retainer Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs Application Filed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>SS Send drip emails if status = Paper Retainer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SS_Retainer_received_Needs_App_Day_3</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SS_Retainer_received_Needs_App_Day_20</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SS_Retainer_received_Needs_App_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SS_Retainer_received_Needs_App_Day_2</name>
                <type>Alert</type>
            </actions>
            <timeLength>46</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Unable to Contact</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Last_Call_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Last_Correct_Contact__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>SS Send drip emails if Last Correct Contact = null and Last attempt not null</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_30</name>
                <type>Alert</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Drip Email - SS - Unable to Contact Update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Email_Unsubscribe__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs to be Qualified</value>
        </criteriaItems>
        <description>SS Send drip emails if Last Correct Contact = null and Last attempt not null</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_14</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_10</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_30</name>
                <type>Alert</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_5</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Drip_SS_Unable_to_Contact_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>46</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Exhausted Leads</fullName>
        <actions>
            <name>Set_Status_Detail_to_Exhausted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Number_Outbound_Dials_Status__c</field>
            <operation>equals</operation>
            <value>12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs to be Qualified</value>
        </criteriaItems>
        <description>Updates the status detail of Intake leads, once they hit a certain dial count (12). The new status detail would be “Exhausted”.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Exhausted MT Leads</fullName>
        <actions>
            <name>Set_Status_Detail_to_Exhausted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Number_Outbound_Dials_Status__c</field>
            <operation>equals</operation>
            <value>7</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs to be Qualified</value>
        </criteriaItems>
        <description>Updates the status detail of Mass Tort leads once they hit a certain dial count (7). The new status detail would be “Exhausted”.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FCRA - How To Dispute Inaccuracies Letter</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>FCRA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type_Specific_Questions__c</field>
            <operation>contains</operation>
            <value>Have you disputed the incorrect information to the creditor or credit agency?  -  No</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FCRA_How_To_Dispute_Inaccuracies</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>FCRA_How_to_Email_Sent</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FCRA - How To Dispute Inaccuracies Mailout</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>FCRA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type_Specific_Questions__c</field>
            <operation>contains</operation>
            <value>Have you disputed the incorrect information to the creditor or credit agency?  -  No</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FCRA_How_To_Dispute_Inaccuracies_Packet</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FCRA - How To Get Credit Report Letter</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>FCRA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Do_they_have_a_crash_report__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FCRA_How_To_Get_Credit_Report</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>FCRA_How_to_Email_Sent</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FCRA - How To Get Credit Report Mailout</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>FCRA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Do_they_have_a_crash_report__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FCRA_How_To_Get_Credit_Report_Packet</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>First CM Conversation</fullName>
        <actions>
            <name>Email_Client_First_Staff_Conversation_Follow_Up_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Updates CM First Conversation Date and Sends Email to Client

** THIS HAS BEEN REPLACED WITH ANOTHER WFR. DO NOT ACTIVATE.**</description>
        <formula>AND(
				ISPICKVAL(Handling_Firm__c,&quot;Morgan &amp; Morgan&quot;),
				Last_Disposition__c=&quot;First CM Conversation&quot;,
				OR(
								CONTAINS($UserRole.Name,&quot;CCC Case Processor&quot;),
								CONTAINS($UserRole.Name,&quot;Case Manager&quot;),
								CONTAINS($UserRole.Name,&quot;Assistant&quot;),
								CONTAINS($UserRole.Name,&quot;Pre-Lit&quot;),
								CONTAINS($UserRole.Name,&quot;Paralegal&quot;)
				),
				ISBLANK(First_Staff_Conversation_Date__c),
				NOT(
								Includes(Type_of_Injury__c,&quot;Death&quot;)
				),
				ISBLANK(Deceased_Date__c),
				NOT(
								ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;)
				),
				NOT(
								ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;)
				),
				NOT(Catastrophic__c),
				NOT(ISBLANK(Case_Manager_Name__c)
				)
			)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation %28Attorney Called%29</fullName>
        <actions>
            <name>Send_First_Conversation_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email_Atty</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Updates CM First Conversation Date and Sends Email to Client - this is if Role = attorney or SME

** THIS HAS BEEN REPLACED WITH ANOTHER WFR. DO NOT ACTIVATE.**</description>
        <formula>AND( ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),Last_Disposition__c = &quot;First CM Conversation&quot;, OR( CONTAINS($UserRole.Name,&quot;CCC SME&quot;),CONTAINS($UserRole.Name,&quot;Attorney&quot;)), ISBLANK(First_Staff_Conversation_Date__c), NOT(Includes(Type_of_Injury__c, &quot;Death&quot;)), ISBLANK(Deceased_Date__c), NOT(ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;)), NOT(ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;)), NOT(Catastrophic__c), NOT(ISBLANK(Assigned_Attorney__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation %28Attorney Called%29 - No Treatment</fullName>
        <actions>
            <name>Send_First_Conversation_Atty_No_Treatment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email_Atty_No_Treatment</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>No Treatment in template</description>
        <formula>AND( ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),Last_Disposition__c = &quot;First CM Conversation&quot;, OR(CONTAINS($UserRole.Name,&quot;CCC SME&quot;),  CONTAINS($UserRole.Name,&quot;Attorney&quot;)  ), ISBLANK(First_Staff_Conversation_Date__c), OR( Includes(Type_of_Injury__c, &quot;Death&quot;), NOT(ISBLANK(Deceased_Date__c)), ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;), ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;), Catastrophic__c ), NOT(ISBLANK(Assigned_Attorney__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation %28Attorney Called%29 - Not PI or Prem</fullName>
        <actions>
            <name>Send_First_Conversation_Atty_No_Video_Link</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email_Atty</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Updates CM First Conversation Date and Sends Email to Client - this is if Role = attorney or SME
(CM) - Only for non-PI or Premises matters</description>
        <formula>AND( 
ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;), 
Last_Disposition__c = &quot;First CM Conversation&quot;, 
NOT(ISPICKVAL(Litigation__c,&quot;Personal Injury&quot;)), 
NOT(ISPICKVAL(Litigation__c,&quot;Labor&quot;)), 
NOT(ISPICKVAL(Litigation__c,&quot;Employment Law&quot;)), 
NOT(ISPICKVAL(Litigation__c,&quot;Premises Liability&quot;)), 
OR( CONTAINS($UserRole.Name,&quot;CCC SME&quot;), 
CONTAINS($UserRole.Name,&quot;Attorney&quot;)), 
ISBLANK(First_Staff_Conversation_Date__c), 
NOT(Includes(Type_of_Injury__c, &quot;Death&quot;)), 
ISBLANK(Deceased_Date__c), 
NOT(ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;)), 
NOT(ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;)), 
NOT(Catastrophic__c), 
NOT(ISBLANK(Assigned_Attorney__c) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation %28Attorney Called%29 - PI and Prem</fullName>
        <actions>
            <name>Send_First_Conversation_Atty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email_Atty</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Updates CM First Conversation Date and Sends Email to Client - this is if Role = attorney or SME
(CM) - Only for PI and Premises matters</description>
        <formula>AND( 				ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;), 				Last_Disposition__c = &quot;First CM Conversation&quot;, 				OR( 								ISPICKVAL(Litigation__c,&quot;Personal Injury&quot;), 								ISPICKVAL(Litigation__c,&quot;Premises Liability&quot;) 				),  				OR(          CONTAINS($UserRole.Name,&quot;CCC SME&quot;),          CONTAINS($UserRole.Name,&quot;Attorney&quot;)      ), 				ISBLANK(First_Staff_Conversation_Date__c), 				NOT(Includes(Type_of_Injury__c, &quot;Death&quot;)), 				ISBLANK(Deceased_Date__c), 				NOT(ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;)), 				NOT(ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;)), 				NOT(Catastrophic__c), 				NOT(ISBLANK(Assigned_Attorney__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation - No Email</fullName>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates CM First Conversation Date but does not send email to Client</description>
        <formula>AND(ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;),Last_Disposition__c = &quot;First CM Conversation - No Email&quot;, ISBLANK(First_Staff_Conversation_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation - No Treatment</fullName>
        <actions>
            <name>Email_Client_First_Staff_Conversation_No_Treatment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email_No_Treatment</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>No Treatment in template</description>
        <formula>AND( 
    ISPICKVAL(Handling_Firm__c, &quot;Morgan &amp; Morgan&quot;), 
    Last_Disposition__c = &quot;First CM Conversation&quot;, 
    OR( 
        CONTAINS($UserRole.Name, &quot;Case Processor&quot;), 
        CONTAINS($UserRole.Name, &quot;Case Manager&quot;), 
        CONTAINS($UserRole.Name, &quot;Pre-Lit&quot;),
								CONTAINS($UserRole.Name, &quot;Assistant&quot;),
        CONTAINS($UserRole.Name, &quot;Paralegal&quot;) 
    ), 
    ISBLANK(First_Staff_Conversation_Date__c), 
    OR( 
        Includes(Type_of_Injury__c, &quot;Death&quot;), 
        NOT(ISBLANK(Deceased_Date__c)
				), 
    ISPICKVAL(Can_IP_sign__c, &quot;No - Deceased&quot;), 
    ISPICKVAL(Case_Type__c, &quot;Rape &amp; Molestation&quot;), 
    Catastrophic__c), 
    NOT(ISBLANK(Case_Manager_Name__c)) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation - Not PI or Prem</fullName>
        <actions>
            <name>Email_Client_First_Staff_Conversation_Follow_Up_Email_No_Video_Link</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Updates CM First Conversation Date and Sends Email to Client if Litigation is not PI or Premises Liability</description>
        <formula>AND( 				ISPICKVAL(Handling_Firm__c,&quot;Morgan &amp; Morgan&quot;), 				NOT(ISPICKVAL(Litigation__c,&quot;Personal Injury&quot;)), 				NOT(ISPICKVAL(Litigation__c,&quot;Premises Liability&quot;)), 				Last_Disposition__c=&quot;First CM Conversation&quot;, 				OR( 								CONTAINS($UserRole.Name,&quot;CCC Case Processor&quot;), 								CONTAINS($UserRole.Name,&quot;Case Manager&quot;), 								CONTAINS($UserRole.Name,&quot;Assistant&quot;), 								CONTAINS($UserRole.Name,&quot;Pre-Lit&quot;), 								CONTAINS($UserRole.Name,&quot;Paralegal&quot;) 				), 				ISBLANK(First_Staff_Conversation_Date__c), 				NOT( 								Includes(Type_of_Injury__c,&quot;Death&quot;) 				), 				ISBLANK(Deceased_Date__c), 				NOT( 								ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;) 				), 				NOT( 								ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;) 				), 				NOT(Catastrophic__c), 				NOT(ISBLANK(Case_Manager_Name__c) 				) 			)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First CM Conversation - PI %26 Prem</fullName>
        <actions>
            <name>Email_Client_First_Staff_Conversation_Follow_Up_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_First_Staff_Conversation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Client_Welcome_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Updates CM First Conversation Date and Sends Email to Client if Litigation is PI or Premises Liability</description>
        <formula>AND(
ISPICKVAL(Handling_Firm__c,&quot;Morgan &amp; Morgan&quot;),
OR(
ISPICKVAL(Litigation__c,&quot;Personal Injury&quot;), 								ISPICKVAL(Litigation__c,&quot;Premises Liability&quot;)							 				),
Last_Disposition__c=&quot;First CM Conversation&quot;,
OR(
CONTAINS($UserRole.Name,&quot;CCC Case Processor&quot;),
CONTAINS($UserRole.Name,&quot;Case Manager&quot;),
CONTAINS($UserRole.Name,&quot;Assistant&quot;),
CONTAINS($UserRole.Name,&quot;Pre-Lit&quot;),
CONTAINS($UserRole.Name,&quot;Paralegal&quot;)
),
ISBLANK(First_Staff_Conversation_Date__c),
NOT(Includes(Type_of_Injury__c,&quot;Death&quot;)),
ISBLANK(Deceased_Date__c),
NOT(ISPICKVAL(Can_IP_sign__c,&quot;No - Deceased&quot;)),
NOT(ISPICKVAL(Case_Type__c,&quot;Rape &amp; Molestation&quot;)),
NOT(Catastrophic__c),
NOT(ISBLANK(Case_Manager_Name__c)
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First Staff Attempt Date</fullName>
        <actions>
            <name>Set_First_Staff_Attempt_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates First Staff Attempt Date</description>
        <formula>AND(
        ISCHANGED(Last_Disposition__c), 
        OR(
                CONTAINS($UserRole.Name,&quot;CCC Case Processor&quot;),
                CONTAINS($UserRole.Name,&quot;Case Manager&quot;),
                CONTAINS($UserRole.Name,&quot;Assistant&quot;),
                CONTAINS($UserRole.Name,&quot;Pre-Lit&quot;),
                CONTAINS($UserRole.Name,&quot;Paralegal&quot;),
                CONTAINS($UserRole.Name,&quot;CCC SME&quot;),
                CONTAINS($UserRole.Name,&quot;Attorney&quot;)
        ),
        ISBLANK(First_Staff_Attempt_Date__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>General Drug%2FDevice Update Status to Turn Down</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Update status to TD if status = Under Review for more than 14 days.
General Drug
General Device</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_Turn_Down</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_TD_Reason_Attorney_not_interested</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Turn_Down_Date_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Status_Detail_Null</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.Under_Review_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Intake - Affiliate Maketing Field Updates - Social Security</fullName>
        <actions>
            <name>Update_Record_Type_Social_Security</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <description>When created, set record type to Social Security if Litigation = Social Security &amp;&amp; Status Detail Needs Qualification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Intake - Correct Contact Date Update</fullName>
        <actions>
            <name>Update_Last_Correct_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Last_Disposition__c</field>
            <operation>equals</operation>
            <value>Correct Contact,Spoke to PC</value>
        </criteriaItems>
        <description>Update Last Correct Contact date if disposition = correct contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Intake - Lead Qualified Longer Than 30 Days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Qualified_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Qualified_Expired_Rescreen_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.Qualified_Date_Time__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Intake - SUIP Qualified Longer Than 30 Days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Esign Sent,Investigation Scheduled,SMS Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Sign_Up_In_Progress_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Qualified_Expired_Rescreen_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.Sign_Up_In_Progress_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Intake - Set LAbor to Labor Record Type</fullName>
        <actions>
            <name>Update_Record_Type_to_Labor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <description>When created, set record type to Labor</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intake - Suppress from DICE - Attorney Portal</fullName>
        <actions>
            <name>If_Retainer_Sent_is_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Retainer_Received_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SUIP_date_set_if_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Signed_Up_By_Attorney_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Disposition_DNC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Retainer_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Suppress_from_DICE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>Internal Attorney</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>Attorney Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Lead_Details__c</field>
            <operation>contains</operation>
            <value>Do not call - already signed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs to be Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Intake - Update Client Email</fullName>
        <actions>
            <name>Intake_Update_Client_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 				Client__r.PersonContact.Email&lt;&gt;Client_Email_txt__c, 				Client__r.PersonContact.Email&lt;&gt;&quot;none@none.com&quot; 				)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Intake - Update TD Call Complete</fullName>
        <actions>
            <name>Update_TD_Attorney_Review_Call_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the status detail to TD by Attorney Review - TD Call Complete</description>
        <formula>AND( ISCHANGED ( Number_Outbound_Dials_Status__c ),
				Number_Outbound_Dials_Status__c &gt; 0,
    ISPICKVAL(Status__c, &quot;Turn Down&quot;), 
				ISPICKVAL(Status_Detail__c, &quot;TD by Attorney Review - TD Call Pending&quot; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intake Client First Last Name Field Update</fullName>
        <actions>
            <name>Client_First_Last_Name_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Client_First_Last_Name__c )   || ( Client_First_Last_Name__c  &lt;&gt;  ( Client__r.FirstName &amp; &quot; &quot; &amp; Client__r.LastName ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Intake Receive Date</fullName>
        <actions>
            <name>Update_Intake_Receive_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Intake_Received_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Migration_Source__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Intake Status On Hold Workflow</fullName>
        <actions>
            <name>Update_On_Hold_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>CCC</value>
        </criteriaItems>
        <description>Updates dates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Intake Under Review Date</fullName>
        <actions>
            <name>Set_UR_Exit_Date_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Under_Review_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets under review date/time to Now when status == Under Review</description>
        <formula>ISPICKVAL(Status__c, &apos;Under Review&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Labor - Attorney Approval</fullName>
        <actions>
            <name>Email_Notification_for_Approval_Under_Review</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Sent for Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>Nationwide,Out of Area,Out of Country</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send notification to attorney for review and update date fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Labor 30 day Turn Down</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Sent for Approval,Atty - TD</value>
        </criteriaItems>
        <description>If Under Review for more than 30 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_Turn_Down</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_TD_Reason_Attorney_not_interested</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Turn_Down_Date_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.Under_Review_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Labor Turn Down</fullName>
        <actions>
            <name>Review_Complete_TD_Call_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down,Turn Down Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>,none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>TD by Client Decision - TD Complete,TD by Agent - Automatic TD,TD by Criteria - TD Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Labor Turn Down rule after status has changed from Under Review, and client does not have an email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Labor Turn Down with Email</fullName>
        <actions>
            <name>Update_Status_Detail_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Labor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down,Turn Down Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>TD by Client Decision - TD Complete,TD by Agent - Automatic TD,TD by Criteria - TD Complete</value>
        </criteriaItems>
        <description>Labor Turn Down rule after status has changed from Under Review.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Labor_TD_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>TD_by_Attorney_Review_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Sent_TD_Email_to_Client</name>
                <type>Task</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Last Status Update Date</fullName>
        <actions>
            <name>Last_Status_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_OB_Calls_Status_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Records the date when Status is changed</description>
        <formula>ISCHANGED(Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MT - Default Caller Is Injured Party to Yes</fullName>
        <actions>
            <name>Webform_default_caller_is_injured_party</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>contains</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <description>***This rule must be turned off prior to any mass update to Created By field.***

When a new web lead comes into system, the Is caller the injured party field should = Yes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MT - Dicamba Welcome Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Dicamba</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>Needs Mail,Mail Sent</value>
        </criteriaItems>
        <description>Workflow email alert - sent to all potential clients 2 hours after DocuSign email has been sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Dicamba_Attorney_Welcome_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>MT_Dicamba_Attorney_Welcome_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MT - General Drug%2FDevice - TD Call Pending</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>General Device,General Drug</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Turn_Down_Reason__c</field>
            <operation>equals</operation>
            <value>Attorney not interested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>TD by Client Decision - TD Complete,TD by Agent - Automatic TD,TD by Criteria - TD Complete</value>
        </criteriaItems>
        <description>Mass Tort Turn Down rule after status has changed from Under Review, and client does not have an email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Review_Complete_TD_Call_Pending</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MT - General Drug%2FDevice - Turn Down Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>General Device,General Drug</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Turn_Down_Reason__c</field>
            <operation>equals</operation>
            <value>Attorney not interested</value>
        </criteriaItems>
        <description>Sends a TD email to General Drug/Device potential clients who have an email address on file, and the attorney is not interested.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Turn_Down_Email_General_Drug_Device</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Sent_TD_Email_to_Client</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MT - Monat Welcome Email</fullName>
        <actions>
            <name>MT_Monat_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MT_Monat_Welcome_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>General Class Action</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.What_type_of_Class_Action_case_is_it__c</field>
            <operation>equals</operation>
            <value>Monat Beauty Products</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Class Member</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MT - Wildfire Welcome Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>NoCal Wildfire</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received</value>
        </criteriaItems>
        <description>Automated emails sent to new clients two hours after their retainer is received.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MT_Wildfire_Welcome_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Wildfire_Welcome_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MT - Zostavax Questionnaire Complete</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Zostavax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>RR- Questionnaire completed</value>
        </criteriaItems>
        <description>MT - Zostavax Email to Case Staff notifying them questionnaire is complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Zostavax_Questionnaire_Complete_to_Case_Staff</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MT - Zostavax Welcome Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Zostavax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Retainer_Received__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <description>MT - Zostavax Attorney Welcome Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Zostavax_Attorney_Welcome_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>MT_Zostavax_Attorney_Welcome_Email</name>
                <type>Task</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Mailout CIP Retainer Agreement</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Esign Sent</value>
        </criteriaItems>
        <description>Assigns a task to Francis Coppola, to send a mailout if client has not signed the Docusign document.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Mailout_CIP_retainer_agreement</name>
                <type>Task</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Marketing Source Changed</fullName>
        <actions>
            <name>Marketing_Source_Changed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Records an activity whenever the marketing source is changed, and the status is not Lead.</description>
        <formula>AND(  ISCHANGED(Marketing_Source__c), NOT(ISPICKVAL(Status__c,&quot;Lead&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mass Tort Update Status to Turn Down</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Under_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Update status for Mass Tort litigation cases to TD if status = Under Review for more than 14 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_Turn_Down</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_TD_Reason_Attorney_not_interested</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Turn_Down_Date_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Status_Detail_Null</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.Under_Review_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MesoAlert</fullName>
        <actions>
            <name>MesoAlert</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>MesoAlert webhook to Zapier</description>
        <formula>OR( 	AND( 		ISCHANGED(Case_Type__c), 		ISPICKVAL(Case_Type__c, &quot;Asbestos&quot;), 		PRIORVALUE(Case_Type__c) &lt;&gt; &quot;Asbestos&quot; 		), 	AND(ISPICKVAL(Case_Type__c, &quot;Asbestos&quot;), 		ISCHANGED(Status__c) 		) 	)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Non Litify Referrals Notification</fullName>
        <actions>
            <name>Class_Action_Referral_Notification_to_Dov</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Referred_Out_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Notify Dov of:

CA.com 
classaction.com 
ClassAction 911 Leads 
ClassAction FL Sinkhole Leads 
ClassAction TCPA Leads 
ClassAction Wage &amp; Hour Leads 
ClassAction Xarelto Leads</description>
        <formula>ISPICKVAL(Status__c, &quot;Referred Out&quot;) &amp;&amp;   NOT(ISPICKVAL(Case_Type__c,&quot;Terrorism Victim Compensation&quot;)) &amp;&amp; OR(ISPICKVAL(Marketing_Source__c,&quot;BenCrump.com&quot;), ISPICKVAL(Marketing_Source__c,&quot;CA.com&quot;),   CONTAINS(UPPER(TEXT(Marketing_Source__c)),&quot;CLASSACTION&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Non Litify Referrals Set Referred Date</fullName>
        <actions>
            <name>Set_Referred_Out_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Clergy Abuse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <description>Case Type = Clergy Abuse</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Mark Nation of Case Convert</fullName>
        <actions>
            <name>Email_Mark_Nation_on_Case_Convert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Alerts_Person_Assigned_to_Case</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Notifies Mark Nation when converted</description>
        <formula>Assigned_Attorney__r.Email &lt;&gt; &quot;steve@knechtlaws.com&quot; &amp;&amp; ISPICKVAL(Case_Type__c,&quot;Cast Iron Pipes&quot;) &amp;&amp; ISPICKVAL(Status__c,&quot;Converted&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PICOM - Auto Update Referring Office</fullName>
        <actions>
            <name>Update_Referring_Office_PICOM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>PersonalInjury.com</value>
        </criteriaItems>
        <description>If Marketing Source = personalinjury.com, update Referring Office to personalinjury.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Patch status %26 status detail web leads</fullName>
        <actions>
            <name>Update_Status_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Rape &amp; Molestation,Tasigna,Terrorism Victim Compensation,TDF,CP4 Bosch Fuel Pump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <description>patches for API</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Qualtrics - CCC Turn Down Survey</fullName>
        <active>true</active>
        <formula>AND(
ISBLANK(TEXT( Under_Review_Reason__c )), 
TEXT( Status__c) = &quot;Turn Down&quot;,
NOT(ISPICKVAL( Status_Detail__c , &quot;TD by Attorney Review - TD Call Pending&quot;)),
ISPICKVAL( Handling_Firm__c , &quot;Morgan &amp; Morgan&quot;), 
NOT(ISPICKVAL( Case_Type__c , &quot;PIP&quot;)),
 Client__r.PersonContact.Email &lt;&gt; &quot;&quot;,
 Client__r.PersonContact.Email &lt;&gt; &quot;none@none.com&quot;,
 Client__r.PersonContact.HasOptedOutOfEmail = FALSE,
 Client__r.SMS_Text_Automated_Communication_OptOut__c = FALSE
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CCC_Turn_Down_Survey</name>
                <type>OutboundMessage</type>
            </actions>
            <actions>
                <name>CCC_Turn_Down_Survey_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Intake__c.Turn_Down_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Referral Email to Simon %26 Simon - PA</fullName>
        <actions>
            <name>Send_Intake_Details</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Intake__c.State_of_incident__c</field>
            <operation>equals</operation>
            <value>PA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Personal Injury,Premises Liability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Asbestos</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>notEqual</operation>
            <value>Internal Attorney</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.County_of_incident__c</field>
            <operation>notEqual</operation>
            <value>Bucks</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Morgan &amp; Morgan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Retainer Receive Field</fullName>
        <actions>
            <name>If_Retainer_Sent_is_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Retainer_Received_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SUIP_date_set_if_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received,Converted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Retainer Sent Field</fullName>
        <actions>
            <name>Last_Correct_Contact_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Retainer_Sent_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 and 3) or (1 and 2)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>New Application - Mail Sent,Open Claim - Mail Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>notEqual</operation>
            <value>Social Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS Notify Mail Out App Received</fullName>
        <actions>
            <name>Email_Mail_Out_Agent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Application Completed</value>
        </criteriaItems>
        <description>Email to Mailout rep when application is received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SS Notify Mail Out Intake updated from Retainer Received</fullName>
        <actions>
            <name>Email_Mail_Out_Agent_Status_Changed_from_Retainer_Received</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email to Mailout rep when status changed from Retainer Received, but not converted</description>
        <formula>ISPICKVAL(Litigation__c, &quot;Social Security&quot;) &amp;&amp; ISCHANGED(Status__c) &amp;&amp; TEXT(PRIORVALUE(Status__c)) = &quot;Retainer Received&quot; &amp;&amp; NOT(ISPICKVAL(Status__c,&quot;Converted&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send JUUL Email on Retainer Received</fullName>
        <actions>
            <name>Agent_Send_JUUL_Questionnaire_from_Intake</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>JUUL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to Litify - MA</fullName>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4) AND (5 OR ( 6 and 7))</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>MA - Boston</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Premises Liability,Personal Injury,Medical Malpractice,Nursing Home,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Maritime/Admiralty</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Ben Crump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Ben Crump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Jail,Police Brutality,Wrongful Imprisonment</value>
        </criteriaItems>
        <description>Send all MA and NJ cases to Litify when status = UR.    Only IF:

Personal Injury(except Maritime/ Admiralty) 
Medical Malpractice 
Insurance Dispute Removed Per Marvin request
Nursing Home 
Workers Compensation
or Ben Crump etc</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to Litify - NJ</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4) AND (5 OR ( 6 and 7))</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>NJ - New Jersey</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Medical Malpractice,Nursing Home,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Maritime/Admiralty</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Ben Crump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
            <value>Ben Crump</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Jail,Police Brutality,Wrongful Imprisonment</value>
        </criteriaItems>
        <description>Send all MA and NJ cases to Litify when status = UR.    Only IF:

Personal Injury(except Maritime/ Admiralty) 
Medical Malpractice 
Insurance Dispute Removed Per Marvin request
Nursing Home 
Workers Compensation
or Ben Crump etc</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send to Litify - PA</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Venue__c</field>
            <operation>equals</operation>
            <value>PA - Pennsylvania</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Personal Injury,Medical Malpractice,Nursing Home,Workers Compensation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Maritime/Admiralty</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>Automobile Accident</value>
        </criteriaItems>
        <description>Send all PA (Except Auto) cases to Litify when status = UR.    Only IF:

Personal Injury(except Maritime/ Admiralty) 
Medical Malpractice 
Insurance Dispute  Removed Per Marvin L
Nursing Home 
Workers Compensation
or Ben Crump etc</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set All New Leads - Needs to be qualified</fullName>
        <actions>
            <name>Update_Status_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Contact ID on Intake</fullName>
        <actions>
            <name>Set_Contact_ID_on_Intake</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Input Channel - Hubspot</fullName>
        <actions>
            <name>Set_Input_Channel_Hubspot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.CreatedById</field>
            <operation>contains</operation>
            <value>hubspot</value>
        </criteriaItems>
        <description>Sets to hubspot when created by = hubspot</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Input Channel - infra-intakes</fullName>
        <actions>
            <name>Set_Input_Channel_infra_intakes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.CreatedById</field>
            <operation>contains</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <description>Sets to infra-intakes when created by = infra-intakes</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Marketing Sub source</fullName>
        <actions>
            <name>Update_Marketing_sub_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Cisco_Queue__c</field>
            <operation>equals</operation>
            <value>5764</value>
        </criteriaItems>
        <description>If the cisco queue equal 5764 set the marketing sub source to injury.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Morgan %26 Morgan Handling Firm Based On Role</fullName>
        <actions>
            <name>Set_Morgan_Morgan_Handling_Firm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) and 3</booleanFilter>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Mass Tort,GVW,Labor/TCPA Intake Specialist</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>notEqual</operation>
            <value>,None,Nation Law,nationlaw.com,The Nation Law Firm</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>MT,  Labor, TCPA, GVW will all default with MM</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set No 24 Hour Attempt</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Staff_Assigned_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.First_Staff_Attempt_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.X24_Hour_Call_Expiration__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set No 24 Hour Attempt 24 hours after trigger</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_No_24_Hour_Attempt</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.X24_Hour_Call_Expiration__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set No 24 Hour Convo</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Staff_Assigned_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.First_Staff_Conversation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.X24_Hour_Call_Expiration__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set No 24 Hour Convo 24 hours after trigger</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_No_24_Hour_Convo</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Intake__c.X24_Hour_Call_Expiration__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Pipes to Classaction%2Ecom marketing source</fullName>
        <actions>
            <name>Update_Marketing_Source_to_Classaction</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) and 5</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Nation Law</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>notEqual</operation>
            <value>Firm Employee,External Attorney,Internal Attorney</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>Direct Entry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.State_of_incident__c</field>
            <operation>equals</operation>
            <value>FL</value>
        </criteriaItems>
        <description>Set all cast iron pipes to classaction.com marketing source</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Qualified Date%2FTime</fullName>
        <actions>
            <name>Set_Qualified_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Qualified_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Qualified,Attorney Approved For Signup</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress,Retainer Sent,Retainer Received,Converted</value>
        </criteriaItems>
        <description>Qualified Date/Time = Now</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Qualified Date%2FTime to Null</fullName>
        <actions>
            <name>Set_Qualified_Date_Time_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Qualified_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Qualified Date/Time = Null, if the status changes to Under Review and the case is no longer Qualified.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Referred Out Date</fullName>
        <actions>
            <name>Set_Referred_Out_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Criminal</value>
        </criteriaItems>
        <description>Sets date/time with NOW() - Criminal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Referring Firm Text Field</fullName>
        <active>false</active>
        <description>Set referring firm text for reports</description>
        <formula>NOT(ISBLANK(Referring_Firm_Attorney__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Retainer Sent Date</fullName>
        <actions>
            <name>Retainer_Sent_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CCC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Mail Sent,Esign Sent,Esign</value>
        </criteriaItems>
        <description>Sets the retainer sent date when an esign or mailout are sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set SUIP Date</fullName>
        <actions>
            <name>Set_SUIP_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress,Retainer Sent</value>
        </criteriaItems>
        <description>Sets Sign Up In Progress Date to NOW() when status == Sign Up In Progress OR Retainer Sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Staff Assigned Date</fullName>
        <actions>
            <name>Set_Staff_Assigned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Staff Assigned Date when either Assigned Atty or CM is set</description>
        <formula>OR( NOT(ISBLANK(Case_Manager_Name__c)), NOT(ISBLANK(Assigned_Attorney__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Referred Out</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4) or (1 and 2 and 5)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Terrorism Victim Compensation,TDF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>classaction.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>HubSpot</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>notEqual</operation>
            <value>HubSpot</value>
        </criteriaItems>
        <description>Auto set status to referred out for Terrorism victim comp and TDF</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Task to Mailout Retainer Agreement</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Cast Iron Pipes,Blasting,Roundup</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Esign Sent</value>
        </criteriaItems>
        <description>Assigns a task to Francis Coppola, to send a mailout if client has not signed the Docusign document.
CIP, Cast Iron Pipes, Blasting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Mailout_CIP_retainer_agreement</name>
                <type>Task</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Under Review Exit Date</fullName>
        <actions>
            <name>Update_Under_Review_Exit_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets exit date/time to NOW()</description>
        <formula>ISCHANGED(Status__c) &amp;&amp; TEXT(PRIORVALUE(Status__c)) == &quot;Under Review&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SocSec Updated when Status Detail is Needs Mail</fullName>
        <actions>
            <name>Set_MailOut_Agent_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Mail_Sent_Date_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Retainer_Received_date_to_Nul</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Retainer_Sent_Date_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs Mail,New Application - Needs Mail,Open Claim - Needs Mail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Status Detail = Needs Mail, and Litigation = Social Security. 
Mail Out Agent Name, Mail Sent Date, Retainer Sent date, and Retainer Received date all set the Null</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - Attorney Approval</fullName>
        <actions>
            <name>Social_Security_Approval_to_Attorney</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Intake_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Send notification to attorney for review and update date fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - Attorney Approved Date</fullName>
        <actions>
            <name>Update_Attorney_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>contains</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Send notification to Intake to notify if approved/rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - FCQ Status</fullName>
        <actions>
            <name>Social_Security_FCQ_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_FCQ_Date_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>contains</operation>
            <value>FCQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Send notification to Intake that FCQ status detail has been set and update FCQ date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - Intake Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Attorney - Approved,Turn Down,On Hold</value>
        </criteriaItems>
        <description>Send notification to Intake to notify if approved/rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - Internal Generating Lead Notification</fullName>
        <actions>
            <name>Social_Security_Internal_Generating_Lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Social_Security_Internal_Generating_Lead</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Source__c</field>
            <operation>equals</operation>
            <value>Internal Attorney</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs to be Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Notification to inform that an Internal Generating Lead has been submitted via the Attorney Lead Generating Portal.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - No Contact %E2%80%93 Needs Mail</fullName>
        <actions>
            <name>SS_No_Contact_Needs_Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SS_No_Contact_Turn_Down</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Turn_Down_Date_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>No_Contact_Needs_Mail</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Needs Application Filed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Number_Outbound_Dials_Status__c</field>
            <operation>greaterOrEqual</operation>
            <value>9</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Workflow to change the status of an intake to: Turndown – No Contact – Needs Mail, when no contact with the client has been established after 9 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - Update Mail Sent Date</fullName>
        <actions>
            <name>Social_Security_Update_Mail_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Mail Sent,New Application - Mail Sent,Open Claim - Mail Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Updates Mail Sent Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Security - Update Retainer Sent Date</fullName>
        <actions>
            <name>Social_Security_Retainer_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>New Application - Mail Sent,Open Claim - Mail Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Social Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Handling_Firm__c</field>
            <operation>notEqual</operation>
            <value>Disability Care Center</value>
        </criteriaItems>
        <description>Updates Retainer Sent date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tasigna Web Lead Set to Referred Out</fullName>
        <actions>
            <name>Update_Status_Detail_Pending_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Referred_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Tasigna</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Referred Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Marketing_Sub_source__c</field>
            <operation>equals</operation>
            <value>classaction Tasigna</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.InputChannel__c</field>
            <operation>equals</operation>
            <value>infra-intakes</value>
        </criteriaItems>
        <description>Auto set status to referred out for Tasigna</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TestCaptorra</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>notEqual</operation>
            <value>SSI</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Trucking Case Emails</fullName>
        <actions>
            <name>Trucking_Attorney_Alert_Under_Review_Turn_Down</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Did_the_accident_involve_a_truck_or_bus__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Catastrophic__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Sign Up In Progress,Turn Down</value>
        </criteriaItems>
        <description>When a trucking case goes TD or SUIP and email should go to Trucking Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Turn Down Date</fullName>
        <actions>
            <name>Turn_Down_Date_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CQC_Bypass_TD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status__c, &quot;Turn Down&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Turn Down Letter To Be Sent</fullName>
        <actions>
            <name>Turn_Down_Letter_To_Be_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Retainer_Received__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Litigation__c</field>
            <operation>equals</operation>
            <value>Mass Tort</value>
        </criteriaItems>
        <description>Retainer Received and Intake Status is Turn Down</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update  - Deceased IP</fullName>
        <actions>
            <name>Update_IP_Deceased_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Can_IP_sign__c</field>
            <operation>equals</operation>
            <value>No - Deceased</value>
        </criteriaItems>
        <description>IP will be deceased of they choose IP can&apos;t sign because deceased</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Client Email on Intake When Saved</fullName>
        <actions>
            <name>Update_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates client email text field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Marketing Sub-source from Cisco Queue</fullName>
        <actions>
            <name>Set_Marketing_Sub_source_to_Injury_com</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Cisco_Queue__c</field>
            <operation>equals</operation>
            <value>5764</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Phone on Intake</fullName>
        <actions>
            <name>Update_Phone_on_Intake_from_Client_Accou</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Phone__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If Phone is blank on the Intake it looks to the mobile phone or the home phone and copies it to the intake</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type to CCC for Web Leads</fullName>
        <actions>
            <name>Update_Record_Type_to_CCC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.CreatedByWebform__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Created by Integration Hubspot User - Set to CCC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update status for Bird - Under Review</fullName>
        <actions>
            <name>Bird_Under_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UR_Reason_Bird</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review - Bird</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updated Converted Date</fullName>
        <actions>
            <name>Update_Converted_Date_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Converted_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Converted Date to Today when Status = Converted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VA - Drip - Welcome Email Follow Up</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Package sent - email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Sends a welcome follow-up email to new VA clients who have an email address on file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>VA_TD_Review_File</name>
                <type>Task</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>VA_Welcome_Follow_up_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>VA_F_U_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>VA_30_day_F_U_email_sent</name>
                <type>Task</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VA - Drip - Welcome Package Follow Up</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Package sent - mail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Sends a welcome email to new VA clients who have an email address on file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>VA_TD_Review_File</name>
                <type>Task</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>VA_30_day_Follow_Up_On_Package</name>
                <type>Task</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VA - Package Received - Incomplete Follow-up</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>Package received - incomplete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>VA_TD_Review_File</name>
                <type>Task</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VA - TD%2FReview File</fullName>
        <active>true</active>
        <booleanFilter>(1 AND 2) AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>F/U package sent - email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>F/U package sent - mail</value>
        </criteriaItems>
        <description>Rule sets a task to have the file reviewed if the package was sent but not received after 30 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>VA_TD_Review_File</name>
                <type>Task</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VA - Turndown Email</fullName>
        <actions>
            <name>VA_Turndown_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TD_by_Attorney_Review_Email_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sent_TD_Email_to_Client</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>TD by Attorney Review - TD Call Pending</value>
        </criteriaItems>
        <description>This was deactivated per Marvin  JIRA Ticket EP-1296   Sends a TD email to VA clients who have an email address on file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VA - Turndown Letter</fullName>
        <actions>
            <name>VA_Turndown_Letter</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>equals</operation>
            <value>TD by Attorney Review - TD Call Pending</value>
        </criteriaItems>
        <description>Sets a task to send a TD letter to the client.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VA - Welcome Email</fullName>
        <actions>
            <name>VA_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>VA_Package_sent_email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_VA_Client_Welcome_Email</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Sends a welcome email to new VA clients who have an email address on file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VA - Welcome Packet</fullName>
        <actions>
            <name>VA_Mail_Package</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Veteran Disability</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Sets a task to send a packet for new VA clients who don&apos;t have an email address on file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Wage %26 Hour - TD Call Pending</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Wage &amp; Hour</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Turn_Down_Reason__c</field>
            <operation>equals</operation>
            <value>Attorney not interested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>TD by Client Decision - TD Complete,TD by Agent - Automatic TD,TD by Criteria - TD Complete</value>
        </criteriaItems>
        <description>Labor Turn Down rule after status has changed from Under Review, and client does not have an email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Review_Complete_TD_Call_Pending</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Wage %26 Hour - Turn Down Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Wage &amp; Hour</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>notEqual</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Turn Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Turn_Down_Reason__c</field>
            <operation>equals</operation>
            <value>Attorney not interested</value>
        </criteriaItems>
        <description>Sends a TD email to Wage &amp; Hour potential clients who have an email address on file, and the attorney is not interested.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Wage_Hour_TD_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Sent_TD_Email_to_Client</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Web Form Auto-Responder - CA</fullName>
        <actions>
            <name>Web_Form_Submission_Auto_Responder_CA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Auto email sent to client - CA</description>
        <formula>ISNEW() &amp;&amp; NOT(ISPICKVAL(Status__c,&quot;Turn Down&quot;)) &amp;&amp; ISPICKVAL(Marketing_Source__c,&quot;classaction.com&quot;) &amp;&amp; ISPICKVAL(InputChannel__c,&quot;infra-intakes&quot;) &amp;&amp; NOT(ISBLANK(Client_Email_txt__c)) &amp;&amp; NOT(ISPICKVAL(Marketing_Sub_source__c,&quot;injury.com&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Web Form Auto-Responder - FTP</fullName>
        <actions>
            <name>Web_Form_Submission_Auto_Responder_FTP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Auto email sent to client - FTP</description>
        <formula>ISNEW() &amp;&amp; NOT(ISPICKVAL(Status__c,&quot;Turn Down&quot;)) &amp;&amp; ISPICKVAL(Marketing_Source__c,&quot;ForThePeople-com-INTERNET&quot;) &amp;&amp; ISPICKVAL(InputChannel__c,&quot;infra-intakes&quot;) &amp;&amp; NOT(ISPICKVAL(Marketing_Sub_source__c,&quot;Hurricanelawyer.com&quot;)) &amp;&amp; NOT(ISBLANK(Client_Email_txt__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Zostavax Welcome Packet</fullName>
        <actions>
            <name>Send_Zostavax_Welcome_Packet</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Intake__c.Case_Type__c</field>
            <operation>equals</operation>
            <value>Zostavax</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status__c</field>
            <operation>equals</operation>
            <value>Retainer Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Status_Detail__c</field>
            <operation>notEqual</operation>
            <value>Esign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Intake__c.Client_Email_txt__c</field>
            <operation>equals</operation>
            <value>none@none.com</value>
        </criteriaItems>
        <description>A task that instructs me to Drawloop and mail a welcome package to all Zostavax clients that return a physical retainer packet completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>CCC_Catastrophic_Case_Under_Review_Turned_Down</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Email - Case Under Review/Turned Down</subject>
    </tasks>
    <tasks>
        <fullName>CCC_Email_Catastrophic_Case_Scheduled_for_Sign_Up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Email - Case Scheduled for Sign Up</subject>
    </tasks>
    <tasks>
        <fullName>CCC_Email_New_Case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Email - New Case</subject>
    </tasks>
    <tasks>
        <fullName>CCC_Email_Ultra_Cat</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email sent to Ultra Team</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Email - Ultra Cat</subject>
    </tasks>
    <tasks>
        <fullName>CCC_Turn_Down_Survey_Sent</fullName>
        <assignedTo>integrationuser@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Turn Down Survey Sent to the Client.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CCC Turn Down Survey Sent</subject>
    </tasks>
    <tasks>
        <fullName>Cast_Iron_Pipes_Auto_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Cast Iron Pipes Auto Email Sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Cast Iron Pipes Auto Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Email_1_Hour_Retainer_Received</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: 1 Hour Retainer Received</subject>
    </tasks>
    <tasks>
        <fullName>Email_1_Hour_Retainer_Received_with_Link</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email with questionnaire link sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: 1 Hour Retainer Received - with Link</subject>
    </tasks>
    <tasks>
        <fullName>Email_1_Hour_Retainer_Received_with_Link_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: 1 Hour Retainer Received - with Link (Follow Up)</subject>
    </tasks>
    <tasks>
        <fullName>Email_1_Hour_TD_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sent email to TD</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: 1 Hour TD Email</subject>
    </tasks>
    <tasks>
        <fullName>Email_4_Hour_Retainer_Received</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sent 4 hour RR email</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: 4 Hour Retainer Received</subject>
    </tasks>
    <tasks>
        <fullName>Email_4_Hour_Retainer_Received_with_Link</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: 4 Hour Retainer Received - with Link</subject>
    </tasks>
    <tasks>
        <fullName>Email_Alerts_Case_Status_Various_Roles</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Alerts - Case Status - Various Roles</subject>
    </tasks>
    <tasks>
        <fullName>Email_Alerts_Person_Assigned_to_Case</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a Task being created to document the system sent out an email</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Alerts - Person Assigned to Case</subject>
    </tasks>
    <tasks>
        <fullName>Email_Client_Turn_Down_Notification</fullName>
        <assignedToType>owner</assignedToType>
        <description>TD email sent to client</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: Client Turn Down Notification</subject>
    </tasks>
    <tasks>
        <fullName>Email_Client_Welcome_Email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: Client Welcome Email</subject>
    </tasks>
    <tasks>
        <fullName>Email_Client_Welcome_Email_Atty</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: Client Welcome Email - Atty</subject>
    </tasks>
    <tasks>
        <fullName>Email_Client_Welcome_Email_Atty_No_Treatment</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: Client Welcome Email - Atty No Treatment</subject>
    </tasks>
    <tasks>
        <fullName>Email_Client_Welcome_Email_No_Treatment</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: Client Welcome Email - No Treatment</subject>
    </tasks>
    <tasks>
        <fullName>Email_New_Case_Retainer_Receive</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: New Case Retainer Receive</subject>
    </tasks>
    <tasks>
        <fullName>Email_VA_Client_Welcome_Email</fullName>
        <assignedTo>VA_Intake_Team</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: VA Client Welcome Email</subject>
    </tasks>
    <tasks>
        <fullName>FCRA_How_To_Dispute_Inaccuracies_Packet</fullName>
        <assignedTo>fcoppola@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please send the FCRA How To Dispute Inaccuracies Packet to the client.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>FCRA - How To Dispute Inaccuracies Packet</subject>
    </tasks>
    <tasks>
        <fullName>FCRA_How_To_Get_Credit_Report_Packet</fullName>
        <assignedTo>fcoppola@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please send the FCRA - How To Get Credit Report Packet</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>FCRA - How To Get Credit Report Packet</subject>
    </tasks>
    <tasks>
        <fullName>FCRA_How_to_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>FCRA How-to Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Generating_Atty_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email sent to attorney on status change</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Generating Atty Email - Status Change</subject>
    </tasks>
    <tasks>
        <fullName>MT_Dicamba_Attorney_Welcome_Email</fullName>
        <assignedTo>rrocha@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Welcome email sent on behalf of attorney, 2 hours after retainer is sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>MT - Dicamba Attorney Welcome Email</subject>
    </tasks>
    <tasks>
        <fullName>MT_Monat_Welcome_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Monat Welcome Email Sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>MT - Monat Welcome Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>MT_Zostavax_Attorney_Welcome_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Welcome email sent on behalf of attorney, 2 hours after retainer is sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>MT - Zostavax Attorney Welcome Email</subject>
    </tasks>
    <tasks>
        <fullName>Mailout_CIP_retainer_agreement</fullName>
        <assignedTo>fcoppola@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Mailout Retainer Agreement</subject>
    </tasks>
    <tasks>
        <fullName>Marketing_Source_Changed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Marketing Source is changed, and the status is not Lead.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Marketing Source Changed</subject>
    </tasks>
    <tasks>
        <fullName>No_Contact_Needs_Mail</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>No Contact – Needs Mail</subject>
    </tasks>
    <tasks>
        <fullName>Send_Zostavax_Welcome_Packet</fullName>
        <assignedTo>pborrero@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Zostavax Welcome Packet</subject>
    </tasks>
    <tasks>
        <fullName>Sent_30_Day_Turn_Down_Email_to_Clietn</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Sent 30 Day Turn Down Email to Clietn</subject>
    </tasks>
    <tasks>
        <fullName>Sent_TD_Email_to_Client</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Sent TD Email to Client</subject>
    </tasks>
    <tasks>
        <fullName>Social_Security_Internal_Generating_Lead</fullName>
        <assignedTo>CCC_Supervisor</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Social Security - Internal Generating Lead</subject>
    </tasks>
    <tasks>
        <fullName>Turn_Down_Letter_To_Be_Sent</fullName>
        <assignedTo>fcoppola@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please send turn down letter to this client via certified USPS mail with return receipt. Then upload certified slip to the intake page.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Intake__c.Turn_Down_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Turn Down Letter To Be Sent</subject>
    </tasks>
    <tasks>
        <fullName>VA_30_day_F_U_email_sent</fullName>
        <assignedTo>VA_Intake_Team</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>VA - 30-day F/U email sent</subject>
    </tasks>
    <tasks>
        <fullName>VA_30_day_Follow_Up_On_Package</fullName>
        <assignedTo>VA_Intake_Team</assignedTo>
        <assignedToType>role</assignedToType>
        <description>It&apos;s been 30 days since the last status update. Please send the follow-up welcome packet to the client.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>VA - 30-day Follow-Up On Package</subject>
    </tasks>
    <tasks>
        <fullName>VA_Mail_Package</fullName>
        <assignedTo>VA_Intake_Team</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Client does not have an email address. Please mail welcome package to client.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>VA - Mail Package</subject>
    </tasks>
    <tasks>
        <fullName>VA_TD_Review_File</fullName>
        <assignedTo>VA_Intake_Team</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Please review the file.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>VA - TD/Review File</subject>
    </tasks>
    <tasks>
        <fullName>VA_Turndown_Letter</fullName>
        <assignedTo>VA_Intake_Team</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Please mail the client the TD letter.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>VA - Turndown Letter</subject>
    </tasks>
    <tasks>
        <fullName>Welcome_Letter_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Welcome Letter Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Wildfire_Welcome_Email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Wildfire Welcome Email</subject>
    </tasks>
    <tasks>
        <fullName>X10_Day_SUIP_Reminder_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sent 10 day reminder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>10 Day SUIP Reminder Email</subject>
    </tasks>
    <tasks>
        <fullName>X14_Day_SUIP_Reminder_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sent 14 day reminder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>14 Day SUIP Reminder Email</subject>
    </tasks>
    <tasks>
        <fullName>X20_Day_SUIP_Reminder_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sent 20 day reminder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>20 Day SUIP Reminder Email</subject>
    </tasks>
    <tasks>
        <fullName>X5_Day_SUIP_Reminder_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Sent 5 day reminder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>5 Day SUIP Reminder Email</subject>
    </tasks>
    <tasks>
        <fullName>emailtest</fullName>
        <assignedTo>ksadler@forthepeople.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>emailtest</subject>
    </tasks>
</Workflow>
