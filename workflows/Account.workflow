<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Automated_OptOut_to_False</fullName>
        <field>SMS_Text_Automated_Communication_OptOut__c</field>
        <literalValue>0</literalValue>
        <name>Automated OptOut to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Automated_OptOut_to_True</fullName>
        <field>SMS_Text_Automated_Communication_OptOut__c</field>
        <literalValue>1</literalValue>
        <name>Automated OptOut to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Base_Date_Invoice_Date</fullName>
        <field>c2g__CODABaseDate1__c</field>
        <literalValue>Invoice Date</literalValue>
        <name>Base Date = Invoice Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_Country_Update</fullName>
        <field>BillingCountry</field>
        <formula>&quot;US&quot;</formula>
        <name>Billing Country Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Tax_ID_to_Taxpayer_Id_Number</fullName>
        <field>c2g__CODATaxpayerIdentificationNumber__c</field>
        <formula>Tax_ID__c</formula>
        <name>Copy Tax ID to Taxpayer Id Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_Update</fullName>
        <field>Phone</field>
        <formula>PersonMobilePhone</formula>
        <name>Phone Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Account_External_ID</fullName>
        <description>Populates the Account.External_ID__c field with a seed value from the External_ID_Seed__c</description>
        <field>External_ID__c</field>
        <formula>External_ID_Seed__c</formula>
        <name>Populate Account External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS_p_to_False</fullName>
        <field>tdc_tsw__SMS_Opt_out__c</field>
        <literalValue>0</literalValue>
        <name>SMS_p to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS_p_to_True</fullName>
        <field>tdc_tsw__SMS_Opt_out__c</field>
        <literalValue>1</literalValue>
        <name>SMS_p to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS_pc_to_False</fullName>
        <field>tdc_tsw__SMS_Opt_out__pc</field>
        <literalValue>0</literalValue>
        <name>SMS_pc to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS_pc_to_True</fullName>
        <field>tdc_tsw__SMS_Opt_out__pc</field>
        <literalValue>1</literalValue>
        <name>SMS_pc to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contact_ID_on_Account</fullName>
        <description>set personaccount contact ID</description>
        <field>Contact_ID__c</field>
        <formula>CASESAFEID(PersonContactId)</formula>
        <name>Set Contact ID on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Shipping_Country_Update</fullName>
        <field>ShippingCountry</field>
        <formula>&quot;US&quot;</formula>
        <name>Shipping Country Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trading_Currency_USD</fullName>
        <field>c2g__CODAAccountTradingCurrency__c</field>
        <formula>&apos;USD&apos;</formula>
        <name>Trading Currency = USD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_of_Birth_c</fullName>
        <description>Updates Date_of_Birth__c to equal Date_of_Birth_mm__c</description>
        <field>Date_of_Birth__c</field>
        <formula>Date_of_Birth_mm__c</formula>
        <name>Update Date_of_Birth__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Add US as default country</fullName>
        <actions>
            <name>Billing_Country_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Shipping_Country_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automated OptOut False</fullName>
        <actions>
            <name>Automated_OptOut_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.tdc_tsw__SMS_Opt_out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automated OptOut True</fullName>
        <actions>
            <name>Automated_OptOut_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.tdc_tsw__SMS_Opt_out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Accounting Fields</fullName>
        <actions>
            <name>Base_Date_Invoice_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trading_Currency_USD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Determine If Account External ID Needs Populating</fullName>
        <actions>
            <name>Populate_Account_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule determines if the Account.External_ID__c field needs populating when the Account record is created.</description>
        <formula>ISBLANK( External_ID__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SMS Text OptOut False</fullName>
        <actions>
            <name>SMS_p_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SMS_pc_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SMS_Text_Automated_Communication_OptOut__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Person Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SMS Text OptOut True</fullName>
        <actions>
            <name>SMS_p_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SMS_pc_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SMS_Text_Automated_Communication_OptOut__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Person Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Contact ID on Account</fullName>
        <actions>
            <name>Set_Contact_ID_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>set contact ID from person account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sync Date of Birth</fullName>
        <actions>
            <name>Update_Date_of_Birth_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies Date_of_Birth_mm__c to Date_of_Birth__c.</description>
        <formula>Or(isnew(),Ischanged(Date_of_Birth_mm__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tax ID to Taxpayer Identification Number Sync</fullName>
        <actions>
            <name>Copy_Tax_ID_to_Taxpayer_Id_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Tax_ID__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Write Mobile into %22Number%22 Field</fullName>
        <actions>
            <name>Phone_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(Phone) &amp;&amp; NOT(ISBLANK(PersonMobilePhone))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
