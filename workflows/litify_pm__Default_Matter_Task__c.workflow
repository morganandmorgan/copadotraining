<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>litify_pm__Update_Billable_Amount_w_Default_Role_Rt</fullName>
        <field>litify_pm__Estimated_Time_Value__c</field>
        <formula>litify_pm__Matter_Team_Role__r.litify_pm__Default_Billable_Hourly_Wage__c* litify_pm__Estimated_Duration__c</formula>
        <name>Update Billable Amount w Default Role Rt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>litify_pm__Calculate Cost of Time Worked</fullName>
        <actions>
            <name>litify_pm__Update_Billable_Amount_w_Default_Role_Rt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(litify_pm__Estimated_Time_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
