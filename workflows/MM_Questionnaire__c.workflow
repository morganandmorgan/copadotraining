<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Manager_Questionnaire_Notification</fullName>
        <description>Case Manager Questionnaire Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Notification_of_Questionnaire</template>
    </alerts>
    <alerts>
        <fullName>JUUL_Questionnaire_Notification</fullName>
        <ccEmails>juul@forthepeople.com</ccEmails>
        <description>JUUL Questionnaire Notification</description>
        <protected>false</protected>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/JUUL_Questionnaire_Notification</template>
    </alerts>
    <alerts>
        <fullName>RoundUp_Questionnaire_Received_Notification</fullName>
        <ccEmails>roundup@forthepeople.com</ccEmails>
        <description>RoundUp Questionnaire Received Notification</description>
        <protected>false</protected>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Notification_of_Questionnaire</template>
    </alerts>
    <fieldUpdates>
        <fullName>Description_of_Incident</fullName>
        <field>Description_of_Incident__c</field>
        <formula>Intake__r.Description_of_Incident__c</formula>
        <name>Description of Incident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Manager_Email</fullName>
        <field>Case_Manager_Email__c</field>
        <formula>Intake__r.Case_Manager_Name__r.Email</formula>
        <name>Set Case Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Description_of_Incident</fullName>
        <field>Description_of_Injuries__c</field>
        <formula>Intake__r.Description_of_Incident__c</formula>
        <name>Set Description of Incident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email Case Manager that Questionnaire is Complete</fullName>
        <actions>
            <name>Case_Manager_Questionnaire_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email when questionnaire inserted from web</description>
        <formula>NOT(ISBLANK(Case_Manager_Email__c))
&amp;&amp;
ISPICKVAL(Intake__r.Handling_Firm__c,&quot;Morgan &amp; Morgan&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Description of Incident on MM Questionnaire</fullName>
        <actions>
            <name>Description_of_Incident</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Case_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
  ISNEW(),
  OR(
    ISPICKVAL(Intake__r.Litigation__c, &quot;Personal Injury&quot;),
    ISPICKVAL(Intake__r.Litigation__c, &quot;Premises Liability&quot;)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
