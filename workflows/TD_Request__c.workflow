<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_TD_Request_Submitter_Approval_Result_Status</fullName>
        <description>Email Alert to TD Request Submitter - Approval Result Status</description>
        <protected>false</protected>
        <recipients>
            <field>TD_Approving_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/TD_Request_Submit_for_Approval</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_TD_Request_Submitter_Approval_Status</fullName>
        <description>Email Alert to TD Request Submitter - Approval Status</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Paralegal_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>TD_Approving_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/TD_Request_Approval_Result</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_TD_Request_Submitter_Approval_Status_48_Hour_Reminder</fullName>
        <description>Email Alert to TD Request Submitter - Approval Status - 48 Hour Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Paralegal_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>TD_Approving_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/TD_Request_Submit_for_Approval_48_Hour_Reminder</template>
    </alerts>
    <alerts>
        <fullName>TD_Request_Audit</fullName>
        <ccEmails>auditing@forthepeople.com</ccEmails>
        <description>TD Request Audit</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TD_Request_Audit_Template</template>
    </alerts>
    <alerts>
        <fullName>TD_Request_Email_Case_Staff_to_Close_File</fullName>
        <description>TD Request - Email Case Staff to Close File</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Developer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Handling_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Managing_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Paralegal_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/TD_Request_Close_File</template>
    </alerts>
    <alerts>
        <fullName>TD_Request_Email_Case_Staff_to_Obtain_Different_Address_Method_for_Delivery</fullName>
        <description>TD Request - Email Case Staff to Obtain Different Address/Method for Delivery</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Developer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Handling_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Managing_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Paralegal_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/TD_Request_Obtain_Different_Address_Method_for_Delivery</template>
    </alerts>
    <alerts>
        <fullName>TD_Request_Email_Case_Staff_to_Update_CP</fullName>
        <description>TD Request - Email Case Staff to Update CP</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Developer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Handling_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Managing_Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Paralegal_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/TD_Request_Update_CP_Notification</template>
    </alerts>
    <alerts>
        <fullName>TD_Request_Rejected_Geico_UM_TD</fullName>
        <description>TD Request - Rejected Geico UM TD</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmatthews@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Customer_Emails/TD_Request_Rejected_Geico_UM_TD</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Approval_Date</fullName>
        <description>set Approval date of TD request field</description>
        <field>Approval_Date_Time__c</field>
        <formula>now()</formula>
        <name>Set Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Submission</fullName>
        <description>Update status to submitted</description>
        <field>Approval_Status__c</field>
        <literalValue>Pending Attorney Approval</literalValue>
        <name>Set Approval Submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Submission_Date</fullName>
        <description>Set date of approval submission field</description>
        <field>Submitted_Date__c</field>
        <formula>now()</formula>
        <name>Set Approval Submission Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Confirmation_Received_Status_TD_Re</fullName>
        <description>Set Confirmation Received Status - TD Request</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved - Complete (Confirmation Received)</literalValue>
        <name>Set Confirmation Received Status - TD Re</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Description_of_Incident_on_TD_Reques</fullName>
        <field>Description_of_Incident__c</field>
        <formula>Matter__r.Description_of_Incident__c</formula>
        <name>Set Description of Incident on TD Reques</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LCP_Information</fullName>
        <field>LCP_Details__c</field>
        <formula>&quot;No LCP Approved: &quot;&amp;BR()&amp;IF(Matter__r.No_LCP_Approved__c,&quot;True&quot;,&quot;False&quot;)&amp;BR()&amp; 
BR()&amp; 
&quot;No LCP Reason: &quot;&amp;BR()&amp;Matter__r.No_LCP_Reason__c</formula>
        <name>Set LCP Information</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Draft</fullName>
        <description>Sets record type to Draft on the Initial Submission Actions</description>
        <field>RecordTypeId</field>
        <lookupValue>Draft</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Submitted</fullName>
        <description>Sets record type to Submitted on the Initial Submission Actions</description>
        <field>RecordTypeId</field>
        <lookupValue>Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejection_Date</fullName>
        <description>Set Rejection of TD request field</description>
        <field>Rejected_Date_Time__c</field>
        <formula>now()</formula>
        <name>Set Rejection Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_Call_Pending</fullName>
        <description>Set status field to approved-call pending</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved-Call Pending</literalValue>
        <name>Set Status to Approved-Call Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Pending_Attorney_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending Attorney Approval</literalValue>
        <name>Set Status to Pending Attorney Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Pending_Man_Atty_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending Managing Attorney Approval</literalValue>
        <name>Set Status to Pending Man. Atty Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Pending_Team_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending TD Team Approval</literalValue>
        <name>Set Status to Pending Team Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected</fullName>
        <description>Set status field to rejected</description>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_TDR_Status_to_Draft</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set TDR Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_Attorney_Email</fullName>
        <description>Sets email from lawsuit/matter</description>
        <field>Assigned_Attorney_Email__c</field>
        <formula>Matter__r.Principal_Attorney_2__r.Email</formula>
        <name>Update Assigned Attorney Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Atty_Approved_Date</fullName>
        <field>Attorney_Approval_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Atty Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Developer_Email</fullName>
        <description>Sets email from lawsuit/matter</description>
        <field>Case_Developer_Email__c</field>
        <formula>Matter__r.Assistant__r.Email</formula>
        <name>Update Case Developer Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Manager_Email</fullName>
        <description>Sets email from lawsuit/matter</description>
        <field>Case_Manager_Email__c</field>
        <formula>Matter__r.Case_Manager__r.Email</formula>
        <name>Update Case Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Paralegal_Email</fullName>
        <description>Sets email from lawsuit/matter</description>
        <field>Paralegal_Email__c</field>
        <formula>Matter__r.Litigation_Paralegal__r.Email</formula>
        <name>Update Case Paralegal Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Handling_Attorney_Email</fullName>
        <description>Sets email from lawsuit/matter</description>
        <field>Handling_Attorney_Email__c</field>
        <formula>Matter__r.litify_pm__Principal_Attorney__r.Email</formula>
        <name>Update Handling Attorney Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Attorney_Email</fullName>
        <description>Sets email from lawsuit/matter</description>
        <field>Managing_Attorney_Email__c</field>
        <formula>Matter__r.Managing_Attorney__r.Email</formula>
        <name>Update Managing Attorney Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_TD_Status_Confirmation_Not_Recvd</fullName>
        <description>Update TD Request Status to Approved - Complete (Confirmation Not Received)</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved - Complete (Confirmation Not Received)</literalValue>
        <name>Update TD Status Confirmation Not Recvd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Confirmation Not Received Status - TD Request</fullName>
        <actions>
            <name>Update_TD_Status_Confirmation_Not_Recvd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.Tracking_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Email Addresses on TD Request</fullName>
        <actions>
            <name>Update_Assigned_Attorney_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Developer_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Paralegal_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Handling_Attorney_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Managing_Attorney_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets email addresses from Lawsuit</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Fields on TD Request</fullName>
        <actions>
            <name>Set_Description_of_Incident_on_TD_Reques</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_LCP_Information</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets from matter</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TD Request - Confirmation Received</fullName>
        <actions>
            <name>Set_Confirmation_Received_Status_TD_Re</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.Certified_Mailing_Status__c</field>
            <operation>equals</operation>
            <value>Delivered,Refused,Unclaimed</value>
        </criteriaItems>
        <description>Update the TDR status = Confirmation Received.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TD Request - Email Case Staff Call Complete</fullName>
        <actions>
            <name>TD_Request_Email_Case_Staff_to_Update_CP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved-Call Complete</value>
        </criteriaItems>
        <description>Notify case staff to update CP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TD Request - Email Case Staff Certified Mail Status</fullName>
        <actions>
            <name>TD_Request_Email_Case_Staff_to_Obtain_Different_Address_Method_for_Delivery</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify case staff to find another address/method for delivery.</description>
        <formula>AND( TEXT( Certified_Mailing_Status__c ) &lt;&gt; &quot;Delivered&quot;, TEXT( Certified_Mailing_Status__c ) &lt;&gt; &quot;Refused&quot;, TEXT( Certified_Mailing_Status__c ) &lt;&gt; &quot;Unclaimed&quot;, OR( TEXT(PRIORVALUE(Certified_Mailing_Status__c)) = &quot;Address Unknown/Insufficient Address/Undeliverable as Addressed&quot;, TEXT(PRIORVALUE(Certified_Mailing_Status__c)) = &quot;No Forwarding Order&quot;, TEXT(PRIORVALUE(Certified_Mailing_Status__c)) = &quot;No Record of Mailing/Nothing Returned&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TD Request - Email Case Staff Close File</fullName>
        <actions>
            <name>TD_Request_Email_Case_Staff_to_Close_File</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved - Complete (Confirmation Received)</value>
        </criteriaItems>
        <description>Notify case staff to close the file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TD Request Audit Email Alert</fullName>
        <actions>
            <name>TD_Request_Audit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TD_Request__c.Send_to_Auditor__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends an email to auditors, when a user requests an audit of a TDR.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TD Request Reminder Email</fullName>
        <actions>
            <name>Email_Alert_to_TD_Request_Submitter_Approval_Status_48_Hour_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends Email Alert to TD Request Submitter - Approval Status after 48 hours</description>
        <formula>CreatedDate &lt; (NOW()-2) &amp;&amp;  ISPICKVAL(Approval_Status__c, &quot;Pending Attorney Approval&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
