<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Allocated_Fee_Zero</fullName>
        <field>Allocated_Fee__c</field>
        <formula>0</formula>
        <name>Allocated Fee = 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocated_Hard_Cost_0</fullName>
        <field>Allocated_Hard_Cost__c</field>
        <formula>0</formula>
        <name>Allocated Hard Cost = 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocated_Hard_Cost_Deposit_Amount</fullName>
        <field>Allocated_Hard_Cost__c</field>
        <formula>Amount__c</formula>
        <name>Allocated Hard Cost = Deposit Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocated_Soft_Cost_0</fullName>
        <field>Allocated_Soft_Cost__c</field>
        <formula>0</formula>
        <name>Allocated Soft Cost = 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deposit_Allocated_True</fullName>
        <field>Deposit_Allocated__c</field>
        <literalValue>1</literalValue>
        <name>Deposit Allocated = True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Allocated_Fee_Amount</fullName>
        <field>Allocated_Fee__c</field>
        <formula>Amount__c</formula>
        <name>Set Allocated Fee Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allocate Amount to Fee</fullName>
        <actions>
            <name>Allocated_Hard_Cost_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Allocated_Soft_Cost_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deposit_Allocated_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Allocated_Fee_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Deposit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Trust Payout - Fees</value>
        </criteriaItems>
        <description>Used on Deposits that are trust payout for fees - the full amount should be allocated to fees</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Transfer Deposit Fields</fullName>
        <actions>
            <name>Allocated_Fee_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Allocated_Hard_Cost_Deposit_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Allocated_Soft_Cost_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deposit_Allocated_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Deposit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transfer</value>
        </criteriaItems>
        <description>Sets some default fields when a transfer deposit is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
