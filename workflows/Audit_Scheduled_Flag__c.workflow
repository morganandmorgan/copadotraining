<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Audit_Scheduled_Flag_Notification</fullName>
        <description>Audit - Scheduled Flag Notification</description>
        <protected>false</protected>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>casesuccess@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Audit_Scheduled_Flag_User_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Active_Notification_Set_to_True</fullName>
        <description>Sets to true to kick off notification</description>
        <field>Active_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Active Notification - Set to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Trigger_Notification</fullName>
        <description>Sets the Trigger Notification checkbox to True</description>
        <field>Trigger_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Set Trigger Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Active_Notification_False</fullName>
        <description>Sets notification to false once sent</description>
        <field>Active_Notification__c</field>
        <literalValue>0</literalValue>
        <name>Update Active Notification - False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Message</fullName>
        <description>Set message field to be used in notification email template</description>
        <field>Scheduled_Notification_Message__c</field>
        <formula>Audit_Rule__r.Scheduled_Flag_Notification_Message__c</formula>
        <name>Update Message</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Audit Schedule - Set Active Notification to True</fullName>
        <actions>
            <name>Active_Notification_Set_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets active notification when created, or scheduled date is changed</description>
        <formula>AND(   Notification_Scheduled_Date__c &gt; NOW(),   Audit_Rule__r.Scheduled_Flag_Notification__c,   User__r.Receive_Case_Success_Notifications__c,   OR(     ISNEW(),     ISCHANGED(Scheduled_Date__c)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Audit Schedule - Set Values %26 Notification Trigger</fullName>
        <actions>
            <name>Update_Message</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update: 
Yitzi Requested to turn this off so that ATL users can be set up. 

Sets the time based trigger to send email and set active notification to false</description>
        <formula>AND(   Active_Notification__c,   ISPICKVAL(Status__c, &quot;Pending&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Trigger_Notification</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Audit_Scheduled_Flag__c.Notification_Scheduled_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>CS_Notification_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email notification sent to record user.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CS Notification Email Sent</subject>
    </tasks>
</Workflow>
