<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Matter_Search</fullName>
        <description>Updates Matter Search from Matter Record Name</description>
        <field>Matter_Search__c</field>
        <formula>Matter__r.Record_Name__c</formula>
        <name>Set Matter Search</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Matter Search</fullName>
        <actions>
            <name>Set_Matter_Search</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Matter Record Name to Matter Search field so that documents are searchable by Matter in Salesforce</description>
        <formula>ISNEW() || ISCHANGED(Matter__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
