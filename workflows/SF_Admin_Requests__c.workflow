<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_SFDC_Admin_Team_of_new_request</fullName>
        <description>Email Alert to SFDC Admin Team of new request</description>
        <protected>false</protected>
        <recipients>
            <recipient>jcary@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ksadler@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mphipps@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Request_Email_Templates/SF_Admin_Request_Notification</template>
    </alerts>
    <rules>
        <fullName>New Request Notification to Admin</fullName>
        <actions>
            <name>Email_Alert_to_SFDC_Admin_Team_of_new_request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SF_Admin_Requests__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Workflow rule which sends an email notification every time an SF Admin request is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
