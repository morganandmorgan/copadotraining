<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capture_Original_Due_Date</fullName>
        <description>Captures the original Due Date.</description>
        <field>Original_Due_Date__c</field>
        <formula>Due_Date__c</formula>
        <name>Capture Original Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Discovery Original Due Date</fullName>
        <actions>
            <name>Capture_Original_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures the original Due Date on the Discovery.</description>
        <formula>NOT(ISBLANK( Due_Date__c )) &amp;&amp; ISBLANK( Original_Due_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
