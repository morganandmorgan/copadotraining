<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_employee_name</fullName>
        <field>Employee_name__c</field>
        <formula>User__r.FirstName &amp;&quot; &quot;&amp; User__r.LastName</formula>
        <name>Set employee name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set User Name on Case Success Report Cards</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Audit_Report_Card__c.User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets the employee name on the CSR</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set employee name on Case Success Report Card</fullName>
        <actions>
            <name>Set_employee_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Audit_Report_Card__c.User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Takes the user field value and sets it to the employee name field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
