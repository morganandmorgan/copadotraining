<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Admin_Email_Litify_Sync_Error</fullName>
        <description>Send Admin Email Litify Sync Error</description>
        <protected>false</protected>
        <recipients>
            <recipient>ksadler@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rlamont@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Litify_Sync_Errors</template>
    </alerts>
    <alerts>
        <fullName>litify_pm__System_log_created_email_alert</fullName>
        <description>System log created email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>atreuhaft@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jdaniel@forthepeople.com1</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ksadler@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steven@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>litify_pm__Litify_Support/litify_pm__System_Log_Created_Email_Template</template>
    </alerts>
    <rules>
        <fullName>Inbound Sync Error</fullName>
        <actions>
            <name>Send_Admin_Email_Litify_Sync_Error</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Log__c.litify_pm__Classname__c</field>
            <operation>equals</operation>
            <value>ReferralInboundHandler # refresh_incoming_referrals</value>
        </criteriaItems>
        <description>Error sends keith email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>litify_pm__System Log Alert Email Notification</fullName>
        <actions>
            <name>litify_pm__System_log_created_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>litify_pm__Log__c.litify_pm__Severity__c</field>
            <operation>equals</operation>
            <value>ERROR,FATAL</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
