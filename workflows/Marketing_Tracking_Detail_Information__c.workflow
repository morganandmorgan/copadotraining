<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_CTM_ID_on_MTI</fullName>
        <field>ctm_id__c</field>
        <formula>Detail_Tracking_Value__c</formula>
        <name>Set CTM ID on MTI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Marketing_Tracking_Information__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>MTDI - Update CTM ID on Marketing Tracking Information</fullName>
        <actions>
            <name>Set_CTM_ID_on_MTI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Tracking_Detail_Information__c.Detail_Tracking_Parameter__c</field>
            <operation>equals</operation>
            <value>ctm_id</value>
        </criteriaItems>
        <description>Updates the CTM ID</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
