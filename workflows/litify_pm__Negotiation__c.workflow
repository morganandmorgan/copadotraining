<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BCC_Notification_to_Matt</fullName>
        <description>BCC Notification to Matt</description>
        <protected>false</protected>
        <recipients>
            <recipient>mattmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Negotiation_Approval_Submission_BCC_to_Matt</template>
    </alerts>
    <alerts>
        <fullName>Matt_Morgan_PFS_Approval_Request_Notification</fullName>
        <description>Matt Morgan PFS Approval Request Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>mattmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/PFS_Approval_Submission</template>
    </alerts>
    <alerts>
        <fullName>Memphis_Offer_To_Matt_Morgan</fullName>
        <description>Memphis Offer To Matt Morgan</description>
        <protected>false</protected>
        <recipients>
            <recipient>ewyka@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Offer_Notification_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Negotiation_Approval_Status_Atty</fullName>
        <description>Negotiation Approval Status - Atty</description>
        <protected>false</protected>
        <recipients>
            <field>Approving_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Negotiations_Approval_Result_HTML</template>
    </alerts>
    <alerts>
        <fullName>Negotiation_Approval_Status_Notification</fullName>
        <description>Negotiation Approval Status Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Negotiations_Approval_Result_HTML</template>
    </alerts>
    <alerts>
        <fullName>Negotiation_Demand_Approval_Status_Notification</fullName>
        <description>Negotiation Demand Approval Status Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Approving_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Negotiations_Demand_Approval_Result_HTML</template>
    </alerts>
    <alerts>
        <fullName>Negotiation_New_Insurance_Company</fullName>
        <ccEmails>operations@forthepeople.com</ccEmails>
        <description>Negotiation - New Insurance Company</description>
        <protected>false</protected>
        <senderAddress>contact@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Internal_Notifications/Negotiation_New_Insurance_Company</template>
    </alerts>
    <alerts>
        <fullName>Negotiation_Offer_Monitor_Email_to_Matt_Morgan_Amount_10K</fullName>
        <description>Negotiation Offer Monitor Email to Matt Morgan Amount &gt;= 10K</description>
        <protected>false</protected>
        <recipients>
            <recipient>mattmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Negotiation_Offer_Over_10_000_To_Matt_Morgan</template>
    </alerts>
    <alerts>
        <fullName>Notification_Over_50_000_To_Matt</fullName>
        <description>Notification Over $50,000 To Matt</description>
        <protected>false</protected>
        <recipients>
            <recipient>mattmorgan@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Negotiation_Over_50_000_To_Matt_Morgan</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_Offer_In_Progress</fullName>
        <description>Set status to in progress</description>
        <field>Approval_Status__c</field>
        <literalValue>Approval in Process</literalValue>
        <name>Approval Status - Offer In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Offer_In_Progress_BCC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>BCC Approval in Process</literalValue>
        <name>Approval Status - Offer In Progress BCC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approving_Attorney_Approval_Date</fullName>
        <field>Approving_Attorney_Approved_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Approving Attorney Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Matter_Stage</fullName>
        <field>Matter_Stage_when_Submitted__c</field>
        <formula>litify_pm__Matter__r.Reporting_Matter_Stage__c</formula>
        <name>Capture Matter Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Matter_Status</fullName>
        <field>Matter_Status_when_Submitted__c</field>
        <formula>TEXT(litify_pm__Matter__r.litify_pm__Status__c)</formula>
        <name>Capture Matter Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Negotiation_Update_Search_Display</fullName>
        <field>Search_Display__c</field>
        <formula>TEXT(MONTH(litify_pm__Date__c))&amp;&quot;/&quot;&amp;
TEXT(DAY(litify_pm__Date__c))&amp;&quot;/&quot;&amp;
TEXT(YEAR(litify_pm__Date__c))
&amp;&quot; - $&quot;&amp;
TEXT(litify_pm__Amount__c)
&amp;&quot; - &quot;&amp;
TEXT(Insurance_Type__c)
&amp;&quot; - &quot;&amp;
RecordType.DeveloperName</formula>
        <name>Negotiation - Update Search Display</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Date_Time</fullName>
        <field>Rejected_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Rejected Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Description_of_Incident</fullName>
        <description>Set description of incident on negotiation record on all edit/saves</description>
        <field>Description_of_Incident__c</field>
        <formula>litify_pm__Matter__r.Description_of_Incident__c</formula>
        <name>Set Description of Incident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Negotiation_Name</fullName>
        <field>Name</field>
        <formula>IF(
				litify_pm__Negotiating_with__r.litify_pm__Party__r.IsPersonAccount,
				litify_pm__Negotiating_with__r.litify_pm__Party__r.FirstName&amp;&quot; &quot;&amp;litify_pm__Negotiating_with__r.litify_pm__Party__r.LastName&amp;&quot; - &quot;&amp;litify_pm__Matter__r.ReferenceNumber__c,
				litify_pm__Negotiating_with__r.litify_pm__Party_Name__c&amp;&quot; - &quot;&amp;litify_pm__Matter__r.ReferenceNumber__c
)</formula>
        <name>Set Negotiation Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Draft</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Approval Status - Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BCC_Submission_Date</fullName>
        <field>BCC_Approval_Submitted_Date_Time__c</field>
        <formula>IF(
  litify_pm__Amount__c &gt;= 150000, NOW(), NULL
  )</formula>
        <name>Update BCC Submission Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Final_Approval_Date</fullName>
        <field>Approved_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Final Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submission_Date</fullName>
        <field>Approval_Submitted_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Submission Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Negotiation - Update Search Display</fullName>
        <actions>
            <name>Negotiation_Update_Search_Display</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When fields are updated, this will update the Search Display field used in Search Layouts</description>
        <formula>OR( ISNEW(), ISCHANGED(litify_pm__Date__c), ISCHANGED(RecordTypeId), ISCHANGED(Insurance_Type__c), ISCHANGED(litify_pm__Amount__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Negotiation BCC notification to Matt</fullName>
        <actions>
            <name>BCC_Notification_to_Matt</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>litify_pm__Negotiation__c.BCC_Approval_Submitted_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notifies Matt when a BCC case is approved by attorney</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Negotiation Email Notification For Memphis Offers</fullName>
        <actions>
            <name>Memphis_Offer_To_Matt_Morgan</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies Matt Morgan for all offers submitted for Memphis.</description>
        <formula>litify_pm__Matter__r.Assigned_Office_Location__c = &quot;001o000000tkgYF&quot;  &amp;&amp; TEXT(PRIORVALUE(Approval_Status__c))&lt;&gt; &quot;Approval in Process&quot; &amp;&amp; ISPICKVAL( Approval_Status__c, &quot;Approval in Process&quot;) &amp;&amp; (ISPICKVAL( litify_pm__Type__c, &quot;Initial Offer&quot;)  ||  ISPICKVAL( litify_pm__Type__c, &quot;Counter Offer&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Negotiation Email Notification For Offers Over %2450%2C000</fullName>
        <actions>
            <name>Notification_Over_50_000_To_Matt</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies Matt Morgan when a negotiation of over $50,000 is submitted.</description>
        <formula>litify_pm__Matter__r.Assigned_Office_Location__c &lt;&gt; &quot;001o000000tkgYF&quot;  &amp;&amp; litify_pm__Amount__c  &gt;= 50000  &amp;&amp; TEXT(PRIORVALUE(Approval_Status__c))&lt;&gt; &quot;Approval in Process&quot; &amp;&amp; ISPICKVAL( Approval_Status__c, &quot;Approval in Process&quot;) &amp;&amp; (ISPICKVAL( litify_pm__Type__c, &quot;Initial Offer&quot;)  ||  ISPICKVAL( litify_pm__Type__c, &quot;Counter Offer&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Negotiation Offer Monitor Email to Matt</fullName>
        <actions>
            <name>Negotiation_Offer_Monitor_Email_to_Matt_Morgan_Amount_10K</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>KS updated 5/11 to include all for Malory (removed 10k and PL rules) per Ash

If approving attorney Monitor Offers = true and Amount &gt;= 10,000
Adjusted so that policy limits are exluded</description>
        <formula>AND(
  Approving_Attorney__r.Monitor_Offers__c = true,
  NOT(ISBLANK(Approval_Submitted_Date_Time__c)),
  RecordType.DeveloperName = &quot;Offer&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Description of Incident</fullName>
        <actions>
            <name>Set_Description_of_Incident</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Negotiation__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set description of incident on Negotiation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Negotiation Name</fullName>
        <actions>
            <name>Set_Negotiation_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(
				ISNEW(),
				ISCHANGED(litify_pm__Negotiating_with__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
