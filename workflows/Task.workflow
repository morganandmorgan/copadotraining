<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Wendy_Nation</fullName>
        <description>Email to Wendy Nation</description>
        <protected>false</protected>
        <recipients>
            <recipient>wnation@nationlaw.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cccnotifications@forthepeople.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Wendy_New_Regarding_Task</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Task_Type_as_Call</fullName>
        <field>Type</field>
        <literalValue>Call</literalValue>
        <name>Populate Task Type as Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_Date_to_NOW</fullName>
        <field>Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Completed Date to NOW()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disposition</fullName>
        <field>CallDisposition</field>
        <formula>Disposition__c</formula>
        <name>Update Disposition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Status_to_Cancelled</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Update Task Status to Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Status_to_Complete</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Update Task Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Call Logged from Matter Page</fullName>
        <actions>
            <name>Populate_Task_Type_as_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Cancel Tasks if Due Date %3E 60 days</fullName>
        <actions>
            <name>Update_Task_Status_to_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Task_Due_Date__c &lt; TODAY() &amp;&amp; TODAY() - 60 &gt; Task_Due_Date__c &amp;&amp; NOT(ISBLANK(litify_pm__Default_Matter_Task__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Wendy New Regarding Task</fullName>
        <actions>
            <name>Email_to_Wendy_Nation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Wendy Nation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Mail Out</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Disposition field on Tast</fullName>
        <actions>
            <name>Update_Disposition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Task Completed Date</fullName>
        <actions>
            <name>Update_Completed_Date_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(     OR( 				    ISCHANGED(Status),    				    ISNEW() 				),     ISPICKVAL(Status,&quot;Completed&quot;),     ISNULL(Completed_Date__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task - Set Email Task to Completed</fullName>
        <actions>
            <name>Update_Completed_Date_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Task_Status_to_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Email:</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Work Item Task Expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Work_Item_Task__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Cancels an open Work Item Task on the Due Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Task_Status_to_Complete</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
