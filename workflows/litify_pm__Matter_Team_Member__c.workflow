<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Billable_Hourly_Rate</fullName>
        <field>litify_pm__Billable_Hourly_Rate__c</field>
        <formula>litify_pm__User__r.Billable_Hourly_Rate__c</formula>
        <name>Set Billable Hourly Rate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MTM_Name</fullName>
        <field>Name</field>
        <formula>User_Name_Formula__c</formula>
        <name>Update MTM Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MTM - Enforce Naming Convention</fullName>
        <actions>
            <name>Update_MTM_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Name field to match the User&apos;s name.</description>
        <formula>Name!=User_Name_Formula__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Billable Hourly Rate</fullName>
        <actions>
            <name>Set_Billable_Hourly_Rate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW() &amp;&amp; ISNULL(litify_pm__User__r.Billable_Hourly_Rate__c) = FALSE,  ISCHANGED(litify_pm__User__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
