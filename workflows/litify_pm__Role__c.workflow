<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Role_Name</fullName>
        <field>Name__c</field>
        <formula>IF(litify_pm__Party__r.IsPersonAccount,
litify_pm__Party__r.FirstName&amp;&quot; &quot;&amp;litify_pm__Party__r.LastName,
litify_pm__Party__r.Name
)</formula>
        <name>Set Role Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Role Name</fullName>
        <actions>
            <name>Set_Role_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>litify_pm__Role__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets role name for searching in lookups</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
