<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Trial_Request_Approval</fullName>
        <description>Trial Request Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Matter_Principal_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Trial_Request_Approval_Notice</template>
    </alerts>
    <alerts>
        <fullName>Trial_Request_Rejection</fullName>
        <description>Trial Request Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Matter_Principal_Attorney__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Internal_Notifications/Trial_Request_Rejection_Notice</template>
    </alerts>
</Workflow>
