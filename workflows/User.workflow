<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Call_Center_is_Empty</fullName>
        <description>Call Center is Empty</description>
        <protected>false</protected>
        <recipients>
            <recipient>rlamont@forthepeople.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Call_Center_Empty_Warning</template>
    </alerts>
    <fieldUpdates>
        <fullName>Null_IC_Email_Sync</fullName>
        <field>IC_Email_Sync__c</field>
        <formula>Email</formula>
        <name>Null IC Email Sync</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IC_Email_Sync</fullName>
        <field>IC_Email_Sync__c</field>
        <formula>Email</formula>
        <name>Set IC Email Sync</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Call Center Empty Warning</fullName>
        <actions>
            <name>Call_Center_is_Empty</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>CCC Case Consultant</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CallCenterId</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Null IC Email Sync</fullName>
        <actions>
            <name>Null_IC_Email_Sync</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set IC Email Sync</fullName>
        <actions>
            <name>Set_IC_Email_Sync</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
