public class mmbrms_UserInRolesSpecified
    implements mmbrms_IRuleEvaluation
{
    public List<String> rolesSpecifiedList = new List<String>();

    public Boolean isEvaluationResponseInverted = false;

    public Boolean evaluate()
    {
        if (isEvaluationResponseInverted == null)
        {
            isEvaluationResponseInverted =  false;
        }

        Boolean evalResponse = new mmcommon_UserInfo( UserInfo.getUserId() ).hasUserRoleName( rolesSpecifiedList );

        return isEvaluationResponseInverted ? ! evalResponse : evalResponse;
    }
}