public class mmlitifylrn_CredentialsInputController
{
    public string lrnUsername { get; set; }
    public string lrnPassword { get; set; }
    public string lrnFirmSelected { get; set; }

    public list<SelectOption> lrnFirmsOptions { get; private set; } { lrnFirmsOptions = new list<SelectOption>(); }

    public list<mmlitifylrn_ThirdPartyCredential__c> credentials
    {
        get
        {
            return mmlitifylrn_CredentialsSelector.newInstance().selectAll();
        }
    }

    public mmlitifylrn_CredentialsInputController()
    {
        primeLRNFirmOptionsList();
    }

    private void primeLRNFirmOptionsList()
    {
        lrnFirmsOptions.clear();

        lrnFirmsOptions.addAll( mmlib_Utils.getSelectOptions( mmlitifylrn_FirmsSelector.newInstance().selectBySelectedThirdPartyReferralManaged(), true ) );
    }

    public PageReference performAuthentication()
    {
        system.debug( 'lrnUsername == '  + lrnUsername );
        system.debug( 'lrnPassword == '  + lrnPassword );
        system.debug( 'lrnFirmSelected == '  + lrnFirmSelected );

        PageReference responsePageRef = null;

        try
        {
            id lrnFirmId = string.isNotBlank( lrnFirmSelected ) ? id.valueOf(lrnFirmSelected) : null;

            mmlitifylrn_AuthenticationsService.authenticateUser( lrnUsername, lrnPassword, lrnFirmId );

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Authentication successful and saved.' ) );

            lrnUsername = null;
            lrnPassword = null;
            lrnFirmSelected = null;
        }
        catch ( mmlitifylrn_Exceptions.AuthenticationsServiceException ase )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Authentication attempt failed. \n\n' + ase.getMessage() ) );
        }
        catch ( Exception e )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Authentication attempt failed. \n\n' + e.getMessage() ) );
        }

        return responsePageRef;
    }
}