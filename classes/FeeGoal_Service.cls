/**
 * FeeGoal_Service
 * @description Services for Fee Goal computing and transactions.
 * @author Jeff Watson
 * @date 2/13/2019
 */

public with sharing class FeeGoal_Service {

    private FeeGoal_Selector feeGoalSelector;
    private FeeGoalAllocation_Selector feeGoalAllocationSelector;

    // Ctors
    public FeeGoal_Service() {
        this.feeGoalSelector = new FeeGoal_Selector();
        this.feeGoalAllocationSelector = new FeeGoalAllocation_Selector();
    }

    public FeeGoal_Service(FeeGoal_Selector feeGoalSelector, FeeGoalAllocation_Selector feeGoalAllocationSelector) {
        this.feeGoalSelector = feeGoalSelector;
        this.feeGoalAllocationSelector = feeGoalAllocationSelector;
    }

    // Methods
    // Complete operations or transactions that commit changes.
    public void updateFeeGoalWithMatterCount(List<Fee_Goal_Allocation__c> feeGoalAllocations) {

        Set<Id> feeGoalIds = new Set<Id>();
        for (Fee_Goal_Allocation__c feeGoalAllocation : feeGoalAllocations) {
            feeGoalIds.add(feeGoalAllocation.Fee_Goal__c);
        }

        List<Fee_Goal__c> feeGoals = feeGoalSelector.selectById(feeGoalIds);
        List<Fee_Goal_Allocation__c> inScopeFeeGoalAllocations = feeGoalAllocationSelector.selectByFeeGoalIds(feeGoalIds);

        for (Fee_Goal__c feeGoal : feeGoals) {
            feeGoal.Matter_Count__c = 0;
            Set<Id> matterIds = new Set<Id>();
            for (Fee_Goal_Allocation__c inScopeFeeGoalAllocation : inScopeFeeGoalAllocations) {
                if (feeGoal.Id == inScopeFeeGoalAllocation.Fee_Goal__c) {
                    matterIds.add(inScopeFeeGoalAllocation.Settlement__r.Matter__c);
                }
            }
            feeGoal.Matter_Count__c = matterIds.size();
        }

        update feeGoals;
    }
}