public class mmpsc_AccountSearchInputController
{
    @AuraEnabled
    public static List<String> searchForAccounts(Map<String, String> data)
    {
        System.debug('<ojs> searchForAccounts(data):\n' + JSON.serializePretty(data));

        List<String> idStringList = new List<String>();
        for (Account a : mmcommon_PersonAccountsService.searchAccountsUsingFuzzyMatching(data))
        {
            idStringList.add(a.Id);
        }
        
        System.debug('<ojs> IDs returned:\n' + JSON.serializePretty(idStringList));

        System.debug('<ojs> IDs returned:\n' + JSON.serializePretty(idStringList));

        return idStringList;
    }
}