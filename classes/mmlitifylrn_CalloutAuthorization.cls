public class mmlitifylrn_CalloutAuthorization
    implements mmlib_ICalloutAuthorizationable
{
    private mmlitifylrn_ICredentials credentials;
    private litify_pm__Firm__c firmMakingTheCallout;
    private mmlib_ISObjectUnitOfWork uow;

    public mmlitifylrn_CalloutAuthorization setCrendentials( mmlitifylrn_ICredentials credentials )
    {
        this.credentials = credentials;
        return this;
    }

    public mmlitifylrn_ICredentials getCredentials()
    {
        return this.credentials;
    }

    public mmlitifylrn_CalloutAuthorization setCurrentFirmMakingCallout( litify_pm__Firm__c firmMakingTheCallout )
    {
        this.firmMakingTheCallout = firmMakingTheCallout;
        return this;
    }

    public litify_pm__Firm__c getFirmMakingTheCallout()
    {
        return this.firmMakingTheCallout;
    }

    public mmlitifylrn_CalloutAuthorization setUow( mmlib_ISObjectUnitOfWork uow )
    {
        this.uow = uow;
        return this;
    }

    public mmlib_ISObjectUnitOfWork getUow()
    {
        return this.uow;
    }

    public void setAuthorization(HttpRequest request)
    {
        system.debug( 'request at the setAuthorization call is ' + request );

        if ( request == null )
        {
            system.debug( System.LoggingLevel.WARN, 'An HttpRequest is required.');
            return;
        }

        if ( this.credentials == null )
        {
            system.debug( System.LoggingLevel.WARN, 'An valid ' + mmlitifylrn_ICredentials.class + ' is required.');
            return;
        }

        if ( this.firmMakingTheCallout == null )
        {
            system.debug( System.LoggingLevel.WARN, 'An valid ' + litify_pm__Firm__c.SObjectType + ' record is required for firmMakingTheCallout.');
            return;
        }

        if ( String.isBlank( this.credentials.getSessionTokenForFirm( this.firmMakingTheCallout ) ) )
        {
            system.debug( System.LoggingLevel.WARN, 'No firmMakingTheCallout was found in the credentials supplied');
            return;
        }

        request.setHeader( mmlitifylrn_Constants.LRN_API_ACCESS_TOKEN_KEY, this.credentials.getSessionTokenForFirm( this.firmMakingTheCallout ));
    }


}