/**
 *  mmmarketing_CreateCampaignsForMFPAction
 */
public with sharing class mmmarketing_CreateCampaignsForMFPAction
    extends mmlib_AbstractAction
{
    public override Boolean runInProcess()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticCampaignSetupEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticCampaignSetupEnabled__c )
        {
            // of the records submitted,
            mmmarketing_CampaignFactory factory = new mmmarketing_CampaignFactory( this.uow );

            Set<String> campaignTechnicalKeysSet = new Set<String>();

            set<String> campaignTechnicalKeysFromRecordSet = new Set<String>();

            // loop through the records
            for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)this.records)
            {
                campaignTechnicalKeysFromRecordSet = mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record );

                //system.debug('~~~~~~ record.Marketing_Campaign__c : '+record.Marketing_Campaign__c);
                //system.debug('~~~~~~ record.Campaign_Value__c : '+record.Campaign_Value__c);
                //system.debug('~~~~~~ record.Campaign_AdWords_ID_Value__c : '+record.Campaign_AdWords_ID_Value__c);

                // find those that do not yet have Marketing_Campaign__c populated
                //      but do have Campaign_Value__c or Campaign_AdWords_ID_Value__c populated
                campaignTechnicalKeysSet.addAll( campaignTechnicalKeysFromRecordSet );
            }

            //system.debug('~~~~~~ campaignTechnicalKeysSet : '+ campaignTechnicalKeysSet);

            // Only proceed if there are values in the campaignTechnicalKeysSet
            if ( ! campaignTechnicalKeysSet.isEmpty() )
            {
                // With that set of campaign technical keys, query the campaign Marketing_Campaign__c table
                List<Marketing_Campaign__c> currentlyAvailableCampaignList = mmmarketing_CampaignsSelector.newInstance().selectByTechnicalKeys( campaignTechnicalKeysSet );

                Set<String> currentlyAvailableCampaignsTechnicalKeysSet = new Set<String>();
                Set<String> campaignTechnicalKeysUtilizedSet = new Set<String>();

                for (Marketing_Campaign__c currentlyAvailableCampaign : currentlyAvailableCampaignList)
                {
                    currentlyAvailableCampaignsTechnicalKeysSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( currentlyAvailableCampaign ) );
                }

                //system.debug('~~~~~~ currentlyAvailableCampaignsTechnicalKeysSet : '+ currentlyAvailableCampaignsTechnicalKeysSet);

                Set<String> technicalKeysDerivedFromMFPSet = new Set<String>();

                boolean isAtLeastOneTechKeyFound = false;

                for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)this.records)
                {
                    system.debug( 'campaignTechnicalKeysUtilizedSet size == ' + campaignTechnicalKeysUtilizedSet.size() );

                    technicalKeysDerivedFromMFPSet.clear();
                    technicalKeysDerivedFromMFPSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) );

                    system.debug( 'technicalKeysDerivedFromMFPSet == ' + technicalKeysDerivedFromMFPSet );

                    isAtLeastOneTechKeyFound = false;

                    for ( String techKey : technicalKeysDerivedFromMFPSet )
                    {
                        if ( string.isNotBlank( techKey ) )
                        {
                            if ( ! currentlyAvailableCampaignsTechnicalKeysSet.contains( techKey )
                                && ! campaignTechnicalKeysUtilizedSet.contains( techKey )
                                )
                            {
                                // techKey1 was not found
                                isAtLeastOneTechKeyFound = true;
                            }
                        }
                    }

                    if ( isAtLeastOneTechKeyFound )
                    {
                        // none of the values found in technicalKeysDerivedFromMFPSet were seen in
                        //      the currentlyAvailableCampaignsTechnicalKeysSet nor the campaignTechnicalKeysUtilizedSet

                        //system.debug('~~~~~~ it is being added.');
                        factory.generateNewFromCampaignValues( record.Campaign_Value__c, record.Campaign_AdWords_ID_Value__c, record.Domain_Value__c );

                        // Add the campaign values to the campaignTechnicalKeysUtilizedSet so we don't create duplicates
                        campaignTechnicalKeysUtilizedSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) );
                    }
                }
            }
        }

        return true;
    }
}