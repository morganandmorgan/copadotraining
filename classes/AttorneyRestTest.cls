@isTest
private class AttorneyRestTest {
    static RestRequest req;
        static {
                req = new RestRequest();
                RestContext.request = req;
                RestContext.response = new RestResponse();

                req.params.put('apiKey', 'b33f8613bf7b');
        }

        @isTest static void NoApiKey_Unauthorized() {
                req.params.remove('apiKey');

                        List<AttorneyRest.AttorneyRestDTO> result = AttorneyRest.DoGet();
                        System.assertEquals(401, RestContext.response.statusCode);
        }

        @isTest static void WrongApiKey_Throws() {
                req.params.put('apiKey', 'incorrect');
                        List<AttorneyRest.AttorneyRestDTO> result = AttorneyRest.DoGet();
                        System.assertEquals(401, RestContext.response.statusCode);
        }
    @isTest static void NoFirmFound_ReturnsEmptyResult() {
        List<AttorneyRest.AttorneyRestDTO> result = AttorneyRest.DoGet();

                System.assertEquals(0, result.size());
    }

    @isTest static void FirmFound_AttorneysFromMorganAreReturned() {
        Account morganAccount = TestUtil.createAccount('Morgan & Morgan P.A.', 'Morgan_Morgan_Businesses');
        Account nonMorganAccount = TestUtil.createAccount('Non Morgan');

        List<Account> allAccounts = new List<Account>{ morganAccount, nonMorganAccount };
        insert allAccounts;

        Contact morganAttorney = new Contact(AccountId = morganAccount.Id, Title = 'Attorney', FirstName = 'Morgan', LastName = 'Attorney', Reveal_In_Attorney_s_Portal__c = TRUE);
        Contact morganSecretary = new Contact(AccountId = morganAccount.Id, Title = 'Secretary', FirstName = 'Morgan', LastName = 'Secretary');
        Contact nonMorganAttorney = new Contact(AccountId = nonMorganAccount.Id, Title = 'Attorney', FirstName = 'NonMorgan', LastName = 'Attorney');

        List<Contact> allContacts = new List<Contact>{ morganAttorney, morganSecretary, nonMorganAttorney };
        insert allContacts;

        Test.startTest();
        List<AttorneyRest.AttorneyRestDTO> result = AttorneyRest.DoGet();
        Test.stopTest();

        System.debug(result);

        System.assertEquals(1, result.size());

        AttorneyRest.AttorneyRestDTO firstResult = result.get(0);
        System.assertEquals(morganAttorney.Id, firstResult.id);
        System.assertEquals(morganAttorney.FirstName + ' ' + morganAttorney.LastName, firstResult.name);
    }

}