/**
 *  mmlib_ICriteria
 */
public interface mmlib_ICriteria
{
    mmlib_ICriteria setRecordsToEvaluate( list<SObject> records );
    list<Sobject> run();
}