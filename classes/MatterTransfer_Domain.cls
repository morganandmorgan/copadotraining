/*============================================================================
Name            : MatterTransfer_Domain
Author          : CLD
Created Date    : Apr 2019
Description     : Domain class for Matter Transfer
=============================================================================*/
public class MatterTransfer_Domain extends fflib_SObjectDomain {

    private List<Matter_Transfer__c> mTransfers;
    private MatterTransfer_Service mTransferService;

    // Ctors
    public MatterTransfer_Domain() {
        super();
        this.mTransfers = new List<Matter_Transfer__c>();
        this.mTransferService = new MatterTransfer_Service();
    }

    public MatterTransfer_Domain(List<Matter_Transfer__c> mTransfers) {
        super(mTransfers);
        this.mTransfers = (List<Matter_Transfer__c>) records;
        this.mTransferService = new MatterTransfer_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new MatterTransfer_Domain(records);
        }
    }

    // Trigger Handlers
    public override void onBeforeInsert() {
        //TO DO: check if specific specific trigger context is disabled

        mTransferService.setFromFields(mTransfers);
    }

    public override void onAfterInsert() {
        //TO DO: check if specific specific trigger context is disabled
        mTransferService.processTransfer(mTransfers);
    }
   
}