public interface mmlitifylrn_IFirmsSelector extends mmlib_ISObjectSelector
{
    List<litify_pm__Firm__c> selectById( Set<id> idSet );
    List<litify_pm__Firm__c> selectAll();
    List<litify_pm__Firm__c> selectBySelectedThirdPartyReferralManaged();
    List<litify_pm__Firm__c> selectByDefaultAndSelectedThirdPartyReferralManaged();
    List<litify_pm__Firm__c> selectByExternalId( Set<Double> externalIdSet );
}