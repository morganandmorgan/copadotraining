public class RelatedIntakesController {
  private final ApexPages.StandardController stdController;

  public List<Intake__c> RelatedIntakes { get; private set; }
  public Boolean HasRelatedIntakes { get {
    return RelatedIntakes.size() > 0;
    }}

	public RelatedIntakesController(ApexPages.StandardController stdControllerParam) {        
    stdController = stdControllerParam;
    Id intakeId = stdController.getId();
    
    Intake__c intake = [SELECT Id, Incident__c FROM Intake__c WHERE Id = :intakeId];
    RelatedIntakes = [SELECT Name, Case_Type__c, Client__r.Id, Client__r.Name, Injured_Party__r.Id, Injured_Party__r.Name, Status__c FROM Intake__c WHERE Incident__c <> NULL AND Incident__c = :intake.Incident__c AND Id <> :intake.id];
	}
}