public class mmlitifylrn_ReferralsSelector
    extends mmlib_SObjectSelector
    implements mmlitifylrn_IReferralsSelector
{
    public static mmlitifylrn_IReferralsSelector newInstance()
    {
        return (mmlitifylrn_IReferralsSelector) mm_Application.Selector.newInstance(litify_pm__Referral__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Referral__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            litify_pm__Referral__c.Check_Amount__c,
            litify_pm__Referral__c.Check_Number__c,
            litify_pm__Referral__c.Check_Received_Date__c,
            litify_pm__Referral__c.Intake__c,
            litify_pm__Referral__c.litify_pm__Active_Referral_Transaction__c,
            litify_pm__Referral__c.litify_pm__Case_Address_1__c,
            litify_pm__Referral__c.litify_pm__Case_Address_2__c,
            litify_pm__Referral__c.litify_pm__Case_City__c,
            litify_pm__Referral__c.litify_pm__Case_Postal_Code__c,
            litify_pm__Referral__c.litify_pm__Case_State__c,
            litify_pm__Referral__c.litify_pm__Case_Type__c,
            litify_pm__Referral__c.litify_pm__Client_Address_1__c,
            litify_pm__Referral__c.litify_pm__Client_Address_2__c,
            litify_pm__Referral__c.litify_pm__Client_City__c,
            litify_pm__Referral__c.litify_pm__Client_First_Name__c,
            litify_pm__Referral__c.litify_pm__Client_Last_Name__c,
            litify_pm__Referral__c.litify_pm__Client_Postal_Code__c,
            litify_pm__Referral__c.litify_pm__Client_State__c,
            litify_pm__Referral__c.litify_pm__Client_email__c,
            litify_pm__Referral__c.litify_pm__Client_phone__c,
            litify_pm__Referral__c.litify_pm__Description__c,
            litify_pm__Referral__c.litify_pm__ExternalId__c,
            litify_pm__Referral__c.litify_pm__External_Ref_Number__c,
            litify_pm__Referral__c.litify_pm__Extra_info__c,
            litify_pm__Referral__c.litify_pm__Handling_Firm__c,
            litify_pm__Referral__c.litify_pm__Incident_date__c,
            litify_pm__Referral__c.litify_pm__Intake__c,
            litify_pm__Referral__c.litify_pm__LRN_Created_At__c,
            litify_pm__Referral__c.litify_pm__Litify_Status__c,
            litify_pm__Referral__c.litify_pm__Litify_com_ID__c,
            litify_pm__Referral__c.litify_pm__Originating_Firm__c,
            litify_pm__Referral__c.litify_pm__Referral_Kind__c,
            litify_pm__Referral__c.litify_pm__Referrals__c,
            litify_pm__Referral__c.litify_pm__Status_Description__c,
            litify_pm__Referral__c.litify_pm__Status__c,
            litify_pm__Referral__c.litify_pm__Suggested_Percentage_fee__c,
            litify_pm__Referral__c.litify_pm__Sync_Status_Description__c,
            litify_pm__Referral__c.litify_pm__Sync_last_updated_at__c,
            litify_pm__Referral__c.litify_pm__sync_fail_count__c,
            litify_pm__Referral__c.litify_pm__sync_msg__c,
            litify_pm__Referral__c.litify_pm__sync_status__c,
            litify_pm__Referral__c.mmlitifylrn_CaseTypeExternalId__c,
            litify_pm__Referral__c.mmlitifylrn_CaseTypeName__c,
            litify_pm__Referral__c.Referral_Transaction_Status__c
        };
    }

    public List<litify_pm__Referral__c> selectById( Set<id> idSet )
    {
        return (List<litify_pm__Referral__c>) selectSObjectsById(idSet);
    }

    public List<litify_pm__Referral__c> selectWithFirmAndCaseTypeAndTransactionsByExternalId( Set<Double> externalIdSet )
    {
        fflib_QueryFactory referralsQueryFactory = newQueryFactory();

        // Add the parent case type
        new mmlitifylrn_CaseTypesSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Case_Type__c );

        // Add the originating firm
        new mmlitifylrn_FirmsSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Originating_Firm__c );

        // Add the handling firm
        new mmlitifylrn_FirmsSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Handling_Firm__c );

        // Add child transactions
        new mmlitifylrn_ReferralTransSelector().addQueryFactorySubselect( referralsQueryFactory );

        return Database.query( referralsQueryFactory.setCondition(litify_pm__Referral__c.litify_pm__ExternalId__c  + ' in :externalIdSet').toSOQL() );
    }
    
    public List<litify_pm__Referral__c> selectWithFirmAndCaseTypeById( Set<id> idSet )
    {
        fflib_QueryFactory referralsQueryFactory = newQueryFactory();

        // Add the parent case type
        new mmlitifylrn_CaseTypesSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Case_Type__c );

        // Add the originating firm
        new mmlitifylrn_FirmsSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Originating_Firm__c );

        // Add the handling firm
        new mmlitifylrn_FirmsSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Handling_Firm__c );

        // finish the query
        return Database.query( referralsQueryFactory.setCondition('Id in :idSet').toSOQL() );
    }

    public String selectWithFirmByExternalIdQuery( )
    {
        fflib_QueryFactory referralsQueryFactory = newQueryFactory();

        // Add the originating firm
        new mmlitifylrn_FirmsSelector().addQueryFactoryParentSelect( referralsQueryFactory, litify_pm__Referral__c.litify_pm__Originating_Firm__c );

        return referralsQueryFactory.setCondition(litify_pm__Referral__c.litify_pm__ExternalId__c  + ' in :externalIdSet').toSOQL();
    }
}