public with sharing class mmbrms_RulesEngine
    implements mmbrms_IRuleEvaluation
{
    private mmbrms_RulesEngine() {}

    public enum RuleInputHint
    {
        requiresRecordToEvaluate
    }

    private String ruleName {private set; get;} {ruleName = '';}
    private SObject recordToEvaluate  { set; private get; }
    private Set<RuleInputHint> ruleInputSet = new Set<RuleInputHint>();
    private List<mmbrms_IRuleEvaluation> ruleSegementsToEvaluateList = new List<mmbrms_IRuleEvaluation>();
    private Boolean defaultResponse = false;

    public static mmbrms_RulesEngine newInstance(String businessRuleToken)
    {
        mmbrms_RulesEngine engine = new mmbrms_RulesEngine();

        engine.ruleName = businessRuleToken;

        mmbrms_IBusinessRules rule = getBusinessRules( businessRuleToken );

        mmbrms_RuleDefinition def = (mmbrms_RuleDefinition) JSON.deserialize( rule.getRuleDefinition(), mmbrms_RuleDefinition.class );

        String ruleSetupJson = null;

        for (mmbrms_RuleDefinition.RuleSegment segment : def.segmentList)
        {
            // this is where we need to JSON.deserialize into the specific ruleType class with rule setup information.
            //engine.ruleSegementsToEvaluateList.add( (mmbrms_IRuleEvaluation) Type.forName(segment.ruleType).newInstance() );

            if ( String.isNotBlank( segment.ruleSetupJson ) )
            {
                ruleSetupJson = segment.ruleSetupJson;
            }
            else
            {
                ruleSetupJson = '{}';
            }

            engine.ruleSegementsToEvaluateList.add( (mmbrms_IRuleEvaluation) JSON.deserialize( ruleSetupJson, Type.forName(segment.ruleType) ) );


            if (segment.ruleInputHints != null)
            {
                engine.ruleInputSet.addAll(segment.ruleInputHints);
            }
        }
        return engine;
    }

    private static mmbrms_IBusinessRules getBusinessRules(String name)
    {
        List<BusinessRule__mdt> brMdtList = mmbrms_BusinessRuleSelector.newInstance().selectByDeveloperNames(new Set<String> {name});

        if ( ! brMdtList.isEmpty() )
        {
            return new mmbrms_BusinessRules(brMdtList.get(0));
        }

        throw new mmbrms_BusinessRuleExceptions.ParameterException('The specified business rule (' + name + ') does not exist.');
    }

    private Boolean isRulesInputReady()
    {
        if ( ruleInputSet.contains(RuleInputHint.requiresRecordToEvaluate) && recordToEvaluate == null)
        {
            throw new mmbrms_BusinessRuleExceptions.ParameterException('');
        }

        return true;
    }

    public Boolean evaluate()
    {
        Boolean response = true;
        for ( mmbrms_IRuleEvaluation ruleSegment : ruleSegementsToEvaluateList )
        {
            response = response && ruleSegment.evaluate();
        }
        return response;
    }

    /*
     *  Needed if the rule definition has a "requiresRecordToEvaluate" ruleInputHint
     */
    public mmbrms_RulesEngine setRecordToEvaluate( SObject recordToEvaluate )
    {
        this.recordToEvaluate = recordToEvaluate;
        return this;
    }
}