@isTest
private class Test_MatterInformationRequestsController {
    
    @testSetup static void setupRecords() {
        RecordType matterRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Matter__c' AND DeveloperName = 'Personal_Injury'];
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account' LIMIT 1];
        
        List<Account> acc = new List<account>{new Account(LastName = 'TestAcc 1', RecordTypeId = acctRcdType.Id)};
        Insert acc;

        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c(Name = 'Slip and Fall');
        insert caseType;

        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c(Name = 'Slip and Fall');
        matterPlan.litify_pm__Case_Type__c = caseType.Id;
        insert matterPlan;

        User testUser = createUser();
        insert testUser;

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = matterRcdType.Id;
        matter.ReferenceNumber__c = '999999056';
        matter.litify_pm__Client__c = acc[0].Id;
        matter.litify_pm__Principal_Attorney__c = testUser.Id;
        matter.litify_pm__Open_Date__c = Date.today();
        matter.litify_pm__Case_Type__c = caseType.Id;
        insert matter;

        RecordType roleRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Role__c' AND DeveloperName = 'Matter_Role'];
        litify_pm__Role__c role = new litify_pm__Role__c();
        role.RecordTypeId = roleRcdType.Id;
        role.litify_pm__Matter__c = matter.Id;
        role.litify_pm__Party__c = acc[0].Id;
        insert role;

        litify_pm__Request__c infoObj = new litify_pm__Request__c(Request__c = '911 Tapes', litify_pm__Matter__c = matter.Id);
        insert infoObj;
    }

    @isTest static void test_component_controller() {
        
        Test.startTest();
        // fetching required data
        litify_pm__Matter__c matterObj = [SELECT Id FROM litify_pm__Matter__c][0];      
        
        litify_pm__Request__c infoObj = [SELECT Id, Request__c FROM litify_pm__Request__c WHERE litify_pm__Matter__c =: matterObj.Id LIMIT 1];

        // fetchRelatedRole
        List<litify_pm__Role__c > roleList1 = MatterInformationRequestsController.fetchRelatedRole(matterObj.Id);
        System.assertEquals(1, roleList1.size());
        List<litify_pm__Role__c > roleList2 = MatterInformationRequestsController.fetchRelatedRole('');
        System.assertEquals(true, roleList2.isEmpty());

        //checkRecordsExist
        Boolean recordExist1 = MatterInformationRequestsController.checkRecordsExist(matterObj.Id);
        Boolean recordExist2 = MatterInformationRequestsController.checkRecordsExist('');
        System.assertEquals(true,recordExist1);
        System.assertEquals(false,recordExist2);


        //saveRecords
        MatterInformationRequestsController.saveRecords(new List<litify_pm__Request__c>{infoObj});
        try { MatterInformationRequestsController.saveRecords(null); }
        catch (Exception dmx) {}

        //fetchRecords   => Fetching matter related records
        List<litify_pm__Request__c> infoList1 = MatterInformationRequestsController.fetchRecords(matterObj.Id);
        
        try { List<litify_pm__Request__c> infoList2 = MatterInformationRequestsController.fetchRecords(''); }
        catch (Exception dmx) {}

        //fetchCreateRecords => fetching related record and creating missing records from CMT criteria
        List<litify_pm__Request__c> infoList3 = MatterInformationRequestsController.fetchCreateRecords(matterObj.Id);        
        try { List<litify_pm__Request__c> infoList4 = MatterInformationRequestsController.fetchCreateRecords(''); }
        catch (Exception dmx) {}  
        
        List<String> cmtList = MatterInformationRequestsController.fetchCMTRecords();
        Test.stopTest();
    }

    public static User createUser() {
        Integer userNum = 1;
        return new User(
            IsActive = true,
            FirstName = 'FirstName ' + userNum,
            LastName = 'LastName ' + userNum,
            //Username = 'apex.temp.test.user@sample.com.' + userNum,
            Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@xyz.com',
            Email = 'apex.temp.test.user@sample.com.' + userNum,
            Alias = 'alib' + userNum,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            Territory__c = null
        );
    }   
}