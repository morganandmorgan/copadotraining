/**
 * AuditFlagSelector
 * @description Selector for Audit Flag SObject.
 * @author Matt Terrill
 * @date 10/17/2019
 */
public without sharing class AuditFlagSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public AuditFlagSelector() {
        sObjectFields = new List<Schema.SObjectField> {
                Audit_Flag__c.id,
                Audit_Flag__c.audit_rule__c,
                Audit_Flag__c.user__c,

                Audit_Flag__c.litify_pm_Matter__c
        };
    } //constructor


    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Audit_Flag__c.SObjectType;
    } //getSObjectType


    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields


    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList


    public List<Audit_Flag__c> selectById(Set<Id> ids) {
        return (List<Audit_Flag__c>)selectSObjectsById(ids);
    } //selectById


    //selects flags based on a rule
    public List<Audit_Flag__c> selectByRuleId(Set<Id> ruleIds, Integer limitCount) {

        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('audit_rule__c  IN :ruleIds');
        query.setLimit(limitCount);

        return (List<Audit_Flag__c>)Database.query(query.toSOQL());
    } //selectByRuleId


    //selects flags based on user
    public List<Audit_Flag__c> selectByUserId(Set<Id> userIds) {

        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('user__c  IN :userIds');

        return (List<Audit_Flag__c>)Database.query(query.toSOQL());
    } //selectByUserId

} //class