@isTest
private class RelatedIntakesControllerTest {
	
	@TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < 3; ++i) {
            intakes.add(new Intake__c(
                    Incident__c = incident.Id,
                    Client__c = client.Id
                ));
        }
        toInsert.addAll((List<SObject>) intakes);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Intake__c> getIntakes() {
        return [SELECT Id FROM Intake__c];
    }
    
    @isTest
    private static void intakeNotFound() {
        Intake__c selectedIntake = new Intake__c();

        try {
            Test.startTest();
            RelatedIntakesController controller = new RelatedIntakesController(new ApexPages.StandardController(selectedIntake));
            Test.stopTest();

            System.assert(false);
        }
        catch (QueryException e) {
            System.assert(true);
        }
        catch (Exception e) {
            System.assert(false);
        }
    }

    @isTest
    private static void noRelatedIntakes() {
        List<Intake__c> intakes = getIntakes();
        
        Intake__c selectedIntake = intakes.get(0);

        Database.delete([SELECT Id FROM Intake__c WHERE Id != :selectedIntake.Id]);

        Test.startTest();
        RelatedIntakesController controller = new RelatedIntakesController(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assert(!controller.HasRelatedIntakes);
    }

    @isTest
    private static void hasRelatedIntakes() {
        List<Intake__c> intakes = getIntakes();
        
        Intake__c selectedIntake = intakes.get(0);

        Map<Id, Intake__c> relatedIntakes = new Map<Id, Intake__c>(intakes);
        relatedIntakes.remove(selectedIntake.Id);

        Test.startTest();
        RelatedIntakesController controller = new RelatedIntakesController(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assert(controller.HasRelatedIntakes);
        System.assertEquals(relatedIntakes.keySet(), new Map<Id, Intake__c>(controller.RelatedIntakes).keySet());
    }
}