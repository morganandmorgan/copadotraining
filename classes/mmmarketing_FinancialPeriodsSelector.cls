/**
 *  mmmarketing_FinancialPeriodsSelector
 */
public with sharing class mmmarketing_FinancialPeriodsSelector
    extends mmlib_SObjectSelector
    implements mmmarketing_IFinancialPeriodsSelector
{
    public static mmmarketing_IFinancialPeriodsSelector newInstance()
    {
        return (mmmarketing_IFinancialPeriodsSelector) mm_Application.Selector.newInstance(Marketing_Financial_Period__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Marketing_Financial_Period__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Marketing_Financial_Period__c.Ad_Cost__c,
                Marketing_Financial_Period__c.Campaign_AdWords_ID_Value__c,
                Marketing_Financial_Period__c.Campaign_Value__c,
                Marketing_Financial_Period__c.Clicks__c,
                Marketing_Financial_Period__c.Domain_Value__c,
                Marketing_Financial_Period__c.Financial_Period_Type__c,
                Marketing_Financial_Period__c.Keyword_Value__c,
                Marketing_Financial_Period__c.Marketing_Campaign__c,
                Marketing_Financial_Period__c.Marketing_Source__c,
                Marketing_Financial_Period__c.Medium_Value__c,
                Marketing_Financial_Period__c.Period_End_Date__c,
                Marketing_Financial_Period__c.Period_Start_Date__c,
                Marketing_Financial_Period__c.Source_Value__c
            };
    }

    public List<Marketing_Financial_Period__c> selectById(Set<Id> idSet)
    {
        return (List<Marketing_Financial_Period__c>) selectSObjectsById(idSet);
    }

    public List<Marketing_Financial_Period__c> selectWhereCampaignNotLinked()
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Financial_Period__c.Marketing_Campaign__c + ' = NULL ').setLimit( 5000 ).toSOQL() );
    }

    public List<Marketing_Financial_Period__c> selectOneWhereCampaignNotLinked()
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Financial_Period__c.Marketing_Campaign__c + ' = NULL ').setLimit( 1 ).toSOQL() );
    }

    public List<Marketing_Financial_Period__c> selectWhereSourceNotLinked()
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Financial_Period__c.Marketing_Source__c + ' = NULL ').setLimit( integer.valueOf(limits.getLimitQueryRows() * 0.25) ).toSOQL() );
    }

    public List<Marketing_Financial_Period__c> selectOneWhereSourceNotLinked()
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Financial_Period__c.Marketing_Source__c + ' = NULL ').setLimit( 1 ).toSOQL() );
    }

    public List<Marketing_Financial_Period__c> selectWhereMarketingSourceNotLinkedBySourceValue( Set<String> sourceValueSet )
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Financial_Period__c.Source_Value__c + ' in :sourceValueSet'
                                                              + ' AND ' + Marketing_Financial_Period__c.Marketing_Source__c + ' = NULL ')
                                                .setLimit( integer.valueOf( limits.getLimitQueryRows() * 0.9 ) )
                                                .toSOQL() );
    }

    public Database.QueryLocator selectQueryLocatorWhereMarketingCampaignNotLinked()
    {
        return Database.getQueryLocator( newQueryFactory().setCondition('(' + Marketing_Financial_Period__c.Campaign_Value__c + ' != null '
                                                                       + ' OR ' + Marketing_Financial_Period__c.Campaign_AdWords_ID_Value__c + ' != null )'
                                                                       + ' AND ' + Marketing_Financial_Period__c.Domain_Value__c + ' != NULL '
                                                                       + ' AND ' + Marketing_Financial_Period__c.Marketing_Campaign__c + ' = NULL ')
                                                          .toSOQL() );
    }

    public Database.QueryLocator selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue( Set<String> sourceValueSet )
    {
        return Database.getQueryLocator( newQueryFactory().setCondition(Marketing_Financial_Period__c.Source_Value__c + ' in :sourceValueSet'
                                                                       + ' AND ' + Marketing_Financial_Period__c.Marketing_Source__c + ' = NULL ')
                                                          .toSOQL() );
    }

    public List<Marketing_Financial_Period__c> selectByCompositeParam( mmmarketing_FinancialPeriodsSelectParam param )
    {
        Set<Date> dates = param.getDates();
        Set<String> sources = param.getSources();
        Set<String> keywords = param.getKeywords();
        Set<String> campaigns = param.getCampaigns();
        Set<String> domains = param.getDomains();

        Boolean isFirstConditionSet = false;

        String queryCondition = '';

        if ( ! dates.isEmpty() )
        {
            queryCondition += Marketing_Financial_Period__c.Period_Start_Date__c + ' in :dates';
            isFirstConditionSet = true;
        }

        if ( ! sources.isEmpty() )
        {
            queryCondition += (isFirstConditionSet ? ' AND ' : '') + Marketing_Financial_Period__c.Source_Value__c + ' in :sources';
            isFirstConditionSet = true;
        }

        if ( ! domains.isEmpty() )
        {
            queryCondition += (isFirstConditionSet ? ' AND ' : '') + Marketing_Financial_Period__c.Domain_Value__c + ' in :domains';
            isFirstConditionSet = true;
        }

        if ( ! campaigns.isEmpty() )
        {
            queryCondition += (isFirstConditionSet ? ' AND ' : '') + '(' + Marketing_Financial_Period__c.Campaign_Value__c + ' in :campaigns'
                                                                    +' OR ' + Marketing_Financial_Period__c.Campaign_AdWords_ID_Value__c + ' in :campaigns'
                                                                    +')';
            isFirstConditionSet = true;
        }

        if ( ! keywords.isEmpty() )
        {
            queryCondition += (isFirstConditionSet ? ' AND ' : '') + Marketing_Financial_Period__c.Keyword_Value__c + ' in :keywords';
        }

        system.debug( '\n\n\nqueryCondition == ' + queryCondition + '\n\n\n');

        // setup the QueryFactory
        fflib_QueryFactory qf = newQueryFactory();

        // reset the default ordering
        qf.getOrderings().clear();

        return Database.query( qf.setCondition( queryCondition )
                                                .setLimit( 25000 )
                                                .addOrdering( Marketing_Financial_Period__c.Period_Start_Date__c, fflib_QueryFactory.SortOrder.DESCENDING, true )
                                                .toSOQL() );
    }
}