public class QuestionnarieClass{

    public Questionnaire__c ques {get; set;}
    public String caseType {get; set;}
    public String redirectUrl{get; set;}
    Public String QuestionnaireFields {get;set;}
    Public String IntakeId {get;set;}
    Public String personRecordTypeId {get;set;}
    Public String businessRecordTypeId {get;set;}
    Public String lawsuitId{get;set;}
    Public string savehaserrors{get;set;}

    Public set<string> contactIdsOld;
    Public Map<id,Account> AccountMap;
    public List<Account> AccountInst;
    Public List<Account> AccountListToCreateQuestionnaire = New List<Account>();

    // Constructor
    public QuestionnarieClass(ApexPages.StandardController controller) {

        try{

            AccountInst = New List<account>();
            AccountMap = New Map<id,Account>();
            contactIdsOld = New set<string>();
            QuestionnaireFields = '';
            IntakeId = '';
            personRecordTypeId = '';
            businessRecordTypeId = '';
            lawsuitId = '';
            redirectUrl = '';
            savehaserrors = 'true';
            ques = new Questionnaire__c();
            String questionnarieId = string.valueof(controller.getRecord().id);

            if(questionnarieId != null){

                Map<String, Schema.SObjectField> maps = Schema.SObjectType.Questionnaire__c.fields.getMap();
                for(String fieldName : maps.keySet()) {

                    if(maps.get(fieldName).getDescribe().isAccessible()) {

                        if(QuestionnaireFields == '')
                            QuestionnaireFields = fieldName;
                        else
                            QuestionnaireFields = QuestionnaireFields + ',' + fieldName;
                    }
                }
                QuestionnaireFields += ',Client__r.Name,Client__r.Id,Lawsuits__r.Name,Lawsuits__r.Id,Intake__r.Id,Intake__r.Name ';
                QuestionnaireFields = 'select ' + QuestionnaireFields +' from Questionnaire__c where id = \''+ questionnarieId +'\'';
                ques = database.query(QuestionnaireFields);
            }
            else if(apexpages.currentpage().getparameters().get('PassedLawsuitId') != Null){

                lawsuitId = apexpages.currentpage().getparameters().get('PassedLawsuitId');
                ques.Lawsuits__c = lawsuitId;
            }

            if(apexpages.currentpage().getparameters().get('FromWhere') != Null && ques.Lawsuits__c != null){

                if(apexpages.currentpage().getparameters().get('FromWhere') == 'Lawsuit'){

                    List<Lawsuit__c> LwInst = [ SELECT Intake__r.id,Intake__r.Client__r.FirstName,
                                                    Intake__r.Client__r.LastName,Intake__r.Client__r.id,
                                                    Intake__r.Client__c,
                                                    Intake__r.Client__r.BillingStreet,Intake__r.Client__r.Billingpostalcode,
                                                    Intake__r.Client__r.BillingCity,Intake__r.Client__r.BillingState,
                                                    Intake__r.Client__r.BillingCountry,Intake__r.Client__r.ShippingStreet,
                                                    Intake__r.Client__r.Shippingpostalcode,Intake__r.Client__r.ShippingCity,
                                                    Intake__r.Client__r.ShippingState,Intake__r.Client__r.ShippingCountry,
                                                    Intake__r.Client__r.PersonMobilePhone,Intake__r.Client__r.Phone,
                                                    Intake__r.Client__r.Work_Phone__c,Intake__r.Client__r.PersonEmail,
                                                    Intake__r.Client__r.Fax,Intake__r.Client__r.PersonBirthdate,
                                                    Intake__r.Client__r.Social_Security_Number__c,Intake__r.Client__r.Marital_Status__c,
                                                    Intake__r.Caller_different_than_Injured_party__c,Intake__r.Injured_party_relationship_to_Caller__c,
                                                    Intake__r.Client__r.Maiden_Name__c,Intake__r.Client__r.Middle_Name__c,
                                                    Intake__r.Client__r.Other_Names_Used__c,Intake__r.Client__r.Mailing_Address_2__c,
                                                    Intake__r.Client__r.Physical_Address_2__c,Intake__r.Client__r.Date_of_Birth_mm__c,
                                                    Intake__r.Caller_First_Name__c,Intake__r.Caller_Last_Name__c
                                                    FROM Lawsuit__c
                                                    where id=:lawsuitId
                                                    limit 1];


                    if(LwInst[0].Intake__c != null){

                        ques.Intake__c = LwInst[0].Intake__r.id;
                        List<Account> AccountIst = [select id from Account where id=:LwInst[0].Intake__r.Client__r.id];
                        AccountMap.put(LwInst[0].Intake__r.Client__r.id,AccountIst[0]);
                        if(AccountIst != null){ques.Client__c = AccountIst[0].id;}
                        if(LwInst[0].Intake__r.Client__r.PersonMobilePhone != null){ques.Mobile_Phone__c = LwInst[0].Intake__r.Client__r.PersonMobilePhone;}
                        if(LwInst[0].Intake__r.Client__r.Phone != null){ques.Home_Phone__c = LwInst[0].Intake__r.Client__r.Phone;}
                        if(LwInst[0].Intake__r.Client__r.Work_Phone__c != null){ques.Work_Phone__c = LwInst[0].Intake__r.Client__r.Work_Phone__c;}
                        if(LwInst[0].Intake__r.Client__r.Fax != null){ques.Fax_Number__c = LwInst[0].Intake__r.Client__r.Fax;}
                        if(LwInst[0].Intake__r.Client__r.PersonEmail != null && LwInst[0].Intake__r.Client__r.PersonEmail != ''){ques.Email__c = LwInst[0].Intake__r.Client__r.PersonEmail;}
                        if(LwInst[0].Intake__r.Client__r.FirstName != null && LwInst[0].Intake__r.Client__r.FirstName != ''){ques.First_Name__c = LwInst[0].Intake__r.Client__r.FirstName;}
                        if(LwInst[0].Intake__r.Client__r.LastName != null && LwInst[0].Intake__r.Client__r.LastName != ''){ques.Last_Name__c = LwInst[0].Intake__r.Client__r.LastName;}
                        if(LwInst[0].Intake__r.Client__r.Middle_Name__c != null){ques.Middle_Name__c = LwInst[0].Intake__r.Client__r.Middle_Name__c;}
                        if(LwInst[0].Intake__r.Client__r.Maiden_Name__c != null){ques.Maiden_Name__c = LwInst[0].Intake__r.Client__r.Maiden_Name__c;}
                        if(LwInst[0].Intake__r.Client__r.Other_Names_Used__c != null){ques.Other_Names_Used__c  = LwInst[0].Intake__r.Client__r.Other_Names_Used__c;}
                        if(LwInst[0].Intake__r.Client__r.Physical_Address_2__c != null){ques.Physical_Address_2__c = LwInst[0].Intake__r.Client__r.Physical_Address_2__c;}
                        if(LwInst[0].Intake__r.Client__r.Mailing_Address_2__c != null){ques.Mailing_Address_2__c = LwInst[0].Intake__r.Client__r.Mailing_Address_2__c;}
                        if(LwInst[0].Intake__r.Client__r.ShippingStreet != null){ques.Mailing_Address_1__c = LwInst[0].Intake__r.Client__r.ShippingStreet;}
                        if(LwInst[0].Intake__r.Client__r.ShippingState != null && LwInst[0].Intake__r.Client__r.ShippingState != ''){ques.Mailing_State__c = LwInst[0].Intake__r.Client__r.ShippingState;}
                        if(LwInst[0].Intake__r.Client__r.ShippingCity != null && LwInst[0].Intake__r.Client__r.ShippingCity != ''){ques.Mailing_City__c = LwInst[0].Intake__r.Client__r.ShippingCity;}
                        if(LwInst[0].Intake__r.Client__r.ShippingCountry != null && LwInst[0].Intake__r.Client__r.ShippingCountry != ''){ques.Mailing_County__c = LwInst[0].Intake__r.Client__r.ShippingCountry;}
                        if(LwInst[0].Intake__r.Client__r.ShippingPostalCode != null){try{ques.Mailing_Zip_Code__c = Decimal.valueof(LwInst[0].Intake__r.Client__r.ShippingPostalCode);}catch(exception ex){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));}}
                        if(LwInst[0].Intake__r.Client__r.BillingStreet != null){ques.Physical_Address_1__c = LwInst[0].Intake__r.Client__r.BillingStreet;}
                        if(LwInst[0].Intake__r.Client__r.BillingCity != null){ques.Physical_City__c = LwInst[0].Intake__r.Client__r.BillingCity;}
                        if(LwInst[0].Intake__r.Client__r.BillingCountry != null){ques.Physical_County__c = LwInst[0].Intake__r.Client__r.BillingCountry;}
                        if(LwInst[0].Intake__r.Client__r.BillingState != null && LwInst[0].Intake__r.Client__r.BillingState != ''){ques.Physical_State__c = LwInst[0].Intake__r.Client__r.BillingState;}
                        if(LwInst[0].Intake__r.Client__r.Date_of_Birth_mm__c != null){ques.Date_of_Birth__c = LwInst[0].Intake__r.Client__r.Date_of_Birth_mm__c;}
                        if(LwInst[0].Intake__r.Caller_First_Name__c != null){ques.Caller_First_Name__c = LwInst[0].Intake__r.Caller_First_Name__c;}
                        if(LwInst[0].Intake__r.Caller_Last_Name__c != null){ques.Caller_Last_Name__c = LwInst[0].Intake__r.Caller_Last_Name__c;}
                        if(LwInst[0].Intake__r.Client__r.Social_Security_Number__c != null){ques.Social_Security__c = LwInst[0].Intake__r.Client__r.Social_Security_Number__c;}
                        if(LwInst[0].Intake__r.Client__r.BillingPostalCode != null){try{ques.Physical_Zip_Code__c = decimal.valueof(LwInst[0].Intake__r.Client__r.BillingPostalCode);}catch(exception ex){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));}}

                    }
                }
            }



            List<RecordType> recTypeList = [select id,ispersontype,DeveloperName from recordType where sobjectType='account'];
            for(RecordType temp:recTypeList){

                if(temp.ispersontype == true && temp.DeveloperName == 'Person_Account')
                    personRecordTypeId = temp.Id;
                else if(temp.ispersontype == false && temp.DeveloperName == 'Business_Account')
                    businessRecordTypeId = temp.id;
            }
            LawsuitSetFields();
            ques.Hidden_Case_Type__c = caseType;
            fillclientdetails();
        }
        catch(exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));
        }

    }


    /// Intake Id and Case Type are Set Here
    public void LawsuitSetFields(){

        caseType = '';
        IntakeId = '';
        List<Intake__c> IntakeList = [SELECT Case_Type__c,Id
                                        FROM Intake__c
                                        WHERE id =:ques.Intake__c
                                        LIMIT 1];
        if(IntakeList.size() > 0){

            IntakeId = IntakeList[0].Id;
            if(IntakeList[0].Case_Type__c != null){
                caseType = IntakeList[0].Case_Type__c;
            }
        }
        if(caseType == ''){

            List<Lawsuit__c> LawsuitList = [select Intake__r.Case_Type__c,Intake__r.Id
                                                   FROM Lawsuit__c
                                                   WHERE id =:ques.Lawsuits__c
                                                   LIMIT 1];
            if(LawsuitList.size() > 0 && LawsuitList[0].Intake__c != null){

                if(IntakeId == '')
                    IntakeId = LawsuitList[0].Intake__r.Id;
                if(LawsuitList[0].Intake__r.Case_Type__c != null)
                    caseType = LawsuitList[0].Intake__r.Case_Type__c;
            }
        }
    }
   /// Action Invoked on Save
    public Pagereference saveQuest(){

        try{
            List<Account> AccountUpdateList = New List<Account>();
            if(AccountMap.containsKey(ques.Client__c)){

                Account AccountClient = AccountMap.get(ques.Client__c);
                AccountClient.Phone = ques.Home_Phone__c;
                AccountClient.Work_Phone__c = ques.Work_Phone__c;
                AccountClient.PersonMobilePhone = ques.Mobile_Phone__c;
                AccountClient.Fax = ques.Fax_Number__c;
                AccountClient.PersonEmail = ques.Email__c;
                AccountClient.billingstreet = ques.Mailing_Address_1__c;
                AccountClient.BillingState = ques.Mailing_State__c;
                AccountClient.BillingCity = ques.Mailing_City__c;
                AccountClient.FirstName = ques.First_Name__c;
                AccountClient.LastName = ques.Last_Name__c;
                AccountClient.Date_of_Birth_mm__c  = ques.Date_of_Birth__c;
                AccountClient.Middle_Name__c = ques.Middle_Name__c ;
                AccountClient.Maiden_Name__c  = ques.Maiden_Name__c;
                AccountClient.Other_Names_Used__c = ques.Other_Names_Used__c;
                AccountClient.Physical_Address_2__c = ques.Physical_Address_2__c;
                AccountClient.Mailing_Address_2__c = ques.Mailing_Address_2__c;
                AccountClient.Social_Security_Number__c = ques.Social_Security__c;
                AccountClient.BillingPostalCode = String.valueof(ques.Mailing_Zip_Code__c);
                AccountUpdateList.add(AccountClient);
            }
            upsert ques;
            if(AccountUpdateList.size() > 0)
                upsert AccountUpdateList;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Successfully Updated'));
            redirectUrl = '/apex/QuestionnaireFormDetail?id='+ques.Id;
            savehaserrors = 'false';
            return null;

            /* PageReference pageRef;
            pageRef = New PageReference('/apex/QuestionnaireFormDetail?id='+ques.Id);
            pageRef.setRedirect(true);
            return pageRef;*/

        }catch(exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));
        }
        return null;
    }

    ////Onchange Function Of Client
    public void fillclientdetails(){

        try{

            AccountInst = New List<account>();
            AccountInst = [SELECT PersonEmail,PersonMobilePhone,Phone,Fax,Work_Phone__c,
                            PersonMailingAddress,ShippingStreet,BillingStreet,
                            BillingCountry,ShippingCity,ShippingCountry,ShippingState,
                            ShippingPostalCode,BillingPostalCode,BillingCity,BillingAddress,
                            BillingState,FirstName,LastName,Middle_Name__c,Maiden_Name__c,Date_of_Birth_mm__c,
                            Other_Names_Used__c,PersonContactId,RecordTypeId,Physical_Address_2__c,
                            Mailing_Address_2__c,Social_Security_Number__c
                            FROM account
                            WHERE id=:ques.Client__c limit 1];

            ques.Mobile_Phone__c = '';
            ques.Fax_Number__c = '';
            ques.Email__c = '';
            ques.Home_Phone__c = '';
            ques.Work_Phone__c = '';
            ques.First_Name__c = '';
            ques.Last_Name__c = '';
            ques.Middle_Name__c = '';
            ques.Maiden_Name__c = '';
            ques.Other_Names_Used__c = '';
            ques.Mailing_Address_1__c = '';
            ques.Physical_Address_1__c = '';
            ques.Mailing_Address_2__c = '';
            ques.Physical_Address_2__c = '';
            ques.Mailing_City__c = '';
            ques.Physical_City__c = '';
            ques.Mailing_State__c = '';
            ques.Physical_State__c = '';
            ques.Mailing_Zip_Code__c = null;
            ques.Physical_Zip_Code__c = null;
            ques.Mailing_County__c = '';
            ques.Physical_County__c = '';
            ques.Physical_Address_2__c = '';
            ques.Mailing_Address_2__c = '';
            ques.Social_Security__c = '';
            ques.Date_of_Birth__c = null;
            if(AccountInst.size() != 0) {

                AccountMap.put(AccountInst[0].Id,AccountInst[0]);
                if(AccountInst[0].Phone != null){ques.Home_Phone__c = AccountInst[0].Phone;}
                if(AccountInst[0].Work_Phone__c != null){ques.Work_Phone__c = AccountInst[0].Work_Phone__c;}
                if(AccountInst[0].PersonEmail != null && AccountInst[0].PersonEmail != ''){ques.Email__c = AccountInst[0].PersonEmail;}
                if(AccountInst[0].PersonMobilePhone != null){ques.Mobile_Phone__c = AccountInst[0].PersonMobilePhone;}
                if(AccountInst[0].Fax != null){ques.Fax_Number__c = AccountInst[0].Fax;}
                if(AccountInst[0].Social_Security_Number__c != null){ques.Social_Security__c = AccountInst[0].Social_Security_Number__c;}
                if(AccountInst[0].ShippingStreet != null){ques.Mailing_Address_1__c = AccountInst[0].ShippingStreet;}
                if(AccountInst[0].ShippingState != null && AccountInst[0].ShippingState != ''){ques.Mailing_State__c = AccountInst[0].ShippingState;}
                if(AccountInst[0].ShippingCity != null && AccountInst[0].ShippingCity != ''){ques.Mailing_City__c = AccountInst[0].ShippingCity;}
                if(AccountInst[0].ShippingCountry != null && AccountInst[0].ShippingCountry != ''){ques.Mailing_County__c = AccountInst[0].ShippingCountry;}
                if(AccountInst[0].FirstName != null && AccountInst[0].FirstName != ''){ques.First_Name__c = AccountInst[0].FirstName;}
                if(AccountInst[0].LastName != null && AccountInst[0].LastName != ''){ques.Last_Name__c = AccountInst[0].LastName;}
                if(AccountInst[0].Middle_Name__c != null && AccountInst[0].Middle_Name__c != ''){ques.Middle_Name__c = AccountInst[0].Middle_Name__c;}
                if(AccountInst[0].Maiden_Name__c != null && AccountInst[0].Maiden_Name__c != ''){ques.Maiden_Name__c = AccountInst[0].Maiden_Name__c;}
                if(AccountInst[0].Other_Names_Used__c != null && AccountInst[0].Other_Names_Used__c != ''){ques.Other_Names_Used__c = AccountInst[0].Other_Names_Used__c;}
                if(AccountInst[0].ShippingPostalCode != null){try{ques.Mailing_Zip_Code__c = Decimal.valueof(AccountInst[0].ShippingPostalCode);}catch(exception ex){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));}}
                if(AccountInst[0].Physical_Address_2__c != null){ques.Physical_Address_2__c = AccountInst[0].Physical_Address_2__c;}
                if(AccountInst[0].Mailing_Address_2__c != null){ques.Mailing_Address_2__c = AccountInst[0].Mailing_Address_2__c;}
                if(AccountInst[0].BillingAddress != null){ques.Physical_Address_1__c = AccountInst[0].BillingStreet;}
                if(AccountInst[0].BillingAddress != null){ques.Physical_Address_1__c = AccountInst[0].BillingStreet;}
                if(AccountInst[0].BillingCity != null){ques.Physical_City__c = AccountInst[0].BillingCity;}
                if(AccountInst[0].Date_of_Birth_mm__c != null){ques.Date_of_Birth__c = AccountInst[0].Date_of_Birth_mm__c;}
                if(AccountInst[0].BillingCountry != null){ques.Physical_County__c = AccountInst[0].BillingCountry;}
                if(AccountInst[0].BillingState != null && AccountInst[0].BillingState != ''){ques.Physical_State__c = AccountInst[0].BillingState;}
                if(AccountInst[0].BillingPostalCode != null){try{ques.Physical_Zip_Code__c = decimal.valueof(AccountInst[0].BillingPostalCode);}catch(exception ex){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));}}
            }
        }catch(exception ex){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,ex.getMessage()));
        }

    }

    // Additional Fields
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Questionnaire__c.FieldSets.New_Added_Fields.getFields();
    }

    ///Cancel Button Action
    public PageReference cancelCustom(){

        PageReference pageRef;
        if(apexpages.currentpage().getparameters().get('retURL') != Null){

            string navURL = apexpages.currentpage().getparameters().get('retURL');
            pageRef = New PageReference(navURL);
            pageRef.setRedirect(true);
            return pageRef;
        }
        pageRef = New PageReference('/'+lawsuitId);
        pageRef.setRedirect(true);
        return pageRef;
    }

    // Todo: Remove along with DateOfBirthCoverageTest.cls once real tests are created.
    @TestVisible
    private static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}