/**
 * SpringSingleUploadTest
 * @description Test class for SpringSingleUpload class.
 * @author Jeff Watson
 * @date 1/23/2019
 */
@IsTest
public with sharing class SpringSingleUploadTest {

    private static Account account;
    private static litify_pm__Matter__c matter;

    static {
        account = TestUtil.createAccount('Unit Test Account');
        insert account;

        matter = TestUtil.createMatter(account);
        insert matter;
    }

    @IsTest
    public static void SpringSingleUpload_Ctor_Test() {

        // Arrange
        ApexPages.StandardController standardController = new ApexPages.StandardController(matter);

        // Act
        Test.startTest();
        SpringSingleUpload springSingleUpload = new SpringSingleUpload(standardController);
        springSingleUpload.redirect();
        Test.stopTest();
    }
}