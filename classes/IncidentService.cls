public class IncidentService {
	
    public static void updateIncidentLabels(Set<Id> incidentIds) {
        if (!incidentIds.isEmpty()) {
            List<Incident__c> incidents = [
                SELECT
                    Incident_Label__c,
                    (SELECT Name, 
                            Client__r.Name
                        FROM Intakes__r
                        ORDER BY Client__r.Name ASC,
                            Name ASC)
                FROM
                    Incident__c
                WHERE
                    Id IN :incidentIds
            ];
            updateIncidentLabels(incidents);
        }
    }

    public static void updateIncidentLabelsForAccountDeletion(Set<Id> clientIds) {
        if (!clientIds.isEmpty()) {
            List<Incident__c> incidents = [
                SELECT
                    Incident_Label__c,
                    (SELECT Name, 
                            Client__r.Name
                        FROM Intakes__r
                        WHERE Client__c NOT IN :clientIds
                        ORDER BY Client__r.Name ASC,
                            Name ASC)
                FROM
                    Incident__c
                WHERE
                    Id IN (SELECT Incident__c FROM Intake__c WHERE Client__c IN :clientIds)
            ];
            updateIncidentLabels(incidents);
        }
    }

    public static void updateIncidentLabelsForAccountChange(Set<Id> clientIds) {
        if (!clientIds.isEmpty()) {
            List<Incident__c> incidents = [
                SELECT
                    Incident_Label__c,
                    (SELECT Name, 
                            Client__r.Name
                        FROM Intakes__r
                        ORDER BY Client__r.Name ASC,
                            Name ASC)
                FROM
                    Incident__c
                WHERE
                    Id IN (SELECT Incident__c FROM Intake__c WHERE Client__c IN :clientIds)
            ];
            updateIncidentLabels(incidents);
        }
    }

    private static void updateIncidentLabels(List<Incident__c> incidents) {
        List<Incident__c> toUpdate = new List<Incident__c>();

        for (Incident__c incident : incidents) {
            List<String> intakeDetails = new List<String>();
            List<Intake__c> intakes = incident.Intakes__r;
            for (Intake__c intake : intakes) {
                String intakeDetail = '';
                if (String.isNotBlank(intake.Client__r.Name)) {
                    intakeDetail += intake.Client__r.Name + ' - ';
                }
                intakeDetail += intake.Name;
                intakeDetails.add(intakeDetail);
            }
            String oldLabel = String.isBlank(incident.Incident_Label__c) ? '' : incident.Incident_Label__c;
            String newLabel = String.join(intakeDetails, ', ');

            if (oldLabel != newLabel) {
                incident.Incident_Label__c = newLabel;
                toUpdate.add(incident);
            }
        }

        Database.update(toUpdate);
    }
}