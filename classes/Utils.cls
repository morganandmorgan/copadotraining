/**
 * Utils
 * @description Shared operation utilities.
 * @author Jeff Watson
 * @date 7/26/2018
 */
public with sharing class Utils {

    public static void createDebugLog(Exception e) {

        if (e != null) {
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }
    }
}