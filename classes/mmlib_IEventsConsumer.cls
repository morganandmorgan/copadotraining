/**
 *  This is the base interface for a class to utilize in order to be
 *  utilized in the event consumer framework
 *  
 *  @see mmlib_PlatformEventsDistributor
 */
public interface mmlib_IEventsConsumer 
    extends Queueable
{
    void setEvents( list<Sobject> events );
}