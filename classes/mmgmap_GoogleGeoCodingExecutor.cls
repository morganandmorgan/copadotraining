public with sharing class mmgmap_GoogleGeoCodingExecutor
	implements
		mmlib_GenericBatch.IGenericExecuteBatch,
		Queueable
{
	private static List<String> addressGroupList = new List<String> {'PersonMailing', 'Billing', 'Shipping', 'PersonOther'};
	private list<Account> records = new list<Account>();

	public mmgmap_GoogleGeoCodingExecutor()
	{

	}

	public static Integer getNumberOfCalloutsPerRecord()
	{
		return addressGroupList.size();
	}

	// ============= mmlib_GenericBatch.IGenericExecuteBatch Implementation ================
	public void run(List<SObject> scope)
	{
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area start');
		if (scope == null || Account.sObjectType != scope.getSObjectType()) {
			throw new mmgmap_GoogleGeoCodingExceptions.ParameterException('The `scope` parameter must contain Accounts.');
		}
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 2');

		this.records = (List<Account>)scope;

		mmgmap_Address addr = null;

		for (Account acct : this.records)
		{

system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 2.5 -- acct: ' + acct);

			for (String addressGroup : addressGroupList)
			{

system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 3a -- addressGroup: ' + addressGroup);

				//Address addr = (Address) acct.get(addressGroup + 'Address');
				addr = new mmgmap_Address().setStreet((string)acct.get(addressGroup + 'Street'))
                                           .setCity((string)acct.get(addressGroup + 'City'))
                                           .setState((string)acct.get(addressGroup + 'State'))
                                           .setPostalCode((string)acct.get(addressGroup + 'PostalCode'))
                                           .setCountry((string)acct.get(addressGroup + 'Country'));

system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 3b -- addr: ' + addr);

				if (addr != null
					&& addr.isValidAddress())
				{
					try
					{
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 4a');
						Location loc = mmgmap_GoogleGeoCodingService.getLatitudeLongitudeForAddress( addr );

						acct.put(addressGroup + 'Latitude', loc.getLatitude());
						acct.put(addressGroup + 'Longitude', loc.getLongitude());
						acct.Last_Geocoding_Update__pc = datetime.now();
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 4b -- acct: ' + acct);
					}
					catch (mmgmap_GoogleGeoCodingExceptions.ParameterException e)
					{

						System.debug(LoggingLevel.WARN, 'Account Id, ' + acct.Id + ', has an invalid address.');
					}
					catch (mmgmap_GoogleGeoCodingExceptions.CalloutExecutionException e)
					{
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 6 -- CalloutExecutionException: ' + e);
						// Timeout exception.
						if (!isQueueable && !mmgmap_GoogleGeoCodingExceptions.DISABLED_SERVICE_MESSAGE.equals(e.getMessage())) {
							System.enqueueJob(new mmgmap_GoogleGeoCodingExecutor(new List<Account> {acct}));
						}
					}
				}
				else
				{
// TODO: Need to throw an exception here (or add one to the stack) which says that the address is not valid for geocoding.
				}

			}
		}

		if ( ! trigger.isExecuting )
		{
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 7');
			mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance(new mmlib_AtomicDML());
			uow.register(scope);
			uow.commitWork();
system.debug( 'mmgmap_GoogleGeoCodingExecutor.run area mark 8');
		}
	}

	// ============= Queueable Implementation ================
	private Boolean isQueueable = false;
	private List<Account> queueableAccountList = null;

	public mmgmap_GoogleGeoCodingExecutor(List<Account> accountList)
	{
		this.queueableAccountList = accountList;
	}

	public void execute(QueueableContext context)
	{
		isQueueable = true;
// TODO: Need to wrap this run call in a try/catch so as to capture a "bad address" exception
		run(queueableAccountList);
	}
}