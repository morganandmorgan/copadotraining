public with sharing class MatterDocsController {

    @AuraEnabled public static Model getModel(String matterId) {

        // Only fetch if a matterId is provided
        if(String.isEmpty(matterId)) return new Model();

        Model model = new Model();
        model.documents = [select Id, CreatedDate, Document_Name__c, Document_Link__c, Document_Type__c from MM_Document__c where Document_Link__c != null and Matter__c = :matterId order by CreatedDate desc limit 10];
        return model;
    }

    public class Model {

        @AuraEnabled List<MM_Document__c> documents { get; set; }

        public Model() {
            this.documents = new List<MM_Document__c>();
        }

        public Model(List<MM_Document__c> documents) {
            this.documents = documents;
        }
    }
}