/**
 *  mmmarketing_TrackingInformations
 */
public class mmmarketing_TrackingInformations extends fflib_SObjectDomain implements mmmarketing_ITrackingInformations {

    public static mmmarketing_ITrackingInformations newInstance(List<Marketing_Tracking_Info__c> records) {
        return (mmmarketing_ITrackingInformations) mm_Application.Domain.newInstance(records);
    }

    public static mmmarketing_ITrackingInformations newInstance(Set<Id> recordIds) {
        return (mmmarketing_ITrackingInformations) mm_Application.Domain.newInstance(recordIds);
    }

    public mmmarketing_TrackingInformations(List<Marketing_Tracking_Info__c> records) {
        super(records);
    }

    public override void onBeforeInsert() {
        autoLinkToMarketingInformationIfRequired();
    }

    public override void onAfterInsert() {
        /*List<User> adminUsers = [SELECT Id FROM User WHERE Profile.Name = 'M&M System Administrator Modified' AND IsActive = true];
        if(adminUsers != null && !adminUsers.isEmpty()) {
            System.runAs(adminUsers[0]) {
                Set<Id> marketingTrackingInfoIds = (new Map<Id, SObject>(records)).keySet();
                new mmmarketing_TrackingInfosServiceImpl().updateAllCallRelatedInformation(marketingTrackingInfoIds);
            }
        }*/
    }

    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        autoLinkToMarketingInformationIfRequired();
    }

    public void autoLinkToMarketingInformationIfRequired() {
        list<Marketing_Tracking_Info__c> qualifiedRecords = new mmmarketing_MTINotLinkedCriteria().setRecordsToEvaluate(records).run();
        new mmmarketing_LinkMTIToMrktngInfoAction().setRecordsToActOn(qualifiedRecords).run();
    }

    public void autoLinkToMarketingInformationIfRequired(mmlib_ISObjectUnitOfWork uow) {
        list<Marketing_Tracking_Info__c> qualifiedRecords = new mmmarketing_MTINotLinkedCriteria().setRecordsToEvaluate(this.records).run();
        mmmarketing_LinkMTIToMrktngInfoAction action = new mmmarketing_LinkMTIToMrktngInfoAction();
        action.setRecordsToActOn(qualifiedRecords);
        ((mmlib_IUnitOfWorkable) action).setUnitOfWork(uow);
        action.run();
    }

    public void autoLinkToMarketingInformationIfRequiredInQueueableMode() {
        list<Marketing_Tracking_Info__c> qualifiedRecords = new mmmarketing_MTINotLinkedCriteria().setRecordsToEvaluate(this.records).run();
        mmmarketing_LinkMTIToMrktngInfoAction action = new mmmarketing_LinkMTIToMrktngInfoAction();
        action.setActionToRunInQueue(true);
        action.setRecordsToActOn(qualifiedRecords).run();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new mmmarketing_TrackingInformations(sObjectList);
        }
    }

    public static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}