/**
 *  mmcommon_PersonAccountsService
 */
public class mmcommon_PersonAccountsService
{
    private static mmcommon_IPersonAccountsService service()
    {
        return (mmcommon_IPersonAccountsService) mm_Application.Service.newInstance( mmcommon_IPersonAccountsService.class );
    }

    public static List<Account> searchAccounts( String firstName, String lastName, String email, String phoneNum, Date birthDate )
    {
        return service().searchAccounts( firstName, lastName, email, phoneNum, birthDate );
    }

    public static List<Account> searchAccountsUsingHighPrecision( String firstName, String lastName, String email, String phoneNum, Date birthDate )
    {
        return service().searchAccountsUsingHighPrecision( firstName, lastName, email, phoneNum, birthDate );
    }

    public static List<Account> searchAccountsUsingFuzzyMatching(Map<String, String> data)
    {
        return service().searchAccountsUsingFuzzyMatching(data);
    }

    public static void saveRecords( list<Account> records )
    {
        service().saveRecords( records );
    }

    public static void geocodeAddresses(Set<Id> idSet)
    {
        service().geocodeAddresses(idSet);
    }
}