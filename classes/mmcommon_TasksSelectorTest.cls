/**
 * mmcommon_TasksSelectorTest
 * @description Test for Tasks Selector class.
 * @author Jeff Watson
 * @date 5/13/2019
 */
@IsTest
private class mmcommon_TasksSelectorTest {

    @IsTest
    private static void taskMySelectorTest() {
        Account a = TestUtil.createAccount('Testing 123');
        insert a;

        insert new List<Task>{
                TestUtil.createTask(a.Id, 'the subject', 'Completed', 'Normal', '', System.today()),
                TestUtil.createTask(null, 'the subject', System.today()),
                TestUtil.createTask(null, 'the subject', System.today().addDays(-1)),
                TestUtil.createTask(null, 'the subject', System.today().addDays(4))
        };

        Test.startTest();
        System.assert(mmcommon_TasksSelector.newInstance().selectMyTodayOpenTasks().size() == 1);
        System.assert(mmcommon_TasksSelector.newInstance().selectFutureOpenTasks(null).size() == 1);
        System.assert(mmcommon_TasksSelector.newInstance().selectMyOverdueTasks().size() == 1);
        System.assert(mmcommon_TasksSelector.newInstance().selectCompletedTasksByRelatedId(a.Id).size() == 1);
        Test.stopTest();
    }
}