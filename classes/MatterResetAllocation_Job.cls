/*==================================================================================
Name            : MatterResetAllocation_Job
Author          : CLD Partners
Created Date    : Aug 2018
Description     : Batch job that resets the matter's expense and deposit allocations / then re-runs the process
==================================================================================*/
global class MatterResetAllocation_Job implements Database.Batchable<sObject>{

	global Set<Id> matterIds;
	global Boolean doReset;
    global Boolean doAlloc;

	/*----------------------------------------------------------------------------
    * CONSTRUCTOR
    -----------------------------------------------------------------------------*/
	public MatterResetAllocation_Job(Set<Id> providedIds, Boolean providedReset, Boolean doAllocation) {
		matterIds = (providedIds != null && !providedIds.isEmpty()) ? providedIds : new Set<Id>();
		doReset = (providedReset != null) ? providedReset : false;
        doAlloc = (doAllocation != null) ? doAllocation : false;

	}
    
    /*----------------------------------------------------------------------------
    * START
    -----------------------------------------------------------------------------*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id FROM litify_pm__Matter__c WHERE Id in : matterIds';
        return Database.getQueryLocator(query);
    }

    /*----------------------------------------------------------------------------
    * EXECUTE
    -----------------------------------------------------------------------------*/
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<Id> matterIds = new Set<Id>();
        for(SObject s : scope){
            litify_pm__Matter__c matter = (litify_pm__Matter__c)s;
            matterIds.add(matter.Id); 
        }

        Matter_Service ms = new Matter_Service();
       	
       	//reset the allocations
       	if(doReset == true){
       		ms.resetDepositAllocations(matterIds);
       	}
       	//now allocate the negative expenses, then deposits
        if(doAlloc == true){
       		ms.autoAllocateAllItems(matterIds);
        }
    }

    /*----------------------------------------------------------------------------
    * FINISH
    -----------------------------------------------------------------------------*/
    global void finish(Database.BatchableContext BC) {
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id = :BC.getJobId()];

        // log Apex job details
        String apexJobDetails = '**** MatterResetAllocation_Job Finish()  status: ' + a.status + ' Total Jobs: ' + a.TotalJobItems + ' Errors: ' + a.NumberOfErrors;
        system.debug(apexJobDetails);
    }
}