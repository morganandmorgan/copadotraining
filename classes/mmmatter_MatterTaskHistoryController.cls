public with sharing class mmmatter_MatterTaskHistoryController {

    @AuraEnabled
    public static Model getRecs(String matterId, Integer count) {

        List<HistoryRec> historyRecs = new List<HistoryRec>();

        map<string, string> descriptionToLabel = new map<string, string>();
        for (Time_Slip_Code__mdt code : mmmatter_TimeSlipCodeSelector.newInstance().selectAll()) {
            if (!String.isEmpty(code.Description__c)) {
                descriptionToLabel.put(code.Description__c, code.MasterLabel);
            }
        }

        List<Task> tasks = mmcommon_TasksSelector.newInstance().selectCompletedTasksByRelatedId(matterId);
        for (Task task : tasks) {
            historyRecs.add(new HistoryRec(task, descriptionToLabel));
        }

        List<EmailMessage> emailMessages = [select Id, CreatedBy.Name, CreatedDate, MessageDate, RelatedToId, Subject, HtmlBody, TextBody from EmailMessage where RelatedToId = :matterId];
        for (EmailMessage emailMessage : emailMessages) {
            historyRecs.add(new HistoryRec(emailMessage));
        }

        return new Model(historyRecs, count, historyRecs.size());
    }

    public class Model {

        @AuraEnabled public List<HistoryRec> items;
        @AuraEnabled public Integer count { get {return this.items.size();} }
        @AuraEnabled public Integer totalCount { get; private set; }

        public Model(List<HistoryRec> historyRecs, Integer count, Integer totalCount) {
            this.items = new List<HistoryRec>();
            this.totalCount = totalCount;

            historyRecs.sort();

            if(count <= 0) {
                this.items.addAll(historyRecs);
            } else {
                Integer loopCount = 0;
                for(HistoryRec historyRec : historyRecs) {
                    if(loopCount < count) {
                        this.items.add(historyRec);
                        loopCount = loopCount + 1;
                    }
                }
            }
        }
    }

    public class HistoryRec implements Comparable {
        @AuraEnabled public String id;
        @AuraEnabled public Date activityDate;
        @AuraEnabled public Datetime completedDate;
        @AuraEnabled public String subject;
        @AuraEnabled public String description;
        @AuraEnabled public String activityPurpose;
        @AuraEnabled public String detail;
        @AuraEnabled public String assignedTo;
        @AuraEnabled public String timeKeeper;
        @AuraEnabled public String detailCode;
        @AuraEnabled public String type;
        @AuraEnabled public String detailHours;
        @AuraEnabled public String documentLink;
        @AuraEnabled public String documentTo;
        @AuraEnabled public String documentFrom;

        public HistoryRec(Task task, Map<String, String> descriptionToCode) {
            this.id = task.Id;
            this.activityDate = task.ActivityDate == null ? date.newinstance(task.CreatedDate.year(), task.CreatedDate.month(), task.CreatedDate.day()) : task.ActivityDate;
            this.completedDate = task.Completed_Date__c == null ? null : task.Completed_Date__c;
            this.subject = task.Subject;
            this.description = task.Description;
            this.activityPurpose = task.Activity_Purpose__c;
            this.detail = task.Detail__c;
            this.assignedTo = task.Owner.Name;
            this.timeKeeper = task.Timekeeper__c == null ? null : task.Timekeeper__c;
            this.detailCode = String.isEmpty(task.Detail__c) ? null : descriptionToCode.get(task.Detail__c);
            this.detailHours = String.valueOf(task.TimeSlip_Hours__c);
            this.type = (String.isNotEmpty(task.litify_pm__Document_Link__c)) ? 'Document' : task.Type;
            this.documentLink = task.litify_pm__Document_Link__c;
            this.documentTo = task.To__c;
            this.documentFrom = task.From__c;
        }

        public HistoryRec(EmailMessage emailMessage) {

            this.id = emailMessage.Id;
            this.assignedTo = emailMessage.CreatedBy.Name;
            this.completedDate = emailMessage.MessageDate;
            this.subject = emailMessage.Subject;
            this.description = emailMessage.TextBody;
            this.type = 'Email';
        }

        public Integer compareTo(Object compareTo)
        {
            HistoryRec historyRec = (HistoryRec) compareTo;
            if (completedDate == historyRec.completedDate) return 0;
            if (completedDate < historyRec.completedDate) return 1;
            return -1;
        }
    }
}