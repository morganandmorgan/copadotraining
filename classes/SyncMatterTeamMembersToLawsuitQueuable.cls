/**
 * SyncMatterTeamMembersToLawsuitQueuable
 * @description 
 * @author Jeff Watson
 * @date 1/12/2019
 */

public with sharing class SyncMatterTeamMembersToLawsuitQueuable implements Queueable {

    private List<SObject> records;
    private Map<Id, litify_pm__Matter_Team_Member__c> existingRecords = null;

    // Ctor
    public SyncMatterTeamMembersToLawsuitQueuable(List<SObject> records) {
        this.records = records;
    }

    public SyncMatterTeamMembersToLawsuitQueuable(List<SObject> records, Map<Id, litify_pm__Matter_Team_Member__c> existingRecords) {
        this.records = records;
        this.existingRecords = existingRecords;
    }

    // Queueable
    public void execute(QueueableContext ctx) {
        handleChangesForSocialSecurityMatters();
    }

    // Methods
    @TestVisible
    private void handleChangesForSocialSecurityMatters() {

        List<litify_pm__Matter_Team_Member__c> relevantRecords = getRelevantRecordsPerMatterRecordType();
        relevantRecords = getRelevantRecordsPerRoleName(relevantRecords, this.existingRecords);

        Set<Id> matterIdSet = new Set<Id>();
        for (SObject mtm : this.records) {
            matterIdSet.add(((litify_pm__Matter_Team_Member__c) mtm).litify_pm__Matter__c);
        }

        String payload = JSON.serialize(matterIdSet);
        mmlawsuit_LawsuitsService.updateTeamMemberAssignmentsFromMatter(matterIdSet);
    }

    private List<litify_pm__Matter_Team_Member__c> getRelevantRecordsPerMatterRecordType() {
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();
        List<litify_pm__Matter_Team_Member__c> mtmRecords = (!records.isEmpty()) ? (List<litify_pm__Matter_Team_Member__c>) records : new List<litify_pm__Matter_Team_Member__c>();

        for (litify_pm__Matter_Team_Member__c mtm : mtmRecords) {
            if (String.isNotEmpty(mtm.litify_pm__Matter__c)) matterMap.put(mtm.litify_pm__Matter__c, null);
        }

        matterMap = new Map<Id, litify_pm__Matter__c>(mmmatter_MattersSelector.newInstance().selectById(matterMap.keyset()));
        List<litify_pm__Matter_Team_Member__c> relevantRecords = new List<litify_pm__Matter_Team_Member__c>();

        for (litify_pm__Matter_Team_Member__c mtm : mtmRecords) {
            relevantRecords.add(mtm);
        }

        return relevantRecords;
    }

    @TestVisible
    private List<litify_pm__Matter_Team_Member__c> getRelevantRecordsPerRoleName(List<litify_pm__Matter_Team_Member__c> relevantRecords, Map<Id, litify_pm__Matter_Team_Member__c> existingRecords) {
        Set<String> relevantRoleNameSet = new Set<String>{'case developer', 'case manager', 'managing attorney', 'principal attorney', 'handling attorney', 'paralegal', 'litigation paralegal', 'assigned attorney'};
        Map<Id, String> roleNameMap = getTeamMemberRoleNameMap();

        List<litify_pm__Matter_Team_Member__c> relevantMTMRecords = new List<litify_pm__Matter_Team_Member__c>();

        for (SObject sobj : relevantRecords) {
            litify_pm__Matter_Team_Member__c mtm = (litify_pm__Matter_Team_Member__c) sobj;
            litify_pm__Matter_Team_Member__c old_mtm = (existingRecords != null) ? existingRecords.get(mtm.Id) : null;

            if (relevantRoleNameSet.contains(roleNameMap.get(mtm.litify_pm__Role__c))) {
                relevantMTMRecords.add(mtm);
            } else if (old_mtm != null && relevantRoleNameSet.contains(roleNameMap.get(old_mtm.litify_pm__Role__c))) {
                relevantMTMRecords.add(mtm);
            }
        }

        return relevantMTMRecords;
    }

    private Map<Id, String> teamRoleNameMap = null;
    private Map<Id, String> getTeamMemberRoleNameMap() {
        if (teamRoleNameMap == null) {
            teamRoleNameMap = new Map<Id, String>();

            for (litify_pm__Matter_Team_Role__c role : mmmatter_MatterTeamRolesSelector.newInstance().selectAll()) {
                teamRoleNameMap.put(role.Id, role.Name.toLowerCase());
            }
        }

        return teamRoleNameMap;
    }
}