@isTest
public  class PayableCreditNoteTest {
    
    @isTest
    public static void testPCN_Trigger_Domain_Service() {
        Map<c2g__codaPurchaseInvoice__c, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> testPIN_Map = TestDataFactory_FFA.payableInvoices_Expense;
        Map<c2g__codaPurchaseCreditNote__c, List<c2g__codaPurchaseCreditNoteExpLineItem__c>> testPCN_Map1 = TestDataFactory_FFA.createPayableCreditNotes_Expense(1, true);
        

        c2g__codaPurchaseCreditNote__c testPCN1;
        c2g__codaPurchaseCreditNote__c testPCN2;

        c2g__codaPurchaseInvoice__c pin;
        for(c2g__codaPurchaseInvoice__c p : testPIN_Map.keySet()){
            pin = p;
        }

        Test.startTest();
            PayableCreditNoteDomain domainTest = new PayableCreditNoteDomain();

            //update and insert first payable credit note            
            for(c2g__codaPurchaseCreditNote__c pcn : testPCN_Map1.keySet()){
                testPCN1 = pcn;
                testPCN1.c2g__AccountCreditNoteNumber__c = 'testin 1';
                testPCN1.c2g__PurchaseInvoice__c = pin.Id;
            }
            update testPCN1; 

            Map<c2g__codaPurchaseCreditNote__c, List<c2g__codaPurchaseCreditNoteExpLineItem__c>> testPCN_Map2 = TestDataFactory_FFA.createPayableCreditNotes_Expense(1, true);

            Try{
                //update and insert second payable credit note            
                for(c2g__codaPurchaseCreditNote__c pcn : testPCN_Map2.keySet()){
                    testPCN2 = pcn;
                    testPCN1.c2g__AccountCreditNoteNumber__c = 'testin 2';
                    testPCN2.c2g__PurchaseInvoice__c = pin.Id;
                }
                update testPCN2; 
            }
            Catch(Exception e){

            }
            

        Test.stopTest();
    }
}