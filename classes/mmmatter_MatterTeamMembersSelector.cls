public class mmmatter_MatterTeamMembersSelector
    extends mmlib_SObjectSelector
    implements mmmatter_IMatterTeamMembersSelector
{
    public static mmmatter_IMatterTeamMembersSelector newInstance()
    {
        return (mmmatter_IMatterTeamMembersSelector) mm_Application.Selector.newInstance(litify_pm__Matter_Team_Member__c.SObjectType);
    }

    public SObjectType getSObjectType()
    {
        return litify_pm__Matter_Team_Member__c.SObjectType;
    }

    public List<SObjectField> getAdditionalSObjectFieldList()
    {
        return
            new List<SObjectField>
            {
                litify_pm__Matter_Team_Member__c.litify_pm__Automatically_Created__c,
                litify_pm__Matter_Team_Member__c.litify_pm__Billable_Hourly_Rate__c,
                litify_pm__Matter_Team_Member__c.litify_pm__Default_Matter_Team__c,
                litify_pm__Matter_Team_Member__c.litify_pm__Matter__c,
                litify_pm__Matter_Team_Member__c.litify_pm__Role__c,
                litify_pm__Matter_Team_Member__c.litify_pm__User__c
            };
    }

    public List<litify_pm__Matter_Team_Member__c> selectById(Set<Id> idSet)
    {
        return (List<litify_pm__Matter_Team_Member__c>) selectSObjectsById(idSet);
    }

    public List<litify_pm__Matter_Team_Member__c> selectByMatter(Set<Id> idSet)
    {
        return
            (List<litify_pm__Matter_Team_Member__c>)
            Database.query(
                newQueryFactory()
                .setCondition(litify_pm__Matter_Team_Member__c.litify_pm__Matter__c + ' in :idSet')
                .toSOQL()
            );
    }

    public List<litify_pm__Matter_Team_Member__c> selectByUser(Set<Id> idSet)
    {
        return
            (List<litify_pm__Matter_Team_Member__c>)
            Database.query(
                newQueryFactory()
                .setCondition(litify_pm__Matter_Team_Member__c.litify_pm__User__c + ' in :idSet')
                .toSOQL()
            );
    }
}