@IsTest
public with sharing class TestDataFactory {

    private static Map<String, Profile> profilesByName {
        get {
            if (profilesByName == null) {
                profilesByName = new Map<String, Profile>();
                List<Profile> profs = [SELECT Id,Name FROM Profile];
                for (Profile prof : profs) {
                    profilesByName.put(prof.Name, prof);
                }
            }
            return profilesByName;
        }
        private set;
    }

    public static Map<string, UserRole> userRolesByName {
        get {
            if (userRolesByName == null) {
                userRolesByName = new Map<String, UserRole>();
                List<UserRole> userRoles = [select Id, Name from UserRole];
                for (UserRole userRole : userRoles) {
                    userRolesByName.put(userRole.Name, userRole);
                }
            }
            return userRolesByName;
        }
        private set;
    }

    public static Account createAccount() {
        Account account = new Account();
        account.Name = 'Test Account';
        return account;
    }

    public static Account createAccount(Id recordType) {
        Account account = new Account();
        account.RecordTypeId = recordType;
        account.Name = 'Test Account';
        return account;
    }

    public static Dice_Queue__c createDiceQueue() {
        Dice_Queue__c diceQueue = new Dice_Queue__c();
        return diceQueue;
    }

    public static Intake__c createIntake(String accountId) {
        Intake__c intake = new Intake__c();
        intake.Client__c = accountId;
        return intake;
    }

    public static litify_pm__Case_Type__c createCaseType(String casetypeName){
        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c(Name = casetypeName);
        return caseType;
    }
              

    public static litify_pm__Matter__c createMatter(String accountId) {
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = [select Id from RecordType where SObjectType = 'litify_pm__Matter__c' AND DeveloperName = 'Social_Security' limit 1][0].Id;
        matter.litify_pm__Client__c = accountId;
        matter.litify_pm__Status__c = 'Open';
        matter.ReferenceNumber__c = '999999056';
        matter.litify_pm__Open_Date__c = Date.today();
        matter.Initial_Claim_Status__c = 'Notice of Disapproved Claim';
        matter.Initial_Denial_Date__c = Date.today();
        return matter;
    }

    public static litify_pm__Expense__c createExpense(String matterId) {
        litify_pm__Expense__c expense = new litify_pm__Expense__c();
        expense.litify_pm__Matter__c = matterId;
        expense.litify_pm__Amount__c = 100;
        expense.litify_pm__Date__c = Date.newInstance(2018, 1, 1);
        return expense;
    }

    // Debug
}