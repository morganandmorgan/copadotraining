public class mmbrms_UserInProfilesSpecified
    implements mmbrms_IRuleEvaluation
{
    public List<String> profilesSpecifiedList = new List<String>();

    public Boolean isEvaluationResponseInverted;

    public Boolean evaluate()
    {
        if (isEvaluationResponseInverted == null)
        {
            isEvaluationResponseInverted =  false;
        }

        Boolean evalResponse = new mmcommon_UserInfo( UserInfo.getUserId() ).hasProfileName( profilesSpecifiedList );

        return isEvaluationResponseInverted ? ! evalResponse : evalResponse;
    }
}