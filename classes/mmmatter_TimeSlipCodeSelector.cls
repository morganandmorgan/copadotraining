public with sharing class mmmatter_TimeSlipCodeSelector
    extends mmlib_SObjectSelector
    implements mmmatter_ITimeSlipCodeSelector
{
    public static mmmatter_ITimeSlipCodeSelector newInstance()
    {
        return (mmmatter_ITimeSlipCodeSelector) mm_Application.Selector.newInstance(Time_Slip_Code__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Time_Slip_Code__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            Time_Slip_Code__mdt.Description__c
        };
    }

    public List<Time_Slip_Code__mdt> selectAll()
    {
        return Database.query( newQueryFactory().toSOQL() );
    }
}