public class mmlitifylrn_Constants
{
    private mmlitifylrn_Constants() { }

    public static final string LRN_API_ACCESS_TOKEN_KEY = 'X-User-Access-Token';
    public static final string LRN_API_REFRESH_TOKEN_KEY = 'X-User-Refresh-Token';

    public enum REFERRAL_SYNCSTATUS
    {
        THIRDPARTYDRAFT,
        THIRDPARTYNEW,
        THIRDPARTYSYNCED,
        THIRDPARTYFAILED
    }

    public static final string LRN_API_ERROR_MESSAGE_INVALID_TOKEN = 'INVALID_TOKEN';
    public static final string LRN_API_ERROR_MESSAGE_INVALID_REFRESH_TOKEN = 'INVALID_REFRESH_TOKEN';
}