public abstract class mmmarketing_UpdateCallInfoAbstract
        implements mmlib_GenericBatch.IGenericExecuteBatch {
    private static list<Type> CALL_SERVICE_TYPES = new list<Type>();

    static {
        CALL_SERVICE_TYPES.add(mmmarketing_UpdateCallInfoCTMLogic.class);
        CALL_SERVICE_TYPES.add(mmmarketing_UpdateCallInfoTwilioLogic.class);
    }

    protected mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance(new mmlib_AtomicDML());
    private list<mmmarketing_IUpdateCallInfoLogic> callServicesList = new list<mmmarketing_IUpdateCallInfoLogic>();
    private map<String, mmlib_NamedCredentialCalloutAuth> availableAuthorizationsbyIdentifierMap = new map<String, mmlib_NamedCredentialCalloutAuth>();
    private map<String, mmmarketing_IUpdateCallInfoLogic> callServicesByIdentifierMap = new map<String, mmmarketing_IUpdateCallInfoLogic>();
    private boolean isDebugOn = false;
    private boolean isCommitWorkEnabled = true;

    public mmmarketing_UpdateCallInfoAbstract() {
        for (Type callServiceType : CALL_SERVICE_TYPES) {
            callServicesList.add((mmmarketing_IUpdateCallInfoLogic) callServiceType.newInstance());
        }

        for (mmmarketing_IUpdateCallInfoLogic callService : callServicesList) {
            availableAuthorizationsbyIdentifierMap.putAll(callService.getAvailableAuthorizations());

            for (String identifier : callService.getAvailableAuthorizations().keyset()) {
                callServicesByIdentifierMap.put(identifier, callService);
            }
        }
    }

    public integer getAvailableAuthorizationsCount() {
        return this.availableAuthorizationsbyIdentifierMap.values().size();
    }

    public String isValid(Marketing_Tracking_Info__c record) {

        if (record.Tracking_Event_Timestamp__c == null) return 'Tracking_Event_Timestamp__c is null';
        if (string.isBlank(record.Calling_Phone_Number__c)) return 'Calling_Phone_Number__c is blank';
        if ((string.isNotBlank(record.Calling_Phone_Number__c) && record.Calling_Phone_Number__c.length() < 10)) return 'Calling_Phone_Number__c is the internal extension.  Possible transfer.';

        return 'Basic validation is OK.\n';
    }

    public void run(list<SObject> scope) {
        if (scope == null || Marketing_Tracking_Info__c.SObjectType != scope.getSobjectType()) {
            throw new ParameterException('The parameter for the run method must be a list of ' + Marketing_Tracking_Info__c.SObjectType.getDescribe().getLabel() + ' and cannot be null.');
        }

        if (!scope.isEmpty()) {
            set<id> recordIDSet = (new map<id, SObject>(scope)).keyset();

            // Get the MTI records that need to be processed
            list<Marketing_Tracking_Info__c> records = mmmarketing_TrackingInfosSelector.newInstance().selectByIdWithMarketingTrackingDetails(recordIDSet);
            mmlib_BaseCallout listCallsCallout = null;
            mmlib_BaseCallout.CalloutResponse listCallsCalloutResponse = null;
            string identifierThatFoundMatch = null;

            // for each MTI-Call record
            for (Marketing_Tracking_Info__c record : records) {
                identifierThatFoundMatch = null;

                // don't make the callout if we don't have all the criteria that we need on the record.
                if (record.Tracking_Event_Timestamp__c == null
                        || string.isBlank(record.Calling_Phone_Number__c)
                        || (string.isNotBlank(record.Calling_Phone_Number__c)
                        && record.Calling_Phone_Number__c.length() < 10
                )
                        ) {
                    continue;
                }

                // for each named credential available
                for (String identifier : availableAuthorizationsbyIdentifierMap.keySet()) {
                    listCallsCallout = callServicesByIdentifierMap.get(identifier).prepareCalloutToService(record, identifier);

                    listCallsCallout.execute();  // generic
                    listCallsCalloutResponse = listCallsCallout.getResponse();   // generic

                    if (listCallsCalloutResponse != null && listCallsCalloutResponse.getTotalNumberOfRecordsFound() > 0) {
                        // we found a match, continue
                        identifierThatFoundMatch = identifier;
                        break;
                    }
                }

                if (listCallsCalloutResponse != null) {
                    if (string.isNotBlank(identifierThatFoundMatch)) {
                        performUpdateIfMatch(callServicesByIdentifierMap.get(identifierThatFoundMatch), record, listCallsCalloutResponse, identifierThatFoundMatch);
                    }

                    if (String.isEmpty(record.Call_Info_Resolution_Status__c)) {
                        record.Call_Info_Resolution_Status__c = 'ProcessedCallNotResolved';
                        uow.registerDirty(record);
                    }
                }
            }

            if (this.isCommitWorkEnabled) {
                this.uow.commitWork();
            }
        }
    }

    public mmmarketing_UpdateCallInfoAbstract debug() {
        this.isDebugOn = true;
        return this;
    }

    public mmlib_ISObjectUnitOfWork getUow() {
        return this.uow;
    }

    public mmmarketing_UpdateCallInfoAbstract disableCommitWork() {
        this.isCommitWorkEnabled = false;
        return this;
    }

    public abstract void performUpdateIfMatch(mmmarketing_IUpdateCallInfoLogic updateLogic, Marketing_Tracking_Info__c record, mmlib_BaseCallout.CalloutResponse listCallsCalloutResponse, String identifierThatFoundMatch);

    public class ParameterException extends Exception {
    }
}