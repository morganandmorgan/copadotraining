/*============================================================================/
* PaymentSummary_Service
* @description Service class for Payment Summaries
* @author CLD
* @date 6/15/2019
=============================================================================*/
public without sharing class PaymentSummary_Service {
	public PaymentSummary_Service() {
		
	}

	/*-----------------------------------------------------------------------------
    Main Method to do other voiding functions such as void the expense and credit the existing PIN
    -----------------------------------------------------------------------------*/
	public void handlePaymentVoid (Set<Id> pmtSummaryIds){

		Set<Id> pinIds = new Set<Id>();

		List<litify_pm__Expense__c> expenseUpdateList = new List<litify_pm__Expense__c>();
		List<litify_pm__Expense__c> financeRequestList = new List<litify_pm__Expense__c>();
		List<c2g__codaPurchaseInvoice__c> pinList = new List<c2g__codaPurchaseInvoice__c>();

		Map<Id, c2g__codaPayment__c> pmtMap = new Map<Id, c2g__codaPayment__c>();
		Map<String, c2g__codaPurchaseCreditNote__c> pcnMap = new Map<String, c2g__codaPurchaseCreditNote__c>();

		//Payment_Selector pmtSelector = new Payment_Selector();
		List<c2g__codaPaymentAccountLineItem__c> pmtSummaryList = [
			SELECT Id,
				Name,
				c2g__Payment__c,
				c2g__Status__c,
				(SELECT Id,
					c2g__Transaction__c,
					c2g__TransactionLineItem__c,
					c2g__TransactionLineItem__r.c2g__Account__c,
					c2g__TransactionLineItem__r.c2g__OwnerCompany__c,
					c2g__TransactionLineItem__r.c2g__OwnerCompany__r.Name,
					c2g__TransactionLineItem__r.c2g__DocumentOutstandingValue__c,
					c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c,
					c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__PayableInvoice__c,
					c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__PayableInvoice__r.c2g__InvoiceDate__c,
					c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__PayableInvoice__r.Finance_Request__c,
					c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__PayableInvoice__r.Finance_Request__r.Name,
					c2g__Transaction__r.c2g__PayableInvoice__c,
					c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c,
					c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c,
					c2g__Transaction__r.c2g__PayableInvoice__r.c2g__InvoiceDate__c,
					c2g__Transaction__r.c2g__PayableInvoice__r.Finance_Request__c,
					c2g__Transaction__r.c2g__PayableInvoice__r.Finance_Request__r.Name
				FROM c2g__PaymentDetailLineItems__r)
			FROM c2g__codaPaymentAccountLineItem__c
			WHERE Id in : pmtSummaryIds
			AND Full_Payment_Void__c = true
		];

		//initial loop through payment summaries and their details
		for(c2g__codaPaymentAccountLineItem__c pmtSummary : pmtSummaryList){
			for(c2g__codaPaymentLineItem__c pmtLine : pmtSummary.c2g__PaymentDetailLineItems__r){
				
				//extract the PIN Ids for the subsequent expense query
				if(pmtLine.c2g__Transaction__r.c2g__PayableInvoice__c != null){
					pinList.add(pmtLine.c2g__Transaction__r.c2g__PayableInvoice__r);
					pinIds.add(pmtLine.c2g__Transaction__r.c2g__PayableInvoice__c);
				}
			}
		}

		for(litify_pm__Expense__c exp : [
			SELECT Id,
				Voided__c,
				litify_pm__Amount__c
			FROM litify_pm__Expense__c
			WHERE Payable_Invoice__c in :pinIds
		]){
			exp.Voided__c = true;
			exp.litify_pm__Amount__c = 0;
			expenseUpdateList.add(exp);
		}


		Savepoint sp1 = Database.setSavepoint();
		Try{
			//update the expenses
			update expenseUpdateList;

			//convert PINs to credit notes and post themn:
			for(c2g__codaPurchaseInvoice__c pin : pinList){
				String vendorCreditNumber = pin.c2g__AccountInvoiceNumber__c.length()>24 ? pin.c2g__AccountInvoiceNumber__c.subString(0,23) : pin.c2g__AccountInvoiceNumber__c;
				Id creditNoteId = c2g.ConvertToPurchaseCreditNoteService.convertToCreditNote(pin.Id, vendorCreditNumber);
				c2g.PurchaseCreditNotePostAndMatchService.postAndMatch(creditNoteId);
			}
		}
		Catch(Exception e){
			system.debug('ERROR - '+e.getMessage() + e.getStackTraceString());
			Database.rollback(sp1);
			throw e;
		}
	}
}