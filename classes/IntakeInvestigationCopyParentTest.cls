@isTest
private class IntakeInvestigationCopyParentTest {

    private static final Integer BULK_SIZE = 20;
	
    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Person');
        toInsert.add(client);

        List<Contact> attorneyContacts = new List<Contact>();
        for (Integer i = 0; i < 5; ++i) {
            attorneyContacts.add(TestUtil.createAssignableAttorney());
        }
        toInsert.addAll((List<SObject>) attorneyContacts);

        Id attorneyProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id;
        List<User> users = new List<User>();
        for (Integer i = 0; i < 6; ++i) {
            users.add(TestUtil.createUser(attorneyProfileId));
        }
        toInsert.addAll((List<SObject>) users);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < BULK_SIZE; ++i) {
            Intake__c intake = new Intake__c(
                    Client__c = client.Id,
                    Approving_Attorney_1__c = users.get(Math.mod(i, 6)).Id,
                    Approving_Attorney_2__c = users.get(Math.mod(i + 1, 6)).Id,
                    Approving_Attorney_3__c = users.get(Math.mod(i + 2, 6)).Id,
                    Approving_Attorney_4__c = users.get(Math.mod(i + 3, 6)).Id,
                    Approving_Attorney_5__c = users.get(Math.mod(i + 4, 6)).Id,
                    Approving_Attorney_6__c = users.get(Math.mod(i + 5, 6)).Id,
                    Assigned_Attorney__c = attorneyContacts.get(Math.mod(i, attorneyContacts.size())).Id,
                    New_Case_Email__c = i + '@test.com'
                );
            intakes.add(intake);
        }
        toInsert.addAll((List<SObject>) intakes);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Intake__c> getIntakes() {
        return [
            SELECT
                Id,
                Approving_Attorney_1__c,
                Approving_Attorney_2__c,
                Approving_Attorney_3__c,
                Approving_Attorney_4__c,
                Approving_Attorney_5__c,
                Approving_Attorney_6__c,
                Assigned_Attorney__c,
                New_Case_Email__c
            FROM
                Intake__c
        ];
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents(List<IntakeInvestigationEvent__c> intakeInvestigationEvents) {
        return [
            SELECT
                Id,
                Approving_Attorney_1__c,
                Approving_Attorney_2__c,
                Approving_Attorney_3__c,
                Approving_Attorney_4__c,
                Approving_Attorney_5__c,
                Approving_Attorney_6__c,
                Assigned_Attorney__c,
                Intake__c,
                Intake__r.Approving_Attorney_1__c,
                Intake__r.Approving_Attorney_2__c,
                Intake__r.Approving_Attorney_3__c,
                Intake__r.Approving_Attorney_4__c,
                Intake__r.Approving_Attorney_5__c,
                Intake__r.Approving_Attorney_6__c,
                Intake__r.Assigned_Attorney__c,
                Intake__r.New_Case_Email__c,
                New_Case_Email__c
            FROM
                IntakeInvestigationEvent__c
            WHERE
                Id IN :intakeInvestigationEvents
        ];
    }

    @isTest
    private static void beforeInsert_IntakesFound() {
        List<Intake__c> intakes = getIntakes();

        List<IntakeInvestigationEvent__c> newInvestigationEvents = new List<IntakeInvestigationEvent__c>();
        for (Integer i = 0; i < BULK_SIZE; ++i) {
            Intake__c intake = intakes.get(Math.mod(i, intakes.size()));
            newInvestigationEvents.add(new IntakeInvestigationEvent__c(
                    Intake__c = intake.Id,
                    EndDateTime__c = Datetime.now().addDays(2),
                    StartDateTime__c = Datetime.now().addDays(1)
                ));
        }

        Test.startTest();
        Database.insert(newInvestigationEvents);
        Test.stopTest();

        List<IntakeInvestigationEvent__c> results = getIntakeInvestigationEvents(newInvestigationEvents);
        for (IntakeInvestigationEvent__c result : results) {
            System.assertNotEquals(null, result.Intake__c);
            System.assertEquals(result.Intake__r.Approving_Attorney_1__c, result.Approving_Attorney_1__c);
            System.assertEquals(result.Intake__r.Approving_Attorney_2__c, result.Approving_Attorney_2__c);
            System.assertEquals(result.Intake__r.Approving_Attorney_3__c, result.Approving_Attorney_3__c);
            System.assertEquals(result.Intake__r.Approving_Attorney_4__c, result.Approving_Attorney_4__c);
            System.assertEquals(result.Intake__r.Approving_Attorney_5__c, result.Approving_Attorney_5__c);
            System.assertEquals(result.Intake__r.Approving_Attorney_6__c, result.Approving_Attorney_6__c);
            System.assertEquals(result.Intake__r.Assigned_Attorney__c, result.Assigned_Attorney__c);
            System.assertEquals(result.Intake__r.New_Case_Email__c, result.New_Case_Email__c);
        }
    }
}