@isTest
public with sharing class LivrSearchAccountTest {

	@testSetup static void setup() {
		List<Account> accountlist = new List<Account>();
		  Account personAccount = TestUtil.createPersonAccount('Unit', 'Test');
		  accountlist.add(TestUtil.createPersonAccount('Unit1', 'Test1'));
		  accountlist.add(TestUtil.createPersonAccount('Unit2', 'Test2'));
		  accountlist.add(TestUtil.createPersonAccount('Unit3', 'Test3'));
		  accountlist.add(TestUtil.createPersonAccount('Unit4', 'Test4'));
		  insert accountlist;
	}

	@IsTest
    private static void testLivrSearchAccount() {

    	List<id> accountIdList = new List<id>();
    	for(Account acc : [Select Id from account]){
    		accountIdList.add(acc.id);
    	}

    	String searchData = '{"id":"","dnis":"5577","birthDate":"5/16/1985","email":"r@gmail.com","lastName":"test","firstName":"testing","ssn":""}';
        LivrSearchAccount intakeController = new LivrSearchAccount();

        test.startTest();
		
		//LivrSearchAccount.insertNewAccountWithIntake(searchData);
		//LivrSearchAccount.insertNewIntakeWithExistAccount(accountIdList[0],searchData);
		//LivrSearchAccount.searchAccountRecord(accountIdList,searchData);
    	test.stopTest();
    }

	@IsTest
	private static void coverage() {
		new LivrSearchAccount().coverage();
	}
}