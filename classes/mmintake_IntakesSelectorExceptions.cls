public class mmintake_IntakesSelectorExceptions 
{
    private mmintake_IntakesSelectorExceptions() { }
    
    public virtual class BaseException extends Exception { }
    public class IncorrectFieldSetSObjectTypeExpectedException extends mmintake_IntakesSelectorExceptions.BaseException { }
}