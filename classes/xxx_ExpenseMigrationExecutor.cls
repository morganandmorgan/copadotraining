/**
 *  script used to manage the cost based expense migration from x_CPExpenseStaging__c to litify_pm__Expense__c records related to litify_pm__Matter__c records
 *
 *  @usage String query = '<<query goes here>>';
 *         new mmlib_GenericBatch( xxx_ExpenseMigrationExecutor.class, query).setBatchSizeTo(100).execute();
 */
public class xxx_ExpenseMigrationExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    private static final id morganAndMorganDisabilitiesAssociatesRecordId;
    private static final id cpDataMigrationUserId;

    private static map<string, Account> morganBusinessByStagingMap;

    private static map<String, x_CPTrancodeToLitifyExpTypeMap__c> expenseTypeMap = new map<String, x_CPTrancodeToLitifyExpTypeMap__c>();

    private map<String, SObject> usersByEmailMap = new map<String, SObject>();

    private map<String, SObject> mattersByLitifyLoadIdMap = new map<String, SObject>();

    static
    {
        /*
            Atlanta         -- MMB-03 -- Morgan & Morgan Atlanta PLLC
            FortMeyers      -- MMB-05 -- Morgan & Morgan Fort Myers PLLC
            Jacksonville    -- MMB-04 -- Morgan & Morgan Jacksonville PLLC
            Lexington       -- MMB-07 -- Morgan & Morgan Kentucky PLLC
            Memphis         -- MMB-06 -- Morgan & Morgan Memphis PLLC
            Nation          -- MMB-14 -- The Nation Law Firm LLP
            Orlando         -- MMB-01 -- Morgan & Morgan Orlando P.A.
            Tampa           -- MMB-02 -- Morgan & Morgan Tampa P.A.

            MMB-101 - Morgan & Morgan Disability Advocates LLC
        */

        cpDataMigrationUserId = [select id, name, username from user where username like 'cpdatamigration@forthepeople.com%' limit 1].id;

        list<x_CPTrancodeToLitifyExpTypeMap__c> allTrancodeToExpTypeMappings = [select Id, Name, OfficeName__c, trancode_sk__c
                                                                                     , ExternalIdCode__c, ExpenseType__c, Expense_Type_Description__c
                                                                                  from x_CPTrancodeToLitifyExpTypeMap__c
                                                                                 order by OfficeName__c, trancode_sk__c];

        // organize this list of mappings by a key of "OfficeName__c + ';' + trancode_sk__c"
        for (x_CPTrancodeToLitifyExpTypeMap__c trancodeToExpTypeMap : allTrancodeToExpTypeMappings )
        {
            expenseTypeMap.put( trancodeToExpTypeMap.OfficeName__c.toLowercase() + ';' + trancodeToExpTypeMap.trancode_sk__c.toLowercase(), trancodeToExpTypeMap);
        }

        // find All Morgan Businesses And Organize
        morganBusinessByStagingMap = new map<string, Account>();

        list<Account> morganBusinesses = [select id, name, external_id__c, recordtypeid
                                            from account
                                           where recordtype.developername = 'Morgan_Morgan_Businesses'];

        for ( Account morganBusiness : morganBusinesses )
        {
            // find the social security business
            if ( 'MMB-101'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganAndMorganDisabilitiesAssociatesRecordId = morganBusiness.id;
            }

            if ( 'MMB-03'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'atlanta', morganBusiness );
            }

            if ( 'MMB-05'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'fortmeyers', morganBusiness );
            }

            if ( 'MMB-04'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'jacksonville', morganBusiness );
            }

            if ( 'MMB-07'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'lexington', morganBusiness );
            }

            if ( 'MMB-06'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'memphis', morganBusiness );
            }

            if ( 'MMB-14'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'nation', morganBusiness );
            }

            if ( 'MMB-01'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'orlando', morganBusiness );
            }

            if ( 'MMB-02'.equalsIgnoreCase( morganBusiness.external_id__c ) )
            {
                morganBusinessByStagingMap.put( 'tampa', morganBusiness );
            }

        }
    }

    public void run( list<SObject> scope )
    {
        x_CPExpenseStaging__c workingRecord = null;
        ExpensePairing aPairing = null;

        list<litify_pm__Expense__c> expenseRecordsToInsert = new list<litify_pm__Expense__c>();
        list<litify_pm__Expense__c> expenseRecordsToUpdate = new list<litify_pm__Expense__c>();
        map<id, x_CPExpenseStaging__c> stagingRecordsToUpdateIdMap = new map<id, x_CPExpenseStaging__c>();
        list<ExpensePairing> thePairings = new list<ExpensePairing>();
        set<id> existingLitifyExpenseIdSet = new set<id>();
        Set<String> matterLitifyLoadIdSet = new Set<String>();
        Set<String> stagingExpenseCreateUserEmailSet = new Set<String>();
        map<id, litify_pm__Expense__c> litifyExpenseByIdMap = new map<id, litify_pm__Expense__c>();

        // organize the scope records into the workingRecordsByRelatedMatterAndTransMap
        for ( SObject record : scope )
        {
system.debug( 'mark B1' );
            workingRecord = (x_CPExpenseStaging__c)record;

            matterLitifyLoadIdSet.add( workingRecord.RelatedMatterLitifyLoadId__c );

            if ( string.isNotBlank( workingRecord.create_user_email__c ) )
            {
                stagingExpenseCreateUserEmailSet.add( workingRecord.create_user_email__c );
            }

            // determine if there are litify_pm__Expense__c records already setup for these staging records
            if ( workingRecord.RelatedExpense__c != null )
            {
                existingLitifyExpenseIdSet.add( workingRecord.RelatedExpense__c );
            }
        }
system.debug( 'mark C1' );
system.debug( 'matterLitifyLoadIdSet size == ' + matterLitifyLoadIdSet.size() );
system.debug( 'stagingExpenseCreateUserEmailSet size == ' + stagingExpenseCreateUserEmailSet.size() );
system.debug( 'existingLitifyExpenseIdSet size == ' + existingLitifyExpenseIdSet.size() );
        findMattersByLitifyLoadIds( matterLitifyLoadIdSet );

        findUsersByStagingExpenseCreateUserEmail( stagingExpenseCreateUserEmailSet );

        if ( ! existingLitifyExpenseIdSet.isEmpty() )
        {
            litifyExpenseByIdMap.putAll( mmmatter_ExpensesSelector.newInstance().selectById( existingLitifyExpenseIdSet ) );
        }

        for ( SObject record : scope )
        {
            workingRecord = (x_CPExpenseStaging__c)record;
system.debug( 'mark D1');
system.debug( 'working x_CPExpenseStaging__c record id = ' + workingRecord.id);
            aPairing = new ExpensePairing().setStagingRecord( workingRecord )
                    .setExpenseRecord( litifyExpenseByIdMap.get( workingRecord.RelatedExpense__c ) )
                    .setMattersMap( this.mattersByLitifyLoadIdMap )
                    .setUsersByEmailMap( this.usersByEmailMap )
                    .processStagingRecord();

            if ( aPairing.isValidPairing() )
            {
                if ( aPairing.expenseRecord.id == null )
                {
                    expenseRecordsToInsert.add( aPairing.expenseRecord );
                }
                else
                {
                    expenseRecordsToUpdate.add( aPairing.expenseRecord );
                }
            }

            thePairings.add( aPairing );
        }
system.debug( 'mark E1');
        // make saves
        database.insert(expenseRecordsToInsert, false);
system.debug( 'mark F1');
        // link all expense records to staging records via aPairing.linkExpenseRecordToStagingRecord() method
        for (ExpensePairing pairing : thePairings)
        {
            //system.debug( 'pairing.getStagingRecord().id == ' + pairing.getStagingRecord().id);
            pairing.linkExpenseRecordToStagingRecord();

            stagingRecordsToUpdateIdMap.put( pairing.getStagingRecord().id, pairing.getStagingRecord() );
        }

        // save staging records
        list<Database.SaveResult> srList = database.update( stagingRecordsToUpdateIdMap.values(), false );
        //system.debug( srList );
    }

    private void findMattersByLitifyLoadIds(Set<String> matterLitifyLoadIdSet)
    {
        list<litify_pm__Matter__c> matters = [select id, name, referencenumber__c, CP_Case_Number__c, litify_load_id__c
                                                from litify_pm__Matter__c
                                               where litify_load_id__c = :matterLitifyLoadIdSet];

        this.mattersByLitifyLoadIdMap = generateSObjectMapByUniqueField( matters, litify_pm__Matter__c.litify_load_id__c );
        system.debug( this.mattersByLitifyLoadIdMap.keyset() );
    }

    private void findUsersByStagingExpenseCreateUserEmail( Set<String> stagingExpenseCreateUserEmailSet )
    {
        if ( ! stagingExpenseCreateUserEmailSet.isempty() )
        {
            String workingQuery = 'select id, name, username, email, FederationIdentifier, IsActive from User';

            boolean isFirstCriteriaElement = true;

            for ( String stagingExpenseCreateUserEmail : stagingExpenseCreateUserEmailSet )
            {
                if ( stagingExpenseCreateUserEmail != null )
                {
                    if ( isFirstCriteriaElement )
                    {
                        workingQuery += ' where';
                        isFirstCriteriaElement = false;
                    }
                    else
                    {
                        workingQuery += ' or';
                    }

                    workingQuery += ' (' + User.Username + ' like \'' + stagingExpenseCreateUserEmail + '%\''
                                +' or ' + User.Email + ' like \'' + stagingExpenseCreateUserEmail + '%\''
                                +' )';
                }
            }

            this.usersByEmailMap = generateSObjectMapByUniqueField( Database.query( workingQuery ), User.Email  );
            system.debug( this.usersByEmailMap.keyset() );
        }
        else
        {
            this.usersByEmailMap = new map<String, SObject>();
        }
    }

    private map<String, SObject> generateSObjectMapByUniqueField( final List<SObject> sObjects
                                                                , final Schema.SObjectField stringFieldToBeKey)
    {
        map<String, SObject> outputMap = new map<String, SObject>();

        String stringValue = null;

        if (sObjects != null && ! sObjects.isEmpty())
        {
            for (SObject sobj : sObjects)
            {
                stringValue = (String)sobj.get(stringFieldToBeKey);

                if ( stringValue.containsIgnoreCase('=forthepeople.com@example.com') )
                {
                    stringValue = stringValue.substringBefore('=forthepeople.com@example.com') + '@forthepeople.com';
                }

                if ( ! outputMap.containsKey( stringValue.toLowercase() ) )
                {
                    outputMap.put( stringValue.toLowercase(), sobj );
                }
            }
        }

        return outputMap;
    }


    public class ExpensePairing
    {
        private litify_pm__Expense__c expenseRecord;
        private x_CPExpenseStaging__c stagingRecord;
        private boolean isPairingValid = true;
        private map<String, SObject> mattersByLitifyLoadIdMap;
        private map<String, SObject> usersByEmailMap;

        public ExpensePairing()
        {
            system.debug( 'new ExpensePairing created ');
        }

        public ExpensePairing setExpenseRecord( litify_pm__Expense__c expenseRecord )
        {
            this.expenseRecord = expenseRecord;

            return this;
        }

        public ExpensePairing setStagingRecord( x_CPExpenseStaging__c stagingRecord )
        {
            this.stagingRecord = stagingRecord;

            return this;
        }

        public ExpensePairing setMattersMap( map<String, SObject> mattersByLitifyLoadIdMap )
        {
            this.mattersByLitifyLoadIdMap = mattersByLitifyLoadIdMap;

            return this;
        }

        public ExpensePairing setUsersByEmailMap( map<String, SObject> usersByEmailMap )
        {
            this.usersByEmailMap = usersByEmailMap;

            return this;
        }

        public litify_pm__Expense__c getExpenseRecord()
        {
            return this.expenseRecord;
        }

        public x_CPExpenseStaging__c getStagingRecord()
        {
            return this.stagingRecord;
        }

        public ExpensePairing processStagingRecord()
        {
            system.debug( 'EP1' );
            if ( this.stagingRecord.RelatedExpense__c != null
                && this.expenseRecord != null )
            {
system.debug( 'EP1A' );
                updateExpenseRecordDataFromStagingRecord();
            }
            else
            {
system.debug( 'EP1B' );
                generateExpenseRecordFromStagingRecord();
            }

            return this;
        }

        public boolean isValidPairing()
        {
            if ( this.expenseRecord.litify_pm__Matter__c == null )
            {
                recordValidationFailure('NORELATEDMATTER', 'Related Matter record was not found');
            }

            if ( this.expenseRecord.RequestedBy__c == null )
            {
                recordValidationFailure('NOACTIVEEMPLOYEE', 'Unable to find active employee with this email address found in create_user_email__c.');
            }

            if ( this.expenseRecord.litify_pm__ExpenseType2__c == null )
            {
                recordValidationFailure('NORELATEDEXPENSETYPE', 'Failed to resolve match to available expense types.');
            }

            if ( this.expenseRecord.PayableTo__c == null )
            {
                recordValidationFailure('NORELATEDMORGANBUSINESS', 'Failed to resolve which Morgan Business should be listed on the "payable to" field.');
            }

            return this.isPairingValid;
        }

        public void linkExpenseRecordToStagingRecord()
        {
            if ( this.isPairingValid )
            {
                this.stagingRecord.RelatedExpense__c = this.expenseRecord.id;
                this.stagingRecord.MigrationStatus__c = 'Successful';
                this.stagingRecord.MigratedTime__c = datetime.now();
            }
        }

        private void recordValidationFailure(String statusCode, String reason)
        {
            this.stagingRecord.MigrationStatus__c = 'Failed = ' + statusCode;
            this.stagingRecord.ProcessingAnomalyComments__c = (string.isBlank(this.stagingRecord.ProcessingAnomalyComments__c) ? '' : this.stagingRecord.ProcessingAnomalyComments__c ) + reason + '\n';
            this.isPairingValid = false;
        }

        private void updateExpenseRecordDataFromStagingRecord()
        {
            this.expenseRecord.litify_pm__Amount__c = this.stagingRecord.amount__c;
            this.expenseRecord.litify_pm__Date__c = date.valueof( this.stagingRecord.create_date__c );
            this.expenseRecord.Timestamp__c = datetime.valueof( this.stagingRecord.create_date__c );
            this.expenseRecord.Litify_Load_Id__c = this.stagingRecord.SfdcLoadId__c;
            this.expenseRecord.litify_pm__Note__c = this.stagingRecord.comment__c;

            if ( String.isNotBlank(this.stagingRecord.banktran_sk__c) )
            {
                this.expenseRecord.litify_pm__Note__c += '\n\nThis record was a deposit received in ClientProfiles system prior to migration to Litify';
            }

            if ( expenseTypeMap.containsKey( this.stagingRecord.OfficeName__c.toLowercase() + ';' + this.stagingRecord.trancode_sk__c.toLowercase() ) )
            {
                this.expenseRecord.litify_pm__ExpenseType2__c = expenseTypeMap.get( this.stagingRecord.OfficeName__c.toLowercase() + ';' + this.stagingRecord.trancode_sk__c.toLowercase() ).ExpenseType__c;
            }
            else
            {
                this.expenseRecord.litify_pm__ExpenseType2__c = null;
            }

            if ( morganBusinessByStagingMap.containsKey( this.stagingRecord.OfficeName__c.toLowercase() ) )
            {
                this.expenseRecord.PayableTo__c = morganBusinessByStagingMap.get( this.stagingRecord.OfficeName__c.toLowercase() ).id;
            }
            else
            {
                this.expenseRecord.PayableTo__c = null;
            }

            system.debug( 'this.stagingRecord.create_user_email__c == ' + this.stagingRecord.create_user_email__c );
            if ( String.isNotBlank( this.stagingRecord.create_user_email__c ) )
            {
                system.debug( 'this.usersByEmailMap.containsKey( this.stagingRecord.create_user_email__c.toLowercase() )  == ' + this.usersByEmailMap.containsKey( this.stagingRecord.create_user_email__c.toLowercase() )  );
            }
            system.debug( 'this.stagingRecord.create_user_id__c == ' + this.stagingRecord.create_user_id__c);

            if ( String.isNotBlank( this.stagingRecord.create_user_email__c )
                && this.usersByEmailMap.containsKey( this.stagingRecord.create_user_email__c.toLowercase() ) )
            {
                system.debug( 'EP-updateExpenseRecordDataFromStagingRecord-3');
                this.expenseRecord.RequestedBy__c = this.usersByEmailMap.get( this.stagingRecord.create_user_email__c.toLowercase() ).id;
            }
            // handle special CP users CPYTRK and CNV_PJW differently
            else if ( 'CPYTRK'.equalsIgnoreCase( this.stagingRecord.create_user_id__c )
                    || 'CNV_PJW'.equalsIgnoreCase( this.stagingRecord.create_user_id__c )
                    || 'PP_PJW'.equalsIgnoreCase( this.stagingRecord.create_user_id__c )
                    || 'ADMIN'.equalsIgnoreCase( this.stagingRecord.create_user_id__c )
                    )
            {
                system.debug( 'EP-updateExpenseRecordDataFromStagingRecord-4');
                this.expenseRecord.RequestedBy__c = cpDataMigrationUserId;
            }
            else
            {
                system.debug( 'EP-updateExpenseRecordDataFromStagingRecord-5');
                this.expenseRecord.RequestedBy__c = null;
            }
        }

        private void generateExpenseRecordFromStagingRecord()
        {
            this.expenseRecord = new litify_pm__Expense__c();

            this.expenseRecord.litify_pm__Status__c = 'UnPaid';
            this.expenseRecord.ParentBusiness__c = morganAndMorganDisabilitiesAssociatesRecordId; // default to disability associates group;
            this.expenseRecord.Create_Payable_Invoice__c = false;

            if ( this.mattersByLitifyLoadIdMap.containsKey( stagingRecord.RelatedMatterLitifyLoadId__c.toLowercase() ) )
            {
                this.expenseRecord.litify_pm__Matter__c = this.mattersByLitifyLoadIdMap.get( stagingRecord.RelatedMatterLitifyLoadId__c.toLowercase() ).id;
            }

            updateExpenseRecordDataFromStagingRecord();
        }
    }
}