public with sharing class mmcommon_TasksSelector
        extends mmlib_SObjectSelector
        implements mmcommon_ITasksSelector {

    public static mmcommon_ITasksSelector newInstance() {
        return (mmcommon_ITasksSelector) mm_Application.Selector.newInstance(Task.SObjectType);
    }

    private Schema.sObjectType getSObjectType() {
        return Task.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Task.Activity_Purpose__c,
                Task.Additional_Documents__c,
                Task.Cisco_Queue__c,
                Task.Completed_Date__c,
                Task.Contacted__c,
                Task.Contracts_to_be_Signed__c,
                Task.CP_Posted_Date__c,
                Task.Detail__c,
                Task.DiceQueueName__c,
                Task.Disposition__c,
                Task.From__c,
                Task.litify_pm__Document_Link__c,
                Task.Incoming_Call__c,
                Task.Intake_Created_From_Task__c,
                Task.Investigation_Status__c,
                Task.Investigator_Notes__c,
                Task.Mail_Out_Status__c,
                Task.Manual_Call_Disposition__c,
                Task.OB_Call_Hour__c,
                Task.OB_Call_Time__c,
                Task.Outbound_Dialed_Number__c,
                Task.Outbound_Dialed_Number_Clean__c,
                Task.Reason_for_Delayed_Signup__c,
                Task.Skill__c,
                Task.Timekeeper__c,
                Task.TimeSlip_Hours__c,
                Task.To__c,
                Task.To_Be_Canceled__c,
                Task.To_Be_Rescheduled__c
        };
    }

    public List<Task> selectMyTodayOpenTasks() {
        Id userId = UserInfo.getUserId();
        return Database.query(newQueryFactory().setCondition(Task.OwnerId + ' = :userId and ' + Task.ActivityDate + ' = TODAY and ' + Task.IsClosed + ' = false').toSOQL());
    }

    public List<Task> selectFutureOpenTasks(Integer numDays) {
        if (numDays == null) numDays = 7;
        Id userId = UserInfo.getUserId();
        return Database.query(newQueryFactory().setCondition(Task.OwnerId + ' = :userId and ' + Task.ActivityDate + ' > TODAY and ' + Task.ActivityDate + ' = NEXT_N_DAYS:' + numDays + ' and ' + Task.IsClosed + ' = false').toSOQL());
    }

    public List<Task> selectMyOverdueTasks() {
        Id userId = UserInfo.getUserId();
        return Database.query(newQueryFactory().setCondition(Task.OwnerId + ' = :userId and ' + Task.ActivityDate + ' < TODAY and ' + Task.IsClosed + ' = false').toSOQL());
    }

    public List<Task> selectCompletedTasksByRelatedId(Id relatedId) {
        set<string> ADDITIONAL_FIELDS_SET = new set<String>{
                'Owner.Name', 'CreatedBy.Name'
        };

        fflib_QueryFactory qs = newQueryFactory();
        qs.getOrderings().clear();

        return Database.query(qs.setCondition(Task.Completed_Date__c + ' <> null and ' + Task.WhatId + ' = :relatedId and ' + Task.IsDeleted + ' = false and ' + Task.IsClosed + ' = true').
                selectFields(ADDITIONAL_FIELDS_SET).
                addOrdering(Task.Completed_Date__c, fflib_QueryFactory.SortOrder.DESCENDING, false).
                addOrdering(Task.ActivityDate, fflib_QueryFactory.SortOrder.DESCENDING, false).
                addOrdering(Task.Subject, fflib_QueryFactory.SortOrder.ASCENDING, false).
                toSoql() + ' all rows');
    }

    public List<Task> selectOpenTasksByRelatedIdSet(Set<Id> relatedIdSet) {
        return
                Database.query(
                        newQueryFactory()
                                .setCondition(Task.WhatId + ' in :relatedIdSet and ' + Task.IsClosed + ' = false')
                                .addOrdering(Task.ReminderDateTime, fflib_QueryFactory.SortOrder.ASCENDING, false)
                                // TODO: Too many SOQL Queries
                                //.setLimit(20000)
                                .toSoql());
    }
}