public class mmattyprtl_IntakeSignUpService
{
    private static mmattyprtl_IIntakeSignUpService service()
    {
        return (mmattyprtl_IIntakeSignUpService) mm_Application.Service.newInstance( mmattyprtl_IIntakeSignUpService.class );
    }

    /**
     *  Service method to handle the creation of the intake sign up request.
     *
     *  @param fieldParameterMap which is a map of the specific Schema.sObjectField and their associated values.
     *  @param screening option needed. Provided as mmattyprtl_Enums.SCREENING_OPTIONS enum type
     *  @param list of additional email addresses to notify outside of the generating attorney and the person creating the email
     *  @return the Intake__c record created
     *  @throws mmattyprtl_IntakeSignUpServiceException
     */
    public static Intake__c processSignUpRequest( map<Schema.sObjectField, string> fieldParameterMap
                                         , mmattyprtl_Enums.SCREENING_OPTIONS screeningOption
                                         , set<string> additionalEmailAddressesToNotifyList)
    {
        return service().processSignUpRequest( fieldParameterMap, screeningOption, additionalEmailAddressesToNotifyList );
    }
}