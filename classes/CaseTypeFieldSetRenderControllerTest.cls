@isTest
private class CaseTypeFieldSetRenderControllerTest {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        Intake__c intake = new Intake__c(
                Client__c = client.Id,
                Incident__c = incident.Id
            );
        toInsert.add(intake);

        Database.insert(toInsert);
        toInsert.clear();

        Task commentTask = new Task(
                WhatId = intake.Id,
                Subject = 'Intake Comment'
            );
        toInsert.add(commentTask);

        Event investigationEvent = new Event(
                WhatId = intake.Id,
                Subject = 'Test Subject',
                StartDateTime = Datetime.now(),
                EndDateTime = Datetime.now().addHours(1),
                IsAllDayEvent = false
            );
        toInsert.add(investigationEvent);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Intake__c getIntake() {
        return [SELECT Id, Client__c FROM Intake__c];
    }

    private static Event getEvent() {
        return [SELECT Id FROM Event];
    }

    private static Task getTask() {
        return [SELECT Id FROM Task];
    }

    private static CaseTypeFieldSetRenderController buildController(Intake__c relatedIntake) {
        CaseTypeFieldSetRenderController controller = new CaseTypeFieldSetRenderController();
        controller.relatedIntakeId = relatedIntake.Id;
        return controller;
    }

    private static CaseTypeFieldSetRenderController buildController(Event relatedEvent) {
        CaseTypeFieldSetRenderController controller = new CaseTypeFieldSetRenderController();
        controller.relatedEventId = relatedEvent.Id;
        return controller;
    }

    @isTest
    private static void initRelatedIntake() {
        Intake__c relatedIntake = getIntake();
        Task callTask = getTask();

        Test.startTest();
        CaseTypeFieldSetRenderController controller = buildController(relatedIntake);
        Test.stopTest();

        System.assertEquals(relatedIntake.Id, controller.thisIntake.Id);
        System.assertEquals(null, controller.thisEvent);
        System.assertEquals(null, controller.eventFieldSet);
        System.assert(String.isNotBlank(controller.intakeFieldSet));
        System.assertEquals(relatedIntake.Client__c, controller.thisAccount.Id);
        System.assertEquals(callTask.Id, controller.thisCallComment.Id);
    }

    @isTest
    private static void initRelatedEvent() {
        Event relatedEvent = getEvent();
        Intake__c relatedIntake = getIntake();

        Test.startTest();
        CaseTypeFieldSetRenderController controller = buildController(relatedEvent);
        Test.stopTest();

        System.assertEquals(relatedIntake.Id, controller.thisIntake.Id);
        System.assertEquals(relatedEvent.Id, controller.thisEvent.Id);
        System.assert(String.isNotBlank(controller.eventFieldSet));
        System.assertEquals(null, controller.intakeFieldSet);
        System.assertEquals(relatedIntake.Client__c, controller.thisAccount.Id);
        System.assertEquals(null, controller.thisCallComment);
    }
}