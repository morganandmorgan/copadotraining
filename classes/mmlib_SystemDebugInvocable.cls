public class mmlib_SystemDebugInvocable
{
    @InvocableMethod( label='Debug Log Message'
                      description='Put a message in the Apex System Debug Log')
    public static list<string> writeDebugMessage( list<String> messages )
    {
        system.debug( messages );

        if ( messages == null )
        {
            system.debug( 'mmlib_SystemDebugInvocable called with message of null.');
        }
        else
        {
            system.debug( string.join(messages, ',') );
        }

        return new list<String>();
    }
}