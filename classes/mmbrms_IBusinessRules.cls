public interface mmbrms_IBusinessRules
{
	String getDeveloperName();
	String getRuleDefinition();
}