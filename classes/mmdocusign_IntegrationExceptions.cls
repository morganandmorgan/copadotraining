public class mmdocusign_IntegrationExceptions
{
	private mmdocusign_IntegrationExceptions()
	{
		// Hide from consumer.
	}

	public abstract class IntegrationBaseException
		extends Exception
	{
		// Abstract class.
	}

	public class CalloutException
		extends IntegrationBaseException
	{
		// No code.
	}

	public class ConfigurationException
		extends IntegrationBaseException
	{
		// No code.
	}

	public class ParameterException
		extends IntegrationBaseException
	{
		// No code.
	}
}