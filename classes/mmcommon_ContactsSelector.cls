/**
 *  mmcommon_ContactsSelector
 */
public with sharing class mmcommon_ContactsSelector
    extends mmlib_SObjectSelector
    implements mmcommon_IContactsSelector
{
    public static mmcommon_IContactsSelector newInstance()
    {
        return (mmcommon_IContactsSelector) mm_Application.Selector.newInstance(Contact.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Contact.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
        };
    }

    public List<Contact> selectById(Set<Id> idSet)
    {
        return (List<Contact>) selectSObjectsById(idSet);
    }

    public List<Contact> selectByEmail(Set<String> emailSet)
    {
        return
            (List<Contact>)
            Database.query(
                newQueryFactory()
                    .setCondition(Contact.Email + ' in :emailSet')
                    .toSOQL());
    }

    public List<Contact> selectAllMorganAndMorganAttorneys()
    {
        // setup the QueryFactory
        fflib_QueryFactory qf = newQueryFactory();

        // reset the default ordering
        qf.getOrderings().clear();

        return Database.query( qf.setCondition( Contact.Reveal_In_Attorney_s_Portal__c + ' = TRUE'
                                                + ' AND Account.RecordType.DeveloperName = \'Morgan_Morgan_Businesses\' ')
                                 .addOrdering( Contact.LastName, fflib_QueryFactory.SortOrder.ASCENDING, true )
                                 .addOrdering( Contact.FirstName, fflib_QueryFactory.SortOrder.ASCENDING, true )
                                 .toSOQL() );
    }
}