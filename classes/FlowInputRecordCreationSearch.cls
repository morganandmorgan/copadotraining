public with sharing class FlowInputRecordCreationSearch {

    public static Map<String, String> lightningFieldMap = new Map<String, String> {
        'address' => 'text',
        'id' => 'text',
        'date' => 'date',
        'double' => 'number',
        'percent' => 'number',
        'phone' => 'tel',
        'textarea' => 'text',
        'string' => 'text',
        'url' => 'url',
        'integer' => 'number',
        'currency' => 'number',
        'datetime' => 'datetime',
        'boolean' => 'boolean',
        'reference' => 'text',
        'email' => 'email',
        'picklist' => 'text',
        'multipicklist' => 'text'
    };

    @AuraEnabled
    public static String retrieveRecordSearchData(String objectName, String fieldSetName) {
        try {
            RecordSearchData recordSearchDataWrapper = new RecordSearchData();
            List<FieldSetWrapper> fieldWrapperList = new List<FieldSetWrapper>();
            Map<String, Schema.SObjectType> mapSObjectDescribe = Schema.getGlobalDescribe();
            Map<String, SObjectField> mapObjectFields = mapSObjectDescribe.get(objectName).getDescribe().fields.getmap();
            for(String mapKey : mapObjectFields.keySet()) {
                Schema.DescribeFieldResult fieldDescribe = mapObjectFields.get(mapKey).getDescribe();
                if(fieldDescribe.isNameField()) {
                    recordSearchDataWrapper.nameFieldAPIName = fieldDescribe.getName();
                    break;
                }
            }
            for(Schema.FieldSetMember fieldMember : mapSObjectDescribe.get(objectName).getDescribe().fieldSets.getmap().get(fieldSetName).getFields()) {
                Schema.DescribeFieldResult fieldDescribe = mapObjectFields.get(fieldMember.getFieldPath()).getDescribe();
                FieldSetWrapper fieldWrapper = new FieldSetWrapper();
                fieldWrapper.fieldLabel = fieldMember.getLabel();
                fieldWrapper.fieldApiName = fieldMember.getFieldPath();
                fieldWrapper.isAutoNumber = fieldDescribe.isAutoNumber();
                fieldWrapper.isCalculated = fieldDescribe.isCalculated();
                fieldWrapper.dataType = String.valueOf(fieldMember.getType()).toLowerCase();
                fieldWrapper.lightningDataType = lightningFieldMap.get(String.valueOf(fieldMember.getType()).toLowerCase());
                if(fieldWrapper.dataType == 'reference') {
                    List<Schema.sObjectType> referencePointingList = fieldDescribe.getReferenceTo();
                    Schema.sObjectType targetObject = referencePointingList[0];
                    Schema.DescribeSObjectResult objectDescribe = targetObject.getDescribe();
                    Map<String, Schema.sObjectField> fieldsMap = objectDescribe.fields.getMap();
                    for(String mapKey : fieldsMap.keySet()) {
                        Schema.DescribeFieldResult targetFieldDescribe = fieldsMap.get(mapKey).getDescribe();
                        if(targetFieldDescribe.isNameField() && targetFieldDescribe.getName() == 'Name' || targetFieldDescribe.getName() == 'Number') {
                            fieldWrapper.relationshipPath = fieldDescribe.getRelationshipName() + '.' + targetFieldDescribe.getName();
                            break;
                        }
                    }
                }
                fieldWrapperList.add(fieldWrapper);
            }

            recordSearchDataWrapper.searchFieldSetData = fieldWrapperList;
            return JSON.serialize(recordSearchDataWrapper);
        } catch(Exception ex) {
            System.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled
    public static List<FieldSetWrapper> retieveFieldSet(String objectName, String fieldSetName) {
        try {
            List<FieldSetWrapper> fieldWrapperList = new List<FieldSetWrapper>();
            Map<String,Schema.SObjectType> mapSObjectDescribe = Schema.getGlobalDescribe();
            Map<String, SObjectField> mapObjectFields = mapSObjectDescribe.get(objectName).getDescribe().fields.getmap();
            for(Schema.FieldSetMember fieldMember : mapSObjectDescribe.get(objectName).getDescribe().fieldSets.getmap().get(fieldSetName).getFields()) {
                FieldSetWrapper fieldWrapper = new FieldSetWrapper();
                fieldWrapper.fieldLabel = fieldMember.getLabel();
                fieldWrapper.fieldApiName = fieldMember.getFieldPath();
                fieldWrapper.isAutoNumber = mapObjectFields.get(fieldMember.getFieldPath()).getDescribe().isAutoNumber();
                fieldWrapper.dataType = String.valueOf(fieldMember.getType()).toLowerCase();
                fieldWrapperList.add(fieldWrapper);
            }
            return fieldWrapperList;
        } catch(Exception ex) {
            System.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled
    public static List<SobjectResultDataWrapper> searchedRecords(String objectName, String resultFieldSetName, String whereClause) {
        try {
            System.debug(whereClause);
            SObjectType sobjType = Schema.getGlobalDescribe().get(objectName);
            Map<String, Schema.SObjectField> sobjFieldMap = sobjType.getDescribe().fields.getMap();
            Map<String, String> dataTypeMap = new Map<String, String>();
            List<SobjectResultDataWrapper> objectResultWrapperList = new List<SobjectResultDataWrapper>();
            String query = '';
            String limitStr = ' LIMIT 10000';
            Map<String, ReferenceFieldWrapper> referenceFieldMap = new Map<String, ReferenceFieldWrapper>();
            List<FlowInputRecordCreationSearch.FieldSetWrapper> fieldWrapperList = retieveFieldSet(objectName, resultFieldSetName);
            Map<String, String> fieldApiNameLabelMap = new Map<String, String>();
            Set<String> notVisibleFields = new Set<String>();
            for(FlowInputRecordCreationSearch.FieldSetWrapper fldwrap : fieldWrapperList) {
                if(String.valueOf(sobjFieldMap.get(fldwrap.fieldApiName).getDescribe().getType()) == 'REFERENCE') {
                    Schema.DescribeFieldResult fieldDescribe = sobjFieldMap.get(fldwrap.fieldApiName).getDescribe();
                    List<Schema.sObjectType> referencePointingList = fieldDescribe.getReferenceTo();
                    Schema.sObjectType targetObject = referencePointingList[0];
                    Schema.DescribeSObjectResult objectDescribe = targetObject.getDescribe();
                    Map<String, Schema.sObjectField> fieldsMap = objectDescribe.fields.getMap();
                    for(String mapKey : fieldsMap.keySet()) {
                        Schema.DescribeFieldResult targetFieldDescribe = fieldsMap.get(mapKey).getDescribe();
                        if(targetFieldDescribe.isNameField() && targetFieldDescribe.getName() == 'Name' || targetFieldDescribe.getName() == 'Number') {
                            fieldApiNameLabelMap.put(fieldDescribe.getRelationshipName() + '.' + targetFieldDescribe.getName(), fieldDescribe.getLabel());
                            referenceFieldMap.put(fieldDescribe.getRelationshipName() + '.' + targetFieldDescribe.getName(), new ReferenceFieldWrapper(fieldDescribe.getRelationshipName(), targetFieldDescribe.getName()));
                            notVisibleFields.add(fldwrap.fieldApiName);
                        }
                    }
                }
                fieldApiNameLabelMap.put(fldwrap.fieldApiName, fldwrap.fieldLabel);
                dataTypeMap.put(fldwrap.fieldApiName, String.valueOf(sobjFieldMap.get(fldwrap.fieldApiName).getDescribe().getType()));
            }
            System.debug(dataTypeMap);
            
            if(fieldApiNameLabelMap.size() > 0){
                query = 'SELECT ' + String.join(new List<String>(fieldApiNameLabelMap.keySet()), ',') + ' FROM '+ objectName;
            }
            System.debug(whereClause);
            if(String.isNotBlank(whereClause) && String.isNotBlank(query)){
                System.debug(whereClause.startsWithIgnoreCase('Order'));
                if(whereClause.startsWithIgnoreCase(' Order')){
                    System.debug(whereClause);
                    query = query +' '+ whereClause;
                }else{
                    query = query +' WHERE '+ whereClause;
                }
            }
            query += limitStr;
            System.debug(query);
            if(String.isNotBlank(query)) {
                System.debug(referenceFieldMap);
                List<sObject> sObjectList = Database.query(query);
                if(sObjectList.size() > 0) {
                    SobjectResultDataWrapper objectResultWrapper = new SobjectResultDataWrapper();
                    List<RecordData> headerList = new List<RecordData>();
                    for(String fieldKey : fieldApiNameLabelMap.keySet()) {
                        System.debug(fieldKey);
                        if(fieldKey.contains('.')){
                            headerList.add(new RecordData(fieldKey, fieldApiNameLabelMap.get(fieldKey), !notVisibleFields.contains(fieldKey), false));
                        }else{
                            headerList.add(new RecordData(fieldKey, fieldApiNameLabelMap.get(fieldKey), !notVisibleFields.contains(fieldKey), sobjFieldMap.get(fieldKey).getDescribe().isSortable()));
                        }
                    }
                    objectResultWrapper.headerList = headerList;
                    objectResultWrapperList.add(objectResultWrapper);
                    for(sObject obj : sObjectList ) {
                        SobjectResultDataWrapper objectResultWrapperV2 = new SobjectResultDataWrapper();
                        List<recordData> recordDataList = new List<recordData>();
                        for(String fld : fieldApiNameLabelMap.keySet() ) {
                            RecordData data = new RecordData();
                            if(referenceFieldMap.containsKey(fld)) {
                                ReferenceFieldWrapper thisRefField = referenceFieldMap.get(fld);
                                if(obj.getSObject(thisRefField.relationshipName) != null)
                                data.fieldvalue = (String) obj.getSObject(thisRefField.relationshipName).get(thisRefField.fieldToFetch);
                                else
                                    data.fieldvalue = '';
                                    
                            } else {
                                data.fieldvalue = String.valueOf(obj.get(fld));
                                data.fieldDataType = dataTypeMap.get(fld);
                            }
                            data.isVisible = TRUE;
                            if(data.fieldDataType == 'DATETIME') {
                                data.fieldvalue = data.fieldvalue != null ? DateTime.valueOf(data.fieldvalue).format() : '';
                            } else if(data.fieldDataType == 'DATE') {
                                data.fieldvalue = data.fieldvalue != null ? Date.valueOf(data.fieldvalue).format() : '';
                            }
                            data.fieldApiName = fld;
                            data.isVisible = !notVisibleFields.contains(fld);
                            recordDataList.add(data);
                        }
                        //objectResultWrapperV2.recordDataList = recordDataList;
                        RecordDataWrapper dataWrapper = new RecordDataWrapper();
                        dataWrapper.recordId = (String) obj.get('Id');
                        dataWrapper.recordList = recordDataList;
                        objectResultWrapperV2.recordData = dataWrapper;
                        objectResultWrapperList.add(objectResultWrapperV2);
                    }
                }
            }
            return objectResultWrapperList;
        } catch(Exception ex) {
            System.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled
    public static String fetchSelectedRecord(String recordId, String objectAPIName) {
        try {
            Map<String, String> returnMap = new Map<String, String>();
            if(String.isNotBlank(objectAPIName)) {
                SObjectType sobjType = Schema.getGlobalDescribe().get(objectAPIName);
                Schema.DescribeSObjectResult objectDescribe = sobjType.getDescribe();
                Map<String, Schema.SObjectField> fieldsMap = objectDescribe.fields.getMap();
                String nameField = '';
                String fieldLabel = '';
                for(String sobjField : fieldsMap.keySet()) {
                    Schema.DescribeFieldResult fieldDescribe = fieldsMap.get(sobjField).getDescribe();
                    if(fieldDescribe.isNameField()) {
                        nameField = fieldDescribe.getName();
                        fieldLabel = fieldDescribe.getLabel();
                        break;
                    }
                }
                if(!String.isBlank(recordId)) {
                    List<SObject> sobjList = Database.query('SELECT Id, ' + nameField + ' FROM ' + objectAPIName + ' WHERE Id = :recordId');
                    if(!sobjList.isEmpty()) {
                        returnMap.put('label', String.valueOf(sobjList[0].get(nameField)));
                        returnMap.put('recordId', recordId);
                    }
                }
                returnMap.put('fieldLabel', fieldLabel);
                returnMap.put('objectLabel', objectDescribe.getLabel());
            }
            return JSON.serialize(returnMap);
        } catch(Exception ex) {
            System.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
    }

    public class FieldSetWrapper {
        @AuraEnabled public String fieldLabel;
        @AuraEnabled public String fieldApiName;
        @AuraEnabled public Boolean isAutoNumber;
        @AuraEnabled public Boolean isCalculated;
        @AuraEnabled public String lightningDataType;
        @AuraEnabled public String relationshipPath;
        @AuraEnabled public String dataType;
        @AuraEnabled public Object dataValue;
    }

    public class SobjectResultDataWrapper {
        @AuraEnabled public List<RecordData> headerList;
        //@AuraEnabled public List<RecordData> recordDataList;
        @AuraEnabled public RecordDataWrapper recordData;
    }

    public class RecordData {
        @AuraEnabled public String fieldvalue;
        @AuraEnabled public String fieldDataType;
        @AuraEnabled public String fieldApiName;
        @AuraEnabled public String fieldLabel;
        @AuraEnabled public Boolean isVisible;
        @AuraEnabled public Boolean isSortable;

        public RecordData() {}

        public RecordData(String fieldApiName, String fieldLabel, Boolean isVisible,  Boolean isSortable) {
            this.fieldApiName = fieldApiName;
            this.fieldLabel = fieldLabel;
            this.isVisible = isVisible;
            this.isSortable = isSortable;
        }
    }

    public class RecordDataWrapper {
        @AuraEnabled public String recordId;
        @AuraEnabled public List<RecordData> recordList;
    }

    public class WhereClauseWrapper {
        @AuraEnabled public String logicType;
        @AuraEnabled public String customLogic;
        @AuraEnabled public List<Integer> logicIndexList;
        @AuraEnabled public List<FieldSetWrapper> filterFieldsList;
    }

    public class ReferenceFieldWrapper {
        public String relationshipName;
        public String fieldToFetch;
        
        public ReferenceFieldWrapper(String relationshipName, String fieldToFetch) {
            this.relationshipName = relationshipName;
            this.fieldToFetch = fieldToFetch;
        }
    }

    public class RecordSearchData {
        public String nameFieldAPIName;
        public List<FieldSetWrapper> searchFieldSetData;
    }
}