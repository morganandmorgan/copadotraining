/**
 * TriggerControlsSelectorTest
 * @description Unit tests for Trigger Controls custom object.
 * @author Jeff Watson
 * @date 1/2/2019
 */
@IsTest
public with sharing class TriggerControlsSelectorTest {

    static {
        Trigger_Control__c triggerControl = new Trigger_Control__c(Name = 'Default Controls');
        insert triggerControl;
    }

    @IsTest
    public static void TriggerControlsSelector_selectDefaultControls() {

        // Arrange + Act
        Test.startTest();
        Trigger_Control__c triggerControl = TriggerControlsSelector.selectDefaultControls();
        Test.stopTest();

        // Assert
        System.assert(triggerControl != null);
    }
}