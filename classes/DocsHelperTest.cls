@isTest
public class DocsHelperTest {
    
    @isTest
    public static void mainTest() {
        
        //create a test template
        litify_docs__Template__c template = new litify_docs__Template__c();
        template.litify_docs__Merged_File_Name__c = 'unitTest.docx';
        template.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        insert template;

        //create a field input
		litify_docs__Input__c inputField = new litify_docs__Input__c();
        inputField.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        inputField.litify_docs__Input_Type__c = 'Salesforce Field';
        inputField.litify_docs__Object_Field_Name__c = 'name';
        insert inputField;

        //create a lookup field input
		litify_docs__Input__c inputLf = new litify_docs__Input__c();
        inputLf.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        inputLf.litify_docs__Input_Type__c = 'Salesforce Field';
        inputLf.litify_docs__Object_Field_Name__c = 'litify_pm__Client__r.name';
        insert inputLf;
        
        //create a currentDate input
		litify_docs__Input__c inputCd = new litify_docs__Input__c();
        inputCd.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        inputCd.litify_docs__Input_Type__c = 'Salesforce Field';
        inputCd.litify_docs__Object_Field_Name__c = 'name';
		inputCd.litify_docs__Current_Date__c = true;
        insert inputCd;

        //create a bar code input
		litify_docs__Input__c inputBc = new litify_docs__Input__c();
        inputBc.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        inputBc.litify_docs__Input_Type__c = 'Salesforce Field';
        inputBc.litify_docs__Object_Field_Name__c = 'name';
        insert inputBc;

		litify_docs__Input_Format_Option__c format = new litify_docs__Input_Format_Option__c();
        format.litify_docs__Format_Type__c = 'format_pdf417';
		format.litify_docs__Input__c = inputBc.id;
        insert format;
        
        //create an input group/node
		litify_docs__Node__c inputGroup = new litify_docs__Node__c();
        insert inputGroup;
        
        //join the template to the group
        litify_docs__Template_Node_Junction__c i2g = new litify_docs__Template_Node_Junction__c();
        i2g.litify_docs__Template__c = template.id;
        i2g.litify_docs__Node__c = inputGroup.id;
        insert i2g;
        
        //join the input(s) to the group
        litify_docs__Node_Input_Junction__c junction = new litify_docs__Node_Input_Junction__c();
        junction.litify_docs__Node__c = inputGroup.id;
        junction.litify_docs__Input__c = inputField.id;
		insert junction;        

        //join the input(s) to the group
        junction = new litify_docs__Node_Input_Junction__c();
        junction.litify_docs__Node__c = inputGroup.id;
        junction.litify_docs__Input__c = inputCd.id;
		insert junction;        

        //join the input(s) to the group
        junction = new litify_docs__Node_Input_Junction__c();
        junction.litify_docs__Node__c = inputGroup.id;
        junction.litify_docs__Input__c = inputBc.id;
		insert junction;        

        //join the input(s) to the group
        junction = new litify_docs__Node_Input_Junction__c();
        junction.litify_docs__Node__c = inputGroup.id;
        junction.litify_docs__Input__c = inputLf.id;
		insert junction;        

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;
        
        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;

    	litify_pm__Role__c role = TestUtil.createRole(matter, account);
        insert role;

        litify_pm__Matter_Team_Role__c matterTeamRole = TestUtil.createMatterTeamMemberRole();
        insert matterTeamRole;

        User u = [SELECT id FROM User where id = :UserInfo.getUserId()];
        
        litify_pm__Matter_Team_Member__c teamMember = TestUtil.createMatterTeamMember(matter, u, matterTeamRole);
		insert teamMember;        
        
        Map<String,Id> sourceIds = new Map<String,Id>();
        sourceIds.put('litify_docs__Template__c', template.id);
        sourceIds.put('litify_pm__Matter__c', matter.id);
        sourceIds.put('litify_pm__Matter_Team_Member__c', teamMember.id);
                       
        Set<Id> roleIds = new Set<id>();
        roleIds.add(role.id);

        DocsHelper dh = new DocsHelper();
        dh.multiMergeOnRoles(sourceIds, roleIds);
        
        dh.singleMerge(sourceIds, 'test.docx');

    } //mainTest
    
} //class