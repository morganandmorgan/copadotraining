/**
 *  mmcommon_BusinessAccountsService
 */
public class mmcommon_BusinessAccountsService
{
    private static mmcommon_IBusinessAccountsService service()
    {
        return (mmcommon_IBusinessAccountsService) mm_Application.Service.newInstance( mmcommon_IBusinessAccountsService.class );
    }

    public static List<Account> findTreatmentFacilitiesCloseToLocation( final Location originatingLocation, final String treatmentCenterType )
    {
        return service().findTreatmentFacilitiesCloseToLocation( originatingLocation, treatmentCenterType );
    }
}