public with sharing class RelatedIncidentInvestigationsExt {
  public class InvestigationWrapper {
    public IncidentInvestigationEvent__c INCIE { get; set; }
    public Event Event { get; set; }
  }

  public ApexPages.StandardController stdController;
	public RelatedIncidentInvestigationsExt(ApexPages.StandardController stdControllerParam) {
    stdController = stdControllerParam;
    Id incidentId = stdController.getId();
    List<IncidentInvestigationEvent__c> incies = [SELECT Id, Investigation_Status__c, StartDateTime__c, EndDateTime__c, OwnerId, Owner.Name, Canceled_Rescheduled_By__c FROM IncidentInvestigationEvent__c WHERE Incident__c = :incidentId ORDER BY StartDateTime__c ASC];

    Set<Id> INCIEids = new Map<Id, IncidentInvestigationEvent__c>(incies).keySet();
    List<Event> events = [SELECT Id, WhatId, WhoId, Who.Name, OwnerId, Owner.Name FROM Event WHERE WhatId IN :INCIEids AND Type = :ScheduleInvestigatorService.EVENT_TYPE];

    IncidentInvestigations = new List<InvestigationWrapper>();
    for(IncidentInvestigationEvent__c incie : incies) {
      InvestigationWrapper wrapper = new InvestigationWrapper();
      wrapper.INCIE = incie;
      IncidentInvestigations.add(wrapper);
      Event associatedEvent = null;
      for (Event ev : events) {
        if (ev.WhatId == incie.Id) {
          wrapper.Event = ev;
          break;
        }
      }
    }
  }

  public List<InvestigationWrapper> IncidentInvestigations { get; private set; }
  public Boolean HasEvents { get { return IncidentInvestigations.size() > 0; } }
}