public interface mmmatter_IMattersSelector
    extends mmlib_ISObjectSelector
{
    List<litify_pm__Matter__c> selectById(Set<Id> idSet);
    List<litify_pm__Matter__c> selectMyOpenWithoutFutureActivities();
    List<litify_pm__Matter__c> selectByIntake(Set<Id> idSet);
    List<litify_pm__Matter__c> selectWithFieldsetByIntake(Schema.FieldSet fs, Set<Id> idSet);
    List<litify_pm__Matter__c> selectByName(Set<String> nameSet);
    List<litify_pm__Matter__c> selectByReferenceNumber(Set<String> referenceNumberSet);
}