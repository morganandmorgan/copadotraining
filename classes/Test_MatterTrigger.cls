@isTest
private class Test_MatterTrigger {
	
	@isTest static void test_matter_trigger() {
		RecordType matterRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Matter__c' AND DeveloperName = 'Personal_Injury'];
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account' LIMIT 1];
        
        List<Account> acc = new List<account>{new Account(LastName = 'TestAcc 1', RecordTypeId = acctRcdType.Id)};
        Insert acc;

        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c(Name = 'Slip and Fall');
        insert caseType;

        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c(Name = 'Slip and Fall');
        matterPlan.litify_pm__Case_Type__c = caseType.Id;
        insert matterPlan;

        User testUser = createUser();
        insert testUser;

        Test.startTest();
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = matterRcdType.Id;
        matter.ReferenceNumber__c = '999999056';
        matter.litify_pm__Client__c = acc[0].Id;
        matter.litify_pm__Principal_Attorney__c = testUser.Id;
        matter.litify_pm__Open_Date__c = Date.today();
        matter.litify_pm__Case_Type__c = caseType.Id;
        insert matter;

        RecordType roleRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Role__c' AND DeveloperName = 'Matter_Role'];
        litify_pm__Role__c role = new litify_pm__Role__c();
        role.RecordTypeId = roleRcdType.Id;
        role.litify_pm__Matter__c = matter.Id;
        role.litify_pm__Party__c = acc[0].Id;
        insert role;

        litify_pm__Request__c infoObj = new litify_pm__Request__c(Request__c = '911 Tapes', litify_pm__Matter__c = matter.Id);
        insert infoObj;
        
        List<litify_pm__Matter__c> matterList = [Select Id from litify_pm__Matter__c];
        System.assertEquals(1, matterList.size());

        Test.stopTest();
	}

	public static User createUser() {
        Integer userNum = 1;
        return new User(
            IsActive = true,
            FirstName = 'FirstName ' + userNum,
            LastName = 'LastName ' + userNum,
            //Username = 'apex.temp.test.user@sample.com.' + userNum,
            Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@xyz.com',
            Email = 'apex.temp.test.user@sample.com.' + userNum,
            Alias = 'alib' + userNum,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            Territory__c = null
        );
    }	
}