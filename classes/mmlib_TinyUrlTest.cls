/**
 * mmlib_TinyUrlTest
 * @description Test for Tiny Url class.
 * @author Jeff Watson
 * @date 5/10/2019
 */
@IsTest
private class mmlib_TinyUrlTest
{
    @IsTest
    private static void getTinyUrl_List() {
        String expectedUrl = 'http://abc.def/test';
        List<String> longUrls = new List<String> {'http://forthepeople.com'};
        Test.setMock(HttpCalloutMock.class, new mmlib_TinyUrlMock(expectedUrl));

        Test.startTest();
        List<String> shortUrls = mmlib_TinyUrl.getTinyUrl(longUrls);
        Test.stopTest();

        System.assertEquals(expectedUrl, shortUrls[0], 'Using mock, so if this fails it is weird.');
    }

    @IsTest
    private static void getTinyUrl_Single()
    {
        String expectedUrl = 'http://abc.def/test';
        Test.setMock(HttpCalloutMock.class, new mmlib_TinyUrlMock(expectedUrl));

        Test.startTest();
        String shortUrl = mmlib_TinyUrl.getTinyUrl('Doesnt really matter');
        Test.stopTest();

        System.assertEquals(expectedUrl, shortUrl, 'Using mock, so if this fails it is weird.');
    }
}