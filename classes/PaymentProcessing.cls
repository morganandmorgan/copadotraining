public with sharing class PaymentProcessing {
    //Namespace container for Entities model wrapper classes
    private PaymentProcessing() {
    }

    public class PaymentCollectionWrapper{
        //TODO:  Turn these fields into properties and fix the case to be Java-like:
        public Payment_Collection__c paymentCollection=null;
        public List<PaymentWrapper> payments=null;
    }  
    
    public class PaymentWrapper implements Comparable{
        public PaymentWrapper(){
        }
        public PaymentWrapper(c2g__codaPayment__c payment){
            this.payment= payment;
        }        
        public c2g__codaPayment__c payment=null;
        public Boolean complete = false;
        public Set<Id> accountIds = new Set<Id>();
        public List<c2g__codaTransactionLineItem__c> transactionLineItems=null;	//TODO:  Replace with wrapper.
        public List<TransactionLineItemWrapper> transactionLineItemWrappers=null;
        public List<PaymentSummaryWrapper> paymentSummaryWrappers=null;

        public Integer compareTo(Object other) {
            //Custom sort by payment record Id:  
            PaymentWrapper that = (PaymentWrapper)other;
            Id thisId=null;
            Id thatId=null;
            if(this.payment!=null){
                thisId=this.payment.Id;
            }
            if(that.payment!=null){
                thatId=that.payment.Id;
            }            

            if (thisId == thatId) return 0;
            if (thisId > thatId) return 1;
            return -1;        
        }

    }
    
    public class TransactionLineItemWrapper{
        public c2g__codaTransactionLineItem__c transactionLineItem=null;
        public Decimal outstandingAmountPositive = 0;
        public Boolean isSelected=false;
        public String accountId = '';
        public String accountName = '';
        public String requestedById = '';
        public String requestedByName = '';
        public String matterId = '';
        public String matterRef = '';
        public String vendorInvoiceNumber = '';
        public String bankType = '';
        public String pinId = '';
        public String pinNumber = '';
        public String checkMemo = '';
        public String invoiceDate = '';
        public String dueDate = '';
    }    
    
    public class PaymentSummaryWrapper{
        public Id paymentId = null;
        public String paymentName = null;
        public Id accountId = null;
        public c2g__codaPaymentAccountLineItem__c paymentSummary=null;
        public c2g__codaPaymentMediaSummary__c paymentMediaSummary=null;
        public String paymentMediaControlId=null;
        public Boolean checksPending=true;
        public String checkNumber=null;	//Integer checknumber.
    }

    public class PaymentVoidWrapper{
        public Id paymentId = null;
        public Boolean isSelected = false;
        public String paymentName = null;
        public Id accountId = null;
        public c2g__codaPaymentMediaSummary__c paymentMediaSummary=null; 
        public c2g__codaPaymentAccountLineItem__c paymentSummary=null;
        public String checkNumber=null;	//Integer checknumber.
    }

    public virtual class PaymentProcessException extends Exception {}

}