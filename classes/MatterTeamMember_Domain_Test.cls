/**
 * MatterTeamMember_Domain_Test
 * @description Test for Matter Team Member Domain class.
 * @author Jeff Watson
 * @date 2/26/2019
 */
@IsTest
public with sharing class MatterTeamMember_Domain_Test {

    private static User user;
    private static Account account;
    private static litify_pm__Matter__c matter;
    private static litify_pm__Matter__c matterforUpdate;
    private static litify_pm__Matter_Team_Role__c matterTeamRole;
    private static litify_pm__Matter_Team_Member__c matterTeamMember;

    static {
        user = TestUtil.createUser();
        insert user;

        account = TestUtil.createPersonAccount('Jimmy', 'Buffett');
        insert account;

        matter = TestUtil.createMatter(account);
        insert matter;
        
        matterforUpdate = TestUtil.createMatter(account);
        insert matterforUpdate;


        matterTeamRole = TestUtil.createMatterTeamMemberRole();
        insert matterTeamRole;
    }

    @IsTest
    private static void triggerCoverage() {
        insert TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
    }

    @IsTest
    private static void ctor() {
        MatterTeamMember_Domain matterTeamMemberDomain = new MatterTeamMember_Domain(new List<litify_pm__Matter_Team_Member__c> {matterTeamMember});
        System.assert(matterTeamMemberDomain != null, 'Target class should not be null.');
    }

    @IsTest
    private static void onBeforeInsert() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        insert matterTeamMember;
    }

    @IsTest
    private static void onBeforeUpdate() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        insert matterTeamMember;
        update matterTeamMember;
    }
    
    @IsTest
    private static void onAfterInsert() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        insert matterTeamMember;
    }

    @IsTest
    private static void onAfterUpdate() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        insert matterTeamMember;
        update matterTeamMember;

        matterTeamMember.litify_pm__Matter__c = matterforUpdate.id;
        update matterTeamMember;
        
    }

    @IsTest
    private static void onAfterDelete() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        insert matterTeamMember;
        delete matterTeamMember;
    }

    @IsTest
    private static void codeCoverage() {
        MatterTeamMember_Domain.coverage();
    }
}