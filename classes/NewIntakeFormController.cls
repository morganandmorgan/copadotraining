public  class NewIntakeFormController {
    public static String phoneNumber = '';
    public static String handlingFirm = '';
    public static String marketingSource = '';
    public static String optOut = '';

   
 @AuraEnabled 
    public static string intakeDetails(String dnis)
    {
      System.debug(dnis+'>>>>>>>>>  dnis');
        List<Cisco_Queue_Setting__mdt> cqsList = mmintake_CiscoQueueSettingsSelector.newInstance().selectByDnis(new Set<String> {dnis});

System.debug(cqsList+'>>>>>>>>>  dnisList');
        if (!cqsList.isEmpty())
        {
            handlingFirm = cqsList.get(0).Handling_Firm__c;
            marketingSource = cqsList.get(0).Marketing_Source__c;
        }

        if (isTest())
        {
            handlingFirm = mock_cqs.handlingFirm;
            marketingSource = mock_cqs.marketingSource;
        }

        System.debug(handlingFirm+'     '+marketingSource);
        IntakeFieldDetails intakeFields = new IntakeFieldDetails();
        intakeFields.litigationTypes = litigationTypes();
        intakeFields.languages       = languages();
        intakeFields.handlingFirms   = handlingFirms(handlingFirm);
        intakeFields.marketingSources= marketingSources(marketingSource);
        intakeFields.optOut          = optOut();
        System.debug(intakeFields);
        return JSON.serialize(intakeFields);
    }

    // ============ Static Functionality =============================
    private static Map<String, Schema.sObjectField> webIdToSObjectFieldMap = new Map<String, Schema.sObjectField>();

    static
    {
        webIdToSObjectFieldMap.put( 'litigationSelect', Intake__c.Litigation__c );
        webIdToSObjectFieldMap.put( 'handlingFirmSelect', Intake__c.Handling_Firm__c );
        webIdToSObjectFieldMap.put( 'marketingSourceSelect', Intake__c.Marketing_Source__c );
        webIdToSObjectFieldMap.put( 'optOutSelect', Account.SMS_Text_Automated_Communication_OptOut__c );
        webIdToSObjectFieldMap.put( 'languageSelect', Account.Language__c );
        webIdToSObjectFieldMap.put( 'caseDetails', Intake__c.Lead_Details__c );
        webIdToSObjectFieldMap.put( 'textarea-generating-source-details', Intake__c.Additional_Marketing_Source_Information__c );
        webIdToSObjectFieldMap.put( 'emailAddress', Account.PersonEmail );
        webIdToSObjectFieldMap.put( 'clientFirstName', Account.FirstName );
        webIdToSObjectFieldMap.put( 'clientLastName', Account.LastName );
        webIdToSObjectFieldMap.put( 'clientPhone', Account.Phone);
        webIdToSObjectFieldMap.put( 'clientMobile', Account.PersonMobilePhone);
    }

   public  static List<IntakePickListManage> litigationTypes()
    {
        return createSObjectFieldMap(Intake__c.Litigation__c,'');
    }

    public static List<IntakePickListManage> languages()
    {
        return createSObjectFieldMap(Account.Language__c,'');
    }

    public static List<IntakePickListManage> handlingFirms(String handlingForm)
    {
        return createSObjectFieldMap(Intake__c.Handling_Firm__c,handlingForm);
    }

  
    public static List<IntakePickListManage> marketingSources(String marketingSorce)
    {
        return createSObjectFieldMap(Intake__c.Marketing_Source__c,marketingSorce);
    }

   
    public static List<IntakePickListManage> optOut()
    {
         List<IntakePickListManage>  output = new List<IntakePickListManage> ();
         IntakePickListManage obj = new IntakePickListManage();
         obj.value = 'false';
         obj.label = 'Yes';
         output.add(obj);
         obj = new IntakePickListManage();
         obj.value = 'true';
         obj.label = 'No';
         output.add(obj);

        return output;
    }

    private static List<IntakePickListManage> createSObjectFieldMap(SObjectField field,string deafultValue)
    {
        List<IntakePickListManage> output = new List<IntakePickListManage>();

        for ( PicklistEntry pe : field.getDescribe().getPicklistValues() )
        {	IntakePickListManage obj = new IntakePickListManage();
            if ( pe.isActive() )
            {
                if(deafultValue == pe.getValue()){
                    obj.selected =  true;
                }
                else{
                    obj.selected =  false;
                }
                obj.value = pe.getValue();
                obj.label =  pe.getLabel();
				output.add(obj);
            }
        }

        return output;
    }

    @AuraEnabled 
    public static IntakeDetails saveIntake(Map<String, String> fieldParameterMap)
    {
        IntakeDetails output = new IntakeDetails();

        system.debug( fieldParameterMap );

        if ( fieldParameterMap != null && ! fieldParameterMap.isEmpty() )
        {
            Map<Schema.sObjectField, String> serviceInputMap = new Map<Schema.sObjectField, String>();
            System.debug('in if');
            for ( String mapKey : fieldParameterMap.keyset() )
            {
                System.debug('JLW:::' + mapKey + ' = ' + fieldParameterMap.get( mapKey ));
                if ( webIdToSObjectFieldMap.containsKey( mapKey ) )
                {
                    if ( String.isNotBlank(fieldParameterMap.get( mapKey ))  )
                    {
                        serviceInputMap.put( webIdToSObjectFieldMap.get( mapKey), fieldParameterMap.get( mapKey ));
                    }
                }
            }
            try
            {
                if (mock_exception != null)
                {
                    throw mock_exception;
                }

                serviceInputMap.put(Intake__c.InputChannel__c, 'MinimalIntake');

                Intake__c newIntake = processSignUpRequest( serviceInputMap );
                System.debug(newIntake+'intake');
                output.intakeName = newIntake.name;
                output.intakeId = newIntake.id;
            }
            catch ( mmattyprtl_IntakeSignUpServiceException isuse )
            {
                if ( isuse.getCause() instanceOf System.DMLException )
                {
                    System.DMLException dmle = (System.DMLException)isuse.getCause();

                    if ( StatusCode.REQUIRED_FIELD_MISSING == dmle.getDmlType(0) )
                    {
                        throw new UXException( 'The following fields are required: ' + String.join( dmle.getDmlFieldNames(0), ', '));
                    }
                    else
                    {
                        throw new UXException( 'There was a problem saving the refereral.  Please try again and if the issue persists, contact IT Support.' );
                    }
                }
                else
                {
                    throw new UXException( isuse.getCause() == null ? isuse.getMessage() : isuse.getCause().getMessage() );
                }
            }
        }
        System.debug(output+'><<><><><>output');
        return output;
    }

    public static Intake__c processSignUpRequest( Map<Schema.sObjectField, String> fieldParameterMap)
    {
        Intake__c intakeRequest = null;
        System.debug(fieldParameterMap);
        if ( fieldParameterMap != null
            && ! fieldParameterMap.isEmpty()
            )
        {

            mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

            Account client = null;

            if (!isTest())
            {
                System.debug(uow+'uow');
                client = new mmcommon_PersonAccountsFactory( uow ).with( fieldParameterMap ).generate();
                System.debug(client+'>>client');
            }
            else
            {
                client = mock_client;
            }

            mmintake_IntakesFactory intakesFactory = new mmintake_IntakesFactory( uow );

            String caseDetailsText = '';

            fieldParameterMap.put( Intake__c.Lead_Details__c, caseDetailsText + ( fieldParameterMap.containsKey( Intake__c.Lead_Details__c )
                                                                    ? '\n\n' + fieldParameterMap.get( Intake__c.Lead_Details__c )
                                                                    : null ) );

            if (!isTest())
            {
                intakeRequest = intakesFactory.with( fieldParameterMap )
                                            .setClient( client )
                                            .setClientAsCaller()
                                            .setClientAsInjuredParty()
                                            .generate();
            }
            else
            {
                intakeRequest = mock_intake;
            }

            if (!isTest())
            {
               
                mmmarketing_TrackingInfoFactory.getInstance(uow)
                    .generateNewCall(
                        intakeRequest,
                        client.Phone,
                        Datetime.now()
                );
            }

            try
            {
                if (!isTest())
                {
                    uow.commitWork();
                    intakeRequest = mmintake_IntakesSelector.newInstance().selectById( new set<id>{ intakeRequest.id } )[0];
                }
            }
            catch ( System.DMLException dmle )
            {
                throw new mmattyprtl_IntakeSignUpServiceException( dmle );
            }
        }

        return intakeRequest;
    }

    public class IntakeFieldDetails
    {
        public List<IntakePickListManage> litigationTypes;
        public List<IntakePickListManage> languages;
        public List<IntakePickListManage> handlingFirms;
        public List<IntakePickListManage> marketingSources;
        public List<IntakePickListManage> optOut;
    }
     public class IntakePickListManage
    {
        public String  value;
        public String  label;
        public Boolean  selected;
    }

     public class IntakeDetails
    { @AuraEnabled 
        public String intakeName;
         @AuraEnabled 
        public String intakeId;
    }

    public class UXException extends Exception { }

    // =============== Test Context ==========================================

    @TestVisible
    private static MockCiscoQueueSetting mock_cqs = null;

    @TestVisible
    private static Account mock_client = null;

    @TestVisible
    private static Intake__c mock_intake = null;

    @TestVisible
    private static mmattyprtl_IntakeSignUpServiceException mock_exception = null;

    private static Boolean isTest()
    {
        return mock_cqs != null || mock_client != null || mock_intake != null || mock_exception != null;
    }

    @TestVisible
    private class MockCiscoQueueSetting
    {
        public String handlingFirm = '';
        public String marketingSource = '';
    }

   }