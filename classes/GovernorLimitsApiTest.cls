/**
 * GovernorLimitsApiTest
 * @description Unit test for GovernorLimitsApi
 * @author Matt Terrill
 * @date 8/22/2019
 */
@isTest
public with sharing class GovernorLimitsApiTest {

    @isTest
    private static void testGetLimits() {

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new GovernorLimitsApiTestMock());

        GovernorLimitsApi gla = new GovernorLimitsApi();

        Map<String,GovernorLimitsApi.LimitsClass> limits = gla.getLimits();

        //make sure we got something back
        System.assertNotEquals(0,limits.size());

    } //testGetLimits


    @isTest
    private static void formatForSlack() {

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new GovernorLimitsApiTestMock());

        GovernorLimitsApi gla = new GovernorLimitsApi();

        String limits = gla.formatForSlack(true);

        //make sure we got something back
        System.assertNotEquals(0,limits.length());


    } //testGetLimits    

} //class