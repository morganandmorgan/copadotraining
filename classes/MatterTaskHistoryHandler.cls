public class MatterTaskHistoryHandler {
    public static void getInsertHistory(List<Task> newTasks, List<TaskHistory__c> taskHistoris) {
        for(Task task : newTasks) {
            addTaskHistory( task, 'Insert', taskHistoris);
        }
    }

    public static void getUpdateHistory(Map<Id,Task> newMap, Map<Id,Task> oldMap,
            List<TaskHistory__c> taskHistoris) {
        for(Id taskId : newMap.keySet()) {
            if(oldMap.get(taskId).ActivityDate != newMap.get(taskId).ActivityDate) {
                addTaskHistory( newMap.get(taskId), 'Update', taskHistoris);
            }
        }
    }

    private static void addTaskHistory(Task task, String actionType, List<TaskHistory__c> taskHistoris) {
        if( (task.WhatId != null) && (task.WhatId.getSobjectType() == litify_pm__Matter__c.getSObjectType() )) {
            String sessionInfo = '';
            
            try {
                if( !Test.isRunningTest() ) {
                	sessionInfo = Json.serialize( Auth.SessionManagement.getCurrentSession() );
                }
            } catch (Exception e) {
                sessionInfo = 'ERROR >>> ' + e.getMessage(); 
            }
                                             
            TaskHistory__c taskHistory = new TaskHistory__c(SessionId__c = UserInfo.getSessionId(),
                                                            TaskId__c = task.Id,
                                                            Type__c = actionType,
                                                            TaskObject__c = 'Task >>> ' + Json.serialize(task) +
                                                                            'SessionInfo >>> ' +  sessionInfo );

            taskHistoris.add(taskHistory);
        }
    }
}