public class mmcommon_Users
    extends mmlib_SObjectDomain
    implements mmcommon_IUsers
{
//    private static Set<Date> futureHolidays = null;

    private Map<Id,SObject> existingRecords = new Map<Id,SObject>();

    public static mmcommon_IUsers newInstance(List<User> records)
    {
        return (mmcommon_IUsers) mm_Application.Domain.newInstance(records);
    }

    public static mmcommon_IUsers newInstance(Set<Id> recordIds)
    {
        return (mmcommon_IUsers) mm_Application.Domain.newInstance(recordIds);
    }

    public mmcommon_Users(List<User> records)
    {
        super(records);
    }

    public override void onAfterInsert()
    {
        publishEventForNewChangedOrDeletedUsers();
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
    {
        this.existingRecords.putAll( existingRecords );

        publishEventForNewChangedOrDeletedUsers();
    }

    public override void onAfterDelete()
    {
        publishEventForNewChangedOrDeletedUsers();
    }

    public void createUserHolidaysIfNeeded()
    {
        this.processDomainLogicInjections( 'createUserHolidaysIfNeeded' );
    }

    @TestVisible
    private void publishEventForNewChangedOrDeletedUsers()
    {
        Set<Id> userIdSet = (new map<id, SObject>( this.records )).keySet();

        String payload = JSON.serialize(userIdSet);

        if (test_publishEvents)
        {
            EventBus.publish(
                new mmlib_Event__e(
                    EventName__c = 'UsersChanged',
                    RelatedSobject__c = 'User',
                    Payload__c = payload));
        }

        test_payload = payload;
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmcommon_Users(sObjectList);
        }
    }

    // ================== Test Context ==================================================
    @TestVisible
    private static Boolean test_publishEvents = true;

    @TestVisible
    private static String test_payload = null;
}