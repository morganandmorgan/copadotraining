/**
 * MatterTeamMember_Domain
 * @description Domain class for Matter Team Member SObject.
 * @author Jeff Watson
 * @date 2/26/2019
 */

public class MatterTeamMember_Domain extends fflib_SObjectDomain {

    // Ctors
    public MatterTeamMember_Domain(List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {
        super(matterTeamMembers);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new MatterTeamMember_Domain(records);
        }
    }

    // Trigger Handling
    public override void onBeforeInsert() {      
       for(litify_pm__Matter_Team_Member__c matt : ( List<litify_pm__Matter_Team_Member__c> ) this.Records ){
           if(matt.litify_pm__User__c != null) {
               matt.OwnerID = matt.litify_pm__User__c;
           }   
        }
    }

    // Trigger Handling
    public override void onBeforeUpdate(map<id, SObject> existing) {      
        for(litify_pm__Matter_Team_Member__c matt : ( List<litify_pm__Matter_Team_Member__c> ) this.Records ){
            if(matt.litify_pm__User__c != null) {
                matt.OwnerID = matt.litify_pm__User__c;
            }
        }
    }

    // Trigger Handling
    public override void onAfterInsert() {      
        MatterSync_Service.updateRoleUser(this.Records);
        /*  The conversion flow is now run in a queueable and this was causing issues and not required, so it is being removed.
            If we need to re-introduce this code, it should be in an @future*/
        //System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(this.Records));
        
        //Map to matter record id and related user Id
        Map<Id, Id> mapOfMatterIdAndUserId =  new Map<Id, Id>();
        for(litify_pm__Matter_Team_Member__c matt : ( List<litify_pm__Matter_Team_Member__c> ) this.Records ){
            if(matt.litify_pm__Matter__c != null)
                mapOfMatterIdAndUserId.put(matt.litify_pm__Matter__c,matt.litify_pm__User__c );
        }
        // call method that provide access to matter record for user
        assignPermissionToUsers(mapOfMatterIdAndUserId);
    }

    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        
        MatterSync_Service.updateRoleUser(this.Records);
        // see note above about this code
        //System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(this.Records, (Map<Id, litify_pm__Matter_Team_Member__c>) existingRecords));
        
        //Map to matter record id and related user Id
        Map<Id, Id> mapOfMatterIdAndUserId =  new Map<Id, Id>();
        //Map to matter record id and related user from where we will remove the access
        Map<Id, Id> removePermissionMap =  new Map<Id, Id>();
        // For loop to iterate matter team member
        for(litify_pm__Matter_Team_Member__c  matt :( List<litify_pm__Matter_Team_Member__c> ) this.Records){
           
           litify_pm__Matter_Team_Member__c existingmatterTeam = (litify_pm__Matter_Team_Member__c) existingRecords.get(matt.id);
           
           //Check the condition and create a map when both user and matter is changes on matter team member
           if(matt.litify_pm__User__c != existingmatterTeam.litify_pm__User__c && matt.litify_pm__Matter__c != existingmatterTeam.litify_pm__Matter__c){
                mapOfMatterIdAndUserId.put(matt.litify_pm__Matter__c,matt.litify_pm__User__c );
                removePermissionMap.put(existingmatterTeam.litify_pm__Matter__c,  existingmatterTeam.litify_pm__User__c);
           }
           //Check the condition and create a map when user change on matter team member
           else if(matt.litify_pm__User__c != existingmatterTeam.litify_pm__User__c && matt.litify_pm__Matter__c == existingmatterTeam.litify_pm__Matter__c){
                mapOfMatterIdAndUserId.put(matt.litify_pm__Matter__c,matt.litify_pm__User__c );
                removePermissionMap.put(matt.litify_pm__Matter__c,  existingmatterTeam.litify_pm__User__c);
           }
            //Check the condition and create a map when  matter change on matter team member
           else if(matt.litify_pm__Matter__c != existingmatterTeam.litify_pm__Matter__c && matt.litify_pm__User__c == existingmatterTeam.litify_pm__User__c){
                mapOfMatterIdAndUserId.put(matt.litify_pm__Matter__c,matt.litify_pm__User__c );
                removePermissionMap.put(existingmatterTeam.litify_pm__Matter__c,  matt.litify_pm__User__c);
           }
        }
       // call method that provide remove access to matter record for user
       removePermissionToUsers(removePermissionMap);
       // call method that provide access to matter record for user
       assignPermissionToUsers(mapOfMatterIdAndUserId); 
    }

    public override void onAfterDelete() {
        MatterSync_Service.deleteRoleUser(this.Records);
        /* see note above about this code
        System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(this.Records));
        */
        
        Map<Id, Id> removePermissionMap =  new Map<Id, Id>();
        for(litify_pm__Matter_Team_Member__c matt : ( List<litify_pm__Matter_Team_Member__c> ) this.Records){
            if(matt.litify_pm__Matter__c != null)
                removePermissionMap.put(matt.litify_pm__Matter__c,matt.litify_pm__User__c );
        }
        removePermissionToUsers(removePermissionMap);   
    }

   //This method provide the access to user for selected matter record
   public static void assignPermissionToUsers(Map<Id, Id> mapOfMatterIdAndUserId){
        
        List<litify_pm__Matter__Share > matterShareList = new List<litify_pm__Matter__Share >();
        for(Id matterId : mapOfMatterIdAndUserId.keyset()) {
            litify_pm__Matter__Share matteObject = new litify_pm__Matter__Share();
            matteObject.ParentId= matterId;
            matteObject.UserOrGroupId= mapOfMatterIdAndUserId.get(matterId);
            matteObject.AccessLevel = 'Edit';
            matterShareList.add(matteObject);
        }
        if(matterShareList.size()>0){
            Database.SaveResult[] sr = Database.insert(matterShareList,false);
        }
    }
    //This methos remove the access from the user
    public static void removePermissionToUsers(Map<Id, Id> removePermissionMap) {
        
        if(!removePermissionMap.isEmpty()){
            Database.executeBatch(new RemoveShareRecordAccess(removePermissionMap));
        }
    }

    // Methods
    @TestVisible
    private static void coverage() {
        Integer x = 0;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
    }
}