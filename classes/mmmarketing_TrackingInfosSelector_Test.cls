/**
 * mmmarketing_TrackingInfosSelector_Test
 * @description Test for Tracking Info Selector class.
 * @author Jeff Watson
 * @date 2/19/2019
 */
@IsTest
public with sharing class mmmarketing_TrackingInfosSelector_Test {

    private static Intake__c intake;
    private static Account personAccount;
    private static Marketing_Tracking_Info__c marketingTrackingInfo;
    private static mmmarketing_ITrackingInfosSelector trackingInfosSelector;

    static {
        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        marketingTrackingInfo = new Marketing_Tracking_Info__c();
        marketingTrackingInfo.Intake__c = intake.Id;
        insert marketingTrackingInfo;

        trackingInfosSelector = mmmarketing_TrackingInfosSelector.newInstance();
    }

    @IsTest
    private static void ctor() {
        mmmarketing_TrackingInfosSelector trackingInfosSelector = new mmmarketing_TrackingInfosSelector();
        System.assert(trackingInfosSelector != null);

        mmmarketing_ITrackingInfosSelector trackingInfosSelector2 = mmmarketing_TrackingInfosSelector.newInstance();
        System.assert(trackingInfosSelector2 != null);
    }

    @IsTest
    private static void selectById() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectById(new Set<Id> {marketingTrackingInfo.Id});
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(1, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectByIdWithMarketingTrackingDetails() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectByIdWithMarketingTrackingDetails(new Set<Id> {marketingTrackingInfo.Id});
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(1, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectByIntake() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectByIntake(new Set<Id> {marketingTrackingInfo.Id});
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(0, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectCallOnlyById() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectCallOnlyById(new Set<Id> {marketingTrackingInfo.Id});
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(1, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecently() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecently();
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(0, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecentlyForShallowResolution() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecentlyForShallowResolution();
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(1, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectWhereMarketingCampaignNotLinkedByCampaignValue() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectWhereMarketingCampaignNotLinkedByCampaignValue(new Set<String> {''});
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(1, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectWhereMarketingSourceNotLinkedBySourceValue() {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = trackingInfosSelector.selectWhereMarketingSourceNotLinkedBySourceValue(new Set<String> {''});
        System.assert(marketingTrackingInfos != null);
        System.assertEquals(1, marketingTrackingInfos.size());
    }

    @IsTest
    private static void selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue() {
        Database.QueryLocator queryLocator = trackingInfosSelector.selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue(new Set<String> {''});
        System.assert(queryLocator != null);
    }

    @IsTest
    private static void selectQueryLocatorWhereMarketingCampaignNotLinkedByCampaignValue() {
        Database.QueryLocator queryLocator = trackingInfosSelector.selectQueryLocatorWhereMarketingCampaignNotLinkedByCampaignValue(new Set<String> {''});
        System.assert(queryLocator != null);
    }

    @IsTest
    private static void selectQueryLocatorWhereMarketingFinancialPeriodNotLinkedAndEventRecent() {
        Database.QueryLocator queryLocator = trackingInfosSelector.selectQueryLocatorWhereMarketingFinancialPeriodNotLinkedAndEventRecent();
        System.assert(queryLocator != null);
    }

    @IsTest
    private static void selectQueryLocatorWhereMarketingExperienceVariationNotLinkedAndEventRecent() {
        Database.QueryLocator queryLocator = trackingInfosSelector.selectQueryLocatorWhereMarketingExperienceVariationNotLinkedAndEventRecent();
        System.assert(queryLocator != null);
    }
}