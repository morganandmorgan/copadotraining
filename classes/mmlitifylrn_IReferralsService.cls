public interface mmlitifylrn_IReferralsService
{
    void referToLitify(Set<id> referralIdSet);

    void syncFromLitify();

    void syncFromLitify(Datetime lastSync);
}