/**
 *  mmmarketing_TI_AutoLinkCampaignsBatch
 */
public class mmmarketing_TI_AutoLinkCampaignsBatch
    implements mmlib_GenericBatch.IGenericExecuteBatch, Database.Batchable<SObject>
{
    public void run( List<SObject> scope )
    {
        mmmarketing_TrackingInformations.newInstance( (list<Marketing_Tracking_Info__c>)scope ).autoLinkToMarketingInformationIfRequiredInQueueableMode();
    }

    public mmmarketing_TI_AutoLinkCampaignsBatch () { }

    private Set<String> campaignValueSet;
    private integer batchSize = 200;

    public mmmarketing_TI_AutoLinkCampaignsBatch(Set<String> campaignValueSet)
    {
        this.campaignValueSet = campaignValueSet;
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return mmmarketing_TrackingInfosSelector.newInstance().selectQueryLocatorWhereMarketingCampaignNotLinkedByCampaignValue( this.campaignValueSet );
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            mmmarketing_TrackingInformations.newInstance( (list<Marketing_Tracking_Info__c>)scope ).autoLinkToMarketingInformationIfRequiredInQueueableMode();
        }
        catch (Exception e)
        {
            System.debug('Error executing batch ' + e);
        }
    }

    public void finish(Database.BatchableContext context)
    {

    }

    public mmmarketing_TI_AutoLinkCampaignsBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public void execute()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticTILinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticTILinkingEnabled__c )
        {
            Database.executeBatch( this, batchSize );
        }
    }
}