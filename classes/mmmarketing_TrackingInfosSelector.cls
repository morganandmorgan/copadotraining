/**
 *  mmmarketing_TrackingInfosSelector
 */
public class mmmarketing_TrackingInfosSelector extends mmlib_SObjectSelector implements mmmarketing_ITrackingInfosSelector {

    public static mmmarketing_ITrackingInfosSelector newInstance() {
        return (mmmarketing_ITrackingInfosSelector) mm_Application.Selector.newInstance(Marketing_Tracking_Info__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType() {
        return Marketing_Tracking_Info__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Marketing_Tracking_Info__c.Ad_Type__c,
                Marketing_Tracking_Info__c.Call_Info_Resolution_Status__c,
                Marketing_Tracking_Info__c.Calling_Phone_Number__c,
                Marketing_Tracking_Info__c.Campaign_Value__c,
                Marketing_Tracking_Info__c.Domain_Value__c,
                Marketing_Tracking_Info__c.Experiment_Value__c,
                Marketing_Tracking_Info__c.Experiment_Variation_Value__c,
                Marketing_Tracking_Info__c.GCLID_Value__c,
                Marketing_Tracking_Info__c.Intake__c,
                Marketing_Tracking_Info__c.Marketing_Campaign__c,
                Marketing_Tracking_Info__c.Marketing_Exp_Variation__c,
                Marketing_Tracking_Info__c.Marketing_Financial_Period__c,
                Marketing_Tracking_Info__c.Marketing_Source__c,
                Marketing_Tracking_Info__c.Medium_Value__c,
                Marketing_Tracking_Info__c.Page_Value__c,
                Marketing_Tracking_Info__c.Raw_Landing_Page_URL__c,
                Marketing_Tracking_Info__c.Source_Value__c,
                Marketing_Tracking_Info__c.Term_Value__c,
                Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c
        };
    }

    public List<Marketing_Tracking_Info__c> selectById(Set<Id> idSet) {
        return (List<Marketing_Tracking_Info__c>) selectSObjectsById(idSet);
    }

    public List<Marketing_Tracking_Info__c> selectByIdWithMarketingTrackingDetails(Set<Id> idSet) {
        fflib_QueryFactory marketingInfosQueryFactory = newQueryFactory().setCondition('Id in :IdSet');
        new mmmarketing_TrackingDetailInfosSelector().addQueryFactorySubselect(marketingInfosQueryFactory);
        return Database.query(marketingInfosQueryFactory.toSOQL());
    }

    public List<Marketing_Tracking_Info__c> selectByIntake(Set<Id> intakeIdSet) {
        Boolean assertCRUD = false;
        Boolean enforceFLS = false;
        Boolean includeSelectorFields = true;
        return Database.query(newQueryFactory(assertCRUD, enforceFLS, includeSelectorFields).setCondition(Marketing_Tracking_Info__c.Intake__c + ' in :intakeIdSet').toSOQL());
    }

    public List<Marketing_Tracking_Info__c> selectCallOnlyById(Set<Id> idSet) {
        Id mtiCallRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Marketing_Tracking_Info__c.SObjectType, 'Call');

        return Database.query(newQueryFactory().setCondition(Marketing_Tracking_Info__c.Id + ' in :idSet'
                + ' AND ' + Marketing_Tracking_Info__c.RecordTypeId + ' = :mtiCallRecordTypeId')
                .toSOQL());
    }

    public List<Marketing_Tracking_Info__c> selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecently() {
        Id mtiCallRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Marketing_Tracking_Info__c.SObjectType, 'Call');

        Datetime maximumHours = datetime.now().addHours(-24);
        Datetime minimumHours = datetime.now().addHours(-18);

        set<String> callInfoResolutionStatusSet = new set<String>();
        callInfoResolutionStatusSet.add('NeedsProcessing');
        callInfoResolutionStatusSet.add('ProcessedCallNotResolved');
        callInfoResolutionStatusSet.add('ProcessedCallResolvedShallow');

        return Database.query(newQueryFactory().setCondition(Marketing_Tracking_Info__c.RecordTypeId + ' = :mtiCallRecordTypeId'
                + ' AND ' + Marketing_Tracking_Info__c.Call_Info_Resolution_Status__c + ' in :callInfoResolutionStatusSet'
                + ' AND ' + Marketing_Tracking_Info__c.CreatedDate + ' >= :maximumHours'
                + ' AND ' + Marketing_Tracking_Info__c.CreatedDate + ' <= :minimumHours')
                .setLimit(integer.valueOf(limits.getLimitQueryRows() * 0.9))
                .toSOQL());
    }

    public List<Marketing_Tracking_Info__c> selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecentlyForShallowResolution() {
        Id mtiCallRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Marketing_Tracking_Info__c.SObjectType, 'Call');

        Datetime maximumHours = datetime.now().addMinutes(-20);

        set<String> callInfoResolutionStatusSet = new set<String>();
        callInfoResolutionStatusSet.add('NeedsProcessing');
        callInfoResolutionStatusSet.add('ProcessedCallNotResolved');

        return Database.query(newQueryFactory().setCondition(Marketing_Tracking_Info__c.RecordTypeId + ' = :mtiCallRecordTypeId'
                + ' AND ' + Marketing_Tracking_Info__c.Call_Info_Resolution_Status__c + ' in :callInfoResolutionStatusSet'
                + ' AND ' + Marketing_Tracking_Info__c.CreatedDate + ' >= :maximumHours')
                .setLimit(integer.valueOf(limits.getLimitQueryRows() * 0.9))
                .toSOQL());
    }

    public List<Marketing_Tracking_Info__c> selectWhereMarketingCampaignNotLinkedByCampaignValue(Set<String> campaignValueSet) {
        return Database.query(newQueryFactory().setCondition(Marketing_Tracking_Info__c.Campaign_Value__c + ' in :campaignValueSet'
                + ' AND ' + Marketing_Tracking_Info__c.Marketing_Campaign__c + ' = NULL ')
                .setLimit(integer.valueOf(limits.getLimitQueryRows() * 0.9))
                .toSOQL());
    }

    public List<Marketing_Tracking_Info__c> selectWhereMarketingSourceNotLinkedBySourceValue(Set<String> sourceValueSet) {
        return Database.query(newQueryFactory().setCondition(Marketing_Tracking_Info__c.Source_Value__c + ' in :sourceValueSet'
                + ' AND ' + Marketing_Tracking_Info__c.Marketing_Source__c + ' = NULL ')
                .setLimit(integer.valueOf(limits.getLimitQueryRows() * 0.9))
                .toSOQL());
    }

    public Database.QueryLocator selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue(Set<String> sourceValueSet) {
        return Database.getQueryLocator(newQueryFactory().setCondition(Marketing_Tracking_Info__c.Source_Value__c + ' in :sourceValueSet'
                + ' AND ' + Marketing_Tracking_Info__c.Marketing_Source__c + ' = NULL ')
                .toSOQL());
    }

    public Database.QueryLocator selectQueryLocatorWhereMarketingCampaignNotLinkedByCampaignValue(Set<String> campaignValueSet) {
        return Database.getQueryLocator(newQueryFactory().setCondition(Marketing_Tracking_Info__c.Campaign_Value__c + ' in :campaignValueSet'
                + ' AND ' + Marketing_Tracking_Info__c.Marketing_Campaign__c + ' = NULL ')
                .toSOQL());
    }

    public Database.QueryLocator selectQueryLocatorWhereMarketingFinancialPeriodNotLinkedAndEventRecent() {

        fflib_QueryFactory qf = newQueryFactory();
        qf.getOrderings().clear();

        return Database.getQueryLocator(qf.setCondition(Marketing_Tracking_Info__c.Marketing_Financial_Period__c + ' = NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Marketing_Source__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Marketing_Campaign__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Medium_Value__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c + ' = last_n_days:45 '
        )
                .addOrdering(Marketing_Tracking_Info__c.Domain_Value__c, fflib_QueryFactory.SortOrder.ASCENDING, false)
                .addOrdering(Marketing_Tracking_Info__c.Source_Value__c, fflib_QueryFactory.SortOrder.ASCENDING, false)
                .addOrdering(Marketing_Tracking_Info__c.Campaign_Value__c, fflib_QueryFactory.SortOrder.ASCENDING, false)
                .addOrdering(Marketing_Tracking_Info__c.Medium_Value__c, fflib_QueryFactory.SortOrder.ASCENDING, false)
                .addOrdering(Marketing_Tracking_Info__c.Term_Value__c, fflib_QueryFactory.SortOrder.ASCENDING, true)
                .addOrdering(Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c, fflib_QueryFactory.SortOrder.ASCENDING, false)
                .toSOQL());
    }

    public Database.QueryLocator selectQueryLocatorWhereMarketingExperienceVariationNotLinkedAndEventRecent() {
        return Database.getQueryLocator(newQueryFactory().setCondition(Marketing_Tracking_Info__c.Marketing_Exp_Variation__c + ' = NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Experiment_Value__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Experiment_Variation_Value__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c + ' != NULL '
                + ' AND ' + Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c + ' = last_n_days:10 '
        )
                .toSOQL());
    }
}