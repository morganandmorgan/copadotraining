public class mmlitifylrn_Credentials
    extends fflib_SObjectDomain
    implements mmlitifylrn_ICredentials
{
    public static mmlitifylrn_ICredentials newInstance(List<mmlitifylrn_ThirdPartyCredential__c> records)
    {
        return (mmlitifylrn_ICredentials) mm_Application.Domain.newInstance(records);
    }

    public static mmlitifylrn_ICredentials newInstance(Set<Id> recordIds)
    {
        return (mmlitifylrn_ICredentials) mm_Application.Domain.newInstance(recordIds);
    }

    public mmlitifylrn_Credentials(List<mmlitifylrn_ThirdPartyCredential__c> records)
    {
        super(records);
    }

    private mmlitifylrn_ThirdPartyCredential__c getRecordForFirmName(String firmName)
    {
        mmlitifylrn_ThirdPartyCredential__c recordForFirmName = null;

        for (mmlitifylrn_ThirdPartyCredential__c record : (list<mmlitifylrn_ThirdPartyCredential__c>)getRecords() )
        {
            if ( record.name.equalsIgnoreCase( firmName ) )
            {
                recordForFirmName = record;
                break;
            }
        }

        return recordForFirmName;
    }

    public void authenticateForFirm(litify_pm__Firm__c firm, string username, string password, mmlib_ISObjectUnitOfWork uow)
    {
        // make the callout with these credentials
        mmlitifylrn_AuthenticateCallout.Response resp = (mmlitifylrn_AuthenticateCallout.Response) new mmlitifylrn_AuthenticateCallout()
                                .setUsername( username )
                                .setPassword( password )
                                .execute()
                                .getResponse();

        mmlitifylrn_ThirdPartyCredential__c credential = getRecordForFirmName( firm.name );

        if ( credential == null )
        {
            credential = new mmlitifylrn_ThirdPartyCredential__c();

            getRecords().add( credential );
        }

        credential.name = firm.name;
        credential.Username__c = username;
        credential.RefreshToken__c = resp.getRefreshToken();
        credential.FirmExternalID__c = firm.litify_pm__ExternalId__c;
        credential.AccessToken__c = resp.getAccessToken();

        uow.register( credential );
    }

    public void refreshSessionForFirm(litify_pm__Firm__c firm, mmlib_ISObjectUnitOfWork uow)
    {
        mmlitifylrn_ThirdPartyCredential__c credential = getRecordForFirmName( firm.name );
        system.debug( 'mmlitifylrn_Credentials.refreshSessionForFirm : credential = ' + credential  );
        if ( credential != null )
        {
            mmlitifylrn_RefreshSessionCallout.Response resp = (mmlitifylrn_RefreshSessionCallout.Response) new mmlitifylrn_RefreshSessionCallout()
                                   .setRefreshToken( credential.RefreshToken__c )
                                   .execute()
                                   .getResponse();

            system.debug( 'mmlitifylrn_Credentials.refreshSessionForFirm : resp = ' + resp );

            credential.AccessToken__c = resp.getAccessToken();
            credential.RefreshToken__c = resp.getRefreshToken();

            uow.registerDirty( credential );
        }
    }

    public string getSessionTokenForFirm(litify_pm__Firm__c firm)
    {
        string accessTokenForFirm = null;
        system.debug( firm );
        mmlitifylrn_ThirdPartyCredential__c credential = getRecordForFirmName( firm.name );

        if ( credential != null )
        {
            accessTokenForFirm = credential.AccessToken__c;
        }

        return accessTokenForFirm;
    }

    public set<Decimal> firmExternalIdSet()
    {
        set<Decimal> output = new set<Decimal>();

        for (mmlitifylrn_ThirdPartyCredential__c record : (list<mmlitifylrn_ThirdPartyCredential__c>)getRecords() )
        {
            output.add( record.FirmExternalID__c );
        }

        return output;
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmlitifylrn_Credentials(sObjectList);
        }
    }


}