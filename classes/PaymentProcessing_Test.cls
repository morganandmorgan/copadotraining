@isTest
private class PaymentProcessing_Test {
    public PaymentProcessing_Test() {
    }

    // @isTest 
    // static void testCreateContainer(){
    //     //TODO:  Consider making the container non-instantiable:  
    //     PaymentProcessing container = null;
    //     container=new PaymentProcessing();
    //     System.assert(container!=null);
    // }

    @isTest 
    static void testCreatePaymentCollectionWrapper(){
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper=new PaymentProcessing.PaymentCollectionWrapper();
        System.assert(paymentCollectionWrapper!=null);
    }

    @isTest 
    static void testCreatePaymentCollectionWrapper0(){
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper=new PaymentProcessing.PaymentCollectionWrapper();
        System.assert(paymentCollectionWrapper!=null);
        paymentCollectionWrapper.paymentCollection = new Payment_Collection__c();
        System.assert(paymentCollectionWrapper.paymentCollection!=null);
        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();
        System.assert(paymentCollectionWrapper.payments!=null);
    }
    
    @isTest 
    static void test_Create_PaymentWrapper_0(){
        PaymentProcessing.PaymentWrapper paymentWrapper = null;
        paymentWrapper=new PaymentProcessing.PaymentWrapper();
        System.assert(paymentWrapper!=null);
        paymentWrapper.payment = new c2g__codaPayment__c();
        System.assert(paymentWrapper.payment!=null);
    }

    @isTest 
    static void test_Create_PaymentWrapper_1(){
        c2g__codaPayment__c payment = null;
        payment = new c2g__codaPayment__c();
        PaymentProcessing.PaymentWrapper paymentWrapper = null;
        paymentWrapper=new PaymentProcessing.PaymentWrapper(payment);
        System.assert(paymentWrapper!=null);        
        System.assert(paymentWrapper.payment!=null);
    }    

    @isTest 
    static void test_PaymentWrapper_CompareTo_0(){

        PaymentProcessing.PaymentWrapper paymentWrapper0 = null;
        paymentWrapper0=createPaymentWrapper();
        //TODO:  Fix:  System.TypeException: Invalid id value for this SObject type: 000000000000000000
        //paymentWrapper0.payment.Id=TestDataFactory_FFA.getFakeId(sObjectType.cod);
        System.assert(paymentWrapper0!=null);

        PaymentProcessing.PaymentWrapper paymentWrapper1 = null;
        paymentWrapper1=createPaymentWrapper();
        //paymentWrapper1.payment.Id=TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        System.assert(paymentWrapper1!=null);  

        System.assert(paymentWrapper1.compareTo(paymentWrapper0) == 1);
        System.assert(paymentWrapper0.compareTo(paymentWrapper1) == -1);
        System.assert(paymentWrapper0.compareTo(paymentWrapper0) == 0);

    }    

    @isTest 
    static PaymentProcessing.PaymentWrapper createPaymentWrapper(){
        //Test Factory fixture 
        PaymentProcessing.PaymentWrapper paymentWrapper = null;
        paymentWrapper=new PaymentProcessing.PaymentWrapper();
        System.assert(paymentWrapper!=null);
        paymentWrapper.payment = new c2g__codaPayment__c();
        //System.TypeException: Invalid id value for this SObject type: 000000000000000000
        paymentWrapper.payment.Id=TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        System.assert(paymentWrapper.payment!=null);
        return paymentWrapper;
    }

    @isTest 
    static void test_Create_PaymentSummaryWrapper_0(){
        
        PaymentProcessing.PaymentSummaryWrapper paymentsummaryWrapper = null;
        paymentsummaryWrapper=new PaymentProcessing.PaymentSummaryWrapper();
        System.assert(paymentsummaryWrapper!=null);  

        paymentsummaryWrapper.accountId=TestDataFactory_FFA.getFakeRecordId(Account.sObjectType);     
        paymentsummaryWrapper.paymentName='Test Payment';         
        paymentsummaryWrapper.paymentMediaControlId='000000000000000000'; 
        paymentsummaryWrapper.checkNumber='00000123'; 
        paymentsummaryWrapper.paymentSummary=new c2g__codaPaymentAccountLineItem__c();
        paymentsummaryWrapper.paymentMediaSummary=new c2g__codaPaymentMediaSummary__c();

        System.assert(paymentsummaryWrapper.paymentSummary!=null);  
        System.assert(paymentsummaryWrapper.paymentMediaSummary!=null);  

    }

    @isTest 
    static void test_Create_PaymentProcessException_0(){
        
        PaymentProcessing.PaymentProcessException error = null;
        error=new PaymentProcessing.PaymentProcessException();
        System.assert(error!=null);
    }    

    @isTest 
    static void test_Throw_PaymentProcessException_0(){
        Boolean caughtIt=false;
        try{
            throw new PaymentProcessing.PaymentProcessException('Testing - 1, 2, 2...');
        }catch(PaymentProcessing.PaymentProcessException pe){
            System.assert(pe!=null);
            System.debug(pe.getMessage());
            caughtIt=true;
        }
        System.assert(caughtIt);
    }       
    
    @isTest 
    static void test_Create_TransactionLineItemWrapper_0(){
        
        PaymentProcessing.TransactionLineItemWrapper wrapper = null;
        wrapper=new PaymentProcessing.TransactionLineItemWrapper();
        System.assert(wrapper!=null);

        wrapper.accountId = TestDataFactory_FFA.getFakeRecordId(Account.sObjectType);
        wrapper.accountName = 'accountName';
        wrapper.requestedById = 'requestedById';
        wrapper.requestedByName = 'requestedByName';
        wrapper.matterId = '000000000000000000';
        wrapper.matterRef = '000000000000000000';
        wrapper.vendorInvoiceNumber = 'vendorInvoiceNumber';
        wrapper.pinNumber = 'pinNumber';
        wrapper.invoiceDate = 'invoiceDate';
        wrapper.dueDate = 'dueDate';    

        wrapper.transactionLineItem=new c2g__codaTransactionLineItem__c();
                 
        System.assert(wrapper.transactionLineItem!=null);          

    }    


}