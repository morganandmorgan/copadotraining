/**
 * IncomingDarkHourTriggerHandler
 * @description Incoming Dark Hour Trigger Handler class.
 * @author 360 Degrees
 *
 * @author Jeff Watson
 * Formatted file.
 */
@isTest
public class IncomingDarkHourTriggerHandlerTest {

    static testMethod void unitTest1() {
        List<tdc_tsw__Message__c> listMessage = new List<tdc_tsw__Message__c>();
        Contact objContact = new Contact(LastName = 'Test');
        insert objContact;

        tdc_tsw__Message__c objMessage = new tdc_tsw__Message__c();
        objMessage.Name = 'Incoming';
        objMessage.tdc_tsw__Related_Object__c = 'Contact';
        objMessage.tdc_tsw__Related_Object_Id__c = objContact.id;
        objMessage.tdc_tsw__Sender_Number__c = '12062020076';
        objMessage.tdc_tsw__Message_Text_New__c = 'test' ;
        objMessage.tdc_tsw__ToNumber__c = '+18043136281';
        insert objMessage;

        listMessage.add(objMessage);
        IncomingDarkHourTriggerHandler.getIncomingMessage(listMessage);

        tdc_tsw__Message_Template__c objMessageTemplate = new tdc_tsw__Message_Template__c();
        objMessageTemplate.Name = 'OutOfOfficeTemplate';
        objMessageTemplate.tdc_tsw__SMSBodyNew__c = 'This is a template';
        insert objMessageTemplate;

        OutOfOffice__c objOutOfOffice = new OutOfOffice__c();
        objOutOfOffice.Name = 'Contact';
        objOutOfOffice.StartTime__c = '25';
        objOutOfOffice.EndTime__c = '20';
        objOutOfOffice.TemplateID__c = objMessageTemplate.Id;
        insert objOutOfOffice;
    }

    static testMethod void unitTest2() {
        List<tdc_tsw__Message__c> listMessage = new List<tdc_tsw__Message__c>();
        Contact objContact = new Contact(LastName = 'Test');
        insert objContact;

        tdc_tsw__Message__c objMessage = new tdc_tsw__Message__c();
        objMessage.Name = 'Incoming';
        objMessage.tdc_tsw__Related_Object__c = 'Contact';
        objMessage.tdc_tsw__Related_Object_Id__c = objContact.id;
        objMessage.tdc_tsw__Sender_Number__c = '12062020076';
        objMessage.tdc_tsw__Message_Text_New__c = 'test' ;
        objMessage.tdc_tsw__ToNumber__c = '18043136281';
        insert objMessage;

        listMessage.add(objMessage);
        IncomingDarkHourTriggerHandler.getIncomingMessage(listMessage);

        tdc_tsw__Message_Template__c objMessageTemplate = new tdc_tsw__Message_Template__c();
        objMessageTemplate.Name = 'OutOfOfficeTemplate';
        objMessageTemplate.tdc_tsw__SMSBodyNew__c = 'This is a template';
        insert objMessageTemplate;

        OutOfOffice__c objOutOfOffice = new OutOfOffice__c();
        objOutOfOffice.Name = 'Contact';
        objOutOfOffice.StartTime__c = '0';
        objOutOfOffice.EndTime__c = '0';
        objOutOfOffice.TemplateID__c = objMessageTemplate.Id;
        insert objOutOfOffice;
    }

    @isTest
    public Static void Test3() {

        tdc_tsw__Message_Template__c objMessageTemplate = new tdc_tsw__Message_Template__c();
        objMessageTemplate.Name = 'OutOfOfficeTemplate';
        objMessageTemplate.tdc_tsw__SMSBodyNew__c = 'This is a template';
        insert objMessageTemplate;

        OutOfOffice__c objOutOfOffice = new OutOfOffice__c();
        objOutOfOffice.Name = 'Contact';
        objOutOfOffice.StartTime__c = '0';
        objOutOfOffice.EndTime__c = '0';
        objOutOfOffice.Include_Weekends__c = true;
        objOutOfOffice.TemplateID__c = objMessageTemplate.Id;
        insert objOutOfOffice;

        List<tdc_tsw__Message__c> listMessage = new List<tdc_tsw__Message__c>();
        Contact objContact = new Contact(LastName = 'Test');
        insert objContact;

        tdc_tsw__Message__c objMessage = new tdc_tsw__Message__c();
        objMessage.Name = 'Incoming';
        objMessage.tdc_tsw__Related_Object__c = 'Contact';
        objMessage.tdc_tsw__Related_Object_Id__c = objContact.id;
        objMessage.tdc_tsw__Sender_Number__c = '12062020076';
        objMessage.tdc_tsw__Message_Text_New__c = 'test' ;
        objMessage.tdc_tsw__ToNumber__c = '18043136281';
        insert objMessage;

        listMessage.add(objMessage);
        IncomingDarkHourTriggerHandler.getIncomingMessage(listMessage);
    }
}