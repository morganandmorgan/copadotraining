public class mmcommon_AccountsNextCallDateAction implements mmlib_IAction {

    public void mmcommon_AccountsNextCallDateAction() { }

    private List<Task> records = null;
    private Map<Id,Task> oldMap = null;

    public mmlib_IAction setRecordsToActOn(List<SObject> recordsToActOn) {
        records = (List<Task>) recordsToActOn;
        return this;
    }

    public void setOldMap(Map<Id,Task> oldMapParam) {
        oldMap = oldMapParam;
    }
    
    public Boolean run() {
        if (records == null || records.isEmpty()) {
            return true;
        }

        Map<Id, List<Task>> tasksByIntakeIdMap = new Map<Id, List<Task>>();
        Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>();
        for (Task t : records) {
            
            Boolean closedFlag = (oldMap==null && t.isClosed==false) || (oldMap!=null && (t.isClosed==false || oldMap.get(t.id).isClosed==false));
            
            if (closedFlag && t.ActivityDate != null && t.ActivityDate >= Date.today()) {
                tasksByIntakeIdMap.put(t.WhatId, new List<Task>());
                intakeMap.put(t.WhatId, null);
            }
        }

        if (intakeMap.keySet().size() != 0) {
            tasksByIntakeIdMap =
                    (Map<Id, List<Task>>)
                            mmlib_Utils.generateSObjectMapByIdField(
                                    mmcommon_TasksSelector.newInstance().selectOpenTasksByRelatedIdSet(tasksByIntakeIdMap.keySet()),
                                    Task.WhatId);

            Map<Id, Account> accountMap = new Map<Id, Account>();
            for (Intake__c intake : mmintake_IntakesSelector.newInstance().selectById(intakeMap.keySet())) {
                intakeMap.put(intake.Id, intake);
                accountMap.put(intake.Client__c, null);
            }

            for (Account acct : mmcommon_AccountsSelector.newInstance().selectById(accountMap.keySet())) {
                accountMap.put(acct.Id, acct);
            }

            for (Intake__c intake : intakeMap.values()) {
                if (intake != null) {
                    Account acct = accountMap.get(intake.Client__c);

                    if (acct != null) {
                        Task soonestTask = null;
                        if (tasksByIntakeIdMap.get(intake.Id) != null && !tasksByIntakeIdMap.get(intake.Id).isEmpty()) {
                            // The related query is ordered by ascending ActivityDate,
                            // so the first in the list will be the soonest, open one.
                            soonestTask = tasksByIntakeIdMap.get(intake.Id).get(0);
                        }

                        acct.NextCallDateTime__c = (soonestTask != null) ? soonestTask.ReminderDateTime : null;
                    }
                }
            }

            if (!isTest) update accountMap.values();
        } //if intakeMap...size

        return true;
    }

    // ============================= Test Context ====================================
    @TestVisible
    private static Boolean isTest = false;
}