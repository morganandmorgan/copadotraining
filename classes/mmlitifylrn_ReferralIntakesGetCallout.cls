/**
 *  This class handles fetching a specific referral by id from the Litify API.
 *  @see http://litify-swagger.bitballoon.com/#!/referral_intakes/showReferralIntakeAsAdmin
 *  @usage mmlitifylrn_ReferralIntakesGetCallout.Response resp = (mmlitifylrn_ReferralIntakesGetCallout.Response) new mmlitifylrn_ReferralsSyncCallout()
 *          .setLRNExternalId(String)
 *          .setAuthorization(mmlitifylrn_CalloutAuthorization)
 *          .debug()
 *          .execute()
 *          .getResponse();
 */
public class mmlitifylrn_ReferralIntakesGetCallout
        extends mmlitifylrn_BaseGETV1Callout
{
    private Map<String, String> paramMap = new map<String, String>();
    private mmlitifylrn_ReferralIntakesGetCallout.Response resp = new mmlitifylrn_ReferralIntakesGetCallout.Response();
    private String LRNExternalId = null;

    public override map<String, String> getParamMap()
    {
        return paramMap;
    }

    public override String getPathSuffix()
    {
        return '/referral_intakes/' + this.LRNExternalId;
    }

    public mmlitifylrn_ReferralIntakesGetCallout setLRNExternalId(String id)
    {
        this.LRNExternalId = id;
        return this;
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        httpResponse httpResp = super.executeCallout();

        if (this.isDebugOn)
        {
            system.debug(httpResp.getStatusCode());
            system.debug(httpResp.getStatus());
        }

        String jsonResponseBody = httpResp.getBody();

        if (this.isDebugOn)
        {
            system.debug(json.deserializeUntyped(jsonResponseBody));
        }

        if (httpResp.getStatusCode() >= 200
                && httpResp.getStatusCode() < 300)
        {
            System.debug(httpResp.getBody());
            resp = (mmlitifylrn_ReferralIntakesGetCallout.Response) json.deserialize('{ "referral": ' + httpResp.getBody() + '}', mmlitifylrn_ReferralIntakesGetCallout.Response.class);
        } else
        {
            throw new mmlitifylrn_Exceptions.CalloutException(httpResp);
        }

        return this;
    }

    public class Response implements mmlib_BaseCallout.CalloutResponse
    {
        public mmlitifylrn_LRNModels.ReferralIntakeBasicData referral; // = new mmlitifylrn_LRNModels.ReferralIntakeBasicData();

        public Integer getTotalNumberOfRecordsFound()
        {
            return 1;
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    private void validateCallout()
    {
        if(String.isBlank(this.LRNExternalId))
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Litify Referral Network requires an id parameter.  Please use the setLRNExternalId(String) method before calling the execute() method.');
        }
    }
}