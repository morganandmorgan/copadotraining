@isTest
private class IntakeFlowControllerTest {

  @TestSetup
  private static void setup() {
    Account acc = new Account(LastName='Test Account');
    insert acc;

    List<Intake__c> intakes = new List<Intake__c>();
    intakes.add(createIntake(acc.Id, acc.Id, acc.Id, getCccRecordType().Id));
    for (RecordType rt : getNonCccRecordTypes()) {
      intakes.add(createIntake(acc.Id, acc.Id, acc.Id, rt.Id));
    }
    insert intakes;
  }

  @isTest static void getAccountPageInfo_PassedId_ReturnsJson() {
    Account acct = getAccount();
    String js = IntakeFlowController.getAccountPageInfo(acct.Id);

    Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(js);
    System.assertEquals(m.get('objectId'), acct.PersonContactId);
    System.assertEquals(m.get('accountId'), acct.Id);
  }
  @isTest static void getIntakePageInfo_PassedId_ReturnsJson() {
    Intake__c intake = getIntake();
    String js = IntakeFlowController.getIntakePageInfo(intake.Id);

    Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(js);
    System.assertEquals(m.get('objectId'), intake.Id);
    System.assertEquals(m.get('displayName'), 'Intake');
  }

  private static Intake__c createIntake(Id clientId, Id callerId, Id injuredPartyId) {
    return new Intake__c(Client__c = clientId, Case_Type__c = 'TestType', Caller__c = callerId, Injured_Party__c = injuredPartyId, Caller_is_Injured_Party__c = 'Yes');
  }

  private static Intake__c createIntake(Id clientId, Id callerId, Id injuredPartyId, Id recordTypeId) {
    Intake__c intake = createIntake(clientId, callerId, injuredPartyId);
    intake.RecordTypeId = recordTypeId;
    return intake;
  }

  private static Account getAccount() {
    return [SELECT Id, LastName, PersonContactId FROM Account];
  }

  private static Intake__c getIntake() {
    return getIntake('CCC');
  }

  private static Intake__c getIntake(String recordTypeDevName) {
    return [SELECT Id, Client__c, Case_Type__c, Litigation__c, Caller_is_Injured_Party__c FROM Intake__c WHERE RecordType.DeveloperName = :recordTypeDevName];
  }

  private static List<Task> getTasks() {
    return [SELECT Id, WhoId, WhatId, Description, CallType, CallDisposition FROM Task];
  }

  private static RecordType getCccRecordType() {
    return [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Intake__c' AND DeveloperName = 'CCC'];
  }

  private static List<RecordType> getNonCccRecordTypes() {
    return [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Intake__c' AND DeveloperName != 'CCC'];
  }

  private static PageReference executeRedirect_CallerVsInjuredPartyIsBlank(Id intakeId) {
    Database.update(new Intake__c(
        Id = intakeId,
        Caller_is_Injured_Party__c = null
      ));

    ApexPages.currentPage().getParameters().put('intakeId', intakeId);
    IntakeFlowController controller = new IntakeFlowController();

    PageReference pr = controller.RefreshIntake();

    return pr;
  }

  private static PageReference executeRedirect_CallerVsInjuredPartyBlankRelationship(Id intakeId) {
    Database.update(new Intake__c(
        Id = intakeId,
        Caller_is_Injured_Party__c = 'No',
        Injured_party_relationship_to_Caller__c = null
      ));

    ApexPages.currentPage().getParameters().put('intakeId', intakeId);
    IntakeFlowController controller = new IntakeFlowController();

    PageReference pr = controller.RefreshIntake();

    return pr;
  }

  private static PageReference executeRedirect_CallerVsInjuredPartyAllPopulated(Id intakeId) {
     Database.update(new Intake__c(
        Id = intakeId,
        Caller_is_Injured_Party__c = 'No',
        Injured_party_relationship_to_Caller__c = 'Parent'
      ));

    ApexPages.currentPage().getParameters().put('intakeId', intakeId);
    IntakeFlowController controller = new IntakeFlowController();

    PageReference pr = controller.RefreshIntake();

    return pr;
  }

	@isTest static void FinishLocation_NoIntake_IsBlank() {
    Test.startTest();
    IntakeFlowController controller = new IntakeFlowController();
    Test.stopTest();

    System.assertEquals(null, controller.IntakeID);
    System.assertEquals('/', controller.finishLocation.getUrl());
	}

  @isTest static void FinishLocation_HasIntake() {
    Intake__c intake = getIntake();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);

    Test.startTest();
    IntakeFlowController controller = new IntakeFlowController();
    Test.stopTest();

    System.assertEquals(intake.Id, controller.IntakeID);
    System.assertEquals('/' + intake.Id, controller.finishLocation.getUrl());
  }

  @isTest
  private static void redirect_CallerVsInjuredPartyIsBlank_CCC() {
    Intake__c intake = getIntake('CCC');
    
    Test.startTest();
    PageReference pr = executeRedirect_CallerVsInjuredPartyIsBlank(intake.Id);
    Test.stopTest();

    System.assertNotEquals(null, pr);
    System.assertEquals('/apex/ContactSearch?intakeId='+intake.Id, pr.getUrl());
  }

  @isTest
  private static void redirect_CallerVsInjuredPartyIsBlank_NonCCC() {
    List<RecordType> nonCccRecordTypes = getNonCccRecordTypes();

    Test.startTest();
    for (RecordType rt : nonCccRecordTypes) {
      Intake__c intake = getIntake(rt.DeveloperName);

      PageReference pr = executeRedirect_CallerVsInjuredPartyIsBlank(intake.Id);

      System.assertEquals(null, pr, rt);
    }
    Test.stopTest();
  }

  @isTest
  private static void redirect_CallerVsInjuredParty_BlankRelationship_CCC() {
    Intake__c intake = getIntake('CCC');

    Test.startTest();
    PageReference pr = executeRedirect_CallerVsInjuredPartyBlankRelationship(intake.Id);
    Test.stopTest();

    System.assertEquals('/apex/ContactSearch?intakeId='+intake.Id, pr.getUrl());
  }

  @isTest
  private static void redirect_CallerVsInjuredParty_BlankRelationship_NonCCC() {
    List<RecordType> nonCccRecordTypes = getNonCccRecordTypes();

    Test.startTest();
    for (RecordType rt : nonCccRecordTypes) {
      Intake__c intake = getIntake(rt.DeveloperName);

      PageReference pr = executeRedirect_CallerVsInjuredPartyBlankRelationship(intake.Id);

      System.assertEquals(null, pr, rt);
    }
    Test.stopTest();
  }

  @isTest
  private static void redirect_CallerVsInjuredParty_AllPopulated_CCC() {
    Intake__c intake = getIntake('CCC');

    Test.startTest();
    PageReference pr = executeRedirect_CallerVsInjuredPartyAllPopulated(intake.Id);
    Test.stopTest();

    System.assertEquals(null, pr);
  }

  @isTest
  private static void redirect_CallerVsInjuredParty_AllPopulated_NonCCC() {
    List<RecordType> nonCccRecordTypes = getNonCccRecordTypes();

    Test.startTest();
    for (RecordType rt : nonCccRecordTypes) {
      Intake__c intake = getIntake(rt.DeveloperName);

      PageReference pr = executeRedirect_CallerVsInjuredPartyAllPopulated(intake.Id);

      System.assertEquals(null, pr, rt);
    }
    Test.stopTest();
  }

  @isTest
  private static void refreshIntake() {
    Intake__c intake = getIntake();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);
    IntakeFlowController controller = new IntakeFlowController();

    System.assertEquals(null, controller.Intake);

    Test.startTest();
    PageReference pr = controller.refreshIntake();
    Test.stopTest();

    System.assertEquals(null, pr);
    System.assertEquals(intake.Id, controller.Intake.Id);
  }

  @isTest
  private static void personAccountIsKnown() {
    Intake__c intake = getIntake();
    Account client = getAccount();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);
    IntakeFlowController controller = new IntakeFlowController();
    controller.AccountID = client.Id;

    Test.startTest();
    System.assertEquals(client.Id, controller.AccountID);
    Test.stopTest();
  }

  @isTest
  private static void saveComment_NoIntakeId() {
    Account personAccount = getAccount();
    Intake__c intake = getIntake();

    IntakeFlowController controller = new IntakeFlowController();
    controller.AccountID = personAccount.Id;

    Test.startTest();
    controller.SaveComment();
    Test.stopTest();

    List<Task> tasks = getTasks();
    System.assertEquals(0, tasks.size());
  }

  @isTest
  private static void saveComment_NoContactId() {
    Account personAccount = getAccount();
    Intake__c intake = getIntake();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);
    IntakeFlowController controller = new IntakeFlowController();

    Test.startTest();
    controller.SaveComment();
    Test.stopTest();

    List<Task> tasks = getTasks();
    System.assertEquals(0, tasks.size());
  }

  @isTest
  private static void saveComment_NoCommentGiven() {
    Account personAccount = getAccount();
    Intake__c intake = getIntake();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);
    IntakeFlowController controller = new IntakeFlowController();
    controller.AccountID = personAccount.Id;

    controller.Comments = '';

    Test.startTest();
    controller.SaveComment();
    Test.stopTest();

    List<Task> tasks = getTasks();
    System.assertEquals(0, tasks.size());
  }

  @isTest
  private static void saveComment_IsIncoming() {
    Account personAccount = getAccount();
    Intake__c intake = getIntake();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);
    IntakeFlowController controller = new IntakeFlowController();
    controller.AccountID = personAccount.Id;

    controller.Comments = 'Sample Comments';
    controller.IsIncoming = true;

    Test.startTest();
    controller.SaveComment();
    Test.stopTest();

    List<Task> tasks = getTasks();
    System.assertEquals(1, tasks.size());

    for (Task t : tasks) {
        System.assertEquals(personAccount.PersonContactId, t.WhoId);
        System.assertEquals(intake.Id, t.WhatId);
        System.assertEquals('Sample Comments', t.Description);
        System.assertEquals('Inbound', t.CallType);
        System.assertEquals('Correct Contact', t.CallDisposition);
    }
  }

  @isTest
  private static void saveComment_IsNotIncoming_CallTypeOverwritten() {
    Account personAccount = getAccount();
    Intake__c intake = getIntake();

    ApexPages.currentPage().getParameters().put('intakeId', intake.Id);
    IntakeFlowController controller = new IntakeFlowController();
    controller.AccountID = personAccount.Id;

    controller.Comments = 'Sample Comments';
    controller.IsIncoming = false;
    controller.CommentTask.CallType = 'Outbound';

    Test.startTest();
    controller.SaveComment();
    Test.stopTest();

    List<Task> tasks = getTasks();
    System.assertEquals(1, tasks.size());

    for (Task t : tasks) {
        System.assertEquals(personAccount.PersonContactId, t.WhoId);
        System.assertEquals(intake.Id, t.WhatId);
        System.assertEquals('Sample Comments', t.Description);
        System.assertEquals(null, t.CallType);
    }
  }

/*
  * The following test throws an exception: System.FlowException: Interview not started
  * according to http://salesforce.stackexchange.com/questions/562/test-class-for-visual-workflow
  * there's no way to test flow since there isn't an API to call (though that article's from 2012)
  * keeping this here as a
  * TODO
  *
	@isTest static void FinishLocation_WithIntake_IsIntakeId() {
    Intake__c intake = getIntake();

    Map<String, Object> params = new Map<String, Object>();
    params.put('IntakeID', intake.ID);

    controller.flowInstance = new Flow.Interview.Contact_Information_Capture(params);

    System.assertEquals(intake.ID, controller.IntakeID);
    System.assertEquals('', controller.finishLocation.getUrl());
	}
*/
}