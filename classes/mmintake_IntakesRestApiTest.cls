@IsTest
private class mmintake_IntakesRestApiTest {

    /*
        Also covers mmmarketing_TrackingInfoFactory (since it had no coverages and I needed to get it deployed with this)
    */

    @IsTest
    static void mainTest() {
	/* We are just going for coverage here, no asserts.
	 * We are trying to eliminate as much "x = x + 1;" as we can
	 */ 

		mmintake_IntakesRestApi testClass = new mmintake_IntakesRestApi();
        
        mmintake_IntakesRestApi.PostRQ post = new mmintake_IntakesRestApi.PostRQ();

        post.additional_marketing_source_information = 'info';
        post.amount_paid = '123.55';
        post.applicants_condition_present_12_months = 'Yes';
        post.case_type = 'Automobile Accident';
        post.case_type_specific_questions = 'question:answer';
        post.city_billing = 'Orlando';
        post.city_of_incident = 'Orlando';
        post.claim_is_filed = 'Yes';
        post.claim_outcome = 'Accepted';
        post.client_age = '55';
        post.country_billing = 'US';
        post.damage_cause = 'Other';
        post.damage_exceeds_deductible = 'Yes';
        post.damage_from_hurricane = 'Yes';
        post.damaged_home_address = 'address';
        post.damaged_house_city = 'Orlando';
        post.damaged_house_state = 'FL';
        post.damaged_house_zipcode = '32800';
        post.damages_exceed_100000 = 'Yes';
        post.date_injury_related_to_drug_discovered = '2020-01-01';
        post.date_of_follow_up_surgery = '2020-01-01';
        post.date_of_original_incident = '2020-01-01';
        post.date_of_original_injury = '2020-01-01';
        post.date_you_started_taking_drug = '2020-01-01';
        post.date_you_stopped_taking_drug = '2020-01-01';
        post.deceased_date = '2020-01-01';
        post.description_of_injuries = 'description';
        post.diagnosed_with_cancer_of = 'Other';
        post.diagnosed_with_heart_failure = 'Yes';
        post.did_device_fracture_or_migrate = 'Yes';
        post.disabilities_prevent_ability_to_work = 'Yes';
        post.email = 'me@here.now';
        post.experiment_name = 'name';
        post.experiment_variation = 'var';
        post.external_id = 'Id10T';
        post.first_name = 'first';
        post.ga_client_id = 'gaId';
        post.have_you_used_the_drug = 'Yes';
        post.incident_date = '2020-01-01';
        post.incident_description = 'description';
        post.injuries_suffered = 'Yes';
        post.input_channel = 'Migration';
        post.insurance_company = 'insurance';
        post.intake_status_detail_suggested = 'SMS';
        post.intake_status_suggested = 'Lead';
        post.last_name = 'last';
        post.lead_details = 'details';
        post.litigation = 'Mass Tort';
        post.marketing_source = 'TV';
        post.marketing_sub_source = 'Attorney at Law';
        post.mobile = '(555) 123-4567';
        post.newsletter_signup = 'true';
        post.number_of_smoke_detectors = '6';
        post.phone = '(555) 123-4567';
        post.policy_subtype = 'Home Owner\'s Insurance';
        post.policy_type = 'Property Insurance';
        post.postal_code_billing = '32800';
        post.preferred_time_to_call = '1pm – 6pm';
        post.questions_case_type = 'question:answer';
        post.state_billing = 'FL';
        post.state_of_incident = 'FL';
        post.status = 'Lead';
        post.status_detail = 'SMS';
        post.still_using_the_drug = 'Yes';
        post.street_billing = 'street billing';
        post.submission_id = 'abc123';
        post.tracked_landing_page = 'landingPage';
        post.turn_down_reason = 'reason';
        post.under_80_at_time_of_injury = 'Yes';
        post.venue = 'FL - Orlando';

        post.tracked_landing_page = 'https://www.forthepeople.com/?utm_Source=utmSource&ads_adsetid=123&ads_adsid=456&utm_medium=mediumValue&gclid=g123';


 		Map<SObjectField, String> reqMap = post.getRequestMap();

        
        Map<SObjectType, List<SObject>> objectMapByObjectType = new Map<SObjectType, List<SObject>>();
        
        Intake__c intake = new Intake__c();
        List<Intake__c> intakeList = new List<Intake__c>();
        intakeList.add(intake);
        
        Account acct = new Account();
        List<Account> accountList = new List<Account>();
        accountList.add(acct);
        
        objectMapByObjectType.put(Intake__c.SObjectType, intakeList);
        objectMapByObjectType.put(Account.SObjectType, accountList);

		mmintake_IntakesRestApi.PostRS postrs = new mmintake_IntakesRestApi.PostRS(objectMapByObjectType);

        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
         
        req.httpMethod = 'POST'; 
        req.requestBody = Blob.valueOf(JSON.serialize(post));
        RestContext.request = req;
        RestContext.response = res;

        mmintake_IntakesRestApi.create();

        //System.debug('mainTest');
        //System.debug(RestContext.response);
        //System.debug(RestContext.response.responseBody.toString());

		req = new RestRequest();
        req.httpMethod = 'GET'; 
        
        try {
        	mmintake_IntakesRestApi.get();
        } catch (Exception e) {
            //we are expecting an exception
        }        

        req = new RestRequest();
        req.httpMethod = 'PATCH'; 
		mmintake_IntakesRestApi.updateObject();

        req = new RestRequest();
        req.httpMethod = 'POST'; 
		mmintake_IntakesRestApi.replaceObject();

        req = new RestRequest();
        req.httpMethod = 'POST'; 
        req.requestURI = '/intake/v1.0/1234/status_suggestion/status_change_key';

        mmintake_IntakesRestApi.updateExecutionForV0100(req, res);
        
        Exception exc = new mmintake_IntakesServiceExceptions.ValidationException();
        exc.setMessage('test');
    	mmintake_IntakesRestApi.convertExceptionToResponse(exc, res);
        
        exc = new mmintake_IntakesServiceExceptions.DuplicateSubmissionIdException();
        exc.setMessage('test');
    	mmintake_IntakesRestApi.convertExceptionToResponse(exc, res);

        
    	Map<String, mmintake_IntakesRestApi.StatusChangeKeyFieldValueWrapper> sValuesMap = mmintake_IntakesRestApi.getStatusChangeKeyFieldValues();
        
		mmintake_IntakesRestApi.SObjectFieldPair sofp = new mmintake_IntakesRestApi.SObjectFieldPair(Account.SObjectType,Account.Name,false);

		Map<String, mmintake_IntakesRestApi.SObjectFieldPair> fieldMap = new Map<String, mmintake_IntakesRestApi.SObjectFieldPair>();
        fieldMap.put('Name',sofp);
        Map<String, Object> dataMap = new Map<String, Object>();
        dataMap.put('Name', new Account());
            
    	Map<SObjectField, String> msos = mmintake_IntakesRestApi.translate(fieldMap, dataMap);
        
        Map<SObjectField, String> dataMap2 = new Map<SObjectField, String>();
        dataMap2.put(Account.name,'Name');
        List<String> invalidReasons = new List<String>();
    	Boolean bTest = mmintake_IntakesRestApi.validDataMap( dataMap2, fieldMap, invalidReasons);
        
        //Additional coverage for mmmarketing_TrackingInfoFactory

        Map<String,String> hubSpotInfo = new Map<String,String>();
        hubSpotInfo.put('test','test');

        mmmarketing_TrackingInfoFactory.ITrackingInfoFactory tif = mmmarketing_TrackingInfoFactory.getInstance();
        tif.generateNewFromHubSpotInfo(new Intake__c(), hubSpotInfo );

        tif.setNewRecordInstance(new Marketing_Tracking_Info__c() );

        tif.generateNewCall( new Intake__c(), '(407) 555-1234', Datetime.now() );

    } //mainTest
    
    @IsTest
    static void GettingVersionNumber() {
        System.assertEquals('1.0', mmintake_IntakesRestApi.getVersionNumber('/intake/v1.0'));
        System.assertEquals('2.2', mmintake_IntakesRestApi.getVersionNumber('/intake/v2.2/jklsjd'));
        System.assertEquals('', mmintake_IntakesRestApi.getVersionNumber('/intake/jdlksjflk'));
        System.assertEquals('22.33', mmintake_IntakesRestApi.getVersionNumber('/intake/V22.33/lskdf/lksjdlfj'));
    }

    @IsTest
    static void ResolveGeneratingAttorney_Normal() {
        Contact ctc = new Contact();
        ctc.Email = 'abc@slkdflksjlf.org';
        ctc.FirstName = 'Fred';
        ctc.Id = fflib_IDGenerator.generate(Contact.SObjectType);

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmcommon_IContactsSelector mockSelector = (mmcommon_ContactsSelector) mocks.mock(mmcommon_ContactsSelector.class);

        mocks.startStubbing();
        mocks.when(mockSelector.sObjectType()).thenReturn(Contact.SObjectType);
        mocks.when(mockSelector.selectByEmail(new Set<String>{
                ctc.Email
        })).thenReturn(new List<Contact>{
                ctc
        });
        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockSelector);
        Map<String, Object> dataMap = new Map<String, Object>();
        dataMap.put('generating_attorney', ctc.Email);

        mmintake_IntakesRestApi.resolveGeneratingAttorney(dataMap);
        System.assertEquals(ctc.Id, dataMap.get('generating_attorney'));
    }
} //class