/**
 * DocsFileInfoDomain
 * @description Domain class for litify_docs__File_Info__c.
 * @author Matt Terrill
 * @date 1/9/2020
 */
public without sharing class DocsFileInfoDomain extends fflib_SObjectDomain {

    private List<litify_docs__File_Info__c> docsFileInfos;

    public DocsFileInfoDomain() {

    } //constructor
        
    public DocsFileInfoDomain(List<litify_docs__File_Info__c> docsFileInfos) {
        super(docsFileInfos);
        this.docsFileInfos = docsFileInfos;
    } //constructor()

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new DocsFileInfoDomain(records);
        }
    } //IConstructable
    
    public override void onBeforeInsert() {
        setToFrom();
    } //onBeforeInsert

    /* The name field is in the format:
     * "toName||fromName.ext" (ext=file extension)
     */ 
    private void setToFrom() { //before insert
        
        Set<Id> templateIds = new Set<Id>();
        for (litify_docs__File_Info__c fileInfo : docsFileInfos) {
            if ( !String.isBlank(fileInfo.litify_docs__Origin_Merge_Template__c) ) {
                templateIds.add(fileInfo.litify_docs__Origin_Merge_Template__c);          
            }
        } //for

        DocsTemplateSelector selector = new DocsTemplateSelector();
        Map<Id, litify_docs__Template__c> templateMap = new Map<Id, litify_docs__Template__c>( selector.selectById(templateIds) );
        
        //split the name field into the seperate To/From fields, and build a list of template ids
        for (litify_docs__File_Info__c fileInfo : docsFileInfos) {
            if ( !String.isBlank(fileInfo.name) ) {
                List<String> parts = fileInfo.name.split('\\|{2}'); // split on || (two pipes)
                if (parts.size() >= 2) {
                    fileInfo.litify_docs__To__c = parts[0].trim();
                    fileInfo.litify_docs__From__c = parts[1].substringBeforeLast('.').trim();
                    //change the name field to be the template name + file extension
                    if (templateMap.containsKey(fileInfo.litify_docs__Origin_Merge_Template__c)) {
                        String ext = fileInfo.name.substringAfterLast('.').trim();
                        fileInfo.name = templateMap.get(fileInfo.litify_docs__Origin_Merge_Template__c).name + '.' + ext;
                    }
                } // 2 parts
            } //if not blank
        } //for
    } //setToFrom

} //class