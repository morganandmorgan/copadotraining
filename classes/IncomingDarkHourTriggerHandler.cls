/**
 * IncomingDarkHourTriggerHandler
 * @description Incoming Dark Hour Trigger Handler class.
 * @author 360 Degrees
 *
 * @author Jeff Watson
 * Clean up of obsolete debugging and formatted file.  This should be moved to a Domain class in the future.
 */
public class IncomingDarkHourTriggerHandler {

    public static void getIncomingMessage(List<tdc_tsw__Message__c> listMessage) {
        try {
            if (listMessage.size() > 0) {
                tdc_tsw__Message__c objMessage = listMessage[0];
                String jsonString = Json.serialize(objMessage);
                if (!System.isFuture()) {
                    sendSMS(jsonString);
                }
            }
        } catch (Exception e) {
            System.debug('Exception =====' + e.getMessage() + 'Line Number =========' + e.getLineNumber());
        }
    }

    @future(callout=true)
    public static void sendSMS(String messageJson) {
        try {
            Map<String, Object> mapStrObj = new Map<String, Object>();
            mapStrObj = (Map<String, Object>) JSON.deserializeUntyped(messageJson);

            if (mapStrObj != null && mapStrObj.containsKey('Name') && mapStrObj.get('Name') != null && mapStrObj.get('Name') == 'Incoming' && mapStrObj.get('tdc_tsw__Sender_Number__c') != null && mapStrObj.get('tdc_tsw__ToNumber__c') != null && mapStrObj.get('tdc_tsw__Related_Object_Id__c') != null && mapStrObj.get('tdc_tsw__Related_Object__c') != null) {
                String senderNumber = String.valueOf(mapStrObj.get('tdc_tsw__Sender_Number__c'));
                String toNumber = String.valueOf(mapStrObj.get('tdc_tsw__ToNumber__c'));
                String relatedObjectId = String.valueOf(mapStrObj.get('tdc_tsw__Related_Object_Id__c'));
                String relatedObject = String.valueOf(mapStrObj.get('tdc_tsw__Related_Object__c'));
                OutOfOffice__c objOutOfOffice = OutOfOffice__c.getInstance(relatedObject);

                if (objOutOfOffice != null) {
                    String strName = String.valueOf(objOutOfOffice.get('Name'));
                    Integer startTime = Integer.valueOf(objOutOfOffice.get('StartTime__c'));
                    Boolean isIncludeWeekends = Boolean.valueOf(objOutOfOffice.get('Include_Weekends__c'));// if this value is true then all the 7 days auto reply will work.
                    Integer endTime = Integer.valueOf(objOutOfOffice.get('EndTime__c'));
                    String templateId = String.valueOf(objOutOfOffice.get('TemplateID__c'));
                    if (objOutOfOffice != null && startTime != null && endTime != null && templateId != null && relatedObject != null) {
                        Integer hour = System.now().hour();
                        String dayName = System.now().format('EEE');
                        if (startTime != null && endTime != null && hour != null) {
                            if (endTime == 0) {
                                endTime = 24;
                            }
                            if (hour == 0) {
                                hour = 24;
                            }
                            if (startTime > endTime) {
                                hour = (hour < 12) ? (hour + 24) : hour;
                                Integer diff = 24 - startTime;
                                endTime = startTime + diff + endTime;
                            }
                            if ((hour >= startTime && hour < endTime) || (isIncludeWeekends && (dayName == 'Sat' || dayName == 'Sun'))) {
                                if (senderNumber != null && toNumber != null) {
                                    tdc_tsw.GlobalSMSSender.isEnableDarkHour = false;
                                    tdc_tsw.GlobalSMSSender.isSyncCall = true;
                                    if (toNumber.contains('+')) {
                                        toNumber = toNumber.replace('+', '');
                                    }
                                    tdc_tsw.GlobalSMSSender.senderNumber = toNumber;
                                    tdc_tsw.GlobalSMSSender.sendSyncSmsWithPhoneNumberAndTemplateId(senderNumber, templateId, relatedObjectId);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.debug('Exception =====' + e.getMessage() + 'Line Number =========' + e.getLineNumber());
        }
    }

}