@isTest
public class PayableInvoiceCreateExpenses_Batch_TEST {
    public static litify_pm__Matter__c matter;
    public static litify_pm__Expense_Type__c expenseType;
    public static c2g__codaAccountingSettings__c accountingSettings;
    public static List<c2g__codaBankAccount__c> bankAccountList;

    static void setupData(){
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        Map<c2g__codaPurchaseInvoice__c, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> testPIN_Map = TestDataFactory_FFA.payableInvoices_Expense;

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        //mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        
        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        INSERT expenseType;
        
    }

    /*--------------------------------------------------------------------
        START TEST METHODS
    --------------------------------------------------------------------*/
    @isTest static void testExpenseCreationBatch(){

        setupData();
        List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLines = [
            SELECT Id, Matter__c, Expense_Type__c 
            FROM c2g__codaPurchaseInvoiceExpenseLineItem__c];
        for(c2g__codaPurchaseInvoiceExpenseLineItem__c line : pinLines){
            line.Matter__c = matter.Id;
            line.Expense_Type__c = expenseType.Id;
        }
        update pinLines;

        c2g__codaPurchaseInvoice__c pin = [SELECT Id, Create_Litify_Expenses__c FROM c2g__codaPurchaseInvoice__c LIMIT 1];
        pin.Create_Litify_Expenses__c = true;
        update pin;

        Test.startTest();
        String CRON_EXP = '0 0 * * * ?';
        PayableInvoiceCreateExpenses_Batch sch = new PayableInvoiceCreateExpenses_Batch();
        system.schedule('test job', CRON_EXP, sch);

        PayableInvoiceCreateExpenses_Batch batch = new PayableInvoiceCreateExpenses_Batch(new Set<Id>{pin.Id});
        Id batchID = database.executebatch(batch, 1);
        Test.stopTest();
    }
}