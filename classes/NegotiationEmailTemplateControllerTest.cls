@isTest
public class NegotiationEmailTemplateControllerTest {
    @isTest static void testNegotiationEmailTemplateControllerTest() {
        Account acc = new Account(LastName = 'Testing',litify_pm__Salutation__c = 'Mr.');
        insert acc;
        
        litify_pm__Matter__c matter = new litify_pm__Matter__c(litify_pm__Client__c =acc.Id);
        matter.Total_Coverage__c =1324;
        matter.Total_Bills__c=123124;
        matter.Life_Care_Plan_Cost__c=14124;
        matter.Property_Damage__c=131241;
        insert matter;
        
        litify_pm__Role__c role = new litify_pm__Role__c(litify_pm__Matter__c = matter.Id,litify_pm__Party__c=acc.Id);
        insert role;
        
        Id offerRecordTypeId = Schema.Sobjecttype.litify_pm__Negotiation__c.getRecordTypeInfosByName().get('Offer').getRecordTypeId();
        Id demandRecordTypeId = Schema.Sobjecttype.litify_pm__Negotiation__c.getRecordTypeInfosByName().get('Demand').getRecordTypeId();
        litify_pm__Negotiation__c demand = new litify_pm__Negotiation__c();
        demand.RecordTypeId = demandRecordTypeId;
        demand.litify_pm__Amount__c = 1232.12;
        demand.litify_pm__Negotiating_with__c = role.id;
        demand.Approval_Status__c = 'draft';
        demand.litify_pm__Matter__c = matter.Id;
        demand.litify_pm__Type__c = 'Initial Demand';
        demand.litify_pm__Date__c = System.today();
        insert demand;
        
        litify_pm__Negotiation__c offer = new litify_pm__Negotiation__c();
        offer.RecordTypeId = offerRecordTypeId;
        offer.litify_pm__Amount__c = 1232.12;
        offer.litify_pm__Negotiating_with__c = role.id;
        offer.Most_Recent_Demand__c = demand.id;
        offer.litify_pm__Matter__c = matter.Id;
        offer.Approval_Status__c = 'Approved';
        offer.litify_pm__Type__c = 'Initial Offer';
        offer.litify_pm__Date__c = System.today();
        insert offer;
        
        NegotiationEmailTemplateController negotiationTemplate = new NegotiationEmailTemplateController();
        negotiationTemplate.setOfferRecordId(offer.Id);
        negotiationTemplate = new NegotiationEmailTemplateController();
        negotiationTemplate.setOfferRecordId(offer.Id);
        
        offer.litify_pm__Negotiating_with__c = null;
        update offer;
        
		negotiationTemplate = new NegotiationEmailTemplateController();
        negotiationTemplate.setOfferRecordId(offer.Id);
        negotiationTemplate.getofferRecordId();
    }
}