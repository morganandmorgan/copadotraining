public with sharing class mmcti_CTIScreenPopRedirectSelector
    extends mmlib_SObjectSelector
    implements mmcti_ICTIScreenPopRedirectSelector
{
    public static mmcti_ICTIScreenPopRedirectSelector newInstance()
    {
        return (mmcti_ICTIScreenPopRedirectSelector) mm_Application.Selector.newInstance(CTI_Screen_Pop_Redirect_Setting__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return CTI_Screen_Pop_Redirect_Setting__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            CTI_Screen_Pop_Redirect_Setting__mdt.Profile_Name__c,
            CTI_Screen_Pop_Redirect_Setting__mdt.Redirect_URL__c,
            CTI_Screen_Pop_Redirect_Setting__mdt.Role_Name__c
        };
    }

    public List<CTI_Screen_Pop_Redirect_Setting__mdt> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }
}