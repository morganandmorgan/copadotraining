public with sharing class JSONHelper {
	public JSONHelper() {
		
	}
  public static Object SafeDeserializeUntyped(String str) {
    if (str == null || str.length() < 2) {
      return null;
    }

    str = str.trim();
    if (!str.startsWith('{')) {
      // SF only supports objects so first wrap inside an object
      str = '{"container":' + str + '}';
      Map<String, Object> s = (Map<String, Object>)JSON.deserializeUntyped(str);
      return s.get('container');
    }

    return JSON.deserializeUntyped(str);
  }
}