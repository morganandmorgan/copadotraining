public interface mmmatter_ITimeSlipCodeSelector extends mmlib_ISObjectSelector
{
    List<Time_Slip_Code__mdt> selectAll();
}