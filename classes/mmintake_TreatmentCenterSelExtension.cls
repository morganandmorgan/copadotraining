/**
 *  mmintake_TreatmentCenterSelExtension
 *
 *  This is the Treatment Center Selection Extension class for the TreatmenCenterSelection page
 */
public with sharing class mmintake_TreatmentCenterSelExtension {
    public Intake__c intake { get; private set; }

    public List<TreatmentCenterOption> treatmentCenterOptions { get; private set; } {
        treatmentCenterOptions = new List<TreatmentCenterOption>();
    }

    public List<SelectOption> tfOptions { get; private set; } {
        tfOptions = new List<SelectOption>();
    }

    public id selectedTreatmentFacilityId { get; set; }

    public TreatmentCenterSelectionCriteria tfSelectionCriteria { get; set; }
    {
        tfSelectionCriteria = new TreatmentCenterSelectionCriteria(mmlib_Utils.getSelectOptionList(Account.Treatment_Center_Type__c));
    }

    public TreatmentCenterOption selectedTreatmentFacility { get; private set; }

    public boolean isTreatmentFacilitySelected {
        get {
            return selectedTreatmentFacilityId != null;
        }
    }

    public boolean isSaveError { get; private set; } {
        isSaveError = false;
    }

    public boolean isInEditMode { get; private set; } {
        isInEditMode = false;
    }

    public boolean isAssignTreatmentFacilityResponseGiven { get; private set; } {
        isAssignTreatmentFacilityResponseGiven = false;
    }

    public boolean isClientsAddressValid { get; private set; } {
        isClientsAddressValid = true;
    }

    /**
     * StandardController contstructor
     *
     * @param stdContrller ApexPages.StandardController
     * @see ApexPages.StandardController
     */
    public mmintake_TreatmentCenterSelExtension(ApexPages.StandardController stdController) {
        this.intake = (Intake__c) stdController.getRecord();

        refreshIntakeRecord();
    }

    private void refreshIntakeRecord() {
        if (this.intake != null && this.intake.id != null) {
            //refresh the record
            this.intake = mmintake_IntakesSelector.newInstance().selectById(new set<Id>{
                    this.intake.id
            })[0];
        }
    }

    public void findTreatmentCentersCloseBy(Location originatingLocation) {
        system.debug('treatmentCenterOptions size == ' + treatmentCenterOptions.size());
        System.debug('originatingLocation:\n' + originatingLocation);

        if (originatingLocation != null) {
            TreatmentCenterOption tco = null;

            string labelTextTemplate = '{0} ({1} miles away)';
            List<String> textFillers = new List<string>();

            treatmentCenterOptions.clear();
            tfoptions.clear();

            for (Account treatmentCenterRecord : mmcommon_BusinessAccountsService.findTreatmentFacilitiesCloseToLocation(originatingLocation, tfSelectionCriteria.selectedTreatmentFacilityType)) {
                textFillers.clear();

                tco = new TreatmentCenterOption(treatmentCenterRecord, originatingLocation);
                treatmentCenterOptions.add(tco);

                textFillers.add(treatmentCenterRecord.name);

                textFillers.add(String.valueOf(tco.distanceFromOriginatingLocation.round()));

                tfOptions.add(new SelectOption(treatmentCenterRecord.id, String.format(labelTextTemplate, textFillers)));
            }

            this.isAssignTreatmentFacilityResponseGiven = true;
            this.isClientsAddressValid = true;
        } else {
            this.isClientsAddressValid = false;
        }
    }

    public PageReference switchToEditMode() {
        System.debug('facility type:\n' + tfSelectionCriteria.selectedTreatmentFacilityType);
        System.debug('use provided location:\n' + tfSelectionCriteria.useProvidedLocation);
        System.debug('street:\n' + tfSelectionCriteria.providedStreet);
        System.debug('city:\n' + tfSelectionCriteria.providedCity);
        System.debug('state:\n' + tfSelectionCriteria.providedState);

        this.isInEditMode = true;
        this.isAssignTreatmentFacilityResponseGiven = false;

        Location loc = getLocation();
        if (loc != null) {
            findTreatmentCentersCloseBy(loc);
        } else {
            this.isInEditMode = false;
        }

        system.debug('switchToEditMode area - isAssignTreatmentFacilityResponseGiven:' + isAssignTreatmentFacilityResponseGiven);
        system.debug('switchToEditMode area - isInEditMode:' + isInEditMode);

        return null;
    }

    private Location getLocation() {

        if (tfSelectionCriteria.useProvidedLocation) {
            return getProvidedLocationData();
        } else {
            return getClientLocationData();
        }

    }

    private Location getClientLocationData() {
        system.debug('getting location for client == ' + this.intake.Client__c);

        Location resultingLocation = null;

        List<Account> personAccountList = mmcommon_AccountsSelector.newInstance().selectById(new Set<id>{
                this.intake.Client__c
        });

        if (!personAccountList.isEmpty()) {

            // found the client id, now find the closest treatment facilities
            Account client = personAccountList[0];

            // check the client.  Does it have current geocoding information?
            if (client.Last_Geocoding_Update__pc == null && this.isInEditMode) {
                // there is no current geocoding for this client.  quickly update this information
                // some service call
                mmcommon_PersonAccountsService.geocodeAddresses(new set<id>{
                        client.id
                });

                // requery the client record.
                client = mmcommon_AccountsSelector.newInstance().selectById(new Set<id>{
                        client.id
                })[0];
            }

            resultingLocation = Location.newInstance(client.BillingLatitude, client.BillingLongitude);
        }

        System.debug('client location:\n' + resultingLocation);

        return resultingLocation;
    }

    private Location getProvidedLocationData() {
        System.debug('getting provided location for:\n' + tfSelectionCriteria);

        mmgmap_Address addr = new mmgmap_Address();
        addr.setCountry('USA');

        if (String.isNotEmpty(tfSelectionCriteria.providedStreet)) {
            addr.setStreet(tfSelectionCriteria.providedStreet);
        }

        if (String.isNotEmpty(tfSelectionCriteria.providedCity)) {
            addr.setCity(tfSelectionCriteria.providedCity);
        }

        if (String.isNotEmpty(tfSelectionCriteria.providedState)) {
            addr.setState(tfSelectionCriteria.providedState);
        }

        System.debug('provided address:\n' + addr);

        Location loc = mmgmap_GoogleGeoCodingService.getLatitudeLongitudeForAddress(addr);

        System.debug('provided location:\n' + loc);

        return loc;
    }

    public PageReference cancelEditMode() {
        this.isInEditMode = false;
        this.isAssignTreatmentFacilityResponseGiven = false;
        this.selectedTreatmentFacilityId = null;
        this.selectedTreatmentFacility = null;

        return null;
    }

    public PageReference assignTreatmentFacility() {
        this.isAssignTreatmentFacilityResponseGiven = false;

        if (selectedTreatmentFacilityId != null) {
            this.intake.Treating_Facility__c = selectedTreatmentFacilityId;

            persistChangeOfTreatmentFacility();
        }

        return null;
    }

    public PageReference removeTreatmentCenter() {
        system.debug('removeTreatmentCenter called');

        selectedTreatmentFacility = null;
        selectedTreatmentFacilityId = null;

        this.intake.Treating_Facility__c = null;

        persistChangeOfTreatmentFacility();

        return null;
    }

    private void persistChangeOfTreatmentFacility() {
        isSaveError = false;

        try {
            new IntakeService().saveRecords(new List<Intake__c>{
                    this.intake
            }, false);

            isInEditMode = false;

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Changes made'));

            refreshIntakeRecord();
        } catch (mmintake_IntakesServiceExceptions.ValidationException ve) {
            isSaveError = true;

            if (ve.getCause() != null
                    && ve.getCause() instanceOf mmlib_BaseExceptions.ValidationException) {
                mmlib_BaseExceptions.ValidationException beve = (mmlib_BaseExceptions.ValidationException) ve.getCause();

                map<Schema.sObjectField, set<string>> messagesMap = null;

                for (id recordId : beve.getRecordIds()) {
                    for (Schema.sObjectField field : beve.getFieldMessagesByRecordId(recordId).keyset()) {
                        ApexPages.addMessage(
                                new ApexPages.Message(
                                        ApexPages.Severity.ERROR,
                                        'Validation Issue: ' + String.join(new List<String>(beve.getFieldMessagesByRecordId(recordId).get(field)), ', ')));
                    }

                    for (String msg : beve.getMessagesByRecordId(recordId)) {
                        ApexPages.addMessage(
                                new ApexPages.Message(
                                        ApexPages.Severity.ERROR,
                                        'Validation Issue: ' + msg));
                    }
                }
            }

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please correct these issues before attempting to assign a ' + Intake__c.Treating_Facility__c.getDescribe().getLabel().toLowerCase() + '.'));
        } catch (mmintake_IntakesServiceExceptions.ServiceException se) {
            isSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an issue that prevented assignment of the treatment facility.  Please try again and check with a system administrator if the issue constinues.'));
        } catch (Exception e) {
            isSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an issue that prevented assignment of the treatment facility.  Please try again and check with a system administrator if the issue constinues.'));
        }
    }

    public PageReference performSelectionOfTreatmentCenter() {
        if (selectedTreatmentFacilityId != null) {
            for (TreatmentCenterOption tco : treatmentCenterOptions) {
                if (tco.record.id == selectedTreatmentFacilityId) {
                    selectedTreatmentFacility = tco;
                    break;
                }
            }
        }
        return null;
    }

    public class TreatmentCenterOption {
        public Account record { get; set; }
        public double distanceFromOriginatingLocation { get; private set; }
        public boolean isSelected { get; set; } {
            isSelected = false;
        }

        public TreatmentCenterOption(Account treatmentCenterRecord, Location originatingLocation) {
            this.record = treatmentCenterRecord;
            this.distanceFromOriginatingLocation = Location.newInstance(treatmentCenterRecord.BillingLatitude, treatmentCenterRecord.BillingLongitude).getDistance(originatingLocation, 'mi');
        }
    }

    public class TreatmentCenterSelectionCriteria {
        public String facilityOptionListLabel { get; set; } {
            facilityOptionListLabel = 'Treatment Facility Type';
        }
        public String useProvidedLocationCheckboxLabel { get; set; } {
            useProvidedLocationCheckboxLabel = 'Use this location for search';
        }
        public String streetAddressLabel { get; set; } {
            streetAddressLabel = 'Street Address';
        }
        public String cityLabel { get; set; } {
            cityLabel = 'City';
        }
        public String stateLabel { get; set; } {
            stateLabel = 'State';
        }

        public List<SelectOption> treatmentFacilityTypeOptionList { get; private set; }
        public String selectedTreatmentFacilityType { get; set; }
        public String providedStreet { get; set; }
        public String providedCity { get; set; }
        public String providedState { get; set; }
        public Boolean useProvidedLocation { get; set; } {
            useProvidedLocation = false;
        }

        public TreatmentCenterSelectionCriteria(List<SelectOption> treatmentFacilityTypeOptionList) {
            this.treatmentFacilityTypeOptionList = treatmentFacilityTypeOptionList;
        }
    }

    @TestVisible
    private static void coverage() {
        Integer x = 0;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
    }
}