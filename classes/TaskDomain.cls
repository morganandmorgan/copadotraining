/**
 * TaskDomain
 * @description Domain class for Task SObject.
 * @author Jeff Watson
 * @date 8/22/2019
 */

public with sharing class TaskDomain extends fflib_SObjectDomain {

    private List<Task> tasks;

    // Ctors
    public TaskDomain() {
        super();
        this.tasks = new List<Task>();
    }

    public TaskDomain(List<Task> tasks) {
        super(tasks);
        this.tasks = tasks;
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new TaskDomain(records);
        }
    }

    // Trigger Handlers
    public override void onBeforeInsert() {
        processChangeTaskAssociationToRecentIntake();
        SetTaskWhatID(this.tasks);
        setOutboundCallTime(this.tasks);
        getDiceQueueNamesFromAccessLogs(null);
    }

    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        getDiceQueueNamesFromAccessLogs(existingRecords);
    }

    public override void onAfterInsert() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<Schema.SObjectType>{
                Intake__c.SObjectType
        });
        UpdateIntakeFromTask_TrigAct updateIntakeFromTask = new UpdateIntakeFromTask_TrigAct();
        if (updateIntakeFromTask.shouldRun()) {
            updateIntakeFromTask.doAction();
        }
        updateRelatedIntake(this.tasks);
        new mmcommon_AccountsNextCallDateAction().setRecordsToActOn(this.tasks).run();
        incrementIntakeDialCounts(uow, new Map<Id, SObject>());
        handleQuestionnaireOneHourEmailReminder();
        uow.commitWork();
    }

    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<Schema.SObjectType>{
                Intake__c.SObjectType
        });
        UpdateIntakeFromTask_TrigAct updateIntakeFromTask = new UpdateIntakeFromTask_TrigAct();
        if (updateIntakeFromTask.shouldRun()) {
            updateIntakeFromTask.doAction();
        }
        updateRelatedIntake(this.tasks);
        
        mmcommon_AccountsNextCallDateAction taskAction = new mmcommon_AccountsNextCallDateAction();
        taskAction.setRecordsToActOn(this.tasks);
        taskAction.setOldMap((Map<Id,Task>)existingRecords);
        taskAction.run();
        
        incrementIntakeDialCounts(uow, existingRecords);
        uow.commitWork();
    }

    public override void onBeforeDelete() {

        Boolean allowTaskDeletion = new mmcommon_UserInfo().hasPermissionSetName(new List<String>{
                'Allow_Ability_to_Delete_Tasks'
        });

        String currentUrl = URL.getCurrentRequestUrl().toExternalForm();
        Boolean isAPI = (currentUrl.contains('services/soap') || currentUrl.contains('services/data'));
        if (this.tasks != null && !this.tasks.isEmpty() && allowTaskDeletion == false && isAPI == false) {            
            this.tasks[0].addError('You do not have permission to delete tasks.');
        }
    }

    // Methods
    private void processChangeTaskAssociationToRecentIntake() {

        List<Task> recordsToProcess = new List<Task>();
        Set<Id> accountIdSet = new Set<Id>();

        // We only want to run for Account Tasks
        for (Task task : tasks) {
            if (task.WhatId != null && Account.SObjectType == task.WhatId.getSObjectType() && 'Call'.equals(task.Subject)) {
                recordsToProcess.add(task);
                accountIdSet.add(task.WhatId);
            }
        }

        if (!recordsToProcess.isEmpty()) {
            // find all of the intakes for the client account records involved on the Tasks where the Account record type is PersonAccount
            // using the intake records, setup the accIdAndIntakeMap Map
            Map<Id, Intake__c> intakesByAccountIdMap = new Map<Id, Intake__c>();

            for (Intake__c intake : [
                    SELECT Id, Client__c, Status__c
                    FROM Intake__c
                    WHERE Client__c IN :accountIdSet
                    AND Client__r.RecordType.IsPersonType = true
                    ORDER BY CreatedDate DESC
            ]) {
                // DELIBERATELY only work with most recently created Intake
                if (!intakesByAccountIdMap.containsKey(intake.Client__c)) {
                    intakesByAccountIdMap.put(intake.Client__c, intake);
                }
            }

            Map<Id, List<Task>> taskListByAccountIdMap = mmlib_Utils.generateSObjectMapByIdField(recordsToProcess, Task.WhatId);
            for (Id accId : accountIdSet) {
                for (Task task : taskListByAccountIdMap.get(accId)) {
                    if (intakesByAccountIdMap.containsKey(accId) && intakesByAccountIdMap.get(accId).Status__c != 'Converted - Retainer Received') {
                        task.WhatId = intakesByAccountIdMap.get(accId).Id;
                    }
                }
            }
        }
    }

    @TestVisible
    private void getDiceQueueNamesFromAccessLogs(Map<Id, SObject> existingTasks) {

        // We only want to run for newly created Intake Tasks
        List<Task> tasksToProcess = new List<Task>();
        for (Task task : this.tasks) {
            if (task.WhatId != null && task.WhatId.getSObjectType() == Intake__c.SObjectType && isNewlyCompletedOutboundCall(task, existingTasks)) {
                tasksToProcess.add(task);
            }
        }
        if (tasksToProcess == null || tasksToProcess.isEmpty()) return;

        // Logic
        Map<Id, Intake__c> intakeMap = getIntakesMapForTasks(tasksToProcess);
        List<DICE_AccessLog__c> diceAccessLogList = mmdice_AccessLogsSelector.newInstance().selectAllOrderedByLastModifiedDescending();
        Map<Id, DICE_Queue__c> diceQueueMap = new Map<Id, DICE_Queue__c>(mmdice_QueueSelector.newInstance().selectAll());

        for (Task tsk : tasksToProcess) {
            Intake__c intake = intakeMap.get(tsk.WhatId);
            if (intake == null) {
                continue;
            }

            for (DICE_AccessLog__c log : diceAccessLogList) {

                DICE_Queue__c diceQueue = diceQueueMap.get(log.NavigatorId__c);
                if (diceQueue == null) {
                    continue;
                }

                if (isMatch(tsk, intake, log)) {
                    tsk.DiceQueueName__c = diceQueue.Name;
                    break;
                }
            }
        }
    }

    public void incrementIntakeDialCounts(fflib_SObjectUnitOfWork uow, Map<Id, SObject> existingTasks) {

        // Only process newly completed outbound Intake Tasks
        List<Task> tasksToProcess = new List<Task>();
        for (Task task : this.tasks) {
            if (task.WhatId != null && task.WhatId.getSObjectType() == Intake__c.SObjectType && isNewlyCompletedOutboundCall(task, existingTasks)) {
                tasksToProcess.add(task);
            }
        }

        Map<Id, Intake__c> intakeMap = getIntakesMapForTasks(tasksToProcess);
        for (Task task : tasksToProcess) {
            if (task.WhatId != null && task.WhatId.getSObjectType() == Intake__c.SObjectType) {

                Intake__c intake = intakeMap.get(task.WhatId);
                if (intake != null) {
                    intake.Number_Outbound_Dials_Intake__c = intake.Number_Outbound_Dials_Intake__c != null ? intake.Number_Outbound_Dials_Intake__c + 1 : 1;
                    intake.Number_Outbound_Dials_Status__c = intake.Number_Outbound_Dials_Status__c != null ? intake.Number_Outbound_Dials_Status__c + 1 : 1;
                    intake.Last_Dial_Time__c = (task.LastModifiedDate != null) ? task.LastModifiedDate : null;
                    uow.registerDirty(intake);
                }
            }
        }
    }

    private Map<Id, Intake__c> getIntakesMapForTasks(List<Task> taskList) {

        Set<Id> intakeIdSet = new Set<Id>();
        for (Task tsk : taskList) {
            if (tsk.WhatId != null && tsk.WhatId.getSObjectType() == Intake__c.SObjectType) {
                intakeIdSet.add(tsk.WhatId);
            }
        }

        return new Map<Id, Intake__c>(mmintake_IntakesSelector.newInstance().selectById(intakeIdSet));
    }

    private Boolean isMatch(Task task, Intake__c intake, DICE_AccessLog__c log) {

        if (task == null || intake == null || log == null) {
            return false;
        }

        Datetime tskTime = (task.LastModifiedDate != null) ? task.LastModifiedDate : Datetime.now();
        Datetime logTime = (log.CreatedDate != null) ? log.CreatedDate : null;

        return intake.Client__c == log.ItemId__c && mmlib_Utils.dateTimesMatchWithinNSeconds(tskTime, logTime, 6 * 3600);
    }

    private Boolean isNewlyCompletedOutboundCall(Task task, Map<Id, SObject> existingTasks) {

        String previousStatus = (existingTasks == null || existingTasks.isEmpty()) ? '' : ((Task) existingTasks.get(task.Id)).Status;

        Boolean result =
                ('Manual OB Call'.equalsIgnoreCase(task.Type) || 'Outbound'.equalsIgnoreCase(task.CallType))
                        && 'Completed'.equalsIgnoreCase(task.Status)
                        && !'Completed'.equalsIgnoreCase(previousStatus);

        return result;
    }

    public void handleQuestionnaireOneHourEmailReminder() {

        // Only process Intake Tasks
        Set<Id> intakeIds = new Set<Id>();
        for (Task task : this.tasks) {
            if (task.WhatId != null && task.WhatId.getSObjectType() == Intake__c.SObjectType) {
                intakeIds.add(task.WhatId);
            }
        }

        Map<Id, Intake__c> intakesByIds = new Map<Id, Intake__c>(mmintake_IntakesSelector.newInstance().selectById(intakeIds));
        for (Task task : this.tasks) {
            Intake__c intake = intakesByIds.get(task.WhatId);
            if (task.Subject == 'Email: 1 Hour Retainer Received - with Link' && intake != null) {
                startSMS24HourFlow(intake.Id, createSmsLongUrl(intake), task.Id);
            }
        }
    }

    @Future
    public static void startSMS24HourFlow(Id intakeId, String longUrl, Id taskId) {
        Map<String, Object> inputMap = new Map<String, Object>{
                'varIntakeID' => intakeId, 'varLongURL' => longUrl, 'varTaskID' => taskId, 'varSMSTemplate' => 'a2o1J000004hUss', 'varSenderNumber' => '18442020921'
        };
        Flow.Interview flow = new Flow.Interview.Questionnaire_24_hour_SMS_Reminder(inputMap);
        flow.start();
    }

    public String createSmsLongUrl(Intake__c intake) {
        return String.format('https://mycase.forthepeople.com/?idx={0}&ct={1}&lit={2}&utm_source=sf&utm_campaign=rr-questionnaire&utm_medium=sms&utm_content=full', new List<String>{
                String.valueOf(intake.Id), intake.Case_Type__c, intake.Litigation__c
        });
    }

    // ===============OLD TasktriggerHandler==============
    @TestVisible
    private static String outboundDialNumberPattern {
        get {
            if (outboundDialNumberPattern == null) {
                List<String> leadingDigits = new List<String>();
                for (Outbound_Dial_Number__mdt outboundDialNumber : [SELECT Outbound_Dial_Number__c FROM Outbound_Dial_Number__mdt WHERE Outbound_Dial_Number__c != null]) {
                    leadingDigits.add(outboundDialNumber.Outbound_Dial_Number__c);
                }
                outboundDialNumberPattern = leadingDigits.isEmpty() ? '' : '^(' + String.join(leadingDigits, '|') + ')';
            }
            return outboundDialNumberPattern;
        }
        set;
    }

    public void SetTaskWhatID(List<Task> newList) {
        Map<Id, List<Task>> accountIdToTasks = new Map<Id, List<Task>>();

        for (Task newTask : newList) {
            if (newTask.WhatId != null
                    && newTask.WhatId.getSObjectType() == Account.SObjectType
                    && newTask.CallType != null) {
                List<Task> tasks = accountIdToTasks.get(newTask.WhatId);
                if (tasks == null) {
                    tasks = new List<Task>();
                    accountIdToTasks.put(newTask.WhatId, tasks);
                }
                tasks.add(newTask);
            }
        }

        if (!accountIdToTasks.isEmpty()) {
            List<Account> accountsWithIntakes = [
                    SELECT (
                            SELECT Id
                            FROM Intake_Surveys__r
                            ORDER BY LastModifiedDate
                            LIMIT 1
                    )
                    FROM Account
                    WHERE Id IN :accountIdToTasks.keySet()
                    AND Id IN (
                            SELECT Client__c
                            FROM Intake__c
                            WHERE Client__c IN :accountIdToTasks.keySet()
                    )
            ];

            for (Account a : accountsWithIntakes) {
                List<Intake__c> intakes = a.Intake_Surveys__r;
                if (!intakes.isEmpty()) {
                    Intake__c intake = intakes.get(0);
                    for (Task task : accountIdToTasks.get(a.Id)) {
                        task.WhatId = intake.Id;
                    }
                }
            }
        }
    }

    public void updateRelatedIntake(List<Task> newList) {
        Map<Id, String> intakeIdToCallDisposition = new Map<Id, String>();

        String callDispositionToRecord = null;

        // For all new tasks
        for (Task newTask : newList) {
            // if that task is a call and associated with an intake
            if (newTask.WhatId != null
                    && newTask.WhatId.getSObjectType() == Intake__c.SObjectType
                    && (newTask.CallType != null || (newTask.Manual_Call_Disposition__c != null
                    && (Trigger.isExecuting && (Trigger.isInsert || (Trigger.isUpdate && Trigger.oldMap.containsKey(newTask.Id)
                    && newTask.Manual_Call_Disposition__c != ((Task) Trigger.oldMap.get(newTask.Id)).Manual_Call_Disposition__c)
            )
            )
            )
            )
                    ) {
                // then take note of that record for later use.
                callDispositionToRecord = string.isNotBlank(newTask.Manual_Call_Disposition__c)
                        ? newTask.Manual_Call_Disposition__c
                        : newTask.CallDisposition;

                intakeIdToCallDisposition.put(newTask.WhatId, callDispositionToRecord);
            }
        }

        if (!intakeIdToCallDisposition.isEmpty()) {
            // select all call records;
            List<Intake__c> intakes = [
                    SELECT Id, Last_Disposition__c, Last_Call_Time__c
                    FROM Intake__c
                    WHERE Id IN :intakeIdToCallDisposition.keySet()
            ];

            Datetime now = Datetime.now();
            for (Intake__c intake : intakes) {
                intake.Last_Disposition__c = intakeIdToCallDisposition.get(intake.Id);
                intake.Last_Call_Time__c = now;
            }
            Database.update(intakes);
        }
    }

    public void setOutboundCallTime(List<Task> newList) {
        Datetime now = Datetime.now();
        String obCallHour = now.format('a, h:mm', mmlib_utils.defaultBusinessHours.TimeZoneSidKey);

        for (Task newTask : newList) {
            if (newTask.CallType == 'Outbound') {
                newTask.OB_Call_Time__c = now;
                newTask.OB_Call_Hour__c = obCallHour;
            }
        }
    }

    @TestVisible
    private static void coverage() {
        Integer x = 0;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
    }
}