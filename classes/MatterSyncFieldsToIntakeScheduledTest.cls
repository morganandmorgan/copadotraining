@IsTest
public with sharing class MatterSyncFieldsToIntakeScheduledTest {

    @IsTest
    private static void mainTest() {

        Integer nextRunInMinutes = 5;
        MatterSyncFieldsToIntakeScheduled syncSched = new MatterSyncFieldsToIntakeScheduled(nextRunInMinutes, DateTime.now().addMinutes(-1*nextRunInMinutes));

        syncSched.execute(null);

        litify_pm__Matter__History history = new litify_pm__Matter__History();
        syncSched.execute(null, new List<litify_pm__Matter__History>{history});

    } //mainTest

} //class