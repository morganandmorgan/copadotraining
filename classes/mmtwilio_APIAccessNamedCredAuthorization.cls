/**
 *  mmtwilio_APIAccessNamedCredAuthorization
 */
public class mmtwilio_APIAccessNamedCredAuthorization
    extends mmlib_NamedCredentialCalloutAuth
    implements mmlib_IDomain
{
    private String domain = null;

    public string getDomain()
    {
        return this.domain;
    }

    public void setDomain( string domain )
    {
        this.domain = domain;
    }
}