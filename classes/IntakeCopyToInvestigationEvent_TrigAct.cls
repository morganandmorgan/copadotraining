public class IntakeCopyToInvestigationEvent_TrigAct extends TriggerAction {

    private final List<Intake__c> intakesWithChanges = new List<Intake__c>();

	public IntakeCopyToInvestigationEvent_TrigAct() {
	   super();
	}

    public override Boolean shouldRun() {
        intakesWithChanges.clear();

        if (isAfter() && isUpdate()) {
            Map<Id, Intake__c> intakeNewMap = (Map<Id, Intake__c>) triggerMap;
            Map<Id, Intake__c> intakeOldMap = (Map<Id, Intake__c>) triggerOldMap;

            for (Intake__c intake : intakeNewMap.values()) {
                Intake__c oldIntake = intakeOldMap.get(intake.Id);

                if (intake.Approving_Attorney_1__c != oldIntake.Approving_Attorney_1__c
                        || intake.Approving_Attorney_2__c != oldIntake.Approving_Attorney_2__c
                        || intake.Approving_Attorney_3__c != oldIntake.Approving_Attorney_3__c
                        || intake.Approving_Attorney_4__c != oldIntake.Approving_Attorney_4__c
                        || intake.Approving_Attorney_5__c != oldIntake.Approving_Attorney_5__c
                        || intake.Approving_Attorney_6__c != oldIntake.Approving_Attorney_6__c
                        || intake.Assigned_Attorney__c != oldIntake.Assigned_Attorney__c
                        || intake.New_Case_Email__c != oldIntake.New_Case_Email__c) {

                    intakesWithChanges.add(intake);
                }
            }

        }

        return !intakesWithChanges.isEmpty();
    }

    public override void doAction() {
        if (!intakesWithChanges.isEmpty()) {
            Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>(intakesWithChanges);

            List<IntakeInvestigationEvent__c> intakeInvestigationEventsToUpdate = new List<IntakeInvestigationEvent__c>();

            for (IntakeInvestigationEvent__c intakeInvestigationEvent : [
                SELECT
                    Approving_Attorney_1__c,
                    Approving_Attorney_2__c,
                    Approving_Attorney_3__c,
                    Approving_Attorney_4__c,
                    Approving_Attorney_5__c,
                    Approving_Attorney_6__c,
                    Assigned_Attorney__c,
                    Intake__c,
                    New_Case_Email__c
                FROM
                    IntakeInvestigationEvent__c
                WHERE
                    Intake__c IN :intakeMap.keySet()
                    AND Investigation_Status__c = 'Scheduled'
            ]) {
                Intake__c intake = intakeMap.get(intakeInvestigationEvent.Intake__c);

                if (intake.Approving_Attorney_1__c != intakeInvestigationEvent.Approving_Attorney_1__c
                        || intake.Approving_Attorney_2__c != intakeInvestigationEvent.Approving_Attorney_2__c
                        || intake.Approving_Attorney_3__c != intakeInvestigationEvent.Approving_Attorney_3__c
                        || intake.Approving_Attorney_4__c != intakeInvestigationEvent.Approving_Attorney_4__c
                        || intake.Approving_Attorney_5__c != intakeInvestigationEvent.Approving_Attorney_5__c
                        || intake.Approving_Attorney_6__c != intakeInvestigationEvent.Approving_Attorney_6__c
                        || intake.Assigned_Attorney__c != intakeInvestigationEvent.Assigned_Attorney__c
                        || intake.New_Case_Email__c != intakeInvestigationEvent.New_Case_Email__c) {

                    intakeInvestigationEvent.Approving_Attorney_1__c = intake.Approving_Attorney_1__c;
                    intakeInvestigationEvent.Approving_Attorney_2__c = intake.Approving_Attorney_2__c;
                    intakeInvestigationEvent.Approving_Attorney_3__c = intake.Approving_Attorney_3__c;
                    intakeInvestigationEvent.Approving_Attorney_4__c = intake.Approving_Attorney_4__c;
                    intakeInvestigationEvent.Approving_Attorney_5__c = intake.Approving_Attorney_5__c;
                    intakeInvestigationEvent.Approving_Attorney_6__c = intake.Approving_Attorney_6__c;
                    intakeInvestigationEvent.Assigned_Attorney__c = intake.Assigned_Attorney__c;
                    intakeInvestigationEvent.New_Case_Email__c = intake.New_Case_Email__c;

                    intakeInvestigationEventsToUpdate.add(intakeInvestigationEvent);
                }
            }

            Database.update(intakeInvestigationEventsToUpdate);
        }
    }
}