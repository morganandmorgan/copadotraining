/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class LineupMembersMigrationBatchable implements Database.Batchable<SObject> {
    global LineupMembersMigrationBatchable() {

    }
    global void execute(Database.BatchableContext BC, List<litify_pm__Matter_Team_Member__c> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
