public interface mmmatter_IMatterTeamRolesSelector extends mmlib_ISObjectSelector
{
    List<litify_pm__Matter_Team_Role__c> selectAll();
	List<litify_pm__Matter_Team_Role__c> selectByName(Set<String> nameSet);
    List<litify_pm__Matter_Team_Role__c> selectById(Set<Id> ids);
}