public class mmcommon_EventsSelector
    extends mmlib_SObjectSelector
    implements mmcommon_IEventsSelector
{
    public static mmcommon_IEventsSelector newInstance()
    {
        return (mmcommon_IEventsSelector) mm_Application.Selector.newInstance(Event.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Event.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {

        };
    }

    public List<Event> selectById(Set<Id> idSet)
    {
        return (List<Event>) selectSObjectsById(idSet);
    }

    public List<Event> selectByUserIdAndDate(Set<Id> idSet, Set<Date> dateSet)
    {
        String queryCriteria = 'OwnerId IN :idSet';

        Time midnight = Time.newInstance(0, 0, 0, 0);

        List<String> dateCriteriaList = new List<String>();

        for (Date d : dateSet)
        {
            Datetime dateStart = Datetime.newInstance(d, midnight);
            Datetime dateEnd = dateStart.addDays(1);

            String dateString = String.valueOf(d);
            String dateStartString = mmlib_Utils.formatSoqlDatetimeUTC(dateStart);
            String dateEndString = mmlib_Utils.formatSoqlDatetimeUTC(dateEnd);

            String dateCriteria = '( IsAllDayEvent = true'
                                   + ' AND DAY_ONLY(StartDateTime) <= ' + dateString
                                   + ' AND DAY_ONLY(EndDateTime) >= ' + dateString
                                   + ')'
                            + ' OR ( IsAllDayEvent = false'
                                   + ' AND ( (StartDateTime >= ' + dateStartString
                                          + ' AND StartDateTime < ' + dateEndString
                                           + ')'
                                        + ' OR ( EndDateTime > ' + dateStartString
                                           + ' AND EndDateTime <= ' + dateEndString
                                            + ')'
                                        + ' OR ( StartDateTime <= ' + dateStartString
                                           + ' AND EndDateTime >= ' + dateEndString
                                            + ')'
                                          +')'
                                  +')';
            dateCriteriaList.add(dateCriteria);
        }

        if ( ! dateCriteriaList.isEmpty() )
        {
            queryCriteria += ' AND (' + String.join(dateCriteriaList, ' OR ') + ')';
            dateCriteriaList = null;
        }

        String queryString = newQueryFactory().setCondition( queryCriteria ).toSOQL();
        System.debug(queryString);
        return Database.query(queryString);
    }

    public List<Event> selectByUserOrderByDate(Set<Id> idSet)
    {
        fflib_QueryFactory.Ordering queryOrderBy = new fflib_QueryFactory.Ordering(Event.ActivityDate, fflib_QueryFactory.SortOrder.ASCENDING);

        return Database.query( newQueryFactory().setCondition('OwnerId IN :idSet').addOrdering( queryOrderBy ).toSOQL() );
    }

    public List<Event> selectAll()
    {
        return Database.query( newQueryFactory().toSOQL() );
    }
    public List<Event> selectMyTodayStartEvents() {
        Id userId = UserInfo.getUserId();
        return Database.query(newQueryFactory().setCondition(Event.OwnerId + ' = :userId and ' + Event.StartDateTime + ' = TODAY').toSOQL());
    }
    public List<Event> selectFutureOpenTasks(Integer numDays) {
        if (numDays == null) numDays = 7;
        Id userId = UserInfo.getUserId();
        return Database.query(newQueryFactory().setCondition(Event.OwnerId + ' = :userId and ' + Event.StartDateTime + ' > TODAY and ' + Event.StartDateTime + ' = NEXT_N_DAYS:' + numDays).toSOQL());
    }

    public list<Event> selectByTypeAndWhat( Set<String> typeSet, Set<Id> relatedWhatIdSet)
    {
        return Database.query( newQueryFactory().setCondition( Event.Type + ' in :typeSet and ' + Event.WhatId + ' in :relatedWhatIdSet' ).toSOQL() );
    }
}