/**
 * Constants
 * @description Contants for use across the code base.
 * @author Jeff Watson
 * @date 1/18/2019
 */

public with sharing class Constants {

    //API Version + base URL
    public static final String API_VERSION = 'v1';
    public static final String BASE_URL = '/services/apexrest/' + API_VERSION;

    // Queries and Filters
    public static final String   QUERY_LIMIT = 'limit';
    public static final Integer  QUERY_LIMIT_DEFAULT = 100;
    public static final String   QUERY_BEFORE = 'before';
    public static final Datetime QUERY_BEFORE_DEFAULT = Datetime.now().addDays(1);
    public static final String   QUERY_SEARCH = 'search';

    // HTTP Status Codes
    public static final Integer HTTP_STATUS_CODE_OK = 200;

    public static final Integer HTTP_STATUS_CODE_NO_CONTENT = 204;
    public static final String  HTTP_STATUS_CODE_NO_CONTENT_MESSAGE = 'No Content';

    public static final Integer HTTP_STATUS_CODE_BAD_REQUEST = 400;
    public static final String  HTTP_STATUS_CODE_BAD_REQUEST_MESSAGE = 'Bad Request';

    public static final Integer HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
    public static final String  HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR_MESSAGE = 'Internal Server Error';

    // Error Messages

    // URI PATH ENTITIES
    public static final String PATH_ENTITY_MATTERS = 'matters';
    public static final String PATH_ENTITY_QUEUES = 'queues';

    // MIME Types
    public static final String APPLICATION_JSON = 'application/json';
    public static final String UTF8 = 'UTF-8';
}