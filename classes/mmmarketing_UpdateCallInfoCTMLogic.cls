public class mmmarketing_UpdateCallInfoCTMLogic
    implements mmmarketing_IUpdateCallInfoLogic
{
    private map<String, mmlib_NamedCredentialCalloutAuth> apiAuthorizationsByIdentifierMap = new map<String, mmlib_NamedCredentialCalloutAuth>();

    private map<string, string> apiAuthToCTMAccountIdMap = new map<string, string>();

    private boolean isDebugOn = false;

    public mmmarketing_UpdateCallInfoCTMLogic()
    {
        CallTrackingMetricsIntegrationRelated__c customSetting = CallTrackingMetricsIntegrationRelated__c.getInstance();

        if (customSetting != null
            && customSetting.IsIntegrationDebugEnabled__c == true)
        {
            this.isDebugOn = true;
        }

        mmctm_APIAccessNamedCredAuthorization apiAuthorization = null;

        // This loads the CTM releated authorizations needed into the apiAuthorizationsByIdentifierMap
        for ( CallTrackingMetricsAccountMapping__mdt record : mmctm_CTMAccountMappingSelector.newInstance().selectAll() )
        {
            apiAuthorization = new mmctm_APIAccessNamedCredAuthorization();
            apiAuthorization.setIdentifier( record.CTM_Auth_Named_Credential__c );
            apiAuthorization.setDomain( record.Domain_Value__c );

            this.apiAuthorizationsByIdentifierMap.put( apiAuthorization.getIdentifier(), apiAuthorization );

            this.apiAuthToCTMAccountIdMap.put( apiAuthorization.getIdentifier(), record.CTM_Account_Id__c );
        }

    }

    public mmlib_BaseCallout prepareCalloutToService( Marketing_Tracking_Info__c record, string identifier )
    {
        return (mmctm_ListCallsCallout)((new mmctm_ListCallsCallout())
                                         .setQueryStartDate( record.Tracking_Event_Timestamp__c.date() )
                                         .setQueryEndDate( record.Tracking_Event_Timestamp__c.date() )
                                         .setCallerNumber( record.Calling_Phone_Number__c )
                                         .setCTMAccountId( this.apiAuthToCTMAccountIdMap.get( identifier ) )
                                         .setAuthorization( apiAuthorizationsByIdentifierMap.get( identifier ) ) );
    }


    public map<String, mmlib_NamedCredentialCalloutAuth> getAvailableAuthorizations()
    {
        return this.apiAuthorizationsByIdentifierMap;
    }

    public void updateMTICallRecordShallowReconciliationIfMatch( Marketing_Tracking_Info__c record
                                   , mmlib_BaseCallout.CalloutResponse theCallServiceResponse
                                   , String identifier
                                   , mmlib_ISObjectUnitOfWork uow )
    {
        mmctm_ListCallsCallout.Response ctmResponse = (mmctm_ListCallsCallout.Response)theCallServiceResponse;

        list<mmctm_ListCallsCallout.ResponseCall> ctmCalls = ctmResponse.getCTMCalls();

        string workingString = null;

        for ( mmctm_ListCallsCallout.ResponseCall ctmCall : ctmCalls )
        {
            system.debug( '\n\n'
                        + '\nctmCall.called_at = ' + ctmCall.called_at
                        + '\nctmCall.getCalledAtDatetime() == ' + ctmCall.getCalledAtDatetime()
                        + '\nrecord.Tracking_Event_Timestamp__c == ' + record.Tracking_Event_Timestamp__c
                        + '\n\n\n');

            System.debug('TIME LOGIC');
            System.debug(ctmCall.getCalledAtDatetime() + ' >= ' + record.Tracking_Event_Timestamp__c.addMinutes(-60));
            System.debug(ctmCall.getCalledAtDatetime() + ' <= ' + record.Tracking_Event_Timestamp__c.addMinutes(60));

            if ( ctmCall.getCalledAtDatetime() >= record.Tracking_Event_Timestamp__c.addMinutes(-60)
                && ctmCall.getCalledAtDatetime() <= record.Tracking_Event_Timestamp__c.addMinutes(60)
                )
            {
                System.debug('MATCH: ' + true);
                record.Calling_Phone_Number__c = ctmCall.caller_number_complete;
                record.Tracking_Event_Timestamp__c = ctmCall.getCalledAtDatetime();

                record.Medium_Value__c = ctmCall.medium;
                record.Term_Value__c = ctmCall.keyword;

                record.Domain_Value__c = ((mmlib_IDomain)apiAuthorizationsByIdentifierMap.get( identifier )).getDomain();

                record.Call_Info_Resolution_Status__c = 'ProcessedCallResolvedShallow';

                uow.registerDirty( record );
                break;
            }
        }

        //if ( this.isDebugOn )
        system.debug('recordid : ' + record.id + ' -- record.Call_Info_Resolution_Status__c == ' + record.Call_Info_Resolution_Status__c);
    }

    public void updateMTICallRecordIfMatch( Marketing_Tracking_Info__c record
                                   , mmlib_BaseCallout.CalloutResponse theCallServiceResponse
                                   , String identifier
                                   , mmlib_ISObjectUnitOfWork uow )
    {
        mmctm_ListCallsCallout.Response ctmResponse = (mmctm_ListCallsCallout.Response)theCallServiceResponse;

        list<mmctm_ListCallsCallout.ResponseCall> ctmCalls = ctmResponse.getCTMCalls();

        string workingString = null;

        for ( mmctm_ListCallsCallout.ResponseCall ctmCall : ctmCalls )
        {
            system.debug( '\n\n'
                        + '\nctmCall.called_at = ' + ctmCall.called_at
                        + '\nctmCall.getCalledAtDatetime() == ' + ctmCall.getCalledAtDatetime()
                        + '\nrecord.Tracking_Event_Timestamp__c == ' + record.Tracking_Event_Timestamp__c
                        + '\n\n\n');

            if ( ctmCall.getCalledAtDatetime() >= record.Tracking_Event_Timestamp__c.addMinutes(-60)
                && ctmCall.getCalledAtDatetime() <= record.Tracking_Event_Timestamp__c.addMinutes(60)
                )
            {
                //if ( this.isDebugOn ) system.debug( 'it is a match' );
                record.Calling_Phone_Number__c = ctmCall.caller_number_complete;
                record.Tracking_Event_Timestamp__c = ctmCall.getCalledAtDatetime();

                if ( string.isNotBlank( ctmCall.campaign_id ) )
                {
                    record.Campaign_Value__c = ctmCall.campaign_id;
                }
                else if ( string.isNotBlank( ctmCall.campaign ) )
                {
                    record.Campaign_Value__c = ctmCall.campaign;
                }

                // Sort out the Source_Value__c
                if ( string.isNotBlank( ctmCall.web_source )
                    || string.isNotBlank( ctmCall.source )
                    )
                {
                    workingString = string.isNotBlank( ctmCall.web_source ) ? ctmCall.web_source : ctmCall.source;

                    //restrict values to be google, facebook, youtube, bing, or email only
                    if ( workingString.containsIgnoreCase( 'google' ) )
                    {
                        record.Source_Value__c = 'google';
                    }
                    else if ( workingString.containsIgnoreCase( 'facebook' ) )
                    {
                        record.Source_Value__c = 'facebook';
                    }
                    else if ( workingString.containsIgnoreCase( 'youtube' ) )
                    {
                        record.Source_Value__c = 'youtube';
                    }
                    else if ( workingString.containsIgnoreCase( 'bing' ) )
                    {
                        record.Source_Value__c = 'bing';
                    }
                    else if ( workingString.containsIgnoreCase( 'email' ) )
                    {
                        record.Source_Value__c = 'email';
                    }
                    else
                    {
                        // did not find a match
                        // don't set a value for source.
                        record.Source_Value__c = null;
                    }
                }

                record.Medium_Value__c = ctmCall.medium;
                record.Term_Value__c = ctmCall.keyword;

                if ( string.isBlank( ctmCall.campaign_id )
                    && string.isBlank( ctmCall.web_source )
                    && 'Google Ad Extension'.equalsIgnoreCase( ctmCall.source )
                    )
                {
                    // this is a special edge case for a Google Ad Extension.  This is a call as a result of a desktop call extension.
                    // That is, on the desktop, sometimes Google runs out of dynamic forwarding numbers and, instead, serves the raw
                    // tracking number instead in these cases. When this happens, CTM is unable to pull information about the call
                    // from the adwords API

                    record.Campaign_Value__c = ctmCall.tracking_label;
                    record.Source_Value__c = 'google';
                }

                if ( 'Google Ad Extension'.equalsIgnoreCase( ctmCall.source )
                    || 'Google Adwords'.equalsIgnoreCase( ctmCall.source )
                    )
                {
                    record.Medium_Value__c = 'cpc';
                }

                if ( string.isNotBlank( ctmCall.source )
                    && ctmCall.source.containsIgnoreCase('Ad Extension')  // this should cover source = 'Ad Extension' and source = 'Google Ad Extension'
                    )
                {
                    record.Ad_Type__c = 'CallExtension';
                }

                if ( string.isNotBlank( ctmCall.source )
                    && ctmCall.source.containsIgnoreCase('Google Local Search Ads')
                    )
                {
                    record.Medium_Value__c = 'cpc';
                    record.Source_Value__c = 'google';
                    record.Campaign_Value__c = 'SN - Local Search Ads';
                }

                record.Domain_Value__c = ((mmlib_IDomain)apiAuthorizationsByIdentifierMap.get( identifier )).getDomain();

                record.Call_Info_Resolution_Status__c = 'ProcessedCallResolved';

                uow.registerDirty( record );

                mmmarketing_TrackingInfoFactory.getInstance(uow).setNewRecordInstance( record );

                if ( string.isNotBlank( ctmCall.source )) mmmarketing_TrackingInfoFactory.getInstance(uow).prepareTrackingDetailInfoRecord('source', ctmCall.source );
                if ( string.isNotBlank( ctmCall.web_source )) mmmarketing_TrackingInfoFactory.getInstance(uow).prepareTrackingDetailInfoRecord('source', ctmCall.web_source );
                if ( string.isNotBlank( ctmCall.campaign )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('campaign', ctmCall.campaign );
                if ( string.isNotBlank( ctmCall.campaign_id )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('campaign_id', ctmCall.campaign_id );
                if ( string.isNotBlank( ctmCall.adgroup_id )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('adgroup_id', ctmCall.adgroup_id );
                if ( string.isNotBlank( ctmCall.ad_group_id )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('ad_group_id', ctmCall.ad_group_id );
                if ( string.isNotBlank( ctmCall.latitude )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('latitude', ctmCall.latitude );
                if ( string.isNotBlank( ctmCall.longitude )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('longitude', ctmCall.longitude );
                if ( string.isNotBlank( ctmCall.id )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('ctm_id', ctmCall.id );
                if ( string.isNotBlank( ctmCall.hold_time )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('hold_time', ctmCall.hold_time );
                if ( string.isNotBlank( ctmCall.ad_slot )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('ad_slot', ctmCall.ad_slot );
                if ( string.isNotBlank( ctmCall.ad_slot_position )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('ad_slot_position', ctmCall.ad_slot_position );
                if ( string.isNotBlank( ctmCall.duration )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('duration', ctmCall.duration );
                if ( string.isNotBlank( ctmCall.hold_time )) mmmarketing_TrackingInfoFactory.getInstance().prepareTrackingDetailInfoRecord('hold_time', ctmCall.hold_time );

                break;
            }
        }

        //if ( this.isDebugOn )
        system.debug('recordid : ' + record.id + ' -- record.Call_Info_Resolution_Status__c == ' + record.Call_Info_Resolution_Status__c);

    }

    public void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}