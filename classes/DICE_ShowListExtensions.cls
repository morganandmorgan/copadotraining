public with sharing class DICE_ShowListExtensions {

    public Model model { get; private set; }

    public DICE_ShowListExtensions(ApexPages.StandardController stdController) {
        DICE_Queue__c queue = (DICE_Queue__c) stdController.getRecord();
        DICEService diceService = new DICEService();
        this.model = new Model();

        this.model.intakes = diceService.getIntakes(queue.Id, 2000);
        //this.model.intakesCount = this.model.intakes.size();

        this.model.suppressedIntakes = diceService.SuppressedIntakes;
        //this.model.suppressedIntakesCount = this.model.suppressedIntakes.size();

        this.model.query = diceService.BaseQuery;
    }

    public class Model {

        public List<Intake__c> intakes {
            get;
            set {
                intakes = value;
                intakesCount = intakes.size();
            }
        }
        public List<Intake__c> suppressedIntakes {
            get;
            set {
                suppressedIntakes = value;
                suppressedIntakesCount = suppressedIntakes.size();
            }
        }
        public Integer intakesCount { get; private set; }
        public Integer suppressedIntakesCount { get; private set; }
        public String query { get; set; }
        public List<Schema.FieldSetMember> fieldSetMembers { get { return SObjectType.Intake__c.FieldSets.DICE_Show_List.getFields(); }}

        public Model() {
            this.intakes = new List<Intake__c>();
            this.suppressedIntakes = new List<Intake__c>();
            this.intakesCount = 0;
            this.suppressedIntakesCount = 0;
        }
    }
}