public interface mmmatter_IExpenseTypesSelector
    extends mmlib_ISObjectSelector
{
    List<litify_pm__Expense_Type__c> selectByExternalId(Set<String> idSet);
}