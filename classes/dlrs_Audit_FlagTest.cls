/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Audit_FlagTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Audit_FlagTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Audit_Flag__c());
    }
}