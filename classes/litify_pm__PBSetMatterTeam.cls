/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBSetMatterTeam {
    global PBSetMatterTeam() {

    }
    @InvocableMethod(label='Set Matter Team' description='Sets the matters' teams to the teams provided.')
    global static void setMatterTeam(List<litify_pm.PBSetMatterTeam.PBSetMatterTeamWrapper> input) {

    }
global class PBSetMatterTeamWrapper {
    @InvocableVariable( required=false)
    global litify_pm__Matter__c matter;
    @InvocableVariable( required=false)
    global Id matterTeamId;
    global PBSetMatterTeamWrapper() {

    }
}
}
