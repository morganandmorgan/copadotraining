/*============================================================================
Name            : PaymentMediaDetailsTest
Author          : CLD
Created Date    : July 2018
Description     : Test class for Payment Media Details Trigger - note it must use seeAllData = true due to limitations in FFA API
=============================================================================*/
@isTest (seeAllData=true)
public class PaymentMediaDetailsTest {

    @isTest
    public static void testTriggerCode(){
        List<c2g__codaPaymentMediaDetail__c> pmdlist = [SELECT Id FROM c2g__codaPaymentMediaDetail__c  WHERE Payable_Invoice__r.Finance_Request__c != null
			AND c2g__PaymentMediaSummary__r.c2g__PaymentReference__c != null LIMIT 1];
        update pmdlist;
    }
}