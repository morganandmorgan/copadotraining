public class mmlib_CronTriggerAbortExecutor
	implements mmlib_GenericBatch.IGenericExecuteBatch
{
	public void run( list<SObject> scope )
	{
		for ( SObject process : scope )
		{
			System.abortJob( process.id );
		}
	}
}