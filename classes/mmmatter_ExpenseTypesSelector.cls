public class mmmatter_ExpenseTypesSelector
    extends mmlib_SObjectSelector
    implements mmmatter_IExpenseTypesSelector
{
    public static mmmatter_IExpenseTypesSelector newInstance()
    {
        return (mmmatter_IExpenseTypesSelector) mm_Application.Selector.newInstance(litify_pm__Expense_Type__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Expense_Type__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            litify_pm__Expense_Type__c.CostType__c,
            litify_pm__Expense_Type__c.ExternalID__c
        };
    }

    public List<litify_pm__Expense_Type__c> selectById(Set<Id> idSet)
    {
        return Database.query(newQueryFactory().setCondition('id in :idSet').toSOQL());
    }

    public List<litify_pm__Expense_Type__c> selectByExternalId(Set<String> idSet)
    {
        return
            (List<litify_pm__Expense_Type__c>)
            Database.query(
                newQueryFactory()
                .setCondition(litify_pm__Expense_Type__c.ExternalID__c + ' in :idSet')
                .toSOQL()
            );
    }
}