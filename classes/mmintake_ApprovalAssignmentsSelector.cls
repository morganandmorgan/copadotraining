public class mmintake_ApprovalAssignmentsSelector
    extends mmlib_SObjectSelector
    implements mmintake_IApprovalAssignmentsSelector
{
    public static mmintake_IApprovalAssignmentsSelector newInstance()
    {
        return (mmintake_IApprovalAssignmentsSelector) mm_Application.Selector.newInstance(Approval_Assignment__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Approval_Assignment__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            Approval_Assignment__c.Approving_Attorney_1__c,
            Approval_Assignment__c.Approving_Attorney_2__c,
            Approval_Assignment__c.Approving_Attorney_3__c,
            Approval_Assignment__c.Approving_Attorney_4__c,
            Approval_Assignment__c.Approving_Attorney_5__c,
            Approval_Assignment__c.Approving_Attorney_6__c,
            Approval_Assignment__c.Case_Type__c,
            Approval_Assignment__c.ClassActionType__c,
            Approval_Assignment__c.County__c,
            Approval_Assignment__c.Did_the_accident_involve_a_truck_or_bus__c,
            Approval_Assignment__c.Handling_Firm__c,
            Approval_Assignment__c.New_Case_Email__c,
            Approval_Assignment__c.State_of_incident__c,
            Approval_Assignment__c.Type_of_Dispute__c,
            Approval_Assignment__c.Venue__c,
            Approval_Assignment__c.Where_did_the_injury_occur__c
        };
    }

    public List<Approval_Assignment__c> selectForAssignmentOperation
    (
        Set<String> caseTypeSet,
        Set<String> venueSet,
        Set<String> handlingFirmSet
    )
    {
        return 
            Database.query(
                newQueryFactory()
                .setCondition(
                    Approval_Assignment__c.Case_Type__c + ' in :caseTypeSet and ' +
                    Approval_Assignment__c.Venue__c + ' in :venueSet and ' +
                    Approval_Assignment__c.Handling_Firm__c + ' in :handlingFirmSet')
                .addOrdering(Approval_Assignment__c.CreatedDate, fflib_QueryFactory.SortOrder.Ascending)
                .toSOQL());
    }
}