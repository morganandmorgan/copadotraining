public with sharing class CityStateListController {
  public String CityStatesJSArray { get; private set; }
	public CityStateListController() {
		List<Address_by_Zip_Code__c> CityStates = [SELECT City__c, State__c, State_Code__c FROM Address_by_Zip_Code__c];
    List<String> strs = new List<String>();
    for (Address_by_Zip_Code__c cs : CityStates) {
      strs.add('{city: "' + cs.City__c + '", state: "' + cs.State__c + '", state_code:"' + cs.State_Code__c + '"}');
    }
    CityStatesJSArray = '[' + String.join(strs, ',') + ']';
	}
}