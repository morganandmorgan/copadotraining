public with sharing class ContactSearchCtlr {

    @TestVisible private static final String ROLE_CALLER = Intake__c.Caller__c.getDescribe().getLabel(); // 'Caller';
    @TestVisible private static final String ROLE_CLIENT = Intake__c.Client__c.getDescribe().getLabel(); // 'Client';
    // eventually this should be replaced with Intake__c.Injured_Party__c.getDescribe().getLabel(); but for now I am leaving it alone.
    //  The missing space between the two words is the issue.  There are several references in the ContactSearch page as well as
    //  the ContactSearch.js Static Resource.  Those would need to be accounted for if this change were made.
    @TestVisible private static final String ROLE_INJURED_PARTY = 'InjuredParty';

    private static final Map<String, Schema.SObjectField> ROLE_TO_RELATIONSHIP = new Map<String, Schema.SObjectField>{
            ROLE_CALLER => Intake__c.Caller__c,
            ROLE_CLIENT => Intake__c.Client__c,
            ROLE_INJURED_PARTY => Intake__c.Injured_Party__c
    };

    @TestVisible private final String DNIS; // DNIS (Dialed Number Identification Service) is a telephone service that identifies for the receiver of a call the number that the caller dialed.
    @TestVisible private final String ANI; // ANI (Automatic Number Identification) is a service that provides the receiver of a telephone call with the number of the calling phone.
    @TestVisible private final String CISCO_QUEUE;
    @TestVisible
    private final DateTime ANICallStartTime;

    private transient Boolean hasUpsertAccountError = false;
    private transient Boolean searchOnLoad = false;

    public Id intakeToLoadId { get; set; }
    public Id personToLoadId { get; set; }
    public String personRole { get; set; }
    public List<Account> accountsWithRoleValidationError { get; private set; } {
        accountsWithRoleValidationError = new List<Account>();
    }

    public String callerSwapSelection { get; set; }
    public String injuredPartySwapSelection { get; set; }
    public String clientSwapSelection { get; set; }

    public String newPersonFirstName {
        get {
            if (newPerson != null) {
                return newPerson.FirstName;
            }
            return null;
        }
        set {
            if (newPerson != null) {
                newPerson.FirstName = value;
            }
        }
    }

    public String newPersonMiddleName {
        get {
            if (newPerson != null) {
                return newPerson.MiddleName;
            }
            return null;
        }
        set {
            if (newPerson != null) {
                newPerson.MiddleName = value;
            }
        }
    }

    public String newPersonLastName {
        get {
            if (newPerson != null) {
                return newPerson.LastName;
            }
            return null;
        }
        set {
            if (newPerson != null) {
                newPerson.LastName = value;
            }
        }
    }

    public String newPersonSuffix {
        get {
            if (newPerson != null) {
                return newPerson.Suffix;
            }
            return null;
        }
        set {
            if (newPerson != null) {
                newPerson.Suffix = value;
            }
        }
    }

    public String newPersonBirthDate {
        get {
            if (newPerson != null && newPerson.Date_of_Birth_mm__c != null) {
                return newPerson.Date_of_Birth_mm__c.format();
            }
            return newPersonBirthDate;
        }
        set {
            newPersonBirthDate = null;
            if (newPerson != null) {
                try {
                    newPerson.Date_of_Birth_mm__c = String.isBlank(value) ? null : Date.parse(value);
                } catch (Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid ' + Account.Date_of_Birth_mm__c.getDescribe().getLabel() + '. Please select again.', e.getMessage()));
                    newPersonBirthDate = value;
                }
            }
        }
    }

    public Intake__c selectedIntake { get; set; }

    public Account newPerson {
        get {
            if (newPerson == null) {
                // TODO: Move to an Account Factory??
                newPerson = new Account(RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Person_Account'));
            }
            return newPerson;
        }
        set;
    }

    /**
     * Default constructor
     */
    public ContactSearchCtlr() {
        system.debug('ContactSearchCtlr constructor has been called.');

        ANI = ApexPages.currentPage().getParameters().get('ani');

        ANICallStartTime = datetime.now();

        CISCO_QUEUE = ApexPages.currentPage().getParameters().get('dnis');
        Id intakeId = getOptionalIdFromPageParameters('intakeId');
        Id cid = getOptionalIdFromPageParameters('cid');

        queryWizardReasonForCall(ApexPages.currentPage().getParameters().get('CallVariable6'));

        intakeToLoadId = intakeId;
        loadIntake();

        if (String.isNotBlank(ANI)) {
            newPerson.Phone = ANI;
            searchOnLoad = true;
        } else {
            personToLoadId = cid;
            loadPersonAccount();
        }

        initializeModal();
    }

    private Boolean isNewIntakeRecord() {
        return this.selectedIntake != null && this.selectedIntake.Id == null;
    }

    private PageReference getNextPage() {
        PageReference nextPage = null;

        // if the ANI value is blank
        if (string.isblank(ANI)
                // and there are no existing MTI call records for this intake
                && mmmarketing_TrackingInfosSelector.newInstance().selectByIntake(new set<id>{
                selectedIntake.id
        }).isEmpty()) {
            // then go to the CallerID page
            system.debug('selectedIntake.id == ' + selectedIntake.id);
            system.debug('ANICallStartTime == ' + ANICallStartTime);

            nextPage = mmintake_PageReferenceUtils.getConfirmCallerID(selectedIntake.id, ANICallStartTime);
        } else {
            // otherwise go to the IntakeFlow page
            nextPage = mmintake_PageReferenceUtils.getIntakeFlow(selectedIntake);
        }

        return nextPage;
    }

    /**
     * Display helper method
     */
    private void swapContactInfo(Account sourceAccount, String sourceRole, String targetRole) {
        if (sourceRole != targetRole) {
            if (targetRole == ROLE_CALLER) {
                setIntakePersonAccountRelationship(selectedIntake, sourceRole, selectedIntake.Caller__r);
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CALLER, sourceAccount);
            } else if (targetRole == ROLE_INJURED_PARTY) {
                setIntakePersonAccountRelationship(selectedIntake, sourceRole, selectedIntake.Injured_Party__r);
                setIntakePersonAccountRelationship(selectedIntake, ROLE_INJURED_PARTY, sourceAccount);
            } else if (targetRole == ROLE_CLIENT) {
                setIntakePersonAccountRelationship(selectedIntake, sourceRole, selectedIntake.Client__r);
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, sourceAccount);
            }
        }

        callerSwapSelection = null;
        injuredPartySwapSelection = null;
        clientSwapSelection = null;
    }

    private Boolean saveIntakeRecord(Boolean validateAccountRoles) {
        Boolean success = false;

        try {
            new IntakeService().saveRecords(new List<Intake__c>{
                    selectedIntake
            }, validateAccountRoles);

            intakeToLoadId = selectedIntake.Id;

            loadIntake();

            if (String.isNotBlank(ANI)) {
                mmmarketing_ITrackInfoRecordCallRequest recordCallRequest = mmmarketing_TrackInfoRecordCallRequest.newInstance()
                        .setCallingNumber(ANI)
                        .setCallStartTime(ANICallStartTime)
                        .setRelatedIntakeId(selectedIntake.Id);

                mmmarketing_TrackingInfosService.recordCallsForIntakes(new List<mmmarketing_ITrackInfoRecordCallRequest>{
                        recordCallRequest
                });
            }

            success = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Save was successful.', ''));
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occured attempting to save.', e.getMessage()));
        }

        return success;
    }

//TODO: Move this upsert logic to service tier.
    private Boolean upsertAllIntakeRoles() {
        Boolean success = false;
        system.debug('upsertAllIntakeRoles() method has been called');
        if (selectedIntake != null) {
            Boolean isNewIntakeRecord = selectedIntake.Id == null;

            try {
                Database.update(accountsWithRoleValidationError);

                map<id, Account> accountMap = new map<id, Account>(mmcommon_AccountsSelector.newInstance().selectById(new Set<id>{
                        selectedIntake.Caller__c
                        , selectedIntake.Injured_Party__c
                        , selectedIntake.Client__c
                }));

                setIntakePersonAccountRelationship(selectedIntake, ROLE_CALLER, accountMap.get(selectedIntake.Caller__c));
                setIntakePersonAccountRelationship(selectedIntake, ROLE_INJURED_PARTY, accountMap.get(selectedIntake.Injured_Party__c));
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, accountMap.get(selectedIntake.Client__c));

                if (!isInvalidIntake(selectedIntake) && !hasInvalidRoles(selectedIntake)) {
                    // UPSERTing an Intake
                    Database.upsert(selectedIntake);

                    success = true;

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Save was successful.', ''));
                }
            } catch (Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occured attempting to save.', e.getMessage()));

                if (isNewIntakeRecord) {
                    selectedIntake.Id = null;
                }
            }

            if (success) {
                intakeToLoadId = selectedIntake.Id;
                loadIntake();
            }
        }
        return success;
    }

    private Boolean isInvalidIntake(Intake__c intakeToSave) {
        Boolean hasValidationError = false;

        if (String.isBlank(intakeToSave.Caller_is_Injured_Party__c)) {
            intakeToSave.addError(Intake__c.Caller_is_Injured_Party__c.getDescribe().getLabel() + ' is required.');
            hasValidationError = true;
        }

        if (intakeToSave.Caller_is_Injured_Party__c == 'Yes'
                && intakeToSave.Caller__c != intakeToSave.Injured_Party__c) {
            intakeToSave.addError('Caller and Injured Party must be set to the same person');
            hasValidationError = true;
        }

        if (intakeToSave.Caller_is_Injured_Party__c == 'No') {
            if (intakeToSave.Caller__c == intakeToSave.Injured_Party__c
                    && intakeToSave.Caller__c != null) {
                intakeToSave.addError('Caller and Injured Party must be set to different people');
                hasValidationError = true;
            } else if (String.isBlank(intakeToSave.Injured_party_relationship_to_Caller__c)) {
                intakeToSave.addError(Intake__c.Injured_party_relationship_to_Caller__c.getDescribe().getLabel() + ' is required.');
                hasValidationError = true;
            } else if (intakeToSave.Injured_Party__c == null) {
                intakeToSave.addError('Injured Party is required');
                hasValidationError = true;
            }
        }

        if (intakeToSave.Can_IP_sign__c == 'Yes'
                && intakeToSave.Injured_Party__c != intakeToSave.Client__c) {
            intakeToSave.addError('Injured Party and Client must be set to the same person');
            hasValidationError = true;
        }

        if (intakeToSave.Can_IP_sign__c != 'Yes'
                && String.isNotBlank(intakeToSave.Can_IP_sign__c)
                && intakeToSave.Injured_Party__c == intakeToSave.Client__c
                && intakeToSave.Injured_Party__c != null) {
            intakeToSave.addError('Injured Party and Client must be set to different people');
            hasValidationError = true;
        }

        return hasValidationError;
    }

    /*
     *  validation method to ensure that
     */
    private Boolean hasInvalidRoles(Intake__c intakeWithRoles) {
        accountsWithRoleValidationError.clear();

        // this basically says that if an account associated with the intake record does not have a email address in PersonEmail,
        //  then that account has a "role validation error"
        for (Account intakeAccount : getUniqueAccounts(intakeWithRoles).values()) {
            if (String.isBlank(intakeAccount.PersonEmail)) {
                accountsWithRoleValidationError.add(intakeAccount);
            }
        }
        return getHasIntakeRoleValidationError();
    }

    /*
     *  this method simply sorts all of the associated related accounts to the intake
     */
    private static Map<Id, Account> getUniqueAccounts(Intake__c intake) {
        Map<Id, Account> allRelatedAccounts = new Map<Id, Account>();

        if (intake.Client__c != null) {
            allRelatedAccounts.put(intake.Client__c, intake.Client__r);
        }
        if (intake.Injured_Party__c != null) {
            allRelatedAccounts.put(intake.Injured_Party__c, intake.Injured_Party__r);
        }
        if (intake.Caller__c != null) {
            allRelatedAccounts.put(intake.Caller__c, intake.Caller__r);
        }

        return allRelatedAccounts;
    }

    private static Id getOptionalIdFromPageParameters(String paramName) {
        Id result = null;

        String param = ApexPages.currentPage().getParameters().get(paramName);

        if (String.isNotBlank(param)) {
            try {
                result = Id.valueOf(param);
            } catch (Exception e) {
            }
        }

        return result;
    }

    private static void setIntakePersonAccountRelationship(Intake__c selectedIntake, String personRole, Account newPerson) {
        Schema.SObjectField fieldToken = ROLE_TO_RELATIONSHIP.get(personRole);

        if (fieldToken != null) {
            Id newPersonId = newPerson == null ? null : newPerson.Id;
            selectedIntake.put(fieldToken, newPersonId);
            selectedIntake.putSObject(fieldToken, newPerson);
        }
    }

    private static Account getIntakePersonAccountRelationship(Intake__c selectedIntake, String personRole) {
        Schema.SObjectField fieldToken = ROLE_TO_RELATIONSHIP.get(personRole);

        if (fieldToken != null) {
            return (Account) selectedIntake.getSObject(fieldToken);
        }
        return null;
    }

    /**
     * Display helper method
     */
    public Boolean getRenderInjuredPartyBlock() {
        return selectedIntake.Injured_Party__c != null
                || (selectedIntake.Caller_is_Injured_Party__c == 'No'
                && String.isNotBlank(selectedIntake.Injured_party_relationship_to_Caller__c)
                && getNextRoleForNewAccount() == ROLE_INJURED_PARTY);
    }

    /**
     * Display helper method
     */
    public Boolean getRenderClientBlock() {
        return selectedIntake.Client__c != null
                || (selectedIntake.Injured_Party__c != null
                && selectedIntake.Can_IP_Sign__c != null);
    }

    public PageReference clearRole() {
        if (selectedIntake != null) {
            setIntakePersonAccountRelationship(selectedIntake, personRole, null);

            if (personRole == ROLE_INJURED_PARTY) {
                selectedIntake.Caller_is_Injured_Party__c = null;
            } else if (personRole == ROLE_CLIENT) {
                if (selectedIntake.Can_IP_sign__c == 'Yes') {
                    selectedIntake.Can_IP_sign__c = null;
                }
                selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c = null;
            }
            personRole = null;
        }
        return null;
    }

    public PageReference loadIntake() {
        if (intakeToLoadId != null) {
            List<Schema.FieldSet> fieldSets = new List<Schema.FieldSet>{
                    getFieldSet()
            };
            List<Intake__c> result = mmintake_IntakesSelector.newInstance().selectByIdWithCallerClientInjuredPartyAndAdditionalFields(new Set<id>{
                    intakeToLoadId
            }, fieldSets, null);

            if (!result.isEmpty()) {
                selectedIntake = result.get(0);
            }
        }

        if (selectedIntake == null) {
            // TODO : Move this to the intake service.

            selectedIntake = new Intake__c();

            selectedIntake.InputChannel__c = 'ContactSearch';

            if (String.isNotBlank(CISCO_QUEUE)) {
                selectedIntake.Cisco_Queue__c = CISCO_QUEUE;

                List<Cisco_Queue_Setting__mdt> ciscoQueueSettings = [
                        SELECT Marketing_Source__c, Handling_Firm__c
                        FROM Cisco_Queue_Setting__mdt
                        WHERE Cisco_Queue__c = :CISCO_QUEUE
                        AND Venue__c = null
                ];

                if (!ciscoQueueSettings.isEmpty()) {
                    selectedIntake.Marketing_Source__c = ciscoQueueSettings.get(0).Marketing_Source__c;
                    selectedIntake.Handling_Firm__c = ciscoQueueSettings.get(0).Handling_Firm__c;
                }
            }
        }

        newPerson = null;

        return null;
    }

    public PageReference loadPersonAccount() {
        List<Account> accountList = mmcommon_AccountsSelector.newInstance().selectById(new set<id>{
                personToLoadId
        });

        if (!accountList.isEmpty()) {
            String nextRole = getNextRoleForNewAccount();

            setIntakePersonAccountRelationship(selectedIntake, nextRole, accountList[0]);

            newPerson = null;
        }

        personToLoadId = null;

        return null;
    }

    public PageReference updatePersonRole() {
        return null;
    }

    /**
     * Display helper method
     */
    public PageReference swapCallerInfo() {
        swapContactInfo(selectedIntake.Caller__r, ROLE_CALLER, callerSwapSelection);

        return null;
    }

    /**
     * Display helper method
     */
    public PageReference swapInjuredPartyInfo() {
        swapContactInfo(selectedIntake.Injured_Party__r, ROLE_INJURED_PARTY, injuredPartySwapSelection);

        return null;
    }

    /**
     * Display helper method
     */
    public PageReference swapClientInfo() {
        swapContactInfo(selectedIntake.Client__r, ROLE_CLIENT, clientSwapSelection);

        return null;
    }

    public PageReference changedCallerSameAsInjuredParty() {
        if (selectedIntake.Caller_is_Injured_Party__c == 'No') {
            if (selectedIntake.Caller__c == selectedIntake.Injured_Party__c) {
                setIntakePersonAccountRelationship(selectedIntake, ROLE_INJURED_PARTY, null);
            }

            if (selectedIntake.Can_IP_sign__c == 'Yes') {
                selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c = null;
                selectedIntake.Injured_party_relationship_to_Client__c = null;
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, selectedIntake.Injured_Party__r);
            } else if (selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c == 'Caller') {
                selectedIntake.Injured_party_relationship_to_Client__c = null;
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, selectedIntake.Caller__r);
            }
        } else if (selectedIntake.Caller_is_Injured_Party__c == 'Yes') {
            setIntakePersonAccountRelationship(selectedIntake, ROLE_INJURED_PARTY, selectedIntake.Caller__r);

            selectedIntake.Injured_party_relationship_to_Caller__c = null;

            if (selectedIntake.Can_IP_sign__c == 'Yes') {
                selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c = null;
                selectedIntake.Injured_party_relationship_to_Client__c = null;
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, selectedIntake.Injured_Party__r);
            } else {
                selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c = 'Other';

                if (selectedIntake.Client__c == selectedIntake.Injured_Party__c) {
                    setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, null);
                }
            }
        }
        return null;
    }

    public PageReference changedInjuredPartyAbilityToSign() {
        if (selectedIntake.Can_IP_sign__c == 'Yes') {
            selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c = null;
            selectedIntake.Injured_party_relationship_to_Client__c = null;
            setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, selectedIntake.Injured_Party__r);
        } else {
            if (selectedIntake.Caller_is_Injured_Party__c == 'No') {
                if (selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c == 'Caller') {
                    selectedIntake.Injured_party_relationship_to_Client__c = null;
                    setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, selectedIntake.Caller__r);
                }
            } else if (selectedIntake.Caller_is_Injured_Party__c == 'Yes') {
                selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c = 'Other';
                if (selectedIntake.Client__c == selectedIntake.Injured_Party__c) {
                    setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, null);
                }
            }
        }
        return null;
    }

    public PageReference changedInjuredPartyLegalSigner() {
        if (selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c == 'Caller') {
            selectedIntake.Injured_party_relationship_to_Client__c = null;
            setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, selectedIntake.Caller__r);
        } else if (selectedIntake.Who_can_legally_sign_on_behalf_of_IP__c == 'Other') {
            if (selectedIntake.Client__c == selectedIntake.Injured_Party__c
                    || selectedIntake.Client__c == selectedIntake.Caller__c) {
                setIntakePersonAccountRelationship(selectedIntake, ROLE_CLIENT, null);
            }
        }
        return null;
    }

    /**
     * Display helper method
     */
    public Boolean getHasUpsertAccountError() {
        return hasUpsertAccountError == true;
    }

    /**
     * Display helper method
     */
    public Boolean getHasIntakeRoleValidationError() {
        return !accountsWithRoleValidationError.isEmpty();
    }

    /**
     * Display helper method
     */
    public Boolean getSearchOnLoad() {
        return searchOnLoad == true;
    }

    public PageReference upsertAccount() {
        system.debug('upsertAccount');

        hasUpsertAccountError = false;

        Account selectedPerson = String.isBlank(personRole) ? newPerson : getIntakePersonAccountRelationship(selectedIntake, personRole);

        if (selectedPerson != null) {
            try {
                Boolean isNewPersonRecord = selectedPerson.Id == null;

                mmcommon_PersonAccountsService.saveRecords(new List<Account>{
                        selectedPerson
                });

                if (isNewPersonRecord) {
                    String nextRole = getNextRoleForNewAccount();

                    List<Account> requeriedPersons = mmcommon_AccountsSelector.newInstance().selectById(new set<id>{
                            selectedPerson.Id
                    });

                    setIntakePersonAccountRelationship(selectedIntake, nextRole, requeriedPersons[0]);

                    newPerson = null;
                }

                personRole = null;

            } catch (mmcommon_PersonAccountsServiceExceptions.ValidationException ve) {
                system.debug(ve);
                system.debug(ve.getCause());

                if (ve.getCause() == null) {
                    system.debug(ve.getMessage());
                } else {
                    system.debug(ve.getCause().getMessage());

                    if (ve.getCause().getCause() != null) {
                        system.debug(ve.getCause().getCause().getMessage());
                    }
                }

                hasUpsertAccountError = true;
            } catch (Exception e) {
                system.debug(e);
                system.debug(e.getStackTraceString());
                system.debug(e.getCause());

                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occured attempting to save.', e.getMessage()));

                hasUpsertAccountError = true;
            }
        }
        return null;
    }

    /**
     * Display helper method
     */
    public String getNextRoleForNewAccount() {
        if (selectedIntake != null) {
            if (selectedIntake.Caller__c == null) {
                return ROLE_CALLER;
            } else if (selectedIntake.Injured_Party__c == null) {
                return ROLE_INJURED_PARTY;
            } else if (selectedIntake.Client__c == null) {
                return ROLE_CLIENT;
            }
        }
        return null;
    }

    public PageReference saveAndEdit() {
        if (saveIntakeRecord(true)) {
            return getNextPage();
        }
        return null;
    }

    public PageReference quickSave() {
        saveIntakeRecord(false);
        return null;
    }

    public PageReference saveInvalidIntakeRoles() {
        if (upsertAllIntakeRoles()) {
            return getNextPage();
        }
        return null;
    }

    @RemoteAction
    public static List<SearchResult> searchAccounts(String firstName, String lastName, String email, String phoneNum, String birthDateStr) {
        Date birthDate = String.isEmpty(birthDateStr) ? null : Date.parse(birthDateStr);
        List<Account> accounts = mmcommon_PersonAccountsService.searchAccounts(firstName, lastName, email, phoneNum, birthDate);

        List<SearchResult> searchResults = new List<SearchResult>();

        for (Account a : accounts) {
            System.debug(a);
            SearchResult newSearchResult = new SearchResult(a);

            newSearchResult.isFirstNameMatching = String.isNotBlank(firstName) && String.isNotBlank(a.FirstName) && a.FirstName.equalsIgnoreCase(mmlib_Utils.clean(firstName)) ? 'true' : 'false';
            newSearchResult.isLastNameMatching = String.isNotBlank(lastName) && String.isNotBlank(a.LastName) && a.LastName.equalsIgnoreCase(mmlib_Utils.clean(lastName)) ? 'true' : 'false';

            newSearchResult.isPhoneMatching = String.isNotBlank(phoneNum) && String.isNotBlank(a.Phone_Unformatted_Formula__c) && a.Phone_Unformatted_Formula__c.equalsIgnoreCase(mmlib_Utils.stripPhoneNumber(phoneNum)) ? 'true' : 'false';
            newSearchResult.isPersonMobilePhoneMatching = String.isNotBlank(phoneNum) && String.isNotBlank(a.MobilePhone_Unformatted_Formula__pc) && a.MobilePhone_Unformatted_Formula__pc.equalsIgnoreCase(mmlib_Utils.stripPhoneNumber(phoneNum)) ? 'true' : 'false';

            newSearchResult.isEmailMatching = String.isNotBlank(email) && String.isNotBlank(a.PersonEmail) && a.PersonEmail.equalsIgnoreCase(mmlib_Utils.clean(email)) ? 'true' : 'false';

            if (birthDate != null
                    && a.Date_of_Birth_mm__c != null) {
                system.debug(birthDate);
                system.debug(a.Date_of_Birth_mm__c);

                if (birthDate.day() == a.Date_of_Birth_mm__c.day()
                        && birthDate.month() == a.Date_of_Birth_mm__c.month()
                        && birthDate.year() == a.Date_of_Birth_mm__c.year()) {
                    newSearchResult.isDateOfBirthMatching = 'true';
                }

            }

            searchResults.add(newSearchResult);
        }

        return searchResults;
    }

    private String targetQuestionToken = 'questionIVR01';
    private String wizardReasonText = null;
    private void queryWizardReasonForCall(String ciscoSessionId) {
        if (wizardReasonText == null && String.isNotBlank(ciscoSessionId)) {

            for
                (
                        QuestionAndAnswer__c ans :
                        mmwiz_QuestionAndAnswerSelector.newInstance().selectBySessionGuid(new Set<String>{
                                ciscoSessionId
                        })
                ) {
                if (targetQuestionToken.equalsIgnoreCase(ans.QuestionToken__c)) {
                    wizardReasonText = ans.Answer__c;
                }
            }
        } else {
            wizardReasonText = '';
        }
    }

    public String getWizardReasonForCall() {
        return wizardReasonText;
    }

    public class SearchResult {
        public Account person { get; private set; }

        public List<IntakeResult> intakes { get; private set; } {
            intakes = new List<IntakeResult>();
        }

        public String formattedBirthDate { get; private set; }

        public String isFirstNameMatching { get; private set; } {
            isFirstNameMatching = 'false';
        }
        public String isLastNameMatching { get; private set; } {
            isLastNameMatching = 'false';
        }
        public String isDateOfBirthMatching { get; private set; } {
            isDateOfBirthMatching = 'false';
        }
        public String isPhoneMatching { get; private set; } {
            isPhoneMatching = 'false';
        }
        public String isPersonMobilePhoneMatching { get; private set; } {
            isPersonMobilePhoneMatching = 'false';
        }
        public String isEmailMatching { get; private set; } {
            isEmailMatching = 'false';
        }

        public SearchResult(Account a) {
            this.person = a;

            if (a != null) {
                Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>();
                System.debug('caller' + a.Caller_Intakes__r);
                System.debug('Survyes' + a.Intake_Surveys__r);
                intakeMap.putAll(a.Intake_Surveys__r);
                intakeMap.putAll(a.Caller_Intakes__r);
                intakeMap.putAll(a.InjuredParty_Intakes__r);

                for (Intake__c intake : intakeMap.values()) {
                    this.intakes.add(new IntakeResult(intake));
                }

                if (a.Date_of_Birth_mm__c != null) {
                    this.formattedBirthDate = a.Date_of_Birth_mm__c.format();
                }
            }
        }
    }

    public class IntakeResult {
        public Intake__c intake { get; private set; }

        public String formattedIncidentDate { get; private set; }

        public IntakeResult(Intake__c intake) {
            this.intake = intake;

            if (intake.What_was_the_date_of_the_incident__c != null) {
                this.formattedIncidentDate = intake.What_was_the_date_of_the_incident__c.format();
            }
        }
    }

    // Modal support below.  initializeModal() is also called from ContactSearchCtlr() constructor for this class above.
    public Model model { get; set; }

    private void initializeModal() {
        this.model = new Model();

        if (ApexPages.currentPage().getParameters().containsKey('ID')) {
            System.debug('JLW:::' + ApexPages.currentPage().getParameters().get('ID'));
            String id = ApexPages.currentPage().getParameters().get('ID').replace('%2F', '/');
            if (String.isNotBlank(id)) {
                List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByCiscoCallId(id);
                if (intakes != null && !intakes.isEmpty()) {
                    this.model = new Model(true);
                    this.intakeToLoadId = intakes[0].Id;
                    this.loadIntake();
                }
            }
        }
    }

    public void closeModal() {
        this.model.showModal = false;
    }

    public Schema.FieldSet getFieldSet() {
        return SObjectType.Intake__c.fieldSets.Contact_Search_Modal;
    }

    public List<Schema.FieldSetMember> getFields() {
        return getFieldSet().getFields();
    }

    public class Model {
        public Boolean showModal { get; set; }

        public Model() {
            this.showModal = false;
        }

        public Model(Boolean showModal) {
            this.showModal = showModal;
        }
    }
}