/**
 *  mmwiz_QuestionnaireService
 */
public with sharing class mmwiz_QuestionnaireService
{
    private static mmwiz_IQuestionnaireService service()
    {
        return (mmwiz_IQuestionnaireService) mm_Application.Service.newInstance( mmwiz_IQuestionnaireService.class );
    }

    public static mmwiz_QuestionnaireModel findQuestionnaire( string questionnaireToken, PageReference pageRef, String sessionGuid )
    {
        return service().findQuestionnaire( questionnaireToken, pageRef, sessionGuid );
    }

    public static list<mmwiz_AbstractActionModel> processResponseActions( String questionnaireToken, String pageToken, map<string, list<string>> questionResponseMap, PageReference pageRef, String sessionGuid )
    {
        return service().processResponseActions( questionnaireToken, pageToken, questionResponseMap, pageRef, sessionGuid );
    }
}