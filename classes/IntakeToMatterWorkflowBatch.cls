global class IntakeToMatterWorkflowBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

	String SUCCESS_STATUS = 'Success';
	String PROCESSING_STATUS = 'Processing';
	Set<Id> completedTaskIds = new Set<Id>();
	Map<Id, String> matterIdTaskIdMap;
	Map<Id, String> matterIdTaskIdMapRemaining;
	Set<Id> idSet;
	String query;
	

	global IntakeToMatterWorkflowBatch( Map<Id, String> matterIdTaskIdMap ) {
		this.matterIdTaskIdMap = matterIdTaskIdMap;
		this.idSet = matterIdTaskIdMap.keySet();
		this.matterIdTaskIdMapRemaining = new Map<Id, String>();
	}

	global Database.QueryLocator start( Database.BatchableContext BC ) {
		query = 'SELECT Id FROM litify_pm__Matter__c WHERE Id IN :idSet';
		return Database.getQueryLocator( query );
	}
	
	global void execute( Database.BatchableContext BC, List<litify_pm__Matter__c> matterList ) {
		SpringCMService service = new SpringCMService();
		SpringCMImportTaskStatus importTaskStatus;
		for( Id matterId : matterIdTaskIdMap.keySet() ) {
			importTaskStatus = service.checkImportStatus( matterIdTaskIdMap.get( matterId ) );
			if( importTaskStatus.Status == PROCESSING_STATUS ) {
				matterIdTaskIdMapRemaining.put( matterId, matterIdTaskIdMap.get( matterId ) );
			} else if( importTaskStatus.Status == SUCCESS_STATUS ) {
				completedTaskIds.add( matterId );
			}
		}
	}

	global void finish( Database.BatchableContext BC ) {
		System.debug( 'in job finish' );
		if( !matterIdTaskIdMapRemaining.isEmpty() ) {
			IntakeToMatterWorkflowBatch workflowBatchInstance = new IntakeToMatterWorkflowBatch( matterIdTaskIdMap );
			System.scheduleBatch(workflowBatchInstance, 'IntakeToMatterWorkflowBatch ' + System.now().getTime(), 1 );
		}
		if( !completedTaskIds.isEmpty() ) {
			List<litify_pm__Matter__c> matterList = [SELECT Id, Intake__c FROM litify_pm__Matter__c WHERE Id IN :completedTaskIds];
			for( litify_pm__Matter__c matter : matterList ) {
				IntakeToMatterController.callIntakeToMatterWorkflow( matter.Intake__c, matter.Id );
			}
		}
	}
}