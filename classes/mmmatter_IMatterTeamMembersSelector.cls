public interface mmmatter_IMatterTeamMembersSelector
    extends mmlib_ISObjectSelector
{
    List<litify_pm__Matter_Team_Member__c> selectById(Set<Id> idSet);
    List<litify_pm__Matter_Team_Member__c> selectByMatter(Set<Id> idSet);
    List<litify_pm__Matter_Team_Member__c> selectByUser(Set<Id> idSet);
}