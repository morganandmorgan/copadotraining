public class mmgmap_VerifyAddressExtension
{
	public mmgmap_VerifyAddressExtension(ApexPages.StandardController ctrlr)
	{
		// code
	}

	public String getSourceApiString()
	{
		String accessKey = '';

		Google_Maps_Integration__c settings = Google_Maps_Integration__c.getInstance();
		if (settings != null) {
			accessKey = settings.Geocoding_Access_Key__c;
		}

		return 'https://maps.googleapis.com/maps/api/js?key=' + accessKey + '&libraries=places';
	}
}