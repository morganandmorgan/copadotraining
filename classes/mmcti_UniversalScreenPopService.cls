public class mmcti_UniversalScreenPopService 
{
    private static mmcti_IUniversalScreenPopService service()
    {
        return (mmcti_IUniversalScreenPopService) mm_Application.Service.newInstance( mmcti_IUniversalScreenPopService.class );
    }

    public static CTI_Screen_Pop_Redirect_Setting__mdt getScreenPopSetting()
    {
        return service().getScreenPopSetting();
    }   
}