/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBCreateIntake {
    global PBCreateIntake() {

    }
    @InvocableMethod(label='Create Intake' description='Create an Intake from a Referral')
    global static void createIntake(List<litify_pm.PBCreateIntake.ProcessBuilderCreateIntakeWrapper> createIntakeItems) {

    }
global class ProcessBuilderCreateIntakeWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderCreateIntakeWrapper() {

    }
}
}
