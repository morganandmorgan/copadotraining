/**
 *  mmctm_APIAuthorizations
 */
public class mmctm_APIAuthorizations
{
    private mmctm_APIAuthorizations() {  }

    private static list<mmctm_APIAccessNamedCredAuthorization> authroizationList = null;

    public static list<mmctm_APIAccessNamedCredAuthorization> getAuthroizationList()
    {
        if ( authroizationList == null )
        {
            authroizationList = new list<mmctm_APIAccessNamedCredAuthorization>();

            mmctm_APIAccessNamedCredAuthorization auth = null;

            for ( CallTrackingMetricsAccountMapping__mdt record : mmctm_CTMAccountMappingSelector.newInstance().selectAll() )
            {
                auth = new mmctm_APIAccessNamedCredAuthorization();
                auth.setIdentifier( record.CTM_Auth_Named_Credential__c );
                auth.setDomain( record.Domain_Value__c );

                authroizationList.add( auth );
            }
        }

        return authroizationList;
    }
}