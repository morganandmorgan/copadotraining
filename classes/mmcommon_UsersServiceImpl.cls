public with sharing class mmcommon_UsersServiceImpl implements mmcommon_IUsersService {
    public List<User> searchUsersUsingFuzzyMatching(Map<String, String> data) {
        cleanseAndValidateForFuzzyMatch(data);

        if (dataIsEmpty(data))
        {
            return new List<User>();
        }

        Set<String> tags = new Set<String>();
        if (!String.isEmpty(data.get('tags'))) {
            for (String s : data.get('tags').split(';')) {
                tags.add(s);
            }
        }

        mmcommon_IUsersSelectorEmployeeRequest req =
            mmcommon_UsersSelectorEmployeeRequest.newInstance()
            .setFirstName(data.get('firstName'))
            .setLastName(data.get('lastName'))
            .setDepartment(data.get('department'))
            .setTags(tags)
            .setPhoneNumber(data.get('phoneNumber'))
            .setExtension(data.get('extension'))
            .setLocation(data.get('location'));

        return mmcommon_UsersSelector.newInstance().selectByFuzzyMatching(req);
    }

    @testvisible
    private static void cleanseAndValidateForFuzzyMatch(Map<String, String> data)
    {
        if (data == null || data.isEmpty())
        {
            return;
        }

        for (String k : data.keySet())
        {
            String v = data.get(k);

            if (String.isBlank(v))
            {
                continue;
            }

            if ('firstName'.equalsIgnoreCase(k) || 'lastName'.equalsIgnoreCase(k))
            {
                data.put(k, mmlib_Utils.clean(v));
            }
            else if ('phoneNumber'.equalsIgnoreCase(k))
            {
                v = mmlib_Utils.clean_DigitsOnly(v);

                if (10 == v.length())
                {
                    data.put(k, v);
                }
                else
                {
                    data.put(k, '');
                }
            }
            else if ('tags'.equalsIgnoreCase(k))
            {
                v = mmlib_Utils.clean(v);
                if (!String.isEmpty(v)) {
                    List<String> tags = new List<String>();
                    for (String s : v.split(';')) {
                        if (String.isEmpty(s.trim())) continue;
                        tags.add(s.trim());
                    }
                    data.put(k, String.join(tags, ';'));
                } else {
                    data.put(k, v);
                }
            }
            else
            {
                data.put(k, mmlib_Utils.clean(v));
            }
        }
    }

    @testvisible
    private static Boolean dataIsEmpty(Map<String, String> data)
    {
        if (data == null && data.isEmpty())
        {
            return true;
        }

        for(String s : data.values())
        {
            if (String.isNotBlank(s))
            {
                return false;
            }
        }

        return true;
    }
}