@isTest
private class mmmarketing_zTemporaryUnitTest
{
    static testMethod void myUnitTest()
    {
        // create a Marketing Source record

        Marketing_Source__c sourceRec = new Marketing_Source__c();
        sourceRec.Description__c = 'blue utm';
        sourceRec.utm_source__c = 'blue';

        insert sourceRec;

        // create a Marketing Campaign record
        Marketing_Campaign__c campaignRec = new Marketing_Campaign__c();
        campaignRec.Source_Campaign_ID__c = '987654321';
        campaignRec.utm_campaign__c = 'numbers';
        campaignRec.Domain_Value__c = 'www.classaction.com';

        insert campaignRec;

        // create a Marketing Financial Period record
        Marketing_Financial_Period__c fp = new Marketing_Financial_Period__c();

        fp.Period_Start_Date__c = date.today();
        fp.Ad_Cost__c = 5;
        fp.Domain_Value__c = 'www.salesforce.com';
        fp.Campaign_Value__c = 'blue';
        fp.Source_Value__c = 'facebook';
        fp.Medium_Value__c = 'blue bird';
        fp.Keyword_Value__c = 'blue';
        fp.Campaign_AdWords_ID_Value__c = '987654321';
        fp.Clicks__c = 7;

        insert fp;

        // create a Marketing Tracking Info record
        Marketing_Tracking_Info__c ti = new Marketing_Tracking_Info__c();

        ti.Tracking_Event_Timestamp__c = datetime.now().addMinutes(-5);
        ti.Campaign_Value__c = 'blue';
        ti.Domain_Value__c = 'www.salesforce.com';
        ti.Source_Value__c = 'facebook';
        ti.Medium_Value__c = 'blue bird';
        ti.Term_Value__c = 'blue';
        ti.Raw_Landing_Page_URL__c = 'https://www.salesforce.com/';

        //insert ti;
    }
}