/**
 * Company_Selector_Test
 * @description Test for Company Selector
 * @author CLD Partners
 * @date 5/5/2019
 */
@isTest
private without sharing class Company_Selector_Test {

    static {
        Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.journals;
    }

    @isTest
    private static void ctor() {
        Company_Selector compSelector = new Company_Selector();
        System.assert(compSelector != null);
    }

    @isTest
    private static void selectByIds() {
        Company_Selector compSelector = new Company_Selector();
        Set<Id> aIds = new Set<Id>();
        for(Account tli : [SELECT Id FROM Account]){
            aIds.add(tli.Id); 
        }
        List<c2g__codaCompany__c> tliList = compSelector.selectByAccountId(aIds);
    }

    @isTest
    private static void test_getSObjectFieldList_0() {
        //public List<Schema.SObjectField> getSObjectFieldList() {
        Company_Selector companySelector = new Company_Selector();
        System.assert(companySelector != null);
        List<Schema.SObjectField> fields = null;
        fields=companySelector.getSObjectFieldList();
        System.assert(fields!=null);
    }
    
    @isTest
    private static void test_getSObjectType_0() {
        //public Schema.SObjectType getSObjectType() {
        Company_Selector companySelector = new Company_Selector();
        System.assert(companySelector != null);
        Schema.SObjectType result = null;
        result=companySelector.getSObjectType();
        System.assert(result!=null);
    }
    
    @isTest
    private static void test_selectByAccountId_0() {
        
        Company_Selector companySelector = new Company_Selector();
        System.assert(companySelector != null);
        
        Id companyId = null;
        companyId = TestDataFactory_FFA.getFakeRecordId(c2g__codaCompany__c.sObjectType);

        Set<Id> ids = null;
        ids=new Set<Id>();

        ids.add(companyId);

        List<c2g__codaCompany__c> results=null;
        results=companySelector.selectByAccountId(ids);        

    }

    @isTest
    private static void test_selectById_0() {
        
        Company_Selector companySelector = new Company_Selector();
        System.assert(companySelector != null);
        
        Id companyId = null;
        companyId = TestDataFactory_FFA.getFakeRecordId(c2g__codaCompany__c.sObjectType);

        Set<Id> ids = null;
        ids=new Set<Id>();

        ids.add(companyId);

        List<c2g__codaCompany__c> results=null;
        results=companySelector.selectById(ids);        

    }    
        


}