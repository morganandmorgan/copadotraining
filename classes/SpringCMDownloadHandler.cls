public class SpringCMDownloadHandler {

    public String base64Data { get; set; }
    public String contentType { get; set; }
    public String fileName { get; set; }
    
    // taken from https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
    private Map<String, String> extensionToMimeType = new Map<String, String>{
            'aac' => 'audio/aac',
            'abw' => 'application/x-abiword',
            'arc' => 'application/octet-stream',
            'avi' => 'video/x-msvideo',
            'azw' => 'application/vnd.amazon.ebook',
            'bin' => 'application/octet-stream',
            'bmp' => 'image/bmp',
            'bz' => 'application/x-bzip',
            'bz2' => 'application/x-bzip2',
            'csh' => 'application/x-csh',
            'css' => 'text/css',
            'csv' => 'text/csv',
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'eot' => 'application/vnd.ms-fontobject',
            'epub' => 'application/epub+zip',
            'gif' => 'image/gif',
            'htm' => 'text/html',
            'html' => 'text/html',
            'ico' => 'image/x-icon',
            'ics' => 'text/calendar',
            'jar' => 'application/java-archive',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'js' => 'text/javascript',
            'json' => 'application/json',
            'mid' => 'audio/midi audio/x-midi',
            'midi' => 'audio/midi audio/x-midi',
            'mjs' => 'text/javascript',
            'mp3' => 'audio/mpeg',
            'mpeg' => 'video/mpeg',
            'mpkg' => 'application/vnd.apple.installer+xml',
            'odp' => 'application/vnd.oasis.opendocument.presentation',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
            'odt' => 'application/vnd.oasis.opendocument.text',
            'oga' => 'audio/ogg',
            'ogv' => 'video/ogg',
            'ogx' => 'application/ogg',
            'otf' => 'font/otf',
            'png' => 'image/png',
            'pdf' => 'application/pdf',
            'ppt' => 'application/vnd.ms-powerpoint',
            'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'rar' => 'application/x-rar-compressed',
            'rtf' => 'application/rtf',
            'sh' => 'application/x-sh',
            'svg' => 'image/svg+xml',
            'swf' => 'application/x-shockwave-flash',
            'tar' => 'application/x-tar',
            'tif' => 'image/tiff',
            'tiff' => 'image/tiff',
            'ts' => 'application/typescript',
            'ttf' => 'font/ttf',
            'txt' => 'text/plain',
            'vsd' => 'application/vnd.visio',
            'wav' => 'audio/wav',
            'weba' => 'audio/webm',
            'webm' => 'video/webm',
            'webp' => 'image/webp',
            'woff' => 'font/woff',
            'woff2' => 'font/woff2',
            'xhtml' => 'application/xhtml+xml',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xml' => 'application/xml',
            'xul' => 'application/vnd.mozilla.xul+xml',
            'zip' => 'application/zip',
            '3gp' => 'video/3gpp ',
            '3g2' => 'video/3gpp2',
            '7z' => 'application/x-7z-compressed'
    };

    public void SpringCMDownloadHandler(string documentId) {
        try {
            Spring_CM_Matter_Forms__mdt config = [select APIDownlaod__c from Spring_CM_Matter_Forms__mdt limit 1];
            string apiDownloadUrl = config.APIDownlaod__c;
            MM_Document__c doc = [select Id, External_ID__c, Document_Name__c, File_Type__c from MM_Document__c where Id = :documentId];
            SpringCMService service = new SpringCMService(UserInfo.getSessionId());
            SpringCMDocument springDoc = new SpringCMDocument();
            springDoc.DownloadDocumentHref = apiDownloadUrl + doc.External_ID__c;

            contentType = getMimeType(doc.File_Type__c); // TODO: need to determine actual content type
            filename = doc.Document_Name__c;
            
            Blob springBlob = service.downloadDocument(springDoc, null, null, null);
            base64Data = EncodingUtil.base64Encode(springBlob);
        } catch (Exception e) {

        }
    }

    public SpringCMDownloadHandler() {
        string documentId = ApexPages.currentPage().getParameters().get('docId');
        SpringCMDownloadHandler(documentId);
    }

    private String getMimeType(String extension) {
        if (!String.isEmpty(extension)) {
            extension = extension.toLowerCase();
        }

        if (extensionToMimeType.containsKey(extension)) {
            return extensionToMimeType.get(extension);
        }

        return 'application/octet-stream';
    }
}