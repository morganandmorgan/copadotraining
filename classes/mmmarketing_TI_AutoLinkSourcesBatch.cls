/**
 *  mmmarketing_TI_AutoLinkSourcesBatch
 */
public class mmmarketing_TI_AutoLinkSourcesBatch
    implements Database.Batchable<SObject>
{
    public mmmarketing_TI_AutoLinkSourcesBatch () { }

    private Set<String> sourceValueSet;
    private integer batchSize = 200;

    public mmmarketing_TI_AutoLinkSourcesBatch(Set<String> sourceValueSet)
    {
        this.sourceValueSet = sourceValueSet;
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return mmmarketing_TrackingInfosSelector.newInstance().selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue( sourceValueSet );
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            mmmarketing_TrackingInformations.newInstance( (list<Marketing_Tracking_Info__c>)scope ).autoLinkToMarketingInformationIfRequiredInQueueableMode();
        }
        catch (Exception e)
        {
            System.debug('Error executing batch ' + e);
        }
    }

    public void finish(Database.BatchableContext context)
    {

    }

    public mmmarketing_TI_AutoLinkSourcesBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public void execute()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticTILinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticTILinkingEnabled__c )
        {
            Database.executeBatch( this, batchSize );
        }
    }
}