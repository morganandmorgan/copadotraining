/**
 *  mmlib_GenericBatch
 */
public class mmlib_GenericBatch
    implements Database.Batchable<SObject>, Database.AllowsCallouts
{
    public class GenericBatchException extends Exception { }

    public interface IGenericExecuteBatch
    {
        void run( List<SObject> scope);
    }

    private Type theGenericExecutorType = null;
    private String theQueryLocatorQuery = null;
    private Integer batchSize = 200;
    private Set<Id> idSet = null;
    private Set<Decimal> externalIdSet = null;
    private mmlib_GenericBatch genericBatchToRunAfterwards = null;

    private mmlib_GenericBatch() { }

    public mmlib_GenericBatch( Type theGenericExecutorType
                             , String theQueryLocatorQuery)
    {
        // verify that the Type if is implements IGenericExecuteBatch
        try
        {
            IGenericExecuteBatch genericExecutorRunner = (IGenericExecuteBatch)theGenericExecutorType.newInstance();
        }
        catch ( exception e )
        {
            throw new GenericBatchException( 'theGenericExecutorType parameter Type must implement the interface \'' + mmlib_GenericBatch.IGenericExecuteBatch.class.getName() + '\'');
        }

        this.theGenericExecutorType = theGenericExecutorType;

        if (String.isBlank( theQueryLocatorQuery ))
        {
            throw new GenericBatchException( 'theQueryLocatorQuery must represent a valid SOQL query.');
        }
        this.theQueryLocatorQuery = theQueryLocatorQuery;
    }

    public mmlib_GenericBatch( Type theGenericExecutorType
                             , String theQueryLocatorQuery
                             , Set<Id> idSet)
    {
        this(theGenericExecutorType, theQueryLocatorQuery);

        if ( idSet == null || idSet.isEmpty() )
        {
            throw new GenericBatchException( 'The idSet constructor parameter must not be null and it must contain at least one record.');
        }

        if ( ! theQueryLocatorQuery.containsIgnoreCase(':idSet') )
        {
            throw new GenericBatchException( 'theQueryLocatorQuery string must contain a bind variable reference to the Set<Id> idSet constructor parameter.');
        }

        this.idSet = idSet;
    }

    public mmlib_GenericBatch( Type theGenericExecutorType
            , String theQueryLocatorQuery
            , Set<Decimal> externalIdSet)
    {
        this(theGenericExecutorType, theQueryLocatorQuery);

        if ( externalIdSet == null || externalIdSet.isEmpty() )
        {
            throw new GenericBatchException( 'The idSet constructor parameter must not be null and it must contain at least one record.');
        }

        if ( ! theQueryLocatorQuery.containsIgnoreCase(':externalIdSet') )
        {
            throw new GenericBatchException( 'theQueryLocatorQuery string must contain a bind variable reference to the Set<String> externalIdSet constructor parameter.');
        }

        this.externalIdSet = externalIdSet;
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return Database.getQueryLocator( this.theQueryLocatorQuery );
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            system.debug('Invoking executor : ' + theGenericExecutorType.getName());

            IGenericExecuteBatch genericExecutorRunner = (IGenericExecuteBatch)theGenericExecutorType.newInstance();

            genericExecutorRunner.run( scope );
        }
        catch (Exception e)
        {
            System.debug('Error executing batch -- ' + e);
            System.debug( e.getStackTraceString() );
        }
    }

    public void finish(Database.BatchableContext context)
    {
        if ( this.genericBatchToRunAfterwards != null )
        {
            this.genericBatchToRunAfterwards.execute();
        }
    }

    public mmlib_GenericBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public mmlib_GenericBatch setGenericBatchToRunAfterwards( mmlib_GenericBatch genericBatchToRunAfterwards )
    {
        this.genericBatchToRunAfterwards = genericBatchToRunAfterwards;

        return this;
    }

    public void execute()
    {
        Database.executeBatch( this, batchSize );
    }

}