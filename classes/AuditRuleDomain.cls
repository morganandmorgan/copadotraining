/**
 * AuditRuleDomain
 * @description Domain class for Audit_Rule__c SObject.
 * @author Matt Terrill
 * @date 9/20/2019
 */
public without sharing class AuditRuleDomain extends fflib_SObjectDomain {

    private List<Audit_Rule__c> auditRules;
    //fields that can be changed event if there are flags from the rule
    private Set<String> unrestrictedFields = new Set<String>{'isactive__c','scheduled_flag_notification__c','scheduled_flag_notification_days__c','scheduled_flag_notification_message__c'};
        
    public AuditRuleDomain() {
        super();
        this.auditRules = new List<Audit_Rule__c>();
    } //constructor


    public AuditRuleDomain(List<Audit_Rule__c> auditRules) {
        super(auditRules);
        this.auditRules = auditRules;
    } //constructor()


    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new AuditRuleDomain(records);
        }
    } //IConstructable


    public Map<Id,Boolean> ruleHasFlags(List<Audit_Rule__c> rules) {

        Map<Id,Boolean> ruleFlags = new Map<Id,Boolean>();
        //lets default the return values
        for (Audit_Rule__c rule : rules) {
            ruleFLags.put(rule.id, false);
        }

        Map<Id,Audit_Rule__c> ruleMap = new Map<Id,Audit_Rule__c>(rules);

        //first look for real-time flags
        AuditFlagSelector flagSelector = new AuditFlagSelector();
        for (Audit_Flag__c flag : flagSelector.selectByRuleId(ruleMap.keySet(),10) ) {
            ruleFlags.put(flag.audit_rule__c, true);
        }

        //now do the same for scheduled flags
        AuditScheduledFlagSelector scheduledFlagSelector = new AuditScheduledFlagSelector();
        for (Audit_Scheduled_Flag__c flag : scheduledFlagSelector.selectByRuleId(ruleMap.keySet(),10) ) {
            ruleFlags.put(flag.audit_rule__c, true);
        }

        return ruleFlags;
    } //ruleHasFlags


    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {

        //detrimine which rules have flags
        Map<Id,Boolean> ruleFlags = ruleHasFlags(auditRules);

        for (Audit_Rule__c rule : auditRules) {
            //if this rule has a flag...
            if ( ruleFlags.get(rule.Id) ) {
                //check to see if they are changing any field other than isActive__c
                List<SelectOption> baseObjects = new List<SelectOption>();
                baseObjects.add( new SelectOption('',''));

                SObjectType objType = Schema.getGlobalDescribe().get('Audit_Rule__c');
                Map<String,Schema.SObjectField> ofields = objType.getDescribe().fields.getMap();

                for (String fieldName : ofields.keySet()) {
                    //we will just check custom fields, isActive__c is the only field they can change
                    if (fieldName.containsIgnoreCase('__c') && !unrestrictedFields.contains(fieldName.toLowerCase())) {
                        if (rule.get(fieldName) != existingRecords.get(rule.id).get(fieldName)) {
                            rule.addError('Cannot edit a rule once it has generated flags.');
                            break;
                        } //if field changed
                    } //if customfield, but isActive
                } //for fields
            }
        } //for auditRules

    } //onBeforeUpdate


    public void upsertRule(AuditEvalEngine.Rule rule) {
        rule.auditRule.rule_criteria__c = JSON.serialize(rule.expressions);
        upsert rule.auditRule;
    } //upsertRule

} //class