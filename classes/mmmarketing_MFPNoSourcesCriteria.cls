/**
 *  mmmarketing_MFPNoSourcesCriteria
 */
public with sharing class mmmarketing_MFPNoSourcesCriteria
    implements mmlib_ICriteria
{
    private list<Marketing_Financial_Period__c> records = new list<Marketing_Financial_Period__c>();

    public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
    {
        if (records != null
            && Marketing_Financial_Period__c.SObjectType == records.getSobjectType()
            )
        {
            this.records.addAll( (list<Marketing_Financial_Period__c>) records);
        }

        return this;
    }

    public list<SObject> run()
    {
        system.debug( 'mmmarketing_MFPNoSourcesCriteria run()');
        system.debug( 'mmmarketing_MFPNoSourcesCriteria this.records.size : ' +this.records.size());
        list<Marketing_Financial_Period__c> qualifiedRecords = new list<Marketing_Financial_Period__c>();

        Set<String> sourceValuesSet = new Set<String>();

        // loop through the records
        for (Marketing_Financial_Period__c record : this.records)
        {
            // collect all of the Source_Value__c values
            if (record.Source_Value__c != null)
            {
                sourceValuesSet.add( record.Source_Value__c );
            }
        }

        system.debug( 'mmmarketing_MFPNoSourcesCriteria sourceValuesSet : ' +sourceValuesSet);

        if ( ! sourceValuesSet.isEmpty() )
        {
            // With that set of sources values, query the sources table
            List<Marketing_Source__c> currentlyAvailableSourceRecordList = mmmarketing_SourcesSelector.newInstance().selectByUtm( sourceValuesSet );
            system.debug( 'mmmarketing_MFPNoSourcesCriteria currentlyAvailableSourceRecordList.size : ' +currentlyAvailableSourceRecordList.size());

            // Sort source records in to a set
            Set<String> currentlyAvailableSourceNameList = new Set<String>();

            for (Marketing_Source__c currentlyAvailableSource : currentlyAvailableSourceRecordList)
            {
                if ( String.isNotBlank( currentlyAvailableSource.utm_source__c ) )
                {
                    currentlyAvailableSourceNameList.add( currentlyAvailableSource.utm_source__c );
                }
            }

            // loop through the records again.
            for (Marketing_Financial_Period__c record : this.records)
            {
                //      If the record does not have a matching source, then that is a qualifiedRecords
                if ( ! currentlyAvailableSourceNameList.contains(record.Source_Value__c) )
                {
                    qualifiedRecords.add( record );
                }
            }
        }
        system.debug( 'mmmarketing_MFPNoSourcesCriteria qualifiedRecords.size : ' +qualifiedRecords.size());
        return qualifiedRecords;
    }
}