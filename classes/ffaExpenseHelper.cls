global class ffaExpenseHelper 
{
    // Gets list of person accounts associated with a matter to assciate it with an
    // expense. 
	public static List<Account> getPayableToList(String matterId)
    {
        List<litify_pm__Role__c> roles = [select litify_pm__Party__c, litify_pm__Party__r.Name, litify_pm__Role__c
            FROM litify_pm__Role__c
            WHERE litify_pm__Matter__c = :matterId];
        List<Account> accountList = new List<Account>();
        FOR (litify_pm__Role__c role : roles)
        {
            Account acc = new Account();
            acc.Name = role.litify_pm__Party__r.Name;
            acc.Id = role.litify_pm__Party__c;

            acc.Last_Intake_Status__c = role.litify_pm__Role__c;
            accountList.add(acc);
        }
        /*
        List<Account> accounts = [Select Id, Name FROM Account WHERE RecordType.Name = 'Employee'];
        FOR (Account account : accounts)
        {
            account.Last_Intake_Status__c = 'Employee';
            accountList.add(account);
        }
        */
        return accountList;

    }
}