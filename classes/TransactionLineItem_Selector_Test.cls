/**
 * TransactionLineItem_Selector_Test
 * @description Test for Transaction Line Selector
 * @author CLD Partners
 * @date 2/20/2019
 */
@IsTest
public with sharing class TransactionLineItem_Selector_Test {

    static {
        Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.journals;
    }

    @IsTest
    private static void ctor() {
        TransactionLineItem_Selector tliSelector = new TransactionLineItem_Selector();
        System.assert(tliSelector != null);
    }

    @IsTest
    private static void selectByIds() {
        TransactionLineItem_Selector tliSelector = new TransactionLineItem_Selector();
        Set<Id> tliIds = new Set<Id>();
        for(c2g__codaTransactionLineItem__c tli : [SELECT Id FROM c2g__codaTransactionLineItem__c]){
            tliIds.add(tli.Id); 
        }
        List<c2g__codaTransactionLineItem__c> tliList = tliSelector.selectById_TrustTransaction(tliIds);
    }
}