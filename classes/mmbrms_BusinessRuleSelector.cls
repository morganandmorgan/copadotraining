public with sharing class mmbrms_BusinessRuleSelector
    extends mmlib_SObjectSelector
    implements mmbrms_IBusinessRuleSelector
{
    public static mmbrms_IBusinessRuleSelector newInstance()
    {
        return (mmbrms_IBusinessRuleSelector) mm_Application.Selector.newInstance(BusinessRule__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return BusinessRule__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            BusinessRule__mdt.Description__c,
            BusinessRule__mdt.RuleDefinition__c
        };
    }

    public List<BusinessRule__mdt> selectByMasterLabels(Set<String> labelSet)
    {
        if (labelSet == null || labelSet.isEmpty())
        {
            return new List<BusinessRule__mdt>();
        }

        return
            Database.query
            (
                newQueryFactory()
                    .setCondition
                    (
                        BusinessRule__mdt.MasterLabel + ' in :labelSet'
                    )
                    .toSOQL()
            );
    }

    public List<BusinessRule__mdt> selectByDeveloperNames(Set<String> developerNamesSet)
    {
        List<BusinessRule__mdt> records = new list<BusinessRule__mdt>();

        if ( developerNamesSet != null && ! developerNamesSet.isEmpty() )
        {
            records.addAll( (List<BusinessRule__mdt>) Database.query( newQueryFactory().setCondition( BusinessRule__mdt.DeveloperName + ' in :developerNamesSet').toSOQL() ));
        }

        return records;
    }

    public List<BusinessRule__mdt> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }
}