public without sharing class IntakeConvertFlowQueueable implements Queueable, Database.AllowsCallouts {

/* This class allows the intake trigger to thread the Flow that does the intake conversion process 
    We are "daisy-chaining" the queueables for each intake to avoid record locking if they run asynchronously
*/

    private Set<Id> intakeIds;
    private Map<Id,Error_Log__c> errors; //null for the first try

    public IntakeConvertFlowQueueable(Set<Id> intakeIds, Map<Id,Error_Log__c> errors) {
        this.intakeIds = intakeIds;
        this.errors = errors;
    } //constructor


    public void execute(QueueableContext context) {
        if (intakeIds.size() == 0) return;

        //grab the first intake and error
        Id intakeId = (new List<Id>(intakeIds))[0];
        Error_Log__c error;
        if (errors != null) {
            error = errors.get(intakeId);
        }

        IntakeDomain it = new IntakeDomain();
        //process the one
        it.tryConversionFlow(intakeId, error);

        //remove the one we processed and daisy-chain if there are any more
        intakeIds.remove(intakeId);
        if (errors != null) {
            errors.remove(intakeId);
        }            
        if (intakeIds.size() != 0) {
            Id jobId = System.enqueueJob(new IntakeConvertFlowQueueable(intakeIds, errors));
        }

    } //execute

} //class