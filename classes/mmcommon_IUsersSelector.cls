public interface mmcommon_IUsersSelector extends mmlib_ISObjectSelector
{
    List<User> selectById(Set<Id> idSet);
    List<User> selectAllActiveInvestigators();
    List<User> selectByIdWithProfileRolePermissionSets( Set<Id> idSet );
    List<User> selectByFuzzyMatching(mmcommon_IUsersSelectorEmployeeRequest request);
    List<User> selectByUsernameLikeFields(Set<String> userNames);
}