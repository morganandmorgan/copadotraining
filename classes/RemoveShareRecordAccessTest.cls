/**
 * RemoveShareRecordAccessTest
 * @description RemoveShareRecordAccess test class for to remove shared record when matter team member deleted or changed.
 * @author 10k
 * @date 6/18/2019
 */
@IsTest
public with sharing class RemoveShareRecordAccessTest{

    private static User user;
    private static Account account;
    private static litify_pm__Matter__c matter;
    private static litify_pm__Matter__c matterforUpdate;
    private static litify_pm__Matter_Team_Role__c matterTeamRole;
    private static litify_pm__Matter_Team_Member__c matterTeamMember;

    static {

        user = TestUtil.createUser();
        insert user;

        account = TestUtil.createPersonAccount('Jimmy', 'Buffett');
        insert account;

        matter = TestUtil.createMatter(account);
        insert matter;
        
        matterforUpdate = TestUtil.createMatter(account);
        insert matterforUpdate;


        matterTeamRole = TestUtil.createMatterTeamMemberRole();
        insert matterTeamRole;
    }

    @IsTest
    private static void RemoveShareRecordAccessTest() {
        
        Map<Id, Id> removePermissionMap = new Map<Id, Id>();
        litify_pm__Matter__Share matteObject = new litify_pm__Matter__Share();
        matteObject.ParentId= matter.id;
        matteObject.UserOrGroupId= user.id;
        matteObject.AccessLevel = 'Edit';
        insert matteObject;
        System.assertEquals(matteObject.AccessLevel, 'Edit');
        removePermissionMap.put(matter.id, user.id);
        Database.executeBatch(new RemoveShareRecordAccess(removePermissionMap));
        
    }  
}