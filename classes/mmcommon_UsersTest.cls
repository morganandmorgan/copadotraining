@isTest
private class mmcommon_UsersTest {

    private static final Date TEST_HOLIDAY_DATE = Date.today().addDays(10);

    private static list<Holiday> originalFutureHolidayList = new list<Holiday>();

    @TestSetup
    private static void setup()
    {
        originalFutureHolidayList.addAll(mmcommon_HolidaysSelector.newInstance().selectAllFuture());

        Holiday newHoliday = TestUtil.createHoliday('Test Holiday', TEST_HOLIDAY_DATE);

        Database.insert(newHoliday);
    }

    private static void assertHolidaysForUsers(List<User> users, List<Date> orderedDates)
    {
        // //This test assertion needs to be completed so as to account for any existing holiday dates in the org

        // // get all of the events for the test users
        // List<Event> userEvents = mmcommon_EventsSelector.selectByUserOrderByDate( (new Map<Id, User>( users )).keyset()) ;

        // // organize the userEvents into a map buy user id
        // Map<id, Set<date>> eventsByUserIdMap = new Map<id, Set<date>>();

        // for (Event userEvent : userEvents)
        // {
        //     if ( ! eventsByUserIdMap.containsKey(userEvent.OwnerId ))
        //     {
        //         eventsByUserIdMap.put(userEvent.OwnerId, new Set<Date>() );
        //     }
        //     eventsByUserIdMap.get(userEvent.OwnerId).add(userEvent.ActivityDate);
        // }

        // for (User u : users)
        // {
        //     //   List<Event> userEvents = mmcommon_EventsSelector.selectByUserOrderByDate(new Set<Id>{ u.Id });
        //     integer referenceDateSize = orderedDates.size() + originalFutureHolidayList.size();

        //     System.assertEquals( referenceDateSize, userEvents.size());

        //     for (Integer i = 0; i < orderedDates.size(); ++i)
        //     {
        //         Date d = orderedDates.get(i);
        //         Event userEvent = userEvents.get(i);

        //         System.assertEquals(d, userEvent.ActivityDate);
        //         System.assertEquals(mmcommon_Events.EVENT_SUBJECT_HOLIDAY, userEvent.Subject);
        //     }
        // }
    }

    @isTest
    private static void onAfterInsert_CreateHolidays()
    {
        User user1 = TestUtil.createActiveInvestigator();
        User user2 = TestUtil.createActiveInvestigator();
        List<User> users = new List<User>{
            user1,
            user2
        };

        Test.startTest();

        System.runAs(new User(Id = UserInfo.getUserId()))
        {
            Database.insert(users);
        }

        Test.stopTest();

        assertHolidaysForUsers(users, new List<Date>{ TEST_HOLIDAY_DATE });
    }

    @isTest
    private static void onAfterUpdate_CreateHolidays()
    {
        User user1 = TestUtil.createInactiveInvestigator();
        User user2 = TestUtil.createInactiveInvestigator();
        List<User> users = new List<User>{
            user1,
            user2
        };
        Database.insert(users);

        Map<Id, User> userMap = new Map<Id, User>(users);
        System.assertEquals(0, mmcommon_EventsSelector.newInstance().selectByUserOrderByDate(userMap.keySet()).size());

        Test.startTest();
        for (User u : users) {
            u.IsActive = true;
        }
        System.runAs(new User(Id = UserInfo.getUserId())) {
            Database.update(users);
        }
        Test.stopTest();

        assertHolidaysForUsers(users, new List<Date>{ TEST_HOLIDAY_DATE });
    }

    @isTest
    private static void createUserHolidaysIfNeeded()
    {
        User user1 = TestUtil.createInactiveInvestigator();
        User user2 = TestUtil.createInactiveInvestigator();
        List<User> users = new List<User>{
            user1,
            user2
        };
        Database.insert(users);

        Map<Id, User> userMap = new Map<Id, User>(users);
        System.assertEquals(0, mmcommon_EventsSelector.newInstance().selectByUserOrderByDate(userMap.keySet()).size());

        Test.startTest();

        for (User u : users) {
            u.IsActive = true;
        }

        System.runAs(new User(Id = UserInfo.getUserId()))
        {
            mmcommon_Users.newInstance(users).createUserHolidaysIfNeeded();
        }
        Test.stopTest();

        assertHolidaysForUsers(users, new List<Date>{ TEST_HOLIDAY_DATE });
    }

    @isTest
    private static void publishEventTest()
    {
        User usr = new User();
        usr.Id = fflib_IDGenerator.generate(User.SObjectType);

        mmcommon_Users.test_publishEvents = false;
        new mmcommon_Users(new List<User> {usr}).publishEventForNewChangedOrDeletedUsers();

        System.debug(mmcommon_Users.test_payload);

        List<Id> userIdList = (List<Id>) JSON.deserialize(mmcommon_Users.test_payload, List<Id>.class);

        System.assertNotEquals(null, userIdList);
        System.assertEquals(1, userIdList.size());
        System.assertEquals(usr.Id, userIdList.get(0));
    }
}