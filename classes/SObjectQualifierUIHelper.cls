/**
 *  This class has been deprecated as of October 2017 and replaced by the mmlib_UIHelper class
 *
 *  In time, this class will be purged.
 */
public with sharing class SObjectQualifierUIHelper
{
    public static List<SelectOption> getOperatorOptions()
    {
        return mmlib_UIHelper.getOperatorOptions();
    }

    public static List<SelectOption> getFormulaOptions()
    {
        return mmlib_UIHelper.getFormulaOptions();
    }

    public static List<SelectOption> getFieldList(Schema.sObjectType objectType)
    {
        return mmlib_UIHelper.getFieldList( objectType );
    }
}