public class mmwiz_QuestionModel
    extends mmwiz_AbstractBaseModel
{
    private static final String ISREQUIRED_PROPERTY_NAME = 'isRequired';
    private static final String QUESTIONTYPE_PROPERTY_NAME = 'questionType';
    private Boolean isRequired = false;
    private String questionType;

    public mmwiz_QuestionModel()
    {
        setType(mmwiz_QuestionModel.class);
    }

    public override void initialize( Wizard__mdt wizard )
    {
        super.initialize( wizard );

        if ( this.serializedDataMap.containsKey( ISREQUIRED_PROPERTY_NAME )) {
            setIsRequired ((Boolean)serializedDataMap.get(ISREQUIRED_PROPERTY_NAME));
        }
        if ( this.serializedDataMap.containsKey( QUESTIONTYPE_PROPERTY_NAME )) {
            setQuestionType ((String)serializedDataMap.get(QUESTIONTYPE_PROPERTY_NAME));
        }
    }


    public String getQuestionType()
    {
        return questionType;
    }

    public String setQuestionType(String value)
    {
        questionType = value;
        return questionType;
    }

    public Boolean getIsRequired()
    {
        return isRequired;
    }

    public Boolean setIsRequired(Boolean value)
    {
        isRequired = value;
        return isRequired;
    }

    public override void prepareForSerialization()
    {
        super.prepareForSerialization();
        serializedDataMap.put(ISREQUIRED_PROPERTY_NAME, getIsRequired());
        serializedDataMap.put(QUESTIONTYPE_PROPERTY_NAME, getQuestionType());
    }
}