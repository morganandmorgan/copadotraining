public class mmlitifylrn_ReferralsService
{
    private static mmlitifylrn_IReferralsService service()
    {
        return (mmlitifylrn_IReferralsService) mm_Application.Service.newInstance( mmlitifylrn_IReferralsService.class );
    }

    public static void referToLitify(Set<id> referralIdSet)
    {
        service().referToLitify(referralIdSet);
    }

    public static void syncFromLitify()
    {
        service().syncFromLitify();
    }

    public static void syncFromLitify(Datetime lastSync)
    {
        service().syncFromLitify(lastSync);
    }
}