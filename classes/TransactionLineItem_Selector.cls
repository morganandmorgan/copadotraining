/*============================================================================/
* TransactionLineItem_Selector
* @description Selector for FFA Transaction Line Itesm.
* @author Brian Krynitsky
* @date 2/20/2019
=============================================================================*/

public class TransactionLineItem_Selector extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                c2g__codaTransactionLineItem__c.Id,
                c2g__codaTransactionLineItem__c.Matter__c,
                c2g__codaTransactionLineItem__c.c2g__LineType__c,
                c2g__codaTransactionLineItem__c.c2g__BankAccount__c,
                c2g__codaTransactionLineItem__c.c2g__HomeValue__c,
                c2g__codaTransactionLineItem__c.c2g__LineReference__c,
                c2g__codaTransactionLineItem__c.Deposit__c
        };
    }

    // public List<Schema.SObjectField> searchFields { get; set; }

    // Constructor
    public TransactionLineItem_Selector() {
    }

    //required implemenation from fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return c2g__codaTransactionLineItem__c.SObjectType;
    }

    // Methods
    public List<c2g__codaTransactionLineItem__c> selectById_TrustTransaction(Set<Id> ids) {
        fflib_QueryFactory query = newQueryFactory();
        
        //additional fields:
        query.selectField('Matter__r.Name');
        query.selectField('Matter__r.AssignedToMMBusiness__r.Name');
        query.selectField('c2g__BankAccount__r.Bank_Account_Type__c');
        query.selectField('c2g__Transaction__r.c2g__TransactionDate__c');
        query.selectField('c2g__Account__r.Name');
        query.selectField('Deposit__r.Deposit_Description__c');
        query.selectField('Deposit__r.Check_Number__c');
        
        //where conditions:
        query.setCondition('Id IN :ids AND c2g__BankAccount__c != null AND c2g__BankAccount__r.Bank_Account_Type__c = \'Trust\' AND Matter__c != null');

        return (List<c2g__codaTransactionLineItem__c>) Database.query(query.toSOQL());
    }
}