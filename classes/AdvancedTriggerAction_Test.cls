/*
	Test class for AdvancedTriggerAction
*/

@isTest
private class AdvancedTriggerAction_Test {

	// Map of records to be / have been processed;
	// using account because it is 'less customized' than others
	private static List<Account> accList;
	// Map of which records have been processed (for asserts)
	private static Map<Id, Boolean> markedMap;

	// Test of run history tracking
	static testMethod void historyTest(){
		// make testing data
		setupData();
		// setup some faux data/variables for use later
		List<SObject> fauxList = new List<SObject>();
		Map<Id,SObject> fauxMap = new Map<Id,SObject>();

		// first test trigger action
		AdvFauxTrigAct trig = new AdvFauxTrigAct(fauxMap,TriggerAction.TriggerContext.AFTER_INSERT);

		// mark the first and validate
		trig.markTouched(accList[0]); // mark using sobject method
		markedMap.put(accList[0].id,true); // put into marked list
		System.Assert(trig.hasTouched(accList[0].id)); // validate using id method
		checkMarkings(trig); // validate all using helper

		// mark the second and validate
		trig.markTouched(accList[1].id); // mark using id method
		markedMap.put(accList[1].id,true); // put into marked list
		System.Assert(trig.hasTouched(accList[1])); // validate using a sobject method
		checkMarkings(trig); // validate all using helper

		// mark the third and validate
		trig.markTouched(new Map<Id,SObject>(new List<SObject>{accList[2]})); // mark using map<id,sobject> method
		markedMap.put(accList[2].id,true); // put into marked list
		checkMarkings(trig); // validate all using helper

		// mark the fourth & fifth and validate
		trig.markTouched(new List<SObject>{accList[3],accList[4]}); // mark using list<sobject> method
		markedMap.put(accList[3].id,true); // put into marked list
		markedMap.put(accList[4].id,true); // put into marked list
		// validate using a list<sobject> method
		for (Boolean b : trig.hasTouched(new List<SObject>{accList[3],accList[4]}).values()){
			System.Assert(b);
		}
		checkMarkings(trig); // validate all using helper

		// second test trigger action, create a trigger in this context
		trig = new AdvFauxTrigAct(fauxMap,TriggerAction.TriggerContext.AFTER_INSERT);

		// mark the sixth & seventh and validate
		trig.markTouched(new Set<Id>{accList[5].Id,accList[6].Id}); // mark using set<id> method
		markedMap.put(accList[5].id,true); // put into marked list
		markedMap.put(accList[6].id,true); // put into marked list
		// validate using a set<Id> method
		for (Boolean b : trig.hasTouched(new Set<Id>{accList[5].id,accList[6].id}).values()){
			System.Assert(b);
		}
		checkMarkings(trig); // validate all using helper
	}

	// Helper to validate markings (with asserts)
	private static void checkMarkings(AdvancedTriggerAction trig){
		// iterate account list
		for(Account acc : accList){
			// assert that trigger and markedMap agree
			System.assertEquals(trig.hasTouched(acc.Id), markedMap.get(acc.Id));
		}
	}

	// Test context combinations where the contructors should/shouldn't fail
	static testMethod void constructTest(){
		// setup some faux data/variables for use later
		List<SObject> fauxList = new List<SObject>();
		Map<Id,SObject> fauxMap = new Map<Id,SObject>();
		// test trigger action variable declaration
		AdvFauxTrigAct trig;

		// first case - default constructor positive ![not possible using DML-less testing]!
			//NOOP

		// second case - default constructor negative
		try {
			trig = null;
			trig = new AdvFauxTrigAct();
			System.assertEquals(trig, null);
		} catch (TriggerAction.InvalidContextException e){}

		// third case - map constructor positive
		trig = null;
		trig = new AdvFauxTrigAct(fauxMap,TriggerAction.TriggerContext.AFTER_INSERT);
		System.assertNotEquals(trig, null);

		// fourth case - map constructor negative
		try {
			trig = null;
			trig = new AdvFauxTrigAct(fauxMap,TriggerAction.TriggerContext.BEFORE_INSERT);
			System.assertEquals(trig, null);
		} catch (TriggerAction.InvalidContextException e){}

		// fifth case - twoMap constructor positive
		trig = null;
		trig = new AdvFauxTrigAct(fauxMap, fauxMap,TriggerAction.TriggerContext.BEFORE_UPDATE);
		System.assertNotEquals(trig, null);

		// sixth case - twoMap constructor negative
		try {
			trig = null;
			trig = new AdvFauxTrigAct(fauxMap, fauxMap,TriggerAction.TriggerContext.BEFORE_INSERT);
			System.assertEquals(trig, null);
		} catch (TriggerAction.InvalidContextException e){}
	}

	// Helper method to setup test data
	private static void setupData(){
		// temporary list
		accList = new List<Account>();
		// create seven auto-rando-magical accounts
		for (Integer i=0; i<7; i++){
			// use TestUtil to get base account data
			accList.add(TestUtil.createAccount('Test Account '+i));
		}
		// insert them to get SObject Ids
		insert accList;
		// iterate inserted accounts and set as unmarked
		markedMap = new Map<Id,Boolean>();
		for (Account acc : accList){
			markedMap.put(acc.Id,false);
		}
	}

	// Helper class to implement superclass for testing
	private class AdvFauxTrigAct extends AdvancedTriggerAction {
		// add constructors because reasons
		public AdvFauxTrigAct(){
			super();
		}
		public AdvFauxTrigAct(Map<Id,SObject> triggerMap, TriggerContext ctx){
			super(triggerMap, ctx);
		}
		public AdvFauxTrigAct(Map<Id,SObject> triggerNewMap,Map<Id,SObject> triggerOldMap, TriggerContext ctx){
			super(triggerNewMap, triggerOldMap, ctx);
		}
		// very simple override of shouldRun
		public override Boolean shouldRun(){
			// very simplistic criteria
			return this.isUpdate() || this.isDelete();
		}
		// very simple doAction
		public override void doAction(){
			this.markRun();
		}
	}
}