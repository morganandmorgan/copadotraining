public class mmsysadmin_UADNameLabel
    implements Comparable
{
    public Id id { get; private set; }
    public String apiName { get; private set; }
    public String label { get; private set; }

    public mmsysadmin_UADNameLabel(String apiName, String label)
    {
        this(null, apiName, label);
    }

    public mmsysadmin_UADNameLabel(Id id, String apiName, String label)
    {
        this.id = id;
        this.apiName = apiName;
        this.label = label;
    }

    public Integer compareTo(Object obj)
    {
        mmsysadmin_UADNameLabel other = (mmsysadmin_UADNameLabel) obj;
        return label.compareTo(other.label);
    }
}