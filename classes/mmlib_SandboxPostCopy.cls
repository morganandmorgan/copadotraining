/**
 *  This class handles all of the operations that occur after a sandbox is refreshed
 *  Typically, this is executed in the refresh of the UAT or a training related
 *  sandbox
 */
global class mmlib_SandboxPostCopy
    implements SandboxPostCopy
{
    global void runApexClass(SandboxContext context)
    {
        execute();
    }

    public void execute()
    {
        removeAllScheduleJobsAndReports();

        clearLitifyLRNInfo();
    }

    private void removeAllScheduleJobsAndReports()
    {
        new mmlib_GenericBatch( mmlib_CronTriggerAbortExecutor.class, 'select Id from CronTrigger' ).setBatchSizeTo(150).execute();
    }

    private void clearLitifyLRNInfo()
    {
        try
        {
            database.delete( database.query('select id from litify_pm__Public_Setup__c '), false );
            database.delete( database.query('select id from mmlitifylrn_ThirdPartyCredential__c '), false );
            database.delete( database.query('select id from mmlitifylrn_ThirdPartyConfig__c '), false );
        }
        catch ( Exception e )
        {
            system.debug( e.getMessage() );
        }

        try
        {
            database.delete( database.query('select id from ReferralRule__c'));

            // clear out the Litify_pm__Case_Type__c after the Litify_pm__Firm__c
            mmlib_GenericBatch caseTypeDeleteGenericBatch = new mmlib_GenericBatch( mmlib_SObjectDeleteExecutor.class, 'SELECT id FROM Litify_pm__Case_Type__c' );

            // clear out the Litify_pm__Firm__c after the litify_pm__Firm_Case_Type__c
            mmlib_GenericBatch firmDeleteGenericBatch = new mmlib_GenericBatch( mmlib_SObjectDeleteExecutor.class, 'SELECT id FROM Litify_pm__Firm__c' )
                                                                                    .setBatchSizeTo(5000)
                                                                                    .setGenericBatchToRunAfterwards( caseTypeDeleteGenericBatch );

            // clear out the litify_pm__Firm_Case_Type__c after the Litify_pm__Referral__c
            mmlib_GenericBatch firmCaseTypeDeleteGenericBatch = new mmlib_GenericBatch( mmlib_SObjectDeleteExecutor.class, 'SELECT id FROM litify_pm__Firm_Case_Type__c' )
                                                                                    .setBatchSizeTo(5000)
                                                                                    .setGenericBatchToRunAfterwards( firmDeleteGenericBatch );

            // clear out the Litify_pm__Referral__c after the Litify_pm__Referral_Transaction__c
            mmlib_GenericBatch referralDeleteGenericBatch = new mmlib_GenericBatch( mmlib_SObjectDeleteExecutor.class, 'SELECT id FROM Litify_pm__Referral__c' )
                                                                                    .setBatchSizeTo(5000)
                                                                                    .setGenericBatchToRunAfterwards( firmCaseTypeDeleteGenericBatch );

            // clear out the Litify_pm__Referral_Transaction__c
            new mmlib_GenericBatch( mmlib_SObjectDeleteExecutor.class, 'SELECT id FROM Litify_pm__Referral_Transaction__c' )
                                        .setBatchSizeTo(5000)
                                        .setGenericBatchToRunAfterwards( referralDeleteGenericBatch )
                                        .execute();
        }
        catch ( Exception e )
        {
            system.debug( e.getMessage() );
        }

    }
}