/**
 *  mmmarketing_TrackingInfosService
 */
public class mmmarketing_TrackingInfosService
{
    private static mmmarketing_ITrackingInfosService service()
    {
        return (mmmarketing_ITrackingInfosService) mm_Application.Service.newInstance( mmmarketing_ITrackingInfosService.class );
    }

    public static void recordCallsForIntakes( list<mmmarketing_ITrackInfoRecordCallRequest> requestList )
    {
        service().recordCallsForIntakes( requestList );
    }

    public static void updateCallRelatedInformation( set<id> mtiCallRecordIdSet )
    {
        service().updateCallRelatedInformation( mtiCallRecordIdSet );
    }

    public static void updateCallRelatedInformation_ShallowReconciliation( set<id> mtiCallRecordIdSet )
    {
        service().updateCallRelatedInformation_ShallowReconciliation( mtiCallRecordIdSet );
    }
}