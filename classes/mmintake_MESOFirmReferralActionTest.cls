@isTest
private class mmintake_MESOFirmReferralActionTest
{
    @isTest
    static void normal_increment()
    {
        normal_implementation(1, 2);
    }

    @isTest
    static void normal_wraparound()
    {
        normal_implementation(3, 0);
    }

    static void normal_implementation(Integer startIndex, Integer resultIndex)
    {
        List<String> mock_picklistEntryList = new List<String>();
        mock_picklistEntryList.add('Zero');
        mock_picklistEntryList.add('One');
        mock_picklistEntryList.add('Two');
        mock_picklistEntryList.add('Three');

        Intake__c intake = new Intake__c();
        intake.MESO_Firm_Referred__c = mock_picklistEntryList.get(startIndex);

        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmintake_IIntakesSelector mockIntakesSelector = (mmintake_IIntakesSelector) mocks.mock(mmintake_IIntakesSelector.class);

        mocks.startStubbing();
        mocks.when(mockIntakesSelector.sObjectType()).thenReturn(Intake__c.SObjectType);
        mocks.when(mockIntakesSelector.selectMostRecentMesoReferral()).thenReturn(new List<Intake__c> {intake});
        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockIntakesSelector);

        mmintake_MESOFirmReferralAction.mock_picklistEntryList = mock_picklistEntryList;
        new mmintake_MESOFirmReferralAction().setRecordsToActOn(new List<SObject> {intake}).run();

        System.assertEquals(mock_picklistEntryList.get(resultIndex), intake.MESO_Firm_Referred__c);
    }
}