/**
 *  mmcommon_IAccountsSelector
 */
public interface mmcommon_IAccountsSelector extends mmlib_ISObjectSelector
{
    list<Account> selectById( Set<Id> idSet );
    list<Account> selectWithFieldsetById(Schema.FieldSet fs, Set<Id> idSet );
    List<Account> selectByOfficeLocationCode(Set<String> codeSet);
    list<Account> selectByPersonFieldRequest( mmcommon_IAccountsSelectorPersonRequest request );
    List<Account> selectByFuzzyMatching(mmcommon_IAccountsSelectorPersonRequest request);
    list<Account> selectTreatmenCentersCloseToLocation(Location originatingLocation, integer distanceInMiles, String treatmentCenterType);
}