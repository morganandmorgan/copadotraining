/**
 * FeeGoal_Service_Test
 * @description Services for Fee Goal transactions and operations.
 * @author Jeff Watson
 * @date 2/13/2019
 */
@IsTest
public with sharing class FeeGoal_Service_Test {

    private static Fee_Goal__c feeGoal;
    private static Settlement__c settlement;
    private static Fee_Goal_Allocation__c feeGoalAllocation;

    static {
        feeGoal = TestUtil.createFeeGoal();
        insert feeGoal;

        settlement = TestUtil.createSettlement();
        insert settlement;

        feeGoalAllocation = TestUtil.createFeeGoalAllocation(feeGoal, settlement);
        insert feeGoalAllocation;
    }

    @IsTest
    private static void ctor() {
        FeeGoal_Service feeGoalService = new FeeGoal_Service();
        System.assert(feeGoalService != null);

        FeeGoal_Service feeGoalService2 = new FeeGoal_Service(new FeeGoal_Selector(), new FeeGoalAllocation_Selector());
        System.assert(feeGoalService2 != null);
    }
}