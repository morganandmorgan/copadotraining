/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TaskController {
    global TaskController() {

    }
    @AuraEnabled
    global static String deleteItems(List<SObject> items) {
        return null;
    }
    @AuraEnabled
    global static Map<String,List<SObject>> fetchRecords(String type, List<String> ids) {
        return null;
    }
    @AuraEnabled
    global static List<Map<String,List<String>>> getFieldValues(List<String> fields) {
        return null;
    }
    @AuraEnabled
    global static String updateItems(List<Task> items) {
        return null;
    }
}
