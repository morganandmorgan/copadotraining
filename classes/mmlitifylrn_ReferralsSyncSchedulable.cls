/**
 *  mmlitifylrn_ReferralsSyncSchedulable
 */
public class mmlitifylrn_ReferralsSyncSchedulable
    implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        Type targettype = Type.forName('mmlitifylrn_ReferralsSyncSchedule');

        if ( targettype != null )
        {
            mmlib_ISchedule obj = (mmlib_ISchedule)targettype.newInstance();
            obj.execute(sc);
        }
        else
        {
            system.debug( 'mmlib_ISchedule implementation of mmlitifylrn_ReferralsSyncSchedule not found');
        }
    }

    public static String setupSchedule( String scheduleJobName, String crontabSchedule )
    {
        if ( string.isblank(crontabSchedule) )
        {
            crontabSchedule = '0 15 * * * ?';
        }

        if ( string.isblank(scheduleJobName) )
        {
            scheduleJobName = mmlitifylrn_ReferralsSyncSchedulable.class.getName() + ' scheduled job';
        }

        String jobID = system.schedule( scheduleJobName, crontabSchedule, new mmlitifylrn_ReferralsSyncSchedulable() );

        system.debug( 'the jobID == '+ jobID);

        return jobID;
    }

    public static void setupDefaultSchedule()
    {
        String scheduleJobString = mmlitifylrn_ReferralsSyncSchedulable.class.getName() + ' scheduled job';

        system.debug( 'the jobID == '+ setupSchedule( scheduleJobString + ' - every 2 hours M-F', '0 0 6-23/2 ? * MON-FRI' ));
        system.debug( 'the jobID == '+ setupSchedule( scheduleJobString + ' - every 6 hours Weekend', '0 0 0-23/6 ? * 1,7' ));
    }
}