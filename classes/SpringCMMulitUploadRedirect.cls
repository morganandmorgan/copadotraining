public class SpringCMMulitUploadRedirect {
public SpringCMMulitUploadRedirect() {
      
          Spring_CM_Matter_Forms__mdt config = [select Multi_Matter_Upload_URL__c from Spring_CM_Matter_Forms__mdt limit 1];
		this.url = config.Multi_Matter_Upload_URL__c + '&sessionId=' + UserInfo.getSessionId() + '&host=' + ApexPages.currentPage().getHeaders().get('Host');
      
			
    }

    
    public PageReference redirect(){
        
            PageReference pageRef = new PageReference(this.url);
            return pageRef;
        
    }
    public string url {get;set;}
}