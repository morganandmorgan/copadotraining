public with sharing class mmdocusign_DocusignRestApiService
{
	private static mmdocusign_IDocusignRestApiService service()
    {
        return (mmdocusign_IDocusignRestApiService) mm_Application.Service.newInstance(mmdocusign_IDocusignRestApiService.class);
	}
	
	public static mmdocusign_CreateEnvelopeResponseData createEnvelope(Intake__c record)
	{
		return service().createEnvelope(record);
	}

	public static Task createTasksForEnvelope(Intake__c intake, String envelopeId, String clientUserId)
	{
		return service().createTasksForEnvelope(intake, envelopeId, clientUserId);
	}

	public static String getRecipientLinkToEnvelope(String envelopeId, String clientEmail, String clientName, String clientUserId)
	{
		return service().getRecipientLinkToEnvelope(envelopeId, clientEmail, clientName, clientUserId);
	}
}