/**
 *  @usage new mmtraining_MatterSocialSecDataFactory().forTrainingUser( new User(username='jdaniel@forthepeople.com.training'), 0 ).adjustLawsuitRecordsRelatedMatters();
 *
 *  @usage new mmtraining_MatterSocialSecDataFactory().forTrainingUser( new User(username='jdaniel@forthepeople.com.training'), 0 )
 *                                                    .forAttorney( new User(username='sbates@forthepeople.com.training') )
 *                                                    .generateAccounts()
 *                                                    .generateIntakes()
 *                                                    .generateLawsuits()
 *                                                    .generateMatters()
 *                                                    .generateMatterRelatedTasks()
 *                                                    .persist();
 */
public class mmtraining_MatterSocialSecDataFactory
    extends mmlib_AbstractAction
{
    private User trainingUser = null;
    private String trainingUserNumber = null;
    private User attorneyUser = null;
    private map<string, string> additionalFieldMerges = null;

    // base external id patterns with merge fields
    private static final string TRAINING_USER_NUMBER_PATTERN = '<<XXX>>';
    private static final string RECORD_COUNT_PATTERN = '<<YYYY>>';
    private static final string ACCOUNT_EXTERNAL_PATTERN = 'ID-T' + TRAINING_USER_NUMBER_PATTERN + '-C' + RECORD_COUNT_PATTERN;

    private Boolean isSetupOfAccountsRequested = false;
    private Boolean isSetupOfIntakesRequested = false;
    private Boolean isSetupOfLawsuitsRequested = false;
    private Boolean isSetupOfMattersRequested = false;
    private Boolean isSetupOfMatterStagesRequested = false;
    private Boolean isSetupOfTasksRequested = false;

    private mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

    public mmtraining_MatterSocialSecDataFactory() { }

    public mmtraining_MatterSocialSecDataFactory forTrainingUser( User trainingUser, Integer trainingUserNumber )
    {
        this.trainingUser = trainingUser;
        this.trainingUserNumber = trainingUserNumber == null ? null : String.valueOf( trainingUserNumber ).leftPad( 4, '0' );

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory forAttorney( User attorneyUser )
    {
        this.attorneyUser = attorneyUser;

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory includeAdditionalFieldMerges( map<string, string> additionalFieldMerges )
    {
        this.additionalFieldMerges = additionalFieldMerges;

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory generateAccounts()
    {
        this.isSetupOfAccountsRequested = true;

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory generateIntakes()
    {
        this.isSetupOfIntakesRequested = true;

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory generateLawsuits()
    {
        this.isSetupOfLawsuitsRequested = true;

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory generateMatters()
    {
        this.isSetupOfMattersRequested = true;
        this.isSetupOfMatterStagesRequested = true;

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory generateMatterRelatedTasks()
    {
        this.isSetupOfTasksRequested = true;

        return this;
    }

    private void performValidationOfConditions()
    {
        // Do not persist if executing in a production org
        if ( ! mmlib_Utils.org.isSandbox && ! Test.isRunningTest())
        {
            throw new mmtraining_Exceptions.ExecutionInProductionNotAllowedException('Execution of this command in Production is not permitted.');
        }

        if ( this.trainingUser == null )
        {
            throw new mmtraining_Exceptions.UserNotSpecifiedException('No trainingUser was specified.  Please include the forUser(User, Integer) method in the command.');
        }

        if ( this.trainingUserNumber == null )
        {
            throw new mmtraining_Exceptions.UserNumberNotSpecifiedException('No trainingUserNumber value was specified.  Please include the forUser(User, Integer) method in the command.');
        }

    }

    public mmtraining_MatterSocialSecDataFactory reset()
    {
        performValidationOfConditions();

        string completeWildcard = 'ID-T' + trainingUserNumber + '%';

        if (isSetupofTasksRequested)
        {
            delete [select id, subject, litify_pm__Matter_Stage_Activity__r.External_ID__c
                      from Task
                     where litify_pm__Matter_Stage_Activity__r.External_ID__c like :completeWildcard];
        }

        if (isSetupOfMatterStagesRequested)
        {
            delete [select id, name, External_ID__c
                      from litify_pm__Matter_Stage_Activity__c
                     where External_ID__c like :completeWildcard];
        }

        if (isSetupOfMattersRequested)
        {
            delete [select id, name, Litify_Load_Id__c
                      from litify_pm__Matter__c
                     where Litify_Load_Id__c like :completeWildcard];
        }

        if (isSetupOfLawsuitsRequested)
        {
            delete [select id, name, AccountId__r.External_ID__c
                      from Lawsuit__c
                     where AccountId__r.External_ID__c like :completeWildcard];
        }

        if (isSetupOfIntakesRequested)
        {
            delete [select id, name, UniqueSubmissionKey__c
                      from Intake__c
                     where UniqueSubmissionKey__c like :completeWildcard];
        }

        if (isSetupOfAccountsRequested)
        {
            delete [select id, name, External_ID__c
                      from Account
                     where External_ID__c like :completeWildcard];
        }

        return this;
    }

    public mmtraining_MatterSocialSecDataFactory executeFactoryDataSetup()
    {
        system.debug( 'HELLO from executeFactoryDataSetup ');
        performValidationOfConditions();

        if (isSetupOfAccountsRequested)
        {
            setupAccountData();
        }

        if (isSetupOfIntakesRequested)
        {
            setupIntakeData01();
            setupIntakeData02();
            setupIntakeData03();
            setupIntakeData04();
        }

        if (isSetupOfLawsuitsRequested)
        {
            setupLawsuitData();
        }

        if (isSetupOfMattersRequested)
        {
            setupMatterData01();
            setupMatterData02();
            setupMatterData03();
        }

        if (isSetupOfMattersRequested)
        {
            adjustMatterStageRecordsExternalId();
        }

        if (isSetupOfMatterStagesRequested)
        {
            adjustMatterRecordsStageActivity01();
            adjustMatterRecordsStageActivity02();
        }

        if (isSetupOfLawsuitsRequested)
        {
            adjustLawsuitRecordsRelatedMatters();
        }

        if (isSetupofTasksRequested)
        {
            setupTaskRecords();
        }


        return this;
    }

    private map<string, string> getFieldMerges()
    {
        map<string, string> fieldMerges = new map<string, string>();

        fieldMerges.put( TRAINING_USER_NUMBER_PATTERN, string.valueOf(this.trainingUserNumber).leftPad(4, '0') );
        fieldMerges.put( '<<training_user_username>>', this.trainingUser.username );
        fieldMerges.put( '<<created_date>>', string.valueOf( Date.today().addDays(-2) ) );
        fieldMerges.put( '<<retainer_sent>>', string.valueOf( Date.today().addDays(-2) ) );

        if ( this.attorneyUser != null )
        {
            fieldMerges.put( '<<attorney_username>>', this.attorneyUser.username);
        }

        if ( this.additionalFieldMerges != null && ! this.additionalFieldMerges.isEmpty() )
        {
            fieldMerges.putAll( this.additionalFieldMerges );
        }

        return fieldMerges;
    }

    public void adjustMatterStageRecordsExternalId()
    {
        string TRAINING_USER_NUMER = getFieldMerges().get(TRAINING_USER_NUMBER_PATTERN);

        string completeWildcard = 'ID-T' + TRAINING_USER_NUMER + '%';
        list<litify_pm__Matter_Stage_Activity__c> records = [select Id, Name, CreatedDate
                                                                  , External_ID__c
                                                                  , litify_pm__Matter__r.Litify_Load_Id__c
                                                                  , litify_pm__Order__c
                                                                  , litify_pm__Original_Matter_Stage__r.External_ID__c
                                                                  , litify_pm__Set_As_Active_At__c
                                                                  , litify_pm__Stage_Status__c
                                                                  , litify_pm__Guidance_for_success__c
                                                               from litify_pm__Matter_Stage_Activity__c
                                                              where litify_pm__Matter__r.Litify_Load_Id__c like :completeWildcard
                                                              order by litify_pm__Matter__r.Litify_Load_Id__c, litify_pm__Order__c, name];

        for ( litify_pm__Matter_Stage_Activity__c record : records )
        {
            record.external_id__c = record.litify_pm__Matter__r.Litify_Load_Id__c + '-MS' +  (record.litify_pm__Order__c < 10 ? '0' : '' )  + string.valueOf( record.litify_pm__Order__c );
            system.debug( record.external_id__c );
        }

        database.update( records, true );
    }

    public void adjustMatterRecordsStageActivity01()
    {
        upsertMatterRecords('Training_44_litify_pm_Matter_c_Stage_Activity_XXXX_chunk01');
    }

    public void adjustMatterRecordsStageActivity02()
    {
        upsertMatterRecords('Training_44_litify_pm_Matter_c_Stage_Activity_XXXX_chunk02');
    }

    public void adjustLawsuitRecordsRelatedMatters()
    {
        upsertLawsuitRecords('Training_46_Lawsuit_c_Related_Matter_XXXX');
    }

    public void setupAccountData()
    {
        insertRecords( 'Training_10_Account_XXXX', Account.SObjectType );
    }

    public void setupIntakeData01()
    {
        insertRecords( 'Training_20_Intake_c_XXXX_chunk01', Intake__c.SObjectType );
    }

    public void setupIntakeData02()
    {
        insertRecords( 'Training_20_Intake_c_XXXX_chunk02', Intake__c.SObjectType );
    }

    public void setupIntakeData03()
    {
        insertRecords( 'Training_20_Intake_c_XXXX_chunk03', Intake__c.SObjectType );
    }

    public void setupIntakeData04()
    {
        insertRecords( 'Training_20_Intake_c_XXXX_chunk04', Intake__c.SObjectType );
    }

    public void setupLawsuitData()
    {
        insertRecords( 'Training_30_Lawsuit_c_XXXX', Lawsuit__c.SObjectType );
    }

    public void setupMatterData01()
    {
        insertRecords( 'Training_40_litify_pm_Matter_c_XXXX_chunk01', litify_pm__Matter__c.SObjectType );
    }

    public void setupMatterData02()
    {
        insertRecords( 'Training_40_litify_pm_Matter_c_XXXX_chunk02', litify_pm__Matter__c.SObjectType );
    }

    public void setupMatterData03()
    {
        insertRecords( 'Training_40_litify_pm_Matter_c_XXXX_chunk03', litify_pm__Matter__c.SObjectType );
    }

    public void setupTaskRecords()
    {
        insertRecords( 'Training_60_Task_XXXX', Task.SObjectType );

        // Now update the Task data so that the What.Name is specified to the related Matter record based on the litify_pm__Matter_Stage_Activity__c field
        // find all tasks where litify_pm__Matter_Stage_Activity__r.External_Id__c != null and What.Name is null
        list<Task> matterTaskRecords = [select id, What.Name, litify_pm__Matter_Stage_Activity__r.External_Id__c
                                             , litify_pm__Matter_Stage_Activity__r.litify_pm__Matter__r.Name
                                             , litify_pm__Matter_Stage_Activity__r.litify_pm__Matter__c
                                          from Task
                                         where litify_pm__Matter_Stage_Activity__r.External_Id__c != null
                                           and What.Name = null];

        for ( Task matterTaskRecord : matterTaskRecords )
        {
            matterTaskRecord.WhatId = matterTaskRecord.litify_pm__Matter_Stage_Activity__r.litify_pm__Matter__c;
        }

        //database.update( matterTaskRecords, true );
        this.uow.registerDirty( records );
    }

    private void insertRecords(string csvFileName, SObjectType recordsSObjectType)
    {
        map<string, string> fieldMerges = getFieldMerges();

        list<SObject> records = new mmlib_CSVFileUtils().forCSVFile(csvFileName).recordsAreSObjectType( recordsSObjectType ).useFieldMerges( fieldMerges ).consume();

        //system.debug( records.size() );
        database.insert( records, true );
        //this.uow.registerNew( records );
    }

    private void upsertLawsuitRecords( string csvFileName )
    {
        map<string, string> fieldMerges = getFieldMerges();

        SObjectType recordsSObjectType = Lawsuit__c.SObjectType;

        list<SObject> records = new mmlib_CSVFileUtils().forCSVFile(csvFileName).recordsAreSObjectType( recordsSObjectType ).useFieldMerges( fieldMerges ).consume();

        //system.debug( records.size() );

        list<Lawsuit__c> lawsuitRecords = new list<Lawsuit__c>();

        for ( SObject record : records )
        {
            lawsuitRecords.add( (Lawsuit__c)record );
            //  system.debug( record );
            //  insert record;
        }

        database.upsert( lawsuitRecords, Lawsuit__c.External_ID__c, true );
    }

    private void upsertMatterRecords(string csvFileName)
    {
        map<string, string> fieldMerges = getFieldMerges();

        SObjectType recordsSObjectType = litify_pm__Matter__c.SObjectType;

        list<SObject> records = new mmlib_CSVFileUtils().forCSVFile(csvFileName).recordsAreSObjectType( recordsSObjectType ).useFieldMerges( fieldMerges ).consume();

        //system.debug( records.size() );

        list<litify_pm__Matter__c> matterRecords = new list<litify_pm__Matter__c>();

        for ( SObject record : records )
        {
            matterRecords.add( (litify_pm__Matter__c)record );
            //  system.debug( record );
            //  insert record;
        }

        database.upsert( matterRecords, litify_pm__Matter__c.Litify_Load_Id__c, true );
    }

    public override boolean runInProcess()
    {
        executeFactoryDataSetup();

        return true;
    }
}