/**
 * MatterSync_Service
 * @description Provides functionality to sync data onto Matters.
 * @author Jeff Watson
 * @date 2/26/2019
 */

public with sharing class MatterSync_Service {

    public class InvalidRoleAssignmentException extends Exception { }

    // Service methods
    public static void updateRoleUser(List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {

        Set<Id> matterIds = getMatterIds(matterTeamMembers);
        Map<Id, litify_pm__Matter__c> mattersById = new Map<Id, litify_pm__Matter__c> (mmmatter_MattersSelector.newInstance().selectById(matterIds));

        Set<Id> matterTeamRoleIds = getMatterTeamRoleIds(matterTeamMembers);
        Map<Id, litify_pm__Matter_Team_Role__c> matterTeamRolesByIds = new Map<Id, litify_pm__Matter_Team_Role__c> (mmmatter_MatterTeamRolesSelector.newInstance().selectById(matterTeamRoleIds));

        List<litify_pm__Matter_Team_Member__c> allTeamMembersForTheseMatters = mmmatter_MatterTeamMembersSelector.newInstance().selectByMatter(matterIds);

        Boolean syncRequestIsValid = false;
        for (litify_pm__Matter_Team_Member__c matterTeamMember : matterTeamMembers) {
            litify_pm__Matter__c matter = mattersById.get(matterTeamMember.litify_pm__Matter__c);
            if (matter != null && syncRequestIsValid(matter, matterTeamMember.litify_pm__User__c, matterTeamMember.litify_pm__Role__c, allTeamMembersForTheseMatters)) {
                setTeamRoleUser(matter, matterTeamMember, matterTeamRolesByIds.get(matterTeamMember.litify_pm__Role__c));
                syncRequestIsValid = true;
            }
        }

        if (syncRequestIsValid) update mattersById.values();
    }

    public static void deleteRoleUser(List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {

        Set<Id> matterIds = getMatterIds(matterTeamMembers);
        Map<Id, litify_pm__Matter__c> mattersById = new Map<Id, litify_pm__Matter__c> (mmmatter_MattersSelector.newInstance().selectById(matterIds));

        Set<Id> matterTeamRoleIds = getMatterTeamRoleIds(matterTeamMembers);
        Map<Id, litify_pm__Matter_Team_Role__c> matterTeamRolesByIds = new Map<Id, litify_pm__Matter_Team_Role__c> ((List<litify_pm__Matter_Team_Role__c>) mmmatter_MatterTeamRolesSelector.newInstance().selectSObjectsById(matterTeamRoleIds));

        for (litify_pm__Matter_Team_Member__c matterTeamMember : matterTeamMembers) {
            litify_pm__Matter__c matter = mattersById.get(matterTeamMember.litify_pm__Matter__c);
            if (matter != null && String.isNotBlank(matterTeamMember.litify_pm__Role__c)) {
                clearTeamRoleUser(matter, matterTeamMember, matterTeamRolesByIds.get(matterTeamMember.litify_pm__Role__c));
            }
        }

        update mattersById.values();
    }

    // Utility methods
    private static Set<Id> getMatterIds(List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {
        Set<Id> matterIds = new Set<Id>();
        for (litify_pm__Matter_Team_Member__c matterTeamMember : matterTeamMembers) {
            if (String.isNotBlank(matterTeamMember.litify_pm__Matter__c)) {
                matterIds.add(matterTeamMember.litify_pm__Matter__c);
            }
        }
        return matterIds;
    }

    private static Set<Id> getMatterTeamRoleIds(List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {
        Set<Id> matterTeamRoleIds = new Set<Id>();
        for (litify_pm__Matter_Team_Member__c matterTeamMember : matterTeamMembers) {
            if (String.isNotBlank(matterTeamMember.litify_pm__Role__c)) {
                matterTeamRoleIds.add(matterTeamMember.litify_pm__Role__c);
            }
        }
        return matterTeamRoleIds;
    }

    // Validation methods
    private static Boolean syncRequestIsValid(litify_pm__Matter__c matter, Id userId, Id roleId, List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {

        if (teamMemberRoleIsAlreadyAssigned(matter.Id, roleId, matterTeamMembers)) throw new InvalidRoleAssignmentException('Team Member Role already exist for this Matter.');
        if (userIsAlreadyAssigned(matter.Id, userId, matterTeamMembers)) throw new InvalidRoleAssignmentException('User is already assigned a Team Member Role for this Matter.');

        return true;
    }

    private static Boolean teamMemberRoleIsAlreadyAssigned(Id matterId, Id roleId, List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {
        Integer roleCountForMatter = 0;
        for (litify_pm__Matter_Team_Member__c matterTeamMember : matterTeamMembers) {
            if (matterTeamMember.litify_pm__Matter__c == matterId && matterTeamMember.litify_pm__Role__c == roleId) {
                roleCountForMatter = roleCountForMatter + 1;
            }
        }
        return (roleCountForMatter > 1);
    }

    private static Boolean userIsAlreadyAssigned(Id matterId, Id userId, List<litify_pm__Matter_Team_Member__c> matterTeamMembers) {
        Integer numberOfRolesForUser = 0;
        for (litify_pm__Matter_Team_Member__c matterTeamMember : matterTeamMembers) {
            if (matterTeamMember.litify_pm__Matter__c == matterId && matterTeamMember.litify_pm__User__c == userId) {
                numberOfRolesForUser = numberOfRolesForUser + 1;
            }
        }
        return (numberOfRolesForUser > 1);
    }

    // Worker methods
    private static void setTeamRoleUser(litify_pm__Matter__c matter, litify_pm__Matter_Team_Member__c matterTeamMember, litify_pm__Matter_Team_Role__c role) {

        String roleName = role.Name;
        resetRoleUsersInMatter(matter, matterTeamMember, role);
        switch on roleName {
            when 'Principal Attorney' {
                matter.litify_pm__Principal_Attorney__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Principal Attorney 2' {
                matter.Principal_Attorney_2__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Principal Attorney 3' {
                matter.Principal_Attorney_3__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Case Developer' {
                if(matter.RecordType.Name == 'Social Security') {
                    matter.Case_Manager__c = matterTeamMember.litify_pm__User__c;
                }
            }
            when 'Case Manager' {
                matter.Case_Manager__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Intake Specialist' {
                matter.Intake_Specialist__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Paralegal' {
                matter.Litigation_Paralegal__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Paralegal 2' {
                matter.Paralegal_2__c = matterTeamMember.litify_pm__User__c;
            }
            when 'Legal Assistant' {
                if(matter.RecordType.Name == 'Social Security') {
                    matter.Case_Manager__c = matterTeamMember.litify_pm__User__c;
                } else {
                    matter.Assistant__c = matterTeamMember.Litify_pm__User__c;
                }
            }
            when 'Legal Assistant 2' {
                if(matter.RecordType.Name == 'Social Security') {
                    matter.Case_Manager__c = matterTeamMember.litify_pm__User__c;
                } else {
                    matter.Assistant_2__c = matterTeamMember.Litify_pm__User__c;
                }
            }
        }
    }

    private static void clearTeamRoleUser(litify_pm__Matter__c matter, litify_pm__Matter_Team_Member__c matterTeamMember, litify_pm__Matter_Team_Role__c role) {

        String roleName = role.Name;

        switch on roleName {
            when 'Principal Attorney' {
                matter.litify_pm__Principal_Attorney__c = null;
            }
            when 'Principal Attorney 2' {
                matter.Principal_Attorney_2__c = null;
            }
            when 'Principal Attorney 3' {
                matter.Principal_Attorney_3__c = null;
            }
            when 'Case Developer' {
                matter.Case_Manager__c = null;
            }
            when 'Case Manager' {
                matter.Case_Manager__c = null;
            }
            when 'Intake Specialist' {
                matter.Intake_Specialist__c = null;
            }
            when 'Paralegal' {
                matter.Litigation_Paralegal__c = null;
            }
            when 'Paralegal 2' {
                matter.Paralegal_2__c = null;
            }
            when 'Legal Assistant' {
                if(matter.RecordType.Name == 'Social Security') {
                    matter.Case_Manager__c = null;
                }
                matter.Assistant__c = null;
            }
            when 'Legal Assistant 2' {
                if(matter.RecordType.Name == 'Social Security') {
                    matter.Case_Manager__c = null;
                }
                matter.Assistant_2__c = null;
            }
        }
    }

    private static void resetRoleUsersInMatter(litify_pm__Matter__c matter, litify_pm__Matter_Team_Member__c matterTeamMember, litify_pm__Matter_Team_Role__c role) {

        String roleName =  role.Name;

        Map<String,String> mapOfMatterRoleDetails  = new Map<String,String>(); 
        mapOfMatterRoleDetails.put('Principal Attorney','litify_pm__Principal_Attorney__c');
        mapOfMatterRoleDetails.put('Principal Attorney 2','Principal_Attorney_2__c');
        mapOfMatterRoleDetails.put('Principal Attorney 3','Principal_Attorney_3__c');
        mapOfMatterRoleDetails.put('Case Manager','Case_Manager__c');
        mapOfMatterRoleDetails.put('Case Developer','Case_Manager__c');
        mapOfMatterRoleDetails.put('Intake Specialist','Intake_Specialist__c');
        mapOfMatterRoleDetails.put('Paralegal','Litigation_Paralegal__c');
        mapOfMatterRoleDetails.put('Paralegal 2','Paralegal_2__c');
        mapOfMatterRoleDetails.put('Legal Assistant','Assistant__c');
        mapOfMatterRoleDetails.put('Legal Assistant 2','Assistant_2__c');

        For(String mapKey : mapOfMatterRoleDetails.keySet() ){

            if( roleName != mapKey ){
                if( matter.get(mapOfMatterRoleDetails.get(mapKey)) == matterTeamMember.litify_pm__User__c){
                    matter.put(mapOfMatterRoleDetails.get(mapKey), null);
                }
            }

        }
    }

    @TestVisible
    private static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}