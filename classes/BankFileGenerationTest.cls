/*==================================================================================
Name            : BankFileGenerationTest
Author          : CLD Partners
Created Date    : Mar 2018
Description     : Primary test class for Consolidated Invoice unit testing
==================================================================================*/

@isTest
public with sharing class BankFileGenerationTest {
    static Doc_Generation_Line__c testHeaderLine;
    static Doc_Generation_Line__c testDetailLine;
    static Doc_Generation_Line__c testFooterLine;
    static list<Doc_Generation_Line__c> dglList;
    static List<Doc_Generation_Detail__c> HeaderDetailList = new List<Doc_Generation_Detail__c>();
    static List<Doc_Generation_Detail__c> DetailDetailList = new List<Doc_Generation_Detail__c>();
    static List<Doc_Generation_Detail__c> FooterDetailList = new List<Doc_Generation_Detail__c>();
    static map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> TestfileHeaderLineMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>();
    static map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> TestfileDetailLineMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>();
    static map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> TestfileFooterLineMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>();

    static Doc_Generation_Template__c testTemplate;

    static List<Account> acctList;
    static c2g__codaGeneralLedgerAccount__c testGLA;
    static c2g__codaBankAccount__c testBankAccount;
    static c2g__codaPayment__c testPayment;
    static c2g__codaAccountingCurrency__c testCurrency;
    static List<c2g__codaPaymentMediaSummary__c> testPMSList = new List<c2g__codaPaymentMediaSummary__c>();
    static c2g__codaCompany__c company;

    public static void setupAllTestData (){

        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__CustomerSettlementDiscount__c = testGLA.Id;
        update company;
        testCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c LIMIT 1];
        List<c2g__codaBankAccount__c> bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        insert bankAccountList;

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;


        //retrieve current period
        c2g__codaPeriod__c currentperiod = [SELECT Id from c2g__codaPeriod__c WHERE c2g__StartDate__c <= today AND c2g__EndDate__c >= today Limit 1];

        //create payment:
        testPayment = new c2g__codaPayment__c(
            c2g__PaymentDate__c = Date.today(),
            c2g__SettlementDiscountReceived__c = testGLA.Id,
            c2g__DiscountDate__c = Date.today(),
            c2g__Period__c = currentperiod.Id,
            c2g__BankAccount__c = operatingBankAccount.Id,
            c2g__PaymentCurrency__c = testCurrency.Id);
    }

    public static void createDocumentTemplate(String fileType, String delimiter, String dateformat, String numberformat){
        
        dglList = new List<Doc_Generation_Line__c>();
        testTemplate = new Doc_Generation_Template__c(
            File_Type__c = fileType,
            Delimiter__c = delimiter,
            File_Extension__c = '.txt',
            Date_Format__c = dateformat,
            Number_Format__c = numberformat);
        insert testTemplate;
        
        testHeaderLine = new Doc_Generation_Line__c(
            Order__c = 1,
            Root_Object__c = 'c2g__codaPayment__c',
            File_Record_Type__c = 'File Header',
            Doc_Generation_Template__c = testTemplate.Id);
        testDetailLine = new Doc_Generation_Line__c(
            Order__c = 2,
            Root_Object__c = 'c2g__codaPaymentMediaSummary__c',
            File_Record_Type__c = 'File Detail',
            Doc_Generation_Template__c = testTemplate.Id);
        testFooterLine = new Doc_Generation_Line__c(
            Order__c = 3,
            Root_Object__c = 'c2g__codaPayment__c',
            File_Record_Type__c = 'File Header',
            Doc_Generation_Template__c = testTemplate.Id);
        dglList.add(testHeaderLine);
        dglList.add(testDetailLine);
        dglList.add(testFooterLine);
        insert dglList;

        //create doc generation details for header
        Doc_Generation_Detail__c docDeatail1 = new Doc_Generation_Detail__c(
            Doc_Generation_Line__c = testHeaderLine.id,
            Order__c = 1,
            Length__c = 5,
            Field_Path__c = null,
            Mapping_Literal__c = '***',
            Text_Filler__c = '0',
            Text_Justification__c = 'Left');
        HeaderDetailList.add(docDeatail1);
        Doc_Generation_Detail__c docDeatail2 = new Doc_Generation_Detail__c(
            Doc_Generation_Line__c = testHeaderLine.id,
            Order__c = 2,
            Length__c = 40,
            Field_Path__c = 'c2g__PaymentDate__c',
            Mapping_Literal__c = null,
            Text_Filler__c = '0',
            Text_Justification__c = 'Left');
        HeaderDetailList.add(docDeatail2);
        insert HeaderDetailList;

        //create doc generation details for detail Rows
        Doc_Generation_Detail__c docDeatail3 = new Doc_Generation_Detail__c(
            Doc_Generation_Line__c = testDetailLine.id,
            Order__c = 1,
            Length__c = 5,
            Field_Path__c = null,
            Mapping_Literal__c = '***',
            Text_Filler__c = '0',
            Text_Justification__c = 'Left');
        DetailDetailList.add(docDeatail3);
        Doc_Generation_Detail__c docDeatail4 = new Doc_Generation_Detail__c(
            Doc_Generation_Line__c = testDetailLine.id,
            Order__c = 2,
            Length__c = 40,
            Field_Path__c = 'c2g__LineNumber__c',
            Mapping_Literal__c = null,
            Text_Filler__c = '0',
            Text_Justification__c = 'Left');
        DetailDetailList.add(docDeatail4);
        insert DetailDetailList;

        //populate the maps:
        TestfileHeaderLineMap.put(testHeaderLine, HeaderDetailList);
        TestfileDetailLineMap.put(testDetailLine, DetailDetailList);

    }

    // ===================== START TEST METHODS ======================
    @isTest static void testBankFileController() {
        setupAllTestData();
        createDocumentTemplate('Delimited', ',', 'MMDDYY', 'Default');
        testPayment.c2g__PaymentMediaTypes__c = 'Electronic';
        insert testPayment;

        ApexPages.currentPage().getParameters().put('id', testPayment.Id);
        ApexPages.StandardController stdCrl =  new ApexPages.StandardController(testPayment);
        BankFileGenerationController cont = new BankFileGenerationController(stdCrl);
        list<SelectOption> selectOptions = cont.getDocOptions();
        PageReference pgref1 = cont.backToPayment();
        cont.docTemplateId = testTemplate.Id;
        cont.generateFile();
    }
    
    @isTest static void testBankFileControllerCompany() {
        setupAllTestData();
        createDocumentTemplate('Delimited', ',', 'MMDDYY', 'Default');
        
        ApexPages.currentPage().getParameters().put('id', company.Id);
        ApexPages.StandardController stdCrl =  new ApexPages.StandardController(company);
        BankFileGenerationCompanyController cont = new BankFileGenerationCompanyController(stdCrl);
        list<SelectOption> selectOptions = cont.getDocOptions();
        PageReference pgref1 = cont.backToCompany();
        cont.docTemplateId = testTemplate.Id;
        cont.generateFile();
    }

    @isTest static void testBankFileHandler_Delimited() {
        setupAllTestData();
        createDocumentTemplate('Delimited', '|', 'MMDDYY', 'Default');
        testPayment.c2g__PaymentMediaTypes__c = 'Electronic';
        insert testPayment;

        c2g__codaPaymentMediaSummary__c testPMS1 = new c2g__codaPaymentMediaSummary__c(
            c2g__LineNumber__c = 1);

        testPMSList.add(testPMS1);

        BankFileHandler handler = new BankFileHandler(testPayment.Id, null, null, null, null, testTemplate.Id);
        handler.fileHeaderLineMap = TestfileHeaderLineMap;
        handler.fileDetailLineMap = TestfileDetailLineMap;
        handler.fileFooterLineMap = TestfileFooterLineMap;
        handler.paymentSummaryList = testPMSList;
        handler.renderBankFile();
        String testDate1 = handler.formatDateYYYYMMDD(Date.today());
        String testDate2 = handler.formatDateMMDDYY(Date.today());
        Double testDoub = 1.11;
        String testNumber1 = handler.formatDoubleNoDecimal(testDoub);
    }
    @isTest static void testBankFileHandler_FixedLength() {
        setupAllTestData();
        createDocumentTemplate('Fixed Length', '|', 'MMDDYY', 'Default');
        testPayment.c2g__PaymentMediaTypes__c = 'Check';
        insert testPayment;

        c2g__codaPaymentMediaSummary__c testPMS1 = new c2g__codaPaymentMediaSummary__c(
            c2g__LineNumber__c = 1);

        testPMSList.add(testPMS1);

        BankFileHandler handler = new BankFileHandler(testPayment.Id, null, null, null, null, testTemplate.Id);
        handler.paymentSummaryList = testPMSList;
        handler.renderBankFile();
        String testDate1 = handler.formatDateYYYYMMDD(Date.today());
        String testDate2 = handler.formatDateMMDDYY(Date.today());
        Double testDoub = 1.11;
        String testNumber1 = handler.formatDoubleNoDecimal(testDoub);

        handler.stripAlphaCharacters('11234dfasdf23343');
        handler.formatDateTimeHHMM(Datetime.now());

    }
}