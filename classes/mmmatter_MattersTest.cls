@isTest
private class mmmatter_MattersTest
{
    private static Account account;
    private static Lawsuit__c lawsuit;

    static {
        account = TestUtil.createAccount('UT Test');
        insert account;

        lawsuit = TestUtil.createLawsuit();
        insert lawsuit;
    }

    static testMethod void Coverage()
    {
        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');

        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.litify_pm__Status__c = 'starting_value';
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        mmmatter_Matters.newInstance(new Set<Id> {matter.Id});
        mmmatter_Matters.newInstance(new List<litify_pm__Matter__c> {matter});

        mmmatter_Matters.coverage();
    }

    @isTest
    static void InsertNewMatters()
    {
        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');

        List<litify_pm__Matter__c> recordList = new List<litify_pm__Matter__c>();
        List<litify_pm__Matter__c> existingList = new List<litify_pm__Matter__c>();

        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.litify_pm__Status__c = 'starting_value';
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        recordList.add(matter);

        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.litify_pm__Status__c = 'starting_value';
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        recordList.add(matter);

        // create existing Matter
        // clone(preserveId, isDeepClone, preserveReadonlyTimestamps, preserveAutonumber)

        // execute test
        mmmatter_Matters dom = new mmmatter_Matters(recordList);
        dom.handleSocialSecurityStatusChanged(null);
    }

    @isTest
    static void UpdateExistingMatters()
    {
        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');

        List<litify_pm__Matter__c> recordList = new List<litify_pm__Matter__c>();
        List<litify_pm__Matter__c> existingList = new List<litify_pm__Matter__c>();

        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.litify_pm__Status__c = 'new_value';
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        recordList.add(matter);

        litify_pm__Matter__c existingMatter = matter.clone(true, true, true, true);
        existingMatter.litify_pm__Status__c = 'old_value';

        existingList.add(existingMatter);

        // --------------------------------------------------------------------------
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.litify_pm__Status__c = 'starting_value';
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        recordList.add(matter);

        existingMatter = matter.clone(true, true, true, true);
        //existingMatter.litify_pm__Status__c = 'old_value';

        existingList.add(existingMatter);

        // execute test
        mmmatter_Matters dom = new mmmatter_Matters(recordList);
        dom.handleSocialSecurityStatusChanged(new Map<Id, litify_pm__Matter__c>(existingList));
    }

    public static testMethod void code_coverage() {

        Account account = TestUtil.createAccount('UT Test');
        insert account;

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;

        lawsuit.CP_Active_Case_Indicator__c = 'N';
        update lawsuit;

        matter.Related_Lawsuit__c = lawsuit.Id;
        matter.litify_pm__Status__c = 'New Status';
        update matter;

        delete matter;
    }
}