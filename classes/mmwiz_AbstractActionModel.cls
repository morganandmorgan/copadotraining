public abstract class mmwiz_AbstractActionModel
    extends mmwiz_AbstractBaseModel
{
    private boolean isUIBasedAction = false;

    public Boolean getIsUIBasedAction()
    {
        return this.isUIBasedAction;
    }

    public void setIsUIBasedAction( Boolean value )
    {
        this.isUIBasedAction = value;
    }
}