public interface mmdice_IAccessLogsSelector extends mmlib_ISObjectSelector
{
	List<DICE_AccessLog__c> selectById(Set<Id> idSet);
	String selectForPeriodicDeletionQuery();
    List<DICE_AccessLog__c> selectAll();
    List<DICE_AccessLog__c> selectAllOrderedByLastModifiedDescending();
}