/**
 * Matter_Domain_Test
 * @description Test for Matter Domain class.
 * @author Jeff Watson
 * @date 5/5/2019
 */
@IsTest
public with sharing class Matter_Domain_Test {

    @IsTest
    private static void ctor() {
        Test.startTest();
        Matter_Domain matterDomain = new Matter_Domain();
        System.assert(matterDomain != null);
        matterDomain = new Matter_Domain(new List<litify_pm__Matter__c>());
        System.assert(matterDomain != null);
        Test.stopTest();
    }

    @IsTest
    private static void onAfterInsert() {
        Test.startTest();
        Account account = TestUtil.createAccount('Unit Test Account');
        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        Matter_Domain matterDomain = new Matter_Domain(new List<litify_pm__Matter__c> {matter});
        matterDomain.onAfterInsert();
        Test.stopTest();
    }

    @IsTest
    private static void onAfterUpdate() {

        Intake__c intake = TestUtil.createIntake();
        insert intake;

        Account oa = TestUtil.createAccount('office account');
        oa.type = 'MM Office';
        insert oa;

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;

        litify_pm__Matter__c oldMatter = TestUtil.createMatter(account);
        oldMatter.litify_pm__Open_Date__c = Date.today();
        oldMatter.intake__c = intake.id;

        litify_pm__Matter__c newMatter = oldMatter.clone(true,true,true,true);
        newMatter.assigned_office_location__c = oa.id;

        Map<Id,litify_pm__Matter__c> oldMap = new Map<Id,litify_pm__Matter__c>();
        oldMap.put(oldMatter.id,oldMatter);

        Matter_Domain matterDomain = new Matter_Domain(new List<litify_pm__Matter__c> {newMatter});

        //the audit code doesn't like null Id's and we are not testing audit stuff here, and trying to insert a matter causes other issues in a test, so...
        //suppress the audit processor
        Triggers_Setting__c ts = new Triggers_Setting__c(SetupOwnerId = UserInfo.getUserId(), Object_API_Name__c = 'AuditProcessor');
        insert ts;

        matterDomain.onAfterUpdate(oldMap);

        intake = [SELECT id, assigned_office__c FROM Intake__c WHERE id = :intake.id];

        System.assertEquals(newMatter.assigned_office_location__c, intake.assigned_office__c);

    } //onAfterUpdate


    @IsTest
    private static void syncPersonnelToIntake() {

        Account fa = TestUtil.createAccount('firm account');
        fa.name = 'Morgan & Morgan P.A.';
        insert fa;

        Contact c = new Contact();
        c.lastname = UserInfo.getLastName();
        c.email = UserInfo.getUserEmail();
        c.title = 'Attorney';
        c.accountId = fa.id;
        insert c;

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;

        Intake__c intake = TestUtil.createIntake();
        insert intake;        

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        matter.litify_pm__Open_Date__c = Date.today();
        matter.intake__c = intake.id;
        matter.litify_pm__Principal_Attorney__c = UserInfo.getUserId();
        matter.case_Manager__c = UserInfo.getUserId();

        Matter_Domain matterDomain = new Matter_Domain(new List<litify_pm__Matter__c>{matter});

        matterDomain.syncPersonnelToIntake();

        intake = [SELECT id, assigned_attorney__c, case_Manager_name__c FROM Intake__c WHERE id = :intake.id];

        System.assertEquals(c.id, intake.assigned_attorney__c);
        System.assertEquals(c.id, intake.case_Manager_name__c);

    } //syncPersonnelToIntake

}