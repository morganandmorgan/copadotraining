@isTest
private class mmintake_NewMinimalIntakeControllerTest
{
    @isTest
    static void ConstructionProvidesCqsProperties()
    {
        mmintake_NewMinimalIntakeController.mockCiscoQueueSetting cqs = new mmintake_NewMinimalIntakeController.MockCiscoQueueSetting();
        cqs.handlingFirm = 'Joe\'s Tavern';
        cqs.marketingSource = 'Walk in';

        mmintake_NewMinimalIntakeController.mock_cqs = cqs;

        mmintake_NewMinimalIntakeController c = new mmintake_NewMinimalIntakeController();

        System.assertEquals(cqs.handlingFirm, c.getHandlingFirm());
        System.assertEquals(cqs.marketingSource, c.getMarketingSource());
        c.getOptOut();
    }

    @isTest
    static void FetchingPicklistItems()
    {
        Map<String, String> litigationTypeMap = mmintake_NewMinimalIntakeController.litigationTypes();
        System.AssertEquals(true, !litigationTypeMap.isEmpty());

        Map<String, String> languageMap = mmintake_NewMinimalIntakeController.languages();
        System.AssertEquals(true, !languageMap.isEmpty());

        Map<String, String> handlingFirmMap = mmintake_NewMinimalIntakeController.handlingFirms();
        System.AssertEquals(true, !handlingFirmMap.isEmpty());

        Map<String, String> marketingSourceMap = mmintake_NewMinimalIntakeController.marketingSources();
        System.AssertEquals(true, !marketingSourceMap.isEmpty());

        Map<String, String> optOutMap = mmintake_NewMinimalIntakeController.optOut();
        System.AssertEquals(true, !optOutMap.isEmpty());
    }

    @isTest
    static void SaveIntake()
    {
        Account client = new Account();
        client.Id = fflib_IDGenerator.generate( Account.SObjectType );
        mmintake_NewMinimalIntakeController.mock_client = client;

        Intake__c intake = new Intake__c();
        intake.Id = fflib_IDGenerator.generate( Intake__c.SObjectType );
        mmintake_NewMinimalIntakeController.mock_intake = intake;

        Map<String, String> fieldParameterMap = new Map<String, String>();
        fieldParameterMap.put('text-input-client-first-name', 'Joe');
        fieldParameterMap.put('text-input-client-last-name', 'Dude');
        fieldParameterMap.put('text-input-client-home-phone', '888-555-3300');
        fieldParameterMap.put('text-input-client-email-address', 'Joe.Dude@eiowfj.com');

        mmintake_NewMinimalIntakeController.IntakeDetails details = mmintake_NewMinimalIntakeController.saveIntake(fieldParameterMap);

        System.assertEquals(intake.Id, details.intakeId);
    }

    @isTest
    static void SaveIntake_ThrowsDMLException_MissingFieldsException()
    {
        DMLException dml_Exception = null;
        try
        {
            insert new Intake__c();
        }
        catch(DMLException exc)
        {
            dml_Exception = exc;
        }

        mmintake_NewMinimalIntakeController.mock_exception = new mmattyprtl_IntakeSignUpServiceException('test', dml_Exception);

        Map<String, String> fieldParameterMap = new Map<String, String>();
        fieldParameterMap.put('text-input-client-first-name', 'Joe');
        fieldParameterMap.put('text-input-client-last-name', 'Dude');
        fieldParameterMap.put('text-input-client-home-phone', '888-555-3300');
        fieldParameterMap.put('text-input-client-email-address', 'Joe.Dude@eiowfj.com');

        try
        {
            mmintake_NewMinimalIntakeController.IntakeDetails details = mmintake_NewMinimalIntakeController.saveIntake(fieldParameterMap);
            System.assert(false, 'An exception was expected.');
        }
        catch(Exception exc)
        {
            System.assert(exc.getMessage().startsWith('The following fields are required:'));
        }
    }

    @isTest
    static void SaveIntake_ThrowsNonDMLException()
    {
        mmintake_NewMinimalIntakeController.mock_exception = new mmattyprtl_IntakeSignUpServiceException('test');

        Map<String, String> fieldParameterMap = new Map<String, String>();
        fieldParameterMap.put('text-input-client-first-name', 'Joe');
        fieldParameterMap.put('text-input-client-last-name', 'Dude');
        fieldParameterMap.put('text-input-client-home-phone', '888-555-3300');
        fieldParameterMap.put('text-input-client-email-address', 'Joe.Dude@eiowfj.com');

        try
        {
            mmintake_NewMinimalIntakeController.IntakeDetails details = mmintake_NewMinimalIntakeController.saveIntake(fieldParameterMap);
            System.assert(false, 'An exception was expected.');
        }
        catch(Exception exc)
        {
            System.assertEquals(mmintake_NewMinimalIntakeController.mock_exception.getMessage(), exc.getMessage());
        }
    }
}