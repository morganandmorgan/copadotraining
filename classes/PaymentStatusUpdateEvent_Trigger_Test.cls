@isTest
private without sharing class PaymentStatusUpdateEvent_Trigger_Test {
    //exercises the platform event listener trigger.

    @isTest 
    static void testPublishEvent(){
  
        List<Payment_Status_Update__e> paymentEvents=null;
        paymentEvents=new List<Payment_Status_Update__e>();

        Payment_Status_Update__e event=null;
        event=new Payment_Status_Update__e();

        //Consumers assume this is a record Id, so I had to put a filter on this:  
        String dateString = null;
        dateString = String.valueOf(Datetime.now());

        //Exceeding a limit in the field length quietly causes the event to fail.  No error happens at publish time.
        
        event.Payment_Id__c='Testing';
 
        //event.Payment_Collection_Id__c=event.Payment_Id__c;
        //event.Payment_Collection_Id__c='TestPayCollection';

        //System.debug(event.Payment_Id__c);
        //System.debug(event.Payment_Collection_Id__c);

        event.Status__c = 'Unit Test';                   
        event.Message__c = 'Unit Testing - ' + dateString;

        paymentEvents.add(event);            

        Test.startTest();           

        if((paymentEvents!=null) && (paymentEvents.size()>0)){
            List<Database.SaveResult> streamingEventResults = null;
            streamingEventResults = EventBus.publish(paymentEvents);
        }

        //Stand and deliver:  
        Test.getEventBus().deliver();

        Test.stopTest();
        
    }    

    @isTest 
    static void testPublishEventWithRealPaymentId(){

        System.debug('Creating setup test fixture data...');

        //setupData();

        c2g__codaPayment__c payment = null;
        c2g__PaymentsPlusErrorLog__c record = null;

        System.debug('Creating payment object...');

        {

            //Note:  Looks like cnt param doesn't do anything
            payment=TestDataFactory_FFA.createPayment(1, false);
            
            {
                Database.SaveResult result = null;

                System.debug('Inserting payment object...');               
                
                result=Database.insert(payment);

                System.assert(result.isSuccess());

            }

            System.assert(payment!=null);
            System.assert(payment.Id!=null);

        }

        List<Payment_Status_Update__e> paymentEvents=null;
        paymentEvents=new List<Payment_Status_Update__e>();

        Payment_Status_Update__e event=null;
        event=new Payment_Status_Update__e();

        //Consumers assume this is a record Id, so I had to put a filter on this:  
        String dateString = null;
        dateString = String.valueOf(Datetime.now());

        //Exceeding a limit in the field length quietly causes the event to fail.  No error happens at publish time.
        event.Payment_Id__c=payment.Id;

        //A realistic fake record Id would cause downstream errors.

        //event.Payment_Collection_Id__c=event.Payment_Id__c;
        //event.Payment_Collection_Id__c='TestPayCollection';

        //System.debug(event.Payment_Id__c);
        //ystem.debug(event.Payment_Collection_Id__c);

        event.Status__c = 'Unit Test';                   
        event.Message__c = 'Unit Testing - ' + event.Payment_Id__c + ' - ' + dateString;

        paymentEvents.add(event); 

        Test.startTest();           

        if((paymentEvents!=null) && (paymentEvents.size()>0)){
            List<Database.SaveResult> streamingEventResults = null;
            streamingEventResults = EventBus.publish(paymentEvents);
        }

        //Stand and deliver:  
        Test.getEventBus().deliver();

        Test.stopTest();
        
    }


}