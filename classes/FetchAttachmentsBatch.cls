global class FetchAttachmentsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	
	String query;
	Set<Id> filteredIdSet;
	Map<Id, String> matterIdTaskIdMap = new Map<Id, String>();
	
	global FetchAttachmentsBatch( Set<Id> filteredIdSet ) {
		this.filteredIdSet = filteredIdSet;
		query = 'SELECT Id, Intake__c FROM litify_pm__Matter__c WHERE Id IN :filteredIdSet';
	}
	
	global Database.QueryLocator start( Database.BatchableContext BC ) {
		return Database.getQueryLocator( query );
	}

   	global void execute( Database.BatchableContext BC, List<litify_pm__Matter__c> matterList ) {
		SpringCMService service = new SpringCMService();
		SpringCMFolder folder;
		for( litify_pm__Matter__c matter : matterList ) {
			folder = service.findOrCreateEosFolder( matter.Intake__c, 'Intake__c' );
			SpringCMImportTask importTask = new SpringCMImportTask();
			if( Test.isRunningTest() ) {
			    importTask.Folder = new SpringCMFolder();
			    importTask.Href = 'test/taskid';
			} else {
			    importTask.Folder = folder;
			    importTask.Href = folder.Folders.Href;
			}
			System.debug( importTask );
			importTask = service.startImport( importTask );
			System.debug( importTask );
			matterIdTaskIdMap.put( matter.Id, importTask.Href.substringAfterLast( '/' ) );
		}
	}
	
	global void finish( Database.BatchableContext BC ) {
		IntakeToMatterWorkflowBatch workflowBatchInstance = new IntakeToMatterWorkflowBatch( matterIdTaskIdMap );
		System.scheduleBatch(workflowBatchInstance, 'IntakeToMatterWorkflowBatch ' + System.now().getTime(), 1 );
	}
}