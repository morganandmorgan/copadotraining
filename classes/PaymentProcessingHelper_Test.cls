@isTest
private without sharing class PaymentProcessingHelper_Test {
    
    @isTest 
    static void test00(){
        //method template
    }

    @isTest 
    static void testCreate0(){
        PaymentProcessingAddToProposalBatch batchProcessor = null;

        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);

        batchProcessor = new PaymentProcessingAddToProposalBatch(paymentCollectionId);
        System.assert(batchProcessor!=null, 'batchProcessor is null/not created.');
    }
    
    @isTest 
    static void testCreate1(){
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();  
        System.assert(utility!=null, 'helper utility is null/not created.  Darn.');
    }    

    @isTest 
    static void testCreateProposals1(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Test.startTest();

        utility.createProposals(paymentCollection, transactionLineItems);

        Test.stopTest();

        //TODO:  Assert something.

    }

    @isTest 
    static void testCreateProposals_Synchronous_0(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        paymentCollection.Payment_Method__c = 'Electronic';
        update paymentCollection;
        System.assert(paymentCollection.Payment_Method__c == 'Electronic');
        System.assert(paymentCollection.Payment_Method__c != 'Check');

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Test.startTest();

        utility.createProposals_Synchronous(paymentCollection, transactionLineItems);

        Test.stopTest();

        //TODO:  Assert payment and tracker exists and lines are added...
        //TODO:  Next step:  run post and match, especially on an electronic one that doesn't require checks.

    }
    
    @isTest 
    static void testCreateProposals_Synchronous_ThenRunPost_0(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        paymentCollection.Payment_Method__c = 'Electronic';
        update paymentCollection;
        System.assert(paymentCollection.Payment_Method__c == 'Electronic');
        System.assert(paymentCollection.Payment_Method__c != 'Check');

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Test.startTest();

        utility.createProposals_Synchronous(paymentCollection, transactionLineItems);

        Boolean postRan = false;

        {

            Integer launchPostCount = 1;

            System.debug('Launching postAndMatchPaymentsAysnc() ' + launchPostCount + ' times.');
            //Exercise the launch when already-launched smarts, and cover some more code:  
            for(Integer i=0;i<launchPostCount; i++){
                postRan=utility.postAndMatchPaymentsAysnc(paymentCollectionId);
            }
                    
        }

        Test.stopTest();    //Wait on post-and-match batch job to finish.

        System.assert(postRan);

        //TODO:  Assert payment and tracker exists and lines are added...
        //TODO:  Next step:  run post and match, especially on an electronic one that doesn't require checks.

    }
    
    @isTest 
    static void testCreateProposals_Synchronous_ThenRunPost_Repeatedly_0(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        paymentCollection.Payment_Method__c = 'Electronic';
        update paymentCollection;
        System.assert(paymentCollection.Payment_Method__c == 'Electronic');
        System.assert(paymentCollection.Payment_Method__c != 'Check');

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Test.startTest();

        utility.createProposals_Synchronous(paymentCollection, transactionLineItems);

        Boolean postRan = false;

        {

            Integer launchPostCount = 3;

            System.debug('Launching postAndMatchPaymentsAysnc() ' + launchPostCount + ' times.');
            //Exercise the launch when already-launched smarts, and cover some more code:  
            for(Integer i=0;i<launchPostCount; i++){
                postRan=utility.postAndMatchPaymentsAysnc(paymentCollectionId);
            }
                    
        }

        Test.stopTest();    //Wait on post-and-match batch job to finish.

        System.assert(postRan);

        //TODO:  Assert payment and tracker exists and lines are added...
        //TODO:  Next step:  run post and match, especially on an electronic one that doesn't require checks.

    }      
    
    @isTest 
    static void testCreateProposals_ExercisePageController_CheckStatus(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Map<String, Object> result=null;

        Test.startTest();

        utility.createProposals(paymentCollection, transactionLineItems);        
		
        //We should have payment summaries, etc., now:  
		result=PaymentProcessingController.checkBatchStatus_Step1(paymentCollectionId);
        result=PaymentProcessingController.checkBatchStatus_Step2(paymentCollectionId);
        result=PaymentProcessingController.checkBatchStatus_Step3(paymentCollectionId);

        //TODO:  The thing:  

        Test.stopTest();

        //TODO:  Assert something.

    }
    
    @isTest 
    static void testCreateProposals_ExercisePageController_RunOperation(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        TestDataFactory_FFA.createCheckRange();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Map<String, Object> result=null;

        Test.startTest();

        utility.createProposals(paymentCollection, transactionLineItems);

        PaymentProcessingDataAccess dataLayer = null;
        dataLayer = new PaymentProcessingDataAccess();
        System.assert(dataLayer!=null);
        
        paymentCollectionWrapper=dataLayer.loadPaymentCollectionData(paymentCollectionId);        

		PaymentProcessingController.ActionResult actionResult=null;

		String jsonPayload_CheckRequest = null;
		
		PaymentProcessingController.CheckNumberRequest requestObject=null;
		requestObject= new PaymentProcessingController.CheckNumberRequest();
        requestObject.paymentSummaryWrappers=paymentCollectionWrapper.payments[0].paymentSummaryWrappers;
		jsonPayload_CheckRequest=JSON.serialize(requestObject);		
       		
        //We should have payment summaries, etc., now:  
        //TODO:  The thing:  
		
        //Update check numbers:  
        actionResult=PaymentProcessingController.processPayments_Step2(jsonPayload_CheckRequest, paymentCollection.Id); 
        result=PaymentProcessingController.checkBatchStatus_Step2(paymentCollectionId);
        result=PaymentProcessingController.checkBatchStatus_Step3(paymentCollectionId);
        
        Test.stopTest();

        //TODO:  Assert something.

    }        


    @isTest 
    static void testCreateProposals_NonSeparatePayments_0(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        

        Id accountId = null;
        Account account = null;
        account=TestDataFactory_FFA.accounts[0];
        accountId = account.Id;

        account.Split_Invoices_on_Separate_Payments__c=false;
        update account;

        System.assert(accountId!=null);
        System.assert(account.Split_Invoices_on_Separate_Payments__c==false);
         
        Test.startTest();

        utility.createProposals(paymentCollection, transactionLineItems);

        Test.stopTest();

        //TODO:  Assert something.

    }     

    @isTest 
    static void testCreateProposalsAndUpdateCheckNumbers1(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        utility.createProposals(paymentCollection, transactionLineItems);
        
        //createProposals is still running asynchronously:  
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.assert(paymentCollectionWrapper!=null);
        System.assert(paymentCollectionWrapper.paymentCollection!=null);
        System.assert(paymentCollectionWrapper.payments!=null);
        System.assert(paymentCollectionWrapper.payments.size()>0);
        // System.assert(paymentCollectionWrapper.payments[0].paymentSummaryWrappers!=null);
        // System.assert(paymentCollectionWrapper.payments[0].paymentSummaryWrappers.size()>0);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);

        Test.stopTest();    //wait on batches to finish

        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);

    }

    @isTest 
    static void testCreateProposalsAndUpdateCheckNumbers2(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        utility.createProposals(paymentCollection, transactionLineItems);

        Test.stopTest();    //wait on batches to finish
        
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.assert(paymentCollectionWrapper!=null);
        System.assert(paymentCollectionWrapper.paymentCollection!=null);
        System.assert(paymentCollectionWrapper.payments!=null);
        System.assert(paymentCollectionWrapper.payments.size()>0);
        System.assert(paymentCollectionWrapper.payments[0].paymentSummaryWrappers!=null);
        System.assert(paymentCollectionWrapper.payments[0].paymentSummaryWrappers.size()>0);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);
        
        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);

    }    

    @isTest 
    static void testCreateProposalsAndUpdateCheckNumbers_Batch_0(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        paymentCollectionWrapper = utility.createProposals(paymentCollection, transactionLineItems);
        
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        //checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);

        PaymentProcessingUpdateCheckNumbersBatch batchProcessor = null;
        // batchProcessor = new PaymentProcessingUpdateCheckNumbersBatch((PaymentProcessing.PaymentCollectionWrapper ) null );
        batchProcessor = new PaymentProcessingUpdateCheckNumbersBatch((PaymentProcessing.PaymentCollectionWrapper ) paymentCollectionWrapper );
        
        checkNumberBatchJobId = Database.executeBatch(batchProcessor);

        Test.stopTest();

        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);

    }    

    @isTest 
    static void testCreateProposalsAndLaunchStatusUpdate1(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();        

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        Test.startTest();

        utility.createProposals(paymentCollection, transactionLineItems);

        Id updateStatusBatchJobId = null;
        updateStatusBatchJobId = utility.launchStatusUpdateBatchProcess(paymentCollectionId);

        Test.stopTest();

        System.assert(updateStatusBatchJobId!=null);

    }        

    private static List<c2g__codaTransactionLineItem__c> getTestTransactionLines(){
    
        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = 
        [

        select 
        Id
        ,Name 
        ,c2g__Account__c
        ,c2g__Account__r.Name 
        ,c2g__Account__r.Split_Invoices_on_Separate_Payments__c 

        ,c2g__Transaction__r.c2g__DocumentNumber__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Name
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.Name 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__r.Name 

        ,c2g__Transaction__r.c2g__PayableInvoice__r.Payment_Bank_Account_Type__c 

        ,c2g__OwnerCompany__c
        ,c2g__OwnerCompany__r.Name 
        ,c2g__DocumentOutstandingValue__c 
        ,c2g__LineType__c 

        from c2g__codaTransactionLineItem__c 

        where 
        c2g__LineType__c = 'Account'
        and c2g__Transaction__r.c2g__PayableInvoice__c != null 
        and c2g__DocumentOutstandingValue__c != 0 
        and c2g__Account__r.c2g__CODAIntercompanyAccount__c != true 
        and c2g__OwnerCompany__c = :TestDataFactory_FFA.company.Id

        and c2g__Account__r.Split_Invoices_on_Separate_Payments__c = true 

        and Id not in 
        (
            select 
            c2g__TransactionLineItem__c
            from c2g__codaPaymentLineItem__c
        )

        order by 
        c2g__Transaction__r.c2g__Account__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c 

        limit 1
        ];

        return transactionLineItems;

    } 

    private static List<c2g__codaTransactionLineItem__c> getTestTransactionLines(Id accountId){
    
        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = 
        [

        select 
        Id
        ,Name 
        ,c2g__Account__c
        ,c2g__Account__r.Name 
        ,c2g__Account__r.Split_Invoices_on_Separate_Payments__c 

        ,c2g__Transaction__r.c2g__DocumentNumber__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Name
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.Name 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__r.Name 

        ,c2g__Transaction__r.c2g__PayableInvoice__r.Payment_Bank_Account_Type__c 

        ,c2g__OwnerCompany__c
        ,c2g__OwnerCompany__r.Name 
        ,c2g__DocumentOutstandingValue__c 
        ,c2g__LineType__c 

        from c2g__codaTransactionLineItem__c 

        where 
        c2g__LineType__c = 'Account'
        and c2g__Transaction__r.c2g__PayableInvoice__c != null 
        and c2g__DocumentOutstandingValue__c != 0 
        and c2g__Account__r.c2g__CODAIntercompanyAccount__c != true 
        and c2g__OwnerCompany__c = :TestDataFactory_FFA.company.Id

        and c2g__Account__r.Split_Invoices_on_Separate_Payments__c = true 

        and Id not in 
        (
            select 
            c2g__TransactionLineItem__c
            from c2g__codaPaymentLineItem__c
        )
        and c2g__Account__c =  :accountId 

        order by 
        c2g__Transaction__r.c2g__Account__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c 

        limit 1
        ];

        return transactionLineItems;

    }     

    @isTest 
    static void testCreateProposalsUpdateCheckNumbersAndPostAndMatch_WithBatch(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        paymentCollectionWrapper=utility.createProposals(paymentCollection, transactionLineItems);
        
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);

        //For post and match to work, would have to block here until the batch is done.

        // Boolean postRan = false;
        // postRan=utility.postAndMatchPaymentsAysnc(paymentCollectionId);

        {
            PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;

            Id paymentId = null;
            //paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

            paymentId = paymentCollectionWrapper.payments[0].payment.Id;

            batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(paymentId);           
            
            // System.assert(batchProcessor!=null);

            // Id jobId = null;
            // jobId = Database.executeBatch(batchProcessor);

            // System.assert(jobId!=null);
        }             

        Test.stopTest();

        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);        

        //TODO:  Retrieve the payment and Assert posted status?

    }    


    @isTest 
    static void testCreateProposalsUpdateCheckNumbersAndPostAndMatch1(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        utility.createProposals(paymentCollection, transactionLineItems);
        
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);

        Boolean postRan = false;
        postRan=utility.postAndMatchPaymentsAysnc(paymentCollectionId);

        Test.stopTest();

        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);
        System.assert(postRan);

        //TODO:  Retrieve the payment and Assert posted status?

    }

    @isTest 
    static void testCreateProposalsUpdateCheckNumbersAndPostAndMatch_Repeatedly(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        utility.createProposals(paymentCollection, transactionLineItems);
        
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);

        Boolean postRan = false;

        Integer launchPostCount = 10;

        System.debug('Launching postAndMatchPaymentsAysnc() ' + launchPostCount + ' times.');
        //Exercise the launch when already-launched smarts, and cover some more code:  
        for(Integer i=0;i<launchPostCount; i++){
            postRan=utility.postAndMatchPaymentsAysnc(paymentCollectionId);
        }

        Test.stopTest();

        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);
        System.assert(postRan);

        //TODO:  Retrieve the payment and Assert posted status?

    }

    @isTest 
    static void testCreateProposalsUpdateCheckNumbersAndPostAndMatch_Repeatedly_WithJobs(){
        
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();
        
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();

        PaymentProcessingStatusDataAccess statusData=null;
        statusData=new PaymentProcessingStatusDataAccess();

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;
 
        TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert(transactionLineItems.size()>0);        
         
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        TestDataFactory_FFA.createCheckRange();

        Test.startTest();

        System.debug('Unit test: creating payments, adding-to-proposal, creating media data...');

        utility.createProposals(paymentCollection, transactionLineItems);
        
        paymentCollectionWrapper = data.loadPaymentCollectionData(paymentCollectionId);

        System.debug('Unit test:  updating check numbers...');

        Id checkNumberBatchJobId = null;
        checkNumberBatchJobId = utility.updateCheckNumbersAysnc(paymentCollectionWrapper);

        Payment_Processing_Batch_Status__c tracker=null;

        List<Payment_Processing_Batch_Status__c> trackers=null;
        
        trackers=statusData.getAllPaymentCollectionTrackingRecords(paymentCollectionId);

        tracker=trackers[0];

        String paymentStatus = 'Ready to Post';
        String jobStatus = 'Failed';
        String extendedStatus = 'Something went wrong, I know not what.';

        //TODO:  
        AsyncApexJob checkApexJob=null;
        checkApexJob = createApexJobRecord(jobStatus, extendedStatus);
        if(tracker.Update_Check_Numbers_Job_Id__c==null){
            //Inject a fake job Id if blank:  
            tracker.Update_Check_Numbers_Job_Id__c = checkApexJob.Id;
            update tracker;
        }
        
        //TODO:  
        AsyncApexJob apexJob=null;
        apexJob = createApexJobRecord(jobStatus, extendedStatus);
        tracker.Post_Job_Id__c = apexJob.Id;
        if(tracker.Post_Job_Id__c==null){
            tracker.Post_Job_Id__c = apexJob.Id;
            update tracker;
        }        

        Boolean postRan = false;

        Integer launchPostCount = 10;

        System.debug('Launching postAndMatchPaymentsAysnc() ' + launchPostCount + ' times.');
        //Exercise the launch when already-launched smarts, and cover some more code:  
        for(Integer i=0;i<launchPostCount; i++){
            postRan=utility.postAndMatchPaymentsAysnc(paymentCollectionId);
        }

        Test.stopTest();

        //TODO:  Assert something.
        System.assert(checkNumberBatchJobId!=null);
        System.assert(postRan);

        //TODO:  Retrieve the payment and Assert posted status?

    }

    static Payment_Collection__c createTestPaymentCollection(){
        //Note:  These are better candidates for reuse.
        System.debug('Creating payment collection...');

        Payment_Collection__c record = null;
     
		record = new Payment_Collection__c(
			Company__c = TestDataFactory_FFA.company.Id,
			Bank_Account__c = TestDataFactory_FFA.bankAccounts[0].Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		
        return record;

    }      

    @isTest 
    static void test_insertPaymentCollection_0(){
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();  
        System.assert(utility!=null, 'helper utility is null/not created.  Darn.');

        Payment_Collection__c paymentCollection=null;
        paymentCollection=createTestPaymentCollection();
        utility.insertPaymentCollection(paymentCollection);
        
    } 

    @isTest 
    static void test_insertPaymentCollection_1(){
        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();  
        System.assert(utility!=null, 'helper utility is null/not created.  Darn.');

        Payment_Collection__c paymentCollection=null;
        paymentCollection=createTestPaymentCollection();

        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper=null;

        paymentCollectionWrapper=new PaymentProcessing.PaymentCollectionWrapper();
        paymentCollectionWrapper.paymentCollection=paymentCollection;

        utility.insertPaymentCollection(paymentCollectionWrapper);
        
    } 

    @isTest 
    static void testPaymentProcessPostDriver_handleAsyncException_1(){

        PaymentProcessPostDriver postDriver = null;
        postDriver = new PaymentProcessPostDriver();  
        System.assert(postDriver!=null, 'helper utility is null/not created.  How could this happen?');

        System.AsyncException ae = new System.AsyncException();
        Payment_Processing_Batch_Status__c tracker=null;
        tracker=new Payment_Processing_Batch_Status__c();

        Test.startTest();        
        postDriver.handleAsyncException(ae, tracker);
        Test.stopTest();

    }    

    @isTest 
    static void test_BatchHelper_0(){

        BatchProcessHelper target = null;
        target = new BatchProcessHelper();

        Test.startTest();        
        target.getExecutingBatchJobCount();
        target.getHoldingBatchJobCount();
        Test.stopTest();

        System.assert(target!=null, 'batch helper utility is null/not created. ');

    }     

    static AsyncApexJob createApexJobRecord(String status, String extendedStatus){
        AsyncApexJob record=null;
        record=new AsyncApexJob();
        if(status!=null){
            //Hack to get around //Field is not writeable: AsyncApexJob.Status
            String jobJSON = null;
            jobJSON = '{"Status": "'+ status + '"}';
            if(extendedStatus!=null){
                jobJSON = '{"Status": "'+ status + '", "ExtendedStatus": "'+ extendedStatus + '"}';
            }
            record = (AsyncApexJob) JSON.deserialize(jobJSON, AsyncApexJob.class);
            System.assert(record.Status!=null);
        }
        System.assert(record!=null);

        Id jobId = null;
        jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType);  

        record.Id = jobId;      

        System.assert(record.Id!=null);

        return record;
    }    
    
}