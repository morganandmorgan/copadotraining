/**
 * SpringCMImportTaskStatus_Test
 * @description Test for Spring CM Import Task Status class.
 * @author Jeff Watson
 * @date 5/5/2019
 */
@IsTest
public with sharing class SpringCMImportTaskStatus_Test {

    @IsTest
    private static void ctor() {
        Test.startTest();
        SpringCMImportTaskStatus springCMImportTaskStatus = new SpringCMImportTaskStatus();
        springCMImportTaskStatus.Status = '';
        springCMImportTaskStatus.Message = '';
        springCMImportTaskStatus.Href = '';
        Test.stopTest();
    }
}