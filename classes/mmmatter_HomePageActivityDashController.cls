public with sharing class mmmatter_HomePageActivityDashController {
    @AuraEnabled
    public static PageSettings getSettings() {
        PageSettings ps = new PageSettings();
        Matter_Related__c mr = Matter_Related__c.getInstance();
        ps.pageSize = Integer.valueOf(mr.Number_of_Records_Per_Page__c);
        return ps;
    }

    @AuraEnabled
    public static List<WrapperTask> getTodaysTasks() {
        return computeWrapperTasks(
            mmcommon_TasksSelector.newInstance().selectMyTodayOpenTasks(),
            mmcommon_EventsSelector.newInstance().selectMyTodayStartEvents()
        );
    }

    @AuraEnabled
    public static List<WrapperTask> getThisWeekTasks() {
        Matter_Related__c mr = Matter_Related__c.getInstance();
        return computeWrapperTasks(
            mmcommon_TasksSelector.newInstance().selectFutureOpenTasks(Integer.valueOf(mr.Number_of_Days_Future_Tasks__c)),
            mmcommon_EventsSelector.newInstance().selectFutureOpenTasks(Integer.valueOf(mr.Number_of_Days_Future_Tasks__c))
        );
    }

    @AuraEnabled
    public static List<WrapperTask> getOverdueTasks() {
        return computeWrapperTasks(
            mmcommon_TasksSelector.newInstance().selectMyOverdueTasks(),
            null
        );
    }

    @TestVisible
    private static List<WrapperTask> computeWrapperTasks(List<Task> tasks, List<Event> events) {
        Map<Id, litify_pm__Matter__c> matters = new Map<Id, litify_pm__Matter__c>();
        if (tasks != null) {
            for (Task t : tasks) {
                matters.put(t.WhatId, null);
            }
        }
        if (events != null) {
            for (Event t : events) {
                matters.put(t.WhatId, null);
            }
        }

        matters = new Map<Id, litify_pm__Matter__c>(mmmatter_MattersSelector.newInstance().selectById(matters.keySet()));
        List<WrapperTask> wt = new List<WrapperTask>();
        if (tasks != null) {
            for (Task t : tasks) {
                wt.add(new WrapperTask(t, matters.get(t.WhatId)));
            }
        }
        if (events != null) {
            for (Event t : events) {
                wt.add(new WrapperTask(t, matters.get(t.WhatId)));
            }
        }

        return wt;
    }

    @AuraEnabled
    public static List<WrapperMatter> getMattersWithoutTasks() {
        List<WrapperMatter> ret = new List<WrapperMatter>();
        for (litify_pm__Matter__c mat : mmmatter_MattersSelector.newInstance().selectMyOpenWithoutFutureActivities()) {
            ret.add(new WrapperMatter(mat));
        }
        return ret;
    }

    public class WrapperMatter {
        @AuraEnabled public Id matterId;
        @AuraEnabled public String matterName;
        @AuraEnabled public Id clientid;
        @AuraEnabled public String clientName;
        @AuraEnabled public String referenceNumber;
        @AuraEnabled public String stage;
        @AuraEnabled public DateTime createdDate;
        @AuraEnabled public DateTime lastModifiedDate;
        public WrapperMatter(litify_pm__Matter__c m) {
            this.matterId = m.Id;
            this.matterName = m.Name;
            this.clientId = m.litify_pm__Client__c;
            this.clientName = m.litify_pm__Client__r.Name;
            this.referenceNumber = m.ReferenceNumber__c;
            this.stage = m.litify_pm__Matter_Stage_Activity_Formula__c;
            this.createdDate = m.CreatedDate;
            this.lastModifiedDate = m.LastModifiedDate;
        }
    }
    public class PageSettings {
        @AuraEnabled public Integer pageSize;
    }
    public class WrapperTask {

        @AuraEnabled public Id taskId;
        @AuraEnabled public String subject;
        @AuraEnabled public Id matterId;
        @AuraEnabled public String matterName;
        @AuraEnabled public String matterDisplayName;
        @AuraEnabled public String matterReferenceNumber;
        @AuraEnabled public Id clientId;
        @AuraEnabled public String clientName;
        @AuraEnabled public Date dueDate;
        @AuraEnabled public DateTime dueDateTime;

        public WrapperTask(Task t, litify_pm__Matter__c m) {
            this.taskId = t.Id;
            this.subject = t.Subject;
            this.dueDate = t.ActivityDate;
            if (m != null) {
                this.matterid = m.Id;
                this.matterName = m.Name;
                this.matterReferenceNumber = m.ReferenceNumber__c;
                this.clientId = m.litify_pm__Client__c;
                this.clientName = m.litify_pm__Client__r.Name;
                this.matterDisplayName = m.litify_pm__Display_Name__c;
            }
        }

        public WrapperTask(Event t, litify_pm__Matter__c m) {
            this.taskId = t.Id;
            this.subject = t.Subject;
            this.dueDateTime = t.StartDateTime;
            if (m != null) {
                this.matterid = m.Id;
                this.matterName = m.Name;
                this.matterReferenceNumber = m.ReferenceNumber__c;
                this.clientId = m.litify_pm__Client__c;
                this.clientName = m.litify_pm__Client__r.Name;
                this.matterDisplayName = m.litify_pm__Display_Name__c;
            }
        }
    }
}