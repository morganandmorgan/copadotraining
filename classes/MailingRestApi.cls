@RestResource(urlMapping='/mailing/*')
global without sharing class MailingRestApi {

/*

{
  "Client": "FORTHEPEOPLE",
  "FileId": "550125",
  "MailingID": "MAIL-0000000",
  "MailDate": "04/06/2020",
  "TrackingNo": "9785946156431248",
  "SharedSecret": "z0ZwaMtA9gkB3NlFMC3JRH/JBuLsmOaIgkMT9V6I2/8"
}

*/

    private static String docuFreeSecret = 'z0ZwaMtA9gkB3NlFMC3JRH/JBuLsmOaIgkMT9V6I2/8';
    private static String internalSecret = 'da510b34-e31a-4074-963f-fd1e9fde6dc1';


    @HttpPost
    global static void updateStatus() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String requestBody = request.requestBody.toString();

        //MailingOnMatterController.sendEmail( requestBody );

        Map<String,String> updateMap = (Map<String,String>)Json.deserialize(requestBody, Map<String,String>.class);

        //bail out if we don't get one of the shared secrets
        if (updateMap.get('SharedSecret') != docuFreeSecret && updateMap.get('SharedSecret') != internalSecret) {
			response.statusCode = 403;
            return;
        }

        String newStatus;
        if (updateMap.get('SharedSecret') == docuFreeSecret) {
            newStatus = 'Sent';
        } else {
            newStatus = 'Processing';
        }

        Id mailingId = updateMap.get('MailingID');
		List<Mailing__c> mailing = [SELECT id, status__c FROM Mailing__c WHERE id = :mailingId];

        Map<String,String> responseMap = new Map<String,String>();
        responseMap.put('mailingId', mailingId);
        responseMap.put('status', newStatus);
        if (mailing.size() == 1) {
            mailing[0].status__c = newStatus;
            if ( !String.IsBlank(updateMap.get('TrackingNo')) ) {
                mailing[0].tracking_number__c = updateMap.get('TrackingNo');
            }
            update mailing;
            responseMap.put('update','success');
        } else {
            responseMap.put('update','failed');
            responseMap.put('message','Mailing__c record not found.');
			response.statusCode = 500;
        }

		response.responseBody = Blob.valueOf( Json.serialize(responseMap) );

    } //updateStatus


} //MailingRestApi