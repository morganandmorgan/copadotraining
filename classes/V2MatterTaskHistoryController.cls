/**
 * Created by noah on 2019-01-17.
 */

public with sharing class V2MatterTaskHistoryController {
  @AuraEnabled
  public static Model getRecs(String matterId) {

    List<HistoryRec> historyRecs = new List<HistoryRec>();

    map<string, string> descriptionToLabel = new map<string, string>();
    for (Time_Slip_Code__mdt code : mmmatter_TimeSlipCodeSelector.newInstance().selectAll()) {
      if (!String.isEmpty(code.Description__c)) {
        descriptionToLabel.put(code.Description__c, code.MasterLabel);
      }
    }

    List<Task> tasks = mmcommon_TasksSelector.newInstance().selectCompletedTasksByRelatedId(matterId);
    for (Task task : tasks) {
      if (String.isNotEmpty(task.litify_pm__Document_Link__c) /* heuristic for determinging that it's a document*/)
      {
        continue; // skip all Task documents
      }
      historyRecs.add(new HistoryRec(task, descriptionToLabel));
    }

    List<MM_Document__c> mmDocs = [SELECT Id, Name, Owner.Name, CreatedBy.Name, CreatedDate, File_Type__c, From__c, Document_Name__c, Document_Link__c, To__c, Document_Type__c, Document_Subtype__c, Uploaded_By__c FROM MM_Document__c WHERE Matter__c = :matterId];
    for (MM_Document__c doc : mmDocs) {
      historyRecs.add(new HistoryRec(doc));
    }

    List<EmailMessage> emailMessages = [select Id, CreatedBy.Name, CreatedDate, MessageDate, RelatedToId, Subject, TextBody, ToAddress, FromAddress, FromName, Email_Type__c, Email_Subtype2__c from EmailMessage where RelatedToId = :matterId];
    for (EmailMessage emailMessage : emailMessages) {
      historyRecs.add(new HistoryRec(emailMessage));
    }

    return new Model(historyRecs, historyRecs.size());
  }

  @AuraEnabled
  public static SObject updateField(Id id, String fieldName, String value) {
    String sobjectField = null;
    Schema.SObjectType token = id.getSobjectType();
    if (token == MM_Document__c.getSObjectType()){
      if (fieldName == 'subject') {
        sobjectField = 'Document_Name__c';
      } else if (fieldName == 'documentType') {
        sobjectField = 'Document_Type__c';
      } else if (fieldName == 'documentSubtype') {
        sobjectField = 'Document_SubType__c';
      } else if (fieldName == 'documentTo') {
        sobjectField = 'To__c';
      } else if (fieldName == 'documentFrom') {
        sobjectField = 'From__c';
      }
    } else if (token == EmailMessage.getSObjectType()){
      if (fieldName == 'documentType') {
        sobjectField = 'Email_Type__c';
      } else if (fieldName == 'documentSubtype') {
        sobjectField = 'Email_Subtype2__c';
      }
    }

    if (sobjectField == null) {
      throw new NoAccessException();
    }

    String name = token.getDescribe().getName();

    String query = 'SELECT id, ' + sobjectField + ' FROM ' + name + ' WHERE ID = \'' + id + '\'';
    SObject obj = Database.Query(query);

    system.debug('going to update ' + name + ' field ' + sobjectField + ' with value ' + value);
    obj.put(sobjectField, value);
    update obj;

    return obj;
  }

  @AuraEnabled
  public static Map<Id, String> getBodies(Id[] ids) {
    List<Id> taskIds = new List<Id>();
    List<Id> emailIds = new List<Id>();

    for (Id i : ids) {
      Schema.SObjectType token = i.getSobjectType();
      if (token == Task.getSObjectType()) {
        taskIds.add(i);
      } else if (token == EmailMessage.getSObjectType()) {
        emailIds.add(i);
      }
    }

    List<EmailMessage> ems = [SELECT Id, HtmlBody, TextBody FROM EmailMessage WHERE Id IN :emailIds];
    List<Task> ts = [SELECT Id, Description FROM Task WHERE Id IN :taskIds];

    Map<Id, String> bodies = new Map<Id, String>();

    if (ems != null) {
      for (EmailMessage e : ems) {
        // bodies.put(e.Id, (String.isNotEmpty(e.HtmlBody)) ? e.TextBody : e.TextBody);
        String textBody = e.TextBody;
          if (textBody == null) {
              textBody = '';
          }
          textBody = textBody.escapeHtml4();
          
        bodies.put(e.Id, textBody);
      }
    }

    if (ts != null) {
      for (Task t : ts) {
        bodies.put(t.Id, t.Description);
      }
    }

    return bodies;
  }

  public class SelectOption {
    @AuraEnabled public String Label { get; set; }
    @AuraEnabled public String Value { get; set; }
    public SelectOption (String l, String v) {
      Label = l;
      Value = v;
    }
  }

  public class Model {

    @AuraEnabled public List<HistoryRec> items;
    @AuraEnabled public Integer count { get {return this.items.size();} }
    @AuraEnabled public Integer totalCount { get; private set; }
    @AuraEnabled public List<SelectOption> documentTypeOptions { get; private set; }
    @AuraEnabled public List<SelectOption> documentSubTypeOptions { get; private set; }

    private List<V2MatterTaskHistoryController.SelectOption> getOptionsFor(String fieldName) {
      List<V2MatterTaskHistoryController.SelectOption> result = new List<V2MatterTaskHistoryController.SelectOption>();
      Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.MM_Document__c.fields.getMap();
      Schema.SObjectField theField = fieldMap.get(fieldName);
      Schema.DescribeFieldResult fieldResult = theField.getDescribe();
      List<Schema.PicklistEntry> options = fieldResult.getPicklistValues();
      result.add(new V2MatterTaskHistoryController.SelectOption('-', null));
      for( Schema.PicklistEntry entry : options) {
        result.add(new V2MatterTaskHistoryController.SelectOption(entry.getLabel(), entry.getValue()));
      }
      return result;
    }

    public Model(List<HistoryRec> historyRecs, Integer totalCount) {
      this.items = new List<HistoryRec>();
      this.totalCount = totalCount;

      historyRecs.sort();

      this.items.addAll(historyRecs);

      documentTypeOptions = getOptionsFor('Document_Type__c');
      documentSubTypeOptions = getOptionsFor('Document_SubType__c');
    }
  }

  public class HistoryRec implements Comparable {
    @AuraEnabled public String id;
    @AuraEnabled public Date activityDate;
    @AuraEnabled public Datetime completedDate;
    @AuraEnabled public String subject;
    @AuraEnabled public String description;
    @AuraEnabled public Boolean hasDescription;
    @AuraEnabled public String activityPurpose;
    @AuraEnabled public String detail;
    @AuraEnabled public String assignedTo;
    @AuraEnabled public String timeKeeper;
    @AuraEnabled public String detailCode;
    @AuraEnabled public String type;
    @AuraEnabled public String detailHours;
    @AuraEnabled public String documentLink;
    @AuraEnabled public String documentTo;
    @AuraEnabled public String documentFrom;
    @AuraEnabled public String documentType;
    @AuraEnabled public String documentSubtype;
    @AuraEnabled public String fileType;
    @AuraEnabled public Boolean editable;

    public HistoryRec(MM_Document__c doc) {
      this.id = doc.Id;
      this.completedDate = doc.CreatedDate;
      this.subject = doc.Document_Name__c;
      this.type = 'Document';
      this.documentType = doc.Document_Type__c;
      this.documentSubtype = doc.Document_Subtype__c;
      this.documentLink = doc.Document_Link__c;
      this.fileType = doc.File_Type__c == null ? '' : doc.File_Type__c.toLowerCase();
      this.documentFrom = doc.From__c;
      this.documentTo = doc.To__c;
      this.assignedTo = doc.Uploaded_By__c;
      this.editable = true;
    }

    public HistoryRec(Task task, Map<String, String> descriptionToCode) {
      this.id = task.Id;
      this.activityDate = task.ActivityDate == null ? date.newinstance(task.CreatedDate.year(), task.CreatedDate.month(), task.CreatedDate.day()) : task.ActivityDate;
      this.completedDate = task.Completed_Date__c == null ? null : task.Completed_Date__c;
      this.subject = task.Subject;
      //this.description = task.Description;
      this.hasDescription = String.isNotEmpty(task.Description);
      this.activityPurpose = task.Activity_Purpose__c;
      this.detail = task.Detail__c;
      this.assignedTo = task.Owner.Name;
      this.timeKeeper = task.Timekeeper__c == null ? null : task.Timekeeper__c;
      this.detailCode = String.isEmpty(task.Detail__c) ? null : descriptionToCode.get(task.Detail__c);
      this.detailHours = String.valueOf(task.TimeSlip_Hours__c);
      this.type = (String.isNotEmpty(task.litify_pm__Document_Link__c)) ? 'Document' : task.Type;
      this.documentLink = task.litify_pm__Document_Link__c;
      this.documentTo = task.Type == 'Document' ? task.To__c : task.CreatedBy.Name;
      this.documentFrom = task.From__c;
    }

    public HistoryRec(EmailMessage emailMessage) {

      this.id = emailMessage.Id;
      this.assignedTo = emailMessage.CreatedBy.Name;
      this.completedDate = emailMessage.MessageDate;
      this.subject = emailMessage.Subject;
      //this.description = (String.isNotEmpty(emailMessage.HtmlBody)) ? emailMessage.HtmlBody : emailMessage.TextBody;
      this.hasDescription = String.isNotEmpty(emailMessage.TextBody);
      this.type = 'Email';
      this.documentTo = emailMessage.ToAddress;
      this.documentFrom = String.isEmpty(emailMessage.FromName) ? emailMessage.FromAddress : emailMessage.FromName;
      this.documentType = emailMessage.Email_Type__c;
      this.documentSubtype = emailMessage.Email_Subtype2__c;
    }

    public Integer compareTo(Object compareTo)
    {
      HistoryRec historyRec = (HistoryRec) compareTo;
      if (completedDate == historyRec.completedDate) return 0;
      if (completedDate < historyRec.completedDate) return 1;
      return -1;
    }
  }
}