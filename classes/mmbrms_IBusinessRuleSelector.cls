public interface mmbrms_IBusinessRuleSelector extends mmlib_ISObjectSelector
{
	List<BusinessRule__mdt> selectByMasterLabels(Set<String> labelSet);
	List<BusinessRule__mdt> selectByDeveloperNames(Set<String> developerNamesSet);
	List<BusinessRule__mdt> selectAll();
}