@isTest
private class SoqlUtilsTest {
    private static Integer idNum = 1;
    
    private static Id getFakeProductId() {
        String myIdNum = String.valueOf(idNum++);
        return Id.valueOf(Product2.SObjectType.getDescribe().getKeyPrefix() + '0'.repeat(12 - myIdNum.length()) + myIdNum);
    }

    @isTest static void testGetSelect() {
        String query = SoqlUtils.getSelect(new List<String> {
            'Id',
            'Name'
        }, 'Product2').toString();

        System.assertEquals('SELECT Id, Name FROM Product2', query);
    }

    @isTest static void testGetSelectAggregate() {
        Map<String, String> fieldMap = new Map<String, String>();
        fieldMap.put('field1__c', 'FIELD_1');
        fieldMap.put('field2__c', 'FIELD_2');
        fieldMap.put('field3__c', 'FIELD_3');

        String query = SoqlUtils.getSelectAggregate('SUM', fieldMap, 'SomeObject').toString();

        System.assertEquals('SELECT SUM(field1__c) FIELD_1, SUM(field2__c) FIELD_2, SUM(field3__c) FIELD_3 FROM SomeObject', query);
    }
    
    @isTest static void testGetLike() {
        String likeClause = SoqlUtils.getLike('Name', '%Produc\'t%').toString();

        System.assertEquals(likeClause, 'Name LIKE \'%Produc\\\'t%\'');
    }

    @isTest static void testGetAndOr() {
        String andClause = SoqlUtils.getAnd(new List<SoqlUtils.SoqlCondition> {
            SoqlUtils.getEq('My_Field__c', '1'),
            SoqlUtils.getEq('Other_Field__c', '2'),
            SoqlUtils.getOr(new List<SoqlUtils.SoqlCondition> {
                SoqlUtils.getLike('Field__c', '3'),
                SoqlUtils.getLike('Last__c', '4')
            })
        }).toString();

        System.assertEquals(andClause, '(My_Field__c = \'1\' AND Other_Field__c = \'2\' AND (Field__c LIKE \'3\' OR Last__c LIKE \'4\'))');
    }

    @isTest static void testGetOrAnd() {
        String andClause = SoqlUtils.getOr(new List<SoqlUtils.SoqlCondition> {
            SoqlUtils.getEq('My_Field__c', '1'),
            SoqlUtils.getEq('Other_Field__c', '2'),
            SoqlUtils.getAnd(new List<SoqlUtils.SoqlCondition> {
                SoqlUtils.getLike('Field__c', '3'),
                SoqlUtils.getLike('Last__c', '4')
            })
        }).toString();

        System.assertEquals(andClause, '(My_Field__c = \'1\' OR Other_Field__c = \'2\' OR (Field__c LIKE \'3\' AND Last__c LIKE \'4\'))');
    }

    @isTest static void testGetEq() {
        String eqClause = SoqlUtils.getEq('My_Field__c', 'Test Val\'ue 123').toString();

        System.assertEquals(eqClause, 'My_Field__c = \'Test Val\\\'ue 123\'');
    }

    @isTest static void testGetNotEq() {
        String eqClause = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 123').toString();

        System.assertEquals(eqClause, 'My_Field__c != \'Test Val\\\'ue 123\'');
    }

    @isTest static void testGetOrderThenBy() {

        String orderClause = SoqlUtils.getOrder('My_Field__c')
            .thenBy('Other_Field__c').descending()
            .thenBy('Final_Field__c').descending().withNullsLast()
            .toString();

        System.assertEquals('My_Field__c, Other_Field__c DESC, Final_Field__c DESC NULLS LAST', orderClause);
    }

    @isTest static void testGetOrder() {
        // order respects the builder pattern

        String orderClause = SoqlUtils.getOrder('My_Field__c').toString();
        String orderClauseWithNullsFirst = SoqlUtils.getOrder('My_Field__c').withNullsFirst().toString();
        String orderClauseWithNullsLast = SoqlUtils.getOrder('My_Field__c').withNullsLast().toString();
        String orderClauseAscending = SoqlUtils.getOrder('My_Field__c').ascending().toString();
        String orderClauseDescending = SoqlUtils.getOrder('My_Field__c').descending().toString();
        String orderClauseDescendingWithNullsLast = SoqlUtils.getOrder('My_Field__c').descending().withNullsLast().toString();

        System.assertEquals(orderClause, 'My_Field__c');
        System.assertEquals(orderClauseWithNullsFirst, 'My_Field__c NULLS FIRST');
        System.assertEquals(orderClauseWithNullsLast, 'My_Field__c NULLS LAST');
        System.assertEquals(orderClauseAscending, 'My_Field__c ASC');
        System.assertEquals(orderClauseDescending, 'My_Field__c DESC');
        System.assertEquals(orderClauseDescendingWithNullsLast, 'My_Field__c DESC NULLS LAST');
    }

    @isTest static void testGetOrderMutability() {
        // order is mutable

        SoqlUtils.SoqlOrder order = SoqlUtils.getOrder('Field');

        System.assertEquals(order.toString(), 'Field');
        order.descending();
        System.assertEquals(order.toString(), 'Field DESC');
        order.withNullsLast();
        System.assertEquals(order.toString(), 'Field DESC NULLS LAST');
        order.ascending();
        System.assertEquals(order.toString(), 'Field ASC NULLS LAST');
    }

    @isTest static void testGetNotInList() {
        String inClause = SoqlUtils.getNotIn('Something__c', new List<String> {
            'value 1',
            'value 2',
            'val\'ue 3'
        }).toString();

        System.assertEquals(inClause, 'Something__c NOT IN (\'value 1\', \'value 2\', \'val\\\'ue 3\')');
    }

    @isTest static void testGetNotInSet() {
        String inClause = SoqlUtils.getNotIn('Something__c', new Set<String> {
            'value 1',
            'value 2',
            'val\'ue 3'
        }).toString();

        System.assert(Pattern.compile('Something__c NOT IN \\((\'(value 1|value 2|val\\\\\'ue 3)\',?\\s*){3}\\)').matcher(inClause).matches());
        System.assert(inClause.contains('\'value 1\''));
        System.assert(inClause.contains('\'value 2\''));
        System.assert(inClause.contains('\'val\\\'ue 3\''));
    }

    @isTest static void testGetNotInIdList() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String inClause = SoqlUtils.getNotIn('Something__c', new List<Id> { id1, id2, id3 }).toString();

        System.assertEquals(inClause, 'Something__c NOT IN (\''+id1+'\', \''+id2+'\', \''+id3+'\')');
    }

    @isTest static void testGetNotInIdSet() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String inClause = SoqlUtils.getNotIn('Something__c', new Set<Id> { id1, id2, id3 }).toString();

        System.assert(Pattern.compile('Something__c NOT IN \\((\'('+id1+'|'+id2+'|'+id3+')\',?\\s*){3}\\)').matcher(inClause).matches());
        System.assert(inClause.contains(id1));
        System.assert(inClause.contains(id2));
        System.assert(inClause.contains(id3));
    }

    @isTest static void testGetNotInSubquery() {
        String inClause = SoqlUtils.getNotIn('Something__c', SoqlUtils.getSelect(new List<String> { 'Id' }, 'Product2')).toString();

        System.assertEquals(inClause, 'Something__c NOT IN (SELECT Id FROM Product2)');
    }

    @isTest static void testGetInList() {
        String inClause = SoqlUtils.getIn('Something__c', new List<String> {
            'value 1',
            'value 2',
            'val\'ue 3'
        }).toString();

        System.assertEquals(inClause, 'Something__c IN (\'value 1\', \'value 2\', \'val\\\'ue 3\')');
    }

    @isTest static void testGetInSet() {
        String inClause = SoqlUtils.getIn('Something__c', new Set<String> {
            'value 1',
            'value 2',
            'val\'ue 3'
        }).toString();

        System.assert(Pattern.compile('Something__c IN \\((\'(value 1|value 2|val\\\\\'ue 3)\',?\\s*){3}\\)').matcher(inClause).matches());
        System.assert(inClause.contains('\'value 1\''));
        System.assert(inClause.contains('\'value 2\''));
        System.assert(inClause.contains('\'val\\\'ue 3\''));
    }

    @isTest static void testGetInIdList() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String inClause = SoqlUtils.getIn('Something__c', new List<Id> { id1, id2, id3 }).toString();

        System.assertEquals(inClause, 'Something__c IN (\''+id1+'\', \''+id2+'\', \''+id3+'\')');
    }

    @isTest static void testGetInIdSet() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String inClause = SoqlUtils.getIn('Something__c', new Set<Id> { id1, id2, id3 }).toString();

        System.assert(Pattern.compile('Something__c IN \\((\'('+id1+'|'+id2+'|'+id3+')\',?\\s*){3}\\)').matcher(inClause).matches());
        System.assert(inClause.contains(id1));
        System.assert(inClause.contains(id2));
        System.assert(inClause.contains(id3));
    }
    @isTest static void testGetInSubquery() {
        String inClause = SoqlUtils.getIn('Something__c', SoqlUtils.getSelect(new List<String> { 'Id' }, 'Product2')).toString();

        System.assertEquals(inClause, 'Something__c IN (SELECT Id FROM Product2)');
    }

    @isTest static void testGetIncludesAllList() {
        String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<String> {
            'value 1',
            'value 2',
            'value 3'
        }).toString();

        System.assertEquals('Something__c INCLUDES (value 1;value 2;value 3)', includesClause);
    }

    @isTest static void testGetIncludesAllListSingle() {
        String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<String> {
            'value 1'
        }).toString();

        System.assertEquals('Something__c INCLUDES (value 1)', includesClause);
    }

    @isTest static void testGetIncludesAllListEmpty() {
        String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<String>()).toString();

        System.assertEquals('Id = null', includesClause);
    }

    @isTest static void testGetIncludesAllSet() {
        String includesClause = SoqlUtils.getIncludesAll('Something__c', new Set<String> {
            'value 1',
            'value 2',
            'value 3'
        }).toString();

        System.assertEquals('Something__c INCLUDES (value 1;value 2;value 3)', includesClause);
    }

    @isTest static void testGetIncludesAllIdList() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<Id> { id1, id2, id3 }).toString();

        System.assertEquals('Something__c INCLUDES (' + id1 + ';' + id2 + ';' + id3 + ')', includesClause);
    }

    @isTest static void testGetIncludesAllIdSet() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String includesClause = SoqlUtils.getIncludesAll('Something__c', new Set<Id> { id1, id2, id3 }).toString();

        System.assertEquals('Something__c INCLUDES (' + id1 + ';' + id2 + ';' + id3 + ')', includesClause);
    }

    @isTest static void testGetIncludesAnyList() {
        String includesClause = SoqlUtils.getIncludesAny('Something__c', new List<String> {
            'value 1',
            'value 2',
            'value 3'
        }).toString();

        System.assertEquals('Something__c INCLUDES (\'value 1\', \'value 2\', \'value 3\')', includesClause);
    }

    @isTest static void testGetIncludesAnyListSingle() {
        String includesClause = SoqlUtils.getIncludesAny('Something__c', new List<String> {
            'value 1'
        }).toString();

        System.assertEquals('Something__c INCLUDES (\'value 1\')', includesClause);
    }

    @isTest static void testGetIncludesAnyListEmpty() {
        String includesClause = SoqlUtils.getIncludesAny('Something__c', new List<String>()).toString();

        System.assertEquals('Id = null', includesClause);
    }

    @isTest static void testGetIncludesAnySet() {
        String includesClause = SoqlUtils.getIncludesAny('Something__c', new Set<String> {
            'value 1',
            'value 2',
            'value 3'
        }).toString();

        System.assertEquals('Something__c INCLUDES (\'value 1\', \'value 2\', \'value 3\')', includesClause);
    }

    @isTest static void testGetIncludesAnyIdList() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String includesClause = SoqlUtils.getIncludesAny('Something__c', new List<Id> { id1, id2, id3 }).toString();

        System.assertEquals('Something__c INCLUDES (\'' + id1 + '\', \'' + id2 + '\', \'' + id3 + '\')', includesClause);
    }

    @isTest static void testGetIncludesAnyIdSet() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();

        String includesClause = SoqlUtils.getIncludesAny('Something__c', new Set<Id> { id1, id2, id3 }).toString();

        System.assertEquals('Something__c INCLUDES (\'' + id1 + '\', \'' + id2 + '\', \'' + id3 + '\')', includesClause);
    }
    
    @isTest static void testQuery() {
        String query = SoqlUtils.getSelect(new List<String> {
                'Id',
                'Name'
            }, 'Product2')
            .withLimit(10)
            .withCondition(SoqlUtils.getIn('Id', SoqlUtils.getSelect(new List<String> { 'Product__c' }, 'Product_Attribute__c')))
            .withOffset(20)
            .withOrder(SoqlUtils.getOrder('Sort_Order__c').descending().withNullsLast())
            .toString();


        System.assertEquals('SELECT Id, Name FROM Product2 WHERE Id IN (SELECT Product__c FROM Product_Attribute__c) ORDER BY Sort_Order__c DESC NULLS LAST LIMIT 10 OFFSET 20', query);
    }

    @isTest static void testBooleans() {
        System.assertEquals('a = true', SoqlUtils.getEq('a', true).toString());
        System.assertEquals('a = false', SoqlUtils.getEq('a', false).toString());
        System.assertEquals('a != true', SoqlUtils.getNotEq('a', true).toString());
        System.assertEquals('a != false', SoqlUtils.getNotEq('a', false).toString());
    }

    @isTest static void testDatetimes() {
        System.assertEquals('My_Field__c = 2009-11-13T08:10:49Z', SoqlUtils.getEq('My_Field__c', Datetime.newInstanceGmt(2009, 11, 13, 8, 10, 49)).toString());
        System.assertEquals('My_Field__c != 2009-11-13T08:10:49Z', SoqlUtils.getNotEq('My_Field__c', Datetime.newInstanceGmt(2009, 11, 13, 8, 10, 49)).toString());
        System.assertEquals('My_Field__c < 2014-01-03T21:00:00Z', SoqlUtils.getLt('My_Field__c', Datetime.newInstanceGmt(2014, 1, 3, 21, 0, 0)).toString());
        System.assertEquals('My_Field__c > 2011-09-27T00:00:00Z', SoqlUtils.getGt('My_Field__c', Datetime.newInstanceGmt(2011, 9, 27, 0, 0, 0)).toString());
        System.assertEquals('My_Field__c <= 2007-04-29T13:10:49Z', SoqlUtils.getLe('My_Field__c', Datetime.newInstanceGmt(2007, 4, 29, 13, 10, 49)).toString());
        System.assertEquals('My_Field__c >= 2018-12-01T23:59:59Z', SoqlUtils.getGe('My_Field__c', Datetime.newInstanceGmt(2018, 12, 1, 23, 59, 59)).toString());
    }

    @isTest static void testNulls() {
        System.assertEquals('My_Field__c = null', SoqlUtils.getNull('My_Field__c').toString());
        System.assertEquals('My_Field__c != null', SoqlUtils.getNotNull('My_Field__c').toString());
    }

    @isTest static void testStar() {
        Set<String> star1 = new Set<String>(SoqlUtils.star(Product2.SObjectType));
        Set<String> star2 = new Set<String>(SoqlUtils.star('Product2'));

        // assert that both stars are the same
        System.assertEquals(star1.size(), star2.size());
        for (String fieldName: star1) {
            System.assert(star2.contains(fieldName));
        }

        // just assert some of the standard fields are returned. we'll get back custom fields too,
        // but asserting those would be inflexible
        System.assert(star1.contains('isactive'));
        System.assert(star1.contains('createdbyid'));
        System.assert(star1.contains('lastmodifiedbyid'));
        System.assert(star1.contains('productcode'));
        System.assert(star1.contains('description'));
        System.assert(star1.contains('family'));
        System.assert(star1.contains('name'));
        System.assert(star1.contains('id'));
    }
    
    @isTest static void testGetAnd() {
        Id id1 = getFakeProductId();
        Id id2 = getFakeProductId();
        Id id3 = getFakeProductId();
        
        String andClause = SoqlUtils.getAnd(new List<SoqlUtils.SoqlCondition> {         
            SoqlUtils.getOr(new List<SoqlUtils.SoqlCondition> {
                SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 123')}),
            SoqlUtils.getAnd(new List<SoqlUtils.SoqlCondition> {
                SoqlUtils.getEq('My_Field__c', '1'),
                SoqlUtils.getEq('Other_Field__c', '2')          
            }),
            SoqlUtils.getIn('Something__c', new List<Id> { id1, id2, id3 })
        }).toString();
        System.assertEquals(andClause, '((My_Field__c != \'Test Val\\\'ue 123\') AND (My_Field__c = \'1\' AND Other_Field__c = \'2\') AND Something__c IN (\''+id1+'\', \''+id2+'\', \''+id3+'\'))');
    }

    @isTest static void testGetAnd3Params() {
        SoqlUtils.SoqlCondition sc1 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 123');
        SoqlUtils.SoqlCondition sc2 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 456');
        SoqlUtils.SoqlCondition sc3 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 789');

        String andClause = SoqlUtils.getAnd(sc1, sc2, sc3).toString();
        System.assertEquals(andClause, '(My_Field__c != \'Test Val\\\'ue 123\' AND My_Field__c != \'Test Val\\\'ue 456\' AND My_Field__c != \'Test Val\\\'ue 789\')');
    }

    @isTest static void testGetAnd4Params() {
        SoqlUtils.SoqlCondition sc1 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 123');
        SoqlUtils.SoqlCondition sc2 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 456');
        SoqlUtils.SoqlCondition sc3 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 789');
        SoqlUtils.SoqlCondition sc4 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 000');

        String andClause = SoqlUtils.getAnd(sc1, sc2, sc3, sc4).toString();
        System.assertEquals(andClause, '(My_Field__c != \'Test Val\\\'ue 123\' AND My_Field__c != \'Test Val\\\'ue 456\' AND My_Field__c != \'Test Val\\\'ue 789\' AND My_Field__c != \'Test Val\\\'ue 000\')');
    }

    @isTest static void testGetAnd5Params() {
        SoqlUtils.SoqlCondition sc1 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 123');
        SoqlUtils.SoqlCondition sc2 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 456');
        SoqlUtils.SoqlCondition sc3 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 789');
        SoqlUtils.SoqlCondition sc4 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 000');
        SoqlUtils.SoqlCondition sc5 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 555');

        String andClause = SoqlUtils.getAnd(sc1, sc2, sc3, sc4, sc5).toString();
        System.assertEquals(andClause, '(My_Field__c != \'Test Val\\\'ue 123\' AND My_Field__c != \'Test Val\\\'ue 456\' AND My_Field__c != \'Test Val\\\'ue 789\' AND My_Field__c != \'Test Val\\\'ue 000\' AND My_Field__c != \'Test Val\\\'ue 555\')');
    }

    @isTest static void testGetOr3Params() {
        SoqlUtils.SoqlCondition sc1 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 123');
        SoqlUtils.SoqlCondition sc2 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 456');
        SoqlUtils.SoqlCondition sc3 = SoqlUtils.getNotEq('My_Field__c', 'Test Val\'ue 789');

        String orClause = SoqlUtils.getOr(sc1, sc2, sc3).toString();
        System.assertEquals(orClause, '(My_Field__c != \'Test Val\\\'ue 123\' OR My_Field__c != \'Test Val\\\'ue 456\' OR My_Field__c != \'Test Val\\\'ue 789\')');
    }

    @isTest static void testGetLtStringDate() {
        String dateStr = '2020-12-31';
        Date dte = Date.newInstance(2020, 12, 31);

        String ltClause = SoqlUtils.getLt(dateStr, dte).toString();
        System.assertEquals(ltClause, '2020-12-31 < 2020-12-31');
    }

    @isTest static void testGetLtStringDecimal() {
        String decimalStr = '1.12';
        Decimal decimalVal = 1.12;

        String ltClause = SoqlUtils.getLt(decimalStr, decimalVal).toString();
        System.assertEquals(ltClause, '1.12 < 1.12');
    }

    @isTest static void testGetGtStringDate() {
        String dateStr = '2020-12-31';
        Date dte = Date.newInstance(2020, 12, 31);

        String gtClause = SoqlUtils.getGt(dateStr, dte).toString();
        System.assertEquals(gtClause, '2020-12-31 > 2020-12-31');
    }

    @isTest static void testGetGtStringDecimal() {
        String decimalStr = '1.12';
        Decimal decimalVal = 1.12;

        String gtClause = SoqlUtils.getGt(decimalStr, decimalVal).toString();
        System.assertEquals(gtClause, '1.12 > 1.12');
    }

    @isTest static void testGetGeStringDate() {
        String dateStr = '2020-12-31';
        Date dte = Date.newInstance(2020, 12, 31);

        String geClause = SoqlUtils.getGe(dateStr, dte).toString();
        System.assertEquals(geClause, '2020-12-31 >= 2020-12-31');
    }

     @isTest static void testGetGeStringDecimal() {
        String decimalStr = '1.12';
        Decimal decimalVal = 1.12;

        String geClause = SoqlUtils.getGe(decimalStr, decimalVal).toString();
        System.assertEquals(geClause, '1.12 >= 1.12');
    }

    @isTest static void testGetLeStringDate() {
        String dateStr = '2020-12-31';
        Date dte = Date.newInstance(2020, 12, 31);

        String leClause = SoqlUtils.getLe(dateStr, dte).toString();
        System.assertEquals(leClause, '2020-12-31 <= 2020-12-31');
    }

    @isTest static void testGetLeStringDecimal() {
        String decimalStr = '1.12';
        Decimal decimalVal = 1.12;

        String leClause = SoqlUtils.getLe(decimalStr, decimalVal).toString();
        System.assertEquals(leClause, '1.12 <= 1.12');
    }

    @isTest static void testGetDateLeStringDate() {
        String dateStr = '2020-12-31T00.00.00Z';
        DateTime dte = DateTime.newInstance(2020, 12, 31, 0, 0, 0);

        String leClause = SoqlUtils.getDateLe(dateStr, dte).toString();
        System.assertEquals(leClause, '2020-12-31T00.00.00Z <= 2020-12-31');
    }

    @isTest static void testGetDateGtStringDate() {
        String dateStr = '2020-12-31T00.00.00Z';
        DateTime dte = DateTime.newInstance(2020, 12, 31, 0, 0, 0);

        String gtClause = SoqlUtils.getDateGt(dateStr, dte).toString();
        System.assertEquals(gtClause, '2020-12-31T00.00.00Z > 2020-12-31');
    }

    @isTest static void testGetEqStringDate() {
        String dateStr = '2020-12-31';
        Date dte = Date.newInstance(2020, 12, 31);

        String leClause = SoqlUtils.getEq(dateStr, dte).toString();
        System.assertEquals(leClause, '2020-12-31 = 2020-12-31');
    }

    @isTest static void testGetEqParam() {
        String dateStr = '2020-12-31';

        String eqParamClause = SoqlUtils.getEqParam(dateStr, 'startDate').toString();
        System.assertEquals(eqParamClause, '2020-12-31 = :startDate');
    }

    @isTest static void testGetEqStringDate_DateNull() {
        String dateStr = '2020-12-31';
        Date dte;

        String eqClause = SoqlUtils.getEq(dateStr, dte).toString();
        System.assertEquals(eqClause, '2020-12-31 = null');
    }

    @isTest static void testGetEqStringDecimal() {
        String decimalStr = '1.12';
        Decimal decimalVal = 1.12;

        String eqClause = SoqlUtils.getEq(decimalStr, decimalVal).toString();
        System.assertEquals(eqClause, '1.12 = 1.12');
    }

    @isTest static void testGetNotEqStringDecimal() {
        String decimalStr = '1.12';
        Decimal decimalVal = 1.12;

        String notEqClause = SoqlUtils.getNotEq(decimalStr, decimalVal).toString();
        System.assertEquals(notEqClause, '1.12 != 1.12');
    }

    @isTest static void testGetIn() {
        String fieldStr = 'sortOrder';
        List<Integer> numbers = new List<Integer>{ 1, 2, 3 };

        String inClause = SoqlUtils.getIn(fieldStr, numbers).toString();
        System.assertEquals(inClause, 'sortOrder IN (1, 2, 3)');
    }

    @isTest static void testConditionAlways() {
        System.assertEquals('SELECT Test FROM Table WHERE Id = null', SoqlUtils.getSelect(new List<String> { 'Test' }, 'Table').withCondition(
            SoqlUtils.getAnd(
                SoqlUtils.getIn('Field2', new List<String> { '1', '2', '3' }),
                SoqlUtils.getIn('Field1', new List<String>()))).toString());

        System.assertEquals('SELECT Test FROM Table WHERE (Field2 = \'5\')', SoqlUtils.getSelect(new List<String> { 'Test' }, 'Table').withCondition(
            SoqlUtils.getAnd(
                SoqlUtils.getEq('Field2', '5'),
                SoqlUtils.getNotIn('Field1', new List<String>()))).toString());

        System.assertEquals('SELECT Test FROM Table WHERE (Field2 IN (\'1\', \'2\', \'3\'))', SoqlUtils.getSelect(new List<String> { 'Test' }, 'Table').withCondition(
            SoqlUtils.getOr(
                SoqlUtils.getIn('Field2', new List<String> { '1', '2', '3' }),
                SoqlUtils.getIn('Field1', new List<String>()))).toString());

        System.assertEquals('SELECT Test FROM Table', SoqlUtils.getSelect(new List<String> { 'Test' }, 'Table').withCondition(
            SoqlUtils.getOr(
                SoqlUtils.getIn('Field2', new List<String> { '1', '2', '3' }),
                SoqlUtils.getNotIn('Field1', new List<String>()))).toString());
    }
}