@isTest
public class TestCommentController {
    
    static testMethod void methodTestCommentController() {
        
        RecordType rt=[SELECT Id FROM RecordType WHERE DeveloperName='Person_Account' AND SobjectType='Account' AND IsActive=true LIMIT 1];
        Account[] accounts=new Account[] {
            
            new Account(LastName='Test Account',RecordTypeId=rt.Id)
        };
        insert accounts;
        Intake__c[] intakes=new Intake__c[] {
            
            new Intake__c(Client__c=accounts[0].Id)
        };
        insert intakes;
        Questionnaire__c[] ques=new Questionnaire__c[] {
            
            new Questionnaire__c(Intake__c=intakes[0].Id)
        };
        insert ques;
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(intakes[0]);
        ApexPages.currentPage().getParameters().put('intakeId', intakes[0].Id);
        String url='/apex/IntakeForm?id='+intakes[0].Id+'&sfdc.override=1';
        ApexPages.currentPage().getParameters().put('retURL',url);
        //CommentController ec = new CommentController(sc);
        ApexPages.currentPage().getParameters().put('quesId', ques[0].Id);
        CommentController ec = new CommentController();
        ec.saveComment();
        ec.cancelComment();
        url='/apex/IntakeForm?id='+ques[0].Id+'&sfdc.override=1';
        ApexPages.currentPage().getParameters().put('retURL',url);
        //ec = new CommentController(sc);
        ec = new CommentController();
        ec.saveComment();
    }
}