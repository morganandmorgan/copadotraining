public with sharing class SpringCMImportTask {
	public SpringCMFolder Folder {get;set;}
	public string Href {get;set;}
	public string Message {get;set;}
	public string Status {get;set;}
}