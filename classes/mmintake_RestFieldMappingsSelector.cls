public class mmintake_RestFieldMappingsSelector
    extends mmlib_SObjectSelector
    implements mmintake_IRestFieldMappingsSelector
{
    public static mmintake_IRestFieldMappingsSelector newInstance()
    {
        return (mmintake_IRestFieldMappingsSelector) mm_Application.Selector.newInstance(IntakeRestFieldMapping__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return IntakeRestFieldMapping__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            IntakeRestFieldMapping__mdt.RequestField__c,
            IntakeRestFieldMapping__mdt.SObjectFieldName__c,
            IntakeRestFieldMapping__mdt.SObjectName__c,
            IntakeRestFieldMapping__mdt.Required__c
        };
    }

    public List<IntakeRestFieldMapping__mdt> selectAll()
    {
        return 
            Database.query(
                newQueryFactory()
                    .toSOQL() );
    }
}