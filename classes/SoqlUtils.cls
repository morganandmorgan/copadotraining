/**
 * Provides methods to generate dynamic SOQL using an object-oriented Builder pattern
 * inspired by the Hibernate criteria API.
 * @author brendan.conniff
 */
public with sharing class SoqlUtils {

    private static List<String> idsToStrings(List<Id> idList) {
        List<String> stringList = new List<String>();
        for (Id id: idList) {
            stringList.add(String.valueOf(id));
        }
        return stringList;
    }

    private static String fmtDatetime(Datetime dt) {
        return dt.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
    }

    private static String fmtDate(Datetime dt) {
        return dt.formatGmt('yyyy-MM-dd');
    }

    /**
     * Gets a list of field names in lowercase for the given type.
     * @param type  sObject type to find fields for
     * @return list of field names
     */
    public static List<String> star(Schema.SObjectType type) {
        return lcList(new List<String>(type.getDescribe().fields.getMap().keySet()));
    }

    /**
     * Gets a list of field names in lowercase for the given type.
     * Use the SObjectType version of this method where possible, since it does not
     * require a global describe.
     * @param typeName  name of sObject type to find fields for
     * @return list of field names
     */
    public static List<String> star(String typeName) {
        return star(Schema.getGlobalDescribe().get(typeName.toLowerCase()));
    }

    /**
     * Creates a new SELECT query.
     * @param fields  list of field names to query
     * @param table   sObject name to query from
     * @return  a SELECT query
     */
    public static SoqlQuery getSelect(List<String> fields, String table) {
        String soql = 'SELECT ';
        return new SoqlQuery('SELECT ' + String.join(fields, ', ') + ' FROM ' + table);
    }

    /**
     * Creates a new SELECT query for the aggregate function of all fields provided.
     * @param aggregateFunction the aggregate function to wrap the fields in
     * @param fields list of the field names to query (key is actual, value is alias)
     * @param table sObject name to query from
     * @return a new SELECT query for the aggregate function of all fields provided
     */
    public static SoqlQuery getSelectAggregate(String aggregateFunction, Map<String, String> fields, String table) {
        return getSelectAggregate(aggregateFunction, fields, table, NULL);
    }

    /**
     * Creates a new SELECT query for the aggregate function of all fields provided.
     * @param aggregateFunction the aggregate function to wrap the fields in
     * @param fields list of the field names to query (key is actual, value is alias)
     * @param table sObject name to query from
     * @param groupingField the field to group by (if specified)
     * @return a new SELECT query for the aggregate function of all fields provided
     */
    public static SoqlQuery getSelectAggregate(String aggregateFunction, Map<String, String> fields, String table, String groupingField) {
        // based on this class works, just reformat the field names
        List<String> sumFields = new List<String>();
        for (String field : fields.keySet()) {
            sumFields.add(aggregateFunction + '(' + field + ') ' + fields.get(field));
        }

        // if we have an aggregate grouping field, use it
        if (String.isNotBlank(groupingField)) {
            sumFields.add(groupingField + ' ROOT_FIELD');
        }

        // return using the core select
        return getSelect(sumFields, table);
    }

    /**
     * Creates a map of aggregate results based on a standard grouping field common to all rows.
     * @param resultSet the complete set of results to examine
     * @param groupingField the field that is unique to all rows
     * @return a map of aggregate results based on a standard grouping field common to all rows
     */
    public static Map<String, AggregateResult> getGroupedAggregates(AggregateResult[] resultSet, String groupingField) {
        Map<String, AggregateResult> segregated = new Map<String, AggregateResult>();
        for (AggregateResult next : resultSet) {
            segregated.put((String) next.get('ROOT_FIELD'), next);
        }
        return segregated;
    }

    /**
     * Creates a LIKE condition.
     * @param field  field name to test in the comparison
     * @param value  pattern to compare the field to
     * @return  a LIKE condition
     */
    public static SoqlCondition getLike(String field, String value) {
        return new SoqlCondition(field + ' LIKE \'' + String.escapeSingleQuotes(value) + '\'');
    }

    /**
     * Creates an AND condition by combining a list of child conditions.
     * @param conditions  list of conditions to combine with AND
     * @return  an AND condition
     */
    public static SoqlCondition getAnd(List<SoqlCondition> conditions) {
        List<SoqlCondition> keptConditions = new List<SoqlCondition>();
        Boolean anyAlwaysTrue = false;

        for (SoqlCondition condition: conditions) {
            if ( condition == null )
            {
                continue; // skip null conditions
            }

            if (condition.isAlwaysTrue()) {
                anyAlwaysTrue = true;
                continue; // skip conditions that are always true
            }
            if (condition.isAlwaysFalse()) {
                return new SoqlConditionAlwaysFalse(); // if any condition is always false, so is the AND
            }
            keptConditions.add(condition);
        }

        // if all conditions are always true, so is the AND
        if (keptConditions.isEmpty() && anyAlwaysTrue) {
            return new SoqlConditionAlwaysTrue();
        }

        String soql = '(';
        String sep = '';
        for (SoqlCondition condition: keptConditions) {
            soql += sep + condition.toString();
            sep = ' AND ';
        }
        return new SoqlCondition(soql+')');
    }

    /**
     * Creates an OR condition by combining a list of child conditions.
     * @param conditions  list of conditions to combine with OR
     * @return  an OR condition
     */
    public static SoqlCondition getOr(List<SoqlCondition> conditions) {
        List<SoqlCondition> keptConditions = new List<SoqlCondition>();
        Boolean anyAlwaysFalse = false;

        for (SoqlCondition condition: conditions) {
            if (condition.isAlwaysTrue()) {
                return new SoqlConditionAlwaysTrue(); // if any condition is always true, so is the OR
            }
            if (condition.isAlwaysFalse()) {
                anyAlwaysFalse = true;
                continue; // skip conditions that are always false
            }
            keptConditions.add(condition);
        }

        // if all conditions are always false, so is the OR
        if (keptConditions.isEmpty() && anyAlwaysFalse) {
            return new SoqlConditionAlwaysFalse();
        }

        String soql = '(';
        String sep = '';
        for (SoqlCondition condition: keptConditions) {
            soql += sep + condition.toString();
            sep = ' OR ';
        }
        return new SoqlCondition(soql+')');
    }

    /**
     * Creates an AND condition by combining child conditions.
     * @param c1  a condition to include in the AND
     * @param c2  a condition to include in the AND
     * @return  an AND condition
     */
    public static SoqlCondition getAnd(SoqlCondition c1, SoqlCondition c2) {
        return getAnd(new List<SoqlCondition> { c1, c2 });
    }

    /**
     * Creates an AND condition by combining child conditions.
     * @param c1  a condition to include in the AND
     * @param c2  a condition to include in the AND
     * @param c3  a condition to include in the AND
     * @return  an AND condition
     */
    public static SoqlCondition getAnd(SoqlCondition c1, SoqlCondition c2, SoqlCondition c3) {
        return getAnd(new List<SoqlCondition> { c1, c2, c3 });
    }

    /**
     * Creates an AND condition by combining child conditions.
     * @param c1  a condition to include in the AND
     * @param c2  a condition to include in the AND
     * @param c3  a condition to include in the AND
     * @param c4  a condition to include in the AND
     * @return  an AND condition
     */
    public static SoqlCondition getAnd(SoqlCondition c1, SoqlCondition c2, SoqlCondition c3, SoqlCondition c4) {
        return getAnd(new List<SoqlCondition> { c1, c2, c3, c4 });
    }

    /**
     * Creates an AND condition by combining child conditions.
     * @param c1  a condition to include in the AND
     * @param c2  a condition to include in the AND
     * @param c3  a condition to include in the AND
     * @param c4  a condition to include in the AND
     * @param c5  a condition to include in the AND
     * @return  an AND condition
     */
    public static SoqlCondition getAnd(SoqlCondition c1, SoqlCondition c2, SoqlCondition c3, SoqlCondition c4, SoqlCondition c5) {
        return getAnd(new List<SoqlCondition> { c1, c2, c3, c4, c5 });
    }

    /**
     * Creates an OR condition by combining child conditions.
     * @param c1  a condition to include in the OR
     * @param c2  a condition to include in the OR
     * @return  an OR condition
     */
    public static SoqlCondition getOr(SoqlCondition c1, SoqlCondition c2) {
        return getOr(new List<SoqlCondition> { c1, c2 });
    }

    /**
     * Creates an OR condition by combining child conditions.
     * @param c1  a condition to include in the OR
     * @param c2  a condition to include in the OR
     * @param c3  a condition to include in the OR
     * @return  an OR condition
     */
    public static SoqlCondition getOr(SoqlCondition c1, SoqlCondition c2, SoqlCondition c3) {
        return getOr(new List<SoqlCondition> { c1, c2, c3 });
    }

    /**
     * Creates a less-than condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than condition
     */
    public static SoqlCondition getLt(String field, Datetime dt) {
        return new SoqlCondition(field + ' < ' + fmtDatetime(dt));
    }

    /**
     * Creates a less-than condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than condition
     */
    public static SoqlCondition getLt(String field, Date dt) {
        return new SoqlCondition(field + ' < ' + fmtDate(dt));
    }

    /**
     * Creates a less-than condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than condition
     */
    public static SoqlCondition getLt(String field, Decimal value) {
        return new SoqlCondition(field + ' < ' + value);
    }

    /**
     * Creates a greater-than condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than condition
     */
    public static SoqlCondition getGt(String field, Datetime dt) {
        return new SoqlCondition(field + ' > ' + fmtDatetime(dt));
    }

    /**
     * Creates a greater-than condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than condition
     */
    public static SoqlCondition getGt(String field, Decimal value) {
        return new SoqlCondition(field + ' > ' + value);
    }

    /**
     * Creates a greater-than condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than condition
     */
    public static SoqlCondition getGt(String field, Date dt) {
        return new SoqlCondition(field + ' > ' + fmtDate(dt));
    }

    /**
     * Creates a greater-than equal to condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than equal condition
     */
    public static SoqlCondition getGe(String field, Date dt) {
        return new SoqlCondition(field + ' >= ' + fmtDate(dt));
    }

    /**
     * Creates a less-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than or equal condition
     */
    public static SoqlCondition getLe(String field, Datetime dt) {
        return new SoqlCondition(field + ' <= ' + fmtDatetime(dt));
    }

    /**
     * Creates a less-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than or equal condition
     */
    public static SoqlCondition getLe(String field, Date dt) {
        return new SoqlCondition(field + ' <= ' + fmtDate(dt));
    }

    /**
     * Creates a less-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than or equal condition
     */
    public static SoqlCondition getLe(String field, Decimal value) {
        return new SoqlCondition(field + ' <= ' + value);
    }

    /**
     * Creates a greater-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than or equal condition
     */
    public static SoqlCondition getGe(String field, Datetime dt) {
        return new SoqlCondition(field + ' >= ' + fmtDatetime(dt));
    }

    /**
     * Creates a greater-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than or equal condition
     */
    public static SoqlCondition getGe(String field, Decimal value) {
        return new SoqlCondition(field + ' >= ' + value);
    }

    /**
     * Creates a less-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a less-than or equal condition
     */
    public static SoqlCondition getDateLe(String field, DateTime dt) {
        return new SoqlCondition(field + ' <= ' + fmtDate(dt));
    }

    /**
     * Creates a greater-than or equal condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  a greater-than or equal condition
     */
    public static SoqlCondition getDateGt(String field, DateTime dt) {
        return new SoqlCondition(field + ' > ' + fmtDate(dt));
    }

    /**
     * Creates an equality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getEq(String field, Datetime dt) {
        if(dt == null) {
            return getNull(field);
        }
        return new SoqlCondition(field + ' = ' + fmtDatetime(dt));
    }

        /**
     * Creates an equality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getEq(String field, Date dt) {
        if(dt == null) {
            return getNull(field);
        }
        return new SoqlCondition(field + ' = ' + fmtDate(dt));
    }

    public static SoqlCondition getEqParam(String field, String paramName) {
        return new SoqlCondition(field + ' = :' + paramName);
    }

    /**
     * Creates an inequality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an inequality condition
     */
    public static SoqlCondition getNotEq(String field, Datetime dt) {
        return new SoqlCondition(field + ' != ' + fmtDatetime(dt));
    }

    /**
     * Creates an equality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getEq(String field, Boolean value) {
        return new SoqlCondition(field + ' = ' + String.valueOf(value));
    }

    /**
     * Creates an equality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getEq(String field, String value) {
        return new SoqlCondition(field + ' = \'' + String.escapeSingleQuotes(value) + '\'');
    }

    /**
     * Creates an equality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getEq(String field, Id value) {
        if (value == null) {
            return new SoqlCondition(field + ' = null');
        } else {
            return new SoqlCondition(field + ' = \'' + String.valueOf(value) + '\'');
        }
    }

    /**
     * Creates an equality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getEq(String field, Decimal value) {
        return new SoqlCondition(field + ' = ' + String.valueOf(value));
    }
    /**
     * Creates an inequality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an equality condition
     */
    public static SoqlCondition getNotEq(String field, Decimal value) {
        return new SoqlCondition(field + ' != ' + String.valueOf(value));
    }

    /**
     * Creates an inequality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an inequality condition
     */
    public static SoqlCondition getNotEq(String field, Boolean value) {
        return new SoqlCondition(field + ' != ' + String.valueOf(value));
    }

    /**
     * Creates an inequality condition.
     * @param field  field name to test in the comparison
     * @param value  value to compare the field to
     * @return  an inequality condition
     */
    public static SoqlCondition getNotEq(String field, String value) {
        return new SoqlCondition(field + ' != \'' + String.escapeSingleQuotes(value) + '\'');
    }

    /**
     * Creates an order clause. By default the order applied to the field is ascending, with nulls first.
     * @param field  field to order the results by
     * @return  an order clause
     */
    public static SoqlOrder getOrder(String field) {
        return new SoqlOrder(field);
    }

    /**
     * Creates an IN condition to test if a field's value is contained by a subquery. The subquery must
     * abide by Salesforce's usual rules for semi-join subqueries (for example, the subquery must be querying
     * a primary or foreign key field).
     * @param field     field name to test in the condition
     * @param subquery  subquery to match field values in
     * @return  an IN condition
     */
    public static SoqlCondition getIn(String field, SoqlQuery subquery) {
        return new SoqlCondition(field + ' IN (' + subquery.toString() + ')');
    }

    /**
     * Creates an IN condition to test if a field's value is contained by a Set.
     * @param field   field name to test in the condition
     * @param values  set to match field values in
     * @return  an IN condition
     */
    public static SoqlCondition getIn(String field, Set<String> values) {
        return getIn(field, new List<String>(values));
    }

    /**
     * Creates an IN condition to test if a field's value is contained by a List.
     * @param field   field name to test in the condition
     * @param values  list to match field values in
     * @return  an IN condition
     */
    public static SoqlCondition getIn(String field, List<String> values) {
        if (values.isEmpty()) {
            return new SoqlConditionAlwaysFalse();
        }
        String sep = '';
        String escaped = '';
        for (String value: values) {
            escaped += sep + (value == null ? 'null' : ('\'' + String.escapeSingleQuotes(value) + '\''));
            sep = ', ';
        }
        return new SoqlCondition(field + ' IN (' + escaped + ')');
    }

    /**
     * Creates an IN condition to test if a field's value is contained by a List.
     * Accepts a list of integers and refrains from wrapping each value in quotes.
     * @param field   field name to test in the condition
     * @param values  list to match field values in
     * @return  an IN condition
     */
    public static SoqlCondition getIn(String field, List<Integer> values) {
        if (values.isEmpty()) {
            return new SoqlConditionAlwaysFalse();
        }
        String sep = '';
        String escaped = '';
        for (Integer value: values) {
            escaped += sep + (value == null ? 'null' : String.valueOf(value));
            sep = ', ';
        }
        return new SoqlCondition(field + ' IN (' + escaped + ')');
    }

    /**
     * Creates an IN condition to test if a field's value is contained by a Set.
     * @param field   field name to test in the condition
     * @param values  set to match field values in
     * @return  an IN condition
     */
    public static SoqlCondition getIn(String field, Set<Id> values) {
        return getIn(field, new List<Id>(values));
    }

    /**
     * Creates an IN condition to test if a field's value is contained by a List.
     * @param field   field name to test in the condition
     * @param values  list to match field values in
     * @return  an IN condition
     */
    public static SoqlCondition getIn(String field, List<Id> values) {
        return getIn(field, idsToStrings(values));
    }

    /**
     * Creates a NOT IN condition to test if a field's value is contained by a subquery. The subquery must
     * abide by Salesforce's usual rules for semi-join subqueries (for example, the subquery must be querying
     * a primary or foreign key field).
     * @param field     field name to test in the condition
     * @param subquery  subquery to exclude field values from
     * @return  an NOT IN condition
     */
    public static SoqlCondition getNotIn(String field, SoqlQuery subquery) {
        return new SoqlCondition(field + ' NOT IN (' + subquery.toString() + ')');
    }

    /**
     * Creates a NOT IN condition to test if a field's value is not contained by a List.
     * @param field   field name to test in the condition
     * @param values  list to exclude field values from
     * @return  an NOT IN condition
     */
    public static SoqlCondition getNotIn(String field, List<String> values) {
        if (values.isEmpty()) {
            return new SoqlConditionAlwaysTrue();
        }
        String sep = '';
        String escaped = '';
        for (String value: values) {
            escaped += sep + (value == null ? 'null' : ('\'' + String.escapeSingleQuotes(value) + '\''));
            sep = ', ';
        }
        return new SoqlCondition(field + ' NOT IN (' + escaped + ')');
    }

    /**
     * Creates a NOT IN condition to test if a field's value is not contained by a Set.
     * @param field   field name to test in the condition
     * @param values  set to exclude field values from
     * @return  an NOT IN condition
     */
    public static SoqlCondition getNotIn(String field, Set<String> values) {
        return getNotIn(field, new List<String>(values));
    }

    /**
     * Creates a NOT IN condition to test if a field's value is not contained by a List.
     * @param field   field name to test in the condition
     * @param values  list to exclude field values from
     * @return  an NOT IN condition
     */
    public static SoqlCondition getNotIn(String field, List<Id> values) {
        return getNotIn(field, idsToStrings(values));
    }

    /**
     * Creates a NOT IN condition to test if a field's value is not contained by a Set.
     * @param field   field name to test in the condition
     * @param values  set to exclude field values from
     * @return  an NOT IN condition
     */
    public static SoqlCondition getNotIn(String field, Set<Id> values) {
        return getNotIn(field, new List<Id>(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains all the values in the given Set.
     * @param field   field name to test in the condition
     * @param values  set of values to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAll(String field, Set<String> values) {
        return getIncludesAll(field, new List<String>(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains all the Ids in the given Set.
     * @param field   field name to test in the condition
     * @param values  set of Ids to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAll(String field, Set<Id> values) {
        return getIncludesAll(field, new List<Id>(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains all the Ids in the given List.
     * @param field   field name to test in the condition
     * @param values  list of Ids to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAll(String field, List<Id> values) {
        return getIncludesAll(field, idsToStrings(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains all the given values.
     * @param field   field name to test in the condition
     * @param values  list of values to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAll(String field, List<String> values) {
        if (values.isEmpty()) {
            return new SoqlConditionAlwaysFalse();
        }
        String sep = '';
        String escaped = '';
        for (String value : values) {
            escaped += sep + String.escapeSingleQuotes(value);
            sep = ';';
        }
        return new SoqlCondition(field + ' INCLUDES (' + escaped + ')');
    }

    /**
     * Creates an INCLUDES condition to test if a field contains any of the values in the given Set.
     * @param field   field name to test in the condition
     * @param values  set of values to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAny(String field, Set<String> values) {
        return getIncludesAny(field, new List<String>(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains any of the Ids in the given Set.
     * @param field   field name to test in the condition
     * @param values  set of Ids to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAny(String field, Set<Id> values) {
        return getIncludesAny(field, new List<Id>(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains any of the Ids in the given List.
     * @param field   field name to test in the condition
     * @param values  list of Ids to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAny(String field, List<Id> values) {
        return getIncludesAny(field, idsToStrings(values));
    }

    /**
     * Creates an INCLUDES condition to test if a field contains any of the given values.
     * @param field   field name to test in the condition
     * @param values  list of values to check for
     * @return  an INCLUDES condition
     */
    public static SoqlCondition getIncludesAny(String field, List<String> values) {
        if (values.isEmpty()) {
            return new SoqlConditionAlwaysFalse();
        }
        String sep = '';
        String escaped = '';
        for (String value : values) {
            if (value == null) {
                escaped += sep + 'null';
            } else {
                escaped += sep + '\'' + String.escapeSingleQuotes(value) + '\'';
            }
            sep = ', ';
        }
        return new SoqlCondition(field + ' INCLUDES (' + escaped + ')');
    }

    /**
     * Creates a condition to test if a field's value is null
     * @param field   field name to test in the condition
     * @return  an "is null" condition
     */
    public static SoqlCondition getNull(String field) {
        return new SoqlCondition(field + ' = null');
    }

    /**
     * Creates a condition to test if a field's value is NOT null
     * @param field   field name to test in the condition
     * @return  an "is not null" condition
     */
    public static SoqlCondition getNotNull(String field) {
        return new SoqlCondition(field + ' != null');
    }

    public static SoqlQuery getRelationshipSelect(List<String> fields, String relationship) {
        return new SoqlRelationshipQuery(getSelect(fields, relationship).toString());
    }

    /**
     * Provides methods to generate dynamic SOQL queries using an object-oriented Builder pattern
     * inspired by the Hibernate criteria API. A SoqlQuery can't be instantiated directly, but can
     * be created using the factory method <code>SoqlUtils.getSelect(...)</code>.
     * @author brendan.conniff
     * @see SoqlUtils.getSelect
     */
    public virtual class SoqlQuery {
        private final String q;
        private SoqlCondition whereClause;
        private SoqlOrder orderClause;
        private Integer limitValue;
        private Integer offsetValue;

        private SoqlQuery(String q) {
            this.q = q;
        }

        /**
         * Sets the WHERE clause of this query to check for the given condition.
         * @param whereClause  condition to use in WHERE clause for this query
         * @return this SoqlQuery
         */
        public SoqlQuery withCondition(SoqlCondition whereClause) {
            this.whereClause = whereClause;
            return this;
        }

        /**
         * Sets the ORDER BY clause of this query to the given order.
         * @param orderClause  order to use in the ORDER BY clause for this query
         * @return this SoqlQuery
         */
        public SoqlQuery withOrder(SoqlOrder orderClause) {
            this.orderClause = orderClause;
            return this;
        }

        /**
         * Sets the LIMIT value of this query to the given number.
         * @param limitValue  value of the LIMIT clause to use for this query
         * @return this SoqlQuery
         */
        public SoqlQuery withLimit(Integer limitValue) {
            this.limitValue = limitValue;
            return this;
        }

        /**
         * Sets the OFFSET value of this query to the given number.
         * @param offsetValue  value of the OFFSET clause to use for this query
         * @return this SoqlQuery
         */
        public SoqlQuery withOffset(Integer offsetValue) {
            this.offsetValue = offsetValue;
            return this;
        }

        /**
         * Retrieves the SOQL for this query as a String, suitible for passing to <code>Database.query(...)</code>.
         * @return the SOQL String for this query.
         */
        public virtual override String toString() {
            String soql = q;
            if (whereClause != null && !whereClause.isAlwaysTrue()) {
                soql += ' WHERE '+whereClause.toString();
            }
            soql += (orderClause == null ? '' : ' ORDER BY '+orderClause.toString());
            soql += (limitValue == null ? '' : ' LIMIT '+limitValue);
            soql += (offsetValue == null ? '' : ' OFFSET '+offsetValue);
            System.debug('[SoqlUtils] ' + soql);
            return soql;
        }
    }

    public class SoqlRelationshipQuery extends SoqlQuery {
        public SoqlRelationshipQuery(String q) {
            super(q);
        }

        public override String toString() {
            return '(' + super.toString() + ')';
        }
    }

    /**
     * Represents a condition which may be used as part of a SOQL WHERE clause. A SoqlCondition can't be
     * instantiated directly, but can be created using any of the factory methods in SoqlUtils with a return
     * type of SoqlCondition.
     * @author brendan.conniff
     */
    public virtual class SoqlCondition {
        private final String q;

        private SoqlCondition(String q) {
            this.q = q;
        }

        public virtual Boolean isAlwaysTrue() {
            return false;
        }

        public virtual Boolean isAlwaysFalse() {
            return false;
        }

        /**
         * Converts this condition to the SOQL String it represents.
         * This should not be used directly, as it will not generate a full, valid query.
         * @return the SOQL String for this condition
         */
        public override String toString() {
            return q;
        }
    }

    private class SoqlConditionAlwaysTrue extends SoqlCondition {
        private SoqlConditionAlwaysTrue() {
            super('Id != null');
        }

        public override Boolean isAlwaysTrue() {
            return true;
        }
    }

    private class SoqlConditionAlwaysFalse extends SoqlCondition {
        private SoqlConditionAlwaysFalse() {
            super('Id = null');
        }

        public override Boolean isAlwaysFalse() {
            return true;
        }
    }

    /**
     * Represents an ordering which may be used as part of a SOQL ORDER BY clause. A SoqlOrder can't be
     * instantiated directly, but can be created using the factory method <code>SoqlUtils.getOrder(...)</code>.
     * @author brendan.conniff
     */
    public class SoqlOrder {
        private final String q;
        private boolean isAsc = null;
        private boolean isNullsFirst = null;
        private SoqlOrder nextOrder;

        private SoqlOrder firstOrder {
            get {
                return firstOrder == null ? this : firstOrder;
            }
            set;
        }

        private SoqlOrder(String q) {
            this.q = q;
        }

        private SoqlOrder(SoqlOrder firstOrder, String q) {
            this.firstOrder = firstOrder;
            this.q = q;
        }

        /**
         * Sets this order to be ascending.
         * @return this SoqlOrder
         */
        public SoqlOrder ascending() {
            this.isAsc = true;
            return this;
        }

        /**
         * Sets this order to be descending.
         * @return this SoqlOrder
         */
        public SoqlOrder descending() {
            this.isAsc = false;
            return this;
        }

        /**
         * Sets this order to return null values first.
         * @return this SoqlOrder
         */
        public SoqlOrder withNullsFirst() {
            this.isNullsFirst = true;
            return this;
        }

        /**
         * Sets this order to return null values last.
         * @return this SoqlOrder
         */
        public SoqlOrder withNullsLast() {
            this.isNullsFirst = false;
            return this;
        }

        /**
         * Adds another field to order by after this.
         * @param next  name of next field to order by
         * @return next SoqlOrder
         */
        public SoqlOrder thenBy(String next) {
            SoqlOrder nextOrder = new SoqlOrder(firstOrder, next);
            this.nextOrder = nextOrder;
            return nextOrder;
        }

        private String chainToString() {
            String soql = q;
            if (isAsc != null) {
                soql += (isAsc ? ' ASC' : ' DESC');
            }
            if (isNullsFirst != null) {
                soql += (isNullsFirst ? ' NULLS FIRST' : ' NULLS LAST');
            }

            if (nextOrder != null) {
                soql += ', ' + nextOrder.chainToString();
            }

            return soql;
        }

        /**
         * Converts this order to the SOQL String it represents.
         * This should not be used directly, as it will not generate a full, valid query.
         * @return the SOQL String for this order
         */
        public override String toString() {
            return this == firstOrder
                ? chainToString()
                : firstOrder.toString();
        }
    }

    private static List<String> lcList(List<String> inputList) {
        List<String> outputList = new List<String>();
        for (String inputStr: inputList) {
            outputList.add(inputStr.toLowerCase());
        }
        return outputList;
    }
}