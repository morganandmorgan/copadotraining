/**
 *  mmcommon_PersonAccountsServiceImpl
 */
public with sharing class mmcommon_PersonAccountsServiceImpl
    implements mmcommon_IPersonAccountsService
{
    public List<Account> searchAccounts( String firstName, String lastName, String email, String phoneNum, Date birthDate)
    {
        return searchAccounts_Implementation(firstName, lastName, email, phoneNum, birthDate, false);
    }

    public List<Account> searchAccountsUsingHighPrecision(String firstName, String lastName, String email, String phoneNum, Date birthDate)
    {
        return searchAccounts_Implementation(firstName, lastName, email, phoneNum, birthDate, true);
    }

    private List<Account> searchAccounts_Implementation
    (
        String firstName,
        String lastName,
        String email,
        String phoneNum,
        Date birthDate,
        Boolean isHighPrecisionSearch
    )
    {

        mmcommon_IAccountsSelectorPersonRequest selectorRequest = mmcommon_AccountsSelectorPersonRequest.newInstance()
                                                                    .setFirstName( firstName )
                                                                    .setLastName( lastName )
                                                                    .setEmail( email )
                                                                    .setPhoneNumber( phoneNum )
                                                                    .setBirthDate( birthDate );

        list<Account> records = mmcommon_AccountsSelector.newInstance().selectByPersonFieldRequest( selectorRequest );

        List<Account> orderedRecords = new mmcommon_AccountsRelevancyOrganizer()
                                                            .setRecordsToSort( records )
                                                            .setRequestCriteria( selectorRequest )
                                                            .setHighPrecisionFlag(isHighPrecisionSearch)
                                                            .filterAndSort();

        return orderedRecords;
    }

    public List<Account> searchAccountsUsingFuzzyMatching(Map<String, String> data)
    {
        cleanseAndValidateForFuzzyMatch(data);

        if (dataIsEmpty(data))
        {
            return new List<Account> {};
        }

        mmcommon_IAccountsSelectorPersonRequest req = mmcommon_AccountsSelectorPersonRequest.newInstance()
                                                            .setFirstName(data.get('firstName'))
                                                            .setLastName(data.get('lastName'))
                                                            .setEmail(data.get('email'))
                                                            .setPhoneNumber(data.get('phoneNumber'))
                                                            .setBirthDate(
                                                                String.isNotBlank(data.get('birthDate')) ?
                                                                Date.parse(data.get('birthDate')) :
                                                                null)
                                                            .setSsn_Last4(data.get('ssn'));

        List<Account> records = mmcommon_AccountsSelector.newInstance().selectByFuzzyMatching(req);

        List<Account> orderedRecords = new mmcommon_AccountsRelevancyOrganizer()
                                                            .setRecordsToSort( records )
                                                            .setRequestCriteria( req )
                                                            .setHighPrecisionFlag( false )
                                                            .filterAndSort();

        records = new List<Account>();
        // only care about the top 10 matching results for fuzzy search (person search component)
        for (Integer i = 0; i < Math.min(orderedRecords.size(), 10); i++) {
            records.add(orderedRecords[i]);
        }
        return records;
    }

    public void saveRecords( list<Account> records )
    {
        if ( records != null
            && ! records.isEmpty() )
        {
            try
            {
                mmcommon_Accounts accounts = new mmcommon_Accounts(records);

                accounts.validate();

                mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

                uow.register( records );

                uow.commitWork();
            }
            catch ( mmcommon_AccountsDomainExceptions.ValidationException ve )
            {
                throw new mmcommon_PersonAccountsServiceExceptions.ValidationException( ve );
            }
            catch (System.DMLException dmle)
            {
                throw new mmcommon_PersonAccountsServiceExceptions.ServiceException( dmle );
            }
        }
    }

    public void geocodeAddresses(Set<Id> idSet)
    {
        if (idSet == null || idSet.isEmpty())
        {
            return;
        }

        try
        {
            mmcommon_IAccountsSelector selector = mmcommon_AccountsSelector.newInstance();

            //if there is only one record, then there is no need to go to a batch operation mode
            if ( idSet.size() == 1 )
            {
                new mmgmap_GoogleGeoCodingExecutor().run( selector.selectById( idSet ) );
            }
            else
            {
                Integer calloutLimit = System.Limits.getLimitCallouts();
                Integer numberOfCalloutsPerRecord = mmgmap_GoogleGeoCodingExecutor.getNumberOfCalloutsPerRecord();
                Integer recordsPerBatch = Integer.valueOf(Math.floor(calloutLimit/numberOfCalloutsPerRecord));

                new mmlib_GenericBatch(
                    mmgmap_GoogleGeoCodingExecutor.class,
                    selector.selectSObjectsByIdQuery(),
                    idSet)
                .setBatchSizeTo(recordsPerBatch)
                .execute();
            }
        }
        catch (Exception e)
        {
            throw new mmcommon_PersonAccountsServiceExceptions.ServiceException(e.getMessage(), e);
        }
    }

    private static void cleanseAndValidateForFuzzyMatch(Map<String, String> data)
    {
        if (data == null || data.isEmpty())
        {
            return;
        }

        for (String k : data.keySet())
        {
            String v = data.get(k);

            if (String.isBlank(v))
            {
                continue;
            }

            if ('firstName'.equalsIgnoreCase(k) || 'lastName'.equalsIgnoreCase(k))
            {
                data.put(k, mmlib_Utils.clean(v));
            }
            else if ('phoneNumber'.equalsIgnoreCase(k))
            {
                v = mmlib_Utils.clean_DigitsOnly(v);

                if (10 == v.length())
                {
                    data.put(k, v);
                }
                else
                {
                    data.put(k, '');
                }
            }
            else if ('email'.equalsIgnoreCase(k))
            {
                data.put(k, mmlib_Utils.clean(v));
            }
            else if ('ssn'.equalsIgnoreCase(k))
            {
                v = mmlib_Utils.clean_DigitsOnly(v);

                if (4 == v.length())
                {
                    data.put(k, v);
                }
                else
                {
                    data.put(k, '');
                }
            }
            else if ('birthDate'.equalsIgnoreCase(k))
            {
                try
                {
                    data.put(k, Date.parse(v).format());
                }
                catch (Exception exc)
                {
                    data.put(k, null);
                }
            }
        }
    }

    private static Boolean dataIsEmpty(Map<String, String> data)
    {
        if (data == null && data.isEmpty())
        {
            return true;
        }

        for(String s : data.values())
        {
            if (String.isNotBlank(s))
            {
                return false;
            }
        }

        return true;
    }
}