/**
 * AuditScheduledFlagDomain
 * @description Domain class for Audit_Scheduled_Flag__c SObject.
 * @author Matt Terrill
 * @date 9/20/2019
 */
public without sharing class AuditScheduledFlagDomain extends fflib_SObjectDomain {

    private List<Audit_Scheduled_Flag__c> auditSFlags;

    public AuditScheduledFlagDomain() {
        super();
        this.auditSFlags = new List<Audit_Scheduled_Flag__c>();
    } //constructor


    public void createFromResults(List<AuditEvalEngine.EvaluationResult> results, String relatedIdField, fflib_SObjectUnitOfWork uow, Boolean checkForExisting) {

        //the results should be critera met and scheduled
        AuditScheduledFlagSelector asfs = new AuditScheduledFlagSelector();

        //find already existing flags
        List<Audit_Scheduled_Flag__c> existingFlags;
        if (checkForExisting) {
            existingFlags = asfs.selectByResultsPending(results, relatedIdField);
        }

        for (AuditEvalEngine.EvaluationResult result : results) {

            Boolean alreadyExists = false;
            if (checkForExisting) {
                //look for a matching already existing scheduled flag
                for (Audit_Scheduled_Flag__c existingFlag : existingFlags) {
                    if ( existingFlag.audit_rule__c == result.rule.auditRule.id && existingFlag.get(relatedIdField) == result.obj.get('id') ) {
                        alreadyExists = true;
                    }
                }
            } //checkForExisting

            //create a scheduled flag if we didn't find one
            if ( !alreadyExists ) {

                DateTime scheduledDate = DateTime.now();
                //check for a custom schedule date on the rule
                if ( result.rule.auditRule.custom_scheduled_date_field__c != null && result.rule.auditRule.custom_scheduled_date_field__c != '') {
                    if ( result.obj.get(result.rule.auditRule.custom_scheduled_date_field__c) instanceOf Date) {
                        //let's use noon, so that different time-zones will not end up on the wrong day, if we use midnight
                        scheduledDate = DateTime.newInstance((Date)result.obj.get(result.rule.auditRule.custom_scheduled_date_field__c), Time.newInstance(12,0,0,0));
                    } else {
                        scheduledDate = (DateTime)result.obj.get(result.rule.auditRule.custom_scheduled_date_field__c);
                    }
                }

                Id userToFlag = AuditEvalEngine.determinWhoToFlag(result);

                //if we have a scheduled date (not null) and someone to flag (not null)
                if (scheduledDate != null && userToFlag != null) {
                    Audit_Scheduled_Flag__c auditFlag = new Audit_Scheduled_Flag__c();

                    auditFlag.scheduled_date__c = scheduledDate.addDays(Integer.valueOf(result.rule.auditRule.days__c));

                    auditFlag.status__c = 'Pending';
                    auditFlag.audit_rule__c = result.rule.auditRule.id;
                    auditFlag.user__c = userToFlag;

                    if ( relatedIdField != null) {
                        auditFlag.put(relatedIdField, result.obj.get('Id'));
                    }

                    uow.registerNew(auditFlag);
                } //if scheduledDate not null
            } //if alreadyExists
        } //for results

    } //createFromResults


    public void updateStatusFromResults(List<AuditEvalEngine.EvaluationResult> results, String relatedIdField, String status, fflib_SObjectUnitOfWork uow) {
        AuditScheduledFlagSelector asfs = new AuditScheduledFlagSelector();
        //we'll go through matching scheduled flags and satisfy any we find
        for (Audit_Scheduled_Flag__c scheduledFlag : asfs.selectByResultsPending(results, relatedIdField) ) {
            scheduledFlag.status__c = status;
            uow.registerDirty(scheduledFlag);
        } //for matches
    } //updateStatusFromResults


    public void voidOrphanedFlags() {

        AuditScheduledFlagSelector selector = new AuditScheduledFlagSelector();
        AuditEvalEngine engine = new AuditEvalEngine();

        List<Audit_Scheduled_Flag__c> voided = new List<Audit_Scheduled_Flag__c>();
        //loop over the flags that need re-evaluated
        for (Audit_Scheduled_Flag__c scheduledFlag : selector.selectNeedsReEvaluation() ) {
            //get the name of the related object field
            String relatedFieldName = engine.fixRelatedFieldName(scheduledFlag.audit_rule__r.object__c);
            //check to see if the field is null, and the scheduled flag is orphaned
            if (scheduledFlag.get(relatedFieldName) == null) {
                scheduledFlag.status__c = 'Void';
                voided.add(scheduledFlag);
            }
        } //for

        update voided;

    } //voidOrphanedFlags

} //class