public with sharing class mmdocusign_RestApiCalloutAuthorization
	implements mmlib_ICalloutAuthorizationable
{
	public void setAuthorization(HttpRequest request)
	{
		String authorizationJson =
			'{"Username": "' + mmdocusign_ConfigurationSettings.getUsername() + '", ' +
			'"Password": "' + mmdocusign_ConfigurationSettings.getPassword() + '", ' +
			'"IntegratorKey": "' + mmdocusign_ConfigurationSettings.getIntegratorKey() + '"}';

		request.setHeader('X-DocuSign-Authentication', authorizationJson);
	}
}