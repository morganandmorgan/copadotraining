/**
 *  script used to sort failed expense migration attempts from x_CPTimeslipStaging__c for easier processing.
 *
 *  @usage String query = 'select id, name, MigrationStatus__c, MigratedTime__c, ProcessingAnomalyComments__c from x_CPTimeslipStaging__c where MigrationStatus__c = \'Failed\'';
 *         new mmlib_GenericBatch( xxx_CPTimeSlipStageResultSortExecutor.class, query).setBatchSizeTo(1000).execute();
 */
public class xxx_CPTimeSlipStageResultSortExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        x_CPTimeslipStaging__c workingRecord = null;

        list<x_CPTimeslipStaging__c> recordsToUpdate = new list<x_CPTimeslipStaging__c>();

        for ( SObject record : scope )
        {
            workingRecord = (x_CPTimeslipStaging__c)record;

            if ( workingRecord.ProcessingAnomalyComments__c.contains('Matter with Litify_Load_Id__c of ') )
            {
                workingRecord.MigrationStatus__c = 'Failed - NORELATEDMATTER';
                recordsToUpdate.add( workingRecord );
                continue;
            }
            if ( workingRecord.ProcessingAnomalyComments__c.contains('User with email') )
            {
                workingRecord.MigrationStatus__c = 'Failed - NOACTIVEEMPLOYEE';
                recordsToUpdate.add( workingRecord );
                continue;
            }
        }

        update recordsToUpdate;
    }
}