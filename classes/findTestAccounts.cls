public class findTestAccounts {
    private final List<Account> accounts;
    public findTestAccounts() {
        accounts = [select Name from Account where Name LIKE 'test_%'];
    }
    public List<Account> getTestAccounts() {
        return accounts;
    }
}