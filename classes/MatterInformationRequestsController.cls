public with sharing class MatterInformationRequestsController {

    @AuraEnabled
    public static List<litify_pm__Request__c> fetchRecords(String recordId) {
        List<litify_pm__Request__c> recordsList = new List<litify_pm__Request__c>();
        try {
            litify_pm__Matter__c matterObj = [SELECT Id FROM litify_pm__Matter__c WHERE Id =: recordId LIMIT 1][0];
            String query = 'SELECT Id, litify_pm__Matter__c,Request__c,Status__c,litify_pm__Facility__c,litify_pm__Facility_Name__c, litify_pm__Date_Requested__c, litify_pm__Date_Received__c, ' +
                            'litify_pm__Comments__c,litify_pm__Document__c FROM litify_pm__Request__c WHERE litify_pm__Matter__c =: recordId';
            
            recordsList = Database.query(query);
        } 
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString());
        }
        return recordsList;
    }

    @AuraEnabled
    public static List<litify_pm__Request__c> fetchCreateRecords(String recordId) {
        List<litify_pm__Request__c> recordsList = new List < litify_pm__Request__c >();
        try {
            Map<String, String> fieldsToMap = MatterHandler.getFieldsToMapFromCMT();            
            String query = 'SELECT Id, ';
            for (String matterFieldName: MatterHandler.getFieldsToMapFromCMT().keyset()) {
                query = query + matterFieldName + ', ';
            }
            query = query.removeEndIgnoreCase(', ') + ' FROM litify_pm__Matter__c WHERE Id =: recordId LIMIT 1';
            litify_pm__Matter__c matterObj = (litify_pm__Matter__c) Database.query(query);
            recordsList = MatterHandler.fetchRecords(matterObj, true);            
        } 
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString());
        }
        return recordsList;
    }

    @AuraEnabled
    public static List<litify_pm__Role__c> fetchRelatedRole(String recordId) {
        List<litify_pm__Role__c> roleList;
        try {
            roleList = [SELECT Id, Name__c FROM litify_pm__Role__c WHERE litify_pm__Matter__c =: recordId];
        } 
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString());
        }
        return roleList;
    }

    @AuraEnabled
    public static void saveRecords(List<litify_pm__Request__c> recordsList) {
        try {
            Type listSobjType = Type.ForName('List<litify_pm__Request__c>');
            List<SObject> sobjList = (List<SObject>) listSobjType.NewInstance();
            sobjList.addAll((List<Sobject>) recordsList);
            upsert sobjList;
        } 
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString());
        }
    }

    @AuraEnabled
    public static Boolean checkRecordsExist(String recordId) {
        List<litify_pm__Request__c> recordsList;
        try {
            recordsList = [SELECT Id FROM litify_pm__Request__c WHERE litify_pm__Matter__c =: recordId];
            if (!recordsList.isEmpty()) {
                return true;
            }
        } 
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString());
        }
        return false;
    }

    @AuraEnabled
    public static List<String> fetchCMTRecords() {
        List<Matter_Information_Request_Setting__mdt> recordsList;
        Set<String> cmtSet = new Set<String>();
        try {
            recordsList = [SELECT Id, MasterLabel FROM Matter_Information_Request_Setting__mdt ORDER BY MasterLabel];
            if (!recordsList.isEmpty()) {
                for (Matter_Information_Request_Setting__mdt cmt : recordsList){
                    cmtSet.add(cmt.MasterLabel);
                }
            }
        } 
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString());
        }
        return new List<String>(cmtSet);
    }
}