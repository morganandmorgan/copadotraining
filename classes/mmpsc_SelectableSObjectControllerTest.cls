@isTest
private class mmpsc_SelectableSObjectControllerTest {
    @isTest
    static void happy_path() {
        // Custom Settings
        mmpsc_CustomSettings__c mmpscSettings = new mmpsc_CustomSettings__c();
        mmpscSettings.AccountFieldset__c = 'PeopleSearch_Account';
        mmpscSettings.IntakeFieldset__c = 'PeopleSearch_Intake';
        mmpscSettings.LawsuitFieldset__c = 'PeopleSearch_Lawsuit';
        mmpscSettings.MatterFieldset__c = 'PeopleSearch_Matter';
        insert mmpscSettings;

        // Users (2)
        Id standardUserProfileId = [select Id from Profile where name ='Standard User'].get(0).Id;

        User u0 = new User();
        u0.Alias = 'jone';
        u0.Email = 'joe.one@example.com';
        u0.EmailEncodingKey='UTF-8';
        u0.FederationIdentifier=u0.Email;
        u0.FirstName = 'Joe';
        u0.IsActive = true;
        u0.LanguageLocaleKey='en_US';
        u0.LastName = 'One';
        u0.LocaleSidKey='en_US';
        u0.ProfileId = standardUserProfileId;
        u0.TimeZoneSidKey='America/New_York';
        u0.Username = u0.Email;

        User u1 = new User();
        u1.Alias = 'jtwo';
        u1.Email = 'jane.two@example.com';
        u1.EmailEncodingKey='UTF-8';
        u1.FederationIdentifier=u1.Email;
        u1.FirstName = 'Jane';
        u1.IsActive = true;
        u1.LanguageLocaleKey='en_US';
        u1.LastName = 'Two';
        u1.LocaleSidKey='en_US';
        u1.ProfileId = standardUserProfileId;
        u1.TimeZoneSidKey='America/New_York';
        u1.Username = u1.Email;

        insert(new List<User> {u0, u1});

        // Account (1)
        Account acct = new Account();
        acct.FirstName = 'Lila';
        acct.LastName = 'Woods';
        acct.PersonEmail = 'lila.woods@donotreply.com';
        acct.Phone = '784-555-5006';
        insert acct;

        // Intake (1)
        Intake__c intake = new Intake__c();
        intake.Client__c = acct.Id;
        intake.Handling_Firm__c = 'Morgan & Morgan';
        intake.Case_Type__c = 'tough one';
        intake.Status__c = 'open';
        insert intake;

        // The matter trigger was causing us problems, and we don't need it to run to test anyhow, so...
        Triggers_Setting__c ts = new Triggers_Setting__c(SetupOwnerId = UserInfo.getUserId(), Object_API_Name__c = 'litify_pm__Matter__c');
        insert ts;

        // Matter (1)
        Id socialSecurityRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socialSecurityRecordTypeId;
        matter.litify_pm__Client__c = acct.Id;
        //matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Status__c = 'Open';
        matter.Intake__c = intake.Id;
        insert matter;

        // Execute method
        List<mmpsc_SelectableSObjectController.RecordData> result = mmpsc_SelectableSObjectController.getAccounts(new List<Id> {acct.Id});

        // Assertion
        //System.assertEquals(u0.FirstName + ' ' + u0.LastName, result.get(0).childrecords.get(0).childrecords.get(0).fieldValues.get(5).value);
        System.assert(result != null);

        //for some extra coverage
        String relatedFieldName = mmpsc_SelectableSObjectController.getRelatedFieldName('relatedField__r');
        relatedFieldName = mmpsc_SelectableSObjectController.getRelatedFieldName('relatedField');

        String formattedPhone = mmpsc_SelectableSObjectController.formatPhone('4071234567');
        formattedPhone = mmpsc_SelectableSObjectController.formatPhone('14071234567');

    } //happy_path
} //class