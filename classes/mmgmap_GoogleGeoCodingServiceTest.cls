@isTest
private class mmgmap_GoogleGeoCodingServiceTest
{
	@isTest
	private static void normalProcess()
	{
		mmgmap_GoogleGeoCodingGetCallout mockCallout = mmgmap_GoogleGeoCodingGetCallout.newInstance();
		mockCallout.mock_ResponseJson = String.join(getResponseJson(), '');

		mockCallout.getHost();
		mockCallout.getPath();
		mockCallout.getParamMap();

		mmgmap_GoogleGeoCodingServiceImpl serv = (mmgmap_GoogleGeoCodingServiceImpl) new mmgmap_GoogleGeoCodingServiceImpl();
		serv.mock_callout = mockCallout;

		serv.getLatitudeLongitudeForAddress(getAddress());
	}

	@isTest
	private static void missingStreetInAddress()
	{
		mmgmap_GoogleGeoCodingGetCallout mockCallout = mmgmap_GoogleGeoCodingGetCallout.newInstance();
		mockCallout.mock_ResponseJson = String.join(getResponseJson(), '');

		mockCallout.getHost();
		mockCallout.getPath();
		mockCallout.getParamMap();

		mmgmap_GoogleGeoCodingServiceImpl serv = (mmgmap_GoogleGeoCodingServiceImpl) new mmgmap_GoogleGeoCodingServiceImpl();
		serv.mock_callout = mockCallout;

		mmgmap_Address data = getAddress();
		data.setStreet(null);

		try {
			serv.getLatitudeLongitudeForAddress(data);
			System.assert(false, 'The service should have returned an exception.');
		}
		catch (mmgmap_GoogleGeoCodingExceptions.ParameterException e) {
			// Expected exception.
		}
	}

	@isTest
	private static void missingRequest()
	{
		try {
			mmgmap_GoogleGeoCodingServiceImpl serv = (mmgmap_GoogleGeoCodingServiceImpl) new mmgmap_GoogleGeoCodingServiceImpl();
			serv.mock_callout = mmgmap_GoogleGeoCodingGetCallout.newInstance();
			serv.getLatitudeLongitudeForAddress(null);
			System.assert(false, 'An exception should have occurred.');
		}
		catch (mmgmap_GoogleGeoCodingExceptions.ParameterException e) {
			// Exception expected.
		}
	}

//	private static Address getAddress(AddressData data)
//	{
//		Id personAccountRecordType = [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
//
//		Account acct = new Account();
//
//		acct.RecordTypeID = personAccountRecordType;
//		acct.FirstName = 'Test FName';
//		acct.LastName = 'Test LName';
//		acct.PersonMailingStreet = data.street;
//		acct.PersonMailingCity = data.city;
//		acct.PersonMailingState = data.state;
//		acct.PersonMailingPostalCode = data.postalCode;
//		acct.PersonMailingCountry = data.country;
//		acct.PersonEmail = 'test@forthepeople.com';
//		acct.PersonHomePhone = '1234567';
//		acct.PersonMobilePhone = '12345678';
//
//		insert acct;
//
//		return [select Id, PersonMailingAddress from Account where Id = :acct.Id limit 1].PersonMailingAddress;
//	}

	private static List<String> getResponseJson()
	{
		List<String> sb = new List<String>();

		sb.add('{');
		sb.add('   "results" : [');
		sb.add('      {');
		sb.add('         "address_components" : [');
		sb.add('            {');
		sb.add('               "long_name" : "1600",');
		sb.add('               "short_name" : "1600",');
		sb.add('               "types" : [ "street_number" ]');
		sb.add('            },');
		sb.add('            {');
		sb.add('               "long_name" : "Amphitheatre Pkwy",');
		sb.add('               "short_name" : "Amphitheatre Pkwy",');
		sb.add('               "types" : [ "route" ]');
		sb.add('            },');
		sb.add('            {');
		sb.add('               "long_name" : "Mountain View",');
		sb.add('               "short_name" : "Mountain View",');
		sb.add('               "types" : [ "locality", "political" ]');
		sb.add('            },');
		sb.add('            {');
		sb.add('               "long_name" : "Santa Clara County",');
		sb.add('               "short_name" : "Santa Clara County",');
		sb.add('               "types" : [ "administrative_area_level_2", "political" ]');
		sb.add('            },');
		sb.add('            {');
		sb.add('               "long_name" : "California",');
		sb.add('               "short_name" : "CA",');
		sb.add('               "types" : [ "administrative_area_level_1", "political" ]');
		sb.add('            },');
		sb.add('            {');
		sb.add('               "long_name" : "United States",');
		sb.add('               "short_name" : "US",');
		sb.add('               "types" : [ "country", "political" ]');
		sb.add('            },');
		sb.add('            {');
		sb.add('               "long_name" : "94043",');
		sb.add('               "short_name" : "94043",');
		sb.add('               "types" : [ "postal_code" ]');
		sb.add('            }');
		sb.add('         ],');
		sb.add('         "formatted_address" : "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA",');
		sb.add('         "geometry" : {');
		sb.add('            "location" : {');
		sb.add('               "lat" : 37.4224764,');
		sb.add('               "lng" : -122.0842499');
		sb.add('            },');
		sb.add('            "location_type" : "ROOFTOP",');
		sb.add('            "viewport" : {');
		sb.add('               "northeast" : {');
		sb.add('                  "lat" : 37.4238253802915,');
		sb.add('                  "lng" : -122.0829009197085');
		sb.add('               },');
		sb.add('               "southwest" : {');
		sb.add('                  "lat" : 37.4211274197085,');
		sb.add('                  "lng" : -122.0855988802915');
		sb.add('               }');
		sb.add('            }');
		sb.add('         },');
		sb.add('         "place_id" : "ChIJ2eUgeAK6j4ARbn5u_wAGqWA",');
		sb.add('         "types" : [ "street_address" ]');
		sb.add('      }');
		sb.add('   ],');
		sb.add('   "status" : "OK"');
		sb.add('}');

		return sb;
	}

	public static mmgmap_Address getAddress()
	{
        return new mmgmap_Address().setStreet('1600 Amphitheatre Pkwy.')
                                .setCity('Mountain View')
                                .setState('CA')
                                .setPostalCode('94043')
                                .setCountry('USA');
	}
}