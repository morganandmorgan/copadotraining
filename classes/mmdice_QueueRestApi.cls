// Usage: /services/apexrest/dice/queues/a003D000001dPZDD?
@RestResource(urlMapping='/dice/queues/*')
global class mmdice_QueueRestApi
{
    public static final RestRequest req = RestContext.request;
    public static final RestResponse resp = RestContext.response;
    public static final DICEService diceService = new DICEService();

    @HttpGet
    global static GetResponse get()
    {
        GetResponse getResponse = new GetResponse();
        Integer count = 20;
        String queueId = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);

        // First check the request is OK.
        if(String.isEmpty(queueId) || !mmlib_Utils.isValidSalesforceId(queueId))
        {
            resp.statusCode = 400; // Bad Request
            return null;
        }

        // Then, check the optional parameters.
        if(String.isNotEmpty(req.params.get('top')))
        {
            try
            {
                count = Integer.valueOf(req.params.get('top'));
            }
            catch (Exception e)
            {
                resp.statusCode = 400; // Bad Request
                return null;
            }
        }

        // The request looks good, so let's get busy.
        try
        {
            List<Intake__c> intakeSObjs = new DICEService().getIntakes(queueId, count);

            System.debug('Request OK and we found ' + intakeSObjs.size() + ' records.');
            if(intakeSObjs.size() > 0)
            {
                for (Intake__c intakeSObj : intakeSObjs)
                {
                    getResponse.records.add(new mmdice_RecordInfo(intakeSObj));
                }

                resp.statusCode = 200; // OK
                return getResponse;
            }
        }
        catch (Exception e)
        {
            resp.statusCode = 500; // Internal Server Error
            return null;
        }

        // Finally, if no reocrds are found, inform the caller.
        resp.statusCode = 204; // No Content
        return null;
    }

    global class GetResponse
    {
        public GetResponse()
        {
            this.records = new List<mmdice_RecordInfo>();
        }

        public List<mmdice_RecordInfo> records { get; set; }

        public Integer total_items
        {
            get { return this.records.size(); }
        }
    }
}