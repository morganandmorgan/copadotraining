@isTest
public class MailingOnMatterControllerTest {

    @isTest
    public static void mainTest() {

        //suppress the audit processor
        Triggers_Setting__c ts = new Triggers_Setting__c(SetupOwnerId = UserInfo.getUserId(), Object_API_Name__c = 'AuditProcessor');
        insert ts;

        Account a = TestUtil.createPersonAccount('Test', 'Account');
        insert a;

        litify_pm__Matter__c m = TestUtil.createMatter(a);
        insert m;

        Account party = TestUtil.createPersonAccount('Test', 'Account');
        party.shippingStreet = '101 Test Street';
        party.shippingCity = 'Test City';
        party.shippingState = 'FL';
        party.shippingPostalCode = '32800';
        insert party;


        litify_docs__file_info__c fileInfo = new litify_docs__file_info__c();
        fileInfo.name = 'test.pdf';
        fileInfo.litify_docs__Document_Category__c = '';
        fileInfo.litify_docs__Document_Subcategory__c = '';
        fileInfo.litify_docs__To__c = 'Document To';
        fileInfo.litify_docs__From__c = 'Document From';
        fileInfo.litify_docs__Related_To__c = m.id;
        insert fileInfo;

        litify_pm__Role__c r = TestUtil.createRole(m, a);
        r.litify_pm__Party__c = party.id;
        insert r;

        List<MailingOnMatterController.DocData> docData = MailingOnMatterController.getDocData(m.id);

        List<MailingOnMatterController.MailType> mailTypes = MailingOnMatterController.getMailTypeOptions();

        List<MailingOnMatterController.RoleData> roleData = MailingOnMatterController.getRoleData(m.id);

        //turn it into a CP matter
        Lawsuit__c ls = TestUtil.createLawsuit(null);
        ls.CP_Active_Case_Indicator__c = 'Y';
        insert ls;

        m.ReferenceNumber__c = '9828224';
        m.CP_Office__c = 'ORL';
        m.related_Lawsuit__c = ls.id;

        update m;

        Test.setMock(HttpCalloutMock.class, new CPDataProviderTest.MockHttpRequests());

        Test.startTest();

        docData = MailingOnMatterController.getDocData(m.id);

        roleData = MailingOnMatterController.getRoleData(m.id);

        Test.stopTest();

        String jsDocuments = '[{"category":"","createdBy":"SALESFORCE","createdDate":"5/24/2018","docFrom":"CCC","docTo":"FILE","filePath":"//PathTo/File/","id":"abcd1234","lastModBy":"SALESFORCE","lastModDate":"5/24/2018","name":"test.pdf","subCategory":""}]';
        String jsRoles = '[{"id":"CP15","name":"Test Name","attn":"ATTN: Test","role":"Plaintiff","shippingCity":"Orlando","shippingPostalCode":"32800","shippingState":"FL","shippingStreet":"101 Main Street","returnEnvelope":true}]';

        List<String> results = MailingOnMatterController.sendMail(m.id, jsDocuments, jsRoles, 'First Class');

    } //mainTest

} //class