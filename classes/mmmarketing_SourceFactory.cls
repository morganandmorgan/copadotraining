/**
 *  mmmarketing_SourceFactory
 */
public class mmmarketing_SourceFactory
{
    private fflib_ISObjectUnitOfWork uow = null;

    private Marketing_Source__c newRecord = null;

    public mmmarketing_SourceFactory()
    {
    }

    public mmmarketing_SourceFactory( fflib_ISObjectUnitOfWork uow )
    {
        this();

        this.uow = uow;
    }

    public Marketing_Source__c generateNewFromSourceValue( String sourceValue )
    {
        // initialize the instance variable
        this.newRecord = new Marketing_Source__c();

        this.newRecord.utm_source__c = sourceValue;

        if (this.uow != null
            && this.newRecord != null
            )
        {
            this.uow.registerNew( this.newRecord );
        }

        return this.newRecord;
    }
}