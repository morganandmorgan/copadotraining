public class mmpsc_PeopleSearchComponentSettings
{
	public mmpsc_PeopleSearchComponentSettings()
	{
        // Static-only class.
    }

    public static String getAccountFieldset()
    {
        return getSettings().AccountFieldset__c;
    }

    public static String getIntakeFieldset()
    {
        return getSettings().IntakeFieldset__c;
    }

    public static String getLawsuitFieldset()
    {
        return getSettings().LawsuitFieldset__c;
    }

    public static String getMatterFieldset()
    {
        return getSettings().MatterFieldset__c;
    }

    private static mmpsc_CustomSettings__c settings = null;
    private static Boolean fetchedSettings = false;

    private static mmpsc_CustomSettings__c getSettings()
    {
        if (!fetchedSettings)
        {
            settings = mmpsc_CustomSettings__c.getInstance();

            if (settings == null)
            {
                throw new mmpsc_PeopleSearchComponenExceptions.SettingsException('Feature\'s custom settings were not found.');
            }
        }

        return settings;
    }
}