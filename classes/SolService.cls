/**
 * SolService
 * @description SOL Service implementation.
 * @author Jeff Watson
 * TestClass: SolServiceTest
 * @date 9/26/2018
 */
public with sharing class SolService {

    public SolService() { }

    public BestMatch getBestMatch(litify_pm__Matter__c matter) {

        // Only execute if a matter is sent in
        if(matter == null) return null;

        BestMatch bestMatch = null;
        String caseTypeName = ([select Id, Name from litify_pm__Case_Type__c where Id = :matter.litify_pm__Case_Type__c limit 1]).Name;
        List<SOL_Manager__c> solManagers = [select Id, Name, State__c, Case_Type__c, Apply_to_All_States__c, Death__c, SOL__c, Type_of_Injury__c, Type_of_Maritime_and_Admiralty_Case__c, Where_Did_The_Injury_Occur__c from SOL_Manager__c where Case_Type__c = :caseTypeName];

        if(solManagers.size() > 0) {

            for (SOL_Manager__c solManager : solManagers) {

                Integer score = 0;

                if(solManager.Apply_to_All_States__c) score = score + 1;
                if(solManager.Death__c = matter.Injured_Party_Deceased__c) score = score + 1;
                if(solManager.State__c == matter.Incident_State__c) score = score + 1;
                if(solManager.Type_of_Injury__c == caseTypeName) score = score + 1;
                if(solManager.Type_of_Maritime_and_Admiralty_Case__c == matter.Type_of_maritime_and_admiralty_case__c) score = score + 1;
                if(solManager.Where_Did_The_Injury_Occur__c == matter.Where_did_the_injury_occur__c) score = score + 1;

                if(bestMatch == null) {
                    bestMatch = new BestMatch(score, solManager);
                } else if(bestMatch.Score < score) {
                    bestMatch = new BestMatch(score, solManager);
                }
            }
        }

        return bestMatch;
    }

    public Map<Id,BestMatch> getBestMatch(List<litify_pm__Matter__c> matterList) {

        Map<Id,BestMatch> returnMap = new Map<Id,BestMatch>();

        // Only execute if a matter is sent in
        if(matterList == null || matterList.isEmpty()) return returnMap;

        Set<Id> caseTypeIds = new Set<Id>();
        Map<Id, String> caseTypeMap = new Map<Id, String>();
        Map<String, List<SOL_Manager__c>> solManagerMap = new Map<String, List<SOL_Manager__c>>(); //key is case type name, value is the list of managers

        //loop and get the case type Ids
        for(litify_pm__Matter__c m : matterList){
            caseTypeIds.add(m.litify_pm__Case_Type__c);
        }
        //populate the case type map
        for(litify_pm__Case_Type__c ct : [select Id, Name from litify_pm__Case_Type__c where Id in :caseTypeIds]){
            caseTypeMap.put(ct.Id, ct.Name);
        }
        //populate the solManagerMap:
        for(SOL_Manager__c manager : [SELECT Id, 
                Name, 
                State__c, 
                Case_Type__c, 
                Apply_to_All_States__c, 
                Death__c, 
                SOL__c, 
                Type_of_Injury__c, 
                Type_of_Maritime_and_Admiralty_Case__c, 
                Where_Did_The_Injury_Occur__c 
            FROM SOL_Manager__c 
            WHERE Case_Type__c in :caseTypeMap.values() 
            AND Case_Type__c != null]){

            //is not in the map so add it
            if(!solManagerMap.containsKey(manager.Case_Type__c)){
                List<SOL_Manager__c> tmpList = new List<SOL_Manager__c>();
                tmpList.add(manager);
                solManagerMap.put(manager.Case_Type__c, tmpList);
            }
            //if it is in the map then add the manager
            else{
                List<SOL_Manager__c> tmpList = solManagerMap.get(manager.Case_Type__c);
                tmpList.add(manager);
                solManagerMap.put(manager.Case_Type__c, tmpList);
            }
        }

        //loop through the matters and populate the bestMatch into the return Map:
        for(litify_pm__Matter__c matter : matterList){
            String caseTypeName = caseTypeMap.containsKey(matter.litify_pm__Case_Type__c) ? caseTypeMap.get(matter.litify_pm__Case_Type__c) : '';
            if(caseTypeName != ''){
                BestMatch bestMatch = null;    
                List<SOL_Manager__c> solManagers = solManagerMap.containsKey(caseTypeName) ? solManagerMap.get(caseTypeName) : null;
                if(solManagers != null && solManagers.size() > 0) {

                    for (SOL_Manager__c solManager : solManagers) {
                        Integer score = 0;

                        if(solManager.Apply_to_All_States__c) score = score + 1;
                        if(solManager.Death__c = matter.Injured_Party_Deceased__c) score = score + 1;
                        if(solManager.State__c == matter.Incident_State__c) score = score + 1;
                        if(solManager.Type_of_Injury__c == caseTypeName) score = score + 1;
                        if(solManager.Type_of_Maritime_and_Admiralty_Case__c == matter.Type_of_maritime_and_admiralty_case__c) score = score + 1;
                        if(solManager.Where_Did_The_Injury_Occur__c == matter.Where_did_the_injury_occur__c) score = score + 1;

                        if(bestMatch == null) {
                            bestMatch = new BestMatch(score, solManager);
                        } else if(bestMatch.Score < score) {
                            bestMatch = new BestMatch(score, solManager);
                        }
                    }
                } 
                //add the best match to the return map
                returnMap.put(matter.Id, bestMatch);   
            }
        }
        return returnMap;
    }

    public class BestMatch {

        public Integer Score { get; set; }
        public SOL_Manager__c SOL { get; set; }

        public BestMatch(Integer score, SOL_Manager__c SOL) {
            this.Score = score;
            this.SOL = SOL;
        }
    }
}