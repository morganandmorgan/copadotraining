public with sharing class CPDataProvider
{
    @AuraEnabled(cacheable=true)
    public static litify_pm__Matter__c GetMatterInfo(String matterId)
    {
        litify_pm__Matter__c[] matters = [select CP_Office__c, Assigned_Office_Location__c, ReferenceNumber__c, litify_pm__Display_Name__c from litify_pm__Matter__c where Id =: matterId];

        if (matters.size() > 0)
        {
            litify_pm__Matter__c matter = matters[0];

            if (String.isBlank(matter.CP_Office__c) && !String.isBlank(matter.Assigned_Office_Location__c))
            {
                Account[] accounts = [select OfficeLocationCode__c from Account where Id =: matter.Assigned_Office_Location__c];

                if (accounts.size() > 0)
                    matter.CP_Office__c = accounts[0].OfficeLocationCode__c;
            }

            return matter;
        }
        
        return null;
    }

    @AuraEnabled(cacheable=true)
    public static User GetUserInfo(String userId)
    {
        User[] users = [select Name, Username from User where Id =: userId];
        
        if (users.size() > 0)
            return users[0];
        
        return null;
    }

    // Get API access token
    public static String GetAccessToken()
    {
        Http client = new Http();

        HttpRequest request = new HttpRequest();

        request.setEndpoint('callout:CPDataProvider/oauth/token');
        request.setHeader('Content-Length', '0');
        request.setMethod('POST');

        HttpResponse response = client.send(request);
        return response.getBody();
    }

    // Returns case expenses, trusts and summary
    @AuraEnabled(cacheable=true)
    public static String GetFinInfo(String matterId)
    {
        String token = GetAccessToken();

        litify_pm__Matter__c matter = GetMatterInfo(matterId);

        if (matter == null)
            return '';

        Http client = new Http();

        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://cp-dataprovider.azurewebsites.net/api/casefininfo?office=' + 
            matter.CP_Office__c + '&matter=' + matter.ReferenceNumber__c);
        request.SetHeader('Authorization', 'Bearer ' + token);
        request.setMethod('GET');

        HttpResponse response = client.send(request);
        return response.getBody();
    }

    @AuraEnabled(cacheable=true)
    public static String GetCheckReqData(String matterId)
    {
        String token = GetAccessToken();

        litify_pm__Matter__c matter = GetMatterInfo(matterId);

        if (matter == null)
            return '';

        Http client = new Http();

        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://cp-dataprovider.azurewebsites.net/api/checkrequest/casedata?office=' + 
            matter.CP_Office__c + '&matter=' + matter.ReferenceNumber__c);
        request.SetHeader('Authorization', 'Bearer ' + token);
        request.setMethod('GET');

        HttpResponse response = client.send(request);
        return response.getBody();
    }



    @AuraEnabled(cacheable=true)
    public static String FindVendors(String matterId, String term)
    {
        String token = GetAccessToken();

        litify_pm__Matter__c matter = GetMatterInfo(matterId);

        if (matter == null)
            return '';

        Http client = new Http();

        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://cp-dataprovider.azurewebsites.net/api/checkrequest/vendors?office=' + 
            matter.CP_Office__c + '&term=' + term);
        request.SetHeader('Authorization', 'Bearer ' + token);
        request.setMethod('GET');

        HttpResponse response = client.send(request);
        return response.getBody();
    }

    @AuraEnabled(cacheable=true)
    public static Boolean SendCheckReq(String checkRequest)
    {
        System.debug(checkRequest);

        String token = GetAccessToken();

        Http client = new Http();

        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://cp-dataprovider.azurewebsites.net/api/checkrequest');
        request.setHeader('Authorization', 'Bearer ' + token);
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(checkRequest);
        request.setMethod('POST');

        HttpResponse response = client.send(request);
        return response.getStatusCode() == 200;
    }

    //

    // Returns documents and parties for a given case
    @AuraEnabled(cacheable=true)
    public static String GetMailData(String matterId)
    {
        String token = GetAccessToken();

        litify_pm__Matter__c matter = GetMatterInfo(matterId);

        if (matter == null)
            return '';

        Http client = new Http();

        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://cp-dataprovider.azurewebsites.net/api/maildata?office=' + 
            matter.CP_Office__c + '&matter=' + matter.ReferenceNumber__c);
        request.SetHeader('Authorization', 'Bearer ' + token);
        request.setMethod('GET');

        HttpResponse response = client.send(request);
        return response.getBody();
    }
}