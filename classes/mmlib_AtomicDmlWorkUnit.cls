public class mmlib_AtomicDmlWorkUnit
{
    private SObject record = null;
    private mmlib_DatabaseResultWrapper wrap = null;

    public SObject getRecord()
    {
        return record;
    }

    public mmlib_DatabaseResultWrapper getResultWrapper()
    {
        return wrap;
    }

    public mmlib_AtomicDmlWorkUnit(SObject record, Database.DeleteResult result)
    {
        this.record = record;
        this.wrap = new mmlib_DatabaseResultWrapper(result);
    }

    public mmlib_AtomicDmlWorkUnit(SObject record, Database.SaveResult result)
    {
        this.record = record;
        this.wrap = new mmlib_DatabaseResultWrapper(result);
    }
}