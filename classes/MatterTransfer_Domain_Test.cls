@isTest
private class MatterTransfer_Domain_Test {
	
	public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;
	public static c2g__codaCompany__c targetCompany_Active;
	public static c2g__codaCompany__c sourceCompany;
	public static List<Account> testAccounts;

	static void setupData()
    {
        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');
		Id mmOfficeAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Office_Locations');

        testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccountSource = new Account();
        mmAccountSource.Name = 'MM COMPANY SOURCE';
        mmAccountSource.litify_pm__First_Name__c = 'TEST';
        mmAccountSource.litify_pm__Last_Name__c = 'CONTACT';
        mmAccountSource.c2g__CODATaxCalculationMethod__c = 'Gross';
        mmAccountSource.litify_pm__Email__c = 'test@testcontact.com';
        //mmAccountSource.FFA_Company__c = sourceCompany.Id;
        mmAccountSource.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccountSource);

		Account mmAccountTarget = new Account();
        mmAccountTarget.Name = 'MM COMPANY TARGET';
        mmAccountTarget.litify_pm__First_Name__c = 'TEST';
        mmAccountTarget.c2g__CODATaxCalculationMethod__c = 'Gross';
        mmAccountTarget.litify_pm__Last_Name__c = 'CONTACT';
        mmAccountTarget.litify_pm__Email__c = 'test@testcontact.com';
        //mmAccountTarget.FFA_Company__c = targetCompany_Active.Id;
        mmAccountTarget.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccountTarget);

		Account mmAccountOffice = new Account();
        mmAccountOffice.Name = 'MM COMPANY OFFICE';
        mmAccountOffice.litify_pm__First_Name__c = 'TEST';
        mmAccountOffice.litify_pm__Last_Name__c = 'CONTACT';
        mmAccountOffice.litify_pm__Email__c = 'test@testcontact.com';
        mmAccountOffice.c2g__CODATaxCalculationMethod__c = 'Gross';
		mmAccountOffice.Type = 'MM Office';
        mmAccountOffice.RecordTypeId = mmOfficeAccount;
        testAccounts.add(mmAccountOffice);

        INSERT testAccounts;

    	/*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaBankAccount__c operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
        update operatingBankAccount;
		
        c2g__codaGeneralLedgerAccount__c testGLA = TestDataFactory_FFA.balanceSheetGLAs[0];

        c2g__codaAccountingSettings__c acctSettings = new c2g__codaAccountingSettings__c(
            Client_Cost_Receivable_GLA_Code__c = testGLA.c2g__ReportingCode__c
        );
        insert acctSettings;

		targetCompany_Active = TestDataFactory_FFA.createCompany('targetCompany_Active');
        targetCompany_Active.Intercompany_GLA__c = testGLA.Id;
        targetCompany_Active.c2g__IntercompanyAccount__c = testAccounts[2].Id;
        update targetCompany_Active;

		Id currentUserId = UserInfo.getUserId();
		List<GroupMember> gmList = [SELECT Id, GroupId FROM GroupMember WHERE UseroRGroupId = :currentUserId];
		delete gmList;

		Test.startTest();
		sourceCompany = TestDataFactory_FFA.createCompany('sourceCompany');
        sourceCompany.Intercompany_GLA__c = testGLA.Id;
        sourceCompany.c2g__IntercompanyAccount__c = testAccounts[1].Id;
        update sourceCompany;

        //update the accounts
        testAccounts[1].FFA_Company__c = sourceCompany.Id;
        testAccounts[2].FFA_Company__c = targetCompany_Active.Id;
        update testAccounts;

       
		/*--------------------------------------------------------------------
        LITIFY Data Setup
        --------------------------------------------------------------------*/

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
		matter.Assigned_Office_Location__c = testAccounts[3].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        INSERT expenseType;

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Internet';
        expenseType2.ExternalID__c = 'INTERNET1';
        INSERT expenseType2;

        List<litify_pm__Expense__c> expList = new List<litify_pm__Expense__c>();
        
        litify_pm__Expense__c testExpense = new litify_pm__Expense__c();
        testExpense.litify_pm__Matter__c = matter.Id;
        testExpense.litify_pm__Amount__c = 100.0;
        testExpense.litify_pm__Date__c = date.today();
        testExpense.Payable_Invoice_Created__c = true;
        testExpense.litify_pm__ExpenseType2__c = expenseType.Id;
        expList.add(testExpense);

        litify_pm__Expense__c testExpense2 = new litify_pm__Expense__c();
        testExpense2.litify_pm__Matter__c = matter.Id;
        testExpense2.litify_pm__Amount__c = 10.0;
        testExpense2.litify_pm__Date__c = date.today();
        testExpense2.Payable_Invoice_Created__c = true;
        testExpense2.litify_pm__ExpenseType2__c = expenseType2.Id;
        expList.add(testExpense2);

        litify_pm__Expense__c testExpense3 = new litify_pm__Expense__c();
        testExpense3.litify_pm__Matter__c = matter.Id;
        testExpense3.litify_pm__Amount__c = 75.0;
        testExpense3.litify_pm__Date__c = date.today();
        testExpense3.Payable_Invoice_Created__c = true;
        testExpense3.litify_pm__ExpenseType2__c = expenseType.Id;
        expList.add(testExpense3);

        litify_pm__Expense__c testExpense4 = new litify_pm__Expense__c();
        testExpense4.litify_pm__Matter__c = matter.Id;
        testExpense4.litify_pm__Amount__c = 25.0;
        testExpense4.litify_pm__Date__c = date.today();
        testExpense4.Payable_Invoice_Created__c = true;
        testExpense4.litify_pm__ExpenseType2__c = expenseType.Id;
        expList.add(testExpense4);

        litify_pm__Expense__c testExpense5 = new litify_pm__Expense__c();
        testExpense5.litify_pm__Matter__c = matter.Id;
        testExpense5.litify_pm__Amount__c = 5.0;
        testExpense5.litify_pm__Date__c = date.today();
        testExpense5.Payable_Invoice_Created__c = true;
        testExpense5.litify_pm__ExpenseType2__c = expenseType2.Id;
        expList.add(testExpense5);

        litify_pm__Expense__c testExpense6 = new litify_pm__Expense__c();
        testExpense6.litify_pm__Matter__c = matter.Id;
        testExpense6.litify_pm__Amount__c = 5.0;
        testExpense6.litify_pm__Date__c = date.today();
        testExpense6.Payable_Invoice_Created__c = true;
        testExpense6.litify_pm__ExpenseType2__c = expenseType2.Id;
        expList.add(testExpense6);

        insert expList;
    }

    //================= TEST METHODS ====================

	@isTest static void test_MatterTransferDomain() {
		setupData();

		Matter_Transfer__c testTransfer = new Matter_Transfer__c(
			Matter__c           = matter.Id,
			From_MM_Business__c = testAccounts[1].Id,
			To_MM_Business__c   = testAccounts[2].Id,
			From_MM_Office__c   = testAccounts[3].Id,
			To_MM_Office__c     = testAccounts[3].Id);
	 	insert testTransfer;

		MatterTransfer_Domain testConstructor = new MatterTransfer_Domain();
	}
}