/**
 * RoleDomain
 * @description Domain class for litify_pm__Role__c.
 * @author Jeff Watson
 * @date 1/18/2019
 */

public with sharing class RoleDomain extends fflib_SObjectDomain {

    // Ctor
    public RoleDomain(List<litify_pm__Role__c> records) {
        super(records);
    }

    // Trigger Handlers
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        updateMatterDeceasedPerRole();
    }

    public override void onBeforeDelete() {
        updateMatterDeceasedPerRole();
    }

    // Methods
    public void updateMatterDeceasedPerRole() {

        Set<Id> matterIds = new Set<Id>();
        for(SObject sobj : records) {
            litify_pm__Role__c role = (litify_pm__Role__c) sobj;
            matterIds.add(role.litify_pm__Matter__c);
        }

        Map<Id, List<litify_pm__Role__c>> rolesMapByMatterIds = new Map<Id, List<litify_pm__Role__c>>();
        for (litify_pm__Role__c role :  [select Id, litify_pm__Additional_Trait__c, Deceased_Date__c, litify_pm__Matter__c from litify_pm__Role__c where litify_pm__Matter__c in :matterIds]) {

            if (String.isNotEmpty(role.litify_pm__Matter__c)) {
                if(!rolesMapByMatterIds.containsKey(Id.valueOf(role.litify_pm__Matter__c))) {
                    rolesMapByMatterIds.put(Id.valueOf(role.litify_pm__Matter__c), new List<litify_pm__Role__c>());
                }
                rolesMapByMatterIds.get(Id.valueOf(role.litify_pm__Matter__c)).add(role);
            }
        }

        List<litify_pm__Matter__c> matters = [select Id, Injured_Party_Deceased__c, Deceased_Date__c from litify_pm__Matter__c where Id in :matterids for update];
        for (litify_pm__Matter__c matter : matters) {

            Boolean isInjuredParty = false;
            Boolean isDeceased = false;
            Date deceasedDate = null;
            Boolean clientIsMinor = false;

            for (litify_pm__Role__c relatedRole : rolesMapByMatterIds.get(matter.Id)) {
                if(String.isNotEmpty(relatedRole.litify_pm__Additional_Trait__c) && relatedRole.litify_pm__Additional_Trait__c.contains('Injured Party')) {
                    isInjuredParty = true;
                    if (relatedRole.litify_pm__Additional_Trait__c.contains('Deceased')) {
                        isDeceased = true;
                        deceasedDate = relatedRole.Deceased_Date__c;
                    }
                    if (relatedRole.litify_pm__Additional_Trait__c.contains('Minor')) {
                        clientIsMinor = true;
                    }
                }
            }

            matter.Client_is_Minor__c = clientIsMinor;
            matter.Injured_Party_Deceased__c = isDeceased;
            if(!isDeceased) {
                matter.Deceased_Date__c = null;
            } else {
                matter.Deceased_Date__c = deceasedDate;
            }
        }

        update matters;
    }

    // IConstructable
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new RoleDomain(records);
        }
    }
}