public class mmlitifylrn_ReferralTransSelector extends mmlib_SObjectSelector 
    implements mmlitifylrn_IReferralTransSelector
{
    public static mmlitifylrn_IReferralTransSelector newInstance()
    {
        return (mmlitifylrn_IReferralTransSelector) mm_Application.Selector.newInstance(litify_pm__Referral_Transaction__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Referral_Transaction__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
	        litify_pm__Referral_Transaction__c.litify_pm__Accepted_fee__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Agreement_Name__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Amount__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Assigned_to_firm_date__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Assigned_to_user_date__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Attempt__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Case_closed_at__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Case_lost_reason__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Case_turn_down_reason__c,
	        litify_pm__Referral_Transaction__c.litify_pm__CaseTurnDownReason2__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Expires_at__c,
	        litify_pm__Referral_Transaction__c.litify_pm__ExternalId__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Handling_firm__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Litify_com_ID__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Referral__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Status__c,
	        litify_pm__Referral_Transaction__c.litify_pm__Suggested_fee__c
        };
    }

    public List<litify_pm__Referral_Transaction__c> selectById( Set<id> idSet )
    {
        return (List<litify_pm__Referral_Transaction__c>) selectSObjectsById(idSet);
    }
}