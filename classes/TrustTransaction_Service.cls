/*============================================================================/
* TrustTransaction_Service
* @description Service class for Litify Matter
* @author Brian Krynitsky
* @date 2/26/2019
=============================================================================*/

public class TrustTransaction_Service {
	// Ctors
	public TrustTransaction_Service() {
	}

	// Methods
	public void updateMatterRollups(List<Trust_Transaction__c> ttList){

		Matter_Service mService = new Matter_Service();
		Set<Id> matterIds = new Set<Id>();

		for(Trust_Transaction__c tt : ttList){
			matterIds.add(tt.Matter__c);
		}
		mService.calculateExpenseTotals(matterIds);
	}
}