@isTest
private class DICE_WidgetControllerTest {
  static DICE_Queue__c testQueue;
  static Intake__c intake_from_NY;
  static Account acct;
  static User integrationUser;
  static {

    integrationUser = new User(Username = 'IntegrationUserTest@morganmorganengineering.com', Email = 'IntegrationUserTest@morganmorganengineering.com', LastName = 'Integration', FirstName = 'Integration', Alias = 'Integ', TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1].Id);
    insert integrationUser;

    testQueue = new DICE_Queue__c(Name='Test Queue', Query__c = '[{"column": "Caller_First_Name__c", "operator": "<>", "argument": "BAD"}, {"column":"CreatedDate", "operator": ">=", "argument": "Today"}, {"column":"litigation__c", "operator": "=", "argument": "MASS TORT"}]', Sorting__c = '[{"column": "Last_Correct_Contact__c", "order": "DESC"}]', EarliestCallTime__c = Integer.valueOf(DateTime.now().format('H', 'US/Eastern')) - 2, LatestCallTime__c = Integer.valueOf(DateTime.now().format('H', 'US/Eastern')) + 2);
    insert testQueue;

    acct = new Account(FirstName = 'Test', LastName = 'Account');
    insert acct;

    intake_from_NY = new Intake__c(Caller_First_Name__c='Test Intake from NY', Client__c = acct.id, Phone__c = '212-111-1111', Last_Correct_Contact__c = DateTime.now().addHours(-2), litigation__c = 'MASS TORT');
    insert intake_from_NY;

    PageReference PageRef = Page.DICE_Widget;
    Test.setCurrentPage(PageRef);
  }

  @isTest static void OnLoad_ContainsListOfQueues() {
    DICE_WidgetController controller = new DICE_WidgetController();
    controller.OnLoad();
    System.assertEquals(1, controller.QueueList.size());
    System.assertEquals(testQueue.Id, controller.QueueList[0].getValue());
    System.assertEquals(testQueue.Name, controller.QueueList[0].getLabel());
  }

  @isTest static void OnLoad_IfNoCookie_SelectedQueueIsBlank() {
    DICE_WidgetController controller = new DICE_WidgetController();
    controller.OnLoad();

    System.assertEquals(null, controller.SelectedQueueId);
  }
  
  @isTest static void OnLoad_SelectsQueueBasedOnCookie() {
    Cookie cook = new Cookie('IntakeRepNavigatorWidget_SelectedQueueId', testQueue.Id, null, -1, false);
    ApexPages.currentPage().setCookies(new Cookie[]{cook});

    DICE_WidgetController controller = new DICE_WidgetController();
    controller.OnLoad();

    System.assertEquals(testQueue.Id, controller.SelectedQueueId);
  }

  @isTest static void goToNext_EarliestCallTimeIsAfterCurrentTimeInTimezone_IntakeIsSkipped() {
    testQueue.EarliestCallTime__c = Time.newInstance(Integer.valueOf(DateTime.now().format('H', 'US/Eastern')),0,0,0).addHours(1).hour();
    testQueue.LatestCallTime__c = Time.newInstance(Integer.valueOf(DateTime.now().format('H', 'US/Eastern')),0,0,0).addHours(2).hour();
    update testQueue;

    DICE_WidgetController controller = new DICE_WidgetController();
    controller.SelectedQueueId = testQueue.Id;

    controller.GoToNext();
    System.assertEquals(null, controller.NextId);
  }

  @isTest static void goToNext_LatestCallTimeIsBeforeCurrentTimeInTimezone_IntakeIsSkipped() {
    testQueue.EarliestCallTime__c = Time.newInstance(Integer.valueOf(DateTime.now().format('H', 'US/Eastern')),0,0,0).addHours(-2).hour();
    testQueue.LatestCallTime__c = Time.newInstance(Integer.valueOf(DateTime.now().format('H', 'US/Eastern')),0,0,0).addHours(-1).hour();
    update testQueue;

    DICE_WidgetController controller = new DICE_WidgetController();
    controller.SelectedQueueId = testQueue.Id;

    controller.GoToNext();
    System.assertEquals(null, controller.NextId);
  }

  @isTest static void goToNext_IntakeJustCreated_DoesNotVisit() {
    DICE_WidgetController controller = new DICE_WidgetController();
    controller.SelectedQueueId = testQueue.Id;

    controller.GoToNext();
    //System.assert(controller.NextId != null);
  }

  @isTest static void goToNext_UserHasBeenVisited_DoesNotRevisit() {
    intake_from_NY.Last_Call_Time__c = null;
    intake_from_NY.Last_Correct_Contact__c = null;
    update intake_from_NY;

    DICE_AccessLog__c logEntry = new DICE_AccessLog__c(ItemId__c = intake_from_NY.Client__c, NavigatorId__c = testQueue.Id);
    INSERT logEntry;

    Test.startTest();
    DICE_WidgetController controller = new DICE_WidgetController();
    controller.SelectedQueueId = testQueue.Id;
    controller.GoToNext();
    Test.stopTest();

    //System.assertEquals(null, controller.NextId);
  }
  
}