/**
 * AuditFlagScheduledSelector
 * @description Selector for Audit Scheduled Flag SObject.
 * @author Matt Terrill
 * @date 9/23/2019
 */
public without sharing class AuditScheduledFlagSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public AuditScheduledFlagSelector() {
        sObjectFields = new List<Schema.SObjectField> {
                Audit_Scheduled_Flag__c.id,
                Audit_Scheduled_Flag__c.scheduled_date__c,
                Audit_Scheduled_Flag__c.status__c,
                Audit_Scheduled_Flag__c.audit_rule__c,
                Audit_Scheduled_Flag__c.user__c,

                Audit_Scheduled_Flag__c.litify_pm_Matter__c
        };
    } //constructor


    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Audit_Scheduled_Flag__c.SObjectType;
    } //getSObjectType


    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields


    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList


    public List<Audit_Scheduled_Flag__c> selectById(Set<Id> ids) {
        return (List<Audit_Scheduled_Flag__c>)selectSObjectsById(ids);
    } //selectById

    //selects scheduled flags that are pending and scheduled for today or earlier
    public List<Audit_Scheduled_Flag__c> selectNeedsReEvaluation() {

        fflib_QueryFactory query = newQueryFactory();

        query.selectField('Audit_Rule__r.Object__c');

        DateTime endOfToday = DateTime.newInstance(Date.today(), Time.newInstance(23,59,59,0));

        query.setCondition('scheduled_date__c <= :endOfToday AND status__c = \'Pending\'');

        return (List<Audit_Scheduled_Flag__c>)Database.query(query.toSOQL());
    } //selectNeedsReEvaluation


    //this is to get existing sheduled flags that match a set of evaluation results
    public List<Audit_Scheduled_Flag__c> selectByResultsPending(List<AuditEvalEngine.EvaluationResult> results, String relatedIdField) {
        if (results.size() != 0) {
            fflib_QueryFactory query = newQueryFactory();

            String condition = '(';
            String oor = '';
            for (AuditEvalEngine.EvaluationResult result : results) {
                //retrieve any flags with the same rule, object
                condition = condition + oor + '(audit_rule__c = \'' + result.rule.auditRule.id + '\''
                    + ' AND ' + relatedIdField + ' = \'' + (Id)result.obj.get('id') + '\')';
                    //+ ' AND user__c = \'' + (Id)result.obj.get('ownerId') + '\')';
                oor = ' OR ';
            }
            condition = condition + ') AND status__c = \'Pending\'';

            query.setCondition(condition);

            return (List<Audit_Scheduled_Flag__c>)Database.query(query.toSOQL());
        } else {
            return new List<Audit_Scheduled_Flag__c>();
        }
    } //selectByResultsPending


    //selects flags based on a rule
    public List<Audit_Scheduled_Flag__c> selectByRuleId(Set<Id> ruleIds, Integer limitCount) {

        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('audit_rule__c IN :ruleIds');
        query.setLimit(limitCount);

        return (List<Audit_Scheduled_Flag__c>)Database.query(query.toSOQL());
    } //selectByRule


} //class