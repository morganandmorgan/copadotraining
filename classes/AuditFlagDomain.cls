/**
 * AuditFlagDomain
 * @description Domain class for Audit_Flag__c SObject.
 * @author Matt Terrill
 * @date 9/12/2019
 */
public without sharing class AuditFlagDomain extends fflib_SObjectDomain {

    private List<Audit_Flag__c> auditFlags;

    public AuditFlagDomain() {
        super();
        this.auditFlags = new List<Audit_Flag__c>();
    } //constructor


    public void createFromResult(AuditEvalEngine.EvaluationResult result, String relatedIdField, fflib_SObjectUnitOfWork uow) {

        Audit_Flag__c auditFlag = new Audit_Flag__c();
        auditFlag.audit_rule__c = result.rule.auditRule.id;
        //auditFlag.category__c = result.rule.auditRule.category__c; formula field, looks up to rule
        //auditFlag.points__c = result.rule.auditRule.points__c; formula field, looks up to rule

        auditFlag.user__c = AuditEvalEngine.determinWhoToFlag(result);
        //if there is no one to flag, bail out
        if (auditFlag.user__c == null) return;

        AuditReportCardSelector arcs = new AuditReportCardSelector();
        Audit_Report_Card__c reportCard = arcs.selectCurrentForUser(auditFlag.user__c);
        if ( reportCard != null) {
            auditFlag.audit_report_card__c = reportCard.id;
        }
        //auditFlag.Status__c = 'New'; //Default value on rule

        if (result.scheduledFlag != null) {
            auditFlag.audit_scheduled_flag__c = result.scheduledFlag.id;
        }

        if ( relatedIdField != null) {
            auditFlag.put(relatedIdField, result.obj.get('Id'));
        }

        uow.registerNew(auditFlag);

    } //createFromResult

} //class