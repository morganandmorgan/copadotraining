public class AccountHighlightPanelExtension {

    public ApexPages.StandardController stdController;
    public Intake__c intakeRcd { get; set; }
    public Comments__c commentRcd { get; set; }
    public Questionnaire__c questRcd { get; set; }
    public AccountHighlightPanelExtension(ApexPages.StandardController stdControllerParam) {
    String intakeId = null;
    String questId = null;
    List<Intake__c> intakeList = new List<Intake__c>();
    List<Questionnaire__c> questList = new List<Questionnaire__c>();
    List<Comments__c> commentList = new List<Comments__c>();
    
        stdController = stdControllerParam;
        Schema.DescribeSObjectResult intakeIdPrefix = Intake__c.sObjectType.getDescribe();
        String intakekeyPrefix = intakeIdPrefix.getKeyPrefix();
        System.debug('intakekeyPrefix::::'+intakekeyPrefix);
        
        Schema.DescribeSObjectResult questIdPrefix = Questionnaire__c.sObjectType.getDescribe();
        String questkeyPrefix = questIdPrefix.getKeyPrefix();
        System.debug('questkeyPrefix::::'+questkeyPrefix);
        System.Debug('stdController.getId():::'+stdController.getId());
        if(stdController.getId() != null && intakekeyPrefix != null && intakekeyPrefix != '' && string.ValueOf(stdController.getId()).substring(0,3) == intakekeyPrefix) {
            intakeId = stdController.getId();
        } else if(stdController.getId() != null && questkeyPrefix != null && questkeyPrefix != '' && string.ValueOf(stdController.getId()).substring(0,3) == questkeyPrefix) {
            questId = stdController.getId();
        }
        if ( intakeId != null && intakeId != '' ) {
            intakeList = [SELECT Client__c, Case_Type__c, Status__c, Client__r.Name, Client__r.PersonEmail, Client__r.Phone, Client__r.BillingStreet, Client__r.BillingCity, Client__r.BillingState, Client__r.Description, Client__r.BillingPostalCode, Client__r.BillingCountry FROM Intake__c WHERE Id = :intakeId];
           
            if(intakeList.size() > 0) {
                intakeRcd = intakeList[0];
            }
           
            commentList = [SELECT Id, Comments__c, Intake__c, Date__c FROM Comments__c WHERE Intake__c =:intakeId ORDER BY Date__c desc LIMIT 1];

            if( commentList != null && commentList.size() > 0 ) {
                commentRcd = commentList[0];
            }
        }
        
        if(questId != null && questId != '') {
            questList = [SELECT Client__c, Intake__r.Case_Type__c, Status__c, Client__r.Name, Client__r.PersonEmail, Client__r.Phone, Client__r.BillingStreet, Client__r.BillingCity, Client__r.BillingState, Client__r.Description, Client__r.BillingPostalCode, Client__r.BillingCountry FROM Questionnaire__c WHERE Id = :questId];
           
            if(questList.size() > 0) {
                questRcd = questList[0];
            }
            commentList = [SELECT Id, Comments__c, Intake__c, Date__c FROM Comments__c WHERE Questionnaire__c =:questId ORDER BY Date__c desc LIMIT 1];

            
            if( commentList != null && commentList.size() > 0 ) {
                commentRcd = commentList[0];
            }
        }
        
    }
}