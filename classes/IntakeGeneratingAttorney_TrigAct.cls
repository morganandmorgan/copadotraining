public class IntakeGeneratingAttorney_TrigAct extends TriggerAction {

    private static final Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

    private static final Map<String, Set<String>> customSettingsByQueue = new Map<String, Set<String>>();
    private static final Map<String, Set<String>> customSettingsByMarketingSource = new Map<String, Set<String>>();

    private static final Set<String> allVenueSettings = new Set<String>();
    private static final Map<String, Set<String>> customSettingsByVenue = new Map<String, Set<String>>();

    private static final Set<String> allCountySettings = new Set<String>();
    private static final Map<String, Set<String>> customSettingsByCounty = new Map<String, Set<String>>();

    private static Boolean settingsInitialized = false;

    private final List<Intake__c> intakesToProcess = new List<Intake__c>();

    public IntakeGeneratingAttorney_TrigAct() {
        super();
        initCriteriaMappings();
    }

    private static void initCriteriaMappings() {
        if (!settingsInitialized) {
            for (Cisco_Queue_Setting__mdt cqs : ciscoQueueSettingsMap.values()) {
                addToQueueMap(cqs);
                addToMarketingMap(cqs);
                addToVenueMap(cqs);
                addToCountyMap(cqs);
            }
            settingsInitialized = true;
        }
    }

    private static void addToQueueMap(Cisco_Queue_Setting__mdt cqs) {
        String fieldValue = toLowerCase(cqs.Cisco_Queue__c);
        Set<String> customSettings = customSettingsByQueue.get(fieldValue);
        if (customSettings == null) {
            customSettings = new Set<String>();
            customSettingsByQueue.put(fieldValue, customSettings);
        }
        customSettings.add(cqs.DeveloperName);
    }

    private static void addToMarketingMap(Cisco_Queue_Setting__mdt cqs) {
        String fieldValue = toLowerCase(cqs.Marketing_Source__c);
        Set<String> customSettings = customSettingsByMarketingSource.get(fieldValue);
        if (customSettings == null) {
            customSettings = new Set<String>();
            customSettingsByMarketingSource.put(fieldValue, customSettings);
        }
        customSettings.add(cqs.DeveloperName);
    }

    private static void addToVenueMap(Cisco_Queue_Setting__mdt cqs) {
        String fieldValue = toLowerCase(cqs.Venue__c);
        if (String.isBlank(fieldValue)) {
            allVenueSettings.add(cqs.DeveloperName);
        }
        else {
            Set<String> customSettings = customSettingsByVenue.get(fieldValue);
            if (customSettings == null) {
                customSettings = new Set<String>();
                customSettingsByVenue.put(fieldValue, customSettings);
            }
            customSettings.add(cqs.DeveloperName);
        }
    }

    private static void addToCountyMap(Cisco_Queue_Setting__mdt cqs) {
        String fieldValue = toLowerCase(cqs.County__c);
        if (String.isBlank(fieldValue)) {
            allCountySettings.add(cqs.DeveloperName);
        }
        else {
            Set<String> customSettings = customSettingsByCounty.get(fieldValue);
            if (customSettings == null) {
                customSettings = new Set<String>();
                customSettingsByCounty.put(fieldValue, customSettings);
            }
            customSettings.add(cqs.DeveloperName);
        }
    }

    private static Map<String, Cisco_Queue_Setting__mdt> getCiscoQueueSettings()
    {
        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap =
            new Map<String, Cisco_Queue_Setting__mdt>();

        for (Cisco_Queue_Setting__mdt cqs : mmintake_CiscoQueueSettingsSelector.newInstance().selectAll())
        {
            ciscoQueueSettingsMap.put(cqs.DeveloperName, cqs);
        }

        return ciscoQueueSettingsMap;
    }

    public override Boolean shouldRun() {
        intakesToProcess.clear();

        if (!ciscoQueueSettingsMap.isEmpty()) {
            List<Intake__c> newList = getNewListForContext();
            Map<Id, Intake__c> intakeOldMap = (Map<Id, Intake__c>) triggerOldMap;

            for (Intake__c intake : newList) {
                if (intakeOldMap == null
                    || intake.Cisco_Queue__c != intakeOldMap.get(intake.Id).Cisco_Queue__c
                    || intake.Marketing_Source__c != intakeOldMap.get(intake.Id).Marketing_Source__c
                    || intake.Venue__c != intakeOldMap.get(intake.Id).Venue__c
                    || intake.County_of_incident__c != intakeOldMap.get(intake.Id).County_of_incident__c) {
                    intakesToProcess.add(intake);
                }
            }
        }

        return !intakesToProcess.isEmpty();
    }

    public override void doAction()
    {
        Map<String, List<Intake__c>> intakesByNewGeneratingAttorney = new Map<String, List<Intake__c>>();

        for (Intake__c intake : intakesToProcess)
        {
            Set<String> matchedCiscoQueueSettingsSet = customSettingsByQueue.get(toLowerCase(intake.Cisco_Queue__c));

            if (matchedCiscoQueueSettingsSet != null && !matchedCiscoQueueSettingsSet.isEmpty())
            {
                matchedCiscoQueueSettingsSet = matchedCiscoQueueSettingsSet.clone();

                Set<String> marketingSourceMatches = customSettingsByMarketingSource.get(toLowerCase(intake.Marketing_Source__c));
                if (marketingSourceMatches == null) {
                    marketingSourceMatches = new Set<String>();
                }
                matchedCiscoQueueSettingsSet.retainAll(marketingSourceMatches);

                Set<String> allVenueMatches = allVenueSettings.clone();
                Set<String> venueMatches = customSettingsByVenue.get(toLowerCase(intake.Venue__c));
                if (venueMatches != null) {
                    allVenueMatches.addAll(venueMatches);
                }
                matchedCiscoQueueSettingsSet.retainAll(allVenueMatches);

                Set<String> allCountyMatches = allCountySettings.clone();
                Set<String> countyMatches = customSettingsByCounty.get(toLowerCase(intake.County_of_incident__c));
                if (countyMatches != null) {
                    allCountyMatches.addAll(countyMatches);
                }
                matchedCiscoQueueSettingsSet.retainAll(allCountyMatches);

                if (!matchedCiscoQueueSettingsSet.isEmpty())
                {
                    Cisco_Queue_Setting__mdt matchedCiscoQueueSetting = getMostExactMatchingCiscoQueueSetting(intake, matchedCiscoQueueSettingsSet);

                    intake.Generating_Attorney__c = null;
                    String attorneyKey = toLowerCase(matchedCiscoQueueSetting.GeneratingAttorney__c);

                    if (String.isNotBlank(attorneyKey))
                    {
                        if (!intakesByNewGeneratingAttorney.containsKey(attorneyKey))
                        {
                            intakesByNewGeneratingAttorney.put(attorneyKey, new List<Intake__c>());
                        }

                        intakesByNewGeneratingAttorney.get(attorneyKey).add(intake);
                    }
                }
            }
        }

        if (!intakesByNewGeneratingAttorney.isEmpty()) {
            for (Contact c : [SELECT Id, Email FROM Contact WHERE Email IN :intakesByNewGeneratingAttorney.keySet()]) {
                List<Intake__c> intakesToSet = intakesByNewGeneratingAttorney.get(toLowerCase(c.Email));
                for (Intake__c intake : intakesToSet) {
                    intake.Generating_Attorney__c = c.Id;
                }
            }
        }
    }

    private static Cisco_Queue_Setting__mdt getMostExactMatchingCiscoQueueSetting(Intake__c intake, Set<String> matchedCustomSettingNames)
    {
        List<String> customSettingNamesList = new List<String>(matchedCustomSettingNames);
        Cisco_Queue_Setting__mdt matchedCiscoQueueSetting = null;

        if (customSettingNamesList.size() == 1)
        {
            matchedCiscoQueueSetting = ciscoQueueSettingsMap.get(customSettingNamesList.get(0));
        }
        else if (customSettingNamesList.size() > 1)
        {
            Integer maxScore = -1;

            for (String customSettingName : customSettingNamesList)
            {
                Cisco_Queue_Setting__mdt currentCiscoQueueSetting = ciscoQueueSettingsMap.get(customSettingName);

                Integer score = intake.Venue__c == currentCiscoQueueSetting.Venue__c ? 1 : 0;
                score += intake.County_of_Incident__c == currentCiscoQueueSetting.County__c ? 1 : 0;

                if (score > maxScore)
                {
                    maxScore = score;
                    matchedCiscoQueueSetting = currentCiscoQueueSetting;
                }
            }
        }

        return matchedCiscoQueueSetting;
    }

    private List<Intake__c> getNewListForContext() {
        if (isBefore() && isInsert()) {
            return (List<Intake__c>) triggerList;
        }
        else if (isBefore() && isUpdate()) {
            return (List<Intake__c>) triggerMap.values();
        }
        return new List<Intake__c>();
    }

    private static String toLowerCase(String s) {
        return s == null ? null : s.toLowerCase();
    }
}