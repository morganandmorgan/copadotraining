@isTest
private class CaseSpecificFieldsetRendererExtensionTst {

  @TestSetup
  private static void setup() {
    Account account = new Account(LastName='Test Account');
    insert account;
    
    // HACK: is there a way to create fieldsets just for testing?
    Intake__c intakeXarelto = new Intake__c(Client__c = account.Id, Case_Type__c = 'Xarelto', Litigation__c = 'Mass Tort');
    Intake__c intake = new Intake__c(Client__c = account.Id, Case_Type__c = 'TestType', Litigation__c = 'Mass Tort');

    insert new List<Intake__c>{ intakeXarelto, intake };
  }

  private static Intake__c getIntake(String caseType) {
    return [SELECT Id, client__c FROM Intake__c WHERE Case_Type__c = :caseType];
  }

  @isTest
  static void IntakeWithoutFieldset() {
    Intake__c intake = getIntake('TestType');

    ApexPages.StandardController controller = new ApexPages.StandardController(intake);
    Test.setCurrentPageReference(new PageReference('salesforce.com'));
    
    CaseSpecificFieldsetRendererExtension extension = new CaseSpecificFieldsetRendererExtension(controller);

    System.assertEquals('td_mass_tort_testtype', extension.CustomFieldSetName);
    System.assertEquals(null, extension.CustomFields);
  }

  @isTest
  static void IntakeWithFieldset() {
    Intake__c intakeXarelto = getIntake('Xarelto');

    ApexPages.StandardController controller = new ApexPages.StandardController(intakeXarelto);
    Test.setCurrentPageReference(new PageReference('salesforce.com'));

    CaseSpecificFieldsetRendererExtension extension = new CaseSpecificFieldsetRendererExtension(controller);

    System.assertEquals('td_mass_tort_xarelto', extension.CustomFieldSetName);
    System.assert(extension.CustomFields.size() > 0);
  }

  @isTest
  static void testQuestionnaire() {
    Intake__c intakeXarelto = getIntake('Xarelto');

    MM_Questionnaire__c qnr = new MM_Questionnaire__c();
    qnr.intake__c = intakeXarelto.id;
    insert qnr;

    ApexPages.StandardController controller = new ApexPages.StandardController(qnr);
    Test.setCurrentPageReference(new PageReference('salesforce.com'));

    CaseSpecificFieldsetRendererExtension extension = new CaseSpecificFieldsetRendererExtension(controller);

    System.assertEquals('td_mass_tort_xarelto', extension.CustomFieldSetName);
    System.assert(extension.CustomFields.size() > 0);
  }

  @isTest
  static void testTask() {
    Intake__c intakeXarelto = getIntake('Xarelto');

    litify_pm__Matter__c testMatter = new litify_pm__Matter__c ();
    testMatter.litify_pm__Client__c = intakeXarelto.client__c;
    insert testMatter;

    testMatter.intake__c = intakeXarelto.id;
    update testMatter;

    Task testTask = new Task();
    testTask.whatId = testMatter.id;
    insert testTask;

    ApexPages.StandardController controller = new ApexPages.StandardController(testTask);
    Test.setCurrentPageReference(new PageReference('salesforce.com'));

    CaseSpecificFieldsetRendererExtension extension = new CaseSpecificFieldsetRendererExtension(controller);

    System.assertEquals('td_mass_tort_xarelto', extension.CustomFieldSetName);
    System.assert(extension.CustomFields.size() > 0);

  }

}