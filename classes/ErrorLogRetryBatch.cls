/*
    //code to schedule first job
    Integer nextRunInMinutes = 5;

    ErrorLogRetryBatch retryBatch = new ErrorLogRetryBatch(nextRunInMinutes);

    DateTime nextShed = System.now();
    nextShed = nextShed.addMinutes(nextRunInMinutes);
    String chronExp = '' + nextShed.second() + ' ' + nextShed.minute() + ' ' + nextShed.hour() + ' ' + nextShed.day() + ' ' + nextShed.month() + ' ? ' + nextShed.year();
    System.schedule('MatterSyncFieldsToIntakeScheduled ' + nextShed.getTime(), chronExp, retryBatch);
*/
global without sharing class ErrorLogRetryBatch implements Database.Batchable<SObject>, Schedulable {
    /*  this class we retry code that failed and logged errors to Error_Log__c
        for now it will only retry IntakeConvertFlowQueueable failures, but others can be added
    */

    private Integer nextRunInMinutes;

    public ErrorLogRetryBatch() {
        this.nextRunInMinutes = null;
    } //constructor

    public ErrorLogRetryBatch(Integer nextRun) {
        this.nextRunInMinutes = nextRun;
    } //constructor

    //scheduled
    global void execute(SchedulableContext sc) {
        if (this.nextRunInMinutes == null) {
            Database.executeBatch(new ErrorLogRetryBatch(), 200);
        } else {
            Database.executeBatch(new ErrorLogRetryBatch(this.nextRunInMinutes), 200);
        }
    } //execute scheduled

    //batch
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //get the errors that need evaluation today or earlier
        return Database.getQueryLocator([SELECT id, recordId__c, source__c, tries__c
            FROM Error_Log__c
            WHERE source__c = 'IntakeConvertFlowQueueable' AND status__c = 'Pending']);
    } //start batch

    //batch
    global void execute(Database.BatchableContext bc, List<Error_log__c> errors) {
        //storage for the intake conversion errors
        Set<Id> convertIntakeIds = new Set<Id>();
        Map<Id,Error_Log__c> convertErrors = new Map<Id,Error_Log__c>();

        //process all the errors
        for (Error_Log__c error : errors) {
            //keep track of the conversion errors
            if (error.source__c == 'IntakeConvertFlowQueueable') {
                convertIntakeIds.add(error.recordId__c);
                convertErrors.put(error.recordId__c, error);
            } //if conversion error
        } //for

        //process the conversion errors, if there are any
        if (convertIntakeIds.size() != 0) {
            Id jobId = System.enqueueJob(new IntakeConvertFlowQueueable(convertIntakeIds, convertErrors));
        } //if conversion errors
    } //execute batch

    global void finish(Database.BatchableContext bc) {
        if (this.nextRunInMinutes != null) {
            //reschedule the batch job
            ErrorLogRetryBatch retryBatch = new ErrorLogRetryBatch(this.nextRunInMinutes);

            DateTime nextShed = System.now();
            nextShed = nextShed.addMinutes(this.nextRunInMinutes);
            String chronExp = '' + nextShed.second() + ' ' + nextShed.minute() + ' ' + nextShed.hour() + ' ' + nextShed.day() + ' ' + nextShed.month() + ' ? ' + nextShed.year();
            if (!Test.isRunningTest()) {
                System.schedule('ErrorLogRetryBatch ' + nextShed.getTime(), chronExp, retryBatch);
            }
        }
    } //finish

} //class