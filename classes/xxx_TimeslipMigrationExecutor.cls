/**
 *  script used to manage the time slip migration from x_CPTimeslipStaging__c to Task records related to litify_pm__Matter__c records
 *
 *  @usage String query = '<<query goes here>>';
 *         new mmlib_GenericBatch( xxx_TimeslipMigrationExecutor.class, query).setBatchSizeTo(100).execute();
 */
public class xxx_TimeslipMigrationExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        list<x_CPTimeslipStaging__c> cpTimeSlipsToUpdate  = new list<x_CPTimeslipStaging__c>();
        //list<Task> taskBasedTimeSlips = new list<Task>();
        list<Task> taskBasedTimeSlipsToInsert = new list<Task>();
        map<id, Task> taskBasedTimeSlipsToUpdateByIdMap = new map<id, Task>();

        x_CPTimeslipStaging__c cpTimeSlip = null;

        Set<String> matterLitifyLoadIdSet = new Set<String>();

        Set<String> timeKeeperUserEmailSet = new Set<String>();

        for ( SObject record : scope )
        {
            cpTimeSlip = (x_CPTimeslipStaging__c)record;

            matterLitifyLoadIdSet.add( cpTimeSlip.RelatedMatterLitifyLoadId__c );

            timeKeeperUserEmailSet.add( cpTimeSlip.timekeeper_user_email__c );
        }

        map<String, SObject> usersByEmailMap = mmlib_utils.generateSObjectMapByUniqueField( mmcommon_UsersSelector.newInstance().selectByUsernameLikeFields( timeKeeperUserEmailSet ), User.Email  );

        list<litify_pm__Matter__c> mattersWithRelatedTasks = findMattersWithRelatedTasks( matterLitifyLoadIdSet );

        map<String, SObject> mattersByLitifyLoadIdMap = mmlib_Utils.generateSObjectMapByUniqueField( mattersWithRelatedTasks, litify_pm__Matter__c.litify_load_id__c );

        TaskGenerator tskGen = new TaskGenerator();

        Task matterTimeSlipTask = null;

        Id timekeeperId = null;
        String timekeeperName = null;

        list<TaskToCPTimeSlipRelationship> newTaskToCPTimeSlipRelationshipList = new list<TaskToCPTimeSlipRelationship>();

        // Loop through the time slip records
        for ( SObject record : scope )
        {
            cpTimeSlip = (x_CPTimeslipStaging__c)record;

            // Is this cpTimeSlip for a matter that we have in our inventory?
            if ( ! mattersByLitifyLoadIdMap.containsKey( cpTimeSlip.RelatedMatterLitifyLoadId__c ) )
            {
                recordValidationFailure( cpTimeSlip, 'NORELATEDMATTER', 'Matter with ' + litify_pm__Matter__c.Litify_Load_Id__c + ' of \'' + cpTimeSlip.RelatedMatterLitifyLoadId__c + '\' was not found in the inventory.');
                cpTimeSlipsToUpdate.add( cpTimeSlip );
                system.debug( 'cpTimeSlipsToUpdate add : ' + cptimeSlip.id );
                continue;
            }

            system.debug( 'cpTimeSlip.timekeeper_user_email__c = ' + cpTimeSlip.timekeeper_user_email__c );
            system.debug( 'usersByEmailMap.containsKey( cpTimeSlip.timekeeper_user_email__c ) == ' + usersByEmailMap.containsKey( cpTimeSlip.timekeeper_user_email__c ));

            // Is this cpTimeSlip record complete?
            if ( String.isBlank( cpTimeSlip.timekeeper_user_email__c ) )
            {
                recordValidationFailure( cpTimeSlip, 'NOTIMEKEEPER', 'This record does not have a value for timekeeper_user_email__c.');
                cpTimeSlipsToUpdate.add( cpTimeSlip );
                system.debug( 'cpTimeSlipsToUpdate add : ' + cptimeSlip.id );
                continue;
            }

            // Is this cpTimeSlip timekeeper in the system?
            if ( ! usersByEmailMap.containsKey( cpTimeSlip.timekeeper_user_email__c.toLowerCase() ) )
            {
                recordValidationFailure( cpTimeSlip, 'NOACTIVEEMPLOYEE', 'User with email \'' + cpTimeSlip.timekeeper_user_email__c.toLowerCase() + '\' cannot be found.');
                cpTimeSlipsToUpdate.add( cpTimeSlip );
                system.debug( 'cpTimeSlipsToUpdate add : ' + cptimeSlip.id );
                continue;
            }

            timekeeperId = usersByEmailMap.get( cpTimeSlip.timekeeper_user_email__c.toLowerCase() ).id;
            timekeeperName = ((User)usersByEmailMap.get( cpTimeSlip.timekeeper_user_email__c.toLowerCase() )).name;

            // process through the task generator
            matterTimeSlipTask = tskGen.setCPTimeSlip( cpTimeSlip )
                                          .setParentMatter( (litify_pm__Matter__c)mattersByLitifyLoadIdMap.get( cpTimeSlip.RelatedMatterLitifyLoadId__c ) )
                                          .setTimekeeperId( timekeeperId )
                                          .setTimekeeperName( timekeeperName )
                                          .returnTask();

            // if a task is returned, then add it to the list to persist
            if ( matterTimeSlipTask != null )
            {
                //taskBasedTimeSlips.add( matterTimeSlipTask );
                if ( matterTimeSlipTask.id != null )
                {
                    // its an update
                    taskBasedTimeSlipsToUpdateByIdMap.put( matterTimeSlipTask.id, matterTimeSlipTask );
                }
                else
                {
                    taskBasedTimeSlipsToInsert.add( matterTimeSlipTask );
                }


                if ( matterTimeSlipTask.id == null )
                {
                    // the task record is new and we will need to eventually tie that new Task Id field value back to the CP Time Slip.  Store the relationship for now.
                    newTaskToCPTimeSlipRelationshipList.add( new TaskToCPTimeSlipRelationship().setCPTimeSlip( cpTimeSlip ).setTask( matterTimeSlipTask ) );
                    system.debug( 'newTaskToCPTimeSlipRelationshipList add : ' + cptimeSlip.id );
                }
                else
                {
                    cpTimeSlipsToUpdate.add( cpTimeSlip );
                    system.debug( 'cpTimeSlipsToUpdate add : ' + cptimeSlip.id );
                }
            }

            matterTimeSlipTask = null;
        }

        //list<Database.UpsertResult> results = database.upsert( taskBasedTimeSlips, false );
        list<Task> taskBasedTimeSlips = new list<Task>();

        taskBasedTimeSlips.addAll( taskBasedTimeSlipsToInsert );
        taskBasedTimeSlips.addAll( taskBasedTimeSlipsToUpdateByIdMap.values() );

        list<Database.UpsertResult> results = database.upsert( taskBasedTimeSlips, false );

        for ( Database.UpsertResult result : results )
        {
            system.debug( result );
        }

        // now that the taskBasedTimeSlips have been saved, the new Task records' id should be stored on the related CP Time Slip records
        for ( TaskToCPTimeSlipRelationship newTaskToCPTimeSlipRelationship : newTaskToCPTimeSlipRelationshipList )
        {
            system.debug( newTaskToCPTimeSlipRelationship.getCPTimeSlip().id + ' :: ' + newTaskToCPTimeSlipRelationship.getTaskTimeSlip().id );
            // need to assign the new, related task id to the CP time slip
            newTaskToCPTimeSlipRelationship.getCPTimeSlip().RelatedTimeSlipTaskRecord__c = String.valueOf(newTaskToCPTimeSlipRelationship.getTaskTimeSlip().id);
            newTaskToCPTimeSlipRelationship.getCPTimeSlip().MigrationStatus__c = 'Successful';
            newTaskToCPTimeSlipRelationship.getCPTimeSlip().MigratedTime__c = datetime.now();
            cpTimeSlipsToUpdate.add( newTaskToCPTimeSlipRelationship.getCPTimeSlip() );
            system.debug( 'cpTimeSlipsToUpdate add : ' + newTaskToCPTimeSlipRelationship.getCPTimeSlip().id );
        }

        // need to save all of x_CPTimeslipStaging__c records next.  They have been updated with comments as needed.
        database.update( cpTimeSlipsToUpdate, false );
    }

    private void recordValidationFailure(x_CPTimeslipStaging__c cpTimeSlip, String statusCode, String reason)
    {
        cpTimeSlip.MigrationStatus__c = 'Failed = ' + statusCode;
        cpTimeSlip.ProcessingAnomalyComments__c = (string.isBlank(cpTimeSlip.ProcessingAnomalyComments__c) ? '' : cpTimeSlip.ProcessingAnomalyComments__c ) + reason + '\n';
    }

    private list<litify_pm__Matter__c> findMattersWithRelatedTasks(Set<String> matterLitifyLoadIdSet)
    {
        list<litify_pm__Matter__c> matters = [select id, name, referencenumber__c, CP_Case_Number__c, litify_load_id__c, litify_pm__Principal_Attorney__c
                                                   , (select Id, WhoId, WhatId, WhoCount, WhatCount, Subject, ActivityDate, Status, Priority, IsHighPriority, OwnerId
                                                           , Description, Type, IsDeleted, AccountId, IsClosed, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById
                                                           , SystemModstamp, IsArchived, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime
                                                           , IsReminderSet, ConnectionReceivedId, ConnectionSentId, RecurrenceActivityId, IsRecurrence, RecurrenceStartDateOnly
                                                           , RecurrenceEndDateOnly, RecurrenceTimeZoneSidKey, RecurrenceType, RecurrenceInterval, RecurrenceDayOfWeekMask
                                                           , RecurrenceDayOfMonth, RecurrenceInstance, RecurrenceMonthOfYear, RecurrenceRegeneratedType, TaskSubtype
                                                           , litify_pm__Default_Matter_Task__c, litify_pm__Document_Link__c, CP_Posted_Date__c, PC_Signup_Url__c, Activity_Purpose__c
                                                           , Detail__c, Event_Type__c, Next_Step__c, Performed_By__c, Rate__c, Task_Due_Date__c, Timekeeper__c
                                                           , litify_pm__AssociatedObjectName__c, Completed_Date__c, Time_Slip_Detail__c, Litify_Load_Id__c
                                                        from Tasks)
                                                from litify_pm__Matter__c
                                               where litify_load_id__c = :matterLitifyLoadIdSet];

        return matters;
    }

    public class TaskGenerator
    {
        private x_CPTimeslipStaging__c cpTimeSlip;
        private litify_pm__Matter__c parentMatter;
        private list<Task> tasksList = new list<Task>();
        private id timekeeperId;
        private String timekeeperName;

        public TaskGenerator setCPTimeSlip(x_CPTimeslipStaging__c cpTimeSlip)
        {
            this.cpTimeSlip = cpTimeSlip;
            return this;
        }

        public TaskGenerator setParentMatter(litify_pm__Matter__c parentMatter)
        {
            this.parentMatter = parentMatter;

            if ( parentMatter.tasks != null
                && ! parentMatter.tasks.isEmpty() )
            {
                this.tasksList.addAll( parentMatter.tasks );
            }

            return this;
        }

        public TaskGenerator setTimekeeperId(Id userId)
        {
            this.timekeeperId = userId;
            return this;
        }

        public TaskGenerator setTimekeeperName( String timekeeperName )
        {
            this.timekeeperName = timekeeperName;
            return this;
        }

        private Task findTaskIfPresent()
        {
            Task taskTimeSlip = null;

            for ( Task aTask : this.tasksList )
            {
                if ( aTask.ActivityDate == date.valueOf( this.cpTimeSlip.timeslip_date__c.substringbefore(' ') )
                    && aTask.OwnerId == this.timekeeperId
                    && aTask.Performed_By__c == this.parentMatter.litify_pm__Principal_Attorney__c
                    && aTask.Time_Slip_Detail__c == this.cpTimeSlip.time_code_sk_short_desc__c
//                    && aTask.subject.equalsIgnoreCase(this.cpTimeSlip.time_code_sk_detail_desc__c)
                    )
                {
                    taskTimeSlip = aTask;
                    break;
                }
            }

            return taskTimeSlip;
        }

        public Task returnTask()
        {
            if ( this.parentMatter == null )
            {
                this.cpTimeSlip.ProcessingAnomalyComments__c = 'Unable to find related matter';

                return null;
            }

            Task taskTimeSlip = findTaskIfPresent();

            if ( taskTimeSlip != null )
            {
                // existing time slip was found, update the CP Time Slip Staging record
                cpTimeSlip.RelatedTimeSlipTaskRecord__c = String.valueOf(taskTimeSlip.id);
                cpTimeSlip.MigrationStatus__c = 'Successful';
                cpTimeSlip.MigratedTime__c = datetime.now();
            }
            else
            {
                // no task exists,
                taskTimeSlip = new Task();

                taskTimeSlip.WhatId = this.parentMatter.id;
                taskTimeSlip.Subject = this.cpTimeSlip.time_code_sk_detail_desc__c;
                taskTimeSlip.ActivityDate = date.valueOf( this.cpTimeSlip.timeslip_date__c.substringbefore(' ') );
                taskTimeSlip.CreatedDate = datetime.valueOf(this.cpTimeSlip.create_date__c);
                taskTimeSlip.Completed_Date__c = taskTimeSlip.CreatedDate;
                taskTimeSlip.Status = 'Completed';
                taskTimeSlip.Priority = 'Normal';
                taskTimeSlip.Activity_Purpose__c = 'Legacy';
                taskTimeSlip.OwnerId = this.timekeeperId;
                taskTimeSlip.TaskSubtype = 'Task';
                taskTimeSlip.Timekeeper__c = this.timekeeperName;
                taskTimeSlip.Performed_By__c = this.parentMatter.litify_pm__Principal_Attorney__c;
                taskTimeSlip.Time_Slip_Detail__c = this.cpTimeSlip.time_code_sk_short_desc__c;
                taskTimeSlip.TimeSlip_Hours__c = Decimal.valueOf( this.cpTimeSlip.hours__c );
                taskTimeSlip.Detail__c = this.cpTimeSlip.time_code_sk_detail_desc__c;
                taskTimeSlip.Litify_Load_Id__c = this.cpTimeSlip.SfdcLoadId__c;
                tasktimeSlip.Description = this.cpTimeSlip.general_notes__c;

                // In this scenario, the cpTimeSlip.RelatedTimeSlipTaskRecord__c will be updated from the taskTimeSlip.id
                //  later on once the new taskTimeSlip record is saved.
                //cpTimeSlip.MigrationStatus__c = 'Successful';
                //cpTimeSlip.MigratedTime__c = datetime.now();
            }

            return taskTimeSlip;
        }
    }

    public class TaskToCPTimeSlipRelationship
    {
        private x_CPTimeslipStaging__c cpTimeSlip;
        private Task taskTimeSlip;

        public TaskToCPTimeSlipRelationship setCPTimeSlip(x_CPTimeslipStaging__c cpTimeSlip)
        {
            this.cpTimeSlip = cpTimeSlip;
            return this;
        }

        public TaskToCPTimeSlipRelationship setTask(Task taskTimeSlip)
        {
            this.taskTimeSlip = taskTimeSlip;
            return this;
        }

        public x_CPTimeslipStaging__c getCPTimeSlip()
        {
            return this.cpTimeSlip;
        }

        public Task getTaskTimeSlip()
        {
            return this.taskTimeSlip;
        }
    }
}