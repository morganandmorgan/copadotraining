/**
 * RoleDomainTest
 * @description Test for Role Domain class.
 * @author Jeff Watson
 * @date 1/18/2019
 */
@IsTest
public with sharing class RoleDomainTest {

    private static Account account;
    private static Account officeAccount;
    private static Account personAccount;
    private static Contact contact;
    private static Incident__c incident;
    private static IncidentInvestigationEvent__c incidentInvestigationEvent;
    private static Intake__c intake;
    private static User user;
    private static litify_pm__Role__c role;
    private Static litify_pm__Role__c providerRole;
    private static litify_pm__Matter__c matter;
    private static Map<String, SObject> roleMap;
    private static litify_pm__Damage__c damage;
    private static litify_pm__Firm__c firm;
    private static litify_pm__Case_Type__c caseType;

    static {

        Trigger_Control__c triggerControl = new Trigger_Control__c(Name = 'Default Controls');
        insert triggerControl;

        account = TestUtil.createAccountBusinessMorganAndMorgan('Biz Account');
        insert account;

        officeAccount = TestUtil.createAccount('MM Office');
        officeAccount.Type = 'MM Office';
        insert officeAccount;

        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        contact = TestUtil.createContact(account.Id);
        insert contact;

        incident = TestUtil.createIncident();
        insert incident;

        incidentInvestigationEvent = TestUtil.createIncidentInvestigationEvent(incident);

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        user = TestUtil.createUser();
        insert user;

        caseType = TestUtil.createCaseType();
        insert caseType;

        matter = TestUtil.createMatter(account);
        matter.litify_pm__Case_Type__c = caseType.Id;
        matter.Assigned_Office_Location__c = officeAccount.Id;
        matter.AssignedToMMBusiness__c = account.Id;
        insert matter;

        role = TestUtil.createRole(matter, account);
        insert role;

        providerRole = TestUtil.createRole(matter, account);
        providerRole.litify_pm__Parent_Role__c = role.Id;
        insert providerRole;

        damage = TestUtil.createDamage(matter);
        damage.litify_pm__Provider__c = providerRole.Id;
        insert damage;

        firm = TestUtil.createFirm('bencrump.com');
        insert firm;
    }

    public static testMethod void RoleDomain_Ctor() {

        // Arrange + Act
        Test.startTest();
        RoleDomain roleDomain = new RoleDomain(new List<litify_pm__Role__c> {role});
        Test.stopTest();

        // Assert
        System.assert(roleDomain != null);
    }

    public static testMethod void RoleDomain_AfterUpdate() {

        // Arrange
        RoleDomain roleDomain = new RoleDomain(new List<litify_pm__Role__c> {role});

        // Act
        Test.startTest();
        roleDomain.onAfterUpdate(null);
        Test.stopTest();

        // Assert
        System.assert(roleDomain != null);
    }

    public static testMethod void RoleDomain_BeforeDelete() {

        // Arrange
        RoleDomain roleDomain = new RoleDomain(new List<litify_pm__Role__c> {role});

        // Act
        Test.startTest();
        roleDomain.onBeforeDelete();
        Test.stopTest();

        // Assert
        System.assert(roleDomain != null);
    }

    public static testMethod void RoleDomain_UpdateMatterDeceasedPerRole() {

        // Arrange
        RoleDomain roleDomain = new RoleDomain(new List<litify_pm__Role__c> {role});

        // Act + Assert
        Test.startTest();
        roleDomain.updateMatterDeceasedPerRole();
        Test.stopTest();

        // Assert
        System.assert(roleDomain != null);
    }
}