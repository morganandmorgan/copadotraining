@isTest
public class Test_FetchAttachmentsBatch {
	
    @testSetup
    static void setupRecords() {
        Account client = TestUtil.createPersonAccount( 'name', 'Account' );
        insert client;
        Intake__c intakeRecord = TestUtil.createIntake();
        intakeRecord.Client__c = client.Id;
        insert intakeRecord;
        
        // create office account
        Account officeAccount = TestUtil.createAccount('MM Office');
        officeAccount.Type = 'MM Office';
        insert officeAccount;
        RecordType matterRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Matter__c'
                                    AND DeveloperName = 'Personal_Injury'
                                   ];
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account'
                                  AND DeveloperName = 'Person_Account'
                                  LIMIT 1
                                 ];
        
        List <Account> acc = new List < account > { new Account(LastName = 'TestAcc 1', RecordTypeId = acctRcdType.Id)};
        insert acc;
        User testUser = createUser();
        insert testUser;
        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c(Name = 'Slip and Fall'); 
        insert caseType;
    }
    
    static testMethod void fetchAttachmentsUnitTestOne() {
        try {
            
            RecordType matterRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Matter__c'
                                        AND DeveloperName = 'Personal_Injury'
                                       ];
            Intake__c intakeRecord = [SELECT Id, Name FROM Intake__c Limit 1];
            List <Account> acc = [SELECT Id, Name FROM Account Where LastName = 'TestAcc 1'];
            Account officeAccount = [SELECT Id, Name FROM Account Where Type = 'MM Office'];
            User testUser = [SELECT Id, Name FROM User Limit 1];
            litify_pm__Case_Type__c caseType = [SELECT Id, Name FROM litify_pm__Case_Type__c Limit 1];
            litify_pm__Matter__c matter = new litify_pm__Matter__c();
            matter.Intake__c = intakeRecord.Id;
            matter.RecordTypeId = matterRcdType.Id;
            matter.ReferenceNumber__c = '999999056';
            matter.litify_pm__Client__c = acc[0].Id;
            matter.litify_pm__Principal_Attorney__c = testUser.Id;
            matter.litify_pm__Open_Date__c = Date.today();
            matter.litify_pm__Case_Type__c = caseType.Id;
            matter.Assigned_Office_Location__c = officeAccount.Id;
            
            Set<Id> filteredIdSet = new Set<Id>();
            
            SpringCMApiManagerMock mock = new SpringCMApiManagerMock();
            SpringCMApiManager api = new SpringCMApiManager();
            
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, mock);
            	insert matter;
            	filteredIdSet.add( matter.Id );
            	FetchAttachmentsBatch batchInstance = new FetchAttachmentsBatch( filteredIdSet );
            	Database.executeBatch( batchInstance );
            Test.stopTest(); 
        }
        catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    
    public static User createUser() {
        Integer userNum = 1;
        return new User(
            IsActive = true,
            FirstName = 'FirstName ' + userNum,
            LastName = 'LastName ' + userNum,
            //Username = 'apex.temp.test.user@sample.com.' + userNum,
            Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@xyz.com',
            Email = 'apex.temp.test.user@sample.com.' + userNum,
            Alias = 'alib' + userNum,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            Territory__c = null
        );
    }
}