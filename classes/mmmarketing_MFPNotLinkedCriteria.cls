/**
 *  mmmarketing_MFPNotLinkedCriteria
 */
public with sharing class mmmarketing_MFPNotLinkedCriteria
    implements mmlib_ICriteria
{
    private list<Marketing_Financial_Period__c> records = new list<Marketing_Financial_Period__c>();

    public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
    {
        if (records != null
            && Marketing_Financial_Period__c.SObjectType == records.getSobjectType()
            )
        {
            this.records.addAll( (list<Marketing_Financial_Period__c>) records);
        }

        return this;
    }

    public list<SObject> run()
    {
        list<Marketing_Financial_Period__c> qualifiedRecords = new list<Marketing_Financial_Period__c>();

        for (Marketing_Financial_Period__c record : this.records)
        {
            if (record.Marketing_Campaign__c == null
                || record.Marketing_Source__c == null
                )
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}