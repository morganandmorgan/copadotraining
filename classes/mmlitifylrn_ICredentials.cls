public interface mmlitifylrn_ICredentials extends fflib_ISObjectDomain
{
    void authenticateForFirm( litify_pm__Firm__c firm, string username, string password, mmlib_ISObjectUnitOfWork uow );
    void refreshSessionForFirm( litify_pm__Firm__c firm, mmlib_ISObjectUnitOfWork uow );
    string getSessionTokenForFirm( litify_pm__Firm__c firm );
    set<Decimal> firmExternalIdSet();
}