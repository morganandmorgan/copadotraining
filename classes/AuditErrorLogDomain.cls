/**
 * AuditErrorLogDomain
 * @description Domain class for Audit_Error_Log__c SObject.
 * @author Matt Terrill
 * @date 10/14/2019
 */
public without sharing class AuditErrorLogDomain extends fflib_SObjectDomain {

    public AuditErrorLogDomain() {
        super();
    } //constructor

    public void logError(Exception e) {
        Audit_Error_Log__c errorLog = new Audit_Error_Log__c();

        errorLog.message__c = e.getMessage().left(255);
        errorLog.stack_trace__c = e.getStackTraceString().left(1024);
        errorLog.type__c = e.getTypeName().left(50);
        errorLog.notification_sent__c = errorNotification();

        insert errorLog;
    } //upsertRule


    public Boolean errorNotification() {
        try {

            AuditErrorLogSelector aels = new AuditErrorLogSelector();
            DateTime hourAgo = DateTime.now().addHours(-1);

            //I'm not using the selector here because of permissions.  It is always 'with sharing'
            //we need this code to always work
            //List<Audit_Error_Log__c> errors = aels.selectRecent(hourAgo);
            List<Audit_Error_Log__c> errors = [SELECT id, notification_sent__c, createdDate FROM Audit_Error_Log__c WHERE createdDate >= :hourAgo];

            //determine if we need to send a notification (did we send one in the last hour?)
            Boolean needToSend = true;
            for (Audit_Error_Log__c errorLog : errors) {
                if (errorLog.notification_sent__c) {
                    needToSend = false;
                    break;
                }
            } //for errors

            if (needToSend) {
                String org = UserInfo.getUserName().substringAfterLast('.');

                String message;
                if (org != 'com') {
                    message = 'Sandbox - ' + org + ': ';
                } else {
                    message = 'PRODUCTION: ';
                }

                if ( errors.size() == 0) {
                    message = message + 'An error has occurred.';
                } else {
                    message = message + 'There have been ' + String.valueOf(errors.size()+1) + ' errors in the last hour.';
                }

                //send the notification
                String slackUrl = 'https://hooks.slack.com/services/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';

                Map<String,String> payload = new Map<String,String>();

                payload.put('text',message);

                Http h = new Http();
                HttpRequest req = new HttpRequest();

                req.setEndpoint(slackUrl);
                req.setMethod('POST');
                req.setBody( JSON.serialize(payload) );

                HttpResponse res = h.send(req);

                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            //we don't want our notification to stop the error from getting logged
            return false;
        } //try/catch

    } //errorNotification

} //class