@isTest
private class mmcommon_UserIsInvstgtrChngdCriteriaTest
{
    private static final String TEST_TERRITORY = 'Test Territory';

    @isTest
    private static void newUserMeetsCriteria()
    {
        User newRecord = new User(Territory__c = TEST_TERRITORY, id = fflib_IDGenerator.generate(User.SObjectType));

        Test.startTest();

        mmcommon_UserIsInvstgtrChngdCriteria criteria = new mmcommon_UserIsInvstgtrChngdCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.size() == 1);
        System.assertEquals( resultRecords[0].id, newRecord.id );
    }

    @isTest
    private static void newUserFailsCriteria()
    {
        User newRecord = new User(Territory__c = null, id = fflib_IDGenerator.generate(User.SObjectType));

        Test.startTest();

        mmcommon_UserIsInvstgtrChngdCriteria criteria = new mmcommon_UserIsInvstgtrChngdCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.isEmpty() );
    }

    @isTest
    private static void changedUsersMeetsCriteria()
    {
        Id userId = fflib_IDGenerator.generate(User.SObjectType);

        User newRecord = new User(Territory__c = TEST_TERRITORY, id = userId);
        User oldRecord = new User(Territory__c = null, id = userId);

        Test.startTest();

        mmcommon_UserIsInvstgtrChngdCriteria criteria = new mmcommon_UserIsInvstgtrChngdCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        Map<id, User> existingRecordMap = new Map<id, User>();
        existingRecordMap.put( userId, oldRecord );

        criteria.setExistingRecords( existingRecordMap );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.size() == 1);
        System.assertEquals( resultRecords[0].id, newRecord.id );
    }

    @isTest
    private static void changedUsersFailsCriteria()
    {
        Id userId = fflib_IDGenerator.generate(User.SObjectType);

        User newRecord = new User(Territory__c = null, id = userId);
        User oldRecord = new User(Territory__c = TEST_TERRITORY, id = userId);

        Test.startTest();

        mmcommon_UserIsInvstgtrChngdCriteria criteria = new mmcommon_UserIsInvstgtrChngdCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        Map<id, User> existingRecordMap = new Map<id, User>();
        existingRecordMap.put( userId, oldRecord );

        criteria.setExistingRecords( existingRecordMap );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.isEmpty() );
    }



    @isTest
    private static void unchangedUserFailsCriteria()
    {
        Id userId = fflib_IDGenerator.generate(User.SObjectType);

        User newRecord = new User(Territory__c = TEST_TERRITORY, id = userId);
        User oldRecord = new User(Territory__c = TEST_TERRITORY, id = userId);

        Test.startTest();

        mmcommon_UserIsInvstgtrChngdCriteria criteria = new mmcommon_UserIsInvstgtrChngdCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        Map<id, User> existingRecordMap = new Map<id, User>();
        existingRecordMap.put( userId, oldRecord );

        criteria.setExistingRecords( existingRecordMap );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.isEmpty() );
    }
}