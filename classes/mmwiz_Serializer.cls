public with sharing class mmwiz_Serializer
{
    private static Map<String, SObject> wizardNameMap = null;

    private static Map<String, SObject> getWizardNameMap()
    {
        if (wizardNameMap == null)
        {
            wizardNameMap =
                mmlib_Utils.generateSObjectMapByUniqueField(
                    mmwiz_WizardsSelector.newInstance().selectAllBeingActive(),
                    Wizard__mdt.DeveloperName);
        }

        return wizardNameMap;
    }

    public static String serialize(mmwiz_AbstractBaseModel model)
    {
        model.prepareForSerialization();
        return JSON.serialize(model.serializedDataMap, true);  // true ignores null fields.
    }

    public static List<mmwiz_AbstractBaseModel> deserialize(Set<String> desiredList)
    {
        List<mmwiz_AbstractBaseModel> modelList = new List<mmwiz_AbstractBaseModel>();

        if (desiredList != null && !desiredList.isEmpty())
        {
            for (String wizardName : desiredList)
            {
                if (getWizardNameMap().containsKey(wizardName))
                {
                    mmwiz_AbstractBaseModel model = instantiateProperModel((Wizard__mdt) getWizardNameMap().get(wizardName));
                    modelList.add(model);

                    // Recursively instantiate the children.
                    if (model != null && model.childTokens != null && !model.childTokens.isEmpty())
                    {
                        model.childModels.addAll( deserialize( new Set<String>(model.childTokens) ) );
                    }

                    if (model != null && model.actionTokens != null && !model.actionTokens.isEmpty())
                    {
                        model.actionModels.addAll( deserializeActions(model.actionTokens) );
                    }
                }
            }
        }

        return modelList;
    }

    public static List<mmwiz_AbstractBaseModel> deserializeActions(List<String> desiredList)
    {

        List<mmwiz_AbstractBaseModel> actionList = new List<mmwiz_AbstractBaseModel>();

        if (desiredList != null && ! desiredList.isEmpty())
        {
            for (String wizardName : desiredList)
            {
                if (getWizardNameMap().containsKey(wizardName))
                {
                    mmwiz_AbstractBaseModel action = instantiateProperModel((Wizard__mdt) getWizardNameMap().get(wizardName));
                    actionList.add(action);
                }
                else
                {
                    actionList.add(null);
                }
            }
        }

        return actionList;
    }

    private static mmwiz_AbstractBaseModel instantiateProperModel(Wizard__mdt unitWizard)
    {
        mmwiz_AbstractBaseModel model = (mmwiz_AbstractBaseModel) Type.forName(unitWizard.Type__c).newInstance();
        model.initialize(unitWizard);
        return model;
    }
}