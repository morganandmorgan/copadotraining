/*
    MCT: EP-860 LIVR Search Results Modifications
    Removed lawsuit and matter team code (per Keith)
    Added code to handle related record fields.
    Reformatted code.
    9/18/2019
*/
public class mmpsc_SelectableSObjectController {
        private static List<SelectField> accountFieldList = null;
        private static List<SelectField> intakeFieldList = null;
        private static List<SelectField> matterFieldList = null;

        private static Map<Id, SObject> matterByIntakeIdMap = null;

    @AuraEnabled
    public static List<RecordData> getAccounts(List<Id> idList) {
        Schema.FieldSet accountFieldSet = Account.SObjectType.getDescribe().FieldSets.getMap().get(mmpsc_PeopleSearchComponentSettings.getAccountFieldset());
        Schema.FieldSet intakeFieldSet = Intake__c.SObjectType.getDescribe().FieldSets.getMap().get(mmpsc_PeopleSearchComponentSettings.getIntakeFieldset());
        Schema.FieldSet matterFieldSet = litify_pm__Matter__c.SObjectType.getDescribe().FieldSets.getMap().get(mmpsc_PeopleSearchComponentSettings.getMatterFieldset());

        accountFieldList = getFieldsetFields(new Account(), accountFieldSet);
        intakeFieldList = getFieldsetFields(new Intake__c(), intakeFieldSet);
        matterFieldList = getFieldsetFields(new litify_pm__Matter__c(), matterFieldSet);

        Map<Id, Account> accountMap = new Map<Id, Account>();
        for(String i : idList) {
            accountMap.put(i, null);
        }
        accountMap = new Map<Id, Account>(mmcommon_AccountsSelector.newInstance().selectWithFieldsetById(accountFieldSet, accountMap.keyset()));

        Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>();
        for (Intake__c ink : mmintake_IntakesSelector.newInstance().selectWithFieldsetByClientId(intakeFieldSet, accountMap.keySet())) {
            if (ink.Status__c != 'Duplicate') intakeMap.put(ink.Id, ink);
        }

        matterByIntakeIdMap =
            mmlib_Utils.generateOneToOneSObjectMapByIdField(
                mmmatter_MattersSelector.newInstance().selectWithFieldsetByIntake(matterFieldSet, intakeMap.keySet()),
                litify_pm__Matter__c.Intake__c);

         //System.debug('<ojs> accountMap:\n' + JSON.serializePretty(accountMap));
         //System.debug('<ojs> intakeMap:\n' + JSON.serializePretty(intakeMap));
         //System.debug('<ojs> matterList:\n' + JSON.serializePretty(matterList));

        Map<Id, List<Intake__c>> intakesByAccountIdMap = new Map<Id, List<Intake__c>>();
        for (Intake__c intake : intakeMap.values()) {
            if (!intakesByAccountIdMap.containsKey(intake.Client__c)) {
                intakesByAccountIdMap.put(intake.Client__c, new List<Intake__c>());
            }
            intakesByAccountIdMap.get(intake.Client__c).add(intake);
        }

        // System.debug('<ojs> intakesByAccountIdMap:\n' + JSON.serializePretty(intakesByAccountIdMap));
        // System.debug('<ojs> lawsuitByIntakeIdMap:\n' + JSON.serializePretty(lawsuitByIntakeIdMap));
        // System.debug('<ojs> matterByIntakeIdMap:\n' + JSON.serializePretty(matterByIntakeIdMap));

        List<RecordData> ret = new List<RecordData>();

        for (Id acctId : idList) {
            Account acct = accountMap.get(acctId);

            RecordData ad = new RecordData();
            ad.id = acct.Id;
            ad.name = acct.Name;
            ad.fieldValues = getSObjectFieldValues(acct, accountFieldList);

            if (intakesByAccountIdMap.containsKey(acct.Id)) {
                // go in backwards order
                List<Intake__c> intks = intakesByAccountIdMap.get(acct.Id);
                for (Integer i = intks.size() - 1; i >= 0; i--) {
                    ad.childRecords.add(createChildObject(intks[i]));
                }
            }

            ret.add(ad);
        }

        System.debug('<ojs> ret:\n' + JSON.serializePretty(ret));
        return ret;
    } //getAccounts


    private static RecordData createChildObject(Intake__c intake) {
        RecordData co = new RecordData();
        co.Id = intake.Id;
        co.fieldValues = getSObjectFieldValues(intake, intakeFieldList);

        SObject matterSobj = matterByIntakeIdMap.get(intake.Id);
        if (matterSobj != null) {
            RecordData rd = new RecordData();
            rd.Id = matterSobj.Id;
            rd.fieldValues = getSObjectFieldValues(matterSobj, matterFieldList);
            //rd.fieldValues.addAll(getTeamMembers(teamMemberMap.get(matterSobj.Id)));
            co.childRecords.add(rd);
        }

        return co;
    } //createChildObject

    @testvisible
    private static List<DisplayField> getSObjectFieldValues(SObject sobj, List<SelectField> fieldList) {
        List<DisplayField> result = new List<DisplayField>();
        Map<String, Schema.SObjectField> mapp = sobj.getSObjectType().getDescribe().fields.getMap();

        for (SelectField fld : fieldList) {
            if(fld.fieldName != 'Owner_Ext__c') {
                result.add(new DisplayField(fld, getFieldValue(sobj, fld.fieldName)));
            }
        }

        return result;
    } //getSObjectFieldValues

    @testvisible
    private static String getRelatedFieldName(String relatedName) {
        String result = relatedName;
        if ( relatedName.containsIgnoreCase('__r')) {
            result = relatedName.toLowerCase().replace('__r','__c');
        } else {
            //Out of the box relationships work... wierd
            //OwnerId is the fields name, but Owner.name is the related fields reference, so lets account for both
            if ( relatedName.right(2).toLowerCase() != 'id' ) {
                result = relatedName + 'Id';
            }
        }
        return result;
    } //getRelatedFieldName

    private static Schema.DisplayType getDisplayType(SObject obj, String fieldName ) {
        Schema.DisplayType result;
        Schema.SObjectType objType;
        String objField;

        if ( fieldName.contains('.') ) {
            //get the relational field name
            String relatedName = getRelatedFieldName(fieldName.split('\\.')[0]);

            //get the object it is related to
            objType = obj.getsObjectType().getDescribe().fields.getMap().get(relatedName).getDescribe().getReferenceTo()[0];
            //get the name of the field on that object
            objField = fieldName.split('\\.')[1];
        } else {
            objType = obj.getsObjectType();
            objField = fieldName;
        } //contains .(dot)

        if (objType.getDescribe().fields.getMap().get(objField) != null) {
            return objType.getDescribe().fields.getMap().get(objField).getDescribe().getType();
        } else {
            return Schema.DisplayType.String;            
        }
    } //getDisplayType


    private Static Object getField(SObject obj, String fieldName) {
        if ( fieldName.contains('.') ) {
            String objName = fieldName.split('\\.')[0];
            //Out of the box relationships work... wierd
            //OwnerId is the fields name, but Owner.name is the related fields reference, so lets account for both
            if ( objName.right(2).toLowerCase() == 'id') {
                objName = objName.toLowerCase().replace('id','');
            }
            String objField = fieldName.split('\\.')[1];

            //make sure there is a related record
            if ( obj.getSObject(objName) != null) {
                return obj.getSObject(objName).get(objField);
            } else {
                return null;
            }
        } else {
            return obj.get(fieldName);
        }
    } //getField


    @testvisible
    private static String getFieldValue(SObject sobj, string fieldName) {
        //Schema.DescribeFieldResult fldRes = fld.getDescribe();
        Schema.DisplayType fieldType = getDisplayType(sobj, fieldName);
        if (getField(sobj,fieldName) == null) return '';
        if (fieldType == Schema.DisplayType.DateTime) return ((DateTime)getField(sobj,fieldName)).format();
        if (fieldType == Schema.DisplayType.Date) return ((Date)getField(sobj,fieldName)).format();
        return String.valueOf(getField(sobj,fieldName));
    } //getFieldValue


    private static String getRelatedFieldLabel(SObject obj, String fieldName) {

        Schema.SObjectType otype = obj.getSobjectType();
        Schema.DescribeSObjectResult odesc = otype.getDescribe();
        Map<String, Schema.SObjectField> ofields = odesc.fields.getMap();

        return ofields.get(fieldName).getDescribe().getLabel();

    } //getRelatedFieldLabel


    @testvisible
    private static List<SelectField> getFieldsetFields(SObject obj, Schema.FieldSet fs) {
        List<SelectField> fieldNameList = new List<SelectField>();

        for (Schema.FieldSetMember fsm : fs.getFields()) {

            //for related fields, we want the label to be the relation name
            if ( fsm.getFieldPath().contains('.') ) {
                //get the relational field name
                String relatedName = getRelatedFieldName(fsm.getFieldPath().split('\\.')[0]);
                fieldNameList.add(new SelectField(getRelatedFieldLabel(obj, relatedName) ,fsm.getFieldPath()));
            } else {
                fieldNameList.add(new SelectField(fsm));
            }
        } //for

        return fieldNameList;
    } //getFieldsetFields

    @testvisible
    private static String formatPhone(String phone) {

        if(String.isEmpty(phone)) {
            return null;
        }

        string nondigits = '[^0-9]';
        string PhoneDigits;

        // remove all non numeric
        PhoneDigits = Phone.replaceAll(nondigits,'');

        // 10 digit: reformat with dashes
        if (PhoneDigits.length() == 10)
            return PhoneDigits.substring(0,3) + '-' +
                    PhoneDigits.substring(3,6) + '-' +
                    PhoneDigits.substring(6,10);
        // 11 digit: if starts with 1, format as 10 digit
        if (PhoneDigits.length() == 11) {
            if (PhoneDigits.substring(0,1) == '1') {
                return  PhoneDigits.substring(1,4) + '-' +
                        PhoneDigits.substring(4,7) + '-' +
                        PhoneDigits.substring(7,11);
            }
        }

        // if it isn't a 10 or 11 digit number, return the original because
        // it may contain an extension or special information
        return( Phone );
    } //formatPhone

    @TestVisible
    private class RecordData {
        @AuraEnabled public String id = null;
        @AuraEnabled public String name = null;
        @AuraEnabled public List<DisplayField> fieldValues = new List<DisplayField>();
        @AuraEnabled public List<RecordData> childRecords = new List<RecordData>();
    } //class RecordData

    @TestVisible
    private class SelectField {
        @AuraEnabled public String label = null;
        @AuraEnabled public String fieldName = null;
        public SelectField(Schema.FieldSetMember fsm) {
            label = fsm.getLabel();
            fieldName = fsm.getFieldPath();
        }
        public SelectField(String lbl, String fn) {
            label = lbl;
            fieldName = fn;
        }
        public SelectField(String v) {
            label = v;
        }
    } //class SelectField

    @TestVisible
    private class DisplayField {
        @AuraEnabled public SelectField fld = null;
        @AuraEnabled public String value = null;
        public DisplayField(SelectField fld, String value) {
            this.fld = fld;
            this.value = value;
        }
    } //class DisplayField
} //class