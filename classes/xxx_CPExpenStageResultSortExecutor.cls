/**
 *  script used to sort failed expense migration attempts from x_CPExpenseStaging__c for easier processing.
 *
 *  @usage String query = 'select id, name, MigrationStatus__c, MigratedTime__c, ProcessingAnomalyComments__c from x_CPExpenseStaging__c where MigrationStatus__c = \'Failed\'';
 *         new mmlib_GenericBatch( xxx_CPExpenStageResultSortExecutor.class, query).setBatchSizeTo(1000).execute();
 */
public class xxx_CPExpenStageResultSortExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        x_CPExpenseStaging__c workingRecord = null;

        list<x_CPExpenseStaging__c> recordsToUpdate = new list<x_CPExpenseStaging__c>();

        for ( SObject record : scope )
        {
            workingRecord = (x_CPExpenseStaging__c)record;

            if ( workingRecord.ProcessingAnomalyComments__c.contains('Related Matter record was not found') )
            {
                workingRecord.MigrationStatus__c = 'Failed - NORELATEDMATTER';
                recordsToUpdate.add( workingRecord );
                continue;
            }
            if ( workingRecord.ProcessingAnomalyComments__c.contains('Unable to find active employee with this email address found in create_user_email__c.') )
            {
                workingRecord.MigrationStatus__c = 'Failed - NOACTIVEEMPLOYEE';
                recordsToUpdate.add( workingRecord );
                continue;
            }
        }

        update recordsToUpdate;
    }
}