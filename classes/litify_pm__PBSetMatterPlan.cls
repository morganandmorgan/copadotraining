/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBSetMatterPlan {
    global PBSetMatterPlan() {

    }
    @InvocableMethod(label='Set Matter Plan' description='Sets the matters' plan, creates stages, and creates initial tasks.')
    global static void setMatterPlan(List<litify_pm.PBSetMatterPlan.PBSetMatterPlanWrapper> input) {

    }
global class PBSetMatterPlanWrapper {
    @InvocableVariable( required=false)
    global litify_pm__Matter__c matter;
    @InvocableVariable( required=false)
    global Id matterPlanId;
    global PBSetMatterPlanWrapper() {

    }
}
}
