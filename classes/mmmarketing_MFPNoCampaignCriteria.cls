/**
 *  mmmarketing_MFPNoCampaignCriteria
 *
 *  This class examines all Marketing_Financial_Period__c records to find those that do not have the
 */
public with sharing class mmmarketing_MFPNoCampaignCriteria
    implements mmlib_ICriteria
{
    private list<Marketing_Financial_Period__c> records = new list<Marketing_Financial_Period__c>();

    public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
    {
        if (records != null
            && Marketing_Financial_Period__c.SObjectType == records.getSobjectType()
            )
        {
            this.records.addAll( (list<Marketing_Financial_Period__c>) records);
        }

        return this;
    }

    public list<SObject> run()
    {
        list<Marketing_Financial_Period__c> qualifiedRecords = new list<Marketing_Financial_Period__c>();

        Set<String> parentMarketingCampaignTechnicalKeysSet = new Set<String>();

        // loop through the records
        for (Marketing_Financial_Period__c record : this.records)
        {
            // find those that do not yet have Marketing_Campaign__c populated
            //      but do have Campaign_Value__c or Campaign_AdWords_ID_Value__c populated
            parentMarketingCampaignTechnicalKeysSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) );
        }

        if ( ! parentMarketingCampaignTechnicalKeysSet.isEmpty() )
        {
            // With that set of campaign values, query the campaign table
            List<Marketing_Campaign__c> currentlyAvailableCampaignList = mmmarketing_CampaignsSelector.newInstance().selectByTechnicalKeys( parentMarketingCampaignTechnicalKeysSet );

            // Sort campaign records in to a map
            Set<String> currentlyAvailableCampaignsTechnicalKeysSet = new Set<String>();

            for (Marketing_Campaign__c currentlyAvailableCampaign : currentlyAvailableCampaignList)
            {
                currentlyAvailableCampaignsTechnicalKeysSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( currentlyAvailableCampaign ) );
            }

            // loop through the records again.
            for (Marketing_Financial_Period__c record : this.records)
            {
                // If the record does not have a matching campaign, then that is a qualifiedRecords
                if ( ! currentlyAvailableCampaignsTechnicalKeysSet.removeAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) ) )
                {
                    qualifiedRecords.add( record );
                }

                //if ( ( ( string.isNotBlank( record.Campaign_Value__c )
                //        && ! currentlyAvailableCampaignsTechnicalKeysSet.contains( record.Campaign_Value__c.toLowerCase() ) )
                //     || string.isBlank( record.Campaign_Value__c )
                //     )
                //    && ! currentlyAvailableCampaignsTechnicalKeysSet.contains(record.Campaign_AdWords_ID_Value__c)
                //   )
                //{
                //    qualifiedRecords.add( record );
                //}
            }
        }

        return qualifiedRecords;
    }
}