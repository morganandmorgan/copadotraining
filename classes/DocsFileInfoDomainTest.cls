@IsTest
public class DocsFileInfoDomainTest {

    @testSetup
    public static void setup() {
        litify_docs__Template__c template = new litify_docs__Template__c();
        template.name = 'testTemplateName';
        template.litify_docs__Merged_File_Name__c = template.name;
        template.litify_docs__Starting_Object__c = 'Account';

        insert template;
    } //setup
        
    @isTest
    public static void testOnBeforeInsert() {
        
        String toName = 'To Name';
        String fromName = 'From Name';
        String ext = 'cls';

        litify_docs__Template__c template = [SELECT id, name FROM litify_docs__Template__c LIMIT 1];
        
        litify_docs__File_Info__c fileInfo = new litify_docs__File_Info__c();
        fileInfo.name = toName + '||' + fromName + '.' + ext;
        fileInfo.litify_docs__Origin_Merge_Template__c = template.id;
        
        DocsFileInfoDomain domain = new DocsFileInfoDomain(new List<litify_docs__File_Info__c>{fileInfo});
        
        domain.onBeforeInsert();
        
        System.assertEquals(toName, fileInfo.litify_docs__To__c);
        System.assertEquals(fromName, fileInfo.litify_docs__From__c);
        System.assertEquals(template.name + '.' + ext, fileInfo.name);
                
        insert fileInfo; //this will cover the trigger
        
    } //testOnBeforeInsert
    
} //class