public class BatchProcessHelper {
    
    public BatchProcessHelper() {
    }

    public Integer getHoldingBatchJobCount(){
        //Flex queue count
        //Up to 100 jobs can be on hold, FIFO.
        Integer count=0;
        count=[
            select count() 
            from AsyncApexJob 
            where 
            JobType='BatchApex' 
            and Status in ( 
                'Holding'
            )            
        ];
        if(count==null){
            count=0;
        }
        return count;
    }

    public Integer getExecutingBatchJobCount(){
        //Actively running batch jobs.  There's a maximum of 5 concurrent executing jobs. 
        //TODO:  Consider adding queued to the list of statuses.
        Integer count=0;
        count=[
            select 
            count() 
            from AsyncApexJob 
            where 
            JobType='BatchApex' 
            and Status in ( 
                'Processing'
                ,'Preparing'
            )           
        ];
        if(count==null){
            count=0;
        }
        return count;
    }    

}