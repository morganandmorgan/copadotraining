@isTest
private class IntakeCopyToInvestigationEventTest {

    private static final Integer BULK_SIZE = 9;
    private static final String INVESTIGATION_STATUS_SCHEDULED = 'Scheduled';
    private static final String INVESTIGATION_STATUS_CANCELED = 'Canceled';

	@TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Person');
        Database.insert(client);

        client = [select Id, PersonContactId from Account where Id = :client.Id];

        List<Contact> attorneyContacts = new List<Contact>();
        for (Integer i = 0; i < 5; ++i) {
            attorneyContacts.add(TestUtil.createAssignableAttorney());
        }
        toInsert.addAll((List<SObject>) attorneyContacts);

        Id attorneyProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id;
        List<User> users = new List<User>();
        for (Integer i = 0; i < 6; ++i) {
            users.add(TestUtil.createUser(attorneyProfileId));
        }
        toInsert.addAll((List<SObject>) users);

        Database.insert(toInsert);
        toInsert.clear();

        // Contact clientContact = TestUtil.createContact(client.Id);
        // clientContact.AccountId = client.PersonCId;
        // Database.insert(clientContact);

        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < BULK_SIZE; ++i) {
            Intake__c intake = new Intake__c(
                    Client__c = client.Id,
                    Approving_Attorney_1__c = users.get(Math.mod(i, 6)).Id,
                    Approving_Attorney_2__c = users.get(Math.mod(i + 1, 6)).Id,
                    Approving_Attorney_3__c = users.get(Math.mod(i + 2, 6)).Id,
                    Approving_Attorney_4__c = users.get(Math.mod(i + 3, 6)).Id,
                    Approving_Attorney_5__c = users.get(Math.mod(i + 4, 6)).Id,
                    Approving_Attorney_6__c = users.get(Math.mod(i + 5, 6)).Id,
                    Assigned_Attorney__c = attorneyContacts.get(Math.mod(i, attorneyContacts.size())).Id,
                    New_Case_Email__c = i + '@test.com'
                );
            intakes.add(intake);
        }
        toInsert.addAll((List<SObject>) intakes);

        Database.insert(toInsert);
        toInsert.clear();

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = new List<IntakeInvestigationEvent__c>();
        for (Integer i = 0; i < BULK_SIZE; ++i) {
            Intake__c intake = intakes.get(Math.mod(i, intakes.size()));
            intakeInvestigationEvents.add(
                new IntakeInvestigationEvent__c(
                    Intake__c = intake.Id,
                    EndDateTime__c = Datetime.now().addDays(2),
                    StartDateTime__c = Datetime.now().addDays(1),
                    Investigation_Status__c = INVESTIGATION_STATUS_SCHEDULED,
                    Contact__c = client.PersonContactId
            ));
        }
        toInsert.addAll((List<SObject>) intakeInvestigationEvents);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Intake__c> getIntakes() {
        return [
            SELECT
                Id,
                Approving_Attorney_1__c,
                Approving_Attorney_2__c,
                Approving_Attorney_3__c,
                Approving_Attorney_4__c,
                Approving_Attorney_5__c,
                Approving_Attorney_6__c,
                Assigned_Attorney__c,
                New_Case_Email__c,
                (SELECT Id,
                    Approving_Attorney_1__c,
                    Approving_Attorney_2__c,
                    Approving_Attorney_3__c,
                    Approving_Attorney_4__c,
                    Approving_Attorney_5__c,
                    Approving_Attorney_6__c,
                    Assigned_Attorney__c,
                    Investigation_Status__c,
                    New_Case_Email__c,
                    SystemModStamp
                FROM IntakeInvestigationEvents__r)
            FROM
                Intake__c
        ];
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents(List<IntakeInvestigationEvent__c> intakeInvestigationEvents) {
        return [
            SELECT
                Id,
                Approving_Attorney_1__c,
                Approving_Attorney_2__c,
                Approving_Attorney_3__c,
                Approving_Attorney_4__c,
                Approving_Attorney_5__c,
                Approving_Attorney_6__c,
                Assigned_Attorney__c,
                EndDateTime__c,
                Intake__c,
                Intake__r.Approving_Attorney_1__c,
                Intake__r.Approving_Attorney_2__c,
                Intake__r.Approving_Attorney_3__c,
                Intake__r.Approving_Attorney_4__c,
                Intake__r.Approving_Attorney_5__c,
                Intake__r.Approving_Attorney_6__c,
                Intake__r.Assigned_Attorney__c,
                Intake__r.New_Case_Email__c,
                Investigation_Status__c,
                New_Case_Email__c,
                StartDateTime__c,
                SystemModStamp
            FROM
                IntakeInvestigationEvent__c
            WHERE
                Id IN :intakeInvestigationEvents
        ];
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents(List<String> investigationStatuses) {
        return [
            SELECT
                Id,
                Approving_Attorney_1__c,
                Approving_Attorney_2__c,
                Approving_Attorney_3__c,
                Approving_Attorney_4__c,
                Approving_Attorney_5__c,
                Approving_Attorney_6__c,
                Assigned_Attorney__c,
                EndDateTime__c,
                Intake__c,
                Intake__r.Approving_Attorney_1__c,
                Intake__r.Approving_Attorney_2__c,
                Intake__r.Approving_Attorney_3__c,
                Intake__r.Approving_Attorney_4__c,
                Intake__r.Approving_Attorney_5__c,
                Intake__r.Approving_Attorney_6__c,
                Intake__r.Assigned_Attorney__c,
                Intake__r.New_Case_Email__c,
                Investigation_Status__c,
                New_Case_Email__c,
                StartDateTime__c,
                SystemModStamp
            FROM
                IntakeInvestigationEvent__c
            WHERE
                Investigation_Status__c IN :investigationStatuses
        ];
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents() {
        return getIntakeInvestigationEvents(new List<String>{ INVESTIGATION_STATUS_SCHEDULED, INVESTIGATION_STATUS_CANCELED });
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents(String investigationStatus) {
        return getIntakeInvestigationEvents(new List<String>{ investigationStatus });
    }

    @isTest
    private static void afterUpdate_NoChangesToCopy() {
        List<Intake__c> intakes = getIntakes();
        Map<Id, IntakeInvestigationEvent__c> intakeInvestigationEvents = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents());

        Test.startTest();
        Database.update(intakes);
        Test.stopTest();

        Map<Id, IntakeInvestigationEvent__c> resultMap = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents());
        System.assertEquals(intakeInvestigationEvents.keySet(), resultMap.keySet());
        for (Id recordId : intakeInvestigationEvents.keySet()) {
            IntakeInvestigationEvent__c oldRecord = intakeInvestigationEvents.get(recordId);
            IntakeInvestigationEvent__c newRecord = resultMap.get(recordId);

            System.assertEquals(oldRecord.SystemModStamp, newRecord.SystemModStamp);
        }
    }

    @isTest
    private static void afterUpdate_ChangesSuccessfullyCopied() {
        List<Intake__c> intakes = getIntakes();
        Map<Id, IntakeInvestigationEvent__c> intakeInvestigationEvents = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents());

        Contact newAttorneyContact = TestUtil.createAssignableAttorney();
        Database.insert(newAttorneyContact);

        for (Integer i = 0; i < intakes.size(); ++i) {
            Intake__c intake = intakes.get(i);

            intake.Assigned_Attorney__c = newAttorneyContact.Id;
            intake.New_Case_Email__c = 'newcaseemail.updatedvalue.' + i + '@test.com';

            Id approvingAttorney1 = intake.Approving_Attorney_1__c;
            intake.Approving_Attorney_1__c = intake.Approving_Attorney_2__c;
            intake.Approving_Attorney_2__c = intake.Approving_Attorney_3__c;
            intake.Approving_Attorney_3__c = intake.Approving_Attorney_4__c;
            intake.Approving_Attorney_4__c = intake.Approving_Attorney_5__c;
            intake.Approving_Attorney_5__c = intake.Approving_Attorney_6__c;
            intake.Approving_Attorney_6__c = approvingAttorney1;
        }

        Test.startTest();
        Database.update(intakes);
        Test.stopTest();

        Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>(intakes);

        Map<Id, IntakeInvestigationEvent__c> resultMap = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents());
        System.assertEquals(intakeInvestigationEvents.keySet(), resultMap.keySet());
        for (Id recordId : intakeInvestigationEvents.keySet()) {
            IntakeInvestigationEvent__c oldRecord = intakeInvestigationEvents.get(recordId);
            IntakeInvestigationEvent__c newRecord = resultMap.get(recordId);

            System.assert(oldRecord.SystemModStamp < newRecord.SystemModStamp);

            System.assertEquals(oldRecord.Intake__c, newRecord.Intake__c);

            Intake__c intake = intakeMap.get(newRecord.Intake__c);
            System.assertEquals(newRecord.Approving_Attorney_1__c, intake.Approving_Attorney_1__c);
            System.assertEquals(newRecord.Approving_Attorney_2__c, intake.Approving_Attorney_2__c);
            System.assertEquals(newRecord.Approving_Attorney_3__c, intake.Approving_Attorney_3__c);
            System.assertEquals(newRecord.Approving_Attorney_4__c, intake.Approving_Attorney_4__c);
            System.assertEquals(newRecord.Approving_Attorney_5__c, intake.Approving_Attorney_5__c);
            System.assertEquals(newRecord.Approving_Attorney_6__c, intake.Approving_Attorney_6__c);
            System.assertEquals(newRecord.Assigned_Attorney__c, intake.Assigned_Attorney__c);
            System.assertEquals(newRecord.New_Case_Email__c, intake.New_Case_Email__c);
        }
    }

    @isTest
    private static void afterUpdate_ChangesCopiedToScheduledEventsOnly() {
        List<Intake__c> intakes = getIntakes();
        Map<Id, IntakeInvestigationEvent__c> intakeInvestigationEvents = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents());

        List<IntakeInvestigationEvent__c> canceledIntakeInvestigationEvents = intakeInvestigationEvents.values().deepClone(false, false, false);
        for (IntakeInvestigationEvent__c record : canceledIntakeInvestigationEvents) {
            record.Investigation_Status__c = INVESTIGATION_STATUS_CANCELED;
        }
        Database.insert(canceledIntakeInvestigationEvents);
        Map<Id, IntakeInvestigationEvent__c> canceledIntakeInvestigationEventsMap = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents(canceledIntakeInvestigationEvents));

        Contact newAttorneyContact = TestUtil.createAssignableAttorney();
        Database.insert(newAttorneyContact);

        for (Integer i = 0; i < intakes.size(); ++i) {
            Intake__c intake = intakes.get(i);

            intake.Assigned_Attorney__c = newAttorneyContact.Id;
            intake.New_Case_Email__c = 'newcaseemail.updatedvalue.' + i + '@test.com';

            Id approvingAttorney1 = intake.Approving_Attorney_1__c;
            intake.Approving_Attorney_1__c = intake.Approving_Attorney_2__c;
            intake.Approving_Attorney_2__c = intake.Approving_Attorney_3__c;
            intake.Approving_Attorney_3__c = intake.Approving_Attorney_4__c;
            intake.Approving_Attorney_4__c = intake.Approving_Attorney_5__c;
            intake.Approving_Attorney_5__c = intake.Approving_Attorney_6__c;
            intake.Approving_Attorney_6__c = approvingAttorney1;
        }

        Test.startTest();
        Database.update(intakes);
        Test.stopTest();

        Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>(intakes);

        Map<Id, IntakeInvestigationEvent__c> scheduledResultMap = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents(INVESTIGATION_STATUS_SCHEDULED));
        Map<Id, IntakeInvestigationEvent__c> canceledResultMap = new Map<Id, IntakeInvestigationEvent__c>(getIntakeInvestigationEvents(INVESTIGATION_STATUS_CANCELED));
        System.assertEquals(intakeInvestigationEvents.keySet(), scheduledResultMap.keySet());
        System.assertEquals(canceledIntakeInvestigationEventsMap.keySet(), canceledResultMap.keySet());

        for (Id recordId : intakeInvestigationEvents.keySet()) {
            IntakeInvestigationEvent__c oldRecord = intakeInvestigationEvents.get(recordId);
            IntakeInvestigationEvent__c newRecord = scheduledResultMap.get(recordId);

            System.assert(oldRecord.SystemModStamp < newRecord.SystemModStamp);

            System.assertEquals(oldRecord.Intake__c, newRecord.Intake__c);

            Intake__c intake = intakeMap.get(newRecord.Intake__c);
            System.assertEquals(newRecord.Approving_Attorney_1__c, intake.Approving_Attorney_1__c);
            System.assertEquals(newRecord.Approving_Attorney_2__c, intake.Approving_Attorney_2__c);
            System.assertEquals(newRecord.Approving_Attorney_3__c, intake.Approving_Attorney_3__c);
            System.assertEquals(newRecord.Approving_Attorney_4__c, intake.Approving_Attorney_4__c);
            System.assertEquals(newRecord.Approving_Attorney_5__c, intake.Approving_Attorney_5__c);
            System.assertEquals(newRecord.Approving_Attorney_6__c, intake.Approving_Attorney_6__c);
            System.assertEquals(newRecord.Assigned_Attorney__c, intake.Assigned_Attorney__c);
            System.assertEquals(newRecord.New_Case_Email__c, intake.New_Case_Email__c);
        }

        for (Id recordId : canceledIntakeInvestigationEventsMap.keySet()) {
            IntakeInvestigationEvent__c canceledIntakeInvestigationEvent = canceledIntakeInvestigationEventsMap.get(recordId);
            IntakeInvestigationEvent__c canceledResult = canceledResultMap.get(recordId);

            System.assertEquals(canceledIntakeInvestigationEvent.SystemModStamp, canceledResult.SystemModStamp);

            System.assertEquals(canceledIntakeInvestigationEvent.Intake__c, canceledResult.Intake__c);

            Intake__c intake = intakeMap.get(canceledResult.Intake__c);
            System.assertNotEquals(canceledResult.Approving_Attorney_1__c, intake.Approving_Attorney_1__c);
            System.assertNotEquals(canceledResult.Approving_Attorney_2__c, intake.Approving_Attorney_2__c);
            System.assertNotEquals(canceledResult.Approving_Attorney_3__c, intake.Approving_Attorney_3__c);
            System.assertNotEquals(canceledResult.Approving_Attorney_4__c, intake.Approving_Attorney_4__c);
            System.assertNotEquals(canceledResult.Approving_Attorney_5__c, intake.Approving_Attorney_5__c);
            System.assertNotEquals(canceledResult.Approving_Attorney_6__c, intake.Approving_Attorney_6__c);
            System.assertNotEquals(canceledResult.Assigned_Attorney__c, intake.Assigned_Attorney__c);
            System.assertNotEquals(canceledResult.New_Case_Email__c, intake.New_Case_Email__c);
        }
    }
}