public class mmmatter_MatterTeamRolesSelector extends mmlib_SObjectSelector implements mmmatter_IMatterTeamRolesSelector {

    public static mmmatter_IMatterTeamRolesSelector newInstance() {
        return (mmmatter_IMatterTeamRolesSelector) mm_Application.Selector.newInstance(litify_pm__Matter_Team_Role__c.SObjectType);
    }

    private Schema.SObjectType getSObjectType() {
        return litify_pm__Matter_Team_Role__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList() {
        return new List<Schema.SObjectField>{
                litify_pm__Matter_Team_Role__c.Name
        };
    }

    public List<litify_pm__Matter_Team_Role__c> selectAll() {
        return
                (List<litify_pm__Matter_Team_Role__c>)
                        Database.query(
                                newQueryFactory()
                                        .toSOQL()
                        );
    }

    public List<litify_pm__Matter_Team_Role__c> selectById(Set<Id> ids) {
        return
                (List<litify_pm__Matter_Team_Role__c>)
                        Database.query(
                                newQueryFactory()
                                        .setCondition(litify_pm__Matter_Team_Role__c.Id + ' in :ids')
                                        .toSOQL()
                        );
    }

    public List<litify_pm__Matter_Team_Role__c> selectByName(Set<String> nameSet) {
        return
                (List<litify_pm__Matter_Team_Role__c>)
                        Database.query(
                                newQueryFactory()
                                        .setCondition(litify_pm__Matter_Team_Role__c.Name + ' in :nameSet')
                                        .toSOQL()
                        );
    }
}