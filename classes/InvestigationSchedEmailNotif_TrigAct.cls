public class InvestigationSchedEmailNotif_TrigAct extends TriggerAction {

	private List<Messaging.SingleEmailMessage> emailsToSend;

	public InvestigationSchedEmailNotif_TrigAct() {
		super();
	}

	public override Boolean shouldRun() {
		Map<Id,Event> eventMap = (Map<Id,Event>)triggerMap;
		emailsToSend = new List<Messaging.SingleEmailMessage>();
		for(Event e : eventMap.values()) {
			if(e.Subject.startsWithIgnoreCase('Investigation')) {
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				system.debug('event: '+e);

				List<String> sendTo = new List<String>();
				User owner = [select id, email from User where id = :e.OwnerId];
				sendTo.add(owner.Email);
				mail.setToAddresses(sendTo);

				mail.setReplyTo('jcary@forthepeople.com');
				mail.setSenderDisplayName('James Cary');

				// mail.setSubject('URGENT TEST');
				// Contact client = [select id, firstname, lastname from contact where id = :e.whoId];
				// String body = 'Dear ' + client.firstname + ', ';
				// body += 'THIS IS A TEST';
				// mail.setHtmlBody(body);
				mail.setTemplateId('00Xq0000000EDPS');
				mail.setTargetObjectId(e.WhoId);
				mail.saveAsActivity = false;
				mail.setWhatId(e.whatId);

				emailsToSend.add(mail);
			}
		}

		return !emailsToSend.isEmpty();
	}

	public override void doAction() {
		try {
			Messaging.sendEmail(emailsToSend);
		} catch (Exception e) {

		}

	}
}

/*
// 	Task Fields
	// Lookups //
	whatId  = Intake
	whoId   = Person Account
	OwnerId = Investigator

	// Other Info //
	Email, Phone
	StartDateTime,EndDateTime
	Description
*/