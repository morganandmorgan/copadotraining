public with sharing class PaymentProcessingDataAccess {

    public Set<String> inProcessPaymentStatuses = null;
    public Set<String> readyToPostPaymentStatuses = null;
    public Set<String> postedPaymentStatuses = null; 

    public PaymentProcessingDataAccess(){

        inProcessPaymentStatuses = new Set<String>{'Proposed', 'Media Prepared','Ready To Post'};
        readyToPostPaymentStatuses = new Set<String>{'Ready To Post','Error','Media Prepared'};
        postedPaymentStatuses = new Set<String>{'Matched','Background Posting','Posted', 'Part Canceled', 'Canceled'};
    }

    public List<c2g__codaTransactionLineItem__c> getTransactionLineItems(   Id companyId,
                                                                            String matterReferenceFilter, 
                                                                            String requestedByFilter, 
                                                                            String payableToFilter,
                                                                            String vendorInvoiceNumberFilter,
                                                                            String pinNumberFilter,
                                                                            String bankTypeToPayFilter,
                                                                            Date invoiceDateFilter,
                                                                            Date dueDateFilter){
        
        List<c2g__codaTransactionLineItem__c> results=null;
                
        String transactionLineQuery = 'SELECT '+
            'Id'+
            ',Name '+
            ',c2g__DueDate__c '+
            ',c2g__LineDescription__c'+
            ',c2g__AccountCurrency__c'+
            ',c2g__BankAccount__c '+
       		',c2g__BankAccountCurrency__c    '+
            ',c2g__Account__c'+
            ',c2g__Account__r.Name '+ 
            ',c2g__Account__r.Split_Invoices_on_Separate_Payments__c ' +
            ',c2g__Transaction__r.c2g__TransactionDate__c'+
            ',c2g__Transaction__r.c2g__Period__c'+
            ',c2g__Transaction__r.c2g__DocumentNumber__c'+
            ',c2g__Transaction__r.c2g__Journal__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Requested_By__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Requested_By__r.Name'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Check_Memo__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Name'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.Name'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.ReferenceNumber__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__r.Name'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.Payment_Bank_Account_Type__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.c2g__InvoiceCurrency__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.c2g__Period__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.c2g__DueDate__c'+
            ',c2g__Transaction__r.c2g__PayableInvoice__r.c2g__InvoiceDescription__c'+
            ',c2g__OwnerCompany__c'+
            ',c2g__OwnerCompany__r.Name'+
            ',c2g__DocumentOutstandingValue__c'+
            ',c2g__LineType__c'+
            ' FROM c2g__codaTransactionLineItem__c'+
            ' WHERE c2g__LineType__c = \'Account\''+
            ' AND c2g__OwnerCompany__c = :companyId'+
            ' AND c2g__Transaction__r.c2g__PayableInvoice__c != null'+
            ' AND c2g__Transaction__r.c2g__TransactionType__c = \'Purchase Invoice\''+ 
            ' and c2g__Account__r.c2g__CODAIntercompanyAccount__c != true ' + 
            ' AND c2g__DocumentOutstandingValue__c != 0'+
            ' AND Id not in (SELECT c2g__TransactionLineItem__c FROM c2g__codaPaymentLineItem__c WHERE c2g__PaymentSummary__r.c2g__Status__c != \'Canceled\')';

        //Add in the dynamic filters
        transactionLineQuery += requestedByFilter != null && requestedByFilter != '' && requestedByFilter != '000000000000000'? ' AND Requested_By__c = : requestedByFilter ' : '';
        transactionLineQuery += payableToFilter != null && payableToFilter != '' && payableToFilter != '000000000000000' ? ' AND c2g__Account__c = :payableToFilter ' : '';
        transactionLineQuery += matterReferenceFilter != null && matterReferenceFilter != '' ? ' AND c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.ReferenceNumber__c = : matterReferenceFilter ' : '';
        transactionLineQuery += vendorInvoiceNumberFilter != null && vendorInvoiceNumberFilter != '' ? ' AND c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c = : vendorInvoiceNumberFilter ' : '';
        transactionLineQuery += pinNumberFilter != null && pinNumberFilter != '' ? ' AND c2g__Transaction__r.c2g__DocumentNumber__c = : pinNumberFilter ' : '';
        transactionLineQuery += bankTypeToPayFilter != null && bankTypeToPayFilter != '' ? ' AND c2g__Transaction__r.c2g__PayableInvoice__r.Payment_Bank_Account_Type__c = : bankTypeToPayFilter ' : '';
        transactionLineQuery += invoiceDateFilter != null ? ' AND c2g__Transaction__r.c2g__TransactionDate__c = : invoiceDateFilter ' : '';
        transactionLineQuery += dueDateFilter != null ? ' AND c2g__DueDate__c = : dueDateFilter ' : '';
                    
        //Add the order of the list
        transactionLineQuery += ' ORDER BY c2g__Transaction__r.c2g__Account__c, c2g__Transaction__r.c2g__PayableInvoice__c';
                    
        //Call the Query:
        results = Database.query(transactionLineQuery);
        
        return results;   
    }

    // public List<c2g__codaTransactionLineItem__c> getTransactionLineItems(Id paymentCollectionId){
    //     List<c2g__codaTransactionLineItem__c> results = null;
    //     //TODO:  This.  If needed.
    //     return results;
    // }    
    
    public List<c2g__codaCheckRange__c> getCheckRanges(c2g__codaBankAccount__c bankAccount){
        
        List<c2g__codaCheckRange__c> results=null;
        
        results=getCheckRanges(bankAccount.Id);
        
        return results;  
    }     
    
    public List<c2g__codaCheckRange__c> getCheckRanges(Id bankAccountId){
        
        List<c2g__codaCheckRange__c> results=null;
        
        results=[
            
            SELECT 
            Id
            ,Name 
            ,c2g__NextCheckNumber__c 
            ,c2g__Activated__c
            ,c2g__BankAccount__c
            ,c2g__BankAccount__r.Name

            FROM c2g__codaCheckRange__c 
            WHERE 
            c2g__Activated__c = true 
            and c2g__BankAccount__c = :bankAccountId
                        
        ];
        //TODO:  Put in the real criteria.
        //and c2g__OwnerCompany__c = :companyID
        
        return results;       
    }

    public Integer getNextCheckNumber(c2g__codaCheckRange__c checkRange){
        //Converts the text check number stored on check range object into a real integer.
        Integer nextCheckNumber = null;

        if(checkRange!=null){
          
            String nextCheckNumberText=null;
            nextCheckNumberText=checkRange.c2g__NextCheckNumber__c;                
            
            if(nextCheckNumberText!=null){
                nextCheckNumber = integer.valueof(nextCheckNumberText);
            }

        }
		
        return nextCheckNumber;
        
    }    
    
    public Integer getNextCheckNumber(Id bankAccountId){
        
        Integer nextCheckNumber = null;       
        
        List<c2g__codaCheckRange__c> checkRanges = null;
        
        checkRanges=getCheckRanges(bankAccountId);

        if((checkRanges!=null)&&(checkRanges.size()>0)){
        
            c2g__codaCheckRange__c checkRange = null;
            checkRange = checkRanges[0];

            if(checkRange!=null)  {
            
                String nextCheckNumberText=null;
                nextCheckNumberText=checkRange.c2g__NextCheckNumber__c;                
                
                nextCheckNumber = integer.valueof(nextCheckNumberText);

            }

        }
		
        return nextCheckNumber;
        
    }    
    
    public List<c2g__codaPaymentAccountLineItem__c> getPaymentSummaryAccounts(String paymentCollectionId){
        //Gets payment-summary-account related to payment so a check can be created for each account.
        //Account is vendor pay-to.
        List<c2g__codaPaymentAccountLineItem__c> results=null;
        
        results=[
            SELECT Id, 
            Name, 
            c2g__Payment__c,
            c2g__Payment__r.Name,
            c2g__Payment__r.Id,  
            c2g__Account__c, 
            c2g__Account__r.Name,
            c2g__LineNumber__c,
            c2g__PaymentValueTotal__c
            FROM c2g__codaPaymentAccountLineItem__c 
            WHERE c2g__Payment__r.Payment_Collection__c = :paymentCollectionId 
            AND c2g__Payment__r.c2g__Status__c in : inProcessPaymentStatuses
            ORDER BY
            c2g__Payment__c, c2g__LineNumber__c];

        return results;
        
    }

    public List<c2g__codaPaymentAccountLineItem__c> getPaymentSummaryAccounts_Void(String paymentCollectionId){
        //Gets payment-summary-account related to payment so a check can be created for each account.
        //Account is vendor pay-to.
        List<c2g__codaPaymentAccountLineItem__c> results=null;
        
        results=[
            SELECT Id, 
            Name, 
            c2g__Payment__c,
            c2g__Payment__r.Name,
            c2g__Payment__r.Id,  
            Full_Payment_Void__c,
            c2g__Account__c, 
            c2g__Account__r.Name,
            c2g__LineNumber__c,
            c2g__PaymentValueTotal__c
            FROM c2g__codaPaymentAccountLineItem__c 
            WHERE c2g__Payment__r.Payment_Collection__c = :paymentCollectionId 
            AND c2g__Status__c != 'Canceled'
            AND c2g__Payment__r.c2g__Status__c in ('Matched', 'Part Canceled', 'Canceling Error')
            ORDER BY c2g__Payment__c, c2g__LineNumber__c];

        return results;
        
    }
    
    public List<c2g__codaPaymentAccountLineItem__c> getPaymentSummaryAccounts(List<Id> paymentIds){
        //Gets payment-summary-account related to payment so a check can be created for each account.
        //Account is vendor pay-to.
        List<c2g__codaPaymentAccountLineItem__c> results=null;
        
        results=[
            SELECT Id, 
            Name, 
            c2g__Payment__c,
            c2g__Payment__r.Name,
            c2g__Payment__r.c2g__Status__c,
            c2g__Payment__r.Id, 
            c2g__Account__c, 
            c2g__Account__r.Name,
            c2g__LineNumber__c,
            c2g__PaymentValueTotal__c
            FROM c2g__codaPaymentAccountLineItem__c 
            WHERE c2g__Payment__c in :paymentIds 
            AND c2g__Payment__r.c2g__Status__c in : inProcessPaymentStatuses
            ORDER BY
            c2g__Payment__c, c2g__LineNumber__c];

        return results;
        
    } 

    public List<c2g__codaPaymentAccountLineItem__c> getPaymentSummaryAccounts(Set<Id> paymentIds){
        //Gets payment-summary-account related to payment so a check can be created for each account.
        //Account is vendor pay-to.
        List<c2g__codaPaymentAccountLineItem__c> results=null;
        
        results=[
            SELECT Id, 
            Name, 
            c2g__Payment__c,
            c2g__Payment__r.Name,
            c2g__Payment__r.Id, 
            c2g__Payment__r.c2g__Status__c, 
            c2g__Account__c, 
            c2g__Account__r.Name,
            c2g__LineNumber__c,
            c2g__PaymentValueTotal__c
            FROM c2g__codaPaymentAccountLineItem__c 
            WHERE c2g__Payment__c in :paymentIds 
            AND c2g__Payment__r.c2g__Status__c in : inProcessPaymentStatuses
            ORDER BY
            c2g__Payment__c, c2g__LineNumber__c];

        return results;
        
    }   

    public List<c2g__codaPaymentMediaSummary__c> getPaymentMediaSummaries(List<Id> paymentIds){
        List<c2g__codaPaymentMediaSummary__c> results=null;
        results=[
            SELECT Id, 
            Name, 
            c2g__PaymentMediaControl__c,
            c2g__PaymentMediaControl__r.c2g__Payment__c,
            c2g__PaymentMediaControl__r.c2g__Payment__r.c2g__Status__c, 
            c2g__Account__c, 
            c2g__Account__r.Name,
            c2g__PaymentValueTotal__c,
            Check_Memo__c,
            c2g__PaymentReference__c
            FROM c2g__codaPaymentMediaSummary__c 
            WHERE 
            c2g__PaymentMediaControl__r.c2g__Payment__c in :paymentIds 
            AND c2g__PaymentMediaControl__r.c2g__Payment__r.c2g__Status__c in : inProcessPaymentStatuses
            ORDER BY c2g__PaymentMediaControl__r.c2g__Payment__c, c2g__LineNumber__c];
        return results;
    }

    public List<c2g__codaPaymentMediaSummary__c> getPaymentMediaSummaries(Id paymentCollectionId){
        List<c2g__codaPaymentMediaSummary__c> results=null;
        results=[
            SELECT Id, 
            Name, 
            c2g__PaymentMediaControl__c,
            c2g__PaymentMediaControl__r.c2g__Payment__c,
            c2g__PaymentMediaControl__r.c2g__Payment__r.c2g__Status__c, 
            c2g__Account__c, 
            c2g__Account__r.Name,
            c2g__PaymentValueTotal__c,
            Check_Memo__c,
            c2g__PaymentReference__c
            FROM c2g__codaPaymentMediaSummary__c 
            WHERE 
            c2g__PaymentMediaControl__r.c2g__Payment__r.Payment_Collection__c = :paymentCollectionId 
            AND c2g__PaymentMediaControl__r.c2g__Payment__r.c2g__Status__c in : inProcessPaymentStatuses
            ORDER BY c2g__PaymentMediaControl__r.c2g__Payment__c, c2g__LineNumber__c];
        return results;
    }

    public List<c2g__codaPaymentMediaSummary__c> getPaymentMediaSummaries_Void(Id paymentCollectionId){
        List<c2g__codaPaymentMediaSummary__c> results=null;
        results=[
            SELECT Id,
            Name,
            c2g__PaymentValueTotal__c,
            c2g__PaymentMediaControl__r.c2g__Payment__c,
            c2g__Account__c, 
            Check_Memo__c,
            c2g__PaymentReference__c
            FROM c2g__codaPaymentMediaSummary__c 
            WHERE c2g__PaymentMediaControl__r.c2g__Payment__r.Payment_Collection__c = :paymentCollectionId 
            AND c2g__PaymentMediaControl__r.c2g__Payment__r.c2g__Status__c in ('Matched','Part Cancelled', 'Canceling Error')
            ORDER BY c2g__PaymentMediaControl__r.c2g__Payment__c, c2g__LineNumber__c];
        return results;
    }

    public List<c2g__codaPayment__c> getPaymentsReadyToPost(Set<Id> paymentIds){
        List<c2g__codaPayment__c> results=null;
        results=[
            SELECT Id,
            Name,
            c2g__Status__c,
            c2g__PaymentValueTotal__c,
            Payment_Collection__c
            FROM c2g__codaPayment__c 
            WHERE Id in :paymentIds 
            AND (c2g__Status__c in : postedPaymentStatuses OR c2g__Status__c in : readyToPostPaymentStatuses)
            ORDER BY Name];
        return results;
    }

    public List<c2g__codaPayment__c> getPaymentsByIds(Set<Id> paymentIds){
        List<c2g__codaPayment__c> results=null;
        results=[
            SELECT Id,
            Name,
            c2g__Status__c,
            c2g__PaymentValueTotal__c,
            Payment_Collection__c
            FROM c2g__codaPayment__c 
            WHERE Id in :paymentIds
            ORDER BY Name];
        return results;
    }

    public List<c2g__codaPayment__c> getPaymentsByIds(Set<String> paymentIds){
        List<c2g__codaPayment__c> results=null;
        results=[
            SELECT Id,
            Name,
            c2g__Status__c,
            c2g__PaymentValueTotal__c,
            Payment_Collection__c
            FROM c2g__codaPayment__c 
            WHERE Id in :paymentIds
            ORDER BY Name];
        return results;
    }

    public List<c2g__codaPayment__c> getPayments(String paymentCollectionId){
        List<c2g__codaPayment__c> results=null;
        results=[
            select 
            Id,
            Name,
            c2g__Status__c,
            c2g__PaymentValueTotal__c,
            Payment_Collection__c
            from c2g__codaPayment__c 
            where 
            Payment_Collection__c = :paymentCollectionId             
            order by Name
            ];
        return results;
    }

    public Payment_Collection__c getPaymentCollection(Id paymentCollectionId){
        Payment_Collection__c result=null;
        List<Payment_Collection__c> pcList =[
            SELECT Id,
                Bank_Account__c,
                Bank_Account__r.Id,
                Company__c,
                Company__r.Id,
                Company__r.OwnerId,
                Company__r.c2g__CustomerSettlementDiscount__c,
                Currency_Write_Off_GLA__c,
                Description__c,
                Name,
                Payment_Currency__c,
                Payment_Currency__r.Id,
                Payment_Date__c,
                Payment_Method__c,
                Period__c,
                Period__r.Id,
                Settlement_Discount_GLA__c,
                Split_Invoices_on_Separate_Payments__c
            FROM Payment_Collection__c
            WHERE ID = :paymentCollectionId];

        if(pcList.size()>0){
            result=pcList[0];
        }
        return result;
    }

    public PaymentProcessing.PaymentCollectionWrapper loadPaymentCollectionData(Id paymentCollectionId){
        //Deserialization method.  Loads up the object graph.
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        Payment_Collection__c paymentCollection=null;
        paymentCollection =getPaymentCollection(paymentCollectionId);

        if(paymentCollection!=null)
        {
            paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();
            paymentCollectionWrapper.paymentCollection=paymentCollection;

            List<c2g__codaPayment__c> payments = null;
            payments = getPayments(paymentCollectionId);

            if((payments!=null)&&(payments.size()>0))
            {

                Map<Id, c2g__codaPayment__c> paymentsById=new Map<Id, c2g__codaPayment__c>(payments);
                Map<Id, PaymentProcessing.PaymentWrapper> paymentWrappersByPaymentId=null;
                paymentWrappersByPaymentId=new Map<Id, PaymentProcessing.PaymentWrapper>();

                paymentCollectionWrapper.payments=new List<PaymentProcessing.PaymentWrapper>();
                for(c2g__codaPayment__c payment: payments){
                    PaymentProcessing.PaymentWrapper wrapper=null;
                    wrapper=new PaymentProcessing.PaymentWrapper(payment);
                    paymentCollectionWrapper.payments.add(wrapper);
                    paymentWrappersByPaymentId.put(payment.Id, wrapper);
                }                

                List<c2g__codaPaymentMediaSummary__c> paymentMediaSummaries = null;
                paymentMediaSummaries = getPaymentMediaSummaries(paymentCollectionId);

                for(c2g__codaPaymentMediaSummary__c paymentMediaSummary: paymentMediaSummaries){
                    PaymentProcessing.PaymentSummaryWrapper summaryWrapper=null;
                    summaryWrapper=new PaymentProcessing.PaymentSummaryWrapper();
                    summaryWrapper.paymentMediaSummary = paymentMediaSummary;
                    summaryWrapper.paymentId = paymentMediaSummary.c2g__PaymentMediaControl__r.c2g__Payment__c;
                    summaryWrapper.accountId = paymentMediaSummary.c2g__Account__c;
                    //These are where the check numbers are stored.

                    PaymentProcessing.PaymentWrapper paymentWrapper=null;
                    paymentWrapper=paymentWrappersByPaymentId.get(summaryWrapper.paymentId);
                    if(paymentWrapper.paymentSummaryWrappers==null){
                        paymentWrapper.paymentSummaryWrappers=new List<PaymentProcessing.PaymentSummaryWrapper>();
                    }
                    paymentWrapper.paymentSummaryWrappers.add(summaryWrapper);
                }

            }

        }

        return paymentCollectionWrapper;
    }    

    //queries for the payment processing batch status
    public List<Payment_Processing_Batch_Status__c> findBatchStatusMonitor(Id paymentCollectionId) {
        Payment_Processing_Batch_Status__c[] monitors = [
            SELECT 
            Id, 
            Name, 
            Payment_Collection__c, 
            Payment_Collection__r.Payment_Method__c, 

            Payment__c, 
            Payment__r.c2g__Status__c,
            Payment__r.Name, 
            
            Are_Proposal_Lines_Added__c, 
            Is_Media_Data_Created__c, 
            Are_Check_Numbers_Updated__c, 
            Is_Posted_and_Matched__c, 
            
            Post_Job_Id__c, 
            
            Has_Post_and_Match_Error__c, 
            Post_and_Match_Last_Error_Message__c, 

            Has_Error__c, 
            Last_Error_Message__c, 
            Message__c, 

            Update_Check_Numbers_Job_Id__c,
            Has_Update_Check_Numbers_Error__c,
            Update_Check_Numbers_Last_Error_Message__c,
                                    
            Ordinal__c
            FROM Payment_Processing_Batch_Status__c
            WHERE Payment_Collection__c = :paymentCollectionId];
        return monitors;
    }
    
}