/**
 *  mmintake_IInvestigationEventsSelector
 */
public interface mmintake_IInvestigationEventsSelector
    extends mmlib_ISObjectSelector
{
    list<IntakeInvestigationEvent__c> selectById( Set<Id> idSet );
    list<IntakeInvestigationEvent__c> selectScheduledByIntakesWithIntakeAndNames( Set<Id> intakeIdSet );
    list<IntakeInvestigationEvent__c> selectByIncidentInvestigationsNotRelatedToIntakesWithIntakeAndNames( Set<Id> incidentInvestigationIdSet, Set<Id> intakeIdSet );
    list<IntakeInvestigationEvent__c> selectCanceledRescheduledByIntakesWithIntakeAndNames( Set<Id> intakeIdSet );
}