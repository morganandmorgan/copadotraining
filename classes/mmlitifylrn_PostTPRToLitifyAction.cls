/**
 *  mmlitifylrn_PostTPRToLitifyAction
 */
public class mmlitifylrn_PostTPRToLitifyAction
    extends mmlib_AbstractAction
    implements Database.AllowsCallouts
{
    private mmlitifylrn_ICredentials credentials = null;

    public override Boolean runInProcess()
    {
        // get all of the available LRN credentials
        credentials = mmlitifylrn_Credentials.newInstance( mmlitifylrn_CredentialsSelector.newInstance().selectAll() );

        if ( credentials.getRecords().isEmpty() )
        {
            // there are no credentials, now what?
            system.debug( LoggingLevel.WARN, 'There are no credentials for third party referral management.');
            return false;
        }

        map<id, litify_pm__Firm__c> firmsManagedMap = new map<id, litify_pm__Firm__c>( mmlitifylrn_FirmsSelector.newInstance().selectBySelectedThirdPartyReferralManaged() );

        if ( firmsManagedMap.isEmpty() )
        {
            // there are no firms selected for third part management, now what?
            system.debug( LoggingLevel.WARN, 'There are no firms selected for third party referral management.');
            return false;
        }

        // requery the records that were passed into the process so that they can be updated.  Be sure to include their parent originating firm, handling firm, and case type records
        list<litify_pm__Referral__c> recordsFreshlyQueried = mmlitifylrn_ReferralsSelector.newInstance().selectWithFirmAndCaseTypeById( new map<id, sobject>(this.records).keyset() );

        // the records are populated.
        // the records are litify_pm__Referral__c records that need to be sent to Litify's Referral Network

        mmlitifylrn_CalloutAuthorization auth = new mmlitifylrn_CalloutAuthorization()
                                                    .setCrendentials( credentials )
                                                    .setUow( uow );
        system.debug( auth.getCredentials() );

        // loop through the records
        for (litify_pm__Referral__c record : recordsFreshlyQueried)
        {
            // get the firm making the referral
            litify_pm__Firm__c firmMakingTheReferral = firmsManagedMap.get( record.litify_pm__Originating_Firm__c );
            system.debug('JLW:::Firm: ' + ( firmMakingTheReferral == null ? 'NULL' : firmMakingTheReferral.Name + ' -- ' + firmMakingTheReferral.Id));
            system.debug( 'Referral record id : ' + record.id
                        + '\n -- OF : ' + record.litify_pm__Originating_Firm__c
                        + '\n -- RecordType : ' + record.RecordTypeId
                        + '\n ' + record
                        + '\n\n\n'
                            );
            system.debug( firmMakingTheReferral );

            if ( firmMakingTheReferral == null )
            {
                // if this were to occur, then there is a configuration problem
                record.litify_pm__sync_status__c = mmlitifylrn_Constants.REFERRAL_SYNCSTATUS.THIRDPARTYDRAFT.name();

                record.litify_pm__sync_msg__c = litify_pm__Referral__c.litify_pm__Originating_Firm__c.getDescribe().getLabel() + ' specified for '
                                                    + litify_pm__Referral__c.SObjectType.getDescribe().getLabel()
                                                    + ' has not been configured for management of its referrals as a third party.  Please find that firm record and check the \''
                                                    + litify_pm__Firm__c.mmlitifylrn_IsOutgoingReferralsManaged__c.getdescribe().getLabel() + '\' field.'
                                                    ;

                continue;
            }

            // get credential appropriate to the firm of this referral
            // create the appropriate authorization record
            //auth = new mmlitifylrn_CalloutAuthorization().setSessionToken( credentials.getSessionTokenForFirm( firmMakingTheReferral ) );
            auth.setCurrentFirmMakingCallout( firmMakingTheReferral );

            system.debug( auth.getCredentials().getSessionTokenForFirm( firmMakingTheReferral) );

            // execute the callout
            try
            {
                // execute the callout and update the record
                performCalloutAndUpdateReferralStatus( record, auth );
            }
            catch(mmlitifylrn_Exceptions.CalloutException ce)
            {
                // problems occured
                record.litify_pm__sync_status__c = mmlitifylrn_Constants.REFERRAL_SYNCSTATUS.THIRDPARTYDRAFT.name();
                record.litify_pm__sync_msg__c = 'Status Code: ' + ce.getStatusCode() + ' -- ' + ce.getCalloutErrorMessage();
            }

            if ( uow != null )
            {
                uow.registerDirty( record );
            }
        }

        return true;
    }

    private void performCalloutAndUpdateReferralStatus( litify_pm__Referral__c record, mmlitifylrn_CalloutAuthorization auth )
    {
        mmlitifylrn_LRNModels.ReferralIntakeBasicData referralIntake = null;

        if ( record.litify_pm__Handling_Firm__c == null )
        {
            // the handling firm is not specified, use the "referral intakes" LRN endpoint
            mmlitifylrn_ReferralIntakesPostCallout.Response resp = (mmlitifylrn_ReferralIntakesPostCallout.Response) new mmlitifylrn_ReferralIntakesPostCallout()
                                   .setReferral( record )
                                   .setAuthorization( auth )
                                   //.debug()
                                   .execute()
                                   .getResponse();

            referralIntake = resp.referral_intake;
        }
        else
        {
            // the handling firm is specified.  Callout to the "referral agreements" LRN endpoint and then callout to the "referral" LRN endpoint.
            mmlitifylrn_ReferralAgreementsCallout.Response referralAgreementsResponse = (mmlitifylrn_ReferralAgreementsCallout.Response) new mmlitifylrn_ReferralAgreementsCallout()
                    .setHandlingOrganization( record.litify_pm__Handling_Firm__r )
                    .setAuthorization( auth )
                    //.debug()
                    .execute()
                    .getResponse();

            system.debug( referralAgreementsResponse.getTotalNumberOfRecordsFound() );

            // Was an agreement found?
            if ( referralAgreementsResponse.getTotalNumberOfRecordsFound() == 0 )
            {
                // since there is no agreement available between this originating firm and this handling firm,
                //  just submit the referral with no handling firm specified.
                mmlitifylrn_ReferralIntakesPostCallout.Response resp = (mmlitifylrn_ReferralIntakesPostCallout.Response) new mmlitifylrn_ReferralIntakesPostCallout()
                                   .setReferral( record )
                                   .setAuthorization( auth )
                                   //.debug()
                                   .execute()
                                   .getResponse();

                referralIntake = resp.referral_intake;
            }
            else
            {
                // default to first agreement found
                mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement agreement = null;

                mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement tbdAgreement = null;

                for ( mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement anAgreement : referralAgreementsResponse.agreements )
                {
                    if ( 'percentage'.equalsIgnoreCase( anAgreement.agreement_type ) )
                    {
                        // use this one
                        agreement = anAgreement;
                        break;
                    }

                    if ( tbdAgreement == null
                        && 'to_be_determined'.equalsIgnoreCase( anAgreement.agreement_type ) )
                    {
                        // this is the first TBD agreement and we will hold that off to the side.
                        tbdAgreement = anAgreement;
                    }
                }

                // if no percentage agreement was found and a TBD agreement is available
                if ( agreement == null
                    && tbdAgreement != null )
                {
                    // then use that one.
                    agreement = tbdAgreement;
                }


                mmlitifylrn_ReferralsCallout.Response resp = (mmlitifylrn_ReferralsCallout.Response) new mmlitifylrn_ReferralsCallout()
                                   .setReferral( record )
                                   .setHandlingOrganization( record.litify_pm__Handling_Firm__r )
                                   .setSuggestedReferralAgreement( agreement )
                                   .setAuthorization( auth )
                                   //.debug()
                                   .execute()
                                   .getResponse();

                referralIntake = resp.referral.referral_intake;
            }
        }

        // the callout was successful, otherwise a mmlitifylrn_Exceptions.CalloutException would have been thrown
        // update the referral record here

        record.litify_pm__sync_status__c = mmlitifylrn_Constants.REFERRAL_SYNCSTATUS.THIRDPARTYSYNCED.name();
        record.litify_pm__ExternalId__c = integer.valueOf( referralIntake.id );

        record.litify_pm__Status__c = referralIntake.referral_intake_status;

        try
        {
            record.litify_pm__LRN_Created_At__c = referralIntake.created_at != null ? datetime.valueOfGmt( referralIntake.created_at.replace('T',' ').left(20) ) : null;
        }
        catch(System.TypeException te)
        {
            system.debug( te );
            system.debug( te.getStackTraceString() );
        }

        try
        {
            record.litify_pm__Sync_last_updated_at__c = referralIntake.updated_at != null ? datetime.valueOfGmt( referralIntake.updated_at.replace('T',' ').left(20) ) : null;
        }
        catch(System.TypeException te)
        {
            system.debug( te );
            system.debug( te.getStackTraceString() );
        }

        //record.litify_pm__External_Ref_Number__c = null;
        //record.litify_pm__Handling_Firm__c
        //record.litify_pm__Referrals__c
    }
}