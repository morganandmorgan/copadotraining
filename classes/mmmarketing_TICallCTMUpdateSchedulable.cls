/**
 *  mmmarketing_TICallCTMUpdateSchedulable
 *
 *  @see mmmarketing_TICallCTMUpdateSchedule
 */
public class mmmarketing_TICallCTMUpdateSchedulable
    implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        Type targettype = Type.forName('mmmarketing_TICallCTMUpdateSchedule');

        if ( targettype != null )
        {
            mmlib_ISchedule obj = (mmlib_ISchedule)targettype.newInstance();
            obj.execute(sc);
        }
    }

    public static void setupSchedule( String crontabSchedule )
    {
        if ( string.isblank(crontabSchedule) )
        {
            crontabSchedule = '0 15 * * * ?';
        }

        String scheduleJobString = mmmarketing_TICallCTMUpdateSchedulable.class.getName() + ' scheduled job';

        mmmarketing_TICallCTMUpdateSchedulable clazz = new mmmarketing_TICallCTMUpdateSchedulable();

        String jobID = system.schedule(scheduleJobString, crontabSchedule, clazz);

        system.debug( 'the jobID == '+ jobID);
    }

    public static void setupScheduleEvery15Minutes()
    {
        String scheduleJobString = mmmarketing_TICallCTMUpdateSchedulable.class.getName() + ' scheduled job';

        mmmarketing_TICallCTMUpdateSchedulable clazz = new mmmarketing_TICallCTMUpdateSchedulable();

        system.debug( 'the jobID == '+ system.schedule(scheduleJobString + ' 00', '0 0 * * * ?', clazz));
        system.debug( 'the jobID == '+ system.schedule(scheduleJobString + ' 15', '0 15 * * * ?', clazz));
        system.debug( 'the jobID == '+ system.schedule(scheduleJobString + ' 30', '0 30 * * * ?', clazz));
        system.debug( 'the jobID == '+ system.schedule(scheduleJobString + ' 45', '0 45 * * * ?', clazz));
    }

}