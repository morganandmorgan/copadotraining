public interface mmlitifylrn_IReferrals extends mmlib_ISObjectDomain
{
    void postThirdPartyReferralToLitify();
    void postThirdPartyReferralToLitifyInQueueableMode();
}