public class mmwiz_MDTVerificationLogic
{
    private static final string ACTION_TOKENS = 'actionTokens';
    private static final string CHILD_TOKENS = 'chidlTokens';
    private static final string BUSINESS_RULE_TOKENS = 'busRuleTokens';

    public static map<string, map<string, set<string>>> verifySpecificWizardMDT( set<string> tokensToVerify )
    {
        // find all Wizard__mdt records
        list<Wizard__mdt> records = mmwiz_WizardsSelector.newInstance().selectByDeveloperName( tokensToVerify );

        return verifyRequestedWizardMDTRecords( records );
    }

    public static map<string, map<string, set<string>>> verifyAllWizardMDT()
    {
        // find all Wizard__mdt records
        list<Wizard__mdt> records = mmwiz_WizardsSelector.newInstance().selectAll();

        return verifyRequestedWizardMDTRecords( records );
    }

    private static map<string, map<string, set<string>>> verifyRequestedWizardMDTRecords( list<Wizard__mdt> records)
    {
        map<string, map<string, set<string>>> verificationResponses = new map<string, map<string, set<string>>>();

        mmwiz_AbstractBaseModel model = null;
        map<string, mmwiz_AbstractBaseModel> modelByTokenMap = new map<string, mmwiz_AbstractBaseModel>();

        map<string, map<string, set<string>>> tokenSetByTypeByModelReferencingMap = new map<string, map<string, set<string>>>();

        // for each wizardRecord,
        for ( Wizard__mdt record : records )
        {
            model = mmwiz_ModelFactory.generateFromWizard( record );

            modelByTokenMap.put( record.DeveloperName, model );

            if ( ! tokenSetByTypeByModelReferencingMap.containsKey( model.getToken() ) )
            {
                tokenSetByTypeByModelReferencingMap.put( model.getToken(), new map<string, set<string>>() );

                tokenSetByTypeByModelReferencingMap.get( model.getToken() ).put( ACTION_TOKENS, new Set<String>() );
                tokenSetByTypeByModelReferencingMap.get( model.getToken() ).put( CHILD_TOKENS, new Set<String>() );
                tokenSetByTypeByModelReferencingMap.get( model.getToken() ).put( BUSINESS_RULE_TOKENS, new Set<String>() );
            }

            // sort through the action tokens
            for ( String tokenReferenced : model.actionTokens )
            {
                system.debug( 'Adding to ' + model.getToken() + ' the action token reference of ' + tokenReferenced);
                tokenSetByTypeByModelReferencingMap.get( model.getToken() ).get( ACTION_TOKENS ).add( tokenReferenced );
            }

            // sort through the child tokens
            for ( String tokenReferenced : model.childTokens )
            {
                system.debug( 'Adding to ' + model.getToken() + ' the child element token reference of ' + tokenReferenced);
                tokenSetByTypeByModelReferencingMap.get( model.getToken() ).get( CHILD_TOKENS ).add( tokenReferenced );
            }

            // TODO: Un-comment this section when business rules are ready.  See JIRA Ticket MM-1466
            // sort through the business rule tokens
            //for ( String tokenReferenced : model.actionTokens )
            //{
            //    system.debug( 'Adding to ' + model.getToken() + ' the business rule token reference of ' + tokenReferenced);
            //    tokenSetByTypeByModelReferencingMap.get( model.getToken() ).get( BUSINESS_RULE_TOKENS ).add( tokenReferenced );
            //}
        }

        // now, loop through the tokenSetByTypeByModelReferencingMap and check each reference to the modelByTokenMap
        for ( String modelTokenKey : tokenSetByTypeByModelReferencingMap.keySet() )
        {
            for ( String actionTokenKey : tokenSetByTypeByModelReferencingMap.get( modelTokenKey ).get( ACTION_TOKENS ) )
            {
                system.debug('Reviewing model ' + modelTokenKey + ' action reference to ' + actionTokenKey);

                if ( ! modelByTokenMap.containsKey( actionTokenKey ) )
                {
                    addMissingTokenToResponse( verificationResponses, modelTokenKey, ACTION_TOKENS, actionTokenKey );
                }
            }

            for ( String childTokenKey : tokenSetByTypeByModelReferencingMap.get( modelTokenKey ).get( CHILD_TOKENS ) )
            {
                system.debug('Reviewing model ' + modelTokenKey + ' child reference to ' + childTokenKey);

                if ( ! modelByTokenMap.containsKey( childTokenKey ) )
                {
                    addMissingTokenToResponse( verificationResponses, modelTokenKey, CHILD_TOKENS, childTokenKey );
                }
            }

            // TODO: Un-comment this section when business rules are ready.  See JIRA Ticket MM-1466
            //for ( String busRulesTokenKey : tokenSetByTypeByModelReferencingMap.get( modelTokenKey ).get( BUSINESS_RULE_TOKENS ) )
            //{
            //    system.debug('Reviewing model ' + modelTokenKey + ' business rule reference to ' + busRulesTokenKey);
            //
            //    if ( ! modelByTokenMap.containsKey( busRulesTokenKey ) )
            //    {
            //        addMissingTokenToResponse( verificationResponses, modelTokenKey, BUSINESS_RULE_TOKENS, busRulesTokenKey );
            //    }
            //}
        }
        return verificationResponses;

    }

    private static void addMissingTokenToResponse( map<string, map<string, set<string>>> verificationResponses, string modelTokenKey, string missingTokenType, string missingToken)
    {
        if ( ! verificationResponses.containsKey( modelTokenKey ) )
        {
            verificationResponses.put( modelTokenKey, new map<string, set<string>>());
        }

        if ( ! verificationResponses.get( modelTokenKey ).containsKey( missingTokenType ) )
        {
            verificationResponses.get( modelTokenKey ).put( missingTokenType, new set<string>() );
        }

        verificationResponses.get( modelTokenKey ).get( missingTokenType ).add( missingToken );
    }
}