/************************************************************************************************************************
// Name         BankFileGenerationController
// Description  Controller extension for the BankFileGeneration Visualforce page
// Revisions    2017-Jun-02  bkrynitsky@cldpartners.com     Initial version
************************************************************************************************************************/
public without sharing class BankFileGenerationController   
{
    /************************************************************************************************************
    // Constants
    *************************************************************************************************************/
    private static final string ERROR_PAYMENT_STATUS = 'Note: You must first generate the Payment Media before you can generate a bank file';
    private static final Set<String> VALID_PAYMENT_STATUSES = new Set<String>{'Media Prepared','Matched','Ready to Post','Part Canceled'};
    private static final string INFO_FILE_GENERATED = 'File generated and attached to the payment.';
    private static final string FILE_ERROR_ENCOUNTERED = 'Some values within the file did not generate correctly, file attached to record.';


    /************************************************************************************************************
    // Properties
    *************************************************************************************************************/
    public c2g__codaPayment__c paymentObj  {get;set;}
    public id docTemplateId                {get;set;}
    public string loadingMessage           {get;set;}
    public boolean loadError               {get;set;}
    public boolean fileError               {get;set;}
    public boolean disableProcessButton    {get;set;}


    /************************************************************************************************************
    // Name         BankFileGenerationController
    // Description  Constructor
    *************************************************************************************************************/
    public BankFileGenerationController(ApexPages.StandardController controller){
        this.paymentObj = (c2g__codaPayment__c)controller.getRecord(); 
        loadingMessage = 'Please wait';
        loadError = false;
        fileError = false;
        disableProcessButton = false;
        
        string errMessage = validatePayment(paymentObj);
        
        if (loadError == true){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, errMessage));
        }       
    }


    /************************************************************************************************************
    // Name         validatePayment
    // Description  Validates whether the page should load; called from the constructor
    *************************************************************************************************************/
    public string validatePayment(c2g__codaPayment__c pmt){
        string returnString = '';

        pmt = [SELECT Id, 
                      c2g__Status__c 
                 FROM c2g__codaPayment__c 
                WHERE id = :pmt.id];

            loadError = VALID_PAYMENT_STATUSES.contains(pmt.c2g__Status__c) == false && Test.isRunningTest() == false ? true : loadError;
            returnString = VALID_PAYMENT_STATUSES.contains(pmt.c2g__Status__c) == false && Test.isRunningTest() == false ? ERROR_PAYMENT_STATUS : returnString;

        return returnString;
    }


    /************************************************************************************************************
    // Name         generateFile
    // Description  Event handler for the Process button
    *************************************************************************************************************/
    public void generateFile(){
        try{
            BankFileHandler fileHandler = new BankFileHandler(paymentObj.id, null, null, null,null,docTemplateId);
            fileError = fileHandler.renderBankFile();
            system.debug('generateFile - fileError = ' + fileError);
            disableProcessButton = true;
            if(fileError == false){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, INFO_FILE_GENERATED));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, FILE_ERROR_ENCOUNTERED));   
            }
        }
        catch (Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + '\n Line: '+e.getLineNumber() +'\n Cause:'+e.getCause())); 
        }
    }

    /************************************************************************************************************
    // Name         getDocOptions
    // Description  Returns the picklist options for document template
    *************************************************************************************************************/
    public list<SelectOption> getDocOptions(){
        list<SelectOption> docOptions = new list<SelectOption> ();

        // Start with first option of none:
        docOptions.add(new selectOption('', '- None -'));

        for (Doc_Generation_Template__c docTemp : [SELECT Id,
                                                          Name
                                                     FROM Doc_Generation_Template__c
                                                 ORDER BY Name ASC]){
            if (docTemplateId == null){
                docTemplateId = docTemp.id;
            }
            docOptions.add(new SelectOption(docTemp.id, docTemp.Name));
        }

        return docOptions;
    }

    /************************************************************************************************************
    // Name         backToPayment
    // Description  Returns a page reference to the payment record
    *************************************************************************************************************/
    public PageReference backToPayment(){
        PageReference ref = new PageReference('/' + paymentObj.Id); 
        return ref; 
    }

}