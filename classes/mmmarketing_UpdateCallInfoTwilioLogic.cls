public class mmmarketing_UpdateCallInfoTwilioLogic implements mmmarketing_IUpdateCallInfoLogic {

    private map<String, mmlib_NamedCredentialCalloutAuth> apiAuthorizationsByIdentifierMap = new map<String, mmlib_NamedCredentialCalloutAuth>();
    private List<Twilio_Account_Mapping__c> twilioAccountMappings = new List<Twilio_Account_Mapping__c>();
    private map<string, Twilio_Account_Mapping__c> apiAuthToTwilioAccountMappingMap = new map<string, Twilio_Account_Mapping__c>();
    private set<string> toPhoneNumbersCurrentlyBeingTrackedSet = new set<string>();
    private boolean isDebugOn = false;

    public mmmarketing_UpdateCallInfoTwilioLogic() {
        TwilioIntegrationRelated__c customSetting = TwilioIntegrationRelated__c.getInstance();

        if (customSetting != null && customSetting.IsIntegrationDebugEnabled__c == true) {
            this.isDebugOn = true;
        }

        mmtwilio_APIAccessNamedCredAuthorization apiAuthorization = null;
        twilioAccountMappings = new mmtwilio_AccountMappingSelector().selectAll();
        for (Twilio_Account_Mapping__c record : twilioAccountMappings) {
            apiAuthorization = new mmtwilio_APIAccessNamedCredAuthorization();
            apiAuthorization.setIdentifier(record.Twilio_Auth_Named_Credential__c);
            apiAuthorization.setDomain(record.Domain_Value__c);

            this.apiAuthorizationsByIdentifierMap.put(apiAuthorization.getIdentifier(), apiAuthorization);
            this.apiAuthToTwilioAccountMappingMap.put(apiAuthorization.getIdentifier(), record);
            this.toPhoneNumbersCurrentlyBeingTrackedSet.add(mmlib_Utils.stripPhoneNumber(record.Phone_Number__c));
        }
    }

    public map<String, mmlib_NamedCredentialCalloutAuth> getAvailableAuthorizations() {
        return this.apiAuthorizationsByIdentifierMap;
    }

    public mmlib_BaseCallout prepareCalloutToService(Marketing_Tracking_Info__c record, string identifier) {
        return (mmtwilio_ListCallsCallout) ((new mmtwilio_ListCallsCallout())
                .setQueryStartDate(record.Tracking_Event_Timestamp__c.date())
                .setCallerNumber(record.Calling_Phone_Number__c)
                .setTwilioAccountID(this.apiAuthToTwilioAccountMappingMap.get(identifier).Twilio_Account_Id__c)
                .setAuthorization(apiAuthorizationsByIdentifierMap.get(identifier)));
    }

    // same logic as full resolution (for now)
    public void updateMTICallRecordShallowReconciliationIfMatch(Marketing_Tracking_Info__c record
            , mmlib_BaseCallout.CalloutResponse theCallServiceResponse
            , String identifier
            , mmlib_ISObjectUnitOfWork uow) {

        Boolean found = false;

        mmtwilio_ListCallsCallout.Response twilioResponse = (mmtwilio_ListCallsCallout.Response) theCallServiceResponse;
        list<mmtwilio_ListCallsCallout.ResponseCall> twilioCalls = twilioResponse.getTwilioCalls();
        string workingString = null;
        Twilio_Account_Mapping__c mappingRecord = null;

        String debugStr = 'Processing...\n';
        for (mmtwilio_ListCallsCallout.ResponseCall twilioCall : twilioCalls) {
            if ((twilioCall.getCalledAtDatetime() >= record.Tracking_Event_Timestamp__c.addMinutes(-15)
                    && twilioCall.getCalledAtDatetime() <= record.Tracking_Event_Timestamp__c.addMinutes(15))
                    && toPhoneNumbersCurrentlyBeingTrackedSet.contains(mmlib_Utils.stripPhoneNumber(twilioCall.to))
                    ) {

                System.debug('First 3 comparisons passed\n');

                record.Calling_Phone_Number__c = mmlib_Utils.stripPhoneNumber(twilioCall.from_formatted);
                record.Tracking_Event_Timestamp__c = twilioCall.getCalledAtDatetime();
                record.Call_Info_Resolution_Status__c = 'ProcessedCallResolvedShallow';


                for (String indentifierKey : apiAuthToTwilioAccountMappingMap.keyset()) {
                    mappingRecord = apiAuthToTwilioAccountMappingMap.get(indentifierKey);
                    System.debug(mmlib_Utils.stripPhoneNumber(mappingRecord.Phone_Number__c) + ' = ' + mmlib_Utils.stripPhoneNumber(twilioCall.to));
                    if (mmlib_Utils.stripPhoneNumber(mappingRecord.Phone_Number__c).equalsIgnoreCase(mmlib_Utils.stripPhoneNumber(twilioCall.to))) {

                        record.Domain_Value__c = mappingRecord.Domain_Value__c;
                        record.Source_Value__c = mappingRecord.Source_Value__c;

                        found = true;
                        uow.registerDirty(record);
                        break;
                    }
                }
            }
        }
    }

    public void updateMTICallRecordIfMatch(Marketing_Tracking_Info__c record
            , mmlib_BaseCallout.CalloutResponse theCallServiceResponse
            , String identifier
            , mmlib_ISObjectUnitOfWork uow) {

        Boolean found = false;

        mmtwilio_ListCallsCallout.Response twilioResponse = (mmtwilio_ListCallsCallout.Response) theCallServiceResponse;
        list<mmtwilio_ListCallsCallout.ResponseCall> twilioCalls = twilioResponse.getTwilioCalls();
        string workingString = null;
        Twilio_Account_Mapping__c mappingRecord = null;

        for (mmtwilio_ListCallsCallout.ResponseCall twilioCall : twilioCalls) {

            System.debug('IF Comparison 1: ' + twilioCall.getCalledAtDatetime() + ' >= ' + record.Tracking_Event_Timestamp__c.addMinutes(-60));
            System.debug('IF Comparison 2: ' + twilioCall.getCalledAtDatetime() + ' <= ' + record.Tracking_Event_Timestamp__c.addMinutes(60));
            System.debug('IF Comparison 3:' + mmlib_Utils.stripPhoneNumber(twilioCall.to) + ' IN ' + JSON.serialize(toPhoneNumbersCurrentlyBeingTrackedSet));
            System.debug('We want true for all 3 in order to go into the IF' + '\n');
            if ((twilioCall.getCalledAtDatetime() >= record.Tracking_Event_Timestamp__c.addMinutes(-60)
                    && twilioCall.getCalledAtDatetime() <= record.Tracking_Event_Timestamp__c.addMinutes(60))
                    && toPhoneNumbersCurrentlyBeingTrackedSet.contains(mmlib_Utils.stripPhoneNumber(twilioCall.to))
                    ) {
                System.debug('First 3 comparisons passed\n');

                record.Calling_Phone_Number__c = mmlib_Utils.stripPhoneNumber(twilioCall.from_formatted);
                record.Tracking_Event_Timestamp__c = twilioCall.getCalledAtDatetime();
                record.Call_Info_Resolution_Status__c = 'ProcessedCallResolved';

                for (Twilio_Account_Mapping__c twilioConfig : twilioAccountMappings) {
                    System.debug(mmlib_Utils.stripPhoneNumber(twilioConfig.Phone_Number__c) + ' = ' + mmlib_Utils.stripPhoneNumber(twilioCall.to));
                    if (mmlib_Utils.stripPhoneNumber(twilioConfig.Phone_Number__c).equalsIgnoreCase(mmlib_Utils.stripPhoneNumber(twilioCall.to))) {

                        record.Domain_Value__c = twilioConfig.Domain_Value__c;
                        record.Source_Value__c = twilioConfig.Source_Value__c;

                        found = true;
                        break;
                    }
                }
            }
        }

        uow.registerDirty(record);
    }

    public void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}