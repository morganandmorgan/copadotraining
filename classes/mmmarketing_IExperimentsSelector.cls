/**
 *  mmmarketing_IExperimentsSelector
 */
public interface mmmarketing_IExperimentsSelector extends mmlib_ISObjectSelector
{
    List<Marketing_Experiment__c> selectById(Set<Id> idSet);
    List<Marketing_Experiment__c> selectWithVariationsByExperimentName(Set<String> nameSet);
}