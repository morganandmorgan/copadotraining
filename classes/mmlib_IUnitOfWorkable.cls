/**
 *  mmlib_IUnitOfWorkable
 */
public interface mmlib_IUnitOfWorkable
{
    mmlib_IUnitOfWorkable setUnitOfWork( mmlib_ISObjectUnitOfWork uow );
}