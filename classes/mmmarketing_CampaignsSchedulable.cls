/**
 *  mmmarketing_CampaignsSchedulable
 */
global class mmmarketing_CampaignsSchedulable
    implements Schedulable
{
    private static final String scheduleJobString = mmmarketing_CampaignsSchedulable.class.getName() + ' scheduled job';

    global void execute(SchedulableContext sc)
    {
        try
        {
            list<Marketing_Financial_Period__c> records = mmmarketing_FinancialPeriodsSelector.newInstance().selectWhereCampaignNotLinked();

            mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance( new mmlib_AtomicDML() );

            mmmarketing_FinancialPeriods financialPeriods = new mmmarketing_FinancialPeriods( records );

            financialPeriods.setUnitOfWork( uow );

            financialPeriods.autoCreateMarketingCampaignsIfRequired();

            uow.commitWork();
        }
        catch (Exception e)
        {
            system.debug(e);
        }

        // now that the job has executed, abort it from the schedule table.
        System.abortJob( sc.getTriggerId() );

        // are there more records that should be processed?
        if ( ! mmmarketing_FinancialPeriodsSelector.newInstance().selectOneWhereCampaignNotLinked().isEmpty() )
        {
            scheduleNextRun( 2 );
        }

    }

    private static void scheduleNextRun( final integer numberOfMinutesDelay )
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticCampaignSetupEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticCampaignSetupEnabled__c )
        {
            mmmarketing_CampaignsSchedulable clazz = new mmmarketing_CampaignsSchedulable();

            // execute this run 3 minutes from now
            datetime datetimeToRun = datetime.now().addMinutes( numberOfMinutesDelay );

            String sch = mmlib_utils.generateCronTabStringFromDatetime( datetimeToRun );

            try
            {
                String jobID = system.schedule(scheduleJobString, sch, clazz);

                system.debug( 'the jobID == '+ jobID);
            }
            catch ( Exception e )
            {
                system.debug( e );
            }
        }
    }

    public static void setupCreateCampaignsFromMFP()
    {
        list<CronTrigger> currentCronjobs = mmlib_CronTriggersSelector.newInstance().selectScheduledApexByName( new Set<String>{ scheduleJobString } );

        if ( currentCronjobs.isEmpty() )
        {
            CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

            integer numberOfMinutesDelay = customSetting == null ? 10 : integer.valueOf( customSetting.DelayCampaignSetupScheduleMinutes__c );

            scheduleNextRun( numberOfMinutesDelay );
        }
    }
}