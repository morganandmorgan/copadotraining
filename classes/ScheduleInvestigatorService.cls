public with sharing class ScheduleInvestigatorService {
  public class ConflictingAppointmentException extends Exception {}
  public class AvailableSlot {
    // TODO make some things private
    public Boolean selectedSlot { get; set; }
    public User investigator { get; set;}
    public Integer timeFrameHour { get; set; }
    public Integer timeFrameMinute { get; set; }
    public String addressOfNextAppointment { get; set; }
    public String addressOfPriorAppointment { get; set; }
    public Integer slotNumber { get; set; }

    public String priorEventName;
    public String nextEventName;
    public Boolean hasConflict { get; private set; }
  }

  private static final String INVESTIGATION_STATUS_SCHEDULED = 'Scheduled';
  public static final String SUBJECT_HOME = 'Home';
  public static final String SUBJECT_TRAVEL = 'Travel';
  public static final String SUBJECT_INVESTIGATION_MULTIPLE = 'Multiple Investigations';
  public static final String SUBJECT_INVESTIGATION_SINGLE = 'Investigation';
  public static final String EVENT_TYPE = 'Investigation';

  public static Map<Id, List<AvailableSlot>> GetInvestigatorAvailability(Date forDate, Integer hoursRequired, Boolean allowTimeOverride, List<String> requiredTerritories, List<String> requiredLanguages, List<String> requiredSkills) {

    Map<Id, List<AvailableSlot>> userAvailableSlots = new Map<Id, List<AvailableSlot>>();

    if (forDate < Date.today()) {
      return userAvailableSlots;
    }

    if (forDate == Date.today() && DateTime.now().hour() == 23) {
      // we only schedule at least an hour out so at 11pm we should find nothing
      return userAvailableSlots;
    }

    List<User> investigators = getInvestigators(requiredTerritories, requiredLanguages, requiredSkills);
    List<Event> allInvestigatorEventForDate = getInvestigatorEventsForDate(investigators, forDate);
    Map<User, List<Event>> userEventsMap = GetMapOfUsersToExistingEvents(allInvestigatorEventForDate, investigators);

    
    // build availability for date
    Integer currentSlot = 0; // todo: remove
    for (User userIterator : userEventsMap.KeySet()) {

      List<Event> existingEvents = userEventsMap.get(userIterator);
      DateTime startDateTime = getInvestigatorStartTime(existingEvents, forDate, allowTimeOverride);
      if (startDateTime == null) {
        continue;
      }
      Time startTime = startDateTime.time();
      DateTime investigatorEndTime = getInvestigatorEndTime(existingEvents, forDate, allowTimeOverride);

      Event previousEvent = null;
      AvailableSlot previousSlot = null;

      while (true) {
        Event overlappingEvent = findConflictingAppointments(existingEvents, forDate, startTime, hoursRequired);
        if (overlappingEvent != null && (allowTimeOverride != true || overlappingEvent.Type == EVENT_TYPE)) {
          if (previousSlot != null) {
            previousSlot.addressOfNextAppointment = overlappingEvent.Location;
            previousSlot = null;
          }
          previousEvent = overlappingEvent;
        } else {

          if (investigatorEndTime != null && startTime >= investigatorEndTime.addHours(-1).time()) {
            break;
          }
          // if we're here then there's an availability
          List<AvailableSlot> availableSlots = userAvailableSlots.get(userIterator.id);
          if (availableSlots == null) {
            availableSlots = new List<AvailableSlot>();
            userAvailableSlots.put(userIterator.id, availableSlots);
          }

          AvailableSlot slot = new AvailableSlot();
          slot.investigator = userIterator;
          slot.slotNumber = currentSlot++;
          slot.timeFrameHour = startTime.hour();
          slot.timeFrameMinute = startTime.minute();
          slot.selectedSlot = false;
          if (previousEvent != null) {
            slot.addressOfPriorAppointment = previousEvent.Location;
            previousEvent = null;
          }
          slot.hasConflict = (overlappingEvent != null && allowTimeOverride == true);
          
          availableSlots.add(slot);
          previousSlot = slot;
        }

        Time nextStartTime = startTime.addHours(2);
        if (nextStartTime < startTime) { // time wrapped around
          break;
        }
        startTime = nextStartTime;
      }
    }
    
    // if today remove availability before an hour from now
    // (we do this after population in order to ensure previous appointments are set)
    if (forDate == Date.today()) {
      // move forward by 2 hours until we get to at least an hour from now
      Time nextHour = DateTime.now().addHours(1).time();

      Map<Id, List<AvailableSlot>> slotsInFuture = new Map<Id, List<AvailableSlot>>();

      Set<Id> userIds = userAvailableSlots.keySet();
      for (Id uid : userIds) {
        List<AvailableSlot> newSlotsList = new List<AvailableSlot>();
        for(AvailableSlot slot : userAvailableSlots.get(uid)) {
          if (Time.newInstance(slot.timeFrameHour, slot.timeFrameMinute, 0, 0) > nextHour) {
            newSlotsList.add(slot);
          }
        }
        if (newSlotsList.size() > 0) {
          userAvailableSlots.put(uid, newSlotsList);
        }
      }
    }


    return userAvailableSlots;

  }


  private static Event findConflictingAppointments(List<Event> events, Date onDate, Time startTime, Integer hoursRequired) {
    DateTime newEventStart = DateTime.newInstance(onDate, startTime);
    DateTime newEventEnd = newEventStart.addHours(hoursRequired);
    Event conflictingEvent = null;
    for (Event ev : events) { 
      if (eventIsAllDay(ev)
          || eventEncompassesNewEvent(ev, newEventStart, newEventEnd)
          || eventEncompassedByNewEvent(ev, newEventStart, newEventEnd)
          || eventStartsDuringNewEvent(ev, newEventStart, newEventEnd)
          || eventEndsDuringNewEvent(ev, newEventStart, newEventEnd)
          || newEventStartsDuringEvent(ev, newEventStart, newEventEnd)
          || newEventEndsDuringEvent(ev, newEventStart, newEventEnd)) {

        if (ev.Type == EVENT_TYPE) {
          conflictingEvent = ev;
          break;
        }
        else if (conflictingEvent == null) {
          conflictingEvent = ev;
        }
      }
    }
    return conflictingEvent;
  }

  private static Boolean eventIsAllDay(Event e) {
    return e.IsAllDayEvent == true;
  }

  private static Boolean eventEncompassesNewEvent(Event ev, Datetime newEventStart, Datetime newEventEnd) {
    return ev.StartDateTime <= newEventStart && ev.EndDateTime >= newEventEnd;
  }

  private static Boolean eventEncompassedByNewEvent(Event ev, Datetime newEventStart, Datetime newEventEnd) {
    return newEventStart <= ev.StartDateTime && newEventEnd >= ev.EndDateTime;
  }

  private static Boolean eventStartsDuringNewEvent(Event ev, Datetime newEventStart, Datetime newEventEnd) {
    return ev.StartDateTime >= newEventStart && ev.StartDateTime < newEventEnd;
  }

  private static Boolean eventEndsDuringNewEvent(Event ev, Datetime newEventStart, Datetime newEventEnd) {
    return ev.EndDateTime > newEventStart && ev.EndDateTime <= newEventEnd;
  }

  private static Boolean newEventStartsDuringEvent(Event ev, Datetime newEventStart, Datetime newEventEnd) {
    return newEventStart >= ev.StartDateTime && newEventStart < ev.EndDateTime;
  }

  private static Boolean newEventEndsDuringEvent(Event ev, Datetime newEventStart, Datetime newEventEnd) {
    return newEventEnd > ev.StartDateTime && newEventEnd <= ev.EndDateTime;
  }

  private static DateTime getInvestigatorEndTime(List<Event> investigatorEvents, Date onDate, Boolean allowTimeOverride) {
    DateTime startTime = getInvestigatorStartTime(investigatorEvents, onDate, allowTimeOverride);
    
    if (startTime == null) {
      return null;
    }

    if (allowTimeOverride == true) {
      return DateTime.newInstance(onDate.addDays(1), Time.newInstance(0, 0, 0, 0));
    }

    for (Event ev : investigatorEvents) {
      if (ev.Subject == SUBJECT_HOME && ev.StartDateTime > startTime) {
        return ev.StartDateTime;
      }
    }

    // investigator does not have an end time during the list of events passed in
    return null;
  }

  private static DateTime getInvestigatorStartTime(List<Event> investigatorEvents, Date onDate, Boolean allowTimeOverride) {

    if (allowTimeOverride == true) {
      return DateTime.newInstance(onDate, Time.newInstance(0, 0, 0, 0));
    }

    DateTime startDateTime = null;

    for (Event ev : investigatorEvents) {
      if (eventIsAllDay(ev)) {
        return null;
      }
      else if (ev.Subject == SUBJECT_HOME) {
        // if this is either the first home time of the day or contiguous home time
        // e.g. 12am-2am, 2am-8am
        // then investigator start time is the end of this home time
        if (startDateTime == null || startDateTime == ev.StartDateTime) {
          startDateTime = ev.EndDateTime;
        }
      }
    }

    if (startDateTime == null) {
      startDateTime = DateTime.newInstance(onDate, Time.newInstance(0, 0, 0, 0));
    }

    return startDateTime;
  }

  private static List<User> getInvestigators(List<String> selectedTerritories, List<String> selectedLanguages, List<String> selectedSkills) {

    List<String> userFields = new List<String>{
      'Id',
      'Name',
      'Language__c',
      'Skill__c',
      'Territory__c'
    };

    List<String> filterConditions = new List<String>{
      'IsActive = true',
      'Territory__c != NULL' // THIS IS THE ONLY THING PREVENTING US FROM FINDING EVERY SINGLE USER
    };

    if (selectedTerritories != null && selectedTerritories.size() > 0) {
      List<String> territoryConditions = new List<String>();
      for (String t : selectedTerritories) {
        territoryConditions.add('Territory__c LIKE \'%' + t + '%\'');
      }
      filterConditions.add('(' + String.join(territoryConditions, ' OR ') + ')');
    }

    if (selectedLanguages != null && selectedLanguages.size() > 0) {
      String delimitedLanguages = String.join(selectedLanguages, ';');
      filterConditions.add('Language__c INCLUDES (:delimitedLanguages)');
    }

    if (selectedSkills != null && selectedSkills.size() > 0) {
      String delimitedSkills = String.join(selectedSkills, ';');
      filterConditions.add('Skill__c INCLUDES (:delimitedSkills)');
    }

    String soqlString = 'SELECT ' + String.join(userFields, ',')
        + ' FROM User'
        + ' WHERE ' + String.join(filterConditions, ' AND ')
        + ' ORDER BY Name ASC';

    return (List<User>) Database.query(soqlString);
  }


  private static List<Event> getInvestigatorEventsForDate(List<User> investigators, Date eventDate) {
    DateTime scheduledDateStart = DateTime.newInstance(eventDate, Time.newInstance(0, 0, 0, 0));
    DateTime scheduledDateEnd = scheduledDateStart.addDays(1);

    Set<Id> investigatorIds = new Set<Id>();
    for (User u : investigators) {
      investigatorIds.add(u.Id);
    }

    return [
      SELECT 
        Owner.Id,
        Owner.Name,
        ActivityDate,
        IsAllDayEvent,
        StartDateTime,
        EndDateTime,
        Location,
        Subject,
        Type
      FROM Event
      WHERE OwnerId IN :investigatorIds
        AND ((IsAllDayEvent = true 
              AND DAY_ONLY(StartDateTime) <= :eventDate AND DAY_ONLY(EndDateTime) >= :eventDate)
          OR (IsAllDayEvent = false
              AND ((StartDateTime >= :scheduledDateStart AND StartDateTime <= :scheduledDateEnd)
                OR (EndDateTime >= :scheduledDateStart AND EndDateTime <= :scheduledDateEnd))))
      ORDER BY Owner.Name,
        StartDateTime
    ];
  }

  public static Event scheduleIncidentMeeting( Id incidentId, List<Id> intakeIds, User investigator, Set<String> attendeeContactIds, Date eventDate, Time meetingStartHour, Integer lengthOfMeetingInHours, String eventDescription, String location, String documentNames, String delayedSignupReason, Boolean allowTimeOverride) {

    // ensure we don't double book
    List<Event> existingEventsForInvestigator = getInvestigatorEventsForDate(new List<User>{investigator}, eventDate);

    Event overlappingEvent = findConflictingAppointments(existingEventsForInvestigator, eventDate, meetingStartHour, lengthOfMeetingInHours);

    if (overlappingEvent != null && (allowTimeOverride != true || overlappingEvent.Type == EVENT_TYPE)) {
      throw new ConflictingAppointmentException('meeting conflicts with existing event ' + overlappingEvent);
    }

    List<Event> events = new List<Event>();

    Event meetingEvent = buildIncidentMeetingEvent(incidentId,
            intakeIds,
            investigator,
            attendeeContactIds,
            eventDate,
            meetingStartHour,
            lengthOfMeetingInHours,
            eventDescription,
            location,
            documentNames,
            delayedSignupReason);
    events.add(meetingEvent);

    events.add(buildTravelEvent(investigator, meetingEvent, meetingEvent.StartDateTime.AddHours(-1)));
  
    events.add(buildTravelEvent(investigator, meetingEvent, meetingEvent.EndDateTime));

    
    Database.insert(events);
    
    List<EventRelation> contactEventLinks = buildEventRelationsForNewEvent(meetingEvent.Id, attendeeContactIds);
    Database.insert(contactEventLinks);

    return meetingEvent;
  }

  private static Event buildTravelEvent(User selectedInvestigator, Event investigation, DateTime travelStart) {

    Event priorTravelEvent = new Event(
      Subject = SUBJECT_TRAVEL,
      StartDateTime = travelStart,
      EndDateTime = travelStart.AddHours(1),
      IsAllDayEvent = false,
      ShowAs = 'OutofOffice',
      OwnerId = selectedInvestigator.Id,
      WhatId = investigation.WhatId
    );
    return priorTravelEvent;
  }

  private static Event buildIncidentMeetingEvent( String incidentId, List<Id> intakeIds, User selectedInvestigator,  Set<String> contactIds, Date eventDate,  Time meetingStartHour, Integer lengthOfMeetingInHours, String eventDescription, String location, String documentNames, String delayedSignupReason) {

    Event meetingEvent = new Event();
    meetingEvent.Additional_Documents__c = documentNames == null ? null : documentNames.removeEnd(']').removeStart('[');
    meetingEvent.Reason_for_Delayed_Signup__c = delayedSignupReason;

    meetingEvent.Description = eventDescription;
    meetingEvent.Subject = contactIds.size() > 1 ? SUBJECT_INVESTIGATION_MULTIPLE : SUBJECT_INVESTIGATION_SINGLE ;
    meetingEvent.IsAllDayEvent = false;
    meetingEvent.OwnerId = selectedInvestigator.Id;

    DateTime eventStartDateTime = DateTime.newInstance(eventDate, meetingStartHour);
    DateTime eventEndDateTime = eventStartDateTime.addHours(lengthOfMeetingInHours);

    DateTime eventReminderDateTime = eventStartDateTime.addHours(-1);

    meetingEvent.ReminderDateTime = eventReminderDateTime;
    meetingEvent.StartDateTime = eventStartDateTime;
    meetingEvent.EndDateTime = eventEndDateTime;
    meetingEvent.IsReminderSet = true;

    IncidentInvestigationEvent__c incidentInvestigation = new IncidentInvestigationEvent__c(
      OwnerId = selectedInvestigator.Id,
      StartDateTime__c = meetingEvent.StartDateTime,
      EndDateTime__c = meetingEvent.EndDateTime,
      Incident__c = incidentId
    );
    insert incidentInvestigation;

    List<IntakeInvestigationEvent__c> intakeInvestigations = new List<IntakeInvestigationEvent__c>();
    for (Id intakeId : intakeIds) {
      IntakeInvestigationEvent__c intakeInvestigation = new IntakeInvestigationEvent__c(
        OwnerId = selectedInvestigator.Id,
        StartDateTime__c = meetingEvent.StartDateTime, 
        EndDateTime__c = meetingEvent.EndDateTime, 
        Intake__c = intakeId, 
        IncidentInvestigation__c = incidentInvestigation.Id
      );
      intakeInvestigations.add(intakeInvestigation);
    }
    insert intakeInvestigations;

    meetingEvent.WhatId = incidentInvestigation.Id;
    meetingEvent.Type = EVENT_TYPE;
    meetingEvent.Investigation_Status__c = INVESTIGATION_STATUS_SCHEDULED;  

    meetingEvent.Location = location;
    return meetingEvent;
  }

  private static List<EventRelation> buildEventRelationsForNewEvent(Id eventId, Set<String> contactIds) {
    List<EventRelation> contactEventLinks = new List<EventRelation>();
    for (String contactId : contactIds) {
      EventRelation newLink = new EventRelation(
        EventId = eventId,
        RelationId = contactId,
        Status = 'Accepted', 
        IsInvitee = true,
        IsParent = true, 
        IsWhat = false
      );
      contactEventLinks.add(newLink);
    }
    return contactEventLinks;
  }

  private static Map<User, List<Event>> GetMapOfUsersToExistingEvents(List<Event> eventList, List<User> userList) {
    Map<Id, List<Event>> userIdEventMap = new Map<Id, List<Event>>();
    for (Event e : eventList) {
      Id ownerId = e.OwnerId;
      if (ownerId != null && ownerId.getSObjectType() == User.SObjectType) {
        List<Event> usersEvents = userIdEventMap.get(ownerId);
        if (usersEvents == null) {
          usersEvents = new List<Event>();
          userIdEventMap.put(ownerId, usersEvents);
        }
        usersEvents.add(e);
      }
    }

    Map<User, List<Event>> userEventMap = new Map<User, List<Event>>();
    for (User u : userList) {
      List<Event> events = userIdEventMap.get(u.Id);
      if (events == null) {
        events = new List<Event>();
      }
      userEventMap.put(u, events);
    }
    return userEventMap;
  }
}