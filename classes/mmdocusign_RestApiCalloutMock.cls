@isTest
global class mmdocusign_RestApiCalloutMock
	implements HttpCalloutMock
{
    // {"url": "https://host/path"}

	private String responseString = '';
    private Integer statusCode = -1;

	public mmdocusign_RestApiCalloutMock(String responseString, Integer statusCode)
	{
		this.responseString = responseString;
        this.statusCode = statusCode;
	}

    global HTTPResponse respond(HTTPRequest req)
	{
        HttpResponse res = new HttpResponse();
        res.setBody(responseString);
        res.setStatusCode(statusCode);
        return res;
    }
}