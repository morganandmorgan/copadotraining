public class mmgmap_Address
{
    private String street;
    private String city;
    private String state;
    private String postalCode;
    private String country;

    public mmgmap_Address setStreet( final string street )
    {
    	this.street = street;
    	return this;
    }

    public mmgmap_Address setCity( final string city )
    {
    	this.city = city;
    	return this;
    }

    public mmgmap_Address setState( final string state )
    {
    	this.state = state;
    	return this;
    }

    public mmgmap_Address setPostalCode( final string postalCode )
    {
    	this.postalCode = postalCode;
    	return this;
    }

    public mmgmap_Address setCountry( final string country )
    {
    	this.country = country;
    	return this;
    }

    public String getStreet()
    {
    	return this.street;
    }

    public String getCity()
    {
    	return this.city;
    }

    public String getState()
    {
    	return this.state;
    }

    public String getPostalCode()
    {
    	return this.postalCode;
    }

    public String getCountry()
    {
    	return this.country;
    }

	public Boolean isValidAddress()
	{
		// Data requirements: Street && State && Country && (City || Postal Code)
		return
			String.isNotEmpty(this.getStreet()) &&
			String.isNotEmpty(this.getState()) &&
			String.isNotEmpty(this.getCountry()) &&
			(
				String.isNotEmpty(this.getCity()) ||
				String.isNotEmpty(this.getPostalCode())
			);
	}
}