public class PaymentProcessingStatusUpdateBatch implements Database.Batchable<SObject>, Database.Stateful {
    
    // public Payment_Collection__c paymentCollection {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    public Id paymentCollectionId {get; set;}    
        
    // public PaymentProcessingStatusUpdateBatch(Payment_Collection__c paymentCollection){
    //     this.paymentCollection=paymentCollection;        
    //     this.paymentCollectionId=paymentCollection.Id;
    // }	

    public PaymentProcessingStatusUpdateBatch(Id paymentCollectionId){
        //TODO:  Consider allowing payment Id somehow.
        this.paymentCollectionId=paymentCollectionId;
    }    
          
    public Database.QueryLocator start(Database.BatchableContext batchContext) {        
        String soql=null;
        soql= 
'select  ' + 
'Id,  ' + 
'Name,  ' + 
'Payment_Collection__c,  ' + 
'Payment_Collection__r.Payment_Method__c,  ' + 
'Payment_Collection__r.Name,  ' + 
' ' + 
'Payment__c,  ' + 
'Payment__r.c2g__Status__c,  ' + 
'Payment__r.Name,  ' + 
' ' + 
'Are_Proposal_Lines_Added__c,  ' + 
'Is_Media_Data_Created__c,  ' + 
'Are_Check_Numbers_Updated__c,  ' + 
'Is_Posted_and_Matched__c,  ' + 
' ' + 
'Post_Job_Id__c,  ' + 
' ' + 
'Has_Post_and_Match_Error__c,  ' + 
' ' + 
'Has_Error__c,  ' + 
'Last_Error_Message__c,  ' + 
'Message__c,  ' + 
' ' + 
'Update_Check_Numbers_Job_Id__c, ' + 
'Has_Update_Check_Numbers_Error__c, ' + 
'Update_Check_Numbers_Last_Error_Message__c, ' + 
' ' + 
'Ordinal__c ' + 
' ' + 
'from Payment_Processing_Batch_Status__c  ' + 
'where  ' + 
'Payment_Collection__c = \'' + paymentCollectionId + '\' ' + 
' ' + 
'order by ' + 
'Ordinal__c '  
;
        
        return Database.getQueryLocator(
            soql
        );
    }
    
    public void execute(Database.BatchableContext batchContext, List<Payment_Processing_Batch_Status__c> scope){
        // process each batch of records

        System.debug('PaymentProcessingStatusUpdateBatch execute() - ' + scope.size() + ' records.'); 

        PaymentProcessingStatusUpdater statusUpdater=null;

        statusUpdater=new PaymentProcessingStatusUpdater();

        statusUpdater.updatePaymentProcessingStatuses(scope);

    }
    
    public void finish(Database.BatchableContext batchContext){
        System.debug('PaymentProcessingStatusUpdateBatch finished.');                
    }
    
}