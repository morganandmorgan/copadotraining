/**
 *  mmmarketing_TrackInfoRecordCallRequest
 */
public class mmmarketing_TrackInfoRecordCallRequest
    implements mmmarketing_ITrackInfoRecordCallRequest
{
    private String callingNumber;
    private Datetime callStartTime;
    private Id relatedIntakeId;

    private mmmarketing_TrackInfoRecordCallRequest() { }

    public static mmmarketing_ITrackInfoRecordCallRequest newInstance()
    {
        return new mmmarketing_TrackInfoRecordCallRequest();
    }

    public mmmarketing_ITrackInfoRecordCallRequest setCallingNumber( String callingNumber )
    {
        this.callingNumber = callingNumber;
        return this;
    }

    public mmmarketing_ITrackInfoRecordCallRequest setCallStartTime( Datetime callStartTime )
    {
        this.callStartTime = callStartTime;
        return this;
    }

    public mmmarketing_ITrackInfoRecordCallRequest setRelatedIntakeId( Id relatedIntakeId )
    {
        this.relatedIntakeId = relatedIntakeId;
        return this;
    }

    public String getCallingNumber()
    {
        return this.callingNumber;
    }

    public Datetime getCallStartTime()
    {
        return this.callStartTime;
    }

    public Id getRelatedIntakeId()
    {
        return this.relatedIntakeId;
    }

}