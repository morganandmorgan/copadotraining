/**
 *  mmcommon_PersonAccountsServiceExceptions
 */
public with sharing class mmcommon_PersonAccountsServiceExceptions
{
    private mmcommon_PersonAccountsServiceExceptions() { }

    public virtual class ServiceException
        extends Exception
    {

    }

    public class ValidationException
        extends mmcommon_PersonAccountsServiceExceptions.ServiceException
    {

    }
}