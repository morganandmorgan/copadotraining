@istest
private class mmmatter_MatterTaskHistoryControllerTest {

    @istest
    private static void test() {

        Account account = TestUtil.createAccountBusinessMorganAndMorgan('Biz Account');
        insert account;

        Account officeAccount = TestUtil.createAccount('MM Office');
        officeAccount.Type = 'MM Office';
        insert officeAccount;

        Account personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        litify_pm__Case_Type__c caseType = TestUtil.createCaseType();
        insert caseType;

        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        matter.litify_pm__Case_Type__c = caseType.Id;
        matter.Assigned_Office_Location__c = officeAccount.Id;
        matter.AssignedToMMBusiness__c = account.Id;
        insert matter;

        insert TestUtil.createTask(matter.Id, 'the subject', 'Completed', 'Normal', '', System.today());

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.Subject = 'JW TEST from Anonymous Apex';
        emailMessage.HtmlBody = '<h3>BAM</h3><br/><br/><b><i>Pretty cool!</i></b>';
        emailMessage.RelatedToId = matter.Id;
        insert emailMessage;

        Test.startTest();
        mmmatter_MatterTaskHistoryController.getRecs(matter.Id, 15);
        Test.stopTest();
    }
}