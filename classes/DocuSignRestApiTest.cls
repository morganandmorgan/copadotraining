@isTest
public with sharing class DocuSignRestApiTest {

    @isTest
    public static void createAndSignTest() {

        DocuSignRestApi.RequestData rd = new DocuSignRestApi.RequestData();

        rd.docusign_template_ids = new List<String>{'8b752e50-838c-43aa-aff4-ec0875341fce','3701e385-01ee-418f-81c3-0a5e9de5baf5'};
        rd.recipient = new Map<String,String>();
        rd.recipient.put('email','mterrill@forthepeople.com');
        rd.recipient.put('name','Dax Test');
        rd.tokens = new Map<String,String>();
        rd.tokens.put('Name1','Name1 Value');
        rd.tokens.put('Name2','Name2 Value');
		rd.originUrl = 'https://forthepeople.com';
        
        String requestBody = Json.serialize(rd);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
            
        req.httpMethod = 'POST'; 
        req.requestBody = Blob.valueOf(requestBody);
        
        RestContext.request = req;
        RestContext.response = res;
        
        DocuSignRestApi.createAndSign();

    } //createAndSignTest

    
    @isTest
    public static void updateDocusignStatusTest() {
        DocuSignRestApi.UpdateData ud = new DocuSignRestApi.UpdateData();

        ud.envelopeId = '123abc456';
        ud.intakeId = 'abc123xyz';
        
        String requestBody = Json.serialize(ud);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
            
        req.httpMethod = 'PUT'; 
        req.requestBody = Blob.valueOf(requestBody);
        
        RestContext.request = req;
        RestContext.response = res;
        
        DocuSignRestApi.updateDocusignStatus();
        
    } //updateDocusignStatusTest
    
} //class