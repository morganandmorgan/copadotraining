/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OrderBy {
    global OrderBy() {

    }
    global litify_pm.OrderBy ByField(String fieldName) {
        return null;
    }
    global litify_pm.OrderBy ByField(String fieldName, litify_pm.OrderBy.OrderDirection direction) {
        return null;
    }
    global static litify_pm.OrderBy NewInstance() {
        return null;
    }
    global override String toString() {
        return null;
    }
global enum OrderDirection {ASCENDING, DESCENDING}
}
