public interface mmlawsuit_ILawsuitsService
{
    void updateStatusForMatterChange(Map<Id, String> matterStatusMap);
    void updateTeamMemberAssignmentsForUserChange(Set<Id> userIdSet);
    void updateTeamMemberAssignmentsFromMatter(Set<Id> matterIdSet);
}