/**
 * A batch job to sync Content from the file system that is associated with the Matter,
 * This is required to retroactively update Matter that was converted from Intakes 
 * with Docusign documents after Docusign updated to Content. This update was expected
 * on April 27, 2020 with v7, but it was found that Content was being created for intake
 * records as far back as April 4, 2020 at ~9:20am EST.
 * 
 * To run execute the following in anonymous apex. 
 * Notice the second parameter, which is a record scope.
 * 
 * -- BOUND BY DATES --
 * For all matter records with explicit bounds 
 * Note: bounds can be set to any DateTime you would like and should 
 * be a list of size 2 representing start and end time. Order does
 * not make a difference. 
 * 
 * The following example is the suggested bound pattern. It will
 * perform Content migration from the time the last Content Document
 * was uploaded by the User in context and a File Info record was 
 * created, to the time that the this class was deployed to the
 * org. This assumes that the DocsApi update to fix the Content 
 * migration issue was deployed at the same time:

// START BOUND SCRIPT
DateTime endBound = [ 
    SELECT CreatedDate
    FROM ApexClass 
    WHERE Name = :MatterFileSync_Job.class.getName()
    LIMIT 1
].CreatedDate;

List<litify_docs__File_Info__c> lastSync = [
    SELECT litify_docs__Related_To__c
    FROM litify_docs__File_Info__c
    WHERE CreatedById = :UserInfo.getUserId()
    ORDER BY CreatedDate DESC
    LIMIT 1
];

DateTime startBound;

if (lastSync.isEmpty()) {
    startBound = MatterFileSync_Job.CONTENT_IMPLEMENTATION_DATE;
} else {
    Id matterId = Id.valueOf(String.valueOf(lastSync[0].litify_docs__Related_To__c));

    litify_pm__Matter__c lastMatter = [
        SELECT CreatedDate
        FROM litify_pm__Matter__c
        WHERE Id = :matterId
    ];

    List<litify_pm__Matter__c> nextMatter = [
        SELECT CreatedDate
        FROM litify_pm__Matter__c
        WHERE CreatedDate > :lastMatter.CreatedDate
        ORDER BY CreatedDate ASC
        LIMIT 1
    ];

    if (!nextMatter.isEmpty()) {
        startBound = nextMatter[0].CreatedDate;
    }
}

if (startBound != null) {
    List<DateTime> bounds = new List<DateTime> { startBound, endBound };
    Database.executeBatch(new MatterFileSync_Job(bounds), 1);
}
// END BOUND SCRIPT

 *
 * -- EXPLICIT MATTER IDS --
 * For matter with a set of Matter Ids and explicit bounds:

// START MATTER IDS SCRIPT
Set<Id> matterIds = new Set<Id> {
    '123MatterId',
    '234MatterId'
};

Database.executeBatch(new MatterFileSync_Job(matterIds), 1);
// START MATTER IDS SCRIPT

 * 
 * NOTE: Ideally, this job should only be run one time right when the update for DocsApi is deployed
 * at a time when the system is stagnant, otherwise we could sync a document multiple times.
 * It likely would be a good idea to remove this batch job after the after it is run to avoid 
 * accidentally mass duplicating records in the future.
 */
global without sharing class MatterFileSync_Job implements Database.Batchable<SObject>, Database.AllowsCallouts {

    // Set the implementation date to April 4, 2020 since that is when the first Content Document is found in Prod
    public static final DateTime CONTENT_IMPLEMENTATION_DATE = DateTime.valueOf('2020-04-04 09:20:00');
    
    // Static for testing purposes. Loses context without it 
    @TestVisible
    private static Set<Id> testMatterIds;

    global DateTime boundStart;
    global DateTime boundEnd;
    global Set<Id> matterIds;

    /**
     * Constructor that allows you to sync Content Documents associated
     * with all Matter records within defined bounds.
     * @param bounds the lower and upper bounds to limit the Matter query
     */
    global MatterFileSync_Job(List<DateTime> bounds) {
        if (bounds == null || bounds.size() != 2 || bounds[0] == null || bounds[1] == null) {
            throw new XMatterFileSyncException('Bounds must be a list of two valid DateTimes, have ' + bounds);
        }

        Boolean flipBounds = bounds[0] > bounds[1];
        this.boundStart = flipBounds ? bounds[1] : bounds[0];
        this.boundEnd = flipBounds ? bounds[0] : bounds[1];
    }

    /**
     * Constructor that allows you to sync Content Documents associated 
     * with certain Matter records.
     * @param matterIds Ids of the matter you want to sync to Litify
     */
    global MatterFileSync_Job(Set<Id> matterIds) {
        this.matterIds = matterIds;

        if (Test.isRunningTest()) {
            testMatterIds = this.matterIds;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, CreatedDate FROM litify_pm__Matter__c ';

        // If we have explicitly set Matter to sync
        if (matterIds != null && !matterIds.isEmpty()) {
            query += 'WHERE Id IN :matterIds';
        } 
        // If we hae explicitly set bounds
        else if (boundStart != null && boundEnd != null) {
            query += 'WHERE CreatedDate >= :boundStart AND CreatedDate < :boundEnd';
        } else if (boundStart != null || boundEnd != null) {
            throw new XMatterFileSyncException('Both bounds must be valid DateTimes, have ' + boundStart + ' and ' + boundEnd);
        } 
        // Else at least print a debug letting the user know why this might fail
        else {
            throw new XMatterFileSyncException('Need valid matter or bounds, cannot perform sync on entire record set');
        }

        // Sort it from oldest to newest, the limit is a limit enforced by getQueryLocator()
        query += ' ORDER BY CreatedDate ASC LIMIT 49999';
        
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<litify_pm__Matter__c> matterRecords){
        matterIds = new Set<Id>();
        for (litify_pm__Matter__c matter : matterRecords) {
            if (boundStart == null || boundStart <= matter.CreatedDate) {
                matterIds.add(matter.Id);
            }
        }
        
        if (Test.isRunningTest()) {
            testMatterIds = matterIds;
        }

        if (matterIds.isEmpty()) {
            return;
        }

        System.debug('Processing ' + matterIds);

        if (!Test.isRunningTest()) {
            DocsApi.migrateContentDocuments(matterIds);
        }
    } 

    global void finish(Database.BatchableContext bc) {}

    public class XMatterFileSyncException extends Exception {}
}