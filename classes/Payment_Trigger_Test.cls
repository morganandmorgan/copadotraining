@isTest
private with sharing class Payment_Trigger_Test {

    @isTest 
    static void testInsertPayment(){

        System.debug('Creating payment...');

        c2g__codaPayment__c payment = null;
        //Note:  Looks like cnt doesn't do anything
        payment=TestDataFactory_FFA.createPayment(1, false);
        System.assert(payment!=null);

        Database.SaveResult result = null;        

        System.debug('Inserting payment...');

        Test.startTest();                
        
        result=Database.insert(payment);

        Test.stopTest();

        System.debug('Payment inserted.');

        System.assert(result.isSuccess());
        System.assert(payment.Id!=null);
        
    }

}