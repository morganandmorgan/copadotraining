public interface mmcti_ICTIScreenPopRedirectSelector
    extends mmlib_ISObjectSelector
{
    List<CTI_Screen_Pop_Redirect_Setting__mdt> selectAll();
}