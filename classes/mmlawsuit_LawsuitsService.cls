public class mmlawsuit_LawsuitsService
{
    public enum RelevantTeamRoleName {CASE_DEVELOPER, CASE_MANAGER, PRINCIPAL_ATTORNEY, HANDLING_ATTORNEY, MANAGING_ATTORNEY, PARALEGAL, LITIGATION_PARALEGAL}

    public static mmlawsuit_ILawsuitsService service()
    {
        return (mmlawsuit_ILawsuitsService) mm_Application.Service.newInstance(mmlawsuit_ILawsuitsService.class);
    }

    public static void updateStatusForMatterChange(Map<Id, String> matterStatusMap)
    {
        service().updateStatusForMatterChange(matterStatusMap);
    }

    public static void updateTeamMemberAssignmentsForUserChange(Set<Id> userIdSet)
    {
        service().updateTeamMemberAssignmentsForUserChange(userIdSet);
    }

    public static void updateTeamMemberAssignmentsFromMatter(Set<Id> matterIdSet)
    {
        service().updateTeamMemberAssignmentsFromMatter(matterIdSet);
    }
}