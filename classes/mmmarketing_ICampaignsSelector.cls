/**
 *  mmmarketing_ICampaignsSelector
 */
public interface mmmarketing_ICampaignsSelector extends mmlib_ISObjectSelector
{
    List<Marketing_Campaign__c> selectById( Set<Id> idSet );
    List<Marketing_Campaign__c> selectByTechnicalKeys( Set<String> technicalKeysSet );
}