public class IntakeInvestigationEventRollup_TrigAct extends TriggerAction {

  private final Set<Id> incidentInvestigationsToUpdate = new Set<Id>();
  
  public IntakeInvestigationEventRollup_TrigAct() {
    super();
  }

  public override Boolean shouldRun() {
    incidentInvestigationsToUpdate.clear();
    if (isAfter() && (isInsert() || isDelete())) {
      Map<Id, IntakeInvestigationEvent__c> intakeInvestigationMap = (Map<Id, IntakeInvestigationEvent__c>) triggerMap;
      for (IntakeInvestigationEvent__c intakeInvestigation : intakeInvestigationMap.values()) {
        if (intakeInvestigation.IncidentInvestigation__c != null) {
          incidentInvestigationsToUpdate.add(intakeInvestigation.IncidentInvestigation__c);
        }
      }
    }
    else if (isAfter() && isUpdate()) {
      Map<Id, IntakeInvestigationEvent__c> intakeInvestigationNewMap = (Map<Id, IntakeInvestigationEvent__c>) triggerMap;
      Map<Id, IntakeInvestigationEvent__c> intakeInvestigationOldMap = (Map<Id, IntakeInvestigationEvent__c>) triggerOldMap;
      for (IntakeInvestigationEvent__c intakeInvestigation : intakeInvestigationNewMap.values()) {
        IntakeInvestigationEvent__c oldIntakeInvestigation = intakeInvestigationOldMap.get(intakeInvestigation.Id);
        if (intakeInvestigation.IncidentInvestigation__c != oldIntakeInvestigation.IncidentInvestigation__c) {
          if (intakeInvestigation.IncidentInvestigation__c != null) {
            incidentInvestigationsToUpdate.add(intakeInvestigation.IncidentInvestigation__c);
          }
          if (oldIntakeInvestigation.IncidentInvestigation__c != null) {
            incidentInvestigationsToUpdate.add(oldIntakeInvestigation.IncidentInvestigation__c);
          }
        }
      }
    }
    return !incidentInvestigationsToUpdate.isEmpty();
  }

  public override void doAction() {
    if (!incidentInvestigationsToUpdate.isEmpty()) {
      Map<Id, IncidentInvestigationEvent__c> parentRecordsToUpdate = setAggregatesForIncidentInvestigations(incidentInvestigationsToUpdate);
      Database.update(parentRecordsToUpdate.values());
    }
  }

  private Map<Id, IncidentInvestigationEvent__c> setAggregatesForIncidentInvestigations(Set<Id> incidentInvestigationIds) {
    Map<Id, List<IntakeInvestigationEvent__c>> triggerRecordsByIncidentInvestigation = getIntakesInvestigationsByIncidentInvestigationId();

    Map<Id, IncidentInvestigationEvent__c> parentRecordsToUpdate = new Map<Id, IncidentInvestigationEvent__c>();

    List<AggregateResult> aggResults = [
      SELECT
        IncidentInvestigation__c, 
        COUNT_DISTINCT(Intake__c) uniqueIntakes
      FROM
        IntakeInvestigationEvent__c
      WHERE
        IncidentInvestigation__c IN :incidentInvestigationIds
      GROUP BY
        IncidentInvestigation__c
    ];

    for (AggregateResult aggResult : aggResults) {
      Id recordId = (Id) aggResult.get('IncidentInvestigation__c');
      Object uniqueIntakes = aggResult.get('uniqueIntakes');

      IncidentInvestigationEvent__c parentRecord = new IncidentInvestigationEvent__c(Id = recordId);
      parentRecord.Contracts_to_be_Signed__c = uniqueIntakes == null ? 0 : (Decimal) uniqueIntakes;

      if (triggerRecordsByIncidentInvestigation.containsKey(recordId)) {
        for (IntakeInvestigationEvent__c triggerRecord : triggerRecordsByIncidentInvestigation.get(recordId)) {
          if (String.isNotBlank(triggerRecord.Handling_Firm__c)) {
            parentRecord.Handling_Firm__c = triggerRecord.Handling_Firm__c;
            break;
          }
        }
      }

      parentRecordsToUpdate.put(recordId, parentRecord);
    }

    if (incidentInvestigationIds != parentRecordsToUpdate.keySet()) {
      List<IncidentInvestigationEvent__c> parentRecordsWithoutChildren = [
        SELECT
          Contracts_to_be_Signed__c
        FROM
          IncidentInvestigationEvent__c
        WHERE
          Id IN :incidentInvestigationIds
          AND Id NOT IN :parentRecordsToUpdate.keySet()
          AND Contracts_to_be_Signed__c != 0
      ];
      for (IncidentInvestigationEvent__c record : parentRecordsWithoutChildren) {
        record.Contracts_to_be_Signed__c = 0;
      }
      parentRecordsToUpdate.putAll(parentRecordsWithoutChildren);
    }

    return parentRecordsToUpdate;
  }

  private Map<Id, List<IntakeInvestigationEvent__c>> getIntakesInvestigationsByIncidentInvestigationId() {
    Map<Id, List<IntakeInvestigationEvent__c>> result = new Map<Id, List<IntakeInvestigationEvent__c>>();
    for (IntakeInvestigationEvent__c intakeInvestigation : (List<IntakeInvestigationEvent__c>) triggerMap.values()) {
      Id incidentInvestigationId = intakeInvestigation.IncidentInvestigation__c;
      List<IntakeInvestigationEvent__c> currentList = result.get(incidentInvestigationId);
      if (currentList == null) {
        currentList = new List<IntakeInvestigationEvent__c>();
        result.put(incidentInvestigationId, currentList);
      }
      currentList.add(intakeInvestigation);
    }
    return result;
  }
}