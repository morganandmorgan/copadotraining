/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SimpleSOQLQuery implements litify_pm.IQuery {
    global List<SObject> Execute() {
        return null;
    }
    global SObject First() {
        return null;
    }
    global litify_pm.SimpleSOQLQuery ForReference() {
        return null;
    }
    global litify_pm.SimpleSOQLQuery ForUpdate() {
        return null;
    }
    global litify_pm.SimpleSOQLQuery ForView() {
        return null;
    }
    global litify_pm.SimpleSOQLQuery Include(String assoc) {
        return null;
    }
    global static litify_pm.SimpleSOQLQuery NewInstance(Schema.SObjectType typeToQuery) {
        return null;
    }
    global static litify_pm.SimpleSOQLQuery NewInstance(String typeToQuery) {
        return null;
    }
    global litify_pm.SimpleSOQLQuery OrderBy(litify_pm.OrderBy o) {
        return null;
    }
    global litify_pm.SimpleSOQLQuery Predicate(litify_pm.IPredicate p) {
        return null;
    }
    global litify_pm.SimpleSOQLQuery SelectField(String f) {
        return null;
    }
    global litify_pm.SimpleSOQLQuery SelectFields(List<String> fs) {
        return null;
    }
    global litify_pm.SimpleSOQLQuery SetLimit(Integer l) {
        return null;
    }
    global litify_pm.SimpleSOQLQuery SetOffset(Integer o) {
        return null;
    }
}
