public with sharing class mmbrms_UserInSkillsSpecified
	implements mmbrms_IRuleEvaluation
{
	public List<String> skillsSpecifiedList = new List<String>();

	public Boolean evaluate()
	{
		return new mmcommon_UserInfo( UserInfo.getUserId() ).hasSkillName( skillsSpecifiedList );
	}
}