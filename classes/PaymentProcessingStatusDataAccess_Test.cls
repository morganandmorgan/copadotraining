@isTest
private without sharing class PaymentProcessingStatusDataAccess_Test {
       
    
    @isTest 
    static void test00(){
        //Note:  This class gets indirect positive tests via statusupdater.
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
    }

    @isTest 
    static void testCreate0(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
    }

    @isTest 
    static void testGetAddToProposalTrackingRecords0(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //c2g__codaPayment__c.sObjectType
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        List<Payment_Process_Add_To_Proposal_Status__c> statii=null;

        statii=target.getAddToProposalTrackingRecords(paymentCollectionId);

        System.assert(paymentCollectionId!=null);

    }    

    @isTest 
    static void testGetTrackingRecords0(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //c2g__codaPayment__c.sObjectType
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        List<Payment_Processing_Batch_Status__c> statii=null;

        statii=target.getPaymentProcessTrackingRecords(paymentCollectionId);

        System.assert(paymentCollectionId!=null);

    }

    @isTest 
    static void test1(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //c2g__codaPayment__c.sObjectType
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        List<Payment_Processing_Batch_Status__c> statii=null;

        statii=target.getPendingPostAndMatchTrackingRecords(paymentCollectionId);

        System.assert(paymentCollectionId!=null);

    }

    @isTest 
    static void test2(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //c2g__codaPayment__c.sObjectType
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        List<Payment_Processing_Batch_Status__c> statii=null;

        statii=target.getAllPaymentCollectionTrackingRecords(paymentCollectionId);

        System.assert(paymentCollectionId!=null);

    }
    


    @isTest 
    static void test3(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        List<Payment_Processing_Batch_Status__c> statii=null;

        statii=target.getAllPaymentTrackingRecords(paymentId);

        System.assert(paymentId!=null);

    }

    @isTest 
    static void test4(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        
        List<Payment_Processing_Batch_Status__c> statii=null;

        statii=target.getPendingPaymentPostAndMatchTrackingRecords(paymentId);

        System.assert(paymentId!=null);

    }

    @isTest 
    static void test5(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
                
        Id paymentCollectionId = null;

        paymentCollectionId=target.getPaymentCollectionId(paymentId);

        System.assert(paymentId!=null);

    }

    @isTest 
    static void testGetApexJobs0(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //List<AsyncApexJob> getPostAndMatchApexJobs(List<Id> apexJobIds){
        Id jobId = null;
        jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType);
                
        List<Id> apexJobIds = null;
        apexJobIds=new List<Id>();
        apexJobIds.add(jobId);

        List<AsyncApexJob> jobRecords=null;
        
        jobRecords=target.getPostAndMatchApexJobs(apexJobIds);

        System.assert(jobId!=null);

    }

    @isTest 
    static void testGetApexJobs1(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //List<AsyncApexJob> getPostAndMatchApexJobs(List<Id> apexJobIds){
        Id jobId = null;
        jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType);
                
        List<Id> apexJobIds = null;
        apexJobIds=new List<Id>();
        apexJobIds.add(jobId);

        List<AsyncApexJob> jobRecords=null;
        
        jobRecords=target.getRunningPostAndMatchApexJobs(apexJobIds);

        System.assert(jobId!=null);

    }

    @isTest 
    static void testGetApexJobs2(){
        PaymentProcessingStatusDataAccess target = null;
        target = new PaymentProcessingStatusDataAccess();
        System.assert(target!=null);
        
        //List<AsyncApexJob> getPostAndMatchApexJobs(List<Id> apexJobIds){
        Id jobId = null;
        jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType);
                
        List<Id> apexJobIds = null;
        apexJobIds=new List<Id>();
        apexJobIds.add(jobId);

        List<AsyncApexJob> jobRecords=null;
        
        jobRecords=target.getApexJobs(apexJobIds);

        System.assert(jobId!=null);

    }


}