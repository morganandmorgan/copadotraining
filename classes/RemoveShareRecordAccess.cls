/**
 * RemoveShareRecordAccess
 * @description RemoveShareRecordAccess class for to remove shared record when matter team member deleted or changed.
 * @author 10k
 * @date 6/18/2019
 */
global class RemoveShareRecordAccess implements Database.Batchable<sObject> {
    //Map of parent record is and user or group id
    Map<Id, Id> removePermissionMap = new Map<Id, Id>();

    //Constructor initialization
    global RemoveShareRecordAccess(Map<Id, Id> removePermissions) {
        removePermissionMap = removePermissions;
    }

    //Retrieve the selected record based on parent id and user id.
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return DataBase.getQueryLocator([SELECT Id ,UserOrGroupId FROM litify_pm__Matter__Share WHERE ParentId IN :removePermissionMap.keyset() And UserOrGroupId IN :removePermissionMap.values()  AND RowCause = 'Manual']);
    }

    //Ececute method will delete the all shared record when permission is no longer needed.
    global void execute(Database.BatchableContext BC,List<litify_pm__Matter__Share> scopeMatterShare) {

        //Check the list if it's contain share record then delete all shared records.
        if(!scopeMatterShare.isEmpty())
            delete scopeMatterShare;
    }

    //Finish method to execute at last.
    global void finish(Database.BatchableContext BC) {}
}