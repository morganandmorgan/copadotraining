public with sharing class IntakeCallHistoryExtension {

  public List<Task> tasks { get; private set; }

	public IntakeCallHistoryExtension(ApexPages.StandardController sc) {
		Id intakeId = sc.getId();

    SoqlUtils.SoqlQuery taskQuery = SoqlUtils.getSelect(getTaskFieldNames(), 'Task')
        // CallType != null and WhatId = :intakeId
      .withCondition(SoqlUtils.getAnd(
          SoqlUtils.getNotNull('CallType'),
          SoqlUtils.getEq('WhatId', intakeId)
          ))
      .withOrder(SoqlUtils.getOrder('CreatedDate').descending());

    tasks = (List<Task>) Database.query(taskQuery.toString());
	}

  private static List<String> getTaskFieldNames() {
    List<String> fieldNames = new List<String>();
    for (FieldSetMember fsm : SObjectType.Task.FieldSets.Intake_Call_History.getFields()) {
      fieldNames.add(fsm.getFieldPath());
    }
    if (fieldNames.isEmpty()) {
      fieldNames.add('Id');
    }
    return fieldNames;
  }
}