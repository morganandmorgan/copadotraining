/**
 * Expense_Trigger_Test
 * @description Test for Expense trigger.
 * @author Jeff Watson
 * @date 5/17/2019
 */
@isTest
public with sharing class Expense_Trigger_Test {

    @isTest
    private static void eventTrigger() {
        litify_pm__Expense_Type__c softExpenseType = new litify_pm__Expense_Type__c(Name = 'Soft Cost Recovered', CostType__c = 'SoftCost', ExternalID__c = 'SOFTCOSTRECOVERED');
        insert softExpenseType;

        insert new litify_pm__Expense_Type__c(Name = 'Hard Cost Recovered', CostType__c = 'HardCost', ExternalID__c = 'HARDCOSTRECOVERED');

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;

        litify_pm__Expense__c expense = TestUtil.createExpense(matter);
        expense.litify_pm__ExpenseType2__c = softExpenseType.Id;

        Test.startTest();
        insert expense;
        expense.litify_pm__Amount__c = 5.0;
        update expense;
        delete expense;
        Test.stopTest();
    }
}