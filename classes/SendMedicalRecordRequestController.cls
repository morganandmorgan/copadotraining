public class SendMedicalRecordRequestController {
    public class FacilityColumn {
        public String ColumnPrefix { get; private set; }
        public String PrettyName  { get; private set; }
        public String Value  { get; private set; }
        public List<String> DocumentsRequested { get; set; }
        public Boolean Selected { get; set; }
        public Boolean IsRush { get; set; }
        public String Style { get; set; }
        public FacilityColumn (String prefix, String v) {
            ColumnPrefix = prefix;
            Value = v;
            PrettyName = GetPrettyName(prefix);
            DocumentsRequested = new List<String>();
            Style = '';
            Selected = TRUE;
        }

        private String GetPrettyName(String name) {
            name = name.toLowerCase();
            List<String> parts = name.split('_');
            List<String> capitalParts = new List<String>();
            for (String p : parts) {
                capitalParts.add(p.capitalize());
            }
            return String.join(capitalParts, ' ');
        }
    }

    public List<FacilityColumn> FacilityColumns { get; private set;}
    public Lawsuit__c Lawsuit { get; private set; }
    public Questionnaire__c Questionnaire { get; private set; }
    private Boolean IsSandbox { get; set; }
    public Boolean HasErrors { 
        get {
            return ErrorList != null && ErrorList.size() > 0;
        }
    }
    public List<String> ErrorList { get; set; }
    public SendMedicalRecordRequestController() {
        ErrorList = new List<String>();
        ID lawsuitId = ApexPages.currentPage().getParameters().get('lid');
        Organization org = [SELECT Id, IsSandbox FROM Organization LIMIT 1];
        IsSandbox = org.IsSandbox;
        Lawsuit = [SELECT ID, Name, AccountId__r.ID , AccountId__r.Date_of_Birth_mm__c, AccountId__r.Social_Security_Number__c, AccountId__r.FirstName, AccountId__r.LastName, Intake__r.ID, Intake__r.What_Was_The_Date_Of_The_Incident__c FROM Lawsuit__c WHERE ID = :lawsuitId];

        if (String.isEmpty(Lawsuit.AccountId__r.Social_Security_Number__c)) {
            ErrorList.add('Social Security Number is required to request medical records');
        }
        if (Lawsuit.AccountId__r.Date_of_Birth_mm__c == null) {
            ErrorList.add('Date of Birth is required to request medical records');
        }
        if (FindHipaaForm(lawsuit) == null) {
            ErrorList.add('A signed HIPAA form is required to request medical records. The form should be attached to the intake and named HIPAA.PDF.');
        }       
        List<String> prefixes = getFacilityFieldPrefixes();
        
        Questionnaire = getQuestionnaireWithFacilityInfoFor(prefixes, Lawsuit.ID);
        //Facilities = prefixes;
        FacilityColumns = getFacilityColumnNamesThatContainData(prefixes, questionnaire);

        if (FacilityColumns.size() == 0) {
            ErrorList.add('No facilities found in the questionnaire');
        }
    }
    
    private Boolean DoValidate() {
        Boolean isValid = true;
        for (FacilityColumn col : FacilityColumns) {
            if (col.Selected == true && (col.DocumentsRequested == null || col.DocumentsRequested.size() == 0)) {
                isValid = false;
                col.Style = 'border:1px solid red;';
            } else {
                col.Style = '';
            }
        }

        return isValid;
    }
    public PageReference SendRequest() {
        String endpoint;
        if (IsSandbox) {
            endpoint = 'https://sandbox-thetreetop.cs18.force.com/services/apexrest/recordrequest';
        } else {
            endpoint = 'https://thetreetop.secure.force.com/services/apexrest/recordrequest';
        }



        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');

        if (!DoValidate()) {
            return null;
        }
        // make single request per facility
        Map<String, FacilityColumn> facilityPrefixes = new Map<String, FacilityColumn>();
        for (FacilityColumn col : FacilityColumns) {
            if (col.Selected) {
                facilityPrefixes.put(col.ColumnPrefix, col);
            }
        }
        
        List<Task> tasks = new List<Task>();
        for (String prefix : facilityPrefixes.keySet()) {
            Map<String, Object> objMap = getObjectMapForPosting(Lawsuit, prefix, questionnaire, facilityPrefixes.get(prefix));
            String body = JSON.serialize(objMap);
            System.debug('JSON BODY: ' + body);
            req.setBody(body);

            Http h = new Http();
            HTTPResponse res = h.send(req);
            System.debug('Response Body: ' + res.getBody());

            tasks.add(BuildTask(prefix, req, res));
        }

        insert tasks;

        return new PageReference('/' + Lawsuit.ID);
    }

    private Task BuildTask(String prefix, HttpRequest req, HttpResponse res) {
        Task t = new Task();
        if (res.getStatusCode() == 200 || res.getStatusCode() == 201) {
            t.Subject = 'Record request sent';
        } else {
            t.Subject = 'Record request send failed';
            t.Description = res.getStatusCode() + res.getBody() + ' (req:' + req.getBody() + ')';
        }
        t.Status = 'Completed';
        t.Priority = 'Normal';
        t.ActivityDate = Date.ValueOf(System.Now());
        t.WhatId = Lawsuit.id ;
        return t;
    }
    private Questionnaire__c getQuestionnaireWithFacilityInfoFor(List<String> columnPrefixes, ID lawsuitId) {
        String query = 'SELECT ID';
        for(String p : columnPrefixes) {
            query += ', ' + p + '_address__c, ' + p + '_city__c,';
            query += p + '_name__c, ' + p + '_state__c,';
            query += p + '_zip__c, ' + getPhoneFieldForPrefix(p);
        }
        
        query += ' FROM Questionnaire__c WHERE Lawsuits__c = :lawsuitId LIMIT 1';

        System.debug('QUERY: ' + query);
        return Database.Query(query);
    }

    private String getPhoneFieldForPrefix(String prefix) {
        Schema.SObjectType objType = Questionnaire__c.sObjectType;
        Schema.DescribeSObjectResult objDescription = objType.getDescribe();
        Map<String, Schema.SObjectField> objFields = objDescription.fields.getMap();
        Set<String> keys = new Set<String>();
        for (String k : objFields.keySet()) {
            keys.add(k.toLowerCase());
        }

        if (keys.contains(prefix + '_number__c')) {
            return prefix + '_number__c';
        }
        return prefix + '_phone__c';
    }
    private Map<String, Object> getObjectMapForPosting(Lawsuit__c lawsuit, String columnPrefix, Questionnaire__c ques, FacilityColumn col) {

        Map<String, Object> postMap = new Map<String, Object>();

        TTTSettings__c tttSettings = TTTSettings__c.getInstance();
        if (Test.isRunningTest() || IsSandbox) {
            postMap.put('api_key', tttSettings.Sandbox_API_Key__c);
            postMap.put('api_secret', tttSettings.Sandbox_API_Secret__c);
            postMap.put('callback_url', 'https://dev-mm-intakes.cs22.force.com/services/apexrest/medicalrecords');
        } else {
            postMap.put('api_key', tttSettings.Production_API_Key__c);
            postMap.put('api_secret', tttSettings.Production_API_Secret__c);
            postMap.put('callback_url', 'https://mm-intakes.secure.force.com/services/apexrest/medicalrecords');
        }
        //description documents_requested facility_type facility_fax record_request_type base64_hipaa_form

        // Todo: DOB Change
        Date dob = Lawsuit.AccountId__r.Date_of_Birth_mm__c;
        if (dob == null) {
            dob = Lawsuit.AccountId__r.PersonBirthdate;
        }

        postMap.put('client_dob', Lawsuit.AccountId__r.Date_of_Birth_mm__c);
        postMap.put('client_first_name', Lawsuit.AccountId__r.FirstName);
        postMap.put('client_last_name', Lawsuit.AccountId__r.LastName);
        postMap.put('client_social_security_number', Lawsuit.AccountId__r.Social_Security_Number__c);

        postMap.put('facility_name', ques.get(columnPrefix + '_name__c'));
        postMap.put('facility_address', ques.get(columnPrefix + '_address__c'));
        postMap.put('facility_city', ques.get(columnPrefix + '_city__c'));
        postMap.put('facility_phone', ques.get(getPhoneFieldForPrefix(columnPrefix)));
        postMap.put('facility_state', ques.get(columnPrefix + '_state__c'));
        postMap.put('facility_zip', ques.get(columnPrefix + '_zip__c'));

        postMap.put('is_rush', col.IsRush);
        postMap.put('documents_requested', String.join(col.DocumentsRequested, ', '));

        if (Lawsuit.Intake__r.What_Was_The_Date_Of_The_Incident__c != null) {
            postMap.put('record_date_range_start', Lawsuit.Intake__r.What_Was_The_Date_Of_The_Incident__c.AddMonths(-6));
            Date future = Lawsuit.Intake__r.What_Was_The_Date_Of_The_Incident__c.AddMonths(6);
            if (future > Date.today()) {
                future = Date.today();
            }
            postMap.put('record_date_range_end', future);
        }

        Blob hipaa = FindHipaaForm(lawsuit);
        if (hipaa != null) {
            postMap.put('base64_hipaa_form', EncodingUtil.base64Encode(hipaa));
        }
        postMap.put('external_id', Lawsuit.ID);

        return postMap;
    }

    private Blob FindHipaaForm(Lawsuit__c lawsuit) {
        ID intakeId = lawsuit.Intake__r.Id;
        List<Attachment> ats = [SELECT body FROM Attachment WHERE ParentId = :intakeId AND Name = 'HIPAA.pdf'];
        if (ats.size() == 0) {
            return null;
        }

        return ats[0].body;
    }
    private List<FacilityColumn> getFacilityColumnNamesThatContainData(List<String> prefixes, Questionnaire__c questionnaire) {
        List<FacilityColumn> colsWithData = new List<FacilityColumn>();

        for(String p : prefixes) {
            String val = (String)questionnaire.get(p + '_name__c');
            if (val != null && val != '') {
                FacilityColumn fc = new FacilityColumn(p, val);
                colsWithData.add(fc);
            }
        }
        return colsWithData;
    }

    /*
     * Returns a list of column names for all the facilities available
     */
    private List<String> getFacilityFieldPrefixes() {
        Schema.SObjectType objType = Questionnaire__c.sObjectType;
        Schema.DescribeSObjectResult objDescription = objType.getDescribe();
        Map<String, Schema.SObjectField> objFields = objDescription.fields.getMap();

        List<String> facilityFieldPrefixes = new List<String>();
        List<String> excludedPrefixes = new List<String>();
        excludedPrefixes.add('probate_attorney');
        excludedPrefixes.add('secondary_contact');

        for (String key : objFields.keySet()) {
            key = key.toLowerCase();
            Boolean isExclude = false;
            if (key.endsWith('_address__c')) {
                for(String exclude : excludedPrefixes) {
                    if (key.startsWith(exclude)) {
                        isExclude = true;
                        break;
                    }
                }

                if (!isExclude) {
                    facilityFieldPrefixes.add(key.substring(0, key.indexOf('_address__c')));
                }

            }
        }

        return facilityFieldPrefixes;
    }

    // Todo: Remove along with DateOfBirthCoverageTest.cls once real tests are created.
    @TestVisible
    private static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}