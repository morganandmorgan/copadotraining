public class mmlitifylrn_Referrals
    extends mmlib_SObjectDomain
    implements mmlitifylrn_IReferrals
{
    public static mmlitifylrn_IReferrals newInstance(List<litify_pm__Referral__c> records)
    {
        return (mmlitifylrn_IReferrals) mm_Application.Domain.newInstance(records);
    }

    public static mmlitifylrn_IReferrals newInstance(Set<Id> recordIds)
    {
        return (mmlitifylrn_IReferrals) mm_Application.Domain.newInstance(recordIds);
    }

    public mmlitifylrn_Referrals(List<litify_pm__Referral__c> records)
    {
        super(records);
    }

    public override void onAfterInsert()
    {
        postThirdPartyReferralToLitifyInQueueableMode();
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
    {
        postThirdPartyReferralToLitifyInQueueableMode();
    }

    public void postThirdPartyReferralToLitify()
    {
        list<litify_pm__Referral__c> qualifiedRecords = new mmlitifylrn_PostTPRToLitifyCriteria().setRecordsToEvaluate( records ).run();

        new mmlitifylrn_PostTPRToLitifyAction().setRecordsToActOn( qualifiedRecords ).run();
    }

    public void postThirdPartyReferralToLitifyInQueueableMode()
    {
        list<litify_pm__Referral__c> qualifiedRecords = new mmlitifylrn_PostTPRToLitifyCriteria().setRecordsToEvaluate( records ).run();

        mmlitifylrn_PostTPRToLitifyAction action = new mmlitifylrn_PostTPRToLitifyAction();

        action.setActionToRunInQueue(true);

        action.setRecordsToActOn( qualifiedRecords ).run();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmlitifylrn_Referrals(sObjectList);
        }
    }
}