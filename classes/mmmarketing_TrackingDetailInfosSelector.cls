/**
 *  mmmarketing_TrackingDetailInfosSelector
 */
public with sharing class mmmarketing_TrackingDetailInfosSelector
    extends mmlib_SObjectSelector
    implements mmmarketing_ITrackingDetailInfosSelector
{
    public static mmmarketing_ITrackingDetailInfosSelector newInstance()
    {
        return (mmmarketing_ITrackingDetailInfosSelector) mm_Application.Selector.newInstance(Marketing_Tracking_Detail_Information__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Marketing_Tracking_Detail_Information__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Marketing_Tracking_Detail_Information__c.Marketing_Tracking_Information__c,
                Marketing_Tracking_Detail_Information__c.Detail_Tracking_Parameter__c,
                Marketing_Tracking_Detail_Information__c.Detail_Tracking_Value__c
            };
    }

    public List<Marketing_Tracking_Detail_Information__c> selectById(Set<Id> idSet)
    {
        return (List<Marketing_Tracking_Detail_Information__c>) selectSObjectsById(idSet);
    }

}