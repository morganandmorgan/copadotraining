/**
 *  mmcommon_TrainingUserUtils
 *
 *  This script will create a group of training users typically for the UAT environment but it can be done in other sandbox environments as well.
 *  The script's defaults can be configured using the TrainingUserSetupRelated__c custom setting.
 *
 *  @usage (new mmcommon_TrainingUserUtils()).presetUserNumberAt(1).generateTrainingUsers(20).persist();
 *  @usage (new mmcommon_TrainingUserUtils()).presetUserNumberAt(1).generateTrainingUsers(20).setEmailAddressTo('adiaz@forthepeople.com').persist();
 *  @usage (new mmcommon_TrainingUserUtils()).presetUserNumberAt(1).generateTrainingUsers(20).setEmailAddressTo('adiaz@forthepeople.com').setProfile('M&M CCC - IP Restricted').setRole('Foobar').setUserPrefix('CCC').persist();
 */
public with sharing class mmcommon_TrainingUserUtils
{
    private String trainingUserProfileName = 'M&M CCC - IP Restricted';
    private profile trainingUserProfile;
    private String sandboxUsernameSuffix = null;

    private integer userNumber = 1;
    private integer numberOfUsersToGenerate = 1;
    private List<User> newUsers = new list<User>();
    private String emailAddressForTrainingUsers = null;
    private String trainingUserRoleName = null;
    private UserRole trainingUserRole = null;
    private String userPrefix = 'CCC';

    private void generateUser()
    {
        User newUser = new User();

        newUser.LastName = '-' + this.userNumber;
        newUser.FirstName = userPrefix + '-training';

        if ( test.isRunningTest() )
        {
            newUser.Username = 'testinguser' + this.userNumber + '@forthepeople.com' + (String.isBlank(sandboxUsernameSuffix) ? '' : '.'+sandboxUsernameSuffix);
            newUser.CommunityNickname = 'testinguser' + this.userNumber;
        }
        else
        {
            newUser.Username = userPrefix + '-training-' + this.userNumber + '@forthepeople.com' + (String.isBlank(sandboxUsernameSuffix) ? '' : '.'+sandboxUsernameSuffix);
            newUser.CommunityNickname = newUser.firstName + newUser.lastName;
        }

        newUser.Email = this.emailAddressForTrainingUsers;
        newUser.Alias = userPrefix + newUser.lastName;
        newUser.IsActive = true;
        newUser.TimeZoneSidKey = 'America/New_York';

        newUser.UserRoleId = trainingUserRole != null ? trainingUserRole.id : null;

        newUser.LocaleSidKey = 'en_US';
        newUser.ReceivesAdminInfoEmails = false;
        newUser.ReceivesInfoEmails = false;
        newUser.ProfileId = trainingUserProfile.id;

        newUser.LanguageLocaleKey = 'en_US';
        newUser.EmailEncodingKey = 'ISO-8859-1';

        system.debug( 'generated user ' + newUser.Username);

        this.userNumber++;

        this.newUsers.add( newUser );
    }

    public mmcommon_TrainingUserUtils()
    {
        TrainingUserSetupRelated__c trainingUserSetupCustomSetting = TrainingUserSetupRelated__c.getInstance();

        if ( trainingUserSetupCustomSetting != null )
        {
            if  ( String.isNotBlank( trainingUserSetupCustomSetting.EmailAddress__c ) )
            {
                this.emailAddressForTrainingUsers = trainingUserSetupCustomSetting.EmailAddress__c;
            }

            if ( trainingUserSetupCustomSetting.NumberOfTrainingUsersToGenerate__c != null )
            {
                this.numberOfUsersToGenerate = integer.valueOf( trainingUserSetupCustomSetting.NumberOfTrainingUsersToGenerate__c );
            }
        }

        // Next determine what the suffix is for this sandbox
        List<User> adminUserList = [select id, username
                                      from user
                                     where ProfileId in (select id
                                                           from Profile
                                                          where UserLicense.name = 'Salesforce'
                                                            and name like '%system administrator%')];

        map<String, Integer> countsBySuffixesMap = new map<String, Integer>();
        string suffix = null;

        // collect all suffixes
        for (User aSysAdmin : adminUserList)
        {
            //system.debug( aSysAdmin.username );
            if ( aSysAdmin.username.contains('forthepeople.com') )
            {
                suffix = aSysAdmin.username.substringAfterLast('forthepeople.com').substringAfterLast('.');
                //system.debug( suffix );
                if ( ! countsBySuffixesMap.containsKey( suffix ) )
                {
                    countsBySuffixesMap.put( suffix, 1 );
                }
                else
                {
                    countsBySuffixesMap.put( suffix, countsBySuffixesMap.get( suffix ) + 1 );
                }
            }
        }

        suffix = null;

        // find the suffix with the most counts
        for ( String aSuffix : countsBySuffixesMap.keyset() )
        {
            if ( suffix == null
                || countsBySuffixesMap.get( suffix ) < countsBySuffixesMap.get( aSuffix )
                )
            {
                suffix = aSuffix;
            }
        }

        // and we have found the real suffix for this sandbox
        sandboxUsernameSuffix = suffix;

    }

    public mmcommon_TrainingUserUtils presetUserNumberAt( final integer userNumberPreset)
    {
        this.userNumber = userNumberPreset;

        return this;
    }

    public mmcommon_TrainingUserUtils persist()
    {
        // Do not persist if executing in a production org
        if (mmlib_Utils.org.isSandbox && ! Test.isRunningTest())
        {
            insert newUsers;

            for (User newUser : newUsers)
            {
                System.resetPassword( newUser.id, true );
            }
        }

        return this;
    }

    public mmcommon_TrainingUserUtils setEmailAddressTo(final String email)
    {
        if ( string.isNotBlank( email ) )
        {
            this.emailAddressForTrainingUsers = email;
        }

        return this;
    }

    public mmcommon_TrainingUserUtils setTotalNumberOfUsersToGenerateTo(final integer totalNumberOfUsers)
    {
        if ( totalNumberOfUsers != null )
        {
            this.numberOfUsersToGenerate = totalNumberOfUsers;
        }

        return this;
    }

    public mmcommon_TrainingUserUtils setProfile(final string profileName)
    {
        if ( string.isNotBlank( profileName ) )
        {
            this.trainingUserProfileName = profileName;
        }

        return this;
    }

    public mmcommon_TrainingUserUtils setRole(final string roleName)
    {
        if ( string.isNotBlank( roleName ) )
        {
            this.trainingUserRoleName = roleName;
        }

        return this;
    }

    public mmcommon_TrainingUserUtils setUserPrefix(final string userPrefix)
    {
        if ( string.isNotBlank( userPrefix ) )
        {
            this.userPrefix = userPrefix;
        }

        return this;
    }

    public mmcommon_TrainingUserUtils generateTrainingUsers()
    {
        if ( String.isBlank(this.emailAddressForTrainingUsers) )
        {
            throw new TrainingUserUtilsException('The email address must be specified with the .setEmailAddressTo(String) method and prior to the call to the generateTrainingUsers(Integer) method.');
        }

        if ( this.numberOfUsersToGenerate == null )
        {
            throw new TrainingUserUtilsException('The number of users to generate must be specified with the .setTotalNumberOfUsersToGenerateTo(Integer) method.');
        }

        try
        {
            trainingUserProfile = [select id, name, UserType, UserLicense.name
                                       from profile
                                      where UserType = 'Standard'
                                        and Name = :trainingUserProfileName];

        }
        catch ( Exception e )
        {
            // let it go
            system.debug( e );
        }

        if ( trainingUserProfile == null )
        {
            throw new TrainingUserUtilsException( 'Could not find \'' + trainingUserProfileName + '\' profile.');
        }

        if ( string.isNotBlank( trainingUserRoleName ) )
        {
            try
            {
                trainingUserRole = [select Id, Name, ParentRoleId, DeveloperName
                                      from UserRole
                                     where Name = :trainingUserRoleName];
            }
            catch ( Exception e )
            {
                // let it go
                system.debug( e );
            }

            if ( trainingUserRole == null )
            {
                throw new TrainingUserUtilsException( 'Could not find \'' + trainingUserRoleName + '\' role.');
            }
        }

        for ( integer i = 1; i <= this.numberOfUsersToGenerate; i++)
        {
            generateUser();
        }

        return this;
    }

    public class TrainingUserUtilsException extends Exception { }
}