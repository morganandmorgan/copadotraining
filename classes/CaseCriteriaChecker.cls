public class CaseCriteriaChecker
{
    public class InvalidConditionTypeException extends Exception {}

    public class InvalidConditionException extends Exception {}

    @InvocableMethod (label='Run Case Criteria'
                    description='Runs the applicable case criteria against a single case id and returns the first resulting error message, if any')
    public static List<ID> CheckIds(List<Id> ids)
    {
        List<ID> returnVal = new List<ID>();

        if (ids == null || ids.size() == 0)
        {
            return returnVal;
        }

        List<Case_Qualify_Criteria__c> allRules = [SELECT Id, Priority__c, TD_Reason__c, UR_Reason__c
                                                        , Reason_Type__c, Criteria__c
                                                     FROM Case_Qualify_Criteria__c
                                                    WHERE Active__c = TRUE
                                                    ORDER BY Reason_Type__c, Priority__c ASC NULLS LAST];

        SObjectQualifier qualifier = new SObjectQualifier( Intake__c.sObjectType );

        for (Id intakeId : ids)
        {
            Intake__c intake = (Intake__c) qualifier.getFullObjectById( intakeId );

            for (Case_Qualify_Criteria__c cqc : allRules)
            {
                Boolean matches = qualifier.criteriaMatches(cqc.Criteria__c, intake);

                if (matches)
                {
                    returnVal.add(cqc.Id);
                    break; // TODO eventually we might want to check multiple rules
                }
            }

            break; // TODO eventually we might want to check multiple ids
        }

        return returnVal;
    }
}