@isTest
private class mmsms_ScheduledSmsLogicTest
{
	@isTest
	static void DuplicateInNewMessageList()
	{
		SMS_Integration__c settings = new SMS_Integration__c();
		settings.Integation_enabled__c = true;
		settings.Integration_Enabled_In_Sandbox__c = true;
		settings.Duplicate_Time_Limit__c = 60;
		insert settings;

		tdc_tsw__Message_Template__c templ = new tdc_tsw__Message_Template__c();
		insert templ;

		tdc_tsw__Scheduled_Sms__c newSms1 = new tdc_tsw__Scheduled_Sms__c();
 		newSms1.tdc_tsw__Phone_Api__c = '8947891984';
		newSms1.tdc_tsw__SMS_Template__c = templ.Id;

		tdc_tsw__Scheduled_Sms__c newSms2 = new tdc_tsw__Scheduled_Sms__c();
 		newSms2.tdc_tsw__Phone_Api__c = newSms1.tdc_tsw__Phone_Api__c;
		newSms2.tdc_tsw__SMS_Template__c = newSms1.tdc_tsw__SMS_Template__c;

		List<tdc_tsw__Scheduled_Sms__c> smsList = new List<tdc_tsw__Scheduled_Sms__c> {newSms1, newSms2};

		insert smsList;

		Set<Id> idSet = new Map<Id, SObject>(smsList).keyset();

		smsList = [select Id, Name, tdc_tsw__Phone_Api__c, tdc_tsw__SMS_Template__c from tdc_tsw__Scheduled_Sms__c where Id in :idSet];

		newSms1 = smsList.get(0);
		newSms2 = smsList.get(1);

		System.assert(
			(newSms1.tdc_tsw__SMS_Template__c == null || newSms2.tdc_tsw__SMS_Template__c == null) &&
			newSms1.tdc_tsw__SMS_Template__c != newSms2.tdc_tsw__SMS_Template__c,
			'One of the Templates should be set to null.');
	}

	@isTest
	static void DuplicateExistsTest()
	{
		tdc_tsw__Scheduled_Sms__c exsistingSms = new tdc_tsw__Scheduled_Sms__c();
		exsistingSms.tdc_tsw__Phone_Api__c = '5482365794';
		exsistingSms.tdc_tsw__SMS_Template__c = fflib_IDGenerator.generate(tdc_tsw__Message_Template__c.SObjectType);

		MockSmsSelector selector = new MockSmsSelector(new List<tdc_tsw__Scheduled_Sms__c> {exsistingSms});

		tdc_tsw__Scheduled_Sms__c newSms = new tdc_tsw__Scheduled_Sms__c();
		newSms.tdc_tsw__Phone_Api__c = exsistingSms.tdc_tsw__Phone_Api__c;
		newSms.tdc_tsw__SMS_Template__c = exsistingSms.tdc_tsw__SMS_Template__c;

		mmsms_ScheduledSmsLogic logic = new mmsms_ScheduledSmsLogic();
		logic.mockSelector = selector;

		logic.removeRecentlySentDuplicates(new List<tdc_tsw__Scheduled_Sms__c> {newSms});

		System.assert(newSms.tdc_tsw__SMS_Template__c == null, 'Template should be set to null.');
	}

	@isTest
	static void NoDuplicateTest_PhoneNumberNotSame()
	{
		tdc_tsw__Scheduled_Sms__c exsistingSms = new tdc_tsw__Scheduled_Sms__c();
		exsistingSms.tdc_tsw__Phone_Api__c = '5482365794';
		exsistingSms.tdc_tsw__SMS_Template__c = fflib_IDGenerator.generate(tdc_tsw__Message_Template__c.SObjectType);

		MockSmsSelector selector = new MockSmsSelector(new List<tdc_tsw__Scheduled_Sms__c> {exsistingSms});

		tdc_tsw__Scheduled_Sms__c newSms = new tdc_tsw__Scheduled_Sms__c();
		newSms.tdc_tsw__Phone_Api__c = '8885551000';
		newSms.tdc_tsw__SMS_Template__c = exsistingSms.tdc_tsw__SMS_Template__c;

		mmsms_ScheduledSmsLogic logic = new mmsms_ScheduledSmsLogic();
		logic.mockSelector = selector;

		logic.removeRecentlySentDuplicates(new List<tdc_tsw__Scheduled_Sms__c> {newSms});

		System.assert(newSms.tdc_tsw__SMS_Template__c != null, 'Template should NOT be set to null.');
	}

	@isTest
	static void NoDuplicateTest_TemplateNotSame()
	{
		tdc_tsw__Scheduled_Sms__c exsistingSms = new tdc_tsw__Scheduled_Sms__c();
		exsistingSms.tdc_tsw__Phone_Api__c = '5482365794';
		exsistingSms.tdc_tsw__SMS_Template__c = fflib_IDGenerator.generate(tdc_tsw__Message_Template__c.SObjectType);

		MockSmsSelector selector = new MockSmsSelector(new List<tdc_tsw__Scheduled_Sms__c> {exsistingSms});

		tdc_tsw__Scheduled_Sms__c newSms = new tdc_tsw__Scheduled_Sms__c();
		newSms.tdc_tsw__Phone_Api__c = exsistingSms.tdc_tsw__Phone_Api__c;
		newSms.tdc_tsw__SMS_Template__c = fflib_IDGenerator.generate(tdc_tsw__Message_Template__c.SObjectType);

		mmsms_ScheduledSmsLogic logic = new mmsms_ScheduledSmsLogic();
		logic.mockSelector = selector;

		logic.removeRecentlySentDuplicates(new List<tdc_tsw__Scheduled_Sms__c> {newSms});

		System.assert(newSms.tdc_tsw__SMS_Template__c != null, 'Template should NOT be set to null.');
	}

	@isTest(seeAllData=true)
	static void SettingsTest()
	{
		mmsms_IntegrationSettings.getIsEnabled();
		mmsms_IntegrationSettings.getIsEnabledInSandbox();
		mmsms_IntegrationSettings.getTimeLimit();
	}

	private class MockSmsSelector
		implements mmsms_IScheduledSmsSelector
	{
		public List<tdc_tsw__Scheduled_Sms__c> mockScheduledSmsList = null;

		public MockSmsSelector(List<tdc_tsw__Scheduled_Sms__c> scheduledSmsList)
		{
			this.mockScheduledSmsList = scheduledSmsList;
		}

		public List<tdc_tsw__Scheduled_Sms__c> getRecentRecords_AllRows(Integer timePeriodInMinutes)
		{
			return this.mockScheduledSmsList;
		}

		public List<SObject> selectSObjectsById(Set<Id> idSet) { return null; }
		public SObjectType sObjectType() { return Intake__c.SObjectType; }
		public String selectSObjectsByIdQuery() { return null; }
	}
}