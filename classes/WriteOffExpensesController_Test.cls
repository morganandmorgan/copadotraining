@IsTest
global class WriteOffExpensesController_Test 
{
    public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;
    public static c2g__codaCompany__c company;

    @testSetup
    static void setup()
    {
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        update company;
        List<c2g__codaBankAccount__c> bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        c2g__codaBankAccount__c costBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Cost');
        bankAccountList.add(costBankAccount);
        c2g__codaBankAccount__c trustBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Trust');
        bankAccountList.add(trustBankAccount);
        insert bankAccountList;

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;
        
        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan; 

        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        // Insert new Expense Type
        List<litify_pm__Expense_Type__c> expTypes = new List<litify_pm__Expense_Type__c>();

        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        expenseType.General_Ledger_Account__c = testGLA.Id;
        expTypes.add(expenseType);

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Cable';
        expenseType2.ExternalID__c = 'CABLE';
        expenseType2.General_Ledger_Account__c = testGLA.Id;
        expTypes.add(expenseType2);

        litify_pm__Expense_Type__c expenseType3 = new litify_pm__Expense_Type__c(
            Name = 'Soft Cost Recovered');
        expenseType3.CostType__c = 'SoftCost';
        expenseType3.ExternalID__c = 'SOFTCOSTRECOVERED';
        expenseType3.Exclude_From_Allocation__c = true;
        expenseType3.General_Ledger_Account__c = testGLA.Id;

        expTypes.add(expenseType3);

        litify_pm__Expense_Type__c expenseType4 = new litify_pm__Expense_Type__c(Name = 'Hard Cost Recovered');
        expenseType4.CostType__c = 'HardCost';
        expenseType4.ExternalID__c = 'HARDCOSTRECOVERED';
        expenseType4.Exclude_From_Allocation__c = true;
        expenseType4.General_Ledger_Account__c = testGLA.Id;

        expTypes.add(expenseType4);

        INSERT expTypes;

        c2g__codaJournal__c newJournal = new c2g__codaJournal__c
        (
            c2g__OwnerCompany__c = company.Id,
            c2g__Type__c = 'Manual Journal',
            c2g__Reference__c = 'TEST CLASS JOURNAL',
            c2g__DeriveCurrency__c = true,
            c2g__DerivePeriod__c = true,
            c2g__JournalDate__c = Date.today()
        );
        INSERT newJournal;

        // Insert new Expense
        litify_pm__Expense__c expense = new litify_pm__Expense__c();
        expense.litify_pm__ExpenseType2__c = expenseType.Id;
        expense.litify_pm__Matter__c = matter.Id;
        expense.litify_pm__Amount__c = 10.50;
        expense.litify_pm__Note__c = 'My note';
        expense.litify_pm__Date__c = Date.today();
        expense.PayableTo__c = client.Id;
        expense.Journal__c = newJournal.Id;
        expense.Payable_Invoice_Created__c = true;

        INSERT expense;

        litify_pm__Expense__c expense2 = new litify_pm__Expense__c();
        expense2.litify_pm__ExpenseType2__c = expenseType2.Id;
        expense2.litify_pm__Matter__c = matter.Id;
        expense2.litify_pm__Amount__c = 44.50;
        expense2.litify_pm__Note__c = 'My note';
        expense2.litify_pm__Date__c = Date.today();
        expense2.PayableTo__c = client.Id;
        expense2.Journal__c = newJournal.Id;

        INSERT expense2;

        Write_Off_Settings__c settings = new Write_Off_Settings__c(
            Write_Off_Hard_Costs_Expense_Type_ID__c = expenseType4.Id,
            Write_Off_Soft_Costs_Expense_Type_ID__c = expenseType3.Id);

        INSERT settings;
    }

    @IsTest
    public static void testHappyPath() 
    {
        litify_pm__Matter__c matter = [Select Id FROM litify_pm__Matter__c LIMIT 1];
        ApexPages.currentPage().getParameters().put('matterId', matter.Id);    
        WriteOffExpensesController controller = new WriteOffExpensesController();

        PageReference pr = controller.cancel();
        System.assertEquals('/lightning/r/litify_pm__Matter__c/' + matter.Id + '/view', pr.getUrl());

        pr = controller.save();

        System.assertEquals(true, ApexPages.hasMessages(ApexPages.severity.ERROR)); // Should have errored out due to no expenses selected.

        System.debug(LoggingLevel.WARN, '***** testHappyPath: beforeSelectAll - allIsSelected : '+controller.allIsSelected);
        System.debug(LoggingLevel.WARN, '***** testHappyPath: beforeSelectAll - expenseList : '+controller.expenseList);

        controller.allIsSelected = true;

        controller.selectAll();

        litify_pm__Expense__c expense = controller.expenseList[1].expense;
        Decimal amountWO = controller.expenseList[1].amountWrittenOff;


        System.debug(LoggingLevel.WARN, '***** testHappyPath: afterSelectAll - expenseList : '+controller.expenseList);

        pr = controller.save();

        System.assertEquals(0, controller.expenseList.size()); // Should be no expenses left.

        System.assert(controller.writtenOffHard + controller.writtenOffSoft >  0, 'Amounts do not Match!');

        System.assert((controller.totalHardCosts + controller.totalSoftCosts) > 0);
    }
}