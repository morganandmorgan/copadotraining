public with sharing class IntakeRepNavigator_QueueListController {
  public List<IntakeRepNavigatorWidgetQueue__c> Queues { get; set; }
	public IntakeRepNavigator_QueueListController() {
		Queues = [SELECT Id, Name FROM IntakeRepNavigatorWidgetQueue__c];
	}
}