/**
 *  mmlib_CronTriggersSelector
 */
public class mmlib_CronTriggersSelector
    extends mmlib_SObjectSelector
    implements mmlib_ICronTriggersSelector
{
    public static mmlib_ICronTriggersSelector newInstance()
    {
        return (mmlib_ICronTriggersSelector) mm_Application.Selector.newInstance(CronTrigger.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return CronTrigger.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {};
    }

    public List<CronTrigger> selectById(Set<Id> idSet)
    {
        return (List<CronTrigger>) selectSObjectsById(idSet);
    }

    public List<CronTrigger> selectScheduledApexByName( Set<String> nameSet )
    {
        Boolean assertCRUD = false;
        Boolean enforceFLS = false;
        Boolean includeSelectorFields = true;

        return Database.query( newQueryFactory(assertCRUD, enforceFLS, includeSelectorFields).setCondition('CronJobDetail.JobType = \'7\''
                                                                                                            + ' and CronJobDetail.Name in :nameSet')
                                                .toSOQL() );
    }

    public List<CronTrigger> selectWaitingScheduledApexByName( Set<String> nameSet )
    {
        Boolean assertCRUD = false;
        Boolean enforceFLS = false;
        Boolean includeSelectorFields = true;

        return Database.query( newQueryFactory(assertCRUD, enforceFLS, includeSelectorFields).setCondition('CronJobDetail.JobType = \'7\''
                                                                                                         + ' and State = \'WAITING\''
                                                                                                         + ' and CronJobDetail.Name in :nameSet')
                                                .toSOQL() );
    }
}