/*
	Abstract class for implementing a trigger actions and enforces development standards.
	Extends the basic TriggerAction class to provide more advanced controls.
	Provides more granular (record level) tracking of if a trigger has run (on a record).
	Relies on Record Ids to be populated so it cannot be used in Before trigger contexts (exception will be thrown).
*/
public abstract class AdvancedTriggerAction extends TriggerAction {

	/* Map of records touched by this class; to be indexed by class name */
	private static Map<String,Set<Id>> touchedMap = new Map<String,Set<Id>>();

	/* Constructors */
	// Basic constructor for all trigger contexts
	protected AdvancedTriggerAction(){
		super();
		// If this is the one invalid context then throw a custom exception
		if (this.context == TriggerAction.TriggerContext.BEFORE_INSERT){
			throw new TriggerAction.InvalidContextException(this.context);
		}
	}
	// Advanced constructor for After Insert and Delete trigger contexts
	protected AdvancedTriggerAction(Map<Id,SObject> triggerMap, TriggerContext ctx) {
		super(triggerMap,ctx);
	}
	// Advanced constructor for Update trigger contexts
	protected AdvancedTriggerAction(Map<Id,SObject> triggerNewMap,Map<Id,SObject> triggerOldMap, TriggerContext ctx){
		super(triggerNewMap,triggerOldMap,ctx);
	}

	/* Setters to mark if a record has been touched.*/
	// Method to mark that a batch of records has been touched providing a set of record Ids
	public void markTouched(Set<Id> recordIds){
		// Add all record Ids
		this.getTouched().addAll(recordIds);
	}
	// Method to mark that a batch of records has been touched providing a list of whole records
	public void markTouched(List<SObject> records){
		// Get touched set
		Set<Id> touched = this.getTouched();
		// Iterate batch and add record Ids
		for (SObject r : records){
			touched.add(r.Id);
		}
	}
	// Method to mark that a batch of records has been touched providing a map of whole records;
	// Assumes map will be constructed with Id keys matching their respective SObjects
	public void markTouched(Map<Id,SObject> records){
		// Add record Ids (from map key set)
		this.getTouched().addAll(records.keySet());
	}
	// Method to mark that a single record has been touched providing the SObject Id
	public void markTouched(Id recordId){
		// Add record Id
		this.getTouched().add(recordId);
	}
	// Method to mark that a single record has been touched providing the SObject
	public void markTouched(SObject record){
		// Add record Id
		this.getTouched().add(record.Id);
	}
	// Helper to get (or init) touched set
	private Set<id> getTouched(){
		// Get touched set
		Set<Id> touched = AdvancedTriggerAction.touchedMap.get(this.getClassName());
		// If not defined yet, initalize and set into global mapping
		if (touched == null){
			touched = new Set<Id>();
			AdvancedTriggerAction.touchedMap.put(this.getClassName(),touched);
		}
		return touched;
	}

	/* Inspectors to check if a record has been touched */
	// Method for checking if a record has been touched using the SObject Id
	public Boolean hasTouched(Id recordId){
		// Get touched set
		Set<Id> touched = AdvancedTriggerAction.touchedMap.get(this.getClassName());
		// Return if touched defined and contains this Id
		return (touched!=null) && touched.contains(recordId);
	}
	// Method for checking if a record has been touched using the whole SObject (then getting the Id from it)
	public Boolean hasTouched(SObject record){
		return this.hasTouched(record.Id);
	}
	// Method for checking if a set of records has been touched;
	// Returns a map index by SObject Ids of booleans indicating if touched
	public Map<Id,Boolean> hasTouched(Set<Id> recordIds){
		// Get touched set
		Set<Id> touched = AdvancedTriggerAction.touchedMap.get(this.getClassName());
		// Initialize return
		Map<Id,Boolean> rtn = new Map<Id,Boolean>();
		// Iterate the set and get the status from the touch set
		for(Id recordId : recordIds){
			rtn.put(recordId, (touched!=null) && touched.contains(recordId));
		}
		// Return compiled result
		return rtn;
	}
	// Method for checking if a set of records has been touched using the whole SObject (then getting the Id from it);
	// Returns a map index by SObject Ids of booleans indicating if touched
	public Map<Id,Boolean> hasTouched(List<SObject> records){
		// Get touched set
		Set<Id> touched = AdvancedTriggerAction.touchedMap.get(this.getClassName());
		// Initialize return
		Map<Id,Boolean> rtn = new Map<Id,Boolean>();
		// Iterate the set and get the status from the touch set
		for(SObject record : records){
			rtn.put(record.Id, (touched!=null) && touched.contains(record.Id));
		}
		// Return compiled result
		return rtn;
	}

}