/**
 *  mmintake_PageReferenceUtils
 *
 *  The class is a common util place to get urls in a consistent manner.
 */
public class mmintake_PageReferenceUtils
{
    private mmintake_PageReferenceUtils() {  }

    public static PageReference getIntakeFlow( Intake__c intake )
    {
        PageReference pr = Page.IntakeFlow;

        pr.getParameters().putAll(new Map<String, String>{
                    'PersonAccountID' => intake.Client__c,
                    'IntakeID' => intake.Id,
                    'CaseType' => intake.Case_Type__c,
                    'PersonGender' => intake.Gender__c,
                    'Litigation' => intake.Litigation__c
                });

        return pr;
    }

    /**
     * Method to get to the ConfirmCallerID page
     *
     * @param intakeId - the id of the current intake
     * @param callStartTime - the DateTime value of start of the call to the call center.
     * @return PageReference object to the ConfirmCallerId VF page with appropriate parameters.
     * @see mmintake_ConfirmCallerIdController
     */
    public static PageReference getConfirmCallerID( id intakeId, datetime callStartTime )
    {
        PageReference pr = Page.ConfirmCallerId;

        pr.getParameters().putAll(new Map<String, String>{
                    'id' => intakeId,
                    'cst' => callStartTime.formatGMT('yyyyMMddHHmmss')
                });

        return pr;
    }

}