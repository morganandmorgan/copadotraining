public interface mmcommon_IBusinessHoursSelector extends mmlib_ISObjectSelector
{
    List<BusinessHours> selectDefault();
}