@isTest
private class mm_ApplicationTest
{
    @isTest
    private static void testAllApplicationFactoryEntriesAreValid()
    {
        // given
        list<ApplicationFactory__mdt> factoryConfigRecords = [select Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName
                                                                   , Type__c, SObjectType__c, Interface__c, SortOrder__c, ClassToInject__c
                                                                from ApplicationFactory__mdt
                                                               order by Type__c, SortOrder__c, DeveloperName];

        Map<String, Schema.SObjectType> schemaGlobalDescribe = Schema.getGlobalDescribe();

        Schema.SObjectType currentSObjectType = null;
        Type currentType = null;

        integer numberOfFactoryConfigRecordsOfTypeDomain = 0;
        integer numberOfFactoryConfigRecordsOfTypeSelector = 0;
        integer numberOfFactoryConfigRecordsOfTypeService = 0;
        integer numberOfFactoryConfigRecordsOfTypeUOW = 0;

        // when
        //  test out the UnitOfWork
        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        for ( ApplicationFactory__mdt factoryConfigRecord : factoryConfigRecords )
        {
            // if the SObjectType is present, check that it exists
            if ( String.isNotBlank( factoryConfigRecord.SObjectType__c ) )
            {
                System.assert( schemaGlobalDescribe.containsKey( factoryConfigRecord.SObjectType__c.toLowerCase() ), 'SObject entry of \''+ factoryConfigRecord.SObjectType__c + '\' was not found in schema for Application Factory '
                                                                                                                        + factoryConfigRecord.Type__c+' record ' + factoryConfigRecord.DeveloperName);
            }

            if ( String.isNotBlank( factoryConfigRecord.Interface__c ) )
            {
                currentType = type.forName( factoryConfigRecord.Interface__c );

                System.assert( currentType != null , 'Interface entry of \''+ factoryConfigRecord.Interface__c + '\' was not found in code for Application Factory '
                                                                                                                        + factoryConfigRecord.Type__c+' record ' + factoryConfigRecord.DeveloperName);
            }

            if ( String.isNotBlank( factoryConfigRecord.ClassToInject__c ) )
            {
                currentType = type.forName( factoryConfigRecord.ClassToInject__c );

                System.assert( currentType != null , 'Apex Class To Inject entry of \''+ factoryConfigRecord.ClassToInject__c + '\' was not found in code for Application Factory '
                                                                                                                        + factoryConfigRecord.Type__c+' record ' + factoryConfigRecord.DeveloperName);
            }



            if ( factoryConfigRecord.Type__c == 'Domain' )
            {
                numberOfFactoryConfigRecordsOfTypeDomain++;
            }
            else if ( factoryConfigRecord.Type__c == 'Selector' )
            {
                numberOfFactoryConfigRecordsOfTypeSelector++;
            }
            else if ( factoryConfigRecord.Type__c == 'Service' )
            {
                numberOfFactoryConfigRecordsOfTypeService++;
            }
            else if ( factoryConfigRecord.Type__c == 'UnitOfWork' )
            {
                numberOfFactoryConfigRecordsOfTypeUOW++;
            }
        }

        // then

        system.assertEquals( numberOfFactoryConfigRecordsOfTypeDomain, mm_Application.domainFactoryTypeMap.keyset().size() );

        system.assertEquals( numberOfFactoryConfigRecordsOfTypeSelector, mm_Application.selectorFactoryTypeMap.keyset().size() );

        system.assertEquals( numberOfFactoryConfigRecordsOfTypeService, mm_Application.serviceFactoryTypeMap.keyset().size() );

        system.assertEquals( numberOfFactoryConfigRecordsOfTypeUOW, mm_Application.unitOfWorkFactorySObjectTypeList.size() );

    }
}