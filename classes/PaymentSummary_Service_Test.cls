/*============================================================================
Name            : PaymentSummary_Service_Test
Author          : CLD
Created Date    : June 2019
Description     : Test class for Payment Summary Service - note it must use seeAllData = true due to limitations in FFA API
=============================================================================*/
@isTest (seeAllData=true)
public class PaymentSummary_Service_Test {

	@isTest static void testCancelPayment(){

		Id companyId = null;
		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if(companies != null){
            companyId = companies.get(0).id;
        }

		if(companyId != null){

			c2g__codaPaymentLineItem__c pmtDetailLine = [SELECT Id, 
					c2g__TransactionLineItem__r.c2g__Account__c, 
					c2g__Payment__c,
					c2g__PaymentSummary__c,
					c2g__PaymentSummary__r.c2g__Payment__c 
				FROM c2g__codaPaymentLineItem__c 
				WHERE c2g__PaymentSummary__r.c2g__Payment__r.c2g__ownerCompany__c = :companyId
				AND c2g__Transaction__r.c2g__PayableInvoice__r.Finance_Request__c != null
				AND c2g__PaymentSummary__r.c2g__Payment__r.c2g__Status__c = 'Matched'
				AND c2g__PaymentSummary__r.c2g__Payment__r.c2g__Period__r.c2g__Closed__c = false
				LIMIT 1];
			Id pmtId = pmtDetailLine.c2g__Payment__c;
			List<Id> cancelPaymentAccountIds = new List<Id>{pmtDetailLine.c2g__TransactionLineItem__r.c2g__Account__c};

			c2g__codaPaymentAccountLineItem__c pmtAccountLine = [SELECT Id,Full_Payment_Void__c FROM c2g__codaPaymentAccountLineItem__c WHERE Id = :pmtDetailLine.c2g__PaymentSummary__c LIMIT 1];
			pmtAccountLine.Full_Payment_Void__c = true;
			update pmtAccountLine;

			c2g__codaPayment__c pmt = [SELECT Id, c2g__Period__c 
				FROM c2g__codaPayment__c 
				WHERE Id = :pmtId 
				LIMIT 1];

			c2g.PaymentsPlusService.CancelPaymentCriteria cancelPaymentCriteria = new c2g.PaymentsPlusService.CancelPaymentCriteria();
			cancelPaymentCriteria.CancelReason = 'Cancel reason';
			cancelPaymentCriteria.PaymentRefundDate = System.today();
			cancelPaymentCriteria.PaymentRefundPeriod = pmt.c2g__Period__c;
			cancelPaymentCriteria.UndoMatchDate = System.today();
			cancelPaymentCriteria.UndoMatchPeriod = pmt.c2g__Period__c;
			
			Id batchProcessId = c2g.PaymentsPlusService.cancelPaymentAsync(pmt.Id, cancelPaymentCriteria, cancelPaymentAccountIds);	
		}
	}
}