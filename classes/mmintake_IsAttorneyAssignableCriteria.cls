public class mmintake_IsAttorneyAssignableCriteria
    implements mmlib_ICriteria
{
    private static Boolean shouldBypassUpdateCriteria = false;

    static
    {
        List<Approval_Assignments_Trigger_Setting__mdt> mdtSetting = [SELECT Id
                                                                        FROM Approval_Assignments_Trigger_Setting__mdt
                                                                       WHERE DeveloperName = 'Default'
                                                                         AND Bypass_Update_Criteria__c = true];
        shouldBypassUpdateCriteria = ! mdtSetting.isEmpty();
    }

    private list<Intake__c> records = new list<Intake__c>();

    public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
    {
        if (records != null
            && Intake__c.SObjectType == records.getSobjectType()
            )
        {
            this.records.addAll( (list<Intake__c>) records);
        }

        return this;
    }

    public list<SObject> run()
    {
        list<Intake__c> qualifiedRecords = new list<Intake__c>();

        if ( shouldBypassUpdateCriteria
            && (Trigger.isExecuting && Trigger.isUpdate)
            )
        {
            qualifiedRecords.addAll(this.records);
        }
        else
        {
            Map<Id, Intake__c> intakeOldMap = null;

            if ( Trigger.isExecuting && Trigger.isUpdate )
            {
                intakeOldMap = (Map<Id, Intake__c>)trigger.oldMap;
            }


            for (Intake__c record : this.records)
            {
                if (String.isNotBlank(record.Case_Type__c)
                    && String.isNotBlank(record.Venue__c)
                    && String.isNotBlank(record.Handling_Firm__c))
                {

                    if (intakeOldMap != null)
                    {
                        Intake__c oldrecord = intakeOldMap.get(record.Id);

                        if (record.Case_Type__c == oldrecord.Case_Type__c
                            && record.Venue__c == oldrecord.Venue__c
                            && record.Handling_Firm__c == oldrecord.Handling_Firm__c
                            && record.Type_of_Dispute__c == oldrecord.Type_of_Dispute__c
                            && record.Where_did_the_injury_occur__c == oldrecord.Where_did_the_injury_occur__c
                            && record.Did_the_accident_involve_a_truck_or_bus__c == oldrecord.Did_the_accident_involve_a_truck_or_bus__c) {

                            continue;
                        }
                    }

                    qualifiedRecords.add(record);
                }
            }
        }

        return qualifiedRecords;
    }

}