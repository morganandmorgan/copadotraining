@isTest
private class mmlib_EventsTest 
{
    @isTest
    private static void basicTestToActivateCodeCoverage() 
    {
        mmlib_Event__e platformEventbus = new mmlib_Event__e();

        platformEventbus.EventName__c = 'bluefish';
        platformEventbus.Payload__c = json.serialize( new Set<Id>{ fflib_IDGenerator.generate(Account.SObjectType ) } );
        platformEventbus.RelatedSObject__c = Account.getSObjectType().getDescribe().getName();

        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish( platformEventbus );
        Test.stopTest();

        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());

    }
}