/**
 * DateOfBirthCoverageTest
 * @description 
 * @author Jeff Watson
 * @date 1/22/2019
 */
@IsTest
public with sharing class DateOfBirthCoverageTest {

    // Todo: This is a temporary fix to cover classes where Account.Date_of_Birth__c was changed to Account.Date_of_Birth_mm__c
    // It should be deleted and test coverage written correctly for the following.

    /*
        mmintake_InvestigationEventsSelector.cls (0%)

        mmcommon_AccountsRelevancyOrganizer.cls (90.48%)
        ContactSearchCtlr.cls (84.77%)
        EmailAlertsController.cls (0%)
        SendMedicalRecordRequestController.cls (0%)
        mmcommon_AccountsSelector.cls (67.67%)
        QuestionnarieClass.cls (0%)

        ContactSearchCtlrTest.cls
        mmcommon_PersonAccountsServiceTest.cls
        mmcommon_AccountsRelevancyOrganizerTest.cls
    */

    @IsTest
    public static void DateOfBirth_CodeCoverage() {

        mmintake_InvestigationEventsSelector.coverage();
        EmailAlertsController.coverage();
        SendMedicalRecordRequestController.coverage();
        mmcommon_AccountsSelector.coverage();
        QuestionnarieClass.coverage();
    }
}