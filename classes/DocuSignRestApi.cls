@RestResource(urlMapping='/docusign/*')
global without sharing class DocuSignRestApi {

    public class RequestData {
        public List<String> docusign_template_ids {get;set;}
        public Map<String,String> recipient {get;set;}
        public Map<String,String> tokens {get;set;}
        public String originUrl {get;set;}
        public String returnUrl {get;set;}
    }
    
    Public class UpdateData {
        public String envelopeId {get;set;}
        public String intakeId {get;set;}
    }

/*    
    {
        "docusign_template_ids": ["1", "2"],
        "recipient": {
            "email": "bob@test.com",
            "name": "Bob Tester"
        },
        "tokens": {
            "first_name": "Bob",
            "last_name": "Tester"
        },
		"originUrl": "https://",
		"returnUrl": "https://",
    }
*/

    @HttpPost
    global static void createAndSign() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String requestBody = request.requestBody.toString();
        
		String clientUserId = mmlib_Utils.generateGuid();

        //deserialize the input into our data structure
        DocuSignRestApi.RequestData rd = (DocuSignRestApi.RequestData)Json.deserialize(requestBody, DocuSignRestApi.RequestData.class);

        DocuSignApi dsApi = new DocuSignApi();

        String envelopeId = dsApi.createCompositeEnvelope(rd.docusign_template_ids, clientUserId, rd.recipient, rd.tokens);

        String signingUrl = dsApi.getSigningUrl(clientUserId, rd.recipient, envelopeId, rd.originUrl, rd.returnUrl);
        
        Map<String,String> responseMap = new Map<String,String>();
        responseMap.put('signingUrl', signingUrl);
        responseMap.put('envelopeId', envelopeId);

        response.responseBody = Blob.valueOf( Json.serialize(responseMap) );

    } //createAndSign


    @HttpPut
    global static void updateDocusignStatus() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;

        String requestBody = request.requestBody.toString();
        
		String clientUserId = mmlib_Utils.generateGuid();

        //deserialize the input into our data structure
        DocuSignRestApi.UpdateData rd = (DocuSignRestApi.UpdateData)Json.deserialize(requestBody, DocuSignRestApi.UpdateData.class);

		List<dsfs__DocuSign_Status__c> dsStatus = [SELECT id, dsfs__DocuSign_Envelope_ID__c, intake__c FROM dsfs__DocuSign_Status__c WHERE dsfs__DocuSign_Envelope_ID__c = :rd.envelopeId];

        Map<String,String> responseMap = new Map<String,String>();
        responseMap.put('envelopeId', rd.envelopeId);
        responseMap.put('intakeId',rd.intakeId);
        
        if (dsStatus.size() == 1) {
            dsStatus[0].intake__c = rd.intakeId;
            update dsStatus;
            responseMap.put('status','success');
            responseMap.put('docuSignStatusId', dsStatus[0].id);
        } else {
            responseMap.put('status','failed');
            responseMap.put('message','DocuSign_Status__c record not found.');
			response.statusCode = 500;
        }
        
		response.responseBody = Blob.valueOf( Json.serialize(responseMap) );

    } //updateDocusignStatus

} //class