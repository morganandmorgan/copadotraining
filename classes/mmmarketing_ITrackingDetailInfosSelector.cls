public interface mmmarketing_ITrackingDetailInfosSelector extends mmlib_ISObjectSelector
{
    List<Marketing_Tracking_Detail_Information__c> selectById(Set<Id> idSet);
}