@isTest()
public Class TestAccountHighlightPanelExtension{
    public static testmethod void AccountHighlightPanelMeth(){
        
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account'];
        
        Account acc = new Account(LastName = 'Test Account', RecordTypeId = acctRcdType.Id);
        insert acc;
        
        Intake__c intakeRec = new Intake__c(Client__c = acc.Id);
        insert intakeRec;
        
        Comments__c intakeCom = new Comments__c(Intake__c = intakeRec.Id,Comments__c = 'Test Comment');
        Insert intakeCom;
        
        ApexPages.StandardController ctlr = new ApexPages.StandardController(intakeRec);
        AccountHighlightPanelExtension accHighCtlr = new AccountHighlightPanelExtension(ctlr);
        
        Questionnaire__c quest  = new Questionnaire__c(Client__c = acc.Id,Intake__c = intakeRec.Id); 
        Insert quest;
        
        Comments__c questCom = new Comments__c(Intake__c = intakeRec.Id,Questionnaire__c = quest.Id,Comments__c = 'Test Comment');
        Insert questCom;
        
        ApexPages.StandardController ctlr1 = new ApexPages.StandardController(quest);
        AccountHighlightPanelExtension questAccHighCtlr = new AccountHighlightPanelExtension(ctlr1);
    }
}