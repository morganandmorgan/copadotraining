/**
 *  mmcommon_IContactsSelector
 */
public interface mmcommon_IContactsSelector extends mmlib_ISObjectSelector
{
    List<Contact> selectById(Set<Id> idSet);
    List<Contact> selectByEmail(Set<String> emailSet);
    List<Contact> selectAllMorganAndMorganAttorneys();
}