@isTest
public class MailingRestApiTest {

    @isTest
    public static void successTest() {

        Mailing__c m = new Mailing__c();
        insert m;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        String jsUpdate = ''
        +'{'
        +'    "Client": "FORTHEPEOPLE",'
        +'    "FileId": "12345",'
        +'    "MailingID": "' + m.id + '",'
        +'    "MailDate": "04/09/2020",'
        +'    "TrackingNo": "123tacking456",'
        +'    "SharedSecret": "z0ZwaMtA9gkB3NlFMC3JRH/JBuLsmOaIgkMT9V6I2/8"'
        +'}';
            
        req.httpMethod = 'POST'; 
        req.requestBody = Blob.valueOf(jsUpdate);

        RestContext.request = req;
        RestContext.response = res;
        
        MailingRestApi.updateStatus();

    } //successTest

    @isTest
    public static void badSecretTest() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        String jsUpdate = ''
        +'{'
        +'    "Client": "FORTHEPEOPLE",'
        +'    "FileId": "12345",'
        +'    "MailingID": "bleh",'
        +'    "MailDate": "04/09/2020",'
        +'    "TrackingNo": "123tacking456",'
        +'    "SharedSecret": "hacker"'
        +'}';
            
        req.httpMethod = 'POST'; 
        req.requestBody = Blob.valueOf(jsUpdate);

        RestContext.request = req;
        RestContext.response = res;
        
        MailingRestApi.updateStatus();

    } //badSecretTest

    @isTest
    public static void noRecordTest() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        String jsUpdate = ''
        +'{'
        +'    "Client": "FORTHEPEOPLE",'
        +'    "FileId": "12345",'
        +'    "MailingID": "aEr8A00000008WhSAI",'
        +'    "MailDate": "04/09/2020",'
        +'    "TrackingNo": "123tacking456",'
        +'    "SharedSecret": "z0ZwaMtA9gkB3NlFMC3JRH/JBuLsmOaIgkMT9V6I2/8"'
        +'}';
            
        req.httpMethod = 'POST'; 
        req.requestBody = Blob.valueOf(jsUpdate);

        RestContext.request = req;
        RestContext.response = res;
        
        MailingRestApi.updateStatus();

    } //noRecordTest


} //class