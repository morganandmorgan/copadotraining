/*============================================================================/
* PaymentCollection_Service
* @description Service class for Payment Collections
* @author Brian Krynitsky
* @date Jun 2019
=============================================================================*/

public class PaymentCollection_Service {

	// Ctors
	public PaymentCollection_Service() {

	}

	// Methods
	/*-----------------------------------------------------------------------------
    Defaults the payment fields such as GLA / period on the payment collection
    -----------------------------------------------------------------------------*/
	public void defaultPaymentFields(List<Payment_Collection__c> newTrigger) {
		
		if((newTrigger!=null)&&(newTrigger.size()>0)){
		
			Company_Selector companySelector = new Company_Selector();
			
			Set<Date> periodStartDateSet = new Set<Date>();
			Set<Id> companyIdSet = new Set<Id>();
			Set<Id> bankAccountIdSet = new Set<Id>();

			Map<Id, c2g__codaCompany__c> companyMap = new Map<Id, c2g__codaCompany__c>();
			Map<String, Id> currencyMap = new Map<String, Id>();
			Map<Id, c2g__codaBankAccount__c> bankAccountMap = new Map<Id, c2g__codaBankAccount__c>();
			Map<String, Id> periodMap = new Map<String, Id>();
			
			//collect info from the set of records:
			for(Payment_Collection__c pc : newTrigger){
				if(pc.Payment_Date__c != null){
					periodStartDateSet.add(pc.Payment_Date__c.toStartOfMonth());
				}
				companyIdSet.add(pc.Company__c);
				bankAccountIdSet.add(pc.Bank_Account__c);
			}

			//query the active FFA companies
			for(c2g__codaCompany__c company : companySelector.selectById(companyIdSet)){
				companyMap.put(company.Id, company);
			}
			//query the accounting currencies
			for(c2g__codaAccountingCurrency__c acctCurr : [SELECT c2g__OwnerCompany__c, Id FROM c2g__codaAccountingCurrency__c WHERE c2g__OwnerCompany__c in : companyIdSet]){
				currencyMap.put(acctCurr.c2g__OwnerCompany__c, acctCurr.Id);
			}
			//query the accounting periods
			for(c2g__codaPeriod__c period : [SELECT Id, c2g__StartDate__c, c2g__OwnerCompany__c FROM c2g__codaPeriod__c WHERE c2g__OwnerCompany__c in : companyIdSet AND c2g__StartDate__c in :periodStartDateSet AND c2g__PeriodNumber__c not in ('100','000','101')]){
				String key = period.c2g__OwnerCompany__c + '|' + period.c2g__StartDate__c.format();
				periodMap.put(key, period.Id);
			}
			//query the accounting currencies
			for(c2g__codaBankAccount__c bankAccount : [SELECT Check_Template_Payment_Collection__c, Id FROM c2g__codaBankAccount__c WHERE c2g__OwnerCompany__c in : companyIdSet]){
				bankAccountMap.put(bankAccount.Id, bankAccount);
			}

			//set the values in the fields
			for(Payment_Collection__c pc : newTrigger){
				c2g__codaCompany__c company = companyMap.get(pc.Company__c);
				String periodKey = pc.Company__c + '|' + pc.Payment_Date__c.toStartOfMonth().format();
				String currencyKey = String.valueOf(pc.Company__c);

				pc.Settlement_Discount_GLA__c = pc.Settlement_Discount_GLA__c == null && company != null ? company.c2g__CustomerSettlementDiscount__c : pc.Settlement_Discount_GLA__c;
				pc.Currency_Write_Off_GLA__c = pc.Currency_Write_Off_GLA__c == null && company != null ? company.c2g__CustomerSettlementDiscount__c : pc.Currency_Write_Off_GLA__c;
				pc.Period__c = pc.Period__c == null && periodMap.containsKey(periodKey) ? periodMap.get(periodKey) : pc.Period__c;
				pc.Payment_Currency__c = pc.Payment_Currency__c == null && currencyMap.containsKey(currencyKey) ? currencyMap.get(currencyKey) : pc.Payment_Currency__c;
				pc.Check_Template__c = pc.Check_Template__c == null && bankAccountMap.containsKey(pc.Bank_Account__c) ? bankAccountMap.get(pc.Bank_Account__c).Check_Template_Payment_Collection__c : pc.Check_Template__c;
			}

		}

	}

}