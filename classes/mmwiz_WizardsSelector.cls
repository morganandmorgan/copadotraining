public with sharing class mmwiz_WizardsSelector
    extends mmlib_SObjectSelector
    implements mmwiz_IWizardsSelector
{
    public static mmwiz_IWizardsSelector newInstance()
    {
        return (mmwiz_IWizardsSelector) mm_Application.Selector.newInstance(Wizard__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Wizard__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            Wizard__mdt.Description__c,
            Wizard__mdt.DisplayText__c,
            Wizard__mdt.IsActive__c,
            Wizard__mdt.IsDeleted__c,
            Wizard__mdt.SerializedData__c,
            Wizard__mdt.Type__c
        };
    }

    public List<Wizard__mdt> selectByMasterLabel(set<String> labelSet)
    {
        if (labelSet == null || labelSet.isEmpty())
        {
            return new List<Wizard__mdt>();
        }

        return
            Database.query
            (
                newQueryFactory()
                    .setCondition
                    (
                        Wizard__mdt.MasterLabel + ' in :labelSet'
                    )
                    .toSOQL()
            );
    }

    public List<Wizard__mdt> selectByDeveloperName(set<String> developerNamesSet)
    {
        List<Wizard__mdt> records = new list<Wizard__mdt>();

        if ( developerNamesSet != null && ! developerNamesSet.isEmpty() )
        {
            records.addAll( (List<Wizard__mdt>) Database.query( newQueryFactory().setCondition( Wizard__mdt.DeveloperName + ' in :developerNamesSet').toSOQL() ));
        }

        return records;
    }

    public List<Wizard__mdt> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }

    public List<Wizard__mdt> selectAllBeingActive()
    {
        return
            Database.query(
                newQueryFactory()
                    .setCondition(Wizard__mdt.IsActive__c + ' = true')
                    .toSOQL());
    }
}