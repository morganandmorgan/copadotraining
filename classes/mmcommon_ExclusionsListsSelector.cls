public class mmcommon_ExclusionsListsSelector
    extends mmlib_SObjectSelector
    implements mmcommon_IExclusionsListsSelector
{
    public mmcommon_ExclusionsListsSelector() { }

    public static mmcommon_IExclusionsListsSelector newInstance()
    {
        return (mmcommon_IExclusionsListsSelector) mm_Application.Selector.newInstance(Exclusions_List__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Exclusions_List__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
                Exclusions_List__mdt.App__c,
                Exclusions_List__mdt.Target_Object__c,
                Exclusions_List__mdt.Target_Object_Fields__c
        };
    }

    // Selects and returns fields excluded per the Exclusions_List__mdt custom metadata for the given app and sobject.
    // The list of fields is stored in Exclusions_List__mdt.Target_Object_Fields__c as a comma delimited list.
    public List<Exclusions_List__mdt> selectByAppAndTargetObject(String app, String sObj)
    {
        List<Exclusions_List__mdt> records = new list<Exclusions_List__mdt>();

        if (String.isNotEmpty(app) && String.isNotEmpty(sObj) )
        {
            records.addAll( (List<Exclusions_List__mdt>) Database.query(newQueryFactory()
                    .setCondition(Exclusions_List__mdt.App__c + ' = :app')
                    .setCondition(Exclusions_List__mdt.Target_Object__c + ' = :sObj')
                    .toSOQL() ));
        }

        return records;
    }

    public class mmcommon_ExclusionsListsSelectorException extends Exception { }
}