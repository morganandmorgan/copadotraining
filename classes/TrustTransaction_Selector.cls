/*============================================================================/
* TrustTransaction_Selector
* @description Selector for Trust Transactions
* @author Brian Krynitsky
* @date 4/8/2019
=============================================================================*/
public class TrustTransaction_Selector extends fflib_SObjectSelector {
	
	public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Trust_Transaction__c.Id,
            Trust_Transaction__c.Matter__c,
            Trust_Transaction__c.Amount__c,
            Trust_Transaction__c.Bank_Account__c,
            Trust_Transaction__c.FFA_Transaction_Line__c,
            Trust_Transaction__c.Type__c
        };
    }

    // Constructor
    public TrustTransaction_Selector() {
    }

    //required implemenation from fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Trust_Transaction__c.SObjectType;
    }

    // Methods
    public List<Trust_Transaction__c> selectByMatterId(Set<Id> matterIds) {
        fflib_QueryFactory query = newQueryFactory();
        
        //where conditions:
        query.setCondition('Matter__c in :matterIds');

        return (List<Trust_Transaction__c>) Database.query(query.toSOQL());
    }
}