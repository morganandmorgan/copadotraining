@istest
private class mmcti_UniversalScreenPopControllerTest {

    private static testmethod void mmcti_UniversalScreenPopController_Redir_Test()
    {
        // Arrange + Act + Assert
        Test.startTest();
        new mmcti_UniversalScreenPopController().redir();
        Test.stopTest();
    }

    private static testMethod void mmcti_UniversalScreenPopController_OutboundHandler_Test()
    {
        // Arrange + Act + Assert
        Test.startTest();
        new mmcti_UniversalScreenPopController().outboundHandler();
        Test.stopTest();
    }
}