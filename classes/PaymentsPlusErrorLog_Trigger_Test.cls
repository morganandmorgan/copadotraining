@isTest
private with sharing class PaymentsPlusErrorLog_Trigger_Test {

	private static c2g__codaCompany__c company=null;
    private static c2g__codaBankAccount__c operatingBankAccount=null;
	private static Payment_Collection__c paymentCollection = null;        

	static void setupData(){
    	/*--------------------------------------------------------------------
        FFA data setup
        --------------------------------------------------------------------*/
		company = TestDataFactory_FFA.company;

        operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
        update operatingBankAccount;

		// Map<c2g__codaPurchaseInvoice__c, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> payableInvoices_ExpenseMap = TestDataFactory_FFA.createPayableInvoices_Expense(2);
		// TestDataFactory_FFA.postPayableInvoices(payableInvoices_ExpenseMap.keySet());

		// paymentCollection = new Payment_Collection__c(
		// 	Company__c = company.Id,
		// 	Bank_Account__c = operatingBankAccount.Id,
		// 	Payment_Method__c = 'Check',
		// 	Payment_Date__c = Date.today()
		// );
		// insert paymentCollection;

	}

    @isTest 
    static void testInsertPaymentsPlusErrorLogRecord(){

        System.debug('Creating setup test fixture data...');

        //setupData();

        c2g__codaPayment__c payment = null;
        c2g__PaymentsPlusErrorLog__c record = null;

        System.debug('Creating payment object...');

        {

            //Note:  Looks like cnt param doesn't do anything
            payment=TestDataFactory_FFA.createPayment(1, false);
            
            {
                Database.SaveResult result = null;

                System.debug('Inserting payment object...');               
                
                result=Database.insert(payment);

                System.assert(result.isSuccess());

            }

            System.assert(payment!=null);
            System.assert(payment.Id!=null);

        }

        System.debug('Retrieving transaction line item...');

        c2g__codaTransactionLineItem__c transactionLineItem = null; 

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = 
        [
            select
            Id
            ,Name
            from c2g__codaTransactionLineItem__c 
            order by CreatedDate desc 
            limit 1
        ];

        if(transactionLineItems.size()>0){
            transactionLineItem=transactionLineItems[0];
            System.assert(transactionLineItem!=null);
            System.assert(transactionLineItem.Id!=null);            
        }else{
            System.debug('No transaction line item.');
        }
        
        System.debug('Creating payment error log record...');        

        record = new c2g__PaymentsPlusErrorLog__c();

        record.c2g__PaymentProposalNumber__c = payment.Id;   //Master-detail lookup to parent pament
        if(transactionLineItem!=null){
            record.c2g__TransactionLineItem__c = transactionLineItem.Id;
        }

        record.c2g__LogType__c = 'Error';
        record.c2g__PaymentStage__c = 'Add to Proposal';

        System.debug('Inserting payment error log record...');

        Test.startTest(); 

        insert record;

        Test.stopTest();

        System.debug('Inserted.');
        
    }

}