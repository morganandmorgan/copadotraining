// Usage: /services/apexrest/dice/checkout/a003D000001dPZDD?
@RestResource(urlMapping='/dice/checkout/*')
global class mmdice_CheckoutRestApi
{
    public static final RestRequest req = RestContext.request;
    public static final RestResponse resp = RestContext.response;

    @HttpPost
    global static PostResponse post(PostRequestBody data)
    {
        String queueId = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        String intakeId = data.intake_id;

        // First check the request is OK.
        if(String.isEmpty(intakeId) || !mmlib_Utils.isValidSalesforceId(intakeId) || String.isEmpty(queueId) || !mmlib_Utils.isValidSalesforceId(queueId))
        {
            resp.statusCode = 400; // Bad Request
            return null;
        }

        try
        {
            PostResponse postResponse = new PostResponse();
            postResponse.success = true;

            postResponse.success  = new DICEService().checkout(Id.valueOf(intakeId), Id.valueOf(queueId));

            resp.statusCode = 200;
            return postResponse;
        }
        catch (Exception e)
        {
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            resp.statusCode = 500; // Internal Error
            return null;
        }
    }

    global class PostRequestBody
    {
        public PostRequestBody() { }

        public String intake_id { get; set; }
    }

    global class PostResponse
    {
        public PostResponse()
        {
            this.success = false;
        }

        public Boolean success { get; set; }
    }
}