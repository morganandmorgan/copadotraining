/**
 *  Domain criteria used to find all Lawsuit__c records where the Lawsuit_Active__c field has changed
 */
public class mmlawsuit_HasIsActiveChangedCriteria
    implements mmlib_ICriteriaWithExistingRecords
{
    private list<Lawsuit__c> records = new list<Lawsuit__c>();
    private Map<Id, Lawsuit__c> existingRecords = new Map<Id, Lawsuit__c>();

    public mmlib_ICriteria setRecordsToEvaluate(List<SObject> records)
    {
        this.records.clear();
        this.records.addAll( (list<Lawsuit__c>)records );

        return this;
    }

    public mmlib_ICriteria setExistingRecords( Map<Id, SObject> existingRecords )
    {
        this.existingRecords.clear();
        this.existingRecords.putAll( (Map<Id, Lawsuit__c>)existingRecords );

        return this;
    }

    public List<SObject> run()
    {
        list<Lawsuit__c> qualifiedRecords = new list<Lawsuit__c>();

        // Loop through the Lawsuit__c records.
        for ( Lawsuit__c record : this.records )
        {
            // Note: we are only interested in Lawsuit__c records tha have changed -- not new Lawsuit__c records
            if ( this.existingRecords.containsKey( record.id )
                && record.Lawsuit_Active__c != this.existingRecords.get( record.id ).Lawsuit_Active__c)
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}