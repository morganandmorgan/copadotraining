@isTest
private class AccountUpdateIncidentTest {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        List<Incident__c> incidents = new List<Incident__c>();
        incidents.add(new Incident__c());
        incidents.add(new Incident__c());
        incidents.add(new Incident__c());
        toInsert.addAll((List<SObject>) incidents);

        List<Account> clients = new List<Account>();
        clients.add(TestUtil.createPersonAccount('Person', 'A'));
        clients.add(TestUtil.createPersonAccount('Person', 'B'));
        clients.add(TestUtil.createPersonAccount('Person', 'C'));
        toInsert.addAll((List<SObject>) clients);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Account client : clients) {
            for (Incident__c incident : incidents) {
                intakes.add(createIntake(client, incident));
            }
        }
        toInsert.addAll((List<SObject>) intakes);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Intake__c createIntake(Account client, Incident__c incident) {
        Intake__c intake = new Intake__c();
        if (client != null) {
            intake.Client__c = client.Id;
        }
        if (incident != null) {
            intake.Incident__c = incident.Id;
        }
        return intake;
    }

    private static List<Account> getClients() {
        return [
            SELECT
                Id,
                LastName
            FROM
                Account
            WHERE
                IsPersonAccount = true
        ];
    }

    private static List<Incident__c> getIncidents() {
        return [
            SELECT
                Id,
                Incident_Label__c
            FROM
                Incident__c
        ];
    }

//	@isTest
//    private static void beforeDelete_IncidentLabelUpdated() {
//        List<Account> clients = getClients();
//
//        List<Incident__c> incidents = getIncidents();
//        for (Incident__c incident : incidents) {
//            System.assertNotEquals(null, incident.Incident_Label__c);
//        }
//
//        Test.startTest();
//        Database.delete(clients);
//        Test.stopTest();
//
//        List<Incident__c> requeriedIncidents = getIncidents();
//        System.assertEquals(incidents.size(), requeriedIncidents.size());
//        for (Incident__c incident : requeriedIncidents) {
//            System.assertEquals(null, incident.Incident_Label__c);
//        }
//    }

    @isTest
    private static void afterUpdate_NameChanged() {
        List<Account> clients = getClients();

        Map<Id, Incident__c> incidentMap = new Map<Id, Incident__c>(getIncidents());
        for (Incident__c incident : incidentMap.values()) {
            System.assertNotEquals(null, incident.Incident_Label__c);
        }

        for (Account client : clients) {
            client.LastName += 'Modified';
        }

        Test.startTest();
        Database.update(clients);
        Test.stopTest();

        List<Incident__c> requeriedIncidents = getIncidents();
        System.assertEquals(incidentMap.size(), requeriedIncidents.size());
        for (Incident__c incident : requeriedIncidents) {
            Incident__c originalIncident = incidentMap.get(incident.Id);

            System.assertNotEquals(null, incident.Incident_Label__c);
            System.assertNotEquals(originalIncident.Incident_Label__c, incident.Incident_Label__c);
        }
    }
}