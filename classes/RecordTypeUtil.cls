/**
 * Old version of Global Recordtype util class for looking up recordtype ids
 * Now replaced by mmlib_RecordTypeUtils and will be eventually purged.
 * For now, it has been converted to a "pass through" until we have more time
 * to replace references.
 *
 * @see mmlib_RecordTypeUtils
 * @deprecated
 *
 */
global class RecordTypeUtil
{
    public static Id getRecordTypeIDByDevName(String sObjectType, String recordTypeDeveloperName)
    {
        return mmlib_RecordTypeUtils.getRecordTypeIDByDevName(sObjectType, recordTypeDeveloperName);
    }

}