public class mmmarketing_FinancialPeriodFactory
{
    public static final string NO_KEYWORD = '<no keyword>';

    private static mmmarketing_FinancialPeriodFactory marketingFinancialPeriodFactoryInstance = null;

    private mmmarketing_FinancialPeriodFactory()
    {
    }

    public static mmmarketing_FinancialPeriodFactory getInstance()
    {
        if (marketingFinancialPeriodFactoryInstance == null)
        {
            marketingFinancialPeriodFactoryInstance = new mmmarketing_FinancialPeriodFactory();
        }
        return marketingFinancialPeriodFactoryInstance;
    }

    private Marketing_Financial_Period__c newRecord = null;

    /*
     *  Accessor to the newRecord instance.  Allowing the methods to
     *  access the variable via this method allows a lazy instantition
     *  of the member variable should there actually be a parameter
     *  that can be used by the Marketing_Financial_Period__c record.
     */
    private Marketing_Financial_Period__c getNewRecordInstance()
    {
        if ( this.newRecord == null )
        {
            this.newRecord = new Marketing_Financial_Period__c();
        }

        return this.newRecord;
    }

    public Marketing_Financial_Period__c generateFromMTIWithNoTerm( Marketing_Tracking_Info__c mti )
    {
        this.newRecord = null;

        getNewRecordInstance().Period_Start_Date__c = mti.Tracking_Event_Timestamp__c.date();
        getNewRecordInstance().Campaign_Value__c = mti.Campaign_Value__c;

        if ( mti.Domain_Value__c.isNumeric() )
        {
            getNewRecordInstance().Campaign_AdWords_ID_Value__c = mti.Domain_Value__c;
        }
        else
        {
            getNewRecordInstance().Domain_Value__c = mti.Domain_Value__c;
        }

        getNewRecordInstance().Source_Value__c = mti.Source_Value__c;
        getNewRecordInstance().Medium_Value__c = mti.Medium_Value__c;
        getNewRecordInstance().Ad_Cost__c = 0;
        getNewRecordInstance().Clicks__c = 0;
        getNewRecordInstance().Keyword_Value__c = NO_KEYWORD;

        return this.newRecord;
    }


}