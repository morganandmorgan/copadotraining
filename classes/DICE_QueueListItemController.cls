public class DICE_QueueListItemController {
  public static final String App = 'DICE';

  public DICE_Queue__c q { get; set; }
  public List<DICE_Json.Query> Queries { get; set; }
  public List<DICE_Json.SortOrder> Sortorders { get; set; }
  public List<SelectOption> FieldList { get; set; }
  public List<SelectOption> OperatorOptions { get; set; }
  public List<SelectOption> SortOrderOptions { get; set; }
  public List<SelectOption> EarliestCallTimes { get; set; }
  public List<SelectOption> LatestCallTimes { get; set; }
  public List<SelectOption> FormulaOptions { get; set; }

  public DICE_QueueListItemController(ApexPages.StandardController stdController) {
    Queries = new List<DICE_Json.Query>();
    SortOrders = new List<DICE_Json.SortOrder>();

    DICE_Queue__c stdObj = (DICE_Queue__c)stdController.getRecord();
    if (stdObj == null || !String.isBlank(stdObj.ID)) {
      q = [SELECT Id, Name, EarliestCallTime__c, LatestCallTime__c, Minutes_Between_Calls__c, Query__c, Sorting__c FROM DICE_Queue__c WHERE Id = :stdObj.Id];
      Queries = DICE_Json.ParseQuery(q.Query__c);
      SortOrders = DICE_Json.ParseSortOrder(q.Sorting__c);
    } else {
      q = new DICE_Queue__c();
    }

    while(Queries.size() < 25) {
      Queries.add(new DICE_Json.Query());
    }
    while(SortOrders.size() < 10) {
      SortOrders.add(new DICE_Json.SortOrder());
    }

    PopulateEarliestCallTimes();
    PopulateLatestCallTimes();
    PopulateFieldList();
    PopulateOperatorOptions();
    PopulateSortOrderOptions();
    PopulateFormulaOptions();
  }

  public PageReference Save() {
    upsert q;
    return null;
  }

  public PageReference Remove() {
    delete q;
    return new PageReference('/apex/IntakeRepNavigatorQueueList');
  }

  public PageReference CloneObj() {
    DICE_Queue__c queue = new DICE_Queue__c(Name = q.Name + ' clone',  EarliestCallTime__c = q.EarliestCallTime__c, LatestCallTime__c = q.LatestCallTime__c, Query__c = q.Query__c, Sorting__c = q.Sorting__c);
    insert queue;
    return new PageReference('/apex/IntakeRepNavigator_QueueListItem?q=' + queue.id);
  }

  private void PopulateOperatorOptions() {
    OperatorOptions = new List<SelectOption>();
    OperatorOptions.add(new SelectOption('', ''));
    OperatorOptions.add(new SelectOption('<', 'Less/Earlier Than'));
    OperatorOptions.add(new SelectOption('<=', 'Less/Earlier Than or Equal'));
    OperatorOptions.add(new SelectOption('=', 'Equals'));
    OperatorOptions.add(new SelectOption('<>', 'Does Not Equal'));
    OperatorOptions.add(new SelectOption('>=', 'Greater/Later Than or Equal'));
    OperatorOptions.add(new SelectOption('>', 'Greater/Later Than'));
    OperatorOptions.add(new SelectOption('contains', 'Contains'));
    OperatorOptions.add(new SelectOption('not_contains', 'Does Not Contain'));
  }
  private void PopulateSortOrderOptions() {
    SortOrderOptions = new List<SelectOption>();
    SortOrderOptions.add(new SelectOption('', ''));
    SortOrderOptions.add(new SelectOption('DESC NULLS FIRST', 'descending (z-a, 100-1, today-yesterday)'));
    SortOrderOptions.add(new SelectOption('DESC NULLS LAST', 'desc nulls last (z-a, 100-1, today-yesterday)'));
    SortOrderOptions.add(new SelectOption('ASC NULLS FIRST', 'ascending (a-z, 1-100, yesterday-today)'));
    SortOrderOptions.add(new SelectOption('ASC NULLS LAST', 'asc nulls last (a-z, 1-100, yesterday-today)'));
  }

  private List<String> getExclusionsForObject(String targetSObj)
  {
    List<String> exclusionFields = new List<String>();
    List<Exclusions_List__mdt> exclusionsLists = mmcommon_ExclusionsListsSelector.newInstance().selectByAppAndTargetObject(App, targetSObj);

    for(Exclusions_List__mdt exclusionsList : exclusionsLists)
    {
      if(exclusionsList.Target_Object_Fields__c != null)
      {
        for (String field : exclusionsList.Target_Object_Fields__c.split(','))
        {
          exclusionFields.add(field.trim());
        }
      }
    }

    return exclusionFields;
  }

  private void PopulateFieldList() {
    FieldList = new List<SelectOption>();

    // start with intake columns
    Schema.DescribeSObjectResult description = Intake__c.sObjectType.getDescribe();
    Map<String, Schema.SObjectField> fields = description.fields.getMap();
    List<String> fieldExclusions = this.getExclusionsForObject(description.name);
    for (String fieldName : fields.keySet()) {
      if(!fieldExclusions.contains(fieldName))
      {
        FieldList.add(new SelectOption(fieldName, '[Intake].' + fields.get(fieldName).getDescribe().getLabel()));
      }
    }

    description = Account.sObjectType.getDescribe();
    fields = description.fields.getMap();
    fieldExclusions = this.getExclusionsForObject(description.name);
    for (String fieldName : fields.keySet())
    {
        if (! fieldName.startsWithIgnoreCase('ffr__')
            && ! fieldName.startsWithIgnoreCase('qbdialer__')
            && ! fieldName.startsWithIgnoreCase('ffaci__')
            && ! fieldName.startsWithIgnoreCase('ffbf__')
            && ! fieldName.startsWithIgnoreCase('fferpcore__')
            && ! fieldName.startsWithIgnoreCase('c2g__')
            && ! fieldName.startsWithIgnoreCase('ffbext__')
            && ! fieldName.startsWithIgnoreCase('ffxp__')
            && ! fieldName.startsWithIgnoreCase('ffc__')
            && ! fieldName.startsWithIgnoreCase('fferpcore__')
            && ! fieldName.startsWithIgnoreCase('ffcash__')
            && ! fieldName.startsWithIgnoreCase('ffqs__')
            && ! fieldName.startsWithIgnoreCase('ffirule__')
            && ! fieldName.startsWithIgnoreCase('ffct__')
            && ! fieldExclusions.contains(fieldName)
            )
        {
            System.debug(fieldName);
            FieldList.add(new SelectOption('Client__r.' + fieldName, '[Account].' + fields.get(fieldName).getDescribe().getLabel()));
        }
    }

    FieldList.sort();

    FieldList.add(0, new SelectOption('', ''));
  }

  private void PopulateEarliestCallTimes() {
    EarliestCallTimes = new List<SelectOption>();
    for(Integer i = 7; i <= 22; i++) {
      String label = '';
      if (i == 12) {
        label = '12pm';
      } else if (i > 12) {
        label = (i - 12) + 'pm';
      } else {
        label = i + 'am';
      }

      EarliestCallTimes.add(new SelectOption(i.format(), label));
    }
  }
  private void PopulateLatestCallTimes() {
    LatestCallTimes = EarliestCallTimes;
  }

  private void PopulateFormulaOptions() {
    FormulaOptions = new List<SelectOption>();
    FormulaOptions.add(new SelectOption('', ''));
    FormulaOptions.add(new SelectOption('hours_ago', 'Hours Ago'));
    FormulaOptions.add(new SelectOption('days_ago', 'Days Ago'));
    FormulaOptions.add(new SelectOption('months_ago', 'Months Ago'));
    FormulaOptions.add(new SelectOption('years_ago', 'Years Ago'));
  }
}