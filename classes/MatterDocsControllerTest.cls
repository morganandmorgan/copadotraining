/**
 * MatterDocsControllerTest
 * @description Test for MatterDocsController.
 * @author Jeff Watson
 * @date 1/28/2019
 */
@IsTest
public with sharing class MatterDocsControllerTest {

    private static Account account;
    private static litify_pm__Matter__c matter;

    static {
        account = TestUtil.createAccount('UT Account');
        insert account;

        matter = TestUtil.createMatter(account);
        insert matter;
    }

    @IsTest
    public static void MatterDocsController_Test() {

        MatterDocsController.getModel(matter.Id);
        MatterDocsController.Model model = new MatterDocsController.Model(new List<MM_Document__c>());
    }
}