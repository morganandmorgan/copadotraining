@isTest
private class LawsuitTriggerHandlerTest {

  @TestSetup
  private static void setup() {
    List<Account> testAccounts = new List<Account>{
        TestUtil.createPersonAccount('Client', 'One'),
        TestUtil.createPersonAccount('Injured', 'Party'),
        TestUtil.createPersonAccount('The', 'Caller')
      };

    Database.insert(testAccounts);

    Incident__c incident = new Incident__c();
    Database.insert(incident);

    Intake__c intake = createIntake(incident.Id, testAccounts.get(0).Id, testAccounts.get(1).Id, testAccounts.get(2).Id);
    Database.insert(intake);

    Lawsuit__c initialLawsuit = createLawsuit(intake.Id);
    Database.insert(initialLawsuit);
  }

  private static Intake__c createIntake(Id incidentId, Id clientId, Id injuredPartyId, Id callerId) {
    return new Intake__c(
      Caller__c = callerId,
      Client__c = clientId,
      Incident__c = incidentId,
      Injured_Party__c = injuredPartyId,
      Case_Type__c = 'RealEstate'
    );
  }

  private static Lawsuit__c createLawsuit(Id intakeId) {
      Lawsuit__c lawsuit = new Lawsuit__c();
      lawsuit.External_Id__c = mmlib_Utils.generateGuid();
      lawsuit.Intake__c = intakeId;
      return lawsuit;
  }

  private static Intake__c getIntake() {
    return [
      SELECT Id
      FROM Intake__c
    ];
  }

  @isTest
  private static void insertDuplicateLawsuit() {
        Intake__c intake = getIntake();
        Lawsuit__c newLawsuit = createLawsuit(intake.Id);

        try {
            Test.startTest();
            Database.insert(newLawsuit);
            Test.stopTest();

            System.assert(false, 'Expected exception to be thrown');
        }
        catch (DmlException e) {
            System.assertEquals(1, e.getNumDml());
            System.assertEquals('The operation would result in more than one Lawsuit associated to the Intake.', e.getDmlMessage(0));
        }
  }

  @isTest
  private static void updateToDuplicateLawsuit() {

    Intake__c originalIntake = getIntake();

    List<Account> testAccounts = new List<Account>{
        TestUtil.createPersonAccount('Client', 'One'),
        TestUtil.createPersonAccount('Injured', 'Party'),
        TestUtil.createPersonAccount('The', 'Caller')};

    for (Account acct : testAccounts)
    {
        acct.External_ID__c = mmlib_Utils.generateGuid();
    }

    Database.insert(testAccounts);

    Incident__c incident = new Incident__c();
    Database.insert(incident);

    Intake__c intake = createIntake(incident.Id, testAccounts.get(0).Id, testAccounts.get(1).Id, testAccounts.get(2).Id);
    Database.insert(intake);

    System.debug(JSON.serialize([select id, name, external_id__c from lawsuit__c]));

    Lawsuit__c newLawsuit = createLawsuit(intake.Id);
    Database.insert(newLawsuit);

    try {
        Test.startTest();
        newLawsuit.Intake__c = originalIntake.Id;
        Database.update(newLawsuit);
        Test.stopTest();

        System.assert(false, 'Expected exception to be thrown');
    }
    catch (DmlException e) {
        System.assertEquals(1, e.getNumDml());
        System.assertEquals('The operation would result in more than one Lawsuit associated to the Intake.', e.getDmlMessage(0));
    }
  }
}