public virtual class mmwiz_ActionGenerateNewRecord
    extends mmwiz_AbstractActionModel
    implements mmwiz_IGenerateRecord
{
    public mmwiz_ActionGenerateNewRecord()
    {
        // Code.
    }

    //private SObjectType sobjType = null;
    private string sobjType = null;

    public virtual void setSObjectType(SObjectType sobjType)
    {
        this.sobjType = sobjType.getDescribe().getName();
    }

    public void setSObjectType( String sobjectType )
    {
        this.sobjType = sobjectType;
    }

    public virtual SObject generateRecord()
    {
        if (sobjType == null)
        {
            throw new mmwiz_Exceptions.ParameterException('An SObjectType is required.  Implement \'setSObjectType()\'.');
        }

        //return sobjType.newSObject();
        return (SObject)system.type.forName( sobjType ).newInstance(); //.newSObject();
    }
}