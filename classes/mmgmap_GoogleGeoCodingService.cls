public with sharing class mmgmap_GoogleGeoCodingService
{
	private static mmgmap_IGeoCodingService service()
	{
		return (mmgmap_IGeoCodingService) mm_Application.Service.newInstance(mmgmap_IGeoCodingService.class);
	}

	public static Location getLatitudeLongitudeForAddress(mmgmap_Address addr)
	{
		return service().getLatitudeLongitudeForAddress(addr);
	}
}