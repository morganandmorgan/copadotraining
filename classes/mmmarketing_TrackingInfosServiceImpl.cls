/**
 *  mmmarketing_TrackingInfosServiceImpl
 */
public with sharing class mmmarketing_TrackingInfosServiceImpl
    implements mmmarketing_ITrackingInfosService
{
    private mmmarketing_TrackingInfoFactory marketingTrackingInfoFactory = null;

    public void recordCallsForIntakes( list<mmmarketing_ITrackInfoRecordCallRequest> requests )
    {
        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        set<id> intakeIdSet = new set<id>();

        for ( mmmarketing_ITrackInfoRecordCallRequest request : requests )
        {
            if ( request.getRelatedIntakeId() != null )
            {
                intakeIdSet.add( request.getRelatedIntakeId() );
            }
        }

        set<id> intakesWithMTIRecordsIdSet = new set<id>();

        list<Marketing_Tracking_Info__c> mtiRecordsThatAlreadyExistList = mmmarketing_TrackingInfosSelector.newInstance().selectByIntake( intakeIdSet );

        for ( Marketing_Tracking_Info__c mtiRecordsThatAlreadyExist : mtiRecordsThatAlreadyExistList )
        {
            intakesWithMTIRecordsIdSet.add( mtiRecordsThatAlreadyExist.Intake__c );
        }

        for ( mmmarketing_ITrackInfoRecordCallRequest request : requests )
        {
            // generate a new call MTI only if doesn't already exist
            if ( request.getRelatedIntakeId() != null
                && string.isNotBlank( request.getCallingNumber() )
                && request.getCallStartTime() != null
                )
            {
                if ( ! intakesWithMTIRecordsIdSet.contains( request.getRelatedIntakeId() ) )
                {
                    // get the factory to generate a new call version of a MTI
                    mmmarketing_TrackingInfoFactory.getInstance(uow).generateNewCall( new Intake__c(id = request.getRelatedIntakeId())
                                                                                                  , request.getCallingNumber()
                                                                                                  , request.getCallStartTime());
                }
                else
                {
                    system.debug( 'A ' + Marketing_Tracking_Info__c.SObjectType.getDescribe().getLabel() + ' record already exists for ' + request.getRelatedIntakeId().getSObjectType().getDescribe().getLabel()
                                   + ' with record id of \'' + request.getRelatedIntakeId() + '\'.');
                }
            }
        }

        try
        {
            uow.commitWork();
        }
        catch ( System.DMLException dmle )
        {
            system.debug( dmle );
        }
    }

    /**
     * Executes batch for previous day
     */
    public void updateAllCallRelatedInformation() {
        Set<Id> marketingInfoIdSet = returnMarketingInfoIdSet(DateTime.Now().AddDays(-1));
        if(marketingInfoIdSet != null) {
            new mmmarketing_TrackingInfosServiceImpl().updateAllCallRelatedInformation(marketingInfoIdSet);
        }
    }
    
    /**
     * Executes batch for the date time mentioned
     */
    public void updateAllCallRelatedInformation(DateTime createdDateForRecord) {
        Set<Id> marketingInfoIdSet = returnMarketingInfoIdSet(createdDateForRecord);
        if(marketingInfoIdSet != null) {
            new mmmarketing_TrackingInfosServiceImpl().updateAllCallRelatedInformation(marketingInfoIdSet);
        }
    }
    
    /**
     * Returns the records id set matching the date criteria and Call_Info_Resolution_Status__c = 'NeedsProcessing' and RecordType.Name = 'Call'
     */
    public Set<Id> returnMarketingInfoIdSet(DateTime createdDateForRecord) {
        List<Marketing_Tracking_Info__c> marketingTrackingInfos = [SELECT Id, Call_Info_Resolution_Status__c
                                                                   FROM Marketing_Tracking_Info__c
                                                                   WHERE Call_Info_Resolution_Status__c = 'NeedsProcessing'
                                                                   AND CreatedDate >: createdDateForRecord
                                                                   AND RecordType.Name = 'Call'];
        if(marketingTrackingInfos != null && !marketingTrackingInfos.isEmpty()) {
            return (new Map<Id, Marketing_Tracking_Info__c>(marketingTrackingInfos)).keySet();
        }
        return null;
    }

    /**
     *  Manages the updating of Marketing_Tracking_Info__c
     */
    public void updateCallRelatedInformation( set<id> mtiCallRecordIdSet )
    {
        // parameter check -- filter this down to only mtiCallRecordIds.   It is possible that the mtiCallRecordIdSet includes non-MTI-Call ids.
        list<Marketing_Tracking_Info__c> mtiCallRecordsList = mmmarketing_TrackingInfosSelector.newInstance().selectCallOnlyById( mtiCallRecordIdSet );

        // what kind of batch size?
        //  1 callout for account id
        // * 6 accounts
        //  All of those are cached for the rest of the excutor life cycle
        // limitCallouts [LC] == 100
        // LC - CTMAccountSize = remaining calls available [RCA]
        // each MTI-Call record potentially could make a maximum of CTMAccountSize callouts to CTM
        // Number of records able to be processed == RCA / CTMAccountSize

        integer numOfCalloutAuthorizations = new mmmarketing_UpdateCallInfoExecutor().getAvailableAuthorizationsCount();
        integer numOfTotalCalloutsAllowed = integer.valueOf( Limits.getLimitCallouts() * 0.75 );
        integer numOfRecordsPerBatchChunk = (numOfTotalCalloutsAllowed / numOfCalloutAuthorizations);

        if(numOfRecordsPerBatchChunk <= 1) {
            numOfRecordsPerBatchChunk = 2000;
        }

        // pass the mtiCallRecordsList list ids to the bacthable and execute
        new mmlib_GenericBatch( mmmarketing_UpdateCallInfoExecutor.class
                                          , mmmarketing_TrackingInfosSelector.newInstance().selectSObjectsByIdQuery()
                                          , mtiCallRecordIdSet
           ).setBatchSizeTo( numOfRecordsPerBatchChunk ).execute();
    }

    public void updateCallRelatedInformation_ShallowReconciliation( set<id> mtiCallRecordIdSet ) {
        // parameter check -- filter this down to only mtiCallRecordIds.   It is possible that the mtiCallRecordIdSet includes non-MTI-Call ids.
        list<Marketing_Tracking_Info__c> mtiCallRecordsList = mmmarketing_TrackingInfosSelector.newInstance().selectCallOnlyById( mtiCallRecordIdSet );

        integer numOfCalloutAuthorizations = new mmmarketing_ShallowReconcileExecutor().getAvailableAuthorizationsCount();
        integer numOfTotalCalloutsAllowed = integer.valueOf( Limits.getLimitCallouts() * 0.75 );
        integer numOfRecordsPerBatchChunk = (numOfTotalCalloutsAllowed / numOfCalloutAuthorizations);

        if(numOfRecordsPerBatchChunk <= 1) {
            numOfRecordsPerBatchChunk = 2000;
        }

        // pass the mtiCallRecordsList list ids to the bacthable and execute
        new mmlib_GenericBatch( mmmarketing_ShallowReconcileExecutor.class
                                          , mmmarketing_TrackingInfosSelector.newInstance().selectSObjectsByIdQuery()
                                          , mtiCallRecordIdSet
           ).setBatchSizeTo( numOfRecordsPerBatchChunk ).execute();
    }


    // Added as an entry point for calling from anonymous apex with records to backfill
    public void updateAllCallRelatedInformation( set<id> mtiCallRecordIdSet )
    {
        integer numOfCalloutAuthorizations = new mmmarketing_UpdateCallInfoExecutor().getAvailableAuthorizationsCount();
        integer numOfTotalCalloutsAllowed = integer.valueOf( Limits.getLimitCallouts() * 0.75 );
        integer numOfRecordsPerBatchChunk = (numOfTotalCalloutsAllowed / numOfCalloutAuthorizations);

        if(numOfRecordsPerBatchChunk <= 1) {
            numOfRecordsPerBatchChunk = 2000;
        }

        // pass the mtiCallRecordsList list ids to the bacthable and execute
        new mmlib_GenericBatch( mmmarketing_UpdateCallInfoExecutor.class
                , mmmarketing_TrackingInfosSelector.newInstance().selectSObjectsByIdQuery()
                , mtiCallRecordIdSet
        ).setBatchSizeTo( numOfRecordsPerBatchChunk ).execute();
    }

    public void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}