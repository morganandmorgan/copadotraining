public abstract class mmlib_AbstractAction
    implements mmlib_IAction, mmlib_IQueueableAction, Queueable, mmlib_IUnitOfWorkable
{
    private boolean isActionToRunInQueue = false;
    private mmlib_IQueueableAction nextQueueableAction = null;
    private Boolean isRecordsRequired = true;

    protected List<SObject> records = new List<SObject>();
    protected mmlib_ISObjectUnitOfWork uow = null;

    public mmlib_AbstractAction setActionToRunInQueue( boolean isActionToRunInQueue )
    {
        this.isActionToRunInQueue = isActionToRunInQueue;

        return this;
    }

    public mmlib_AbstractAction setNextQueueableActionInChain( mmlib_IQueueableAction nextQueueableAction )
    {
        this.nextQueueableAction = nextQueueableAction;

        return this;
    }

    public mmlib_AbstractAction setRecordsRequired( Boolean isRecordsRequired )
    {
        this.isRecordsRequired = isRecordsRequired;

        return this;
    }

    public mmlib_IQueueableAction getNextQueuableAction()
    {
        return this.nextQueueableAction;
    }

    public mmlib_IAction setRecordsToActOn( List<SObject> records )
    {
        this.records = records;

        return this;
    }

    public mmlib_IUnitOfWorkable setUnitOfWork( mmlib_ISObjectUnitOfWork uow )
    {
        this.uow = uow;

        return this;
    }

    public Boolean run()
    {
        Boolean response = false;

        if ( ( isRecordsRequired && ! this.records.isEmpty() )
            || ! isRecordsRequired )
        {
            if (this.isActionToRunInQueue)
            {
                // Send it to the queue for processing later
                System.enqueueJob( this );
            }
            else
            {
                response = runInProcess();
            }
        }
        else
        {
            response = true;
        }

        return response;
    }

    public void execute(QueueableContext context)
    {
        try
        {
            this.uow = mm_Application.UnitOfWork.newInstance();

            runInProcess();

            this.uow.commitWork();
        }
        catch (Exception e)
        {
            system.debug(e);
            system.debug(e.getStackTraceString());
        }

        if (getNextQueuableAction() != null)
        {
            System.enqueueJob( getNextQueuableAction() );
        }
    }

    public abstract boolean runInProcess();
}