/**
 * FeeGoalAllocation_Domain_Test
 * @description Test for Fee Goal Allocation Domain class.
 * @author Jeff Watson
 * @date 2/13/2019
 */
@IsTest
public with sharing class FeeGoalAllocation_Domain_Test {

    private static Fee_Goal__c feeGoal;
    private static Settlement__c settlement;
    private static FeeGoalAllocation_Selector feeGoalAllocationSelector;

    static {
        feeGoal = TestUtil.createFeeGoal();
        insert feeGoal;

        settlement = TestUtil.createSettlement();
        insert settlement;

        feeGoalAllocationSelector = new FeeGoalAllocation_Selector();
    }

    @IsTest
    private static void ctor() {
        FeeGoalAllocation_Domain feeGoalAllocationDomain = new FeeGoalAllocation_Domain();
        System.assert(feeGoalAllocationDomain != null);
    }

    @IsTest
    private static void triggerHandlers() {

        // Test an insert
        Fee_Goal_Allocation__c feeGoalAllocation = TestUtil.createFeeGoalAllocation(feeGoal, settlement);
        insert feeGoalAllocation;
        System.assert(feeGoalAllocation != null);
        System.assert(String.isNotBlank(feeGoalAllocation.Id));

        // Test an update
        feeGoalAllocation.Allocated_Amount__c = 100;
        update feeGoalAllocation;
        feeGoalAllocation = feeGoalAllocationSelector.selectById(new Set<Id> {feeGoalAllocation.Id})[0];
        System.assert(feeGoalAllocation != null);
        System.assertEquals(100, feeGoalAllocation.Allocated_Amount__c);

        // Test a delete
        delete feeGoalAllocation;
        List<Fee_Goal_Allocation__c> feeGoalAllocations = feeGoalAllocationSelector.selectById(new Set<Id> {feeGoalAllocation.Id});
        System.assert(feeGoalAllocations != null);
        System.assertEquals(0, feeGoalAllocations.size());
    }
}