/**
 * FeeGoal_Selector
 * @description Selector for Fee Goal SObject.
 * @author Jeff Watson
 * @date 2/13/2019
 */

public with sharing class FeeGoal_Selector extends fflib_SObjectSelector {

    public FeeGoal_Selector() { }

    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Fee_Goal__c.SObjectType;
    }

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Fee_Goal__c.Apply_Pre_Suit_Only__c,
                Fee_Goal__c.CreatedDate,
                Fee_Goal__c.Current_Target__c,
                Fee_Goal__c.Current_Trend__c,
                //Fee_Goal__c.Department__c,
                Fee_Goal__c.Employee__c,
                Fee_Goal__c.End_Date__c,
                Fee_Goal__c.Goal_Amount__c,
                Fee_Goal__c.Id,
                Fee_Goal__c.Monthly_Target__c,
                Fee_Goal__c.My_Department__c,
                Fee_Goal__c.Name,
                //Fee_Goal__c.Office__c,
                Fee_Goal__c.Start_Date__c,
                Fee_Goal__c.Total_Settlement_Fees__c
        };
    }

    // Methods
    public List<Fee_Goal__c> selectById(Set<Id> ids) {
        return (List<Fee_Goal__c>) selectSObjectsById(ids);
    }
}