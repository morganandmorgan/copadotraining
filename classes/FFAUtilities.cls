/*=======================================================================================
Name            : FFAUtilities
Author          : CLD
Created Date    : Jul 2018
Description     : Contains various methods for FF Accounting Utilities
=======================================================================================*/

public class FFAUtilities {
    
    /*-----------------------------------------------------------------------------
    Method to return a list of the current companies
    -----------------------------------------------------------------------------*/
    public static List<c2g__codaCompany__c> getCurrentCompanies()
    {
        List<c2g__codaCompany__c> allCompanies = [Select id, ownerid, name,c2g__CustomerSettlementDiscount__c,c2g__CustomerWriteOff__c,c2g__CustomerCurrencyWriteOff__c from c2g__codaCompany__c];
        Id currentUserId = UserInfo.getUserId();
        List<c2g__codaCompany__c> myCompanies = new List<c2g__codaCompany__c>();
        Set<Id> currentUserGroupIds = new Set<Id>();
        for(GroupMember gm :[SELECT Id, GroupId FROM GroupMember WHERE UseroRGroupId = :currentUserId]) 
        {
            currentUserGroupIds.add(gm.GroupId);
        }
        for(c2g__codaCompany__c company : allCompanies)
        {
            if(currentUserGroupIds.contains(company.ownerid))
            {
                myCompanies.add(company);
            }
        }
        return myCompanies;
    }
    /*-----------------------------------------------------------------------------
    Method to post Journals - Note the journals should only relate to a single company
    -----------------------------------------------------------------------------*/
    public static List<c2g__codaJournal__c> postSingleJournal(Id journalid)
    {
        List<c2g__codaJournal__c> jnlList = [select id, c2g__OwnerCompany__r.name from c2g__codaJournal__c where id = :journalid];
        for(c2g__codaJournal__c jnl : jnlList)
        {
            c2g.CODAAPICommon_10_0.Context company = new c2g.CODAAPICommon_10_0.Context();
            company.CompanyName = jnl.c2g__OwnerCompany__r.name;
            company.Token = null;
            c2g.CODAAPICommon.Reference jnlRef = new c2g.CODAAPICommon.Reference();
            jnlRef.id = jnl.id;
            c2g.CODAAPIJournal_12_0.PostJournal(company, jnlRef);
        }
        return jnlList;
    }

    /*-----------------------------------------------------------------------------
    Method to post Journals                                                  
    -----------------------------------------------------------------------------*/
    public static List<c2g__codaJournal__c> postJournals(List<Id> journalIds)
    {
        Set<String> companyString = new Set<String>();
        String COMPANY_NAME = '';
        List<c2g__codaJournal__c> journalList = [SELECT Id, c2g__OwnerCompany__r.Name
            FROM c2g__codaJournal__c
            WHERE Id in :journalIds];
        if(!journalList.isEmpty())
        {
            for(c2g__codaJournal__c j : journalList)
            {
                companyString.add(j.c2g__OwnerCompany__r.Name);
                COMPANY_NAME = j.c2g__OwnerCompany__r.Name;
            }
            System.assert(companyString.size() <= 1, 'ERROR: You can only post journals for one company at a time');

            c2g.CODAAPICommon_10_0.Context company = new c2g.CODAAPICommon_10_0.Context(); 
            company.CompanyName = COMPANY_NAME;
            company.Token = null;

            List<c2g.CODAAPICommon.Reference> postList = new List<c2g.CODAAPICommon.Reference>();
            for(c2g__codaJournal__c j : journalList)
            {
                c2g.CODAAPICommon.Reference journalRef = new c2g.CODAAPICommon.Reference();
                journalRef.id = j.id;
                postList.add(journalRef);
            }

            c2g.CODAAPIJournal_12_0.BulkPostJournal(company, postList); 
        }
        return journalList;
    }
    /*-----------------------------------------------------------------------------
    Method to post Payable Invoices                                                  
    -----------------------------------------------------------------------------*/
    public static List<c2g__codaPurchaseInvoice__c> postPINs(List<Id> pinIds)
    {
        Set<String> companyString = new Set<String>();
        String COMPANY_NAME = '';
        List<c2g__codaPurchaseInvoice__c> pinList = [SELECT Id, c2g__OwnerCompany__r.Name
            FROM c2g__codaPurchaseInvoice__c
            WHERE Id in :pinIds];
        if(!pinList.isEmpty())
        {
            for(c2g__codaPurchaseInvoice__c pin : pinList)
            {
                companyString.add(pin.c2g__OwnerCompany__r.Name);
                COMPANY_NAME = pin.c2g__OwnerCompany__r.Name;
            }
            System.assert(companyString.size() <= 1, 'ERROR: You can only post journals for one company at a time');

            c2g.CODAAPICommon_9_0.Context company = new c2g.CODAAPICommon_9_0.Context(); 
            company.CompanyName = COMPANY_NAME;
            company.Token = null;

            List<c2g.CODAAPICommon.Reference> postList = new List<c2g.CODAAPICommon.Reference>();
            for(c2g__codaPurchaseInvoice__c pin : pinList)
            {
                c2g.CODAAPICommon.Reference pinRef = new c2g.CODAAPICommon.Reference();
                pinRef.id = pin.id;
                postList.add(pinRef);
            }

            c2g.CODAAPIPurchaseInvoice_9_0.BulkPostPurchaseInvoice(company, postList); 
        }
        return pinList;
    }

    /*-----------------------------------------------------------------------------
    Method to post Sales Invoices
    -----------------------------------------------------------------------------*/
    /* public static List<c2g__codaInvoice__c> postSINs(List<Id> sinIds)
    {
        Set<String> companyString = new Set<String>();
        String COMPANY_NAME = '';
        List<c2g__codaInvoice__c> pinList = [SELECT Id, c2g__OwnerCompany__r.Name
            FROM c2g__codaInvoice__c
            WHERE Id in :sinIds];
        if(!pinList.isEmpty())
        {
            for(c2g__codaInvoice__c pin : pinList)
            {
                companyString.add(pin.c2g__OwnerCompany__r.Name);
                COMPANY_NAME = pin.c2g__OwnerCompany__r.Name;
            }
            System.assert(companyString.size() <= 1, 'ERROR: You can only post journals for one company at a time');

            c2g.CODAAPICommon_10_0.Context company = new c2g.CODAAPICommon_10_0.Context(); 
            company.CompanyName = COMPANY_NAME;
            company.Token = null;

            List<c2g.CODAAPICommon.Reference> postList = new List<c2g.CODAAPICommon.Reference>();
            for(c2g__codaInvoice__c pin : pinList)
            {
                c2g.CODAAPICommon.Reference pinRef = new c2g.CODAAPICommon.Reference();
                pinRef.id = pin.id;
                postList.add(pinRef);
            }

            c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(company, postList); 
        }
        return pinList;
    } */

    /*-----------------------------------------------------------------------------
    Method to Init Journal Wrapper from FF API
    -----------------------------------------------------------------------------*/
    public static c2g.CODAAPIJournalTypes_12_0.Journal initFFAPIJournal(c2g__codaCompany__c company, Id accountingCurrency){

        c2g.CODAAPIJournalTypes_12_0.Journal returnJNL = new c2g.CODAAPIJournalTypes_12_0.Journal();

        //reference wrapper for onwerId
        c2g.CODAAPICommon.Reference ownerRef = new c2g.CODAAPICommon.Reference();
        ownerRef.Id = company.OwnerId;
        returnJNL.OwnerId = ownerRef;

        //reference wrapper for currency
        c2g.CODAAPICommon.Reference currencyRef = new c2g.CODAAPICommon.Reference();
        currencyRef.Id = accountingCurrency;
        returnJNL.JournalCurrency = currencyRef;

        //journal status
        returnJNL.JournalStatus = c2g.CODAAPIJournalTypes_12_0.enumJournalStatus.InProgress;

         //journal type:
        returnJNL.TypeRef = c2g.CODAAPIJournalTypes_12_0.enumType.ManualJournal;

        return returnJNL;

    }

    /*-----------------------------------------------------------------------------
    Method to Init Journal Line Wrapper from FF API
    -----------------------------------------------------------------------------*/
    public static c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem initFFAPIJournalLine(String dr_cr,
                                                                                            String lineDescription,
                                                                                            String linetype, 
                                                                                            Decimal lineValue, 
                                                                                            Id glaId, 
                                                                                            String dim1, 
                                                                                            String dim2, 
                                                                                            String dim3, 
                                                                                            String dim4,
                                                                                            String matterId){
        c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem returnJNLLine = new c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem();

        //reference wrapper for glaId:
        c2g.CODAAPICommon.Reference glaRef = new c2g.CODAAPICommon.Reference();
        glaRef.Id = glaId;
        returnJNLLine.GeneralLedgerAccount = glaRef;
        
        returnJNLLine.LineDescription = lineDescription;

        //enum for line type
        returnJNLLine.LineTypeRef  = linetype == 'General Ledger Account' ? c2g.CODAAPIJournalLineItemTypes_12_0.enumLineType.GeneralLedgerAccount :
                                     linetype == 'Intercompany' ? c2g.CODAAPIJournalLineItemTypes_12_0.enumLineType.Intercompany :
                                     lineType == 'Account - Customer' ? c2g.CODAAPIJournalLineItemTypes_12_0.enumLineType.Account_Customer : 
                                     lineType == 'Account - Vendor' ? c2g.CODAAPIJournalLineItemTypes_12_0.enumLineType.Account_Vendor : null;

        //enum for debit / credit:
        //returnJNLLine.DebitCredit = dr_cr == 'Debit' ? c2g.CODAAPIJournalLineItemTypes_12_0.enumDebitCredit.Debit : c2g.CODAAPIJournalLineItemTypes_12_0.enumDebitCredit.Credit;

        returnJNLLine.Value = lineValue;

        c2g.CODAAPICommon.Reference dim1Ref = new c2g.CODAAPICommon.Reference();
        dim1Ref.Id = dim1 != '' ? dim1 : null;
        returnJNLLine.Dimension1 = dim1Ref;

        c2g.CODAAPICommon.Reference dim2Ref = new c2g.CODAAPICommon.Reference();
        dim2Ref.Id = dim2 != '' ? dim2 : null;
        returnJNLLine.Dimension2 = dim2Ref;

        c2g.CODAAPICommon.Reference dim3Ref = new c2g.CODAAPICommon.Reference();
        dim3Ref.Id = dim3 != '' ? dim3 : null;
        returnJNLLine.Dimension3 = dim3Ref;

        c2g.CODAAPICommon.Reference dim4Ref = new c2g.CODAAPICommon.Reference();
        dim4Ref.Id = dim4 != '' ? dim4 : null;
        returnJNLLine.Dimension4 = dim4Ref;

        /*Setup Custom Fields*/
        List<c2g.CODAAPIJournalLineItemTypes_12_0.CustomField> jnlLineCustomFields = new List<c2g.CODAAPIJournalLineItemTypes_12_0.CustomField>();            
            //customer engagement custom field:
            if(matterId != null && matterId != ''){
                c2g.CODAAPIJournalLineItemTypes_12_0.CustomField matter_field = new c2g.CODAAPIJournalLineItemTypes_12_0.CustomField();
                matter_field.FieldName = 'Matter__c';
                matter_field.Value = matterId;
                jnlLineCustomFields.add(matter_field);    
            }
        returnJNLLine.CustomFields = jnlLineCustomFields;

        return returnJNLLine;

    }

    /*-----------------------------------------------------------------------------
    Method to check gov limits
    -----------------------------------------------------------------------------*/
    public static String checkGovLimits(){
        String returnString = '';
        returnString += '****** \n\n SOQL Queries Allowed: ' + Limits.getLimitQueries() +'\n';
        returnString += 'SOQL Queries Used: ' + Limits.getQueries() +'\n';
        
        returnString += '\n Query Rows Allowed: ' + Limits.getLimitDmlRows() +'\n';
        returnString += 'Query Rows Used: ' + Limits.getDmlRows() +'\n';

        returnString += '\n DML Statements Allowed: ' + Limits.getLimitDmlStatements() +'\n';
        returnString += 'DML Statements Used: ' +  Limits.getDmlStatements() +'\n';

        returnString += '\n CPU Time Allowed (ms): ' + Limits.getLimitCpuTime() +'\n';
        returnString += 'CPU Time Used (ms): ' + Limits.getCpuTime() +'\n';

        return returnString;
    }

    
}