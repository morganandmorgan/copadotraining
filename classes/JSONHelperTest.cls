@isTest
private class JSONHelperTest {

	@isTest
    private static void nullArgument() {
        String arg = null;

        Test.startTest();
        Object result = JSONHelper.SafeDeserializeUntyped(arg);
        Test.stopTest();

        System.assertEquals(null, result);
    }

    @isTest
    private static void shortArgument() {
        String arg = '{';

        Test.startTest();
        Object result = JSONHelper.SafeDeserializeUntyped(arg);
        Test.stopTest();

        System.assertEquals(null, result);
    }

    @isTest
    private static void missingOpenBrace() {
        String obj = 'Test Value';
        String arg = '"' + obj + '"';

        Test.startTest();
        Object result = JSONHelper.SafeDeserializeUntyped(arg);
        Test.stopTest();

        System.assertEquals(obj, result);
    }

    @isTest
    private static void wellFormattedArgument() {
        Account obj = new Account(Name = 'Test Name');
        String arg = JSON.serialize(obj);

        Test.startTest();
        Object result = JSONHelper.SafeDeserializeUntyped(arg);
        Test.stopTest();

        System.assertEquals(JSON.deserializeUntyped(arg), result);
    }
}