public class QuestionnaireCommentExtension {

    public ApexPages.StandardController stdController;
    Id questId;
    public Comments__c commentRec { get; set; } 
    
    public QuestionnaireCommentExtension(ApexPages.StandardController stdControllerParam) {        
        
        stdController = stdControllerParam;
        questId = stdController.getId();
        if(questId != NULL) {
            Questionnaire__c questRec = [SELECT Intake__c FROM Questionnaire__c WHERE Id = :questId];
            commentRec = new Comments__c(Questionnaire__c = questId, Intake__c = questRec.Intake__c);
        }
    } 
    
    public PageReference saveComment() {        
        try {
            insert commentRec;
            Questionnaire__c questRec = [SELECT Intake__c FROM Questionnaire__c WHERE Id = :questId];
            commentRec = new Comments__c(Comments__c = '', Questionnaire__c = questId, Intake__c = questRec.Intake__c);
        } catch(Exception e) {        
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        } /*catch(DMLException de) {                
            for (Integer i = 0; i < de.getNumDml(); i++) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, de.getDmlMessage(i));
                ApexPages.addMessage(myMsg);
                return null;
            }
        }*/
        return null;
    }   
}