/*=======================================================================================
Name            : WriteOffExpensesController
Author          : CLD
Created Date    : Aug 27,2018
Description     : Write Off Expenses page controller
Test Class      : WriteOffExpensesController_Test
Revisions       :
=======================================================================================*/
public with sharing class WriteOffExpensesController 
{
    String matterId{get;set;}
    public List<ExpenseHolder> expenseList{get;set;}
    public List<litify_pm__Expense__c> otherExpenses{get;set;}
    public Boolean allIsSelected{get;set;}
    public litify_pm__Matter__c matter{get;set;}
    public Write_Off_Settings__c expenseSettings;

    public WriteOffExpensesController()
    {
        matterId = ApexPages.currentPage().getParameters().get('matterId');

        expenseSettings = Write_Off_Settings__c.getOrgDefaults();
        loadData();
    }

    public Decimal writtenOffSoft{
        get{
            Decimal writtenOff = 0.00;
            FOR (litify_pm__Expense__c expense : otherExpenses){
                if (expense.CostType__c == 'SoftCost' && expense.Written_Off__c == true)
                {
                    writtenOff += expense.Written_Off_Amount__c;
                }
            }
            return writtenOff;
        }
    }

    public Decimal writtenOffHard{
        get{
            Decimal writtenOff = 0.00;
            FOR (litify_pm__Expense__c expense : otherExpenses){
                if (expense.CostType__c == 'HardCost' && expense.Written_Off__c == true)
                {
                    writtenOff += expense.Written_Off_Amount__c;
                }
            }
            return writtenOff;
        }
    }

    public Decimal recoveredSoft{
        get{
            Decimal recovered = 0.00;
            FOR (litify_pm__Expense__c expense : otherExpenses){
                if (expense.CostType__c == 'SoftCost' && expense.Amount_Recovered__c != null)
                {
                    recovered += expense.Amount_Recovered__c;
                }
            }
            return recovered;
        }
    }

    public Decimal recoveredHard{
        get{
            Decimal recovered = 0.00;
            FOR (litify_pm__Expense__c expense : otherExpenses){
                if (expense.CostType__c == 'HardCost' && expense.Amount_Recovered__c != null){
                    recovered += expense.Amount_Recovered__c;
                }
            }
            return recovered;
        }
    }

    public Decimal totalHardCosts {
        get{
            Decimal hardCost = 0;
            FOR (litify_pm__Expense__c expense : otherExpenses){
                if (expense.CostType__c == 'HardCost' && expense.litify_pm__Amount__c != null)
                {
                    hardCost += expense.litify_pm__Amount__c;
                }
            }
            return hardCost;
        }
        set;
    }

    public Decimal totalSoftCosts{
        get{
            Decimal softCost = 0;
            FOR (litify_pm__Expense__c expense : otherExpenses){
                if (expense.CostType__c == 'SoftCost' && expense.litify_pm__Amount__c != null)
                {
                    softCost += expense.litify_pm__Amount__c;
                }
            }
            return softCost;
        }
        set;
    }

    public void loadData()
    {
        matter = [SELECT Id, 
            FFA_Company__c, 
            Name,
            Assigned_Office_Location__r.c2g__codaDimension1__c,
            litify_pm__Case_Type__r.Dimension_2__c,
            litify_pm__Case_Type__r.Dimension_3__c,
            Total_Soft_Costs_Recovered__c,
            Total_Hard_Costs_Recovered__c, 
            Hard_cost_To_Recover__c,
            Soft_Cost_To_Recover__c
            FROM litify_pm__Matter__c 
            WHERE Id = :matterId];

        matter.Hard_Cost_To_Recover__c = 0;
        matter.Soft_Cost_To_Recover__c = 0;

        expenseList = new List<ExpenseHolder>();
        FOR (litify_pm__Expense__c expense : [Select Id,
            litify_pm__ExpenseType2__r.Name,
            litify_pm__ExpenseType2__r.General_Ledger_Account__c,
            litify_pm__ExpenseType2__r.Prepaid_Expense_Reverse_GLA__c,
            CostType__c,
            Name,
            PayableTo__c,
            PayableTo__r.Name,
            RequestedBy__r.Name,
            FFA_Company__c,
            litify_pm__Date__c,
            litify_pm__Note__c,
            litify_pm__Amount__c,
            litify_pm__Matter__c,
            Written_Off__c,
            Written_Off_Amount__c,
            Amount_Remaining_to_Write_Off__c,
            Payable_Invoice__c,
            Payable_Invoice_Created__c,
            Journal__c,
            Prepaid_Relief_Journal__c,
            Prepaid_Relief_Processed__c,
            Journal_Line_Item__c,
            Prepaid_Expense_Type__c,
            Amount_Recovered__c
            FROM litify_pm__Expense__c
            WHERE Written_Off__c = false
            AND Amount_Remaining_to_Write_Off__c != 0
            AND Fully_Recovered__c = false
            AND litify_pm__Matter__c = :matterId
            AND litify_pm__ExpenseType2__r.Exclude_From_Allocation__c = FALSE
            ORDER BY litify_pm__Amount__c
            LIMIT 1000])
        {
            expenseList.add(new ExpenseHolder(expense));
        }

        otherExpenses = [SELECT Id, 
            CostType__c,
            Written_Off_Amount__c,
            litify_pm__Amount__c,
            Amount_Recovered__c,
            Fully_Recovered__c,
            Written_Off__c
            FROM litify_pm__Expense__c
            WHERE litify_pm__ExpenseType2__r.Exclude_From_Allocation__c = FALSE
            AND litify_pm__Matter__c = :matterId];
    }
    public PageReference save()
    {
        List<litify_pm__Expense__c> toSaveExpenses = new List<litify_pm__Expense__c>();
        List<litify_pm__Expense__c> toUpdateExpenses = new List<litify_pm__Expense__c>();
        litify_pm__Expense__c hardCostWriteOffExpense;
        litify_pm__Expense__c softCostWriteOffExpense;

        Boolean hasError = false;

        c2g__codaCompany__c company = null;
        try{
            company = [SELECT Id, OwnerId FROM c2g__codaCompany__c WHERE Id = :matter.FFA_Company__c];
        }
        catch (Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'FFA Company was not found on the Matter! Please contact your administrator.'));
            return null;
        }

        Decimal reversingJournalAmt = 0.00;
        Id glaForReverse = null;

        c2g__codaJournal__c newJournal = new c2g__codaJournal__c(
            c2g__OwnerCompany__c = company.Id,
            OwnerId = company.OwnerId,
            c2g__Type__c = 'Manual Journal',
            c2g__Reference__c = 'Write off Recording on '+Date.today().format(),
            c2g__DeriveCurrency__c = true,
            c2g__DerivePeriod__c = true,
            c2g__JournalDate__c = Date.today()
        );

        for (ExpenseHolder holder : expenseList){
            if (holder.isSelected){
                if (holder.expense.litify_pm__Amount__c == holder.writeOffAmount){
                    holder.expense.Written_Off__c = true;
                }

                //set the written off amount to zero if not already
                holder.expense.Written_Off_Amount__c = holder.expense.Written_Off_Amount__c == null ? holder.expense.Written_Off_Amount__c = 0.00 : holder.expense.Written_Off_Amount__c; 

                if(holder.expense.CostType__c == 'HardCost'){
                    //create or update the hard cost summary expense
                    if(hardCostWriteOffExpense == null){
                        hardCostWriteOffExpense = new litify_pm__Expense__c(
                            litify_pm__ExpenseType2__c = expenseSettings.Write_Off_Hard_Costs_Expense_Type_ID__c,
                            litify_pm__Matter__c = matterId,
                            litify_pm__Note__c = 'Hard Cost Write Off',
                            litify_pm__Amount__c = holder.writeOffAmount*-1,
                            litify_pm__Date__c = Date.today(),
                            PayableTo__c = holder.expense.PayableTo__c
                        );
                    }    
                    else{
                        hardCostWriteOffExpense.litify_pm__Amount__c -= holder.writeOffAmount;
                    }
                    holder.expense.Expense_Written_Off__r = hardCostWriteOffExpense;
                }        
                if(holder.expense.CostType__c == 'SoftCost'){
                    //create or update the hard cost summary expense
                    if(softCostWriteOffExpense == null){
                        softCostWriteOffExpense = new litify_pm__Expense__c(
                            litify_pm__ExpenseType2__c = expenseSettings.Write_Off_Soft_Costs_Expense_Type_ID__c,
                            litify_pm__Matter__c = matterId,
                            litify_pm__Note__c = 'Soft Cost Write Off',
                            litify_pm__Amount__c = holder.writeOffAmount*-1,
                            litify_pm__Date__c = Date.today(),
                            PayableTo__c = holder.expense.PayableTo__c
                        );
                    }    
                    else{
                        softCostWriteOffExpense.litify_pm__Amount__c -= holder.writeOffAmount;
                    }
                    holder.expense.Expense_Written_Off__r = softCostWriteOffExpense;
                }

                holder.expense.Written_Off_Amount__c += holder.writeOffAmount;
                
                // If the expense type = 'Hard Cost' and there has been a journal entry created,
                // we need to create a reversing journal entry for it.
                if (hardCostWriteOffExpense != null && holder.expense.CostType__c == 'HardCost' && (holder.expense.Payable_Invoice_Created__c == true || holder.expense.Prepaid_Relief_Processed__c == true))
                {
                    reversingJournalAmt -= holder.amountWrittenOff;
                    glaForReverse = holder.expense.Prepaid_Expense_Type__c == true ? holder.expense.litify_pm__ExpenseType2__r.Prepaid_Expense_Reverse_GLA__c : holder.expense.litify_pm__ExpenseType2__r.General_Ledger_Account__c;
                    hardCostWriteOffExpense.Write_Off_Journal__r = newJournal;
                }

                // if(softCostWriteOffExpense != null){
                //     toSaveExpenses.add(softCostWriteOffExpense);
                // }
                // if(hardCostWriteOffExpense != null){
                //     toSaveExpenses.add(hardCostWriteOffExpense);
                // }
                toUpdateExpenses.add(holder.expense);
            }   
        }

        if (hasError)
            return null;

        if (reversingJournalAmt != 0)
        {
            INSERT newJournal;

            litify_pm__Expense_Type__c woExpType = [SELECT Id, General_Ledger_Account__c from litify_pm__Expense_Type__c 
                WHERE Id = :expenseSettings.Write_Off_Hard_Costs_Expense_Type_ID__c];

            List<c2g__codaJournalLineItem__c> lineItems = new List<c2g__codaJournalLineItem__c>();
            c2g__codaJournalLineItem__c jli = new c2g__codaJournalLineItem__c(
                c2g__Journal__c = newJournal.Id,
                Matter__c = matterId,
                c2g__Dimension1__c = matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
                c2g__Dimension2__c = matter.litify_pm__Case_Type__r.Dimension_2__c,
                c2g__Dimension3__c = matter.litify_pm__Case_Type__r.Dimension_3__c,
                c2g__Linetype__c = 'General Ledger Account',
                c2g__GeneralLedgerAccount__c = glaForReverse,
                c2g__Value__c = reversingJournalAmt,
                c2g__DeriveLineNumber__c = TRUE,
                c2g__LineDescription__c = 'Write Off recorded on Matter - '+ matter.Name);
            lineItems.add(jli);

            c2g__codaJournalLineItem__c jli2 = new c2g__codaJournalLineItem__c(
                c2g__Journal__c = newJournal.Id,
                Matter__c = matterId,
                c2g__Dimension1__c = matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
                c2g__Dimension2__c = matter.litify_pm__Case_Type__r.Dimension_2__c,
                c2g__Dimension3__c = matter.litify_pm__Case_Type__r.Dimension_3__c,
                c2g__Linetype__c = 'General Ledger Account',
                c2g__GeneralLedgerAccount__c = woExpType.General_Ledger_Account__c,
                c2g__Value__c = (reversingJournalAmt * -1),
                c2g__DeriveLineNumber__c = TRUE,
                c2g__LineDescription__c = 'Write Off recorded on Matter - '+ matter.Name);
            lineItems.add(jli2);

            INSERT lineItems;

            //Post the journal
            List<c2g__codaJournal__c> resultList = FFAUtilities.postSingleJournal(newJournal.Id);

            // Look for the new journal entries that have the journal associated.
            // FOR (litify_pm__Expense__c expense : toSaveExpenses){
            //     if (expense.Write_Off_Journal__r != null){
            //         expense.Write_Off_Journal__c = newJournal.Id;
            //         expense.Write_Off_Journal__r = null;
            //     }
            // }
        }   

        if (toUpdateExpenses.size() > 0){
            if(softCostWriteOffExpense != null){
                insert softCostWriteOffExpense;
            }
            if(hardCostWriteOffExpense != null){
                insert hardCostWriteOffExpense;
            }
            INSERT toSaveExpenses;
            FOR (litify_pm__Expense__c expense : toUpdateExpenses)
            {
                expense.Expense_Written_Off__c = expense.Expense_Written_Off__r.Id;
                expense.Write_Off_Journal__c = newJournal.Id;
                expense.Expense_Written_Off__r = null;
            }
            UPDATE toUpdateExpenses;
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 
                'Please select an expense to write off!'));
            return null;
        }
        loadData();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 
            'Write Offs were created successfully.'));
        return null;
    }

    public PageReference selectAll(){
        FOR (ExpenseHolder holder : expenseList)
        {
            holder.isSelected = allIsSelected;
        }
        return null;
    }
    public PageReference cancel(){
        return new PageReference('/lightning/r/litify_pm__Matter__c/'+ matterId + '/view');
    }

    public class ExpenseHolder{
        public litify_pm__Expense__c expense{get;set;}
        public Decimal writeOffAmount {get;set;}
        public Boolean isSelected{get;set;}
        public ExpenseHolder(litify_pm__Expense__c expense){
            this.writeOffAmount = 0;
            this.expense = expense;
            isSelected = false;
            writeOffAmount = expense.Amount_Remaining_to_Write_Off__c;
        }

        public Decimal amountWrittenOff{
            get{
                if (isSelected){
                    return writeOffAmount;
                }
                else{
                    return 0.00;
                }
            }
            set{
                writeOffAmount = value;
            }
        }
    }
}