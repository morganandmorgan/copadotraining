/**
 *  mmlib_BaseExceptions
 */
public class mmlib_BaseExceptions
{
    private mmlib_BaseExceptions() { }

    public abstract class ValidationException
        extends Exception
    {
        private map<Id, map<Schema.sObjectField, set<string>>> validationMessagesByRecordAndFieldMap = new map<Id, map<Schema.sObjectField, set<string>>>();
        private map<Id, set<string>> validationMessagesByRecordMap = new map<Id, set<string>>();
        private map<id, SObject> recordsMap = new map<id, SObject>();

        public ValidationException( list<SObject> records )
        {
            // recordsMap = new map<id, SObject>( records );
            for ( SObject record : records )
            {
                if ( record.id == null )
                {
                    recordsMap.put( fflib_IDGenerator.generate( record.getSobjectType() ), record );
                }
                else
                {
                    recordsMap.put( record.id, record );
                }
            }
        }

        public Set<id> getRecordIds()
        {
            return validationMessagesByRecordMap.keySet();
        }

        public map<Schema.sObjectField, set<string>> getFieldMessagesByRecordId( id recordId )
        {
            return recordId != null && validationMessagesByRecordAndFieldMap.containskey(recordId) ? validationMessagesByRecordAndFieldMap.get(recordId) : new map<Schema.sObjectField, set<string>>();
        }

        public set<string> getMessagesByRecordId( id recordId )
        {
            return recordId != null && validationMessagesByRecordMap.containskey(recordId) ? validationMessagesByRecordMap.get(recordId) : new set<string>();
        }

        private id findStorageIdForNewRecord( SObject newRecord )
        {
            id storageIdForNewRecord = null;

            // organize the fields that are populated on this record
            map<String, Object> newRecordFieldsToValueMap = newRecord.getPopulatedFieldsAsMap();

            // since this is a new record, there is no true record id to find the record from the recordsMap
            // so we will have to compare the populated fields to the populated fields in the recordsMap to see
            // if there is a match.  Once the match is found, we can then find the "storage id" generated for the
            // record and use that to assigned error messages to the correct record.

            SObject currentRecord = null;
            map<String, Object> currentRecordFieldsToValueMap = null;
            boolean isMatch = false;

            for ( id storageId : recordsMap.keySet() )
            {
                currentRecord = recordsMap.get( storageId );

                currentRecordFieldsToValueMap = currentRecord.getPopulatedFieldsAsMap();

                isMatch = true;

                for ( string populatedFieldName : newRecordFieldsToValueMap.keySet() )
                {
                    // if the populatedFieldName part of the currentRecord
                    if ( currentRecordFieldsToValueMap.containsKey( populatedFieldName ))
                    {
                        if ( currentRecordFieldsToValueMap.get( populatedFieldName ) == newRecordFieldsToValueMap.get( populatedFieldName ))
                        {
                            // the fields match
                        }
                        else
                        {
                            // the fields do not match
                            isMatch = false;
                            break;
                        }
                    }
                }

                // if we have looped through all fields and isMatch is still true, then this is the correct record ....we hope
                if ( isMatch )
                {
                    storageIdForNewRecord = storageId;
                    break;
                }
            }

            return storageIdForNewRecord;
        }

        public void addMessage( SObject record, Schema.sObjectField field, String message )
        {
            if ( record != null )
            {
                id recordId = record.id != null ? record.id : findStorageIdForNewRecord( record );

                if ( recordId != null
                    && field != null
                    && String.isNotBlank( message )
                    )
                {
                    if ( ! validationMessagesByRecordAndFieldMap.containsKey( recordId ) )
                    {
                        validationMessagesByRecordAndFieldMap.put( recordId, new map<Schema.sObjectField, set<string>>() );
                    }

                    if ( ! validationMessagesByRecordAndFieldMap.get( recordId ).containsKey( field ) )
                    {
                        validationMessagesByRecordAndFieldMap.get( recordId ).put( field, new set<string>() );
                    }

                    validationMessagesByRecordAndFieldMap.get( recordId ).get( field ).add( message );

                    addMessage( record, message );
                }
            }
        }

        public void addMessage( SObject record, String message )
        {
            if ( record != null )
            {
                id recordId = record.id != null ? record.id : findStorageIdForNewRecord( record );

                if ( recordId != null
                    && String.isNotBlank( message )
                    )
                {
                    if ( ! validationMessagesByRecordMap.containsKey( recordId ) )
                    {
                        validationMessagesByRecordMap.put( recordId, new set<string>() );
                    }

                    validationMessagesByRecordMap.get( recordId ).add( message );

                    if ( ! this.recordsMap.isEmpty()
                        && this.recordsMap.containsKey(recordId))
                    {
                        this.recordsMap.get(recordId).addError( message );
                    }
                }
            }
        }
    }
}