/**
 * mmmarketing_UpdateCallInfoCTMLogic_Test
 * @description Test for Update Call Info CTM Logic class.
 * @author Jeff Watson
 * @date 2/21/2019
 */
@IsTest
public with sharing class mmmarketing_UpdateCallInfoCTMLogic_Test {

    private static Intake__c intake;
    private static Account personAccount;
    private static Marketing_Tracking_Info__c marketingTrackingInfo;

    static {
        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        marketingTrackingInfo = new Marketing_Tracking_Info__c();
        marketingTrackingInfo.Intake__c = intake.Id;
        marketingTrackingInfo.Tracking_Event_Timestamp__c = Datetime.now();
        marketingTrackingInfo.Calling_Phone_Number__c = '5555555555';
        insert marketingTrackingInfo;
    }

    @IsTest
    private static void ctor() {
        mmmarketing_UpdateCallInfoCTMLogic updateCallInfoCTMLogic = new mmmarketing_UpdateCallInfoCTMLogic();
        System.assert(updateCallInfoCTMLogic != null);
    }

    @IsTest
    private static void getAvailableAuthorizations() {
        mmmarketing_UpdateCallInfoCTMLogic updateCallInfoCTMLogic = new mmmarketing_UpdateCallInfoCTMLogic();
        updateCallInfoCTMLogic.getAvailableAuthorizations();
    }

    @IsTest
    private static void coverage() {
        mmmarketing_UpdateCallInfoCTMLogic updateCallInfoCTMLogic = new mmmarketing_UpdateCallInfoCTMLogic();
        updateCallInfoCTMLogic.coverage();
    }
}