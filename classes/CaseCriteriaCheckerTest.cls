@isTest
private class CaseCriteriaCheckerTest {
  static Account acct;
  static Intake__c intake;
  static {
    acct = new Account(Gender__c = 'Male', LastName='Test Account');
    insert acct;
    intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id);
    insert intake;
  }

  private static Case_Qualify_Criteria__c CreateCQC(String reasonType, String col, String op, String arg, String form) {
    Case_Qualify_Criteria__c cqc = new Case_Qualify_Criteria__c(Reason_Type__c = reasonType);
    insert cqc;

    addCriterion(cqc, col, op, arg, form);

    return cqc;
  }
  private static SObjectQualifier.Query addCriterion(Case_Qualify_Criteria__c cqc, String col, String op, String arg, String form) {
    SObjectQualifier.Query crit = new SObjectQualifier.Query();
    crit.column = col;
    crit.operator = op;
    crit.argument = arg;
    crit.formula = form;

    List<SObjectQualifier.Query> criteria;
    if (cqc.Criteria__c != null) {
      criteria = SObjectQualifier.ParseQuery(cqc.Criteria__c);
    } else {
      criteria = new List<SObjectQualifier.Query>();
    }
    criteria.add(crit);

    cqc.Criteria__c = JSON.serialize(criteria);
    update cqc;
    return crit;
  }

  private static List<Id> runCheck() {
    List<Id> ls = new List<Id>();
    ls.add(intake.id);

    return CaseCriteriaChecker.CheckIds(ls);
  }
  @isTest static void CheckIds_NullOrEmptyList_ReturnsEmptyList() {
    List<Id> result = CaseCriteriaChecker.checkIds(null);
    System.assertEquals(0, result.size());

    result = CaseCriteriaChecker.checkIds(new List<Id>());
    System.assertEquals(0, result.size());
  }

  @isTest static void CheckIds_HasTDandUR_ReturnsTDBeforeUR() {
    Case_Qualify_Criteria__c crit1 = CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<', '1/1/2015', null);

    Case_Qualify_Criteria__c crit2 = CreateCQC('TD', 'When_did_you_start_using_the_drug__c', '<', '1/1/2015', null);


    List<Id> failedChecks = runCheck();
    System.assert(failedChecks.size() > 0);
    System.assertEquals(crit2.Id, failedChecks[0]);
  }

  @isTest static void CheckIds_AllCriterionPasses_ReturnsEmptyList() {
    Case_Qualify_Criteria__c crit1 = CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>', '1/1/2015', null);

    Case_Qualify_Criteria__c crit2 = CreateCQC('TD', 'When_did_you_start_using_the_drug__c', '>', '1/1/2015', null);

    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());
  }

  // <
  @isTest static void LessThan_ValueIsLessThan_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<', '1/1/2001', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<', '1/1/2015', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  // <=
  @isTest static void LessThanOrEquals_ValueIsLessThan_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<=', '1/1/2001', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<=', '1/1/2015', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }
  @isTest static void LessThanOrEquals_ValueIsEqual_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<=', '1/1/2001', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<=', '1/1/2010', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  // >=
  @isTest static void GreaterThanOrEquals_ValueIsEqual_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>=', '1/1/2015', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>=', '1/1/2010', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }
  @isTest static void GreaterThanOrEquals_ValueIsGreaterThan_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>=', '1/1/2015', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>=', '1/1/2001', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }
  // >
  @isTest static void GreaterThan_ValueIsGreaterThan_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>', '1/1/2020', null);
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>', '1/1/2010', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '>', '1/1/2001', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }
  // =
  @isTest static void Equals_DateEquals_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '=', '1/1/2001', null);
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '=', '1/1/2020', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '=', '1/1/2010', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void Equals_TextEquals_ReturnsCQC() {
    intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id, Type_of_Injury__c = 'Soft Tissue');
    insert intake;

    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('TD', 'Type_of_Injury__c', '=', 'Soft Tissue', null);

    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void Equals_TextIsDifferentCase_ReturnsCQC() {
    CreateCQC('UR', 'Case_Type__c', '=', 'xaRELto', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void Equals_TextDoesNotEqual_ReturnsEmpty() {
    intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id, Type_of_Injury__c = 'Soft Tissue;Death');
    insert intake;

    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('TD', 'Type_of_Injury__c', '=', 'Soft Tissue', null);

    failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());
  }

  // contains only
  @isTest static void ContainsOnly_OnlyContainsThoseValues_ReturnsCQC() {
    intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id, Type_of_Injury__c = 'Soft Tissue; Brain Injury');
    insert intake;

    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('TD', 'Type_of_Injury__c', 'contains_only', 'Soft Tissue\nDeath\nBrain Injury', null);

    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void ContainsOnly_OnlyContainsThoseValuesDifferentCase_ReturnsCQC() {
    intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id, Type_of_Injury__c = 'Soft Tissue; Brain Injury');
    insert intake;

    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('TD', 'Type_of_Injury__c', 'contains_only', 'Soft tissue\ndeath\nbrain Injury', null);

    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void ContainsOnly_ContainsOtherValues_ReturnsEmpty() {
    intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id, Type_of_Injury__c = 'soft Tissue;brain Injury;death');
    insert intake;

    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('TD', 'Type_of_Injury__c', 'contains_only', 'Soft Tissue\nBrain Injury', null);

    failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());
  }

  // <>
  @isTest static void NotEqual_ValueIsGreaterThan_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<>', '1/1/2010', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<>', '1/1/2020', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }
  @isTest static void NotEqual_ValueIsLessThan_ReturnsCQC() {
    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<>', '1/1/2010', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'When_did_you_start_using_the_drug__c', '<>', '1/1/2001', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  // contains
  @isTest static void Contains_ValueContains_ReturnsCQC() {
    CreateCQC('UR', 'Case_Type__c', 'contains', 'asdf', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'Case_Type__c', 'contains', 'Xarel', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }
  @isTest static void DoesNotContain_ValueDoesNotContain_ReturnsCQC() {
    CreateCQC('UR', 'Case_Type__c', 'does_not_contain', 'Xarel', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(0, failedChecks.size());

    CreateCQC('UR', 'Case_Type__c', 'does_not_contain', 'asdf', null);
    failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void ValueContainsNewline_OptionsAreOred() {
    CreateCQC('UR', 'Case_Type__c', '=', 'Tylenol\nXarelto', null);
    List<Id> failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
  }

  @isTest static void LowestPriorityNotNullWins() {
    Case_Qualify_Criteria__c cqc_null_priority = CreateCQC('UR', 'Case_Type__c', '=', 'Xarelto', null);
    Case_Qualify_Criteria__c cqc_1_priority = CreateCQC('UR', 'Case_Type__c', '=', 'Xarelto', null);
    cqc_1_priority.Priority__c = 1;
    update cqc_1_priority;

    Case_Qualify_Criteria__c cqc_5_priority = CreateCQC('UR', 'Case_Type__c', '=', 'Xarelto', null);
    cqc_5_priority.Priority__c = 5;
    update cqc_5_priority;
    
    List<Id> failedChecks = runCheck();
    System.assertEquals(1, failedChecks.size());
    System.assertEquals(cqc_1_priority.Id, failedChecks[0]);
  }
}