@isTest
public class ErrorLogRetryBatchTest {

    @isTest
    public static void mainTest() {

        Intake__c intake = TestUtil.createIntake();
        insert intake;

        Error_Log__c el = new Error_Log__c();
        el.source__c = 'IntakeConvertFlowQueueable';
        el.status__c = 'Pending';
        el.recordId__c = intake.id;
        el.tries__c = 1;
        insert el;

        ErrorLogRetryBatch elr = new ErrorLogRetryBatch(5);

        elr.execute(null);

        elr.finish(null);

    } //mainTest


} //class