public class mmmarketing_UpdateCallInfoExecutor
    extends mmmarketing_UpdateCallInfoAbstract
{
    public override void performUpdateIfMatch( mmmarketing_IUpdateCallInfoLogic updateLogic, Marketing_Tracking_Info__c record, mmlib_BaseCallout.CalloutResponse listCallsCalloutResponse, String identifierThatFoundMatch ) {
        updateLogic.updateMTICallRecordIfMatch( record, listCallsCalloutResponse, identifierThatFoundMatch, this.uow);
    }
}