/**
 * AuditReportCardSelector
 * @description Selector for Audit Report Card SObject.
 * @author Matt Terrill
 * @date 9/25/2019
 */
public without sharing class AuditReportCardSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public AuditReportCardSelector() {
        sObjectFields = new List<Schema.SObjectField> {
            Audit_Report_Card__c.id,
            Audit_Report_Card__c.Flag_Threshold__c,
            Audit_Report_Card__c.Points_Threshold__c,
            Audit_Report_Card__c.End_Date__c,
            Audit_Report_Card__c.Start_Date__c,
            Audit_Report_Card__c.User__c
        };
    } //constructor


    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Audit_Report_Card__c.SObjectType;
    } //getSObjectType


    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields


    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList


    public List<Audit_Report_Card__c> selectById(Set<Id> ids) {
        return (List<Audit_Report_Card__c>)selectSObjectsById(ids);
    } //selectById


    public Audit_Report_Card__c selectCurrentForUser(Id userId) {
        fflib_QueryFactory query = newQueryFactory();

        Date current = Date.today();

        query.setCondition('user__c = :userId AND start_date__c <= :current AND end_date__c >= :current');

        List<Audit_Report_Card__c> reportCards = (List<Audit_Report_Card__c>)Database.query(query.toSOQL());

        if ( reportCards.size() == 0 ) {
            return null;
        } else {
            return reportCards[0];
        }
    } //selectCurrentForUser


    public List<Audit_Report_Card__c> selectForUsers(Set<Id> userIds) {
        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('user__c IN :userIds');

        return (List<Audit_Report_Card__c>)Database.query(query.toSOQL());
    } //selectForUsers

} //class