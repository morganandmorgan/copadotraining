public with sharing class mmsms_ScheduledSmsLogic
{
	public void removeRecentlySentDuplicates(List<tdc_tsw__Scheduled_Sms__c> newMessageList)
	{
		if (
			(
				mmsms_IntegrationSettings.getIsEnabled() &&
				(!mmlib_Utils.org.isSandbox || mmsms_IntegrationSettings.getIsEnabledInSandbox())
			) ||
			isTest()
		)
		{
			mmsms_IScheduledSmsSelector selector = mmsms_ScheduledSmsSelector.newInstance();
			if (mockSelector != null) {
				selector = mockSelector;
			}

			List<tdc_tsw__Scheduled_Sms__c> scheduledSmsList = selector.getRecentRecords_AllRows(mmsms_IntegrationSettings.getTimeLimit());

			if (1 < newMessageList.size()) {

				Integer focusIndex = 0;

				while (focusIndex < newMessageList.size() - 1) {

					Integer compareToIndex = focusIndex + 1;
					tdc_tsw__Scheduled_Sms__c focus = newMessageList.get(focusIndex);

					while (compareToIndex < newMessageList.size()) {

						tdc_tsw__Scheduled_Sms__c compareTo = newMessageList.get(compareToIndex);

						if (messagesMatch(focus, compareTo)) {
							compareTo.tdc_tsw__SMS_Template__c = null;
							break;
						}

						compareToIndex++;
					}

					focusIndex++;
				}
			}

			for (tdc_tsw__Scheduled_Sms__c newMessage : newMessageList) {

				if (newMessage.tdc_tsw__SMS_Template__c == null || String.isEmpty(newMessage.tdc_tsw__Phone_Api__c)) {
					continue;
				}

				for (tdc_tsw__Scheduled_Sms__c recentMessage : scheduledSmsList) {

					if (messagesMatch(recentMessage, newMessage)) {
						newMessage.tdc_tsw__SMS_Template__c = null;
						break;
					}
				}
			}
		}
		else {
			throw new mmsms_SmsIntegrationExceptions.ExecutionException('The integration is disabled via custom settings.');
		}
	}

	private Boolean messagesMatch(tdc_tsw__Scheduled_Sms__c recentMessage, tdc_tsw__Scheduled_Sms__c newMessage)
	{

		Boolean result = true;

		String recentPhoneNumberString = mmlib_Utils.stripPhoneNumber(recentMessage.tdc_tsw__Phone_Api__c);
		String newPhoneNumberString = mmlib_Utils.stripPhoneNumber(newMessage.tdc_tsw__Phone_Api__c);

		if (!recentPhoneNumberString.equals(newPhoneNumberString))
		{
			result = false;
		}

		if (recentMessage.tdc_tsw__SMS_Template__c == null && newMessage.tdc_tsw__SMS_Template__c != null)
		{
			result = false;
		}

		if (recentMessage.tdc_tsw__SMS_Template__c != newMessage.tdc_tsw__SMS_Template__c)
		{
			result = false;
		}

		return result;
	}

	// =========== Testing Context ==============
	private Boolean isTest()
	{
		return isTestMode || mockSelector != null;
	}

	@TestVisible
	private Boolean isTestMode {get; set;} {isTestMode = false;}

	@TestVisible
	private mmsms_IScheduledSmsSelector mockSelector {get; set;}
}