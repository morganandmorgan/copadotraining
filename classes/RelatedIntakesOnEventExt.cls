public with sharing class RelatedIntakesOnEventExt {

    private final ApexPages.StandardController stdController;

    public List<Intake__c> RelatedIntakes { get; private set; }

    public RelatedIntakesOnEventExt(ApexPages.StandardController stdControllerParam) {
        stdController = stdControllerParam;
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{ 'WhatId' });
        }
        Event event = (Event) stdController.getRecord();

        RelatedIntakes = new List<Intake__c>();

        List<IncidentInvestigationEvent__c> incidentInvestigations = [
                SELECT Incident__c 
                FROM IncidentInvestigationEvent__c 
                WHERE Id = :event.WhatId
            ];

        if (!incidentInvestigations.isEmpty()) {
            IncidentInvestigationEvent__c incidentInvestigation = incidentInvestigations.get(0);

            RelatedIntakes = [
                    SELECT Name, Client__r.Id, Client__r.Name, Case_Type__c, Injured_Party__r.Id, Injured_Party__r.Name
                    FROM Intake__c 
                    WHERE Incident__c = :incidentInvestigation.Incident__c
                        AND Id IN (
                            SELECT Intake__c
                            FROM IntakeInvestigationEvent__c
                            WHERE IncidentInvestigation__c = :incidentInvestigation.Id
                        )
                ];
        }
    }

    public Boolean getHasRelatedIntakes() {
        return RelatedIntakes.size() > 0;
    }
}