/*============================================================================
Name            : BatchManagerController_TEST
Author          : CLD
Created Date    : July 2018
Description     : Test class for Batch Manager Page
=============================================================================*/
@isTest (seeAllData=true)
public class BatchManagerController_TEST {
    @isTest
    public static void testFinanceBatchManager(){
        BatchManagerController testController = new BatchManagerController();
        testController.selectedBatchName = 'FFARevenueScheduleJournal_Batch';
        testController.processJob();
        testController.addPageErrorMessage('test error message');
        testController.addPageSuccessMessage('test success message');
        testController.reloadPendingJobs();
        testController.evalParamsAndReloadJobs();
        BatchManagerController.BatchJob bjob = new BatchManagerController.BatchJob();
        bjob.createdDate = date.today();
        bjob.jobItemsProcessed = 1;
        bjob.totalJobItems = 1;
        bjob.numberOfErrors = 0;
        bjob.createdDateFmt = '';
        system.debug(bjob.createdDateFmt);
        bjob.generateMessage = '';
        system.debug(bjob.generateMessage);
    }
}