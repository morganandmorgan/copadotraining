public with sharing class MatterInformationRequestsDataController {
	@AuraEnabled 
	public static String saveRecord(litify_pm__Request__c recordObj){
		try{
			upsert recordObj;
		}
		catch( Exception ex){
			throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage()+ '\n' + ex.getStackTraceString());
		}
		return recordObj.Id;
	}

	@AuraEnabled 
	public static void deleteRecord(litify_pm__Request__c recordObj){
		if (Schema.sObjectType.litify_pm__Request__c.isDeletable()){				
			try{
				delete recordObj;
			}
			catch( Exception ex){
				throw new AuraHandledException(ex.getMessage() + '\n' + ex.getStackTraceString() + '\n' + ex.getMessage()+ '\n' + ex.getStackTraceString());
			}
		}
		else{
			throw new AuraHandledException('It seems like you are not Authorized to do that.'+ '\n' + 'Please Contact Your Administrator');
		}	
		
	}
}