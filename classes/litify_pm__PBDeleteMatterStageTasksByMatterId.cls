/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBDeleteMatterStageTasksByMatterId {
    global PBDeleteMatterStageTasksByMatterId() {

    }
    @InvocableMethod(label='Delete Tasks on Matter Stage' description='Takes matter id to find the current matter stage activity and delete its tasks')
    global static void deleteTasksFromMatterStageActivity(List<litify_pm.PBDeleteMatterStageTasksByMatterId.ProcessBuilderMatterStageWrapper> matterWithStage) {

    }
global class ProcessBuilderMatterStageWrapper {
    @InvocableVariable( required=false)
    global List<Id> matterIds;
    global ProcessBuilderMatterStageWrapper() {

    }
}
}
