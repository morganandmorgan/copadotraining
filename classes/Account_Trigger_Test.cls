/**
 * Account_Trigger_Test
 * @description Test for Account Trigger.
 * @author Jeff Watson
 * @date 5/15/2019
 */
@isTest
public with sharing class Account_Trigger_Test {

    @isTest
    private static void accountTrigger() {
        Test.startTest();
        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;
        Test.stopTest();
    }
}