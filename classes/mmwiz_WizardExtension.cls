public with sharing class mmwiz_WizardExtension
{
    @RemoteAction
    public static mmwiz_QuestionnaireModel getQuestionnaire( String questionnaireToken, String urlOfPage, String sessionGuid )
    {
        return mmwiz_QuestionnaireService.findQuestionnaire( questionnaireToken, convertToPageRef( urlOfPage ), sessionGuid );
    }

    @RemoteAction
    public static list<mmwiz_AbstractActionModel> processPageResponses( String serializedQuestionResponseMap, String urlOfPage, String sessionGuid )
    {
        system.debug( serializedQuestionResponseMap );

        list<mmwiz_AbstractActionModel> responseActions = new list<mmwiz_AbstractActionModel>();

        map<string, map<string, map<string, list<string>>>> questionResponseMap = (map<string, map<string, map<string, list<string>>>>) JSON.deserialize( serializedQuestionResponseMap, map<string, map<string, map<string, list<string>>>>.class );

        for ( string questionnaireKey : questionResponseMap.keySet() )
        {
            string questionnaireToken = questionnaireKey;

            //system.debug( 'questionnaireToken = ' + questionnaireToken );

            for ( string pageKey : questionResponseMap.get(questionnaireToken).keySet() )
            {
                string pageToken = pageKey;

                // call the service to determine what should be the response.
                responseActions.addAll( mmwiz_QuestionnaireService.processResponseActions( questionnaireToken, pageToken, questionResponseMap.get(questionnaireToken).get(pageToken), convertToPageRef( urlOfPage ), sessionGuid  ) );
            }
        }

        system.debug( responseActions);

        return responseActions;
    }

    private static PageReference convertToPageRef( String urlOfPage )
    {
        PageReference pageRef = null;

        if ( String.isNotBlank( urlOfPage ))
        {
            pageRef = new PageReference(urlOfPage);
        }

        return pageRef;
    }
}