public with sharing class DocsApi {

    @TestVisible
    private class DocumentAttachment {
        public Blob body;
        public String fileType;
        public Id parentId;
        public String uniqueName;

        public DocumentAttachment(Id recordId, String title, Id parentId, String fileType, Blob body) {
            this.body = body;
            this.fileType = fileType;
            this.parentId = parentId;
            this.setUniqueName(recordId, title);
        }

        private void setUniqueName(Id recordId, String title) {
            String name = String.valueOf(recordId).left(15);
            this.uniqueName = title + ' - ' + name.right(3);
        }
    }

	public class MergeData {
        public Id relatedToId {get;set;}
        public String relatedToApiName {get;set;}
        public String category {get;set;}
        public String subcategory {get;set;}
        public String fileType {get;set;}
        public String name {get;set;}
        public Id templateId {get;set;}
        
        public Map<String,Object> tags {get;set;}
    } //class MergeData
    
    public static String defaultFileType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    
    //Everything is static to support the @future
    private static String baseUrl = 'https://2xm5rfl2q0.execute-api.us-east-2.amazonaws.com/v1';

    private static String orgInfoUrl;

    private static List<litify_pm__Matter__c> matters;
    private static Map<Id,Id> intakeMatterMap;
    private static List<DocumentAttachment> attachments;
    private static Map<String, DocumentAttachment> uniqueNameAttachmentMap;
    private static Map<String,Object> litifyDocMap;

    @TestVisible
    private static Set<Id> testFileInfoIds;

    //public in case we ever want to change it
    public static String sessionId = UserInfo.getSessionId();

    /* ************** */
    /* Public Methods */
    /* ************** */

    @future(callout=true)
    public static void migrateIntakeDocuments(Set<Id> matterIds) {
        Set<Id> intakeIds = getMigrationIntakes(matterIds);
        if (intakeIds == null) {
            return;
        }

        addIntakeAttachments(intakeIds);
        addIntakeContentDocuments(intakeIds);
        finalizeMigration();
    } //migrateIntakeDocuments

    public static void migrateContentDocuments(Set<Id> matterIds) {
        Set<Id> intakeIds = getMigrationIntakes(matterIds);
        if (intakeIds == null) {
            return;
        }

        addIntakeContentDocuments(intakeIds);
        finalizeMigration();
    } //migrateContentDocuments

    public static List<Id> multiMerge(List<MergeData> mergeData) {

        getOrgInfoUrl();
        List<Id> recordIds = litifyMerge(mergeData);
        mergeComplete(recordIds);

        return recordIds;
    } //multiMerge

    /* ****************** */
    /* Supporting methods */
    /* ****************** */

    private static void addIntakeAttachments(Set<Id> intakeIds) {
        if (attachments == null) {
            attachments = new List<DocumentAttachment>();
        }

        for (Attachment attch :  [
            SELECT Id, Name, ParentId, ContentType, Body 
            FROM Attachment 
            WHERE ParentId IN :intakeIds
        ]) {
            String contentType = String.isBlank(attch.ContentType) ? 'application/pdf' : attch.ContentType;

            attachments.add(
                new DocumentAttachment(attch.Id, attch.Name, attch.ParentId, contentType, attch.Body)
            );
        }
    }

    private static void addIntakeContentDocuments(Set<Id> intakeIds) {
        if (attachments == null) {
            attachments = new List<DocumentAttachment>();
        }

        for (ContentDocumentLink link : [
            SELECT
                ContentDocumentId,
                ContentDocument.FileType, 
                ContentDocument.LatestPublishedVersion.VersionData,
                ContentDocument.Title, 
                LinkedEntityId
            FROM ContentDocumentLink 
            WHERE LinkedEntityId IN :intakeIds
        ]) {
            attachments.add(
                new DocumentAttachment(
                    link.ContentDocumentId,
                    link.ContentDocument.Title, 
                    link.LinkedEntityId, 
                    link.ContentDocument.FileType, 
                    link.ContentDocument.LatestPublishedVersion.VersionData
                )
            );
        }
    }

    private static void finalizeMigration() {
        if (!attachments.isEmpty()) {
            getOrgInfoUrl();
            litifyPost();
            storeFiles();
            filesComplete();
        }
    }

    private static Set<Id> getMigrationIntakes(Set<Id> matterIds) { 
        mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();
        
        if ( !Test.isRunningTest()) {
            matters = matterSelector.selectById(matterIds);
        } else if (matters == null)  {
            return null; //if this isn't injected, we probably aren't testing this method, so get out of here so we don't break other tests
        }
        
        intakeMatterMap = new Map<Id,Id>();
        Set<Id> intakeIds = new Set<Id>();
        for (litify_pm__Matter__c matter : matters) {
            intakeIds.add(matter.intake__c);
            intakeMatterMap.put(matter.intake__c, matter.id);
        }

        return intakeIds;
    }

    private static String getOrgInfoUrl() {
        if (orgInfoUrl == null) {
            HttpRequest setupOrgReq = new HttpRequest();
            setupOrgReq.setHeader('Authorization', 'Bearer '+sessionId);
            setupOrgReq.setHeader('Content-Type', 'application/json');
            setupOrgReq.setTimeout(120000);
            setupOrgReq.setEndpoint(baseUrl+'/orginfo');
            setupOrgReq.setMethod('GET');

            Http http1 = new Http();

            if ( !Test.isRunningTest()) {
                HTTPResponse resOrgInfo = http1.send(setupOrgReq);
                Map<String,Object> resOrgInfoMap = (Map<String,Object>)JSON.deserializeUntyped(resOrgInfo.getBody());
                orgInfoUrl = (String)resOrgInfoMap.get('AwsApiPrefix');
                System.debug('getOrgInfoUrl()');
                System.debug( UserInfo.getUserName() );
                System.debug( resOrgInfo );
            } else {
                orgInfoUrl = 'testing';
            }
        }
        return orgInfoUrl;
    } //getOrgInfoUrl

    //Post file information to litify
    private static void litifyPost() {
        //Get the signed documents
        List<Object> files = new List<Object>();
        uniqueNameAttachmentMap = new Map<String, DocumentAttachment>();
        for(DocumentAttachment attachment: attachments){
            String uniqueName = attachment.uniqueName;

            Map<String, Object> fileInfo = new Map<String, Object>();
            fileInfo.put('Name', uniqueName);
            fileInfo.put('litify_docs__Related_To__c', intakeMatterMap.get(attachment.parentId) );
            fileInfo.put('litify_docs__Related_To_Api_Name__c', 'litify_pm__Matter__c');
            fileInfo.put('litify_docs__Document_Category__c', 'Intake Documents');
            fileInfo.put('litify_docs__File_Type__c', attachment.fileType);

            files.add(fileInfo);
            uniqueNameAttachmentMap.put(uniqueName, attachment);
        } //for attachments


        String postBody = JSON.serialize(files);

        //make POST to /file
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+sessionId);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody(postBody);
        req.setEndpoint(orgInfoUrl+'/files'); //TBD with named credential
        req.setMethod('POST');

        if ( !Test.isRunningTest()) {
            Http http = new Http();
            HTTPResponse res = http.send(req);
            litifyDocMap = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
            //System.debug('litifyPost()');
            //System.debug(res);
        }
        // else we should have injected test data into litifyDocMap via the testMock method

    } //litifyPost

    //Send files to AWS
    private static void storeFiles() {
        //System.debug('storeFiles()');
        
        for (String mKey : litifyDocMap.keySet()) {
            Map<String,Object> fileMap = (Map<String,Object>)litifyDocMap.get(mKey);

            DocumentAttachment attachment = uniqueNameAttachmentMap.get(mKey);
            if (attachment == null) {
                continue;
            }

            Blob attachmentBody = attachment.body;

            //put file in S3 bucket
            HttpRequest s3Req = new HttpRequest();
            s3Req.setTimeout(120000);
            //send S3 file type (e.g. application/pdf)
            s3Req.setHeader('Content-Type', attachment.fileType);
            
            //s3Req.setBody(EncodingUtil.base64Encode(attachmentBody));
            s3Req.setBodyAsBlob( attachmentBody );
            
            s3Req.setEndpoint((String)fileMap.get('SignedUrl'));
            s3Req.setMethod('PUT');
            Http http2 = new Http();
        
            if (!Test.isRunningTest()) {
                HTTPResponse s3Res = http2.send(s3Req);
                //System.debug(s3Res);
            }
        }
    } //storeFiles

    //Call files/complete in litify
    private static void filesComplete() {
        Set<Id> fileInfoIds = new Set<Id>();

        for (String mKey : litifyDocMap.keySet()) {
            Map<String,Object> fileMap = (Map<String,Object>)litifyDocMap.get(mKey);
            fileInfoIds.add((String)fileMap.get('Id'));
        }

        Map<String,Object> filesInfoMap = new Map<String,Object>();
        filesInfoMap.put('Ids',fileInfoIds);

        //call /file/complete
        HttpRequest req3 = new HttpRequest();
        req3.setHeader('Authorization', 'Bearer '+sessionId);
        req3.setHeader('Content-Type', 'application/json');
        req3.setTimeout(120000);
        req3.setBody(JSON.serialize(filesInfoMap));
        req3.setEndpoint(orgInfoUrl+'/files/complete');
        req3.setMethod('POST');

        Http http3 = new Http();

        if (!Test.isRunningTest()) {
            HTTPResponse res3 = http3.send(req3);
            //System.debug('filesComplete()');
            //System.debug(res3);
        } else {
            testFileInfoIds = fileInfoIds;
        }
    } //filesComplete

    //we need to look up the 'source' File_Info of the template and build a map, mapping <templateId, SourceId>
    private static Map<Id,Id> buildSourceMap(List<MergeData> mergeData) {
        //get a unique set of template ids
        Set<Id> templateIds = new Set<Id>();
        for (MergeData md : mergeData) {
            templateIds.add(md.templateId);
        } //for mergeData
        
        Map<Id,Id> sourceMap = new Map<Id,Id>();
        for (litify_docs__File_Info__c fi : [SELECT id, litify_docs__Related_To__c FROM litify_docs__File_Info__c WHERE litify_docs__Related_To__c IN :templateIds]) {
			sourceMap.put(fi.litify_docs__Related_To__c, fi.id);
        }
        return sourceMap;
    } //buildSourceMap

    public static List<Id> litifyMerge(List<MergeData> mergeData) {

        Map<Id,Id> sourceMap = buildSourceMap(mergeData);
        
        List<Object> templateArray = new List<Object>();
        
        for (MergeData md : mergeData) {
            Map<String,Object> template = new Map<String,Object>();
    
            template.put('SourceId', sourceMap.get(md.templateId));
    
            template.put('Tags', md.tags);
            
            template.put('litify_docs__Related_To__c', md.relatedToId);
            template.put('litify_docs__Related_To_Api_Name__c', md.relatedToApiName);
            template.put('litify_docs__Document_Category__c', md.category);
            template.put('litify_docs__Document_Subcategory__c', md.subcategory);
            template.put('litify_docs__File_Type__c', md.fileType);
            template.put('Name', md.name);
            template.put('litify_docs__Origin_Merge_Template__c', md.templateId);
        
        	templateArray.add(template);
        } //for mergeData

        Map<String,Object> container = new Map<String,Object>();
        container.put('Templates',templateArray);

        String postBody = JSON.serialize( container );
        System.debug( postBody );

       	//make POST to /merge

        String jsResponse;
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+sessionId);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody(postBody);
        req.setEndpoint(orgInfoUrl+'/merge');
        req.setMethod('POST');

        Http http = new Http();

        if ( !Test.isRunningTest()) {
            HTTPResponse res = http.send(req);
            jsResponse = res.getBody();
            System.debug( jsResponse );
        } else {
            jsResponse = '{"Records": [{"Id": "aaa10000001abc1AAA","litify_docs__Related_To__c": "bbb10000001abc1BBB","litify_docs__Document_Subcategory__c": "Docs","litify_docs__Document_Category__c": "Emails","litify_docs__File_Type__c": "application/vnd.openxmlformats-officedocument.wordprocessingml.document","litify_docs__Merge_Id__c": "0","Name": "test.docx"}]}';
        }

        Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(jsResponse);
        List<Object> records = (List<Object>)response.get('Records');

        List<Id> recordIds = new List<Id>();
        for (Object robject : records) {
            Map<String,Object> record = (Map<String,Object>)robject;
            Id recordId = (Id)record.get('Id');
            recordIds.add(recordId);
        }

        return recordIds;
        
    } //litifyMerge


    public static void mergeComplete(List<String> recordIds) {
        Map<String,Object> container = new Map<String,Object>();
        container.put('Ids',recordIds);

        String postBody = JSON.serialize(container);
        
        System.debug(postBody);

        //post to /merge/complete
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+sessionId);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody(postBody);
        req.setEndpoint(orgInfoUrl+'/merge/complete');
        req.setMethod('POST');

        Http http = new Http();
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);
            System.debug(res.getBody());
        }

    } //mergeComplete

    
    @TestVisible //for injecting matters to test with and mock up all responses
    private static void testMock(List<litify_pm__Matter__c> testMatters, String attachmentName, Id documentId) {
        matters = testMatters;
        
        litifyDocMap = new Map<String,Object>();
        
        Map<String,Object> fileMap = new Map<String,Object>();
        fileMap.put('SignedUrl','testUrl');
        fileMap.put('Id', documentId);
        
        litifyDocMap.put(attachmentName, fileMap);
    } //testMock
    
    
} //class