public class PaymentProcessPostDriver {

    private PaymentProcessingStatusDataAccess statusData=new PaymentProcessingStatusDataAccess();


    //TODO:  Factor the status-tracking functionality out into its own class.  
	
    public Boolean launchPostAndMatchOperations(Id paymentCollectionId){

        Boolean noErrorSoFarYall=true;
        PaymentProcessingStatusUpdater statusUpdater=null;
        statusUpdater=new PaymentProcessingStatusUpdater();

        //statusUpdater.updatePostAndMatchOperationStatuses(paymentCollectionId);	//Sync the tracking records before launching.
        
        System.debug('launchPostAndMatchOperations() paymentCollectionId:  ' + paymentCollectionId);

        Integer paymentCount = 0;
        Integer holdingJobsCount= 0;  //count of jobs in the flex queue.  Only 100 are permitted.

        BatchProcessHelper batchHelper=null;
        batchHelper = new BatchProcessHelper();
        
        holdingJobsCount=batchHelper.getHoldingBatchJobCount();
        
        //Get any tracking records that are still pending for the payments in the collection.
        //Launch any batches that need to be launched (haven't been launched or no longer running).
        //TODO:  Factor this into an IMPL method:  
        {
            
            //Status check/status polling for batch processes.
            List<Payment_Processing_Batch_Status__c> trackingRecords=null;
            
            trackingRecords=statusData.getPendingPostAndMatchTrackingRecords(paymentCollectionId);

            paymentCount=trackingRecords.size();

            System.debug('    paymentCount:  ' + paymentCount);
            System.debug('holdingJobsCount:  ' + holdingJobsCount);

            if( paymentCount < (100 - holdingJobsCount - 1) ){
                //We're good:  we can run all the jobs + an updater job.
                System.debug('There is room in the Apex batch queue.');
            }else{
                System.debug('Not enough room in the Apex batch queue.  Have to schedule a bootstrapper.');
            }
            
            //Collect any Batch process records:  
            List<Id> apexJobIds=null;        
            apexJobIds=new List<Id>(); 
            
            Map<Id, Id> jobIDsByBatchStatusId=null;
            Map<Id, AsyncApexJob> jobsByBatchStatusId=null;
            Map<Id, AsyncApexJob> jobsByJobId=null;
            
            jobIDsByBatchStatusId=new Map<Id, Id>();
            jobsByJobId=new Map<Id, AsyncApexJob>();
            jobsByBatchStatusId=new Map<Id, AsyncApexJob>();
            
            for(Payment_Processing_Batch_Status__c tracker: trackingRecords){
                //(Can't have a lookup relationship to an Async Apex Job record, sadly enough.)
                //TODO:  Look at the Batch Id fields on Payment object.
                if(tracker.Post_Job_Id__c!=null){
                    apexJobIds.add(tracker.Post_Job_Id__c);
                    jobIDsByBatchStatusId.put(tracker.Id, tracker.Post_Job_Id__c);
                }
            }
            
            //Get Batch job tracking records, if they still exist...
            //Get a job:  
            List<AsyncApexJob> apexBatchJobs=null;
            apexBatchJobs=statusData.getRunningPostAndMatchApexJobs(apexJobIds);
            
            for(AsyncApexJob job: apexBatchJobs){
                jobsByJobId.put(job.Id, job);
            }
            
            List<Payment_Processing_Batch_Status__c> trackingRecordsToUpdate=null;
            
            trackingRecordsToUpdate=new List<Payment_Processing_Batch_Status__c>();
            
            for(Payment_Processing_Batch_Status__c tracker: trackingRecords){
                Boolean previousJobIsRunning=false;
                Boolean done=false;
                
                Id paymentId = null;
                paymentId = tracker.Payment__c;                
                
                Id apexBatchJobId=null;
                AsyncApexJob apexJob=null;
                //Get a job:
                if(tracker.Post_Job_Id__c!=null){
                    apexBatchJobId = (Id) tracker.Post_Job_Id__c; //casting error?
                    if(jobsByJobId.containsKey(apexBatchJobId)){
                        apexJob=jobsByJobId.get(apexBatchJobId);
                        jobsByBatchStatusId.put(tracker.Id, apexJob);
                    }                
                }            
                
                //Posted
                //Matched
                Boolean updateStatus=false;
                if(tracker.Payment__r.c2g__Status__c=='Posted'){
                    //Weird intermediate state - maybe the batch is still running
                    //Posted but not Matched: 
                    System.debug('Payment ' + paymentId + ' Posted but not Matched.');
                }
                
                if(tracker.Payment__r.c2g__Status__c=='Matched'){
                    done=true;
                }            
                
                if(!done){
                    
                    if(tracker.Payment__r.c2g__Status__c!='Matched'){
                        //Some other problem or delay:  Look at the Apex Batch Job if there is one:  
                        if(apexJob!=null){
                            if((apexJob.Status=='Completed')||(apexJob.Status=='Failed')||(apexJob.Status=='Aborted')){
                                previousJobIsRunning=false;
                                updateStatus=true;                        
                            } else {
                                previousJobIsRunning=true;
                                done=true;
                                //updateStatus=true;
                            }
                        }                        
                        
                        if((apexJob==null)||(!previousJobIsRunning)){
                            //TODO:  Run it, store the batch Id on the tracking record:  
                            updateStatus=true;
                            
							{	//Run the Post and Match Batch:                                  
                
                                //This may throw an error if there the flex queue is full: 
                                   
                                //Run it:
                                //TODO:  catch errors.  postAndMatchAsync() can throw before launching the batch.  Set noErrorSoFarYall=false; if we have any problems.
                                Id batchProcessId = null;	//AsyncApexJob
                                
                                try{

                                    // {   //old direct way:  
                                    //     System.debug('Launching postAndMatchAsync()...  ' + paymentId);
                                    //     //NOTE:  If successful, this calls Database.execute() and goes asynchronous:
                                    //     //Launches a PaymentsPlusPostAndMatchBatch instance:  
                                    //     batchProcessId = c2g.PaymentsPlusService.postAndMatchAsync(paymentId);
                                        
                                    //     tracker.Post_Job_Id__c = batchProcessId;
                                    //     tracker.Message__c = 'Running Post and Match...';
                                        
                                    //     //TODO:  Store the batch Id on the processing record.
                                    //     System.debug('batchProcessId:  ' + batchProcessId);
                                    //     updateStatus=true;
                                    // }

                                    {   //new way:  launch a batch to launch a batch, so each batch has its own soql limits context:  
                                        System.debug('Launching postAndMatchAsync()...  ' + paymentId);

                                        PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;

                                        batchProcessor=new PaymentProcessingLaunchPostAndMatchBatch(paymentId, tracker);

                                        batchProcessId = Database.executeBatch(batchProcessor);
                                        System.debug('launcher batchProcessId:  ' + batchProcessId);
                                        updateStatus=false; //let the launcher-batch update the status record we passed in...
                                    }                                    

                                }catch(System.AsyncException ae){
                                    //too many concurrent batch jobs:
                                    //TODO:  schedule a launcher in the future, if possible:  
                                    handleAsyncException(ae, tracker);

                                    updateStatus=true;                                    
                                    noErrorSoFarYall=false;                                    
                                }                                
                                                           
                                /*    
                                {                                  
                                    
                                    //Schedule a bootstrapper batch to run whatever remains:  

                                    /*
                                    
                                    /*
                                    //schedule this operation in batch form again in 5 mins
                                    LaunchPostAndMatchBatch batch = new LaunchPostAndMatchBatch();
                                    Datetime scheduleDate = Datetime.now().addMinutes(5);  // 
                                    String chronExpression = scheduleDate.format('s m H d M \'?\' yyyy');
                                    Id schedId = System.Schedule('PostAndMatch - '+chronExpression, chronExpression, batch);
                                    */

                                    /*
                                                                        
                                }   
                                */             
                            
                            }                            
                            
                        }
                        
                    }
                    
                }
                
                if(updateStatus){
                    trackingRecordsToUpdate.add(tracker);
                }
                
            }
            
            if(trackingRecordsToUpdate.size()>0){
                update trackingRecordsToUpdate;
            }
        }                
        
        //statusUpdater.updatePostAndMatchOperationStatuses(paymentCollectionId);	//Sync the tracking records immediately after launching.  Some may have finished.
        
        return noErrorSoFarYall;
    }
    
    @TestVisible
    private void handleAsyncException(System.AsyncException ae, Payment_Processing_Batch_Status__c tracker){
        //too many concurrent batch jobs:
        //TODO:  schedule a launcher in the future, if possible:  
        System.debug(ae.getMessage());
        System.debug('Post and Match:  Too many concurrent batch jobs');
        
        tracker.Message__c = 'Post and Match:  Too many concurrent batch jobs';
        tracker.Last_Error_Message__c = 'Post and Match:  Too many concurrent batch jobs';
        tracker.Post_and_Match_Last_Error_Message__c = 'Too many concurrent batch jobs';
        tracker.Post_and_Match_Last_Error_Message__c = 'AsyncException:  ' + ae.getMessage();
        tracker.Is_Posted_and_Matched__c=false;
        tracker.Has_Error__c = true;
        tracker.Has_Post_and_Match_Error__c = true;
                                    
    }     
    
  
    
}