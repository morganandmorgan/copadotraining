@IsTest
public with sharing class mmdice_QueueControllerTest {

    private static Dice_Queue__c diceQueue;
    private static Account personAccount;
    private static Intake__c intake;

    static {
        diceQueue = TestUtil.createDiceQueue();
        insert diceQueue;

        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        intake = TestUtil.createIntake(personAccount);
        insert intake;
    }

    public static testMethod void mmdice_QueueController_GetModel() {

        // Arrange + Act
        Test.startTest();
        mmdice_QueueController.Model model = mmdice_QueueController.getModel(diceQueue.Id);
        Test.stopTest();

        // Assert
        System.assert(model != null);
    }

    public static testMethod void mmdice_QueueController_Model_Ctor() {

        // Arrange + Act
        Test.startTest();
        mmdice_QueueController.Model model = new mmdice_QueueController.Model();
        Test.stopTest();

        // Assert
        System.assert(model != null);
    }

    public static testMethod void mmdice_QueueController_Model_Override_Ctor() {

        // Arrange
        List<Intake__c> intakeSObjs = new List<Intake__c> {intake};
        List<Intake__c> suppressedIntakeSObjs = new List<Intake__c> {intake};
        String debugQuery = 'debug query';

        // Act
        Test.startTest();
        mmdice_QueueController.Model model = new mmdice_QueueController.Model(intakeSObjs, suppressedIntakeSObjs, debugQuery);
        Test.stopTest();

        // Assert
        System.assert(model != null);
    }
}