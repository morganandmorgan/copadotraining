public class mmlitifylrn_Logic
{
    public static string processCalloutErrorMessage( HttpResponse httpResponse )
    {
        string output = null;

        if ( httpResponse != null )
        {
            try
            {
                map<string, Object> errorResponseMap = (map<string, Object>) json.deserializeUntyped( httpResponse.getBody() );
                system.debug( errorResponseMap );
                system.debug( errorResponseMap.get('error_message') );
                output = (String)errorResponseMap.get('error_message');
            }
            catch (Exception e)
            {
                system.debug(System.LoggingLevel.WARN, 'Exception while trying to deserializeUntyed the response body --- \n' + httpResponse.getBody() + '\n\n\n');
            }
        }

        return output;
    }
}