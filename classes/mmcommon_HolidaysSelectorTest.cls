@isTest
private class mmcommon_HolidaysSelectorTest {

  @isTest
  private static void selectExistingHolidays() {
    List<Holiday> holidaysToQuery = new List<Holiday>{
      TestUtil.createHoliday('Holiday A', Date.today()),
      TestUtil.createHoliday('Holiday B', Date.today())
    };
    Database.insert(holidaysToQuery);

    List<Holiday> otherHolidays = new List<Holiday>{
      TestUtil.createHoliday('Holiday C', Date.today()),
      TestUtil.createHoliday('Holiday D', Date.today())
    };
    Database.insert(otherHolidays);

    Set<Id> queryIds = new Map<Id, Holiday>(holidaysToQuery).keySet();

    Test.startTest();
    List<Holiday> result = mmcommon_HolidaysSelector.newInstance().selectById(queryIds);
    Test.stopTest();

    Map<Id, Holiday> resultMap = new Map<Id, Holiday>(result);
    System.assertEquals(queryIds, resultMap.keySet());
  }

  @isTest
  private static void selectFutureHolidays() {
    List<Holiday> holidaysToQuery = new List<Holiday>{
      TestUtil.createHoliday('Holiday A', Date.today()),
      TestUtil.createHoliday('Holiday B', Date.today().addDays(20)),
      TestUtil.createHoliday('Holiday C', Date.today().addYears(1))
    };
    Database.insert(holidaysToQuery);

    List<Holiday> otherHolidays = new List<Holiday>{
      TestUtil.createHoliday('Holiday D', Date.today().addDays(-1)),
      TestUtil.createHoliday('Holiday E', Date.today().addYears(-1))
    };
    Database.insert(otherHolidays);

    Set<Id> queryIds = new Map<Id, Holiday>(holidaysToQuery).keySet();
    Set<Id> otherHolidayIds = new Map<Id, Holiday>(otherHolidays).keySet();

    Test.startTest();
    List<Holiday> result = mmcommon_HolidaysSelector.newInstance().selectAllFuture();
    Test.stopTest();

    Map<Id, Holiday> resultMap = new Map<Id, Holiday>(result);
    System.assert(resultMap.keySet().containsAll(queryIds));
    System.assert(!resultMap.keySet().clone().removeAll(otherHolidayIds));
  }
}