/**
 * MatterSync_Service_Test
 * @description Test for Matter Sync Service class.
 * @author Jeff Watson
 * @date 3/23/2019
 */
@IsTest
public with sharing class MatterSync_Service_Test {

    private static User user;
    private static Account account;
    private static litify_pm__Matter__c matter;
    private static litify_pm__Matter_Team_Role__c matterTeamRole;

    static {
        user = TestUtil.createUser();
        insert user;

        account = TestUtil.createPersonAccount('Jimmy', 'Buffett');
        insert account;

        matter = TestUtil.createMatter(account);
        insert matter;

        matterTeamRole = TestUtil.createMatterTeamMemberRole();
        insert matterTeamRole;
    }

    @IsTest
    private static void ctor() {
        MatterSync_Service matterSyncService = new MatterSync_Service();
        System.assert(matterSyncService != null, 'Target class should not be null');
    }

    @IsTest
    private static void updateRoleUser() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        MatterSync_Service.updateRoleUser(new List<litify_pm__Matter_Team_Member__c> {matterTeamMember});
    }

    @IsTest
    private static void deleteRoleUser() {
        litify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, matterTeamRole);
        MatterSync_Service.deleteRoleUser(new List<litify_pm__Matter_Team_Member__c> {matterTeamMember});
    }

    @IsTest
    private static void coverage() {
        MatterSync_Service.coverage();
    }
}