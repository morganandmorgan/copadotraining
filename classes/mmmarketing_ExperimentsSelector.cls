/**
 *  mmmarketing_ExperimentsSelector
 */
public class mmmarketing_ExperimentsSelector
    extends mmlib_SObjectSelector
    implements mmmarketing_IExperimentsSelector
{
    public static mmmarketing_IExperimentsSelector newInstance()
    {
        return (mmmarketing_IExperimentsSelector) mm_Application.Selector.newInstance(Marketing_Experiment__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Marketing_Experiment__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Marketing_Experiment__c.Description__c,
                Marketing_Experiment__c.End_Date__c,
                Marketing_Experiment__c.Experiment_Name__c,
                Marketing_Experiment__c.Optimizely_Id__c,
                Marketing_Experiment__c.Start_Date__c
            };
    }

    public override String getOrderBy()
    {
        return Marketing_Experiment__c.Experiment_Name__c.getDescribe().getName();
    }

    public List<Marketing_Experiment__c> selectById(Set<Id> idSet)
    {
        return (List<Marketing_Experiment__c>) selectSObjectsById(idSet);
    }

    public List<Marketing_Experiment__c> selectWithVariationsByExperimentName(Set<String> nameSet)
    {
        if (nameSet == null)
        {
            nameSet = new Set<String>();
        }

        fflib_QueryFactory experimentsQueryFactory = newQueryFactory();

        new mmmarketing_ExpVariationsSelector().addQueryFactorySubselect( experimentsQueryFactory );

        return Database.query( experimentsQueryFactory.setCondition(Marketing_Experiment__c.Experiment_Name__c + ' in :nameSet').toSOQL() );
    }
}