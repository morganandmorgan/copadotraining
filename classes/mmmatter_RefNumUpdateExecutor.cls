/**
 *  simple script to manage backfilling of matterRecord.ReferenceNumber__c info
 *
 *  @usage  String query = 'select Id, Name, Intake__c, Intake__r.Name, CP_Case_Number__c, ReferenceNumber__c from litify_pm__Matter__c where ReferenceNumber__c = null';
 *          new mmlib_GenericBatch( mmmatter_RefNumUpdateExecutor.class, query).setBatchSizeTo(15).execute();
 */
 public class mmmatter_RefNumUpdateExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        litify_pm__Matter__c matterRecord = null;

        boolean isReferenceNumberFound = false;

        for ( SObject record : scope )
        {
            matterRecord = (litify_pm__Matter__c)record;

            isReferenceNumberFound = false; // reset the boolean flag

            if ( matterRecord.CP_Case_Number__c != null )
            {
                for ( String chunk : matterRecord.CP_Case_Number__c.splitByCharacterType() )
                {
                    if ( chunk != null
                        && chunk.isNumeric() )
                    {
                        matterRecord.ReferenceNumber__c = chunk.trim().leftPad(7,'0');
                        isReferenceNumberFound = true;
                        break;
                    }
                }
            }
            else if ( matterRecord.Intake__c != null )
            {
                matterRecord.ReferenceNumber__c = matterRecord.Intake__r.Name.replace('INT-','');
                isReferenceNumberFound = true;
            }

            if ( ! isReferenceNumberFound )
            {
                system.debug( 'Reference number was not found for record id : ' + record.id );
            }
        }

        database.update(scope, false);
    }
}