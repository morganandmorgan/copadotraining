public with sharing class mmdocusign_GetEnvelopeLinkCallout
	extends mmlib_BasePOSTCallout
{
	private String clientUserId = '';
	private String envelopeId = '';
	private String email = '';
	private String userName = '';

	public mmdocusign_GetEnvelopeLinkCallout(String envelopeId, String email, String userName, String clientUserId)
	{
		this.envelopeId = envelopeId;
		this.email = email;
		this.userName = userName;
		this.clientUserId = clientUserId;
	}

	// ---------- mmlib_BasePOSTCallout Implementation --------------
	public override mmlib_BaseCallout execute()
	{
		if
		(
			mmdocusign_ConfigurationSettings.getIsEnabled() == false ||
			(mmlib_Utils.org.IsSandbox && mmdocusign_ConfigurationSettings.getIsEnabledInSandbox() == false)
		)
		{
			throw new mmdocusign_IntegrationExceptions.CalloutException('This feature is disabled via settings.  Change the settings to activate.');
		}

		setAuthorization(new mmdocusign_RestApiCalloutAuthorization());

		getHeaderMap().put('Content-Type', 'application/json');

		String requestBody =
			'{ "authenticationMethod": "None",' +
			'"clientUserId": "' + clientUserId + '",' +
		    '"email": "' + email + '",' +
		    '"returnUrl": "' + mmdocusign_ConfigurationSettings.getReturnUrl() + '",' +
		    '"userName": "'+ userName +'" }';

		addPostRequestBodyLine( requestBody );

		String responseBody = '';

        HttpResponse httpResp = super.executeCallout();

        System.debug('<ojs> httpResp:\n' + httpResp);

        if (httpResp.getStatusCode() == 200 || httpResp.getStatusCode() == 201)
        {
            responseBody = httpResp.getBody();
        }
        else
        {
            mmdocusign_IntegrationExceptions.CalloutException exc = new mmdocusign_IntegrationExceptions.CalloutException();
            exc.setMessage(
                'Callout returned a failing status code.\n' +
                httpResp.getBody()
            );
            throw exc;
        }

        if (isDebugOn)
        {
            System.debug('responseBody:\n' + responseBody);
        }

        calloutResponse = new mmdocusign_EnvelopeLinkCalloutResponse(responseBody);

        return this;
	}

	protected override String getHost()
	{
		return mmdocusign_ConfigurationSettings.getHost();
	}

	protected override Map<String, String> getParamMap()
	{
		return new Map<String, String>();
	}

	protected override String getPath()
	{
		return '/restapi/v2/accounts/' + mmdocusign_ConfigurationSettings.getAccountId() + '/envelopes/' + envelopeId + '/views/recipient';
	}

	private mmdocusign_EnvelopeLinkCalloutResponse calloutResponse = null;

	public override CalloutResponse getResponse()
	{
		return (mmlib_BaseCallout.CalloutResponse) calloutResponse;
	}
}