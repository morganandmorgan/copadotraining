@isTest
private class mmlawsuit_LawsuitsServiceImplTest
{
    private static List<litify_pm__Matter_Team_Role__c> roleList;
    private static List<litify_pm__Matter__c> matterList;
    private static List<User> userList;
    private static List<litify_pm__Matter_Plan__c> matterPlanList;
    private static List<Lawsuit__c> lawsuitList;
    private static TestData td;
    private static mmlib_ISObjectUnitOfWork uow;
    static {
        td = new TestData();

        Id standardUserProfileId = [select Id from Profile where name = 'Standard User'].get(0).Id;

        // Users
        userList = new List<User>();
        User usr = new User();
        usr.Alias = 'jone';
        usr.Email = 'joe.one@example.com';
        usr.EmailEncodingKey = 'UTF-8';
        usr.FederationIdentifier = usr.Email;
        usr.FirstName = 'Joe';
        usr.IsActive = true;
        usr.LanguageLocaleKey = 'en_US';
        usr.LastName = 'One';
        usr.LocaleSidKey = 'en_US';
        usr.ProfileId = standardUserProfileId;
        usr.TimeZoneSidKey = 'America/New_York';
        usr.Username = usr.Email;
        userList.add(usr);

        usr = new User();
        usr.Alias = 'jtwo';
        usr.Email = 'jane.two@example.com';
        usr.EmailEncodingKey = 'UTF-8';
        usr.FederationIdentifier = usr.Email;
        usr.FirstName = 'Jane';
        usr.IsActive = true;
        usr.LanguageLocaleKey = 'en_US';
        usr.LastName = 'Two';
        usr.LocaleSidKey = 'en_US';
        usr.ProfileId = standardUserProfileId;
        usr.TimeZoneSidKey = 'America/New_York';
        usr.Username = usr.Email;
        userList.add(usr);

        usr = new User();
        usr.Alias = 'jthree';
        usr.Email = 'john.three@example.com';
        usr.EmailEncodingKey = 'UTF-8';
        usr.FederationIdentifier = usr.Email;
        usr.FirstName = 'John';
        usr.IsActive = true;
        usr.LanguageLocaleKey = 'en_US';
        usr.LastName = 'Three';
        usr.LocaleSidKey = 'en_US';
        usr.ProfileId = standardUserProfileId;
        usr.TimeZoneSidKey = 'America/New_York';
        usr.Username = usr.Email;
        userList.add(usr);

        userList = (List<User>) saveAndRefreshData(userList);
        td.userMap = generateSObjectMapByUniqueField(userList, User.Email);

        // Account
        List<Account> accountList = new List<Account>();
        Account acct = new Account();
        acct.FirstName = 'Lila';
        acct.LastName = 'Woods';
        acct.PersonEmail = 'lila.woods@donotreply.com';
        acct.Phone = '784-555-5006';
        accountList.add(acct);

        accountList = (List<Account>) saveAndRefreshData(accountList);
        td.accountMap = generateSObjectMapByUniqueField(accountList, Account.LastName);

        // Matter Plan
        matterPlanList = new List<litify_pm__Matter_Plan__c>();
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        matterPlanList.add(matterPlan);

        // .......... A MatterPlan selector is nonexistent.
        td.matterPlanMap = generateSObjectMapByUniqueField(matterPlanList, litify_pm__Matter_Plan__c.Name);

        // Matters
        Id socialSecurityRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');

        matterList = new List<litify_pm__Matter__c>();
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socialSecurityRecordTypeId;
        matter.litify_pm__Client__c = td.accountMap.get('Woods').Id;
        matter.litify_pm__Matter_Plan__c = td.matterPlanMap.get('apex test matter plan').Id;
        matter.litify_pm__Status__c = 'Open';
        matterList.add(matter);

        matterList = (List<litify_pm__Matter__c>) saveAndRefreshData(matterList);
        td.matterMap = generateSObjectMapByUniqueField(matterList, litify_pm__Matter__c.Name);

        // Team Roles
        roleList = new List<litify_pm__Matter_Team_Role__c>();
        litify_pm__Matter_Team_Role__c role = new litify_pm__Matter_Team_Role__c();
        role.Name = mmlawsuit_LawsuitsService.RelevantTeamRoleName.CASE_DEVELOPER.name();
        roleList.add(role);

        role = new litify_pm__Matter_Team_Role__c();
        role.Name = mmlawsuit_LawsuitsService.RelevantTeamRoleName.MANAGING_ATTORNEY.name();
        roleList.add(role);

        role = new litify_pm__Matter_Team_Role__c();
        role.Name = mmlawsuit_LawsuitsService.RelevantTeamRoleName.PRINCIPAL_ATTORNEY.name();
        roleList.add(role);

        roleList = (List<litify_pm__Matter_Team_Role__c>) saveAndRefreshData(roleList);
        td.roleMap = generateSObjectMapByUniqueField(roleList, litify_pm__Matter_Team_Role__c.Name);

        // Lawsuits
        lawsuitList = new List<Lawsuit__c>();
        Lawsuit__c lawsuit = new Lawsuit__c();
        lawsuit.Related_Matter__c = td.matterMap.values().get(0).Id;
        lawsuit.CP_Case_Developer__c = 'anonymous-1';
        lawsuit.CP_Case_Developer_Email__c = 'a1@example.com';
        lawsuit.CP_Managing_Attorney__c = 'anonymous-2';
        lawsuit.CP_Managing_Attorney_Email__c = 'a2@example.com';
        lawsuit.CP_Handling_Attorney__c = 'anonymous-3';
        lawsuit.CP_Handling_Attorney_Email__c = 'a3@example.com';
        lawsuitList.add(lawsuit);

        lawsuitList = (List<Lawsuit__c>) saveAndRefreshData(lawsuitList);
        td.lawsuitMap = generateSObjectMapByUniqueField(lawsuitList, Lawsuit__c.Name);

        // Team Members
        List<litify_pm__Matter_Team_Member__c> teamList = new List<litify_pm__Matter_Team_Member__c>();
        litify_pm__Matter_Team_Member__c team = new litify_pm__Matter_Team_Member__c();
        team.litify_pm__Matter__c = td.matterMap.values().get(0).Id;
        team.litify_pm__User__c = td.userMap.get('joe.one@example.com').Id;
        team.litify_pm__Role__c = td.roleMap.get(mmlawsuit_LawsuitsService.RelevantTeamRoleName.CASE_DEVELOPER.name()).Id;
        teamList.add(team);

        team = new litify_pm__Matter_Team_Member__c();
        team.litify_pm__Matter__c = td.matterMap.values().get(0).Id;
        team.litify_pm__User__c = td.userMap.get('jane.two@example.com').Id;
        team.litify_pm__Role__c = td.roleMap.get(mmlawsuit_LawsuitsService.RelevantTeamRoleName.MANAGING_ATTORNEY.name()).Id;
        teamList.add(team);

        team = new litify_pm__Matter_Team_Member__c();
        team.litify_pm__Matter__c = td.matterMap.values().get(0).Id;
        team.litify_pm__User__c = td.userMap.get('john.three@example.com').Id;
        team.litify_pm__Role__c = td.roleMap.get(mmlawsuit_LawsuitsService.RelevantTeamRoleName.PRINCIPAL_ATTORNEY.name()).Id;
        teamList.add(team);

        uow = mm_Application.UnitOfWork.newInstance();

        td.teamMap = generateSObjectMapByUniqueField(teamList, litify_pm__Matter_Team_Member__c.Id);
    }

    //updateStatusForMatterChange(Map<Id, String> matterStatusMap)
    public static testMethod void mmlawsuit_LawsuitsServiceImpl_updateStatusForMatterChange() {

        // Arrange
        mmlawsuit_LawsuitsServiceImpl lawsuitsServiceImpl = new mmlawsuit_LawsuitsServiceImpl();

        // Act
        Test.startTest();
        lawsuitsServiceImpl.updateStatusForMatterChange(null);
        Test.stopTest();

        // Assert
    }

    public static testMethod void mmlawsuit_LawsuitsServiceImpl_updateTeamMemberAssignmentsForUserChange() {

        // Arrange
        mmlawsuit_LawsuitsServiceImpl lawsuitsServiceImpl = new mmlawsuit_LawsuitsServiceImpl();

        // Act
        Test.startTest();
        lawsuitsServiceImpl.updateTeamMemberAssignmentsForUserChange(new Set<Id> {userList[0].Id});
        Test.stopTest();

        // Assert
    }

    public static testMethod void mmlawsuit_LawsuitsServiceImpl_updateTeamMemberAssignmentsFromMatter() {

        // Arrange
        mmlawsuit_LawsuitsServiceImpl lawsuitsServiceImpl = new mmlawsuit_LawsuitsServiceImpl();

        // Act
        Test.startTest();
        lawsuitsServiceImpl.updateTeamMemberAssignmentsFromMatter(new Set<Id> {matterList[0].Id});
        Test.stopTest();

        // Assert
    }

    public static testMethod void mmlawsuit_LawsuitsServiceImpl_setLawsuitClientProfileFields() {

        // Arrange
        mmlawsuit_LawsuitsServiceImpl lawsuitsServiceImpl = new mmlawsuit_LawsuitsServiceImpl();

        // Act
        Test.startTest();
        roleList[0].Name = 'CASE_DEVELOPER';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        roleList[0].Name = 'CASE_MANAGER';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        roleList[0].Name = 'HANDLING_ATTORNEY';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        roleList[0].Name = 'MANAGING_ATTORNEY';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        roleList[0].Name = 'PRINCIPAL_ATTORNEY';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        roleList[0].Name = 'PARALEGAL';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        roleList[0].Name = 'LITIGATION_PARALEGAL';
        lawsuitsServiceImpl.setLawsuitClientProfileFields(lawsuitList[0], roleList[0], userList[0], uow);
        Test.stopTest();

        // Assert
    }

    static Map<String, SObject> generateSObjectMapByUniqueField
            (
                    final List<SObject> sObjects,
                    final Schema.SObjectField stringFieldToBeKey
            ) {
        Map<String, SObject> outputMap = new Map<String, SObject>();

        String stringValue = null;

        if (sObjects != null && !sObjects.isEmpty()) {
            for (SObject sobj : sObjects) {
                stringValue = (String) sobj.get(stringFieldToBeKey);

                if (String.isBlank(stringValue)) {
                    stringValue = mmlib_Utils.generateGuid();
                }

                if (!outputMap.containsKey(stringValue)) {
                    outputMap.put(stringValue, sobj);
                }
            }
        }

        return outputMap;
    }

    static SObjectType getSObjectType(List<SObject> sobjList) {
        if (sobjList != null && !sobjList.isEmpty()) {
            return sobjList.get(0).getSObjectType();
        }
        return null;
    }

    static List<SObject> saveData(List<SObject> dataList) {
        if (String.isBlank(dataList.get(0).Id)) {
            System.debug('insert List<SaveResult>:\n' + Database.insert(dataList));
        } else {
            System.debug('update List<SaveResult>:\n' + Database.update(dataList));
        }

        return dataList;
    }

    static List<SObject> refreshData(List<SObject> dataList) {
        return
                ((mmlib_ISObjectSelector)
                        mm_Application.Selector.newInstance(getSObjectType(dataList)))
                        .selectSObjectsById(generateIdSet(dataList, false));
    }

    static List<SObject> saveAndRefreshData(List<SObject> dataList) {
        return refreshData(saveData(dataList));
    }

    static Set<Id> generateIdSet(List<SObject> sobjList, Boolean addNull) {
        Set<Id> idSet = new Set<Id>();

        for (SObject sobj : sobjList) {
            if (addNull || String.isNotBlank(sobj.Id)) idSet.add(sobj.Id);
        }

        return idSet;
    }

    class TestData {
        public Map<String, SObject> userMap = null;
        public Map<String, SObject> accountMap = null;
        public Map<String, SObject> matterPlanMap = null;
        public Map<String, SObject> matterMap = null;
        public Map<String, SObject> roleMap = null;
        public Map<String, SObject> teamMap = null;
        public Map<String, SObject> lawsuitMap = null;
        public mmmatter_IMatterTeamMembersSelector mockTeamsSelector = null;
        public mmcommon_IUsersSelector mockUsersSelector = null;
        public mmlawsuit_ILawsuitsSelector mockLawsuitsSelector = null;
        public mmmatter_IMatterTeamRolesSelector mockRolesSelector = null;
        public CustomTestingStub cts = null;
        public mmlib_ISObjectUnitOfWork mockUow = null;
    }

    class CustomTestingStub implements System.StubProvider {

        private Map<Id, SObject> sobjMap = new Map<Id, SObject>();

        private List<SObject> sobjList() {
            return sobjMap.values();
        }

        public Object handleMethodCall(
                Object stubbedObject,
                String stubbedMethodName,
                Type returnType,
                List<Type> listOfParamTypes,
                List<String> listOfParamNames,
                List<Object> listOfArgs
        ) {
            if ('registerdirty'.equalsIgnoreCase(stubbedMethodName)) {
                for (Object obj : listOfArgs) {
                    SObject sobj = (SObject) obj;
                    sobjMap.put(sobj.Id, sobj);
                }
            }
            return null;
        }
    }
}