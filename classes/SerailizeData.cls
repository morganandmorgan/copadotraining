public class SerailizeData{
        @AuraEnabled public List<String> actionTokens;
        @AuraEnabled public List<String> tags;
    
        public static SerailizeData parse(String json){
            return (SerailizeData) System.JSON.deserialize(json, SerailizeData.class);
        }
    }