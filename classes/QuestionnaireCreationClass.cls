global class QuestionnaireCreationClass
{
    public static final string MESSAGE_DELIMITER = '~';

    webservice static String questionnaireCreation(Id intakeId) {

        String result;
        List<Intake__c> intakeList;
        if(intakeId != NULL) {

           String qryStr = 'SELECT Client__r.Gender__c, Client__r.Name, Client__r.FirstName, Client__r.LastName,Client__r.PersonAssistantName, Client__r.PersonAssistantPhone, Client__r.PersonBirthdate, Client__r.PersonContactId, Caller_Party__r.FirstName, Caller_Party__r.LastName,Client__r.PersonDepartment, Client__r.PersonEmail, Client__r.PersonEmailBouncedDate, Client__r.PersonEmailBouncedReason,';
                   qryStr += 'Client__r.PersonHomePhone, Client__r.PersonLastCURequestDate, Client__r.PersonLastCUUpdateDate, Client__r.PersonLeadSource, Client__r.PersonMailingCity, Client__r.PersonMailingCountry, Client__r.PersonMailingPostalCode, Client__r.PersonMailingState, Client__r.PersonMailingStreet,';
                   qryStr += 'Client__r.PersonMobilePhone, Client__r.PersonOtherCity, Client__r.PersonOtherCountry, Client__r.PersonOtherPhone, Client__r.PersonOtherPostalCode, Client__r.PersonOtherState, Client__r.PersonOtherStreet, Client__r.PersonTitle,';
                   qryStr += 'Client__r.ShippingAddress, Client__r.ShippingCity, Client__r.ShippingCountry, Client__r.ShippingPostalCode, Client__r.ShippingState, Client__r.ShippingStreet, Client__r.Fax, Client__r.PersonMailingAddress,';
                   qryStr += 'Client__r.Maiden_Name__c, Client__r.Mailing_Address_2__c, Client__r.Marital_Status__c, Client__r.Other_Names_Used__c, Client__r.Physical_Address_2__c, Client__r.Social_Security_Number__c, Client__r.Spouse_Partner_Name__c, Client__r.Work_Phone__c, ';

            Map <string, Schema.SObjectField> fieldMap = Schema.SObjectType.Intake__c.fields.getMap();
            for(Schema.SObjectField fld : fieldmap.values()) {
                qryStr += fld + ', ';
            }
            qryStr = qryStr.subString(0, qryStr.lastIndexOf(','));
            qryStr += ' FROM Intake__c WHERE Id = \'' + intakeId + '\'';
            System.debug(':::::::::qryStr:::::::::'+qryStr);
            intakeList = Database.Query(qryStr);
       }
        if(intakeList != null && intakeList.size() > 0) {

            Intake__c intak = intakeList[0];
            System.debug(':::::::intak:::::'+intak);
            List<Questionnaire__c> questList = [SELECT Id FROM Questionnaire__c WHERE Intake__c = :intak.Id];

            if(!(questList != NULL && questList.size() > 0)) {

                //String questName = intak.Client__r.Name + '-' + intak.Case_Type__c + '-' + DateTime.now().format('MM/dd/yyyy');
                Questionnaire__c questionnaireRcd = new Questionnaire__c (
                    Intake__c = intak.Id,
                    //Questionnaire_Name__c = questName,
                    Status__c = 'New',
                    //Lawsuits__c = opp.Id,
                    First_Name__c = intak.Client__r.FirstName,
                    Last_Name__c = intak.Client__r.LastName,
                    Mailing_Address_1__c = String.valueOf(intak.Client__r.PersonMailingAddress),
                    Mailing_City__c = intak.Client__r.PersonMailingCity,
                    Mailing_County__c = intak.Client__r.PersonMailingCountry,
                    Mailing_State__c = intak.Client__r.PersonMailingState,
                    Mailing_Zip_Code__c = (intak.Client__r.PersonMailingPostalCode != NULL) ? Decimal.valueOf(intak.Client__r.PersonMailingPostalCode) : NULL,
                    Physical_Address_1__c = String.valueOf(intak.Client__r.ShippingAddress),
                    Physical_City__c = intak.Client__r.ShippingCity,
                    Physical_County__c = intak.Client__r.ShippingCountry,
                    Physical_Zip_Code__c = (intak.Client__r.ShippingPostalCode != NULL) ? Decimal.valueOf(intak.Client__r.ShippingPostalCode) : NULL,
                    Physical_State__c = intak.Client__r.ShippingState,
                    Home_Phone__c = intak.Client__r.PersonHomePhone,
                    Mobile_Phone__c = intak.Client__r.PersonMobilePhone,
                    Fax_Number__c = intak.Client__r.Fax,
                    Email__c = intak.Client__r.PersonEmail,
                    Date_of_Birth__c = intak.Client__r.PersonBirthdate,
                    Party_calling_different_than_client__c =  intak.Caller_different_than_Injured_party__c,
                    //Caller_First_Name__c = intak.Caller_Party__r.FirstName,
                    //Caller_Last_Name__c = intak.Caller_Party__r.LastName,
                    Caller_First_Name__c = intak.Caller_First_Name__c,
                    Caller_Last_Name__c = intak.Caller_Last_Name__c,
                    Caller_relationship_to_client__c = intak.Injured_party_relationship_to_Caller__c,
                    Injured_party_deceased__c = intak.Injured_party_deceased__c,
                    Client__c = intak.Client__c,
                    //Caller__c = intak.Caller_Party__c,
                    Maiden_Name__c = intak.Client__r.Maiden_Name__c,
                    Mailing_Address_2__c = intak.Client__r.Mailing_Address_2__c,
                    Marital_Status__c = intak.Client__r.Marital_Status__c,
                    Other_Names_Used__c = intak.Client__r.Other_Names_Used__c,
                    Physical_Address_2__c = intak.Client__r.Physical_Address_2__c,
                    Social_Security__c = (intak.Client__r.Social_Security_Number__c != NULL) ? intak.Client__r.Social_Security_Number__c : NULL,
                    Spouse_Partner_Name__c = intak.Client__r.Spouse_Partner_Name__c,
                    Work_Phone__c = intak.Client__r.Work_Phone__c
                );
                try {

                    insert questionnaireRcd;
                    result = questionnaireRcd.Id;
                } catch(DMLException e) {

                    for (Integer i = 0; i < e.getNumDml(); i++) {

                        result = 'Error' + MESSAGE_DELIMITER + 'Please correct the following error(s): \n-'+e.getDmlMessage(i);
                        return result;
                    }
                }
            } else {

                result = 'Error' + MESSAGE_DELIMITER + 'This Intake record already have a Questionnaire record.';
                return result;
            }
        }
        return result;
    }
}