/**====================================================
 * PaymentProcessingController_Test
 * @description Test for Payment Processing Controller Class
 * @author CLD Partners
 * @date June 2019
 =====================================================*/
@isTest
private without sharing class PaymentProcessingController_Test {

	//TODO:  Exercise all the fields/properties and inner classes and their field and properties - directly or indirectly.

	private static c2g__codaCompany__c company;
    private static c2g__codaBankAccount__c operatingBankAccount;
	private static Payment_Collection__c testPaymentCollection;
	private static PaymentProcessingController controller = null;	

	@isTest
	static void setupData(){
    	/*--------------------------------------------------------------------
        FFA data setup
        --------------------------------------------------------------------*/
		company = TestDataFactory_FFA.company;

		System.assert(company.c2g__CustomerSettlementDiscount__c!=null);
		System.assert(company.c2g__CustomerSettlementDiscount__r!=null);

        operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
		update operatingBankAccount;



		// Map<c2g__codaPurchaseInvoice__c, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> payableInvoices_ExpenseMap = TestDataFactory_FFA.createPayableInvoices_Expense(2);
		// TestDataFactory_FFA.postPayableInvoices(payableInvoices_ExpenseMap.keySet());


		testPaymentCollection = new Payment_Collection__c(
			Company__c = company.Id,
			Company__r=company,
			Bank_Account__c = operatingBankAccount.Id,
			Bank_Account__r = operatingBankAccount,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);

		insert testPaymentCollection;

		{
			ApexPages.currentPage().getParameters().put('id', testPaymentCollection.Id);
			ApexPages.StandardController standardController =  new ApexPages.StandardController(testPaymentCollection);
			controller = new PaymentProcessingController(standardController);
			System.assert(controller!=null);
		}
	} 

    @isTest
    private static void testCreateController0() {
    			
        PaymentProcessingController controller = null;
		controller=new PaymentProcessingController();
		System.assert(controller!=null);

    }	

    @isTest
    private static void testCreateController1() {
    	setupData();

		ApexPages.currentPage().getParameters().put('id', testPaymentCollection.Id);
        ApexPages.StandardController stdCrl =  new ApexPages.StandardController(testPaymentCollection);
        PaymentProcessingController contExtension = new PaymentProcessingController(stdCrl);
		System.assert(contExtension!=null);

    }

	
    // @isTest
    // private static void test_fetchPaymentCollection_0() {
    // 	setupData();
	// 	//public static ActionResult fetchPaymentCollection(String paymentCollectionId) {

    //   	PaymentProcessingController controller = null;
	// 	//controller=new PaymentProcessingController();					

	// 	ApexPages.currentPage().getParameters().put('id', testPaymentCollection.Id);
    //     ApexPages.StandardController standardController =  new ApexPages.StandardController(testPaymentCollection);
    //     controller = new PaymentProcessingController(standardController);
	// 	System.assert(controller!=null);

	// 	PaymentProcessingController.ActionResult result=null;

	// 	Test.startTest();
	// 	result=PaymentProcessingController.fetchPaymentCollection(testPaymentCollection.Id);
	// 	Test.stopTest();

	// 	System.assert(result!=null);

    // }
	
    @isTest
    private static void test_fetchInProcessPayments_0() {
    	setupData();

		//TODO:  create a check range as prerequisite:
		
		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=PaymentProcessingController.fetchInProcessPayments(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_queryInProcessPayments_0() {
    	setupData();

		//TODO:  create a check range as prerequisite:
		 
		System.assert(controller!=null);

		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=PaymentProcessingController.queryInProcessPayments(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }	

    @isTest
    private static void test_fetchReadyToPostPayments_0() {
    	setupData();
		 
		System.assert(controller!=null);

		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=PaymentProcessingController.fetchReadyToPostPayments(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }	

    @isTest
    private static void test_queryReadyToPostPayments_0() {
    	setupData();
		 
		System.assert(controller!=null);

		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=PaymentProcessingController.queryReadyToPostPayments(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_updatePaymentStatusesIfItsTime_0() {
    	setupData();
		 
		System.assert(controller!=null);

		Test.startTest();
		PaymentProcessingController.updatePaymentStatusesIfItsTime(testPaymentCollection.Id);
		Test.stopTest();

    }	

    @isTest
    private static void test_updatePaymentStatuses_0() {
    	setupData();
		 
		System.assert(controller!=null);

		Test.startTest();
		PaymentProcessingController.updatePaymentStatuses(testPaymentCollection.Id);
		Test.stopTest();

    }	

    @isTest
    private static void test_processPayments_Step1_0() {

    	setupData();

		//more setup:
        //TestDataFactory_FFA.postPayableInvoices();

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        // System.assert((transactionLineItems.size()>0)); 		
		 
		System.assert(controller!=null);

		PaymentProcessingController.ActionResult result=null;

		String jsonPayload_Transactions = null;
		//TODO:  Create and serialize a TransLineRequest object:  
		//jsonPayload_CheckRequest=JSON.serialize(controller);
		//System.NullPointerException: null input to JSON parser

		//QueryRequest
		PaymentProcessingController.TransLineRequest requestObject=null;
		requestObject= new PaymentProcessingController.TransLineRequest();
		requestObject.transLinesWrappersToProcess = new List<PaymentProcessing.TransactionLineItemWrapper>();
		for(c2g__codaTransactionLineItem__c transLine: transactionLineItems){
			PaymentProcessing.TransactionLineItemWrapper rapper=null;
			rapper=new PaymentProcessing.TransactionLineItemWrapper();
			rapper.transactionLineItem = transLine;
			requestObject.transLinesWrappersToProcess.add(rapper);
		}
		System.assert(requestObject.transLinesWrappersToProcess!=null);
		// System.assert(requestObject.transLinesWrappersToProcess.size()>0);

		jsonPayload_Transactions=JSON.serialize(requestObject);

		Test.startTest();
		// result=PaymentProcessingController.processPayments_Step1(jsonPayload_Transactions, testPaymentCollection.Id);
		Test.stopTest();

    }	

    @isTest
    private static void test_processPayments_Step2_0() {
    	setupData();
		TestDataFactory_FFA.createCheckRange();
		 
		System.assert(controller!=null);

		PaymentProcessingController.ActionResult actionResult=null;

		String jsonPayload_CheckRequest = null;
		//jsonPayload_CheckRequest=JSON.serialize(controller);
		//System.NullPointerException: null input to JSON parser
		
		PaymentProcessingController.CheckNumberRequest requestObject=null;
		requestObject= new PaymentProcessingController.CheckNumberRequest();
		jsonPayload_CheckRequest=JSON.serialize(requestObject);		

		Test.startTest();
		actionResult=PaymentProcessingController.processPayments_Step2(jsonPayload_CheckRequest, testPaymentCollection.Id);
		Test.stopTest();

    }	

    @isTest
    private static void test_processPayments_Step3_0() {
    	setupData();
		 
		System.assert(controller!=null);

		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=PaymentProcessingController.processPayments_Step3(testPaymentCollection.Id);
		Test.stopTest();

    }

    @isTest
    private static void test_checkBatchStatus_Step1_0() {
    	setupData();
				 
		System.assert(controller!=null);

		Map<String, Object> result=null;

		Test.startTest();
		result=PaymentProcessingController.checkBatchStatus_Step1(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_checkBatchStatus_Step2_0() {
    	setupData();
				 
		System.assert(controller!=null);

		Map<String, Object> result=null;

		Test.startTest();
		result=PaymentProcessingController.checkBatchStatus_Step2(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }

    @isTest
    private static void test_checkBatchStatus_Step3_0() {
    	setupData();
				 
		System.assert(controller!=null);

		Map<String, Object> result=null;

		Test.startTest();
		result=PaymentProcessingController.checkBatchStatus_Step3(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_fetchPaymentDetails_0() {
    	setupData();
				 
		System.assert(controller!=null);

		Map<String, Object> result=null;

		Test.startTest();
		result=PaymentProcessingController.fetchPaymentDetails(testPaymentCollection.Id);
		Test.stopTest();

		System.assert(result!=null);

    }

    @isTest
    private static void test_CreateException() {
    	
		PaymentProcessingController.ApplicationException result=null;

		Test.startTest();
		result=new PaymentProcessingController.ApplicationException();
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_CreateQueryRequest_0() {
    	
		PaymentProcessingController.QueryRequest result=null;

		Test.startTest();
		result=new PaymentProcessingController.QueryRequest();

		//Exercise the setters/mutators:
        result.matterReferenceFilter = 'matterReferenceFilter';
        result.requestedByFilter = 'requestedByFilter';
        result.payableToFilter = 'payableToFilter';
        result.vendorInvoiceNumberFilter = 'vendorInvoiceNumberFilter';
        result.pinNumberFilter = 'pinNumberFilter';
        result.bankTypeToPayFilter = 'bankTypeToPayFilter';

		result.invoiceDateFilter = Date.today();
		result.dueDateFilter = Date.today();

		//Exercise the getters/accessors: 
		Object buffer = null;
        buffer = result.matterReferenceFilter;
        buffer = result.requestedByFilter;
        buffer = result.payableToFilter;
        buffer = result.vendorInvoiceNumberFilter;
        buffer = result.pinNumberFilter;
        buffer = result.bankTypeToPayFilter;

		buffer = result.invoiceDateFilter;
		buffer = result.dueDateFilter;		

		Test.stopTest();

		System.assert(result!=null);

    }

   @isTest
    private static void test_fetchEligibleTransactions_0() {
    	setupData();

		PaymentProcessingController.QueryRequest request=null;
		
		request=new PaymentProcessingController.QueryRequest();

		//Exercise the setters/mutators:
        // request.matterReferenceFilter = 'matterReferenceFilter';
        // request.requestedByFilter = 'requestedByFilter';
        // request.payableToFilter = 'payableToFilter';
        // request.vendorInvoiceNumberFilter = 'vendorInvoiceNumberFilter';
        // request.pinNumberFilter = 'pinNumberFilter';
        // request.bankTypeToPayFilter = 'bankTypeToPayFilter';

		request.invoiceDateFilter = Date.today();
		request.dueDateFilter = Date.today();

		System.assert(request!=null);		

		//TODO:  create a check range as prerequisite:
		
		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=PaymentProcessingController.fetchEligibleTranscations(JSON.serialize(request));
		Test.stopTest();

		System.assert(result!=null);

    }	
	
    @isTest
    private static void test_TransLineRequest() {
    	
		PaymentProcessingController.TransLineRequest result=null;

		Test.startTest();
		result=new PaymentProcessingController.TransLineRequest();
		result.transLinesWrappersToProcess=null;
		Object buffer = null;
		buffer=result.transLinesWrappersToProcess;
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_CheckNumberRequest() {
    	
		PaymentProcessingController.CheckNumberRequest result=null;

		Test.startTest();
		result=new PaymentProcessingController.CheckNumberRequest();
		result.paymentSummaryWrappers=null;
		Object buffer = null;
		buffer = result.paymentSummaryWrappers;
		Test.stopTest();

		System.assert(result!=null);

    }
	
    @isTest
    private static void test_ActionResult() {
    	
		PaymentProcessingController.ActionResult result=null;

		Test.startTest();
		result=new PaymentProcessingController.ActionResult();
		result.batchId=null;
		Object buffer = null;
		buffer = result.batchId;
		Test.stopTest();

		System.assert(result!=null);

    }	

    private static List<c2g__codaTransactionLineItem__c> getTestTransactionLines(){
    
        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = 
        [

        select 
        Id
        ,Name 
        ,c2g__Account__c
        ,c2g__Account__r.Name 
        ,c2g__Account__r.Split_Invoices_on_Separate_Payments__c 

        ,c2g__Transaction__r.c2g__DocumentNumber__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Name
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.Name 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__r.Name 

        ,c2g__Transaction__r.c2g__PayableInvoice__r.Payment_Bank_Account_Type__c 

        ,c2g__OwnerCompany__c
        ,c2g__OwnerCompany__r.Name 
        ,c2g__DocumentOutstandingValue__c 
        ,c2g__LineType__c 

        from c2g__codaTransactionLineItem__c 

        where 
        c2g__LineType__c = 'Account'
        and c2g__Transaction__r.c2g__PayableInvoice__c != null 
        and c2g__DocumentOutstandingValue__c != 0 
        and c2g__Account__r.c2g__CODAIntercompanyAccount__c != true 
        and c2g__OwnerCompany__c = :TestDataFactory_FFA.company.Id

        and c2g__Account__r.Split_Invoices_on_Separate_Payments__c = true 

        and Id not in 
        (
            select 
            c2g__TransactionLineItem__c
            from c2g__codaPaymentLineItem__c
        )

        order by 
        c2g__Transaction__r.c2g__Account__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c 

        limit 1
        ];

        return transactionLineItems;

    }  	

}