@isTest
private class mmffa_DepositsTest
{
    @isTest
    static void testDepositTrigger()
    {
		Account testAccount = TestUtil.createAccount('Test Account');
        
        insert testAccount;
        
        Deposit__c deposit = new Deposit__c();
        deposit.AssignedToMMBusiness__c = testAccount.id;
        
        insert deposit;
    }

}