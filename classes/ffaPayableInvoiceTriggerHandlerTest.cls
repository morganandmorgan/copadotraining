/*============================================================================
Name            : ffaPayableInvoiceTriggerHandlerTest
Author          : CLD
Created Date    : May 2020
Description     : Test class for ffaPayableInvoiceTriggerHandler
=============================================================================*/

@isTest
private class ffaPayableInvoiceTriggerHandlerTest {

  static Account act;
  static c2g__codaGeneralLedgerAccount__c testGLA;
  static c2g__codaCompany__c company;
  
  static ffaPayableInvoiceTriggerHandler handler = new ffaPayableInvoiceTriggerHandler();
	static {
        act = new Account(FirstName = 'Bill', LastName = 'Gates');
        act.c2g__CODATaxCalculationMethod__c = 'Gross';
        insert act;

        date today = date.Today();

        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        c2g__codaDimension1__c testDim1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDim2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDim3 = ffaTestUtilities.createTestDimension3();
        c2g__codaDimension4__c testDim4 = ffaTestUtilities.createTestDimension4(); 

        litify_pm__Matter__c matter = TestUtil.createMatter(act);
        insert matter;

        Deposit__c deposit = new Deposit__c();
        deposit.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Trust_Deposit');
        deposit.Matter__c = matter.Id;
        deposit.Amount__c = 40.00;
        deposit.Source__c = 'Settlement';
        deposit.Check_Date__c = Date.today();
        insert deposit;

        Deposit__c depositPayable = new Deposit__c();
        depositPayable.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Trust_Payout_Costs');
        depositPayable.Matter__c = matter.Id;
        depositPayable.Amount__c = 40.00;
        depositPayable.Source__c = 'Settlement';
        depositPayable.Check_Date__c = Date.today();
        insert depositPayable;

        company = TestDataFactory_FFA.createCompany('company');
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__IntercompanyAccount__c = act.Id;
        update company;

        c2g__codaPurchaseInvoice__c  testPIN = ffaTestUtilities.createPIN(act, today, testGLA.id, 100, testDim1, testDim2, testDim3);
        testPIN.Litify_Matter__c = matter.id;
        testPIN.c2g__OwnerCompany__c = company.id;
        update testPIN;


  }
	@isTest static void TestPIN() {

    Test.startTest();
    Test.stopTest();
	}
	
}