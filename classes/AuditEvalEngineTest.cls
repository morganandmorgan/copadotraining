/**
 * AuditEvalEngineTest
 * @description Units tests for AuditEvalEngine and other Audit classes
 * @author Matt Terrill
 * @date 10/8/2019
 *
    Coverage for:
        AuditEvalEngine
        AuditRuleDomain
*/
@isTest
public class AuditEvalEngineTest {

    @testSetup
    public static void setup() {

        Account a = new Account();

        a.PersonDoNotCall = true;
        a.PersonHasOptedOutOfEmail = false;

        a.PersonBirthdate = Date.today().addYears(-30).addDays(-10).addMonths(-3);
        a.Date_of_Marriage__c = Date.today().addYears(-5).addDays(-5).addMonths(-5);

        a.Intake_Create_Date__c = DateTime.now().addMonths(-6);
        a.Last_Call_Date_Time__c = DateTime.now().addHours(-3);

        a.AnnualRevenue = 12345678.99;
        a.Prior_Balance__c = 123.45;

        a.NumberOfEmployees = 123;

        a.FirstName = 'firstTest';
        a.LastName = 'lastTest';

        a.provider_Insurance_Segmentation__c = 'AOB Provider;PIP Insurance';

        insert a;

    } //setup


    private static Account queryAccount() {
        return [SELECT id, PersonDoNotCall, PersonHasOptedOutOfEmail
            ,PersonBirthdate, Date_of_Marriage__c
            ,Intake_Create_Date__c, Last_Call_Date_Time__c
            ,AnnualRevenue, Prior_Balance__c
            ,NumberOfEmployees
            ,FirstName, LastName
            ,Owner.LastName
            ,provider_Insurance_Segmentation__c
            FROM Account LIMIT 1];
    } //queryAccount


    private static AuditEvalEngine.Rule saveAccountRule() {

        //create a rule
        AuditEvalEngine.Rule rule = new AuditEvalEngine.Rule();
        rule.auditRule = new Audit_Rule__c();
        rule.auditRule.object__c = 'Account';
        rule.auditRule.rule_type__c = 'Real-Time';
        rule.auditRule.isActive__c = true;
        rule.auditRule.custom_scheduled_date_field__c = 'createdDate';
        rule.auditRule.related_flagged_user__c = 'ownerId';
        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        e.argument1 = '{!PersonDoNotCall}';
        e.operator = '=';
        e.argument2 = '{!PersonHasOptedOutOfEmail}';
        rule.expressions = new List<AuditEvalEngine.RuleExpression>();
        rule.expressions.add(e);

        //use the domain to save it
        AuditRuleDomain ruleDomain = new AuditRuleDomain();
        ruleDomain.upsertRule(rule);

        return rule;
    } //saveAccountRule


    @isTest
    public static void createRulesFromSObjectsTest() {
        AuditEvalEngine.Rule rule = saveAccountRule();

        //retrieve it with the selector, which will use AuditEvalEngine.createRulesFromSObjects
        AuditRuleSelector ruleSelector = new AuditRuleSelector();
        List<AuditEvalEngine.Rule> rules = ruleSelector.selectById( new Set<Id> {rule.auditRule.id});
    
        System.assertEquals('=',rules[0].expressions[0].operator);
    } //createRulesFromSObjectsTest


    @isTest
    public static void evaluateTriggerTest() {

        saveAccountRule();

        Account obj = queryAccount();
        Account old = obj.clone(true,true,true,true); //true dat
        old.PersonDoNotCall = !old.PersonDoNotCall;

        Map<Id,SObject> objects = new Map<Id,SObject>();
        objects.put(obj.id,obj);

        Map<Id,SObject> oldObjects = new Map<Id,SObject>();
        oldObjects.put(old.id,old);

        AuditEvalEngine aee = new AuditEvalEngine();
        List<AuditEvalEngine.EvaluationResult> results = aee.evaluate(objects, oldObjects);

        System.assertEquals(1, results.size());
        System.assertEquals(false, results[0].criteriaMet);

        //rerun with old data
        Map<Id,AuditEvalEngine.EvaluationResult> oldResults = aee.evaluate(results, oldObjects);

        System.assertEquals(true, oldResults.containsKey(obj.id));
        System.assertEquals(true, oldResults.get(obj.id).criteriaMet);

        //for some extra coverage
        Id flagId = AuditEvalEngine.determinWhoToFlag(results[0]);

    } //evaluateTriggerTest


    @isTest
    public static void evaluateScheduledFlags() {

        Account a = queryAccount();

        //create a matter
        litify_pm__Matter__c m = new litify_pm__Matter__c();
        m.litify_pm__Client__c = a.id;
        m.litify_pm__Status__c = 'New Case';
        insert m;

        //create a matter rule
        AuditEvalEngine.Rule rule = new AuditEvalEngine.Rule();
        rule.auditRule = new Audit_Rule__c();
        rule.auditRule.object__c = 'litify_pm__Matter__c';
        rule.auditRule.rule_type__c = 'Real-Time';
        rule.auditRule.isActive__c = true;
        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        e.argument1 = '{!litify_pm__Status__c}';
        e.operator = '=';
        e.argument2 = 'New Case';
        rule.expressions = new List<AuditEvalEngine.RuleExpression>();
        rule.expressions.add(e);

        //use the domain to save it
        AuditRuleDomain ruleDomain = new AuditRuleDomain();
        ruleDomain.upsertRule(rule);

        Audit_Scheduled_Flag__c asf = new Audit_Scheduled_Flag__c();
        asf.audit_rule__c = rule.auditRule.id;
        asf.litify_pm_Matter__c = m.id;
        asf.status__c = 'Pending';

        List<Audit_Scheduled_Flag__c> scheduledFlags = new List<Audit_Scheduled_Flag__c>();
        scheduledFlags.add(asf);

        AuditEvalEngine aee = new AuditEvalEngine();
        List<AuditEvalEngine.EvaluationResult> results = aee.evaluate(scheduledFlags);

        System.assertEquals(1,results.size());

    } //evaluateScheduledFlags


    @isTest
    public static void booleanTests() {

        Account a = queryAccount();

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();

        e.argument1 = '{!PersonDoNotCall}';
        e.operator = '=';
        e.argument2 = '{!PersonHasOptedOutOfEmail}';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.operator = '<>';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //test a constant
        e.argument1 = '{!PersonDoNotCall}';
        e.operator = '=';
        e.argument2 = 'true';
        System.assertEquals(true, aee.evaluate(a,e,false));

    } //booleanTests


    @isTest
    public static void dateTests() {

        Account a = queryAccount();

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();

        e.argument1 = '{!PersonBirthdate}';
        e.operator = '=';
        e.argument2 = '{!Date_of_Marriage__c}';
        System.assertEquals(false, aee.evaluate(a,e,false));

        //test a constant
        e.argument1 = '{!PersonBirthdate}';
        e.operator = '<';
        e.argument2 = '01/01/2010';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //test a dateModifier
        e.argument1 = '{!PersonBirthdate}';
        e.operator = '<';
        e.argument2 = '18';
        e.dateModifier = 'years_ago';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //since Date uses the DataTime code, we'll just do a couple

    } //dateTests


    @isTest
    public static void dateTimeTests() {

        Account a = queryAccount();

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();
        
        e.argument1 = '{!Intake_Create_Date__c}';
        e.operator = '=';
        e.argument2 = '{!Last_Call_Date_Time__c}';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.operator = '<>';
        System.assertEquals(true, aee.evaluate(a,e,false));

        e.operator = '>';
        System.assertEquals(false, aee.evaluate(a,e,false));
  
        e.operator = '>=';
        System.assertEquals(false, aee.evaluate(a,e,false));
  
        e.operator = '<';
        System.assertEquals(true, aee.evaluate(a,e,false));
      
        e.operator = '<=';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //test a constant
        e.argument1 = '{!Intake_Create_Date__c}';
        e.operator = '>';
        e.argument2 = '01/01/2015 01:23 PM';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //test a constant, reversed
        e.argument1 = '01/01/2015 01:23 PM';
        e.operator = '<';
        e.argument2 = '{!Intake_Create_Date__c}';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //test a dateModifier
        e.argument1 = '{!Intake_Create_Date__c}';
        e.operator = '>';
        e.argument2 = '1';
        e.dateModifier = 'years_ago';
        System.assertEquals(true, aee.evaluate(a,e,false));

    } //dateTimeTests

    @isTest
    public static void doubleTests() {

        Account a = queryAccount();

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();

        e.argument1 = '{!AnnualRevenue}';
        e.operator = '=';
        e.argument2 = '{!Prior_Balance__c}';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.operator = '<>';
        System.assertEquals(true, aee.evaluate(a,e,false));

        e.operator = '>';
        System.assertEquals(true, aee.evaluate(a,e,false));
  
        e.operator = '>=';
        System.assertEquals(true, aee.evaluate(a,e,false));
  
        e.operator = '<';
        System.assertEquals(false, aee.evaluate(a,e,false));
      
        e.operator = '<=';
        System.assertEquals(false, aee.evaluate(a,e,false));

        //test a constant
        e.argument1 = '{!AnnualRevenue}';
        e.operator = '<';
        e.argument2 = '123.45';
        System.assertEquals(false, aee.evaluate(a,e,false));

        //test a percent
        e.argument1 = '{!Prior_Balance__c}';
        e.operator = '>=';
        e.percent = 10;
        e.argument2 = '1000';
        System.assertEquals(true, aee.evaluate(a,e,false));

    } //doubletests

    @isTest
    public static void integerTests() {

        Account a = queryAccount();

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();

        e.argument1 = '{!NumberOfEmployees}';
        e.operator = '=';
        e.argument2 = '{!NumberOfEmployees}';
        System.assertEquals(true, aee.evaluate(a,e,false));

        e.operator = '<>';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.operator = '>';
        System.assertEquals(false, aee.evaluate(a,e,false));
  
        e.operator = '>=';
        System.assertEquals(true, aee.evaluate(a,e,false));
  
        e.operator = '<';
        System.assertEquals(false, aee.evaluate(a,e,false));
      
        e.operator = '<=';
        System.assertEquals(true, aee.evaluate(a,e,false));
    } //Integer tests

    @isTest
    public static void stringTests() {

        Account a = queryAccount();

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();

        e.argument1 = '{!FirstName}';
        e.operator = '=';
        e.argument2 = '{!LastName}';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.operator = '<>';
        System.assertEquals(true, aee.evaluate(a,e,false));

        e.operator = '>';
        System.assertEquals(false, aee.evaluate(a,e,false));
  
        e.operator = '>=';
        System.assertEquals(false, aee.evaluate(a,e,false));
  
        e.operator = '<';
        System.assertEquals(true, aee.evaluate(a,e,false));
      
        e.operator = '<=';
        System.assertEquals(true, aee.evaluate(a,e,false));

        //test a constant
        e.argument1 = '{!Owner.lastName}';
        e.operator = '=';
        e.argument2 = 'test';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.argument1 = '{!Provider_Insurance_Segmentation__c}';
        e.operator = 'contains';
        e.argument2 = 'AOB Provider';
        System.assertEquals(true, aee.evaluate(a,e,false));

        e.operator = 'does_not_contain';
        System.assertEquals(false, aee.evaluate(a,e,false));

        e.operator = 'contains_only';
        e.argument2 = 'AOB Provider\nPIP Insurance';
        System.assertEquals(true, aee.evaluate(a,e,false));

    } //stringTest

    @isTest
    public static void testTest() {

        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        AuditEvalEngine aee = new AuditEvalEngine();

        AuditEvalEngine.Rule rule = saveAccountRule();

        aee.test(rule);

    } //testTest

} //class