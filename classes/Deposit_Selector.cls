/*============================================================================/
* DepositSelector
* @description Selector for Litify Deposits
* @author Brian Krynitsky
* @date 2/26/2019
=============================================================================*/
public class Deposit_Selector extends fflib_SObjectSelector {
	
	public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Deposit__c.Id,
            Deposit__c.Matter__c,
            Deposit__c.Deposit_Allocated__c,
            Deposit__c.Allocated_Soft_Cost__c,
            Deposit__c.Allocated_Hard_Cost__c,
            Deposit__c.Allocated_Fee__c,
            Deposit__c.Journal__c
        };
    }

    // Constructor
    public Deposit_Selector() {
    }

    //required implemenation from fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Deposit__c.SObjectType;
    }

    // Methods
    public List<Deposit__c> selectOperatingTrustByMatterId(Set<Id> matterIds) {
        fflib_QueryFactory query = newQueryFactory();
        Set<String> recordTypeFilter = new Set<String>{'Operating', 'Trust Payout - Costs'};
        
        //where conditions:
        query.setCondition('RecordType.Name in :recordTypeFilter AND Matter__c in :matterIds');

        return (List<Deposit__c>) Database.query(query.toSOQL());
    }
}