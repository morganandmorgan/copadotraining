/**
 * ContactSelector
 * @description Selector for Contact SObject.
 * @author Matt Terrill
 * @date 8/16/2019
 */

public with sharing class ContactSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public ContactSelector() {
        sObjectFields = new List<Schema.SObjectField>{
                Contact.Id,
                Contact.AccountId,
                Contact.CreatedDate,
                Contact.Email,
                Contact.Fax,
                Contact.FirstName,
                Contact.HomePhone,
                Contact.LastName,
                Contact.Name,
                Contact.Phone
        };
    } //constructor

    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Contact.SObjectType;
    } //getSObjectType

    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields

    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList

    public List<Contact> selectById(Set<Id> ids) {
        return (List<Contact>) selectSObjectsById(ids);
    } //selectById

} //class