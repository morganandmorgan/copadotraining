public class mmmatter_ToggleRelatIntakeMatActAction
    extends mmlib_AbstractAction
{
    public override Boolean runInProcess()
    {
        for (SObject record : this.records)
        {
            Intake__c intake = (Intake__c) record;

            intake.IsMatterActive__c = intake.IsMatterActive__c == null ? true : ! intake.IsMatterActive__c;

            uow.registerDirty( intake );
        }

        return true;
    }
}