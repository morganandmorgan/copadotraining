/**
 * Unit Tests for mmlib_RecordTypeUtils class
 *
 * @see mmlib_RecordTypeUtils
 */
@isTest
private class mmlib_RecordTypeUtilsTest
{
    @isTest
    private static void test01()
    {
        //find all of the current account record types
        list<RecordType> accountRecordTypes = [select Id, Name, DeveloperName, NamespacePrefix
                                                    , Description, BusinessProcessId, SobjectType
                                                    , IsActive, IsPersonType, CreatedById, CreatedDate
                                                    , LastModifiedById, LastModifiedDate, SystemModstamp
                                                 from RecordType
                                                where SobjectType = 'Account'
                                                  and IsActive = true];

        // There is a small chance that this util could be run in an org where there is no Account recordtype.
        // In that case, this test should not be conducted.
        if ( ! accountRecordTypes.isEmpty() )
        {
            Id recordTypeIdFromTest = null;

            Test.startTest();

                recordTypeIdFromTest = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, accountRecordTypes[0].DeveloperName);

            Test.stopTest();

            system.assertEquals(recordTypeIdFromTest, accountRecordTypes[0].id);
        }
    }
}