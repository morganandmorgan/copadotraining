/*==================================================================================================================
 Name         FFAConstants
 Description  Utility class for declaring constants in a central location related to FFA
 Revisions    2019-FEB-20  CLD       Initial version
===================================================================================================================*/
public with sharing class FFAConstants {
	

	/*-----------------------------------------------------------------------------------------------------------
     General Ledger Accounts
    ------------------------------------------------------------------------------------------------------------*/
    static public final string BALANCE_SHEET_GLA_TYPE = 'Balance Sheet';
    static public final string INCOME_STATEMENT_GLA_TYPE = 'Profit and Loss';

    /*-----------------------------------------------------------------------------------------------------------
     General Ledger Accounts - TEST
    ------------------------------------------------------------------------------------------------------------*/
	static public final string TEST_BALANCE_SHEET_GLA_CODE = '10000';    
	static public final string TEST_INCOME_STATEMENT_GLA_CODE = '40000';    
    

    /*-----------------------------------------------------------------------------------------------------------
     Company - TEST
    ------------------------------------------------------------------------------------------------------------*/
    static public final string DEFAULT_TEST_COMPANY_NAME = 'ApexTestCompany';

    
    

}