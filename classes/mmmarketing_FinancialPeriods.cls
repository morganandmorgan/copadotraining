/**
 *  mmmarketing_FinancialPeriods
 */
public class mmmarketing_FinancialPeriods
    extends fflib_SObjectDomain
    implements mmmarketing_IFinancialPeriods
{
    public enum FINANCIAL_PERIODS
    {
        DAILY,
        WEEKLY,
        FORTNIGHTLY,
        SEMIMONTHLY,
        MONTHLY,
        QUARTERLY,
        YEARLY
    }

    private mmlib_ISObjectUnitOfWork uow = null;

    public static mmmarketing_IFinancialPeriods newInstance(List<Marketing_Financial_Period__c> records)
    {
        return (mmmarketing_IFinancialPeriods) mm_Application.Domain.newInstance(records);
    }

    public static mmmarketing_IFinancialPeriods newInstance(Set<Id> recordIds)
    {
        return (mmmarketing_IFinancialPeriods) mm_Application.Domain.newInstance(recordIds);
    }

    public mmmarketing_FinancialPeriods(List<Marketing_Financial_Period__c> records)
    {
        super(records);
    }

    public void setUnitOfWork( mmlib_ISObjectUnitOfWork uow )
    {
        this.uow = uow;
    }

    public override void onApplyDefaults()
    {
        defaultEndDateIfRequired();
        defaultFinancialPeriodTypeIfRequired();
        removeTextNotSetIfRequired();
    }

    public override void onAfterInsert()
    {
        mmmarketing_CampaignsSchedulable.setupCreateCampaignsFromMFP();
        mmmarketing_SourcesSchedulable.setupCreateSourcesFromMFP();
        mmmarketing_TISchedulable.setupAutoLinkingFromMTIToMFP();
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
    {
        mmmarketing_TISchedulable.setupAutoLinkingFromMTIToMFP();
    }

    public override void onBeforeInsert()
    {
        autoLinkToMarketingInformationIfRequired();
    }

    private void defaultEndDateIfRequired()
    {
        for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)records)
        {
            if (record.Period_End_Date__c == null
                && record.Period_Start_Date__c != null)
            {
                record.Period_End_Date__c = record.Period_Start_Date__c;
            }
        }
    }

    private void removeTextNotSetIfRequired()
    {
        String stringToRemove = '(not set)';
        for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)records)
        {
            if (stringToRemove.equalsIgnoreCase(record.Domain_Value__c))
            {
                record.Domain_Value__c = null;
            }

            if (stringToRemove.equalsIgnoreCase(record.Campaign_Value__c))
            {
                record.Campaign_Value__c = null;
            }

            if (stringToRemove.equalsIgnoreCase(record.Source_Value__c))
            {
                record.Source_Value__c = null;
            }

            if (stringToRemove.equalsIgnoreCase(record.Medium_Value__c))
            {
                record.Medium_Value__c = null;
            }

            if (stringToRemove.equalsIgnoreCase(record.Keyword_Value__c))
            {
                record.Keyword_Value__c = null;
            }

            if (stringToRemove.equalsIgnoreCase(record.Campaign_AdWords_ID_Value__c))
            {
                record.Campaign_AdWords_ID_Value__c = null;
            }
        }
    }

    private void defaultFinancialPeriodTypeIfRequired()
    {
        for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)records)
        {
            if (record.Financial_Period_Type__c == null
                && record.Period_Start_Date__c != null
                )
            {
                if (record.Period_Start_Date__c == record.Period_End_Date__c)
                {
                    record.Financial_Period_Type__c = FINANCIAL_PERIODS.DAILY.name().capitalize();
                    continue;
                }

                if (record.Period_Start_Date__c.addDays(6) == record.Period_End_Date__c)
                {
                    record.Financial_Period_Type__c = FINANCIAL_PERIODS.WEEKLY.name().capitalize();
                    continue;
                }

                if (record.Period_Start_Date__c.addDays(13) == record.Period_End_Date__c)
                {
                    record.Financial_Period_Type__c = FINANCIAL_PERIODS.FORTNIGHTLY.name().capitalize();
                    continue;
                }

                //Semi-Monthly - Gotta figure out how we would want to default that.

                if (record.Period_Start_Date__c.addMonths(1).addDays(-1) == record.Period_End_Date__c)
                {
                    record.Financial_Period_Type__c = FINANCIAL_PERIODS.MONTHLY.name().capitalize();
                    continue;
                }

                if (record.Period_Start_Date__c.addMonths(3).addDays(-1) == record.Period_End_Date__c)
                {
                    record.Financial_Period_Type__c = FINANCIAL_PERIODS.QUARTERLY.name().capitalize();
                    continue;
                }

                if (record.Period_Start_Date__c.addYears(1).addDays(-1) == record.Period_End_Date__c)
                {
                    record.Financial_Period_Type__c = FINANCIAL_PERIODS.YEARLY.name().capitalize();
                    continue;
                }
            }
        }
    }

    public void autoLinkToMarketingInformationIfRequired()
    {
        list<Marketing_Financial_Period__c> qualifiedRecords = new mmmarketing_MFPNotLinkedCriteria().setRecordsToEvaluate( records ).run();

        new mmmarketing_LinkMFPToMrktngInfoAction().setRecordsToActOn( qualifiedRecords ).run();
    }

    public void autoLinkToMarketingInformationIfRequired( mmlib_ISObjectUnitOfWork uow )
    {
        list<Marketing_Financial_Period__c> qualifiedRecords = new mmmarketing_MFPNotLinkedCriteria().setRecordsToEvaluate( records ).run();

        mmmarketing_LinkMFPToMrktngInfoAction action = new mmmarketing_LinkMFPToMrktngInfoAction();

        action.setRecordsToActOn( qualifiedRecords );

        ((mmlib_IUnitOfWorkable)action).setUnitOfWork( uow );

        action.run();

    }

    public void autoLinkToMarketingInformationIfRequiredInQueueableMode()
    {
        list<Marketing_Financial_Period__c> qualifiedRecords = new mmmarketing_MFPNotLinkedCriteria().setRecordsToEvaluate( records ).run();

        mmmarketing_LinkMFPToMrktngInfoAction action = new mmmarketing_LinkMFPToMrktngInfoAction();

        action.setActionToRunInQueue(true);

        action.setRecordsToActOn( qualifiedRecords ).run();
    }

    public void autoCreateMarketingCampaignsIfRequired()
    {
        system.debug( 'autoCreateMarketingCampaignsIfRequired() start ');

        // criteria: find MFP records where no campaign exists yet mmarketing_MFPWithNoCampaignCriteria()
        list<Marketing_Financial_Period__c> qualifiedRecordsForCampaigns = new mmmarketing_MFPNoCampaignCriteria().setRecordsToEvaluate( records ).run();

        // action: create MC for MFP records in QueueableMode() mmmarketing_CreateCampaignsForMFPAction
        mmmarketing_CreateCampaignsForMFPAction createCampaignsAction = new mmmarketing_CreateCampaignsForMFPAction();

//        createCampaignsAction.setActionToRunInQueue(true);

        ((mmlib_IUnitOfWorkable)createCampaignsAction).setUnitOfWork( this.uow );

        createCampaignsAction.setRecordsToActOn( qualifiedRecordsForCampaigns );

        createCampaignsAction.run();

        system.debug( 'autoCreateMarketingCampaignsIfRequired() end ');
    }

    public void autoCreateMarketingSourcesIfRequired()
    {
        system.debug( 'autoCreateMarketingSourcesIfRequired() start ');

        // criteria: find MFP records where no source exists yet
        list<Marketing_Financial_Period__c> qualifiedRecordsForSources = new mmmarketing_MFPNoSourcesCriteria().setRecordsToEvaluate( records ).run();

        system.debug( 'autoCreateMarketingSourcesIfRequired() qualifiedRecordsForSources : ' + qualifiedRecordsForSources.size());

        // action: create MS for MFP records in QueueableMode()
        mmmarketing_CreateSourcesForMFPAction createSourcesAction = new mmmarketing_CreateSourcesForMFPAction();

        ((mmlib_IUnitOfWorkable)createSourcesAction).setUnitOfWork( this.uow );

        createSourcesAction.setRecordsToActOn( qualifiedRecordsForSources );

        createSourcesAction.run();

        system.debug( 'autoCreateMarketingSourcesIfRequired() end ');
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmmarketing_FinancialPeriods(sObjectList);
        }
    }
}