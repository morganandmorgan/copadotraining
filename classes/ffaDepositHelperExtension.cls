public with sharing class ffaDepositHelperExtension 
{
    @AuraEnabled
    public static void executeDepositHelper(Id recordId)
    {
        Set<Id> ids = new Set<Id>();
        ids.add(recordId);

        MatterAllocationService.allocateItems_Deposit(ids);
    } 

    @AuraEnabled
    public static void executeDepositReverseHelper(Id recordId, String dateString)
    {
        Date journalDate = Date.valueOf(dateString);
        List<Id> ids = new List<Id>();
        ids.add(recordId);

        ffaDepositHelper.reverseDeposits(ids, journalDate);
    }
}