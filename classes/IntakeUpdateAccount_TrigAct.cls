public class IntakeUpdateAccount_TrigAct extends TriggerAction {

  public IntakeUpdateAccount_TrigAct() {
    super();
  }

  public override Boolean shouldRun() {
    Map<Id, Intake__c> newIntakes = getNewMapForContext();
    return !newIntakes.isEmpty();
  }

  public override void doAction() {
    Map<Id, Intake__c> newIntakes = getNewMapForContext();
    
    Set<Id> clientIds = new Set<Id>();
    for (Intake__c intake : newIntakes.values()) {
      if (intake.Client__c != null) {
        clientIds.add(intake.Client__c);
      }
    }

    if (!clientIds.isEmpty()) {
      List<Account> accountsToUpdate = [
        SELECT
          OB_Dials__c
        FROM
          Account
        WHERE
          Id IN :clientIds
          AND OB_Dials__c != 0
          AND Id NOT IN (
            SELECT Client__c
            FROM Intake__c
            WHERE Id NOT IN :newIntakes.keySet()
              AND Client__c IN :clientIds
              AND DAY_ONLY(CreatedDate) = LAST_N_DAYS:15
          )
        FOR UPDATE
      ];

      for (Account a : accountsToUpdate) {
        a.OB_Dials__c = 0;
      }

      Database.update(accountsToUpdate);
    }
  }

  private Map<Id, Intake__c> getNewMapForContext() {
    if (isAfter() && isInsert()) {
      return (Map<Id, Intake__c>) triggerMap;
    }
    return new Map<Id, Intake__c>();
  }
}