/**
 *  mmctm_ListCallsCallout
 *
 *  @usage mmctm_ListCallsCallout callout = new mmctm_ListCallsCallout();
 *  @usage callout.setCTMAccountId( ctmAccountId ).setAuthorization( (new mmctm_APIAccessNamedCredAuthorization()).setIdentifier( 'CTM_ForThePeople' );
 *  @usage callout.setQueryStartDate( date.newinstance(2017, 5, 6) );
 *  @usage callout.setQueryEndDate( date.newinstance(2017, 5, 6) );
 *  @usage callout.setCallerNumber('5132031926');
 *  @usage callout.execute();
 *  @usage system.debug( callout.getCTMAccounts() ) ;
 *
 */
public class mmctm_ListCallsCallout
    extends mmctm_BaseGETV1Callout
{
    private mmctm_ListCallsCallout.Response resp = new mmctm_ListCallsCallout.Response();
    private string ctmAccountId = null;
    private map<string, string> paramMap = new map<string, string>();

    public override map<string, string> getParamMap()
    {
        return this.paramMap;
    }

    public override string getPathSuffix()
    {
        return '/accounts/' + ctmAccountId + '/calls';
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        httpResponse httpResp = super.executeCallout();

        string jsonResponseBody = httpResp.getBody();

        if ( this.isDebugOn )
        {
            system.debug( json.deserializeUntyped( jsonResponseBody ) );
        }

        resp = (mmctm_ListCallsCallout.Response) json.deserialize( jsonResponseBody, mmctm_ListCallsCallout.Response.class );

        return this;
    }

    public class Response
        implements CalloutResponse
    {
        //public list<mmctm_ListCallsCallout.CTMAccount> accounts = new list<mmctm_ListCallsCallout.CTMAccount>();
        public list<ResponseCall> calls = new list<ResponseCall>();
        public String page;
        public String total_entries;
        public String total_pages;
        public String per_page;
        //public list<string> stats;
        public String next_page;

        public integer getTotalNumberOfRecordsFound()
        {
            return string.isblank(this.total_entries) ? 0 : integer.valueOf( this.total_entries );
        }

        public list<mmctm_ListCallsCallout.ResponseCall> getCTMCalls()
        {
            return this.calls;
        }
    }

    public class ResponseCall
    {
        private Datetime unixTimeAsDT = null;

        public String id;
        public String ad_group_id;
        public String ad_slot;
        public String ad_slot_position;
        public String adgroup_id;
        public String called_at;
        public String caller_number;
        public String caller_number_bare;
        public String caller_number_complete;
        public String campaign;
        public String campaign_id;
        public String duration;
        public String hold_time;
        public String keyword;
        public String latitude;
        public String longitude;
        public String medium;
        public String source;
        public String source_id;
        public String unix_time;
        public String web_source;
        public String tracking_label;

        public Datetime getCalledAtDatetime()
        {
            if ( this.unixTimeAsDT == null
                && string.isNotBlank(this.unix_time) )
            {
                try
                {
                    // CTM's unix time is "number of seconds".  SFDC is looking for "number of milliseconds".
                    //    Mulitply CTM's unix time by 1000
                    this.unixTimeAsDT = datetime.newInstance( long.valueOf(this.unix_time) * 1000 );
                }
                catch (Exception e)
                {
                    system.debug(logginglevel.warn, 'Unable to convert \''+this.unix_time+'\' to a datetime value.  Exception: ' + e.getMessage());
                }
            }

            return this.unixTimeAsDT;
        }
    }

    private void validateCallout()
    {
        if ( string.isBlank(ctmAccountId) )
        {
            throw new ParameterException('The CTM Account ID has not been specified.  Please use the setCTMAccountId(string) method before calling the execute() method.');
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public mmctm_ListCallsCallout setCTMAccountId( string ctmAccountId )
    {
        this.ctmAccountId = ctmAccountId;
        return this;
    }

    public mmctm_ListCallsCallout setQueryStartDate( Date startDate )
    {
        paramMap.put('start_date', string.valueOf( startDate ));
        return this;
    }

    public mmctm_ListCallsCallout setQueryEndDate( Date endDate )
    {
        paramMap.put('end_date', string.valueOf( endDate ));
        return this;
    }

    public mmctm_ListCallsCallout setCallerNumber( String callerNumber )
    {
        // the number needs to be a full phone number including the "+1"

        if ( string.isNotBlank(callerNumber)
            && callerNumber.length() > 8
            )
        {
            String workingNumber = mmlib_Utils.stripPhoneNumber( callerNumber );

            if ( workingNumber.length() == 10 )
            {
                // the number does not have the country code of "+1"
                workingNumber = '+1' + workingNumber;
            }
            else if ( ! workingNumber.startsWith('+') )
            {
                workingNumber = '+' + workingNumber;
            }

            paramMap.put('filter', 'caller_number:' + workingNumber);
        }

        return this;
    }

    public class ParameterException extends Exception {  }
}