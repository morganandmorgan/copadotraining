/**
 *  mmattyprtl_IIntakeSignUpService
 */
public interface mmattyprtl_IIntakeSignUpService
{
    Intake__c processSignUpRequest( map<Schema.sObjectField, string> fieldParameterMap, mmattyprtl_Enums.SCREENING_OPTIONS screeningOption, set<string> additionalEmailAddressesToNotifyList );
}