public class mmintake_EmailAlertsController
{
    private boolean isRelatedIntakeIdValueSet = false;
    private boolean isRelatedIncidentInvestigationEventIdValueSet = false;
    private boolean isRelatedIntakeInvestigationEventIdValueSet = false;

    public mmintake_EmailAlertsInfo info { get; private set; } { info = new mmintake_EmailAlertsInfo(); }

    public Id relatedIntakeId
    {
        get;
        set
        {
system.debug( 'isRelatedIntakeIdValueSet == ' + isRelatedIntakeIdValueSet );
            if ( ! isRelatedIntakeIdValueSet && value != null )
            {
                isRelatedIntakeIdValueSet = true;
                relatedIntakeId = value;
system.debug( 'relatedIntakeId is being set to == ' + relatedIntakeId );
                populateAllRequiredItem();
system.debug( 'mark blue ');
            }
        }
    }

    public Id relatedIncidentInvestigationEventId
    {
        get;
        set
        {
system.debug( 'isRelatedIncidentInvestigationEventIdValueSet == ' + isRelatedIncidentInvestigationEventIdValueSet );
            if ( ! isRelatedIncidentInvestigationEventIdValueSet && value != null )
            {
                isRelatedIncidentInvestigationEventIdValueSet = true;
                relatedIncidentInvestigationEventId = value;
system.debug( 'relatedIncidentInvestigationEventId is being set to == ' + relatedIncidentInvestigationEventId );
system.debug( 'mark 1 ');
                List<IntakeInvestigationEvent__c> inties = [SELECT Intake__c FROM IntakeInvestigationEvent__c WHERE IncidentInvestigation__c = :relatedIncidentInvestigationEventId];
                relatedIntakeId = inties[0].Intake__c;
system.debug( 'mark 2');
                populateAllRequiredItemBasedOnRelatedIncidentInvestigationEvent();
system.debug( 'mark 3 ');
            }
        }
    }


    public Id relatedIntakeInvestigationEventId
    {
        get;
        set
        {
system.debug( 'isRelatedIntakeInvestigationEventIdValueSet == ' + isRelatedIntakeInvestigationEventIdValueSet );
            if ( ! isRelatedIntakeInvestigationEventIdValueSet )
            {
system.debug( 'mark 31');
                isRelatedIntakeInvestigationEventIdValueSet = true;
                relatedIntakeInvestigationEventId = value;
system.debug( 'mark 32');
                List<IntakeInvestigationEvent__c> inties = [SELECT Intake__c FROM IntakeInvestigationEvent__c WHERE ID = :relatedIntakeInvestigationEventId];
system.debug( 'mark 33');
                relatedIntakeId = inties[0].Intake__c;
            }
        }
    }

    public Id relatedIncidentId {
        get;
        set {
            Id incidentId = value;
            List<Intake__c> intakes = [SELECT Id FROM Intake__c WHERE Incident__c = :incidentId];
system.debug( 'mark star wars movie 1');
            relatedIntakeId = intakes[0].Id;
        }
    }

    private void populateAllRequiredItem()
    {
        system.debug( 'mark 1000 relatedIntakeId == ' + relatedIntakeId);
        if ( relatedIntakeId != null )
        {
            mmintake_IIntakesSelector selector = mmintake_IntakesSelector.newInstance();
System.debug( 'mark 1001');
            this.info.intakeFieldsetNonEmpty = getFieldSetByCaseType();
System.debug( 'mark 1002');
            this.info.defaultFieldsetNonEmpty = Schema.SObjectType.Intake__c.fieldSets.Email_Alerts_Intake_Info;
System.debug( 'mark 1003');
            this.info.marketingInfoFieldsetNonEmpty = Schema.SObjectType.Intake__c.fieldSets.Email_Alerts_Marketing_Info;
System.debug( 'mark 1004');
            list<Schema.FieldSet> intakeFieldSetList = new list<Schema.FieldSet>();

            intakeFieldSetList.add( this.info.intakeFieldsetNonEmpty );
            intakeFieldSetList.add( this.info.defaultFieldsetNonEmpty );
            intakeFieldSetList.add( this.info.marketingInfoFieldsetNonEmpty );

            list<Intake__c> intakeRecords = selector.selectByIdWithCallerClientInjuredPartyAndAdditionalFields( new Set<Id>{ relatedIntakeId }
                                                                                                              , intakeFieldSetList
                                                                                                              , Schema.SObjectType.Event.fieldSets.Investigation_Info );
System.debug( 'mark 1005');
            this.info.intake = intakeRecords[0];
System.debug( 'mark 1006');
            this.info.client = this.info.intake.client__r;
System.debug( 'mark 1007');
            mmintake_IInvestigationEventsSelector ieSelector = mmintake_InvestigationEventsSelector.newInstance();
system.debug('mark 1007.1 this.info.allIncidentsForThisIntake.size() == ' + this.info.allIncidentsForThisIntake.size() );
            this.info.allIncidentsForThisIntake.addAll( ieSelector.selectScheduledByIntakesWithIntakeAndNames( new Set<Id>{ relatedIntakeId } ) );
system.debug('mark 1007.2 this.info.allIncidentsForThisIntake.size() is now == ' + this.info.allIncidentsForThisIntake.size() );
            this.info.allIntakeInvestigationsForThisIncident.addAll( ieSelector.selectByIncidentInvestigationsNotRelatedToIntakesWithIntakeAndNames( mmlib_Utils.generateIdSetFromField( this.info.allIncidentsForThisIntake, IntakeInvestigationEvent__c.IncidentInvestigation__c )
                                                                                                                                                   , new Set<Id>{ relatedIntakeId }
                                                                                                                                     ) );
System.debug( 'mark 1008');
            system.debug( this.info.intake );
            system.debug( this.info.client );

            //get the matter
            mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();
            list<litify_pm__Matter__c> matters = matterSelector.selectByIntake(new Set<Id>{this.info.intake.id});
            if (matters.size() != 0) {
                this.info.matter = matters[0];
            }

        }
    }

    private void populateAllRequiredItemBasedOnRelatedIncidentInvestigationEvent()
    {
        this.info.allCanceledIncidentsForThisIntake.addAll( mmintake_InvestigationEventsSelector.newInstance().selectCanceledRescheduledByIntakesWithIntakeAndNames( new Set<Id>{ relatedIntakeId } ) );

        this.info.additionalCaseNotes.addAll( mmcommon_EventsSelector.newInstance().selectByTypeAndWhat( new Set<String>{'Investigation'}, new Set<Id>{ relatedIncidentInvestigationEventId }) );

    }

    private Schema.FieldSet getFieldSetByCaseType()
    {
        Schema.FieldSet fieldSetToReturn = Schema.SObjectType.Intake__c.fieldSets.Email_Alerts_Default;

        Intake__c dummyIntake = [SELECT Id, Case_Type__c FROM Intake__c WHERE Id = :relatedIntakeId];

        for (Case_Type_Field_Set_Mappings__c castTypeFieldSetMapping : Case_Type_Field_Set_Mappings__c.getAll().values() )
        {
            if ( castTypeFieldSetMapping.Case_Type__c.equalsIgnoreCase( dummyIntake.Case_Type__c )
                && String.isNotBlank( castTypeFieldSetMapping.Field_Set_Name__c ) )
            {
                fieldSetToReturn = Schema.SObjectType.Intake__c.fieldSets.getMap().get( castTypeFieldSetMapping.Field_Set_Name__c.toLowerCase() );
                break;
            }
        }

        return fieldSetToReturn;
    }


}