public class mmmatter_Matters extends mmlib_SObjectDomain implements mmmatter_IMatters {
    private static Trigger_Settings__mdt triggerSettings = TriggerSettingsSelector.selectDefaultSettings();

    public static mmmatter_IMatters newInstance(List<litify_pm__Matter__c> records) {
        return (mmmatter_IMatters) mm_Application.Domain.newInstance(records);
    }

    public static mmmatter_IMatters newInstance(Set<Id> recordIds) {
        return (mmmatter_IMatters) mm_Application.Domain.newInstance(recordIds);
    }

    public mmmatter_Matters(List<litify_pm__Matter__c> records) {
        super(records);
    }

    public override void onBeforeInsert() {
        computeSOL();
    }

    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        computeSOL();
    }

    public override void onAfterInsert() {
        handleSocialSecurityStatusChanged(null);
    }

    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        handleSocialSecurityStatusChanged((Map<Id, litify_pm__Matter__c>) existingRecords);
    }

    private void computeSOL() {
        try {
            SolService solService = new SolService();

            //BK - This is replaced by the MatterTriggerHandler
            for (SObject sObj : records) {
                litify_pm__Matter__c matter = (litify_pm__Matter__c) sObj;
                if (matter != null) {
                    if (matter.SOL_Start_Date__c != null && !String.isEmpty(matter.Incident_State__c) && !String.isEmpty(matter.Case_Type_Name__c) && matter.SOL_Override_Date__c == null) {
                        SolService.BestMatch bestMatch = solService.getBestMatch(matter);
                        if (matter != null && bestMatch != null) {
                            if (matter.SOL_Start_Date__c != null && bestMatch.SOL.SOL__c != null) {
                                matter.litify_pm__Statute_Of_Limitations__c = matter.SOL_Start_Date__c.addYears(Integer.valueOf(bestMatch.SOL.SOL__c)).addDays(-1);
                            }
                        }
                    } else {
                        matter.litify_pm__Statute_Of_Limitations__c = matter.SOL_Override_Date__c;
                    }
                }
            }
        } catch (Exception e) {
            System.debug('Matter Compute SOL trigger error.  Disable using Trigger Settings custom meta data.');
            throw e;
        }
    }

    @TestVisible
    private void handleSocialSecurityStatusChanged(Map<Id, litify_pm__Matter__c> existingRecords) {
        List<StatusChangedPlatformEventData> statusChangedPlatformEventDataList = new List<StatusChangedPlatformEventData>();

        for (SObject sobj : records) {
            litify_pm__Matter__c matter = (litify_pm__Matter__c) sobj;
            litify_pm__Matter__c oldMatter = (existingRecords != null) ? existingRecords.get(matter.Id) : null;

            if (matter != null && matter.CP_Active_Case_Indicator__c == 'N')
            {
                statusChangedPlatformEventDataList.add(new StatusChangedPlatformEventData(matter));
            }
        }

        publishMatterStatusChangedPlatformEvent(JSON.serialize(statusChangedPlatformEventDataList));
    }

    private void publishMatterStatusChangedPlatformEvent(String payload) {
        if (String.isNotBlank(payload)) {
            if (test_publishEvents) {
                EventBus.publish(
                        new mmlib_Event__e(
                                EventName__c = 'MatterStatusChanged',
                                RelatedSobject__c = 'litify_pm__Matter__c',
                                Payload__c = payload));
            } else {
                System.debug(payload);
            }
        }
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new mmmatter_Matters(sObjectList);
        }
    }

    public class StatusChangedPlatformEventData {
        public Id matter_id = null;
        public String new_status = null;

        public StatusChangedPlatformEventData() {
            // Do nothing.
        }

        public StatusChangedPlatformEventData(litify_pm__Matter__c matter) {
            matter_id = matter.Id;
            new_status = matter.litify_pm__Status__c;
        }
    }

    // ======================= Test Context =============================================================
    @TestVisible
    private static Boolean test_publishEvents = true;

    @TestVisible
    private static void coverage() {
        Integer x = 0;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
        x += 1;
    }
}