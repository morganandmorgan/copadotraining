/**
 * FeeGoalAllocation_Selector
 * @description Selector for Fee Goal Allocation SObject.
 * @author Jeff Watson
 * @date 2/13/2019
 */

public with sharing class FeeGoalAllocation_Selector extends fflib_SObjectSelector {

    public FeeGoalAllocation_Selector() { }

    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Fee_Goal_Allocation__c.SObjectType;
    }

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Fee_Goal_Allocation__c.Allocated_Amount__c,
                Fee_Goal_Allocation__c.CreatedDate,
                Fee_Goal_Allocation__c.Fee_Goal__c,
                Fee_Goal_Allocation__c.Id,
                Fee_Goal_Allocation__c.Name
        };
    }

    // Methods
    public List<Fee_Goal_Allocation__c> selectById(Set<Id> ids) {
        return (List<Fee_Goal_Allocation__c>) selectSObjectsById(ids);
    }

    public List<Fee_Goal_Allocation__c> selectByFeeGoalIds(Set<Id> feeGoalIds) {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('Settlement__r');
        query.selectField('Settlement__r.Matter__c');
        query.setCondition('Fee_Goal__c IN :feeGoalIds');
        return (List<Fee_Goal_Allocation__c>) Database.query(query.toSOQL());
    }
}