/**
 *  @usage mmlitifylrn_ReferralsSyncCallout.Response resp = (mmlitifylrn_ReferralsSyncCallout.Response) new mmlitifylrn_ReferralsSyncCallout()
 *          .setLastSync(Datetime)
 *          .setAuthorization(mmlitifylrn_CalloutAuthorization)
 *          .debug()
 *          .execute()
 *          .getResponse();
 */
public class mmlitifylrn_RefIntkGetAll4SFDCGetCallout
        extends mmlitifylrn_BaseGETV1Callout
{
    private Map<String, String> paramMap = new map<String, String>();
    private mmlitifylrn_RefIntkGetAll4SFDCGetCallout.Response resp = new mmlitifylrn_RefIntkGetAll4SFDCGetCallout.Response();

    {
        paramMap.put('order_by', 'updated_at');
    }

    public override map<String, String> getParamMap()
    {
        return paramMap;
    }

    public override String getPathSuffix()
    {
        return '/referral_intakes/get_all_for_salesforce';
    }

    public mmlitifylrn_RefIntkGetAll4SFDCGetCallout setLastSync(Datetime lastSync)
    {
        if (lastSync == null)
        {
            lastSync = Date.today().addYears(-1);
        }

        paramMap.put('updated_since_utc', lastSync.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''));
        return this;
    }

    public mmlitifylrn_RefIntkGetAll4SFDCGetCallout setPage(Integer page)
    {
        paramMap.put('page', String.valueOf(page));
        return this;
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        httpResponse httpResp = super.executeCallout();

        if (this.isDebugOn)
        {
            system.debug(httpResp.getStatusCode());
            system.debug(httpResp.getStatus());
        }

        String jsonResponseBody = httpResp.getBody();

        if (this.isDebugOn)
        {
            system.debug(json.deserializeUntyped(jsonResponseBody));
        }

        if (httpResp.getStatusCode() >= 200
                && httpResp.getStatusCode() < 300)
        {
            System.debug(httpResp.getBody());
            resp = (mmlitifylrn_RefIntkGetAll4SFDCGetCallout.Response) json.deserialize(httpResp.getBody().replace('referral_intakes', 'Referrals'), mmlitifylrn_RefIntkGetAll4SFDCGetCallout.Response.class);
        } else
        {
            throw new mmlitifylrn_Exceptions.CalloutException(httpResp);
        }

        return this;
    }

    public class Response implements mmlib_BaseCallout.CalloutResponse
    {
        public List<mmlitifylrn_LRNModels.ReferralIntakeBasicData> referrals = new List<mmlitifylrn_LRNModels.ReferralIntakeBasicData>();
        public String total_items;
        public String total_pages;
        public String current_page;

        public Integer getTotalNumberOfRecordsFound()
        {
            return referrals.size();
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    private void validateCallout()
    {
        if (this.paramMap == null || !this.paramMap.containsKey('updated_since_utc') || this.paramMap.get('updated_since_utc') == null)
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Litify Referral Network requires a last sync parameter.  Please use the setLastSync(Datetime) method before calling the execute() method.');
        }
    }
}