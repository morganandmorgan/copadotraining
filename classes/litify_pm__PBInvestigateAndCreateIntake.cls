/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBInvestigateAndCreateIntake {
    global PBInvestigateAndCreateIntake() {

    }
    @InvocableMethod(label='Investigate and Create Intake' description='Investigates the given referral, then creates an intake')
    global static void investigateAndCreateIntake(List<litify_pm.PBInvestigateAndCreateIntake.ProcessBuilderInvestigateWrapper> investigateItems) {

    }
global class ProcessBuilderInvestigateWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderInvestigateWrapper() {

    }
}
}
