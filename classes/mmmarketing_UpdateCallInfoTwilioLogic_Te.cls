/**
 * mmmarketing_UpdateCallInfoTwilioLogic_Te
 * @description Test for Update Call Info Twilio Logic class.
 * @author Jeff Watson
 * @date 2/19/2019
 */
@IsTest
public with sharing class mmmarketing_UpdateCallInfoTwilioLogic_Te {

    private static Intake__c intake;
    private static Account personAccount;
    private static Marketing_Tracking_Info__c marketingTrackingInfo;

    static {
        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        marketingTrackingInfo = new Marketing_Tracking_Info__c();
        marketingTrackingInfo.Intake__c = intake.Id;
        marketingTrackingInfo.Tracking_Event_Timestamp__c = Datetime.now();
        marketingTrackingInfo.Calling_Phone_Number__c = '5555555555';
        insert marketingTrackingInfo;
    }

    @IsTest
    private static void ctor() {
        mmmarketing_UpdateCallInfoTwilioLogic updateCallInfoTwilioLogic = new mmmarketing_UpdateCallInfoTwilioLogic();
        System.assert(updateCallInfoTwilioLogic != null);
    }

    @IsTest
    private static void getAvailableAuthorizations() {
        mmmarketing_UpdateCallInfoTwilioLogic updateCallInfoTwilioLogic = new mmmarketing_UpdateCallInfoTwilioLogic();
        updateCallInfoTwilioLogic.getAvailableAuthorizations();
    }

    @IsTest
    private static void coverage() {
        mmmarketing_UpdateCallInfoTwilioLogic updateCallInfoTwilioLogic = new mmmarketing_UpdateCallInfoTwilioLogic();
        updateCallInfoTwilioLogic.coverage();
    }
}