/*============================================================================
Name            : processExpensesController
Author          : CLD
Created Date    : July 2018
Description     : Controller class for creating Payable Invoices from Expense records
=============================================================================*/
public with sharing class processExpensesController {
    
    //TO DO:
    /*
    1) Split up functionality by deposit type for trust and trust payouts
    */



    // ------------------------------------------------------------------------
    // Variables:
    // ------------------------------------------------------------------------
    public static Date pinDate {get;set;}
    public static Id companyId {get;set;}
    public static String currentCompanyName {get;set;}
    public List<String> matterRecordTypeNames {get;set;}
    public List<String> depositRecordTypeNames {get;set;}
    public static String matterReferenceFilter {get;set;}
    public List<String> bankAccountNames {get;set;}
    public List<String> paymentMediaTypes {get;set;}
    public static litify_pm__Expense__c dummyExpense {get;set;}
    public static Id payableToFilter {get;set;}
    public static Id requestedByFilter {get;set;}

    
    // ------------------------------------------------------------------------
    // Constructor:
    // ------------------------------------------------------------------------
    public processExpensesController() {
    system.debug('ENTER processExpensesController');
        pinDate = Date.today();
        matterReferenceFilter = '';
        requestedByFilter = null;
        payableToFilter = null;
        dummyExpense = new litify_pm__Expense__c(
        	RequestedBy__c = null,
        	PayableTo__c = null);

        List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if(companies != null && !companies.isEmpty()){
            companyId = companies.get(0).id;
            currentCompanyName = companies.get(0).Name;
        }

        //get the related bank accounts to that company:
        bankAccountNames = new List<String>();
        for(c2g__codaBankAccount__c ba : [SELECT Name FROM c2g__codaBankAccount__c
        	WHERE c2g__OwnerCompany__c = :companyId AND Bank_Account_Type__c != 'Trust']){
        	bankAccountNames.add(ba.Name);
        }

        paymentMediaTypes = new List<String>{'Check', 'Electronic'};

    }

    // ------------------------------------------------------------------------
    // Wrapper for results returned from remote actions
    // ------------------------------------------------------------------------
    public class ActionResult {
        public Object[] records {get;set;}
        public Map<String, Object> response {get; set;}
        public Id[] newInvoiceIds {get;set;}
        public String[] newInvoiceNames {get;set;}
        public String[] recordTypes {get;set;}

        // optional batch id when generating...
        public String batchId {get; set;}

        
        public ActionResult() {
            this.records = new Object[]{};
            this.response = new Map<String, Object>();
            this.newInvoiceIds = new Id[]{};
            this.newInvoiceNames = new String[]{};
        }
    }


    // ------------------------------------------------------------------------
    // Wrapper for an Individual Matter
    // ------------------------------------------------------------------------
    public class Despoit {
         public ID[] depositIds {get;set;}
    }


    // ------------------------------------------------------------------------
    // Wrapper for the Matter Rows
    // ------------------------------------------------------------------------
    public class ExpenseWrapper {
        public litify_pm__Expense__c sourceSObject {get;set;}
        public String matterName {get;set;}
        public String matterId {Get;set;}
        public String expenseName {get;set;}
        public String expenseDate {get;set;}
        public String expenseId {Get;set;}
        public String expenseType {get;set;}
        public String costType {get;set;}
        public Id payableToId {get;set;}
        public String payableToName {get;set;}
        public Id requestedById {get;set;}
        public String requestedByName {get;set;}
        public Id clientId {get;set;}
        public String clientName {get;set;}
        public Id attorneyId {get;set;}
        public String attorneyName {get;set;}
        public Decimal expenseAmount {get;set;}

        public String matterRecordTypeName {get;set;}
        public Boolean isChecked {get;set;}

  }

    // ------------------------------------------------------------------------
    // Queries the eligible matters
    // ------------------------------------------------------------------------
    @RemoteAction
    public static ActionResult fetchExpenseLines(String matterReferenceFilter, String requestedByFilter, String payableToFilter) {

        Id companyId;
        List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if(companies != null && !companies.isEmpty()){
            companyId = companies.get(0).id;
        }
        System.debug(LoggingLevel.WARN, '***FETCHING LINES companyId = ' + companyId);
        System.debug(LoggingLevel.WARN, '***FETCHING LINES matterReferenceFilter = ' + matterReferenceFilter);
        System.debug(LoggingLevel.WARN, '***FETCHING LINES requestedByFilter = ' + requestedByFilter);
        System.debug(LoggingLevel.WARN, '***FETCHING LINES payableToFilter = ' + payableToFilter);
        //System.debug(LoggingLevel.WARN, '***FETCHING LINES dummyExpense.PayableTo__c = ' + dummyExpense.PayableTo__c);
        //System.debug(LoggingLevel.WARN, '***FETCHING LINES dummyExpense.RequestedBy__c = ' + dummyExpense.RequestedBy__c);

        ExpenseWrapper[] records = new ExpenseWrapper[]{};
        List<ExpenseWrapper> expensesToProcess = new List<ExpenseWrapper>();
        List<String> matterRecordTypes = new List<String>();
        List<String> depositRecordTypes = new List<String>();

        //Expense Query String
        String expQuery = 'SELECT Id,'+
            'Name,'+
            'litify_pm__ExpenseType2__c,'+
            'litify_pm__ExpenseType2__r.Name,'+
            'litify_pm__ExpenseType2__r.CostType__c,'+
            'litify_pm__Amount__c,'+
            'Payable_Invoice__c,'+
            'Payable_Invoice_Created__c,'+
            'Create_Payable_Invoice__c,'+
            'Create_Check_Number__c,'+
            'litify_pm__Date__c,'+
            'PayableTo__c,'+
            'PayableTo__r.Name,'+
            'RequestedBy__c,'+
            'Vendor_Invoice_Number__c,'+
            'RequestedBy__r.Name,'+
            'litify_pm__Matter__r.ReferenceNumber__c,'+
            'litify_pm__Matter__c,'+
            'litify_pm__Matter__r.Name,'+
            'litify_pm__Matter__r.RecordType.Name,'+
            'litify_pm__Matter__r.litify_pm__Display_Name__c,'+
            'litify_pm__Matter__r.litify_pm__Client__c,'+
            'litify_pm__Matter__r.Client_Name__c,'+
            'litify_pm__Matter__r.litify_pm__Principal_Attorney__c,'+
            'litify_pm__Matter__r.litify_pm__Principal_Attorney__r.Name,'+
            'litify_pm__Matter__r.litify_pm__Matter_Plan__c,'+
            'litify_pm__Matter__r.litify_pm__Matter_Plan__r.Name '+
         'FROM litify_pm__Expense__c '+
         'WHERE Payable_Invoice_Created__c = false '+
         'AND Prepaid_Expense_Type__c = false '+
         'AND Written_Off__c = false '+
         'AND litify_pm__ExpenseType2__r.Exclude_from_PIN_Creation__c = false '+
         'AND litify_pm__Matter__r.AssignedToMMBusiness__r.FFA_Company__c = :companyId '+
         'AND litify_pm__ExpenseType2__r.CostType__c = \'HardCost\' '+
         'AND litify_pm__Matter__r.AssignedToMMBusiness__r.FFA_Company__c != null ';

        //add dynamic query filtering as needed:
        expQuery += requestedByFilter != null && requestedByFilter != '' && requestedByFilter != '000000000000000'? 'AND RequestedBy__c = : requestedByFilter ' : '';
        expQuery += payableToFilter != null && payableToFilter != '' && payableToFilter != '000000000000000' ? 'AND PayableTo__c = :payableToFilter ' : '';
        expQuery += matterReferenceFilter != null && matterReferenceFilter != '' ? 'AND litify_pm__Matter__r.ReferenceNumber__c = : matterReferenceFilter ' : '';
        
        for(litify_pm__Expense__c expense : Database.query(expQuery)){
            
            //create Expense Wrapper
            ExpenseWrapper ew = new ExpenseWrapper();
            ew.sourceSObject = expense;
            ew.expenseId = expense.Id;
            ew.expenseName = expense.Name;
            ew.expenseDate = expense.litify_pm__Date__c.format();
            ew.matterId = expense.litify_pm__Matter__c;
            ew.matterName = expense.litify_pm__Matter__r.Name;
            ew.clientId = expense.litify_pm__Matter__r.litify_pm__Client__c;
            ew.clientName = expense.litify_pm__Matter__r.Client_Name__c;
            ew.payableToId = expense.PayableTo__c;
            ew.payableToName = expense.PayableTo__r.Name;
            ew.requestedById = expense.RequestedBy__c;
            ew.requestedByName = expense.RequestedBy__r.Name;
            ew.attorneyId = expense.litify_pm__Matter__r.litify_pm__Principal_Attorney__c;
            ew.attorneyName = expense.litify_pm__Matter__r.litify_pm__Principal_Attorney__r.Name;
            ew.expenseAmount = expense.litify_pm__Amount__c;
            ew.isChecked = false;
            records.add(ew);
        }
        System.debug(LoggingLevel.WARN, '***FETCHING LINES records = ' + records);

        ActionResult actionResult = new ActionResult();
        actionResult.records = records;
        return actionResult;
    }

    public class PayableInvoiceWrapper {
        String invoiceNumber {get;set;}
        Id payableTo {get;set;}
        Id requestedBy {get;set;}
        Id matter {get;set;}
        List<litify_pm__Expense__c> litifyExpensesToProcess {get;set;}

        public PayableInvoiceWrapper(){
        }
    }

    public class BankLineWrapper {
        Id gla {get;set;}
        Id bankAccount {get;set;}
        Decimal totalDepositAmount {get;set;}

        public BankLineWrapper(){
            totalDepositAmount = 0;
        }
    }

    public class applicationException extends Exception {}

    
    // ------------------------------------------------------------------------
    // Creates a payable invoice from list of expense wrappers
    //Returns a map of "Status" to either "Error Message" or Success
    // ------------------------------------------------------------------------
    public static ActionResult createPINfromExpenses(Set<Id> expenseIds, PayableInvoiceRequest pin_req){
        List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        c2g__codaCompany__c company = companies[0];
        return createPINfromExpenses(expenseIds, pin_req, company);
    }

    // ------------------------------------------------------------------------
    // Creates a payable invoice from list of expense wrappers
    //Returns a map of "Status" to either "Error Message" or Success
    // ------------------------------------------------------------------------
    public static ActionResult createPINfromExpenses(Set<Id> expenseIds, PayableInvoiceRequest pin_req, c2g__codaCompany__c company){

        System.debug(LoggingLevel.WARN, '*** ENTER createPINfromExpenses!');
        System.debug(LoggingLevel.WARN, '*** pin_req = '+ pin_req);
        ActionResult returnResult = new ActionResult();

        Map<Date,Id> periodMap = new Map<Date,Id>();
        for(c2g__codaPeriod__c period : [SELECT Id, c2g__StartDate__c FROM c2g__codaPeriod__c WHERE c2g__OwnerCompany__c = :company.Id AND c2g__PeriodNumber__c not in ('100','000','101') ]){
        	periodMap.put(period.c2g__StartDate__c, period.Id);
        }
        System.debug(LoggingLevel.WARN, '*** periodMap size = '+ periodMap.size());

        Map<String,Id> currencyMap = new Map<String,Id>();
        for(c2g__codaAccountingCurrency__c acctCurr : [SELECT Id, Name FROM c2g__codaAccountingCurrency__c WHERE c2g__OwnerCompany__c = :company.Id]){
        	currencyMap.put(acctCurr.Name, acctCurr.Id);
        }
        System.debug(LoggingLevel.WARN, '*** currencyMap size = '+ currencyMap.size());

        Map<String,Id> baMap = new Map<String,Id>();
        for(c2g__codaBankAccount__c ba : [SELECT Id, Name FROM c2g__codaBankAccount__c WHERE c2g__OwnerCompany__c = :company.Id]){
        	baMap.put(ba.Name, ba.Id);
        }
        System.debug(LoggingLevel.WARN, '*** baMap size = '+ baMap.size());

        List<litify_pm__Expense__c> expensesUpdate = new List<litify_pm__Expense__c>();
        List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLinesToInsert = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();
        Map<String, c2g__codaPayment__c> paymentMap = new Map<String,c2g__codaPayment__c>();

        Map<String, c2g__codaPurchaseInvoice__c> pinMap = new Map<String, c2g__codaPurchaseInvoice__c>();

        Map<String, String> resultsMap = new Map<String, String>();

        List<litify_pm__Expense__c> expenseList = [
            SELECT 
                Id, 
                litify_pm__Amount__c,
                RequestedBy__c,
                RequestedBy__r.Name,
                PayableTo__c,
                PayableTo__r.Name,
                Payable_Invoice_Created__c,
                Payable_Invoice__c,
                litify_pm__Note__c,
                PayableTo__r.c2g__codaAccountTradingCurrency__c,
                PayableTo__r.c2g__codaAccountsPayableControl__c,
                litify_pm__ExpenseType2__r.General_Ledger_Account__c,
                litify_pm__ExpenseType2__c,
                Vendor_Invoice_Number__c,
                Finance_Request__c,
                Finance_Request__r.Payment_Bank_Account_Type__c,
                litify_pm__Matter__c,
                litify_pm__Matter__r.Name,
                litify_pm__Matter__r.ReferenceNumber__c,
                litify_pm__Matter__r.FFA_Company__c,
                litify_pm__Matter__r.Hard_Cost_To_Recover__c,
                litify_pm__Matter__r.Soft_Cost_To_Recover__c,
                litify_pm__Matter__r.Fee_Income_Allocation__c,
                litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_2__c,
                litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_3__c,
                litify_pm__Matter__r.Intake__c,
                litify_pm__Matter__r.Assigned_Office_Location__c,
                litify_pm__Matter__r.Assigned_Office_Location__r.c2g__codaDimension1__c
            FROM litify_pm__Expense__c
            WHERE Payable_Invoice_Created__c = FALSE
            AND PayableTo__c != null
            AND Id in :expenseIds]; 

        System.debug(LoggingLevel.WARN, ' *** expenseList = '+expenseList);

        //first loop through expenses and perform some validations
        Integer expectedNumberOfPINs = 0;
        Set<String> pinUniqueStringSet = new Set<String>();
        for(litify_pm__Expense__c exp : expenseList){

            //get the expected number of PINs
            String requestedBy = String.valueOf(exp.RequestedBy__c) != null ? String.valueOf(exp.RequestedBy__c) : '';
            String key = String.valueOf(exp.litify_pm__Matter__c) +'|'+ String.valueOf(exp.PayableTo__c) +'|'+ requestedBy;
            pinUniqueStringSet.add(key);

            //check the payable to has a trading currency and a ap control account
            if(exp.PayableTo__r.c2g__codaAccountTradingCurrency__c == null || exp.PayableTo__r.c2g__codaAccountsPayableControl__c == null){
            	throw new applicationException('There is no AP control GLA and/or trading currency specified for this vendor. Please update the vendor account and try again.');
            }
        }
        //if create payment was selected and there are 4 or more PINs to be created, we should throw an error as the FF API will error out with this may payments in one execution:
        expectedNumberOfPINs = pinUniqueStringSet.size();
        if(pin_req.autoCreatePayment == true && expectedNumberOfPINs > 3){
            throw new applicationException('Your selections would create '+expectedNumberOfPINs+ ' payment records in FFA. You can only process a max of 3 payments at a time using this page. \n\nNote: A unique payment is determined by the unique combination of Matter | Payable To | Requested By');
        }
        
        Savepoint sp = Database.setSavepoint();
        try{
        
            for(litify_pm__Expense__c exp : expenseList){
                
                String requestedBy = String.valueOf(exp.RequestedBy__c) != null ? String.valueOf(exp.RequestedBy__c) : '';
                String key = String.valueOf(exp.litify_pm__Matter__c) +'|'+ String.valueOf(exp.PayableTo__c) +'|'+ requestedBy;
                String invoiceNumber = exp.Vendor_Invoice_Number__c != null ? exp.Vendor_Invoice_Number__c : exp.litify_pm__Matter__r.ReferenceNumber__c != null ? exp.litify_pm__Matter__r.ReferenceNumber__c + Datetime.now().format() : Datetime.now().format();
                invoiceNumber = invoiceNumber.length()>25 ? invoiceNumber.substring(0, 24) : invoiceNumber;

                if(!pinMap.containsKey(key)){

                    //new pin header
                    c2g__codaPurchaseInvoice__c newPIN = new c2g__codaPurchaseInvoice__c(
                    	Linking_Key__c = key,
                        c2g__OwnerCompany__c = company.Id,
                        OwnerId = company.OwnerId,
                        Litify_Matter__c = exp.litify_pm__Matter__c,
                        c2g__InvoiceCurrency__c = currencyMap.containsKey('USD') ? currencyMap.get('USD') : null,
                        c2g__Account__c = exp.PayableTo__c,
                        Requested_By__c = exp.RequestedBy__c,
                        c2g__AccountInvoiceNumber__c = invoiceNumber,
                        c2g__InvoiceDescription__c = 'Expenses for Matter: '+ exp.litify_pm__Matter__r.Name + ' from: '+ exp.PayableTo__r.Name + ' req by: ' + exp.RequestedBy__r.Name, 
                        c2g__DeriveCurrency__c = true,
                        c2g__DerivePeriod__c = true,
                        Finance_Request__c = exp.Finance_Request__c,
                        Payment_Bank_Account_Type__c = exp.Finance_Request__r.Payment_Bank_Account_Type__c,
                        c2g__DeriveDueDate__c = false,
                        c2g__InvoiceDate__c = pin_req.pinDate,
                        c2g__DueDate__c = pin_req.pinDate
                    );
                    pinMap.put(key, newPIN);
                }

                //TO DO: Create expense lines
                c2g__codaPurchaseInvoiceExpenseLineItem__c expLine = new c2g__codaPurchaseInvoiceExpenseLineItem__c(
                	Linking_Key__c = key,
                	c2g__GeneralLedgerAccount__c = exp.litify_pm__ExpenseType2__r.General_Ledger_Account__c,
                	Litify_Expense_Created__c = true,
                	c2g__LineDescription__c = exp.litify_pm__Note__c,
                	Matter__c = exp.litify_pm__Matter__c,
                	c2g__Dimension1__c = exp.litify_pm__Matter__r.Assigned_Office_Location__r.c2g__codaDimension1__c,
                   	c2g__Dimension2__c = exp.litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_2__c,
                    c2g__Dimension3__c = exp.litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_3__c,
                	c2g__NetValue__c = exp.litify_pm__Amount__c
                	);
                	pinLinesToInsert.add(expLine);
            }

            //now insert the pins and pin lines
            insert pinMap.values();
            for(c2g__codaPurchaseInvoiceExpenseLineItem__c pinLine : pinLinesToInsert){
            	pinLine.c2g__PurchaseInvoice__c = pinMap.containsKey(pinLine.Linking_Key__c) ? pinMap.get(pinLine.Linking_Key__c).Id : null;
            }
            insert pinLinesToInsert;

            List<c2g__codaPurchaseInvoice__c> pinsCreated = [
            		SELECT Id, Linking_Key__c, Name, Litify_Matter__c, c2g__Account__c,c2g__InvoiceDescription__c, Requested_By__c
            		FROM c2g__codaPurchaseInvoice__c WHERE Id in : pinMap.values()];
           	for(c2g__codaPurchaseInvoice__c pin : pinsCreated){
           		returnResult.newInvoiceIds.add(pin.Id);
           		returnResult.newInvoiceNames.add(pin.Name);
           	}

            //update the expense records;
            for(litify_pm__Expense__c exp : expenseList){
                
                String requestedBy = String.valueOf(exp.RequestedBy__c) != null ? String.valueOf(exp.RequestedBy__c) : '';
                String key = String.valueOf(exp.litify_pm__Matter__c) +'|'+ String.valueOf(exp.PayableTo__c) +'|'+ requestedBy;
                exp.Payable_Invoice__c = pinMap.containsKey(key) ? pinMap.get(key).Id : null;
                exp.Payable_Invoice_Created__c = true;
            }
            update expenseList;


            //Post the invoice if instructed to:
            List<Id> pinIds = new List<Id>();
            if(pin_req.autoPostInvoice == true){
            	for(c2g__codaPurchaseInvoice__c pin : pinMap.values()){
            		pinIds.add(pin.Id);
            	}	
                List<c2g__codaPurchaseInvoice__c> resultList = FFAUtilities.postPINs(pinIds);
            }

            //PAYMENT Processing
            if(pin_req.autoCreatePayment == true){

            	//check to make sure the bank acocunt name isn't null:
            	if(pin_req.selectedBankAccountName == null || pin_req.selectedBankAccountName == '' || pin_req.selectedPaymentMedia == null || pin_req.selectedPaymentMedia == ''){
            		throw new applicationException('You must select a bank account to pay from and a payment media if you have elected to create the payment.');
            	}

            	//create the Payment for each PIN
            	for(c2g__codaPurchaseInvoice__c pin : pinsCreated){
            		
            		c2g__codaPayment__c pmt = new c2g__codaPayment__c(
            		c2g__BankAccount__c = baMap.containsKey(pin_req.selectedBankAccountName) ? baMap.get(pin_req.selectedBankAccountName) : null,
            		c2g__Period__c = periodMap.containsKey(pin_req.pinDate.toStartOfMonth()) ? periodMap.get(pin_req.pinDate.toStartOfMonth()) : null,
            		c2g__OwnerCompany__c = company.Id,
            		c2g__Description__c = pin.c2g__InvoiceDescription__c,
                    OwnerId = company.OwnerId,
                    c2g__PaymentDate__c = pin_req.pinDate,
                    c2g__DiscountDate__c = pin_req.pinDate,
                    c2g__Status__c = 'New',
                    c2g__PaymentMediaTypes__c = pin_req.selectedPaymentMedia,
                    c2g__CurrencyMode__c = 'Document',
                    c2g__PaymentCurrency__c = currencyMap.containsKey('USD') ? currencyMap.get('USD') : null,
                   	c2g__SettlementDiscountReceived__c = company.c2g__CustomerSettlementDiscount__c
                   	);

                   	paymentMap.put(pin.Linking_Key__c, pmt);
            	}

            	insert paymentMap.values();

            	//now add the transaction lines to the appropriate payments
            	Map<String, List<Id>> tliMap = new Map<String, List<Id>>();
            	for(c2g__codaTransactionLineItem__c tli : [SELECT Id, c2g__Transaction__r.c2g__PayableInvoice__r.Linking_Key__c
            		FROM c2g__codaTransactionLineItem__c
            		WHERE c2g__LineType__c = 'Account' AND c2g__Transaction__r.c2g__PayableInvoice__c in : pinIds]){
            		String key = tli.c2g__Transaction__r.c2g__PayableInvoice__r.Linking_Key__c;
            		if(tliMap.containsKey(key)){
            			List<Id> idList = tliMap.get(key);
            			idList.add(tli.Id);
            			tliMap.put(key, idList);
            		}
            		else{
            			List<Id> idList = new List<Id>();
            			idList.add(tli.Id);
            			tliMap.put(key, idList);	
            		}
            	}
            	for(String key : paymentMap.keySet()){
            		Id paymentId = paymentMap.get(key).Id;
            		List<Id> tliIds = tliMap.containsKey(key) ? tliMap.get(key) : null;

            		//call to FF API
            		List<Id> results = c2g.PaymentsPlusService.addToProposal(paymentId, tliIds);
            	}
            }

            //populate the ActionResult
            String successMessage = pin_req.autoCreatePayment == true ? 'Payable Invoices and related Payments successfully created' : 'Payable Invoices successfully created';
            resultsMap.put('Success', successMessage);
            returnResult.response = resultsMap;

        }catch (Exception e){
            System.debug(LoggingLevel.WARN, ' *** processExpensesController - ERROR = '+e.getMessage() + 'STACK: '+ e.getStackTraceString());
            String errMsg = e.getMessage();
            resultsMap.put('Error', errMsg);
            Database.rollback(sp);
            throw e;
        }

        return returnResult;

    }

    //-------------------------------------------------
    // Wrapper class for the Journal payload from the page
    //-------------------------------------------------
    public class PayableInvoiceRequest {
        public Date pinDate {get; set;}
        public Boolean autoPostInvoice {get;set;}
        public Boolean autoCreatePayment {get;set;}
        public String selectedBankAccountName {get;set;}
        public String selectedPaymentMedia {get;set;}
    }

    //-------------------------------------------------
    // Wrapper class for the Deposit payload from the page
    //-------------------------------------------------
    public class ExpenseRequest {
        public List<ExpenseWrapper> expensesToProcess {get; set;}
    }

    // ------------------------------------------------------------------------
    // Remote Action called from page to create journals from selected matters
    //  -  Param 1 = json formatted Ids of matters to deposit
    //  -  Param 2 = json formatted list of new journal fields
    // ------------------------------------------------------------------------
    @RemoteAction
    public static ActionResult processExpenses(String jsonPayload_matters, String invoicePayload) {
        System.debug(LoggingLevel.WARN, '*** ENTER processExpenses!');
        System.debug(LoggingLevel.WARN, '*** jsonPayload_matters = '+jsonPayload_matters);

        //parse the list of journal fields to create
        PayableInvoiceRequest pin_req = (PayableInvoiceRequest) JSON.deserialize(invoicePayload, processExpensesController.PayableInvoiceRequest.class);
        System.debug(LoggingLevel.WARN, '*** processExpenses - req = ' + pin_req);

        //parse the expense list to process
        ExpenseRequest exp_req = (ExpenseRequest) JSON.deserialize(jsonPayload_matters, processExpensesController.ExpenseRequest.class);
        System.debug(LoggingLevel.WARN, '*** processExpenses - exp_req = ' + exp_req);

        Set<Id> expenseIdsToProcess = new Set<Id>();
        for(ExpenseWrapper ew : exp_req.expensesToProcess){
            expenseIdsToProcess.add(ew.expenseId);
        }

        ActionResult actionResult = new ActionResult();
        actionResult = createPINfromExpenses(expenseIdsToProcess, pin_req);

        return actionResult;
    }
}