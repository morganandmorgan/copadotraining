/**
 *  mmmarketing_CampaignsSelector
 */
public with sharing class mmmarketing_CampaignsSelector
    extends mmlib_SObjectSelector
    implements mmmarketing_ICampaignsSelector
{
    public static mmmarketing_ICampaignsSelector newInstance()
    {
        return (mmmarketing_ICampaignsSelector) mm_Application.Selector.newInstance(Marketing_Campaign__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Marketing_Campaign__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Marketing_Campaign__c.Campaign_Name__c,
                Marketing_Campaign__c.Description__c,
                Marketing_Campaign__c.Domain_Value__c,
                Marketing_Campaign__c.Source_Campaign_ID__c,
                Marketing_Campaign__c.Technical_Key_UTM__c,
                Marketing_Campaign__c.Technical_Key_Source_Campaign_ID__c,
                Marketing_Campaign__c.utm_campaign__c
            };
    }

    public List<Marketing_Campaign__c> selectById(Set<Id> idSet)
    {
        return (List<Marketing_Campaign__c>) selectSObjectsById(idSet);
    }

    public List<Marketing_Campaign__c> selectByTechnicalKeys( Set<String> technicalKeysSet )
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Campaign__c.Technical_Key_UTM__c + ' in :technicalKeysSet'
                                                             + ' OR ' + Marketing_Campaign__c.Technical_Key_Source_Campaign_ID__c + ' in :technicalKeysSet')
                                                .toSOQL() );
    }
}