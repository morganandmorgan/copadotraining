@isTest
private class DocuSignStatusObtainUserInfoTest {
	
    @TestSetup
    private static void setup() {
        //
    }

    @isTest
    private static void testPopulatingProfileAndRole() {
        List<User> testUsers = [
            SELECT
                Id,
                Name,
                Profile.Name,
                UserRole.Name
            FROM
                User
            WHERE
                ProfileId != null
                AND UserRoleId != null
            LIMIT
                20
        ];

        System.assert(!testUsers.isEmpty(), 'No user records found');

        List<dsfs__DocuSign_Status__c> newStatuses = new List<dsfs__DocuSign_Status__c>();
        Map<String, User> userByUserName = new Map<String, User>();
        for (User u : testUsers) {
            userByUserName.put(u.Name, u);
            newStatuses.add(new dsfs__DocuSign_Status__c(
                    dsfs__Sender__c = u.Name,
                    Sender_Profile__c = null,
                    Sender_Role__c = null
                ));
        }

        Test.startTest();
        Database.insert(newStatuses);
        Test.stopTest();

        List<dsfs__DocuSign_Status__c> resultStatuses = [
            SELECT
                dsfs__Sender__c,
                Sender_Profile__c,
                Sender_Role__c
            FROM
                dsfs__DocuSign_Status__c
            WHERE
                Id IN :newStatuses
        ];

        System.assertEquals(newStatuses.size(), resultStatuses.size());
        for (dsfs__DocuSign_Status__c resultStatus : resultStatuses) {
            System.assertNotEquals(null, resultStatus.dsfs__Sender__c);

            User testUser = userByUserName.get(resultStatus.dsfs__Sender__c);
            System.assertNotEquals(null, testUser);

            System.assertEquals(testUser.Profile.Name, resultStatus.Sender_Profile__c);
            System.assertEquals(testUser.UserRole.Name, resultStatus.Sender_Role__c);
        }
    }
}