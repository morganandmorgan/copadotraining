@isTest
private class MultimatterInterstitialControllerTest {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Account injuredParty = TestUtil.createPersonAccount('Test', 'Injured Party');
        toInsert.add(injuredParty);

        Account caller = TestUtil.createPersonAccount('Test', 'Caller');
        toInsert.add(caller);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        Intake__c intake = new Intake__c(
                Incident__c = incident.Id,
                What_was_the_date_of_the_incident__c = Date.today(),
                City_of_incident__c = 'Orlando',
                State_of_incident__c = 'Florida',
                Description_of_incident__c = 'Sample description text',
                Client__c = client.Id,
                Injured_Party__c = injuredParty.Id,
                Caller__c = caller.Id,
                Caller_is_Injured_Party__c = null
            );
        toInsert.add(intake);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Intake__c> getIntakes() {
        return [
            SELECT
                RecordTypeId,
                Incident__c,
                What_was_the_date_of_the_incident__c,
                City_of_incident__c,
                State_of_incident__c,
                Description_of_incident__c,
                Client__c,
                Injured_Party__c,
                Caller__c,
                Caller_is_Injured_Party__c
            FROM
                Intake__c
            ORDER BY
                CreatedDate ASC
        ];
    }
	
    @isTest
    private static void redirectToNewIntake() {
        List<Intake__c> originalIntakes = getIntakes();
        System.assertEquals(1, originalIntakes.size());
        Intake__c originalIntake = originalIntakes.get(0);

        MultimatterInterstitialController controller = new MultimatterInterstitialController(new ApexPages.StandardController(new Intake__c(Id = originalIntake.Id)));

        Test.startTest();
        PageReference pr = controller.performRedirect();
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        System.assertEquals(2, requeriedIntakes.size());
        System.assertEquals(originalIntake.Id, requeriedIntakes.get(0).Id);
        Intake__c resultIntake = requeriedIntakes.get(1);

        Map<String, String> expectedParameters = new Map<String, String>{
            'intakeID' => resultIntake.Id
        };

        System.assertNotEquals(null, pr);
        System.assert(pr.getUrl().startsWith(Page.ContactSearch.getUrl() + '?'), pr.getUrl());
        System.assertEquals(expectedParameters, pr.getParameters());

        System.assertEquals(originalIntake.Incident__c, resultIntake.Incident__c);
        System.assertEquals(originalIntake.What_was_the_date_of_the_incident__c, resultIntake.What_was_the_date_of_the_incident__c);
        System.assertEquals(originalIntake.City_of_incident__c, resultIntake.City_of_incident__c);
        System.assertEquals(originalIntake.State_of_incident__c, resultIntake.State_of_incident__c);
        System.assertEquals(originalIntake.Description_of_incident__c, resultIntake.Description_of_incident__c);
        System.assertEquals(originalIntake.Client__c, resultIntake.Client__c);
        System.assertEquals(originalIntake.Injured_Party__c, resultIntake.Injured_Party__c);
        System.assertEquals(originalIntake.Caller__c, resultIntake.Caller__c);
        System.assertEquals('Yes', resultIntake.Caller_is_Injured_Party__c);
    }

    @isTest
    private static void redirectToNewIntake_SameRecordType_CCC() {
        List<Intake__c> originalIntakes = getIntakes();
        System.assertEquals(1, originalIntakes.size());
        Intake__c originalIntake = originalIntakes.get(0);

        originalIntake.RecordTypeId = RecordTypeUtil.getRecordTypeIDByDevName('Intake__c', 'CCC');
        Database.update(originalIntake);

        MultimatterInterstitialController controller = new MultimatterInterstitialController(new ApexPages.StandardController(new Intake__c(Id = originalIntake.Id)));

        Test.startTest();
        PageReference pr = controller.performRedirect();
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        System.assertEquals(2, requeriedIntakes.size());
        System.assertEquals(originalIntake.Id, requeriedIntakes.get(0).Id);
        Intake__c resultIntake = requeriedIntakes.get(1);

        System.assertEquals(originalIntake.RecordTypeId, resultIntake.RecordTypeId);
    }

    @isTest
    private static void redirectToNewIntake_SameRecordType_MassTort() {
        List<Intake__c> originalIntakes = getIntakes();
        System.assertEquals(1, originalIntakes.size());
        Intake__c originalIntake = originalIntakes.get(0);

        originalIntake.RecordTypeId = RecordTypeUtil.getRecordTypeIDByDevName('Intake__c', 'Mass_Tort');
        Database.update(originalIntake);

        MultimatterInterstitialController controller = new MultimatterInterstitialController(new ApexPages.StandardController(new Intake__c(Id = originalIntake.Id)));

        Test.startTest();
        PageReference pr = controller.performRedirect();
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        System.assertEquals(2, requeriedIntakes.size());
        System.assertEquals(originalIntake.Id, requeriedIntakes.get(0).Id);
        Intake__c resultIntake = requeriedIntakes.get(1);

        System.assertEquals(originalIntake.RecordTypeId, resultIntake.RecordTypeId);
    }
}