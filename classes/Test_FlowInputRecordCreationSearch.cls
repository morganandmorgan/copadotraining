@isTest
public class Test_FlowInputRecordCreationSearch {

    @TestSetup
    static void setup() {
        Account testAccount = TestUtil.createAccount('Test', 'Morgan_Morgan_Businesses');
        insert testAccount;
    }

    @isTest public static void unit_test_one() {
        Test.startTest();
            String retrieveData = FlowInputRecordCreationSearch.retrieveRecordSearchData('Account', 'General_Account_Info');
            System.assertNotEquals(retrieveData, NULL);
            try {
                retrieveData = FlowInputRecordCreationSearch.retrieveRecordSearchData('Account_x', 'General_Account_Info');
            } catch(Exception ex) {}
            List<FlowInputRecordCreationSearch.FieldSetWrapper> fieldWrapperList = FlowInputRecordCreationSearch.retieveFieldSet('Account', 'General_Account_Info');
            System.assertNotEquals(fieldWrapperList, NULL);
            List<FlowInputRecordCreationSearch.SobjectResultDataWrapper> resultWrapperList = FlowInputRecordCreationSearch.searchedRecords('Account', 'General_Account_Info', '');
            System.assertNotEquals(resultWrapperList, NULL);
            List<Account> accountList = [SELECT Id FROM Account];
            String fetchRecordString = FlowInputRecordCreationSearch.fetchSelectedRecord(accountList[0].Id, 'Account');
            System.assertNotEquals(fetchRecordString, NULL);
            try {
                fetchRecordString = FlowInputRecordCreationSearch.fetchSelectedRecord(accountList[0].Id, 'Account_x');
            } catch(Exception ex) {}
            new FlowInputRecordCreationSearch.ReferenceFieldWrapper('test','test');
        Test.stopTest();
    }
}