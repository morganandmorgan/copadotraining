/*==================================================================================
Name            : ProcessDeposits_Batch
Author          : CLD Partners
Created Date    : June 2019
Description     : Batch and implemenation for passing pin invoice line items to create litify expenses.
==================================================================================*/
global class ProcessDeposits_Batch implements Database.Batchable<sObject>{

    global Set<Id> depositIds;
	global Boolean autoPost;
	global Boolean reverse;
	global String jnlDescription;
	

    /*============================================================================
    * CONSTRUCTOR
    =============================================================================*/
    global ProcessDeposits_Batch (Set<Id> selectedDepositIds, Boolean selectedAutoPost, Boolean selectedReverse, String selectedJnlDescription) {
        depositIds = selectedDepositIds;
		autoPost = selectedAutoPost;
		reverse = selectedReverse;
		jnlDescription = selectedJnlDescription;
    }

    /*============================================================================
    * START
    =============================================================================*/
    global Database.QueryLocator start(Database.BatchableContext BC) {

		String soql = 'SELECT Id'+
			' FROM Deposit__c'+
			' WHERE Id in : depositIds';

        if(Test.isRunningTest() == TRUE){
    		soql = soql + ' LIMIT 1';
        }
        return Database.getQueryLocator(soql);
    }

    /*============================================================================
    * EXECUTE
    =============================================================================*/
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<Id> depositsToProcess = new Set<Id>();

        for(Sobject s : scope){
            Deposit__c dep = (Deposit__c)s;
			depositsToProcess.add(dep.Id);
		}
        
        //Call the deposit processing method
		ProcessDepositsController.ActionResult actionResult = ProcessDepositsController.createJournalFromDeposits(depositsToProcess, autoPost, jnlDescription, reverse);
    }

    /*============================================================================
    * FINISH
    =============================================================================*/
    global void finish(Database.BatchableContext BC) {
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id = :BC.getJobId()];

        // log Apex job details
        String apexJobDetails = '**** ProcessDeposits_Batch Finish()  status: ' + a.status + ' Total Jobs: ' + a.TotalJobItems + ' Errors: ' + a.NumberOfErrors;
        system.debug(apexJobDetails);
    }
}