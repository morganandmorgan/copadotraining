@RestResource(urlMapping='/attorneyreports/*')
global with sharing class AttorneyReports
{
    global class AttorneyReportDTO
    {
	    public DateTime created_date { get; set; }
	    public String first_name { get; set; }
	    public String last_name { get; set; }
	    public String intake_number { get; set; }
	    public String intake_status { get; set; }
    }

    @HttpGet
    global static List<AttorneyReportDTO> doGet()
    {
        String apiKey = RestContext.request.params.get('apiKey');

        if (apiKey != 'b33f8613bf7b')
        {
            RestContext.response.statusCode = 401;
            return null;
        }

        Date startDate;
        Date endDate;
        Id attorneyId;

        try
        {
            startDate = Date.parse(RestContext.request.params.get('startDate'));
            endDate = Date.parse(RestContext.request.params.get('endDate'));
            attorneyId = RestContext.request.params.get('attorneyId');
        }
        catch (Exception ex)
        {
            RestContext.response.statusCode = 400;
            return null;
        }

        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByGeneratingAttorneyAndCreatedDateRange( new set<Id>{ attorneyId }, startDate, endDate );

        List<AttorneyReportDTO> resp = new List<AttorneyReportDTO>();

        for (Intake__c i : intakes)
        {
            AttorneyReportDTO dto = new AttorneyReportDTO();

            dto.created_date = i.CreatedDate;
            dto.first_name = i.Client__r.FirstName;
            dto.last_name = i.Client__r.LastName;
            dto.intake_number = i.Name;
            dto.intake_status = i.Status__c;

            resp.add(dto);
        }

        return resp;
    }
}