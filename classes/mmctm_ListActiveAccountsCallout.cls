/**
 *  mmctm_ListActiveAccountsCallout
 *
 *  @usage mmctm_ListActiveAccountsCallout callout = new mmctm_ListActiveAccountsCallout();
 *  @usage callout.setAuthorization( (new mmctm_APIAuthorization()).setAccessKey( accessKey ).setSecretKey( secretKey ) ).execute();
 *  @usage system.debug( callout.getCTMAccounts() ) ;
 *
 */
public class mmctm_ListActiveAccountsCallout
    extends mmctm_BaseGETV1Callout
{
    private mmctm_ListActiveAccountsCallout.Response resp = new mmctm_ListActiveAccountsCallout.Response();

    public override map<string, string> getParamMap()
    {
        map<string, string> paramMap = new map<string, string>();

        paramMap.put('names','1');
        paramMap.put('all','1');

        return paramMap;
    }

    public override string getPathSuffix()
    {
        return '/accounts';
    }

    public override mmlib_BaseCallout execute()
    {
        httpResponse httpResp = super.executeCallout();

        string jsonResponseBody = httpResp.getBody();

        if ( this.isDebugOn )
        {
            system.debug( httpResp.getBody() );
            //system.debug( json.deserializeUntyped( jsonResponseBody ) );
        }

        resp = (mmctm_ListActiveAccountsCallout.Response) json.deserialize( jsonResponseBody, mmctm_ListActiveAccountsCallout.Response.class );

        return this;
    }

    public class Response
        implements CalloutResponse
    {
        public list<mmctm_ListActiveAccountsCallout.CTMAccount> accounts = new list<mmctm_ListActiveAccountsCallout.CTMAccount>();

        public integer getTotalNumberOfRecordsFound()
        {
            return this.accounts == null ? 0 : this.accounts.size();
        }
    }

    public class CTMAccount
    {
        public string id;
        public string name;
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public list<mmctm_ListActiveAccountsCallout.CTMAccount> getCTMAccounts()
    {
        return this.resp.accounts != null ? this.resp.accounts : new list<mmctm_ListActiveAccountsCallout.CTMAccount>();
    }
}