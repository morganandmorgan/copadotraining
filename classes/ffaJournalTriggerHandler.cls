/*============================================================================
Name            : ffaJournalTriggerHandler
Author          : CLD
Created Date    : July 2018
Description     : Handler class for Journal trigger operations
=============================================================================*/
public with sharing class ffaJournalTriggerHandler {
	
	public ffaJournalTriggerHandler() {
		
	}

	/*---------------------------------------------------------
	Creates litify expense from a provided trigger.new list and trigger.oldMap
	----------------------------------------------------------*/
	public static void createLitifyExpense(List<c2g__codaJournal__c> newList, Map<Id, c2g__codaJournal__c> oldMap){

		Set<Id> jnlsToProcess = new Set<Id>();
		List<litify_pm__Expense__c> expenseInsertList = new List<litify_pm__Expense__c>();
		List<c2g__codaJournalLineItem__c> jnliUpdateList = new List<c2g__codaJournalLineItem__c>();

		//collect the journals that have been posted:
		for(c2g__codaJournal__c jnl : newList){
			c2g__codaJournal__c oldJnl = oldMap.get(jnl.Id);
			if((jnl.c2g__JournalStatus__c == 'Complete' && oldJnl.c2g__JournalStatus__c != 'Complete' && jnl.Create_Litify_Expenses__c == true)
				||
				(jnl.Create_Litify_Expenses__c == true && oldJnl.Create_Litify_Expenses__c != true && jnl.c2g__JournalStatus__c == 'Complete')
				){
				jnlsToProcess.add(jnl.Id);
			}
		}

		//query for journal lines that should have an expense created:
		for(c2g__codaJournalLineItem__c jnli : [
			SELECT Id,
				Expense_Type__c,
				Matter__c,
				c2g__Journal__r.c2g__JournalDate__c,
				c2g__LineDescription__c,
				c2g__Value__c,
				Litify_Expense_Created__c
			FROM c2g__codaJournalLineItem__c
			WHERE c2g__Journal__c in : jnlsToProcess
			AND Litify_Expense_Created__c = false
			AND Matter__c != null
			AND Expense_Type__c != null]){

			litify_pm__Expense__c newExpense = new litify_pm__Expense__c(
				litify_pm__Matter__c = jnli.Matter__c,
				litify_pm__ExpenseType2__c = jnli.Expense_Type__c,
				litify_pm__Date__c = jnli.c2g__Journal__r.c2g__JournalDate__c,
				litify_pm__Note__c = jnli.c2g__LineDescription__c,
				litify_pm__Status__c = 'Paid',
				litify_pm__Amount__c = jnli.c2g__Value__c);
			expenseInsertList.add(newExpense);

			jnli.Litify_Expense_Created__c = true;
			jnliUpdateList.add(jnli);
		}

		//insert the expenses:
		insert expenseInsertList;

		//update the journal line flag:
		update jnliUpdateList;

	}




}