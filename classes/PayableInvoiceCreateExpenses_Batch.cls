/*==================================================================================
Name            : PayableInvoiceCreateExpenses_Batch
Author          : CLD Partners
Created Date    : June 2019
Description     : Batch and implemenation for passing pin invoice line items to create litify expenses.
==================================================================================*/
global class PayableInvoiceCreateExpenses_Batch implements Database.Batchable<sObject>, Schedulable{

    global Set<Id> pinIds;    

    /*============================================================================
    * CONSTRUCTOR
    =============================================================================*/
	global PayableInvoiceCreateExpenses_Batch () {
    }
    global PayableInvoiceCreateExpenses_Batch (Set<Id> selectedpinIds) {
        pinIds = selectedPinIds;
    }

    /*============================================================================
    * START
    =============================================================================*/
    global Database.QueryLocator start(Database.BatchableContext BC) {

		String soql = 'SELECT Id,'+
				'Expense_Type__c,'+
				'Matter__c,'+
				'c2g__NetValue__c,'+
				'c2g__PurchaseInvoice__c,'+
				'c2g__PurchaseInvoice__r.c2g__Account__c,'+
				'c2g__PurchaseInvoice__r.c2g__InvoiceDate__c,'+
				'c2g__PurchaseInvoice__r.c2g__InvoiceDescription__c,'+
				'c2g__PurchaseInvoice__r.c2g__AccountInvoiceNumber__c,'+
				'c2g__LineDescription__c,'+
				'Litify_Expense_Created__c'+
			' FROM c2g__codaPurchaseInvoiceExpenseLineItem__c'+
			' WHERE Litify_Expense_Created__c = false'+
			' AND c2g__PurchaseInvoice__r.Create_Litify_Expenses__c = true'+
			' AND Matter__c != null'+
			' AND Expense_Type__c != null';
		if(pinIds != null){
			soql += ' AND c2g__PurchaseInvoice__c in : pinIds';
		}
        if(Test.isRunningTest() == TRUE){
    		soql = soql + ' LIMIT 1';
        }
        return Database.getQueryLocator(soql);
    }

    /*============================================================================
    * EXECUTE
    =============================================================================*/
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<Id> expenseIds = new Set<Id>();

		List<litify_pm__Expense__c> expenseInsertList = new List<litify_pm__Expense__c>();
		List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLinesToUpdate = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();

        for(Sobject s : scope){
            c2g__codaPurchaseInvoiceExpenseLineItem__c pinLine = (c2g__codaPurchaseInvoiceExpenseLineItem__c)s;

			//create the litify expenses and update the expense / pin records
			litify_pm__Expense__c newExpense = new litify_pm__Expense__c(
				litify_pm__Matter__c = pinLine.Matter__c,
				litify_pm__ExpenseType2__c = pinLine.Expense_Type__c,
				litify_pm__Date__c = pinLine.c2g__PurchaseInvoice__r.c2g__InvoiceDate__c,
				litify_pm__Note__c = pinLine.c2g__LineDescription__c,
				litify_pm__Status__c = 'Paid',
				Payable_Invoice__c = pinLine.c2g__PurchaseInvoice__c,
				Payable_Invoice_Created__c = true,
				Vendor_Invoice_Number__c = pinLine.c2g__PurchaseInvoice__r.c2g__AccountInvoiceNumber__c,
				PayableTo__c = pinLine.c2g__PurchaseInvoice__r.c2g__Account__c,
				litify_pm__Amount__c = pinLine.c2g__NetValue__c);
			expenseInsertList.add(newExpense);

			pinLine.Litify_Expense_Created__c = true;
			pinLinesToUpdate.add(pinLine);
        }

		Savepoint sp1 = Database.setSavepoint();
		Try{
			//insert the expenses:
			insert expenseInsertList;
			//update the pin lines:
			update pinLinesToUpdate;
		}
		Catch(Exception e){
			system.debug('ERROR - '+e.getMessage() + e.getStackTraceString());
			Database.rollback(sp1);
			throw e;

			//TODO: Add error handling into the Error Log object
		}
    }

    /*============================================================================
    * FINISH
    =============================================================================*/
    global void finish(Database.BatchableContext BC) {
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id = :BC.getJobId()];

        // log Apex job details
        String apexJobDetails = '**** PayableInvoiceCreateExpenses_Batch Finish()  status: ' + a.status + ' Total Jobs: ' + a.TotalJobItems + ' Errors: ' + a.NumberOfErrors;
        system.debug(apexJobDetails);
    }

	/*------------------------------------------------------------------------------
    * SCHEDULE EXECUTE
   	-------------------------------------------------------------------------------*/    
    global void execute(SchedulableContext sc) {
		c2g__codaAccountingSettings__c settings = c2g__codaAccountingSettings__c.getOrgDefaults();
        Integer BATCH_SIZE = settings.Create_Expenses_From_PINs_Batch_Size__c != null ? settings.Create_Expenses_From_PINs_Batch_Size__c.intValue() : 5;
        PayableInvoiceCreateExpenses_Batch b = new PayableInvoiceCreateExpenses_Batch();
        Id batchID = database.executebatch(b, BATCH_SIZE);
    }
}