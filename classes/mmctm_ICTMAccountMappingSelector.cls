/**
 *  mmctm_ICTMAccountMappingSelector
 */
public interface mmctm_ICTMAccountMappingSelector extends mmlib_ISObjectSelector
{
    List<CallTrackingMetricsAccountMapping__mdt> selectById(Set<Id> idSet);
    List<CallTrackingMetricsAccountMapping__mdt> selectAll();
}