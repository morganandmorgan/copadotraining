@isTest
private class mmdocusign_CreateEnvelopeCalloutTest
{
	@isTest
	static void HappyPath()
	{
        Docusign_Integration__c settings = new Docusign_Integration__c();
        settings.DocusignAccountId__c = '598378947';
        settings.Username__c = 'fdlsjfeio@null.com';
        settings.Password__c = 'kdsjflksdjfl';
        settings.IntegratorKey__c = 'dsfljsljf';
        settings.Host__c = 'test.docusign.net';
        settings.Enabled__c = true;
        settings.EnabledInSandbox__c = true;
        settings.ReturnUrl__c = 'https://null.com';
		settings.Signing_Ceremony_Landing_Page_Template__c = '';
        insert settings;

		Map<String, String> paramMap = new Map<String, String>();
		paramMap.put('envelopeId', mmlib_Utils.generateGuid());
		paramMap.put('uri', '/envelopes/' + paramMap.get('envelopeId'));
		paramMap.put('statusDateTime', mmlib_Utils.formatSoqlDatetimeUTC(DateTime.now()));
		paramMap.put('status', 'sent');

		System.debug('<ojs> responseBody:\n' + Json.serialize(paramMap));

        Test.setMock(HttpCalloutMock.class, new mmdocusign_RestApiCalloutMock(Json.serialize(paramMap), 201));

        Test.startTest();

        mmdocusign_CreateEnvelopeResponse resp =
            (mmdocusign_CreateEnvelopeResponse)
            new mmdocusign_CreateEnvelopeCallout(
                '1234-56-7890',
				mmlib_Utils.generateGuid(),
				'arya.stark@winterfell.com',
				'Arya Stark'
            )
            .execute()
            .getResponse();

		Test.stopTest();

        System.assertEquals(paramMap.get('envelopeId'), resp.getEnvelopeId());
	}

    @isTest
    static void CalloutReturnsFailingStatusCode()
	{
        Docusign_Integration__c settings = new Docusign_Integration__c();
        settings.DocusignAccountId__c = '598378947';
        settings.Username__c = 'fdlsjfeio@null.com';
        settings.Password__c = 'kdsjflksdjfl';
        settings.IntegratorKey__c = 'dsfljsljf';
        settings.Host__c = 'test.docusign.net';
        settings.Enabled__c = true;
        settings.EnabledInSandbox__c = true;
        settings.ReturnUrl__c = 'https://null.com';
		settings.Signing_Ceremony_Landing_Page_Template__c = '';
        insert settings;

		Map<String, String> paramMap = new Map<String, String>();
		paramMap.put('envelopeId', mmlib_Utils.generateGuid());
		paramMap.put('uri', '/envelopes/' + paramMap.get('envelopeId'));
		paramMap.put('statusDateTime', mmlib_Utils.formatSoqlDatetimeUTC(DateTime.now()));
		paramMap.put('status', 'sent');

		System.debug('<ojs> responseBody:\n' + Json.serialize(paramMap));

        Test.setMock(HttpCalloutMock.class, new mmdocusign_RestApiCalloutMock(Json.serialize(paramMap), 400));

        Test.startTest();

        try
        {
			mmdocusign_CreateEnvelopeResponse resp =
				(mmdocusign_CreateEnvelopeResponse)
				new mmdocusign_CreateEnvelopeCallout(
					'1234-56-7890',
					mmlib_Utils.generateGuid(),
					'arya.stark@winterfell.com',
					'Arya Stark'
				)
				.execute()
				.getResponse();
                System.assert(false, 'The operation should have generated an exception.');
        }
        catch (mmdocusign_IntegrationExceptions.IntegrationBaseException exc)
        {
            // An exception is expected.
        }

        Test.stopTest();
	}
}