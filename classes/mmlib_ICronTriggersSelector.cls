/**
 *  mmlib_ICronTriggersSelector
 */
public interface mmlib_ICronTriggersSelector extends mmlib_ISObjectSelector
{
    List<CronTrigger> selectById(Set<Id> idSet);
    List<CronTrigger> selectScheduledApexByName( Set<String> nameSet );
    List<CronTrigger> selectWaitingScheduledApexByName( Set<String> nameSet );
}