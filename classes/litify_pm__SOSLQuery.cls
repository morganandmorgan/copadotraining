/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SOSLQuery implements litify_pm.IQuery {
    global litify_pm.SOSLQuery AndReturning(litify_pm.ReturningClause c) {
        return null;
    }
    global List<List<SObject>> Execute() {
        return null;
    }
    global SObject First() {
        return null;
    }
    global static litify_pm.SOSLQuery NewInstance(String query) {
        return null;
    }
    global litify_pm.SOSLQuery OnlyReturning(litify_pm.ReturningClause c) {
        return null;
    }
    global litify_pm.SOSLQuery SetLimit(Integer l) {
        return null;
    }
global enum SearchGroup {ALL_FIELDS, EMAIL_FIELDS, NAME_FIELDS, PHONE_FIELDS, SIDEBAR_FIELDS}
}
