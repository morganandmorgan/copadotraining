@isTest
public class ReasonForCall_ControllerTest {

	@IsTest
    private static void testReasonForCall_Controller() {

       	Test.startTest();
        
        String jsonString = ReasonForCall_Controller.returnRecords(System.Label.LIVRMetadataName);
        List<ReasonForCall_Controller.QuestionMetaData> questionDataObject = (List<ReasonForCall_Controller.QuestionMetaData>)JSON.deserialize(jsonString, List<ReasonForCall_Controller.QuestionMetaData>.class);
       
       	ReasonForCall_Controller.responseActions(questionDataObject[0].serializeContent.actionTokens[0]);
        ReasonForCall_Controller.responseActions(null);
        
        Test.stopTest();

    }
}