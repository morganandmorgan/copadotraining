public interface mmmarketing_ITrackingInformations extends fflib_ISObjectDomain
{
    void autoLinkToMarketingInformationIfRequired();
    void autoLinkToMarketingInformationIfRequired( mmlib_ISObjectUnitOfWork uow );
    void autoLinkToMarketingInformationIfRequiredInQueueableMode();
}