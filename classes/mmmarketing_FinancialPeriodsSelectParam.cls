/**
 *  mmmarketing_FinancialPeriodsSelectParam
 */
public class mmmarketing_FinancialPeriodsSelectParam
{
    private Set<Date> datesSet = new Set<date>();
    private Set<String> keywordSet = new Set<String>();
    private Set<String> campaignSet = new Set<String>();
    private Set<String> sourceSet = new Set<String>();
    private Set<String> domainSet = new Set<String>();

	public Set<Date> getDates() { return this.datesSet; }
	public Set<String> getKeywords() { return this.keywordSet; }
	public Set<String> getCampaigns() { return this.campaignSet; }
	public Set<String> getSources() { return this.sourceSet; }
	public Set<String> getDomains() { return this.domainSet; }

    public mmmarketing_FinancialPeriodsSelectParam setDates( Set<Date> datesSet )
    {
        this.datesSet = datesSet;

        return this;
    }

    public mmmarketing_FinancialPeriodsSelectParam setKeywords( Set<String> keywordSet )
    {
        this.keywordSet = keywordSet;

        return this;
    }

    public mmmarketing_FinancialPeriodsSelectParam setCampaigns( Set<String> campaignSet )
    {
        this.campaignSet = campaignSet;

        return this;
    }

    public mmmarketing_FinancialPeriodsSelectParam setSources( Set<String> sourceSet )
    {
        this.sourceSet = sourceSet;

        return this;
    }

    public mmmarketing_FinancialPeriodsSelectParam setDomains( Set<String> domainSet )
    {
        this.domainSet = domainSet;

        return this;
    }



}