/*============================================================================/
* TransactionLineItem_Service
* @description Service class for Matter Transfers
* @author Brian Krynitsky
* @date Apr 2019
=============================================================================*/
public without sharing class MatterTransfer_Service {

	//Constructor
	public MatterTransfer_Service() {
	}

	// Methods
	/*-----------------------------------------------------------------------------
    Method to set the  Transfer Deposit and Intercompany Sales Invoice
    -----------------------------------------------------------------------------*/
	public void setFromFields(List<Matter_Transfer__c> newTrigger) {

		mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();

		Map<Id, litify_pm__Matter__c> matter_Map = new Map<Id, litify_pm__Matter__c>();

		//collect matter ids to query
		Set<Id> matterIds = new Set<Id>();
		for(Matter_Transfer__c trans : newTrigger){
			matterIds.add(trans.Matter__c);
		}
		
		//store the matters in a map
		List<litify_pm__Matter__c> matterList = matterSelector.selectById(matterIds);
		for(litify_pm__Matter__c m : matterList){
			matter_Map.put(m.Id, m);
		}

		//collect the current companies for the user
		List<c2g__codaCompany__c> currentCompanies = FFAUtilities.getCurrentCompanies();

		for(Matter_Transfer__c transfer : newTrigger){
			
			//check to see if the user's current company is the from company of the matter	
			if(currentCompanies.size() != 1){
				transfer.AddError('Attention: you must select the "Transferring From" Company as your current accounting company');
			}
			else{
				Id currentCompanyId = currentCompanies[0].Id;
				if(currentCompanyId != matter_Map.get(transfer.Matter__c).AssignedToMMBusiness__r.FFA_Company__c){
					transfer.AddError('Attention: you must select the "Transferring From" Company as your current accounting company');	
				}
			}

			//set the "from" fields
			litify_pm__Matter__c m = matter_Map.get(transfer.Matter__c);
			transfer.From_MM_Business__c = m.AssignedToMMBusiness__c;
			transfer.From_MM_Office__c = m.Assigned_Office_Location__c;
			
		}
	}

	/*-----------------------------------------------------------------------------
    Method to create Transfer Deposit and Intercompany Sales Invoice
    -----------------------------------------------------------------------------*/
	public void processTransfer(List<Matter_Transfer__c> newTrigger) {

		//selector classes
		mmmatter_ExpensesSelector       expSelector        = new mmmatter_ExpensesSelector();
		mmmatter_MattersSelector        matterSelector     = new mmmatter_MattersSelector();
		Company_Selector                companySelector    = new Company_Selector();
		GeneralLedgerAccount_Selector   glaSelector        = new GeneralLedgerAccount_Selector();

		//custom settings
		c2g__codaAccountingSettings__c acctSettings = c2g__codaAccountingSettings__c.getOrgDefaults();

		Map<Id, litify_pm__Matter__c> matter_Map = new Map<Id, litify_pm__Matter__c>();
		Map<Id,Decimal> transferExpenseTotal_Map = new Map<Id,Decimal>();
		Map<Id, c2g__codaCompany__c> companyMap = new Map<Id, c2g__codaCompany__c>();
		Map<String, Id> acctCurrencyMap = new Map<String, Id>();
		Map<String, Id> glaMap = new Map<String, Id>();

		c2g.CODAAPIJournalTypes_12_0.Journal jnlWrapper = new c2g.CODAAPIJournalTypes_12_0.Journal();
		List<c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem> jnlLineList = new List<c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem>();
		c2g.CODAAPICommon_10_0.Context compWrapper = new c2g.CODAAPICommon_10_0.Context();

		Set<Id> companyAccountIds = new Set<Id>();
		Set<Id> activeCompanyIds = new Set<Id>();
		Set<String> productNameSet = new Set<String>();
		Set<String> glaCodeSet = new Set<String>();

		List<Deposit__c> newDepositList = new List<Deposit__c>();
		List<Finance_Request__c> newFinanceRequestList = new List<Finance_Request__c>();

		c2g__codaPurchaseInvoice__c newPin;
		List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLineList = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();

		Id transferDepositRT_Id = SObjectType.Deposit__c.getRecordTypeInfosByName().get('Transfer').getRecordTypeId();
		Id transferFinRequestRT_Id = SObjectType.Finance_Request__c.getRecordTypeInfosByName().get('Matter Transfer Request').getRecordTypeId();

		Boolean targetCompanyActive = false;

		Set<Id> matterIds = new Set<Id>();
		for(Matter_Transfer__c trans : newTrigger){
			matterIds.add(trans.Matter__c);
			companyAccountIds.add(trans.To_MM_Business__c);
			companyAccountIds.add(trans.From_MM_Business__c);
		}

		//get values from the settings:
		String arGLACode = acctSettings.Client_Cost_Receivable_GLA_Code__c;
		glaCodeSet.add(arGLACode);
		
		//query the FFA GLAs
		for(c2g__codaGeneralLedgerAccount__c gla : glaSelector.selectByReportingCodes(glaCodeSet)){
			glaMap.put(gla.c2g__ReportingCode__c, gla.id);
		}

		//query the active FFA companies
		for(c2g__codaCompany__c company : companySelector.selectByAccountId(companyAccountIds)){
			companyMap.put(company.c2g__IntercompanyAccount__c, company);
		}

		for(c2g__codaAccountingCurrency__c acctCurr : [SELECT c2g__OwnerCompany__c, Id FROM c2g__codaAccountingCurrency__c]){
			activeCompanyIds.add(acctCurr.c2g__OwnerCompany__c);
			acctCurrencyMap.put(acctCurr.c2g__OwnerCompany__c, acctCurr.Id);
		}

		//store the matters in a map
		List<litify_pm__Matter__c> matterList = matterSelector.selectById(matterIds);
		for(litify_pm__Matter__c m : matterList){
			matter_Map.put(m.Id, m);
		}
		
		//sum up the expenses 
		for(litify_pm__Expense__c expense : expSelector.selectByMatterId(matterIds)){
			if(expense.CostType__c == 'HardCost' && expense.Payable_Invoice_Created__c == true){
				Id key = expense.litify_pm__Matter__c;
				if(!transferExpenseTotal_Map.containsKey(key)){
					transferExpenseTotal_Map.put(key, expense.litify_pm__Amount__c);
				}
				else{
					Decimal d = transferExpenseTotal_Map.get(key) + expense.litify_pm__Amount__c;
					transferExpenseTotal_Map.put(key, d);
				}
			}
		}

		//start loop into matter transfers to create Journals and Deposits
		for(Matter_Transfer__c transfer : newTrigger){
			
			//update the matter first:
			litify_pm__Matter__c matter = matter_Map.get(transfer.Matter__c);
			String fromBusinessName = matter.AssignedToMMBusiness__r.Name;
			matter.AssignedToMMBusiness__c = transfer.To_MM_Business__c;
			matter.Assigned_Office_Location__c = transfer.To_MM_Office__c;
			matter_Map.put(transfer.Matter__c, matter);

			system.debug('companyMap = '+ companyMap);
			system.debug('transfer.From_MM_Business__c = '+ transfer.From_MM_Business__c);
			system.debug('transfer.To_MM_Business__c = '+ transfer.To_MM_Business__c);

			c2g__codaCompany__c targetCompany = companyMap.get(transfer.To_MM_Business__c);
			c2g__codaCompany__c sourceCompany = companyMap.get(transfer.From_MM_Business__c);

			// create the deposit
			if(transferExpenseTotal_Map.containsKey(transfer.Matter__c) && transferExpenseTotal_Map.get(transfer.Matter__c)>0){

				Deposit__c newDeposit = new Deposit__c(
					RecordTypeId              = transferDepositRT_Id,
					Matter__c                 = matter.Id,
					AssignedToMMBusiness__c   = transfer.From_MM_Business__c,
					Deposit_Description__c    = 'Payment of Hard Cost total paid by '+fromBusinessName,
					Amount__c                 = transferExpenseTotal_Map.get(transfer.Matter__c),
					Check_Date__c             = null,
					Source__c                 = 'Operating',
					Matter_Transfer__c        = transfer.Id,
					Operating_Cash_Account__c = null,
					Cost_Account__c           = sourceCompany.Cost_Bank_Account__c,
					Override_GLA__c           = targetCompany.Intercompany_GLA__c);
				newDepositList.add(newDeposit);

				Finance_Request__c newFinReq = new Finance_Request__c(
					RecordTypeId              = transferFinRequestRT_Id,
					Matter__c                 = matter.Id,
					Matter_Transfer__c        = transfer.Id,
					Request_Status__c         = 'In Process',
					Memo__c                   = 'Transfer of hard costs on Matter - '+matter.ReferenceNumber__c,
					Deposit_Info__c           = 'Awaiting Deposit check from  - '+targetCompany.Name,
					Amount_of_Deposit__c      = transferExpenseTotal_Map.get(transfer.Matter__c));
					if(sourceCompany.Operational_Queue_Id__c != null){
						newFinReq.OwnerId = sourceCompany.Operational_Queue_Id__c;
					}
				newFinanceRequestList.add(newFinReq);

				//if the to company is active then create a payable invoice in the target company and a Journal in the source company.
				if(activeCompanyIds.contains(targetCompany.Id)){

					newPin = new c2g__codaPurchaseInvoice__c(
						Litify_Matter__c             = matter.Id,
						c2g__Account__c              = transfer.From_MM_Business__c,
						Matter_Transfer__c           = transfer.Id,
						c2g__AccountInvoiceNumber__c = transfer.Name,
						c2g__OwnerCompany__c         = targetCompany.Id,
						OwnerId                      = targetCompany.OwnerId,
						c2g__Dimension1__c           = matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
						c2g__Dimension2__c           = matter.litify_pm__Case_Type__r.Dimension_2__c,
						c2g__Dimension3__c           = matter.litify_pm__Case_Type__r.Dimension_3__c,
						c2g__InvoiceDescription__c   = 'Transfer of hard costs on Matter - '+matter.ReferenceNumber__c,
						c2g__DeriveDueDate__c        = false,
						c2g__DeriveCurrency__c       = true,
						c2g__DerivePeriod__c         = true,
						c2g__InvoiceDate__c          = transfer.CreatedDate.Date(),
						c2g__DueDate__c              = transfer.CreatedDate.Date()
            		);

					c2g__codaPurchaseInvoiceExpenseLineItem__c expenseLine = new c2g__codaPurchaseInvoiceExpenseLineItem__c(
                        Matter__c                    = matter.Id,
                        c2g__Dimension1__c           = matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
                        c2g__Dimension2__c           = matter.litify_pm__Case_Type__r.Dimension_2__c,
                        c2g__Dimension3__c           = matter.litify_pm__Case_Type__r.Dimension_3__c,
                        c2g__GeneralLedgerAccount__c = glaMap.get(arGLACode),
                        c2g__NetValue__c             = transferExpenseTotal_Map.get(transfer.Matter__c).setScale(2),
                        c2g__DeriveLineNumber__c     = TRUE,
                        c2g__LineDescription__c      = 'Transfer of hard costs on Matter - '+matter.ReferenceNumber__c);
                    pinLineList.add(expenseLine);

				}
				//In either scenario (target company on FFA or not) this journal on the source company is created:
				compWrapper = new c2g.CODAAPICommon_10_0.Context();
				compWrapper.CompanyName = sourceCompany.Name;
				
				jnlWrapper = FFAUtilities.initFFAPIJournal(sourceCompany, acctCurrencyMap.get(sourceCompany.Id));
				jnlWrapper.JournalDescription = 'Transfer of hard costs on Matter - '+matter.ReferenceNumber__c;
				jnlWrapper.Reference = 'Transfer of hard costs on Matter';
				jnlWrapper.JournalDate = transfer.CreatedDate.Date();

				//set custom fields:
				List<c2g.CODAAPIJournalTypes_12_0.CustomField> jnlCustomFields = new List<c2g.CODAAPIJournalTypes_12_0.CustomField>();
					
					//matter custom field:
					c2g.CODAAPIJournalTypes_12_0.CustomField matterField = new c2g.CODAAPIJournalTypes_12_0.CustomField();
					matterField.FieldName = 'Litify_Matter__c';
					matterField.Value = matter.Id;
					jnlCustomFields.add(matterField);

					//matter custom field:
					c2g.CODAAPIJournalTypes_12_0.CustomField matterTransferField = new c2g.CODAAPIJournalTypes_12_0.CustomField();
					matterTransferField.FieldName = 'Matter_Transfer__c';
					matterTransferField.Value = transfer.Id;
					jnlCustomFields.add(matterTransferField);
					
				jnlWrapper.CustomFields = jnlCustomFields;

				Decimal debitAmt = transferExpenseTotal_Map.get(transfer.Matter__c).setScale(2);
				Decimal creditAmt = transferExpenseTotal_Map.get(transfer.Matter__c).setScale(2) * -1;

				c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem debitLine = FFAUtilities.initFFAPIJournalLine('Debit',
																													'Transfer of hard costs on Matter - '+matter.ReferenceNumber__c,
																													'General Ledger Account',
																													debitAmt,
																													targetCompany.Intercompany_GLA__c,
																													matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
																													matter.litify_pm__Case_Type__r.Dimension_2__c,
																													matter.litify_pm__Case_Type__r.Dimension_3__c,
																													null, 
																													matter.Id);
				jnlLineList.add(debitLine);

				c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem creditLine = FFAUtilities.initFFAPIJournalLine('Debit',
																													'Transfer of hard costs on Matter - '+matter.ReferenceNumber__c,
																													'General Ledger Account',
																													creditAmt,
																													glaMap.get(arGLACode),
																													matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
																													matter.litify_pm__Case_Type__r.Dimension_2__c,
																													matter.litify_pm__Case_Type__r.Dimension_3__c,
																													null, 
																													matter.Id);
				jnlLineList.add(creditLine);

				//create the line collection wrapper
				c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItems jnlLinesWrapper = new c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItems();
				jnlLinesWrapper.LineItemList = jnlLineList;
				jnlWrapper.lineItems = jnlLinesWrapper;
			}

		}
		//DML Database Manipulation Language
		Try{
			update matter_Map.values();
			insert newDepositList;
			insert newFinanceRequestList;

			//Create the PIN if applicable:
			if(newPin != null){
				insert newPin;
				for(c2g__codaPurchaseInvoiceExpenseLineItem__c expLine : pinLineList){
					expLine.c2g__PurchaseInvoice__c = newPin.Id;
				}
				insert pinLineList;
			}			

			//Create the journal with API call
			c2g.CODAAPICommon.Reference newJnlID = c2g.CODAAPIJournal_12_0.CreateJournal(compWrapper, jnlWrapper);

			//post the journal entry
			if(Test.isRunningTest() == false){
				List<c2g__codaJournal__c> jnlList = FFAUtilities.postSingleJournal(newJnlID.Id);
			}

		}
		Catch(Exception e){
			throw e;
		}
	}


}