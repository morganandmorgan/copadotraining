public with sharing class CPFinancialInfo
{
    public class FinInfoRecord
    {
        @AuraEnabled
        public String EffDate { get; set; }
        @AuraEnabled
        public String CheckNo { get; set; }
        @AuraEnabled
        public String Invoice { get; set; }
        @AuraEnabled
        public String Bank { get; set; }
        @AuraEnabled
        public String CostCode { get; set; }
        @AuraEnabled
        public String Payee { get; set; }
        @AuraEnabled
        public String Comment { get; set; }
        @AuraEnabled
        public Double Amount { get; set; }
        @AuraEnabled
        public Integer Index { get; set; }
    }

    public class CPCaseExpense
    {
        @AuraEnabled
        public FinInfoRecord[] CaseExpenses { get; set; }
        @AuraEnabled
        public Double TotalCosts { get; set; }
        @AuraEnabled
        public Double TotalDeposits { get; set; }
        @AuraEnabled
        public Double TotalPayables { get; set; }
        @AuraEnabled
        public Double Balance { get; set; }
    }

    public class CPCaseTrust
    {
        @AuraEnabled
        public FinInfoRecord[] CaseTrusts { get; set; }
        @AuraEnabled
        public Double TotalTrust { get; set; }
    }

    public class CPCaseSummary
    {
        @AuraEnabled
        public Double ExpensesToDate { get; set; }
        @AuraEnabled
        public Double OutstandingExpenses { get; set; }
        @AuraEnabled
        public String FeesBilled { get; set; }
        @AuraEnabled
        public String FeesReceived { get; set; }
        @AuraEnabled
        public Double Balance { get; set; }
        @AuraEnabled
        public Double TrustBalance { get; set; }
    }

    public class CPCaseFinInfo
    {
        @AuraEnabled
        public CPCaseExpense CaseExpenseData { get; set; }
        @AuraEnabled
        public CPCaseTrust CaseTrustData { get; set; }
        @AuraEnabled
        public CPCaseSummary CaseTrustSummary { get; set; }
    }

    //////////////////////////////////////////////////////////////////
    
    public static String GetAccessToken()
    {
        Http client = new Http();

        HttpRequest request = new HttpRequest();

        request.setEndpoint('callout:CPDataProvider/oauth/token');
        request.setHeader('Content-Length', '0');
        request.setMethod('POST');

        HttpResponse response = client.send(request);

        return response.getBody();
    }

    @AuraEnabled
    public static CPCaseFinInfo GetMatterFinancialInfo(String matterId)
    {
        String token = GetAccessToken();

        litify_pm__Matter__c[] matters = [select CP_Office__c, ReferenceNumber__c from litify_pm__Matter__c where Id =: matterId];

        if (matters.size() > 0)
        {
            Http client = new Http();

            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://cp-dataprovider.azurewebsites.net/api/fininfo?office=' + 
                matters[0].CP_Office__c + '&matter=' + matters[0].ReferenceNumber__c);
            request.SetHeader('Authorization', 'Bearer ' + token);
            request.setMethod('GET');

            HttpResponse response = client.send(request);
            return (CPCaseFinInfo)JSON.deserialize(response.getBody(), CPCaseFinInfo.class);
        }

        return null;
    }
}