/**
 * ExpenseSummaryBatch
 * @description Batch class for recomputing Expense Summaries where Finance Status != Closed.
 *              Id batchId = Database.executeBatch(new ExpenseSummaryBatchRecalc(), 1);
 *              System.debug(batchId);
 * @author Jeff Watson
 * @date 1/18/2019
 */

global with sharing class ExpenseSummaryBatchRecalc implements Database.Batchable<SObject>, Schedulable {

    public ExpenseSummaryBatchRecalc() {
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        //return Database.getQueryLocator([SELECT Id FROM litify_pm__Matter__c WHERE Finance_Status__c != 'Closed' AND CP_Active_Case_Indicator__c = 'N' AND RecordType.Name != 'Social Security']);
        return Database.getQueryLocator([SELECT Id FROM litify_pm__Matter__c WHERE RecordType.Name = 'Social Security' AND litify_pm__Matter_Stage_Activity_Formula__c = 'Fees and Costs' AND CP_Active_Case_Indicator__c = '']);
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new ExpenseSummaryBatch());
    }

    global void execute(Database.BatchableContext bc, List<litify_pm__Matter__c> scope) {
        Map<Id,Set<Id>> matterIds = new Map<Id,Set<Id>>();
        for (litify_pm__Matter__c matter : scope) {
            matterIds.put(matter.Id, null);
        }
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType>{
                litify_pm__Matter__c.getSObjectType(), Expense_Summary__c.getSObjectType()
        });
        ExpenseSummaryDomain.createExpenseSummaries(matterIds, uow);
        uow.CommitWork();
    }

    global void finish(Database.BatchableContext bc) {
    } //finish

} //class