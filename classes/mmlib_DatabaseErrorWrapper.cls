public class mmlib_DatabaseErrorWrapper
{
    private Database.Error dbError = null;

    private Boolean isTest = false;
    private List<String> fields = null;
    private String message = null;
    private String statusCode = null;

    public List<String> getFields()
    {
        return isTest ? dbError.getFields() : fields;
    }

    public String getMessage()
    {
        return isTest ? dbError.getMessage() : message;
    }

    public String getStatusCode()
    {
        return isTest ? String.valueOf(dbError.getStatusCode()) : statusCode;
    }

    public mmlib_DatabaseErrorWrapper(Database.Error error)
    {
        this.dbError = error;
    }

    public mmlib_DatabaseErrorWrapper(List<String> fields, String message, String statusCode)
    {
        isTest = true;
        this.fields = fields;
        this.message = message;
        this.statusCode = statusCode;
    }
}