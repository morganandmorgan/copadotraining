@IsTest
private class mmlawsuit_OneLawsuitPerIntakeCriteriTest
{
    @IsTest
    static void LawsuitQualifies()
    {
        Intake__c intake_1 = new Intake__c();
        intake_1.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);

        Intake__c intake_2 = new Intake__c();
        intake_2.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);

        List<Lawsuit__c> lawsuitList = new List<Lawsuit__c>();

        Lawsuit__c lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_1.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_1.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_2.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        List<mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult> mock_AggResultList = new List<mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult>();
        mock_AggResultList.add(new mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult(intake_1.Id, 2));
        mock_AggResultList.add(new mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult(intake_2.Id, 1));

        mmlawsuit_OneLawsuitPerIntakeCriteria.mock_AggResultList = mock_AggResultList;

        Map<Id, Lawsuit__c> qualifiedLawsuitMap =
            new Map<Id, Lawsuit__c>(
                (List<Lawsuit__c>)
                new mmlawsuit_OneLawsuitPerIntakeCriteria().setRecordsToEvaluate(lawsuitList).run());

        System.assertNotEquals(null, qualifiedLawsuitMap);
        System.assert(!qualifiedLawsuitMap.isEmpty());
        System.assertEquals(false, qualifiedLawsuitMap.containsKey(lawsuitList.get(0).Id));
        System.assertEquals(false, qualifiedLawsuitMap.containsKey(lawsuitList.get(1).Id));
        System.assertEquals(true, qualifiedLawsuitMap.containsKey(lawsuitList.get(2).Id));
    }

    @IsTest
    static void NoLawsuitsQualifies()
    {
        Intake__c intake_1 = new Intake__c();
        intake_1.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);

        Intake__c intake_2 = new Intake__c();
        intake_2.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);

        List<Lawsuit__c> lawsuitList = new List<Lawsuit__c>();

        Lawsuit__c lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_1.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_1.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_2.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        lawsuit = new Lawsuit__c();
        lawsuit.Intake__c = intake_2.Id;
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuitList.add(lawsuit);

        List<mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult> mock_AggResultList = new List<mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult>();
        mock_AggResultList.add(new mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult(intake_1.Id, 2));
        mock_AggResultList.add(new mmlawsuit_OneLawsuitPerIntakeCriteria.AggResult(intake_2.Id, 2));

        mmlawsuit_OneLawsuitPerIntakeCriteria.mock_AggResultList = mock_AggResultList;

        Map<Id, Lawsuit__c> qualifiedLawsuitMap =
            new Map<Id, Lawsuit__c>(
                (List<Lawsuit__c>)
                    new mmlawsuit_OneLawsuitPerIntakeCriteria().setRecordsToEvaluate(lawsuitList).run());

        System.assertNotEquals(null, qualifiedLawsuitMap);
        System.assert(qualifiedLawsuitMap.isEmpty());
    }
}