/**
 * ErrorLogDomain (formerly AuditErrorLogDomain)
 * @description Domain class for Audit_Error_Log__c SObject.
 * @author Matt Terrill
 * @date 10/14/2019
 */
public without sharing class ErrorLogDomain extends fflib_SObjectDomain {

    public ErrorLogDomain() {
        super();
    } //constructor

    public void logError(Exception e, String source) {
        logError(e, source, null);
    } //logError

    public void logError(Exception e, String source, String recordId) {
        Error_Log__c errorLog = new Error_Log__c();

        errorLog.message__c = e.getMessage().left(255);
        errorLog.stack_trace__c = e.getStackTraceString().left(1024);
        errorLog.type__c = e.getTypeName().left(50);
        errorLog.notification_sent__c = errorNotification();
        errorLog.source__c = source;
        errorLog.recordId__c = recordId;
        if (recordId != null) {
            errorLog.status__c = 'Pending';
        }
        errorLog.tries__c = 1;

        insert errorLog;
    } //logError

    public Boolean errorNotification() {
        try {
            
            ErrorLogSelector aels = new ErrorLogSelector();
            DateTime hourAgo = DateTime.now().addHours(-1);

            //I'm not using the selector here because of permissions.  It is always 'with sharing'
            //we need this code to always work
            //List<Audit_Error_Log__c> errors = aels.selectRecent(hourAgo);
            List<Error_Log__c> errors = [SELECT id, notification_sent__c, createdDate FROM Error_Log__c WHERE createdDate >= :hourAgo];

            //determine if we need to send a notification (did we send one in the last hour?)
            Boolean needToSend = true;
            for (Error_Log__c errorLog : errors) {
                if (errorLog.notification_sent__c) {
                    needToSend = false;
                    break;
                }
            } //for errors

            if (needToSend) {
                String org = UserInfo.getUserName().substringAfterLast('.');

                String message;
                if (org != 'com') {
                    message = 'Sandbox - ' + org + ': ';
                } else {
                    message = 'PRODUCTION: ';
                }

                if ( errors.size() == 0) {
                    message = message + 'An error has occurred.';
                } else {
                    message = message + 'There have been ' + String.valueOf(errors.size()+1) + ' errors in the last hour.';
                }

                //send the notification
                String slackUrl = 'https://hooks.slack.com/services/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';

                Map<String,String> payload = new Map<String,String>();

                payload.put('text',message);

                Http h = new Http();
                HttpRequest req = new HttpRequest();

                req.setEndpoint(slackUrl);
                req.setMethod('POST');
                req.setBody( JSON.serialize(payload) );

                HttpResponse res = h.send(req);

                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            //we don't want our notification to stop the error from getting logged
            return false;
        } //try/catch

    } //errorNotification

} //class