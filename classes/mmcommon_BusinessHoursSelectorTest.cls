@isTest
private class mmcommon_BusinessHoursSelectorTest
{
    @isTest(seeAllData=true)
    static void NormalOperation()
    {
        List<BusinessHours> bhList = mmcommon_BusinessHoursSelector.newInstance().selectDefault();

        System.assert(!bhList.isEmpty());
        System.assertEquals(1, bhList.size());
    }

    @isTest(seeAllData=true)
    static void mmcommon_BusinessHoursSelector_SelectBusinessHoursByName()
    {
        // Arrange
        Integer expectedCount = 1;
        String expectedName = 'Case Processor Operations';
        mmcommon_BusinessHoursSelector target = new mmcommon_BusinessHoursSelector();
        BusinessHours actual = null;

        // Act + Assert
        Test.startTest();
        actual = target.selectBusinessHoursByName(null);
        System.assert(actual == null);
        actual = target.selectBusinessHoursByName(expectedName);
        System.assert(actual != null);
        System.assertEquals(expectedName, actual.Name);
        Test.stopTest();
    }
}