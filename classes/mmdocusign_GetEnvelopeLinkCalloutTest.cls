@isTest
private class mmdocusign_GetEnvelopeLinkCalloutTest
{
	@isTest
    static void HappyPath()
	{
        Docusign_Integration__c settings = new Docusign_Integration__c();
        settings.DocusignAccountId__c = '598378947';
        settings.Username__c = 'fdlsjfeio@null.com';
        settings.Password__c = 'kdsjflksdjfl';
        settings.IntegratorKey__c = 'dsfljsljf';
        settings.Host__c = 'test.docusign.net';
        settings.Enabled__c = true;
        settings.EnabledInSandbox__c = true;
        settings.ReturnUrl__c = 'https://null.com';
        insert settings;

        String resultUrl = 'http://abc.def/test';
        String responseBody = '{"url": "' + resultUrl + '"}';
        Integer statusCode = 201;
        Test.setMock(HttpCalloutMock.class, new mmdocusign_RestApiCalloutMock(responseBody, statusCode));

        Test.startTest();

        mmdocusign_EnvelopeLinkCalloutResponse resp =
            (mmdocusign_EnvelopeLinkCalloutResponse)
            new mmdocusign_GetEnvelopeLinkCallout(
                'envelopeId',
                'email',
                'user name',
                'client user id'
            )
            .execute()
            .getResponse();

        resp.getTotalNumberOfRecordsFound();

        Test.stopTest();

        System.assertEquals(resultUrl, resp.getEnvelopeLink(), 'The resulting URL is not correct.');
	}

    @isTest
    static void CalloutReturnsFailingStatusCode()
	{
        Docusign_Integration__c settings = new Docusign_Integration__c();
        settings.DocusignAccountId__c = '598378947';
        settings.Username__c = 'fdlsjfeio@null.com';
        settings.Password__c = 'kdsjflksdjfl';
        settings.IntegratorKey__c = 'dsfljsljf';
        settings.Host__c = 'test.docusign.net';
        settings.Enabled__c = true;
        settings.EnabledInSandbox__c = true;
        settings.ReturnUrl__c = 'https://null.com';
        insert settings;

        String resultUrl = 'http://abc.def/test';
        String responseBody = '{"url": "' + resultUrl + '"}';
        Integer statusCode = 400;
        Test.setMock(HttpCalloutMock.class, new mmdocusign_RestApiCalloutMock(responseBody, statusCode));

        Test.startTest();

        try
        {
            mmdocusign_EnvelopeLinkCalloutResponse resp =
                (mmdocusign_EnvelopeLinkCalloutResponse)
                new mmdocusign_GetEnvelopeLinkCallout(
                    'envelopeId',
                    'email',
                    'user name',
                    'client user id'
                )
                .execute()
                .getResponse();

                System.assert(false, 'The operation should have generated an exception.');
        }
        catch (mmdocusign_IntegrationExceptions.IntegrationBaseException exc)
        {
            // An exception is expected.
        }

        Test.stopTest();

	}
}