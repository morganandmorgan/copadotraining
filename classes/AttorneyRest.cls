@RestResource(urlMapping='/attorneys/*')
global with sharing class AttorneyRest
{
    public class ApiKeyInvalidException extends Exception {}

    global class AttorneyRestDTO
    {
        public AttorneyRestDTO( Contact attorney )
        {
            id = attorney.id;
            name = attorney.name;
            email = attorney.Email;
        }

        public Id id { get; set; }
        public String name { get; set; }
        public String email { get; set; }
    }

    @HttpGet
    global static List<AttorneyRestDTO> DoGet()
    {
        String apiKey = RestContext.request.params.get('apiKey');

        if (apiKey != 'b33f8613bf7b')
        {
            RestContext.response.statusCode = 401;
            return null;
        }

        List<AttorneyRestDTO> attorneyRestList = new List<AttorneyRestDTO>();

        List<Contact> attorneys = mmcommon_ContactsSelector.newInstance().selectAllMorganAndMorganAttorneys();

        for (Contact attorney : attorneys)
        {
            attorneyRestList.add( new AttorneyRestDTO( attorney ) );
        }

        return attorneyRestList;
    }
}