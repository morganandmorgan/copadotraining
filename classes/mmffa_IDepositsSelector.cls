public interface mmffa_IDepositsSelector
    extends mmlib_ISObjectSelector
{
    List<Deposit__c> selectById(Set<Id> idSet);
}