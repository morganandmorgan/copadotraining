/**
 *  mmmarketing_Sources
 */
public with sharing class mmmarketing_Sources
    extends fflib_SObjectDomain
    implements mmmarketing_ISources
{
    private fflib_ISObjectUnitOfWork uow = null;

    public static mmmarketing_ISources newInstance(List<Marketing_Source__c> records)
    {
        return (mmmarketing_ISources) mm_Application.Domain.newInstance(records);
    }

    public static mmmarketing_ISources newInstance(Set<Id> recordIds)
    {
        return (mmmarketing_ISources) mm_Application.Domain.newInstance(recordIds);
    }

    public mmmarketing_Sources(List<Marketing_Source__c> records)
    {
        super(records);
    }

    public override void onApplyDefaults()
    {
        defaultNameIfRequired();
    }

    public override void onAfterInsert()
    {
        autoLinkMarketingInformationToSourceIfRequired();
        autoLinkMarketingFinancialPeriodsToSourceIfRequired();
    }

    private void defaultNameIfRequired()
    {
        for (Marketing_Source__c record : (list<Marketing_Source__c>)this.records)
        {
            if (record.name == null)
            {
                record.name = record.utm_source__c;
            }
        }
    }

    public void autoLinkMarketingInformationToSourceIfRequired()
    {
        // find all utm_source__c for the records in this domain layer
        Set<String> sourceValueSet = new Set<String>();

        for (Marketing_Source__c record : (list<Marketing_Source__c>)this.records )
        {
            sourceValueSet.add( record.utm_source__c );
        }

        new mmmarketing_TI_AutoLinkSourcesBatch( sourceValueSet ).setBatchSizeTo(100).execute();
    }

    public void autoLinkMarketingFinancialPeriodsToSourceIfRequired()
    {
        // find all utm_source__c for the records in this domain layer
        Set<String> sourceValueSet = new Set<String>();

        for (Marketing_Source__c record : (list<Marketing_Source__c>)this.records )
        {
            sourceValueSet.add( record.utm_source__c );
        }

        new mmmarketing_FP_AutoLinkSourcesBatch( sourceValueSet ).setBatchSizeTo(100).execute();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmmarketing_Sources(sObjectList);
        }
    }
}