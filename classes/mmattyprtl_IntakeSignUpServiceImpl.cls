public without sharing class mmattyprtl_IntakeSignUpServiceImpl
    implements mmattyprtl_IIntakeSignUpService
{
    private static final id CASE_SIGN_UP_EMAIL_TEMPLATE_ID;
    private static final boolean CASE_SIGN_UP_CONFIRMATION_EMAIL_BE_SENT;
    @TestVisible
    private static final String ORG_WIDE_EMAIL_ADDRESS = Attorneys_Portal__c.getInstance().Case_Sign_Up_Confirmation_Email_FromAddr__c;
    @TestVisible
    private static String ORG_WIDE_EMAIL_ADDRESS_NAME;

    static
    {
        Attorneys_Portal__c custSettings = Attorneys_Portal__c.getInstance();

        if ( custSettings == null )
        {
            throw new mmattyprtl_IntakeSignUpServiceException('Custom Setting "Attorneys_Portal__c" was found to be null');
        }

        if ( custSettings.Send_Case_Sign_Up_Confirmation_Email__c != null )
        {
            CASE_SIGN_UP_CONFIRMATION_EMAIL_BE_SENT = custSettings.Send_Case_Sign_Up_Confirmation_Email__c;
        }
        else
        {
            CASE_SIGN_UP_CONFIRMATION_EMAIL_BE_SENT = false;
        }

        try
        {
            EmailTemplate theTemplate = [select Id, Name, DeveloperName, NamespacePrefix, OwnerId, FolderId, BrandTemplateId
                                              , TemplateStyle, IsActive, TemplateType, Encoding, Description, Subject, HtmlValue
                                              , Body, TimesUsed, LastUsedDate, CreatedDate, CreatedById, LastModifiedDate
                                              , LastModifiedById, SystemModstamp, ApiVersion, Markup, UiType, RelatedEntityType
                                           from EmailTemplate
                                          where DeveloperName = :custSettings.Case_Sign_Up_Confirmation_Email_Template__c
                                            and IsActive = true];

            CASE_SIGN_UP_EMAIL_TEMPLATE_ID = theTemplate.id;
        }
        catch (System.DmlException dmle)
        {
            throw new mmattyprtl_IntakeSignUpServiceException('Failed to find email template for attorney portal', dmle);
        }
    }

//    /*
//     *  Method to handle either the finding of the client account record or the creation of the client record.
//     *
//     *  @param fieldParameterMap which is a map of the specific Schema.sObjectField and their associated values.
//     *  @return the id of the Intake__c record created
//     *  @throws mmattyprtl_IntakeSignUpServiceException
//     */
//    private Account processInputAndReturnClientRecord( map<Schema.sObjectField, string> fieldParameterMap, mmlib_ISObjectUnitOfWork uow )
//    {
//        // need to search for the contact
//        String firstName = fieldParameterMap.containsKey( Account.FirstName ) ? mmlib_utils.clean( fieldParameterMap.get( Account.FirstName ) ) : null;
//        String lastName = fieldParameterMap.containsKey( Account.LastName ) ? mmlib_utils.clean( fieldParameterMap.get( Account.LastName ) ) : null;
//        String email = fieldParameterMap.containsKey( Account.PersonEmail ) ? mmlib_utils.clean( fieldParameterMap.get( Account.PersonEmail ) ) : null;
//        String phoneNum = fieldParameterMap.containsKey( Account.Phone ) ? mmlib_utils.stripPhoneNumber( fieldParameterMap.get( Account.Phone ) ) : null;
//
//        if ( String.isBlank( phoneNum ) && fieldParameterMap.containsKey( Account.PersonMobilePhone ) )
//        {
//            phoneNum = mmlib_utils.stripPhoneNumber( fieldParameterMap.get( Account.PersonMobilePhone ) );
//        }
//
//        Date birthDate = null; // currently not specified in the attorneys portal
//
//        list<Account> accountsFound = mmcommon_PersonAccountsService.searchAccounts( firstName, lastName, email, phoneNum, birthDate );
//
//        Account client = null;
//
//        if ( accountsFound.size() > 1 )
//        {
//            //determine best option
//            Account potentialClient = null;
//
//            // loop through first 5 options to find best match
//            integer maxLoopNumber = accountsFound.size() > 5 ? 5 : accountsFound.size();
//
//            for (Integer i = 0; i < maxLoopNumber; i++)
//            {
//                System.debug(i+1);
//                potentialClient = accountsFound[i];
//
//                if ( string.isNotBlank( email )
//                    && email.equalsIgnoreCase( potentialClient.PersonEmail ) )
//                {
//                    //match!
//                    client = potentialClient;
//                    break;
//                }
//
//                if ( string.isNotBlank( phoneNum )
//                    && phoneNum.equalsIgnoreCase( mmlib_utils.stripPhoneNumber( potentialClient.Phone ) ) )
//                {
//                    //match!
//                    client = potentialClient;
//                    break;
//                }
//
//                if ( string.isNotBlank( firstName )
//                    && string.isNotBlank( lastName )
//                    && firstName.equalsIgnoreCase( potentialClient.FirstName )
//                    && lastName.equalsIgnoreCase( potentialClient.LastName )
//                    )
//                {
//                    //match!
//                    client = potentialClient;
//                    break;
//                }
//                // no matches have been found. ....move on to the next potential client
//            }
//        }
//        else if ( accountsFound.size() == 1 )
//        {
//            // the client was found
//            client = accountsFound[0];
//        }
//
//
//
//        // if no viaable client has been found yet.....
//        if ( client == null )
//        {
            // no account was found, create a new one
//            list<Account> accountRecordsToCreate = new list<Account>();
//
//            client = new Account();
//
//            client.FirstName = firstName;
//            client.LastName = lastName;
//            client.PersonEmail = email;
//            client.Phone = fieldParameterMap.containsKey( Account.Phone ) ? fieldParameterMap.get( Account.Phone ) : null;
//            client.PersonMobilePhone = fieldParameterMap.containsKey( Account.PersonMobilePhone ) ? fieldParameterMap.get( Account.PersonMobilePhone ) : null;

//            accountRecordsToCreate.add( client );
//
//            mmcommon_Accounts accounts = new mmcommon_Accounts( accountRecordsToCreate );
//
//            try
//            {
//                accounts.validate();
//            }
//            catch ( mmcommon_AccountsDomainExceptions.ValidationException ve )
//            {
//                // TODO:  Determine what happens in this case.
//                system.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ' + ve);
//            }
//
//            uow.register( accountRecordsToCreate );
//        }

//        return client;
//    }

    private Messaging.SingleEmailMessage prepareBaseEmailForNotification( Intake__c intake )
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setTemplateId( CASE_SIGN_UP_EMAIL_TEMPLATE_ID );

        mail.setReplyTo(ORG_WIDE_EMAIL_ADDRESS);

        if (String.isBlank(ORG_WIDE_EMAIL_ADDRESS_NAME)) {
            try {
                List<OrgWideEmailAddress> orgWideEmails = [
                    SELECT DisplayName
                    FROM OrgWideEmailAddress
                    WHERE Address = :ORG_WIDE_EMAIL_ADDRESS
                    LIMIT 1
                ];

                ORG_WIDE_EMAIL_ADDRESS_NAME = orgWideEmails.isEmpty() ? ORG_WIDE_EMAIL_ADDRESS : orgWideEmails[0].DisplayName;
            } catch (DmlException ex) {
                ORG_WIDE_EMAIL_ADDRESS_NAME = ORG_WIDE_EMAIL_ADDRESS;
            }
        }
        
        mail.setSenderDisplayName(ORG_WIDE_EMAIL_ADDRESS_NAME);

        mail.setUseSignature( false );
        mail.setWhatId( intake.id );

        return mail;
    }

    /*
     * prepares appropriate emails to be sent to the generating attorney, current user, and any additional email addresses
     */
    private list<Messaging.Email> prepareEmailForRecipients( Intake__c intake, Set<String> additionalEmailAddressesToNotifySet )
    {
        list<Messaging.Email> emails = new list<Messaging.Email>();

        // clean up the additionalEmailAddressesToNotifySet
        set<String> additionalEmailAddressesToNotifyCleanSet = new set<string>();
        for ( string dirtyString : additionalEmailAddressesToNotifySet )
        {
            additionalEmailAddressesToNotifyCleanSet.add( mmlib_Utils.clean( dirtyString ) );
        }

        set<String> distilledReceipientEmailAddressSet = new set<String>();

        Contact generatingAttorney = null;

        // find the generating attorney's email address
        if ( intake.Generating_Attorney__c != null )
        {
            list<Contact> contactRecords = mmcommon_ContactsSelector.newInstance().selectById( new set<id>{ intake.Generating_Attorney__c } );

            if ( ! contactRecords.isEmpty() )
            {
                generatingAttorney = contactRecords[0];

                if ( String.isNotBlank( generatingAttorney.Email ) )
                {
                    distilledReceipientEmailAddressSet.add( generatingAttorney.Email );
                }
            }
        }

        boolean isCurrentUserAlsoTheGeneratingAttorney = distilledReceipientEmailAddressSet.contains( UserInfo.getUserEmail() );

        system.debug( 'distilledReceipientEmailAddressSet 1 == ' + distilledReceipientEmailAddressSet );
        system.debug( 'additionalEmailAddressesToNotifyCleanSet 1 == ' + additionalEmailAddressesToNotifyCleanSet );
        system.debug( 'isCurrentUserAlsoTheGeneratingAttorney == ' + isCurrentUserAlsoTheGeneratingAttorney);

        if ( ! isCurrentUserAlsoTheGeneratingAttorney )
        {
            // since the generating attorney is not the current user, add the current user's email address to the distilled set
            distilledReceipientEmailAddressSet.add( UserInfo.getUserEmail() );

            system.debug( 'distilledReceipientEmailAddressSet 2 == ' + distilledReceipientEmailAddressSet );

            // now remove the generating attorney and current user email addresses from the additionalEmailAddressesToNotifyCleanSet, if they are present -- no dups
            additionalEmailAddressesToNotifyCleanSet.removeAll( distilledReceipientEmailAddressSet );

            system.debug( 'distilledReceipientEmailAddressSet 3 == ' + distilledReceipientEmailAddressSet );
            system.debug( 'additionalEmailAddressesToNotifyCleanSet 3 == ' + additionalEmailAddressesToNotifyCleanSet );
        }

        // email for current user
        Messaging.SingleEmailMessage mailForCurrentUser = prepareBaseEmailForNotification( intake );

        mailForCurrentUser.setSaveAsActivity( false ); // this must be false when sending emails to users
        mailForCurrentUser.setTargetObjectId( UserInfo.getUserId() );

        system.debug( 'additionalEmailAddressesToNotifyCleanSet 4 == ' + additionalEmailAddressesToNotifyCleanSet);

        if ( additionalEmailAddressesToNotifyCleanSet != null
            && ! additionalEmailAddressesToNotifyCleanSet.isempty() )
        {
            mailForCurrentUser.setToAddresses( new list<String>( additionalEmailAddressesToNotifyCleanSet ) );
        }

        emails.add( mailForCurrentUser );

        // if the generating attorney is different from the current user, then send the email to the generating attorney as well.
        if ( ! isCurrentUserAlsoTheGeneratingAttorney
            && generatingAttorney != null
            && string.isNotBlank( generatingAttorney.Email )
            )
        {
            // email for generating attorney
            Messaging.SingleEmailMessage mailForGeneratingAttorney = prepareBaseEmailForNotification( intake );

            mailForGeneratingAttorney.setSaveAsActivity( true );
            mailForGeneratingAttorney.setTargetObjectId( intake.Generating_Attorney__c );

            emails.add( mailForGeneratingAttorney );
        }

        return emails;
    }

    /**
     *  Service method to handle the creation of the intake sign up request.
     *
     *  @param fieldParameterMap which is a map of the specific Schema.sObjectField and their associated values.
     *  @param screening option needed. Provided as mmattyprtl_Enums.SCREENING_OPTIONS enum type
     *  @param list of additional email addresses to notify outside of the generating attorney and the person creating the email
     *  @return the id of the Intake__c record created
     *  @throws mmattyprtl_IntakeSignUpServiceException
     */
    public Intake__c processSignUpRequest( map<Schema.sObjectField, string> fieldParameterMap, mmattyprtl_Enums.SCREENING_OPTIONS screeningOption, set<string> additionalEmailAddressesToNotifySet )
    {
        Intake__c intakeRequest = null;

        if ( fieldParameterMap != null
            && ! fieldParameterMap.isEmpty()
            )
        {
            // need to search for the contact

            mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

            Account client = new mmcommon_PersonAccountsFactory( uow ).with( fieldParameterMap ).generate();

            mmintake_IntakesFactory intakesFactory = new mmintake_IntakesFactory( uow );

            // now process the intake creation

            fieldParameterMap.put( Intake__c.Marketing_Source__c, 'Internal Attorney' ); // default this all the time.

            string caseDetailsText = '';

            if ( mmattyprtl_Enums.SCREENING_OPTIONS.CALL_AND_SCREEN == screeningOption )
            {
                //intakeRequest.Screening__c = false;
                fieldParameterMap.put( Intake__c.Screening__c, 'false' );

                caseDetailsText = Label.PAGE_CASE_SIGN_UP_REQUEST_CCC_ACTION_REQUESTED_OPTION_CALL_AND_SCREEN_LABEL;
            }
            else if ( mmattyprtl_Enums.SCREENING_OPTIONS.CALL_AND_SIGN_UP == screeningOption )
            {
                //intakeRequest.Screening__c = true;
                fieldParameterMap.put( Intake__c.Screening__c, 'true' );

                caseDetailsText = Label.PAGE_CASE_SIGN_UP_REQUEST_CCC_ACTION_REQUESTED_OPTION_CALL_AND_SIGNUP_LABEL;
            }
            else if ( mmattyprtl_Enums.SCREENING_OPTIONS.DO_NOT_CALL == screeningOption )
            {
                //intakeRequest.Screening__c = false;
                fieldParameterMap.put( Intake__c.Screening__c, 'false' );

                caseDetailsText = Label.PAGE_CASE_SIGN_UP_REQUEST_CCC_ACTION_REQUESTED_OPTION_DO_NOTHING_LABEL;
            }
            else
            {
                //intakeRequest.Screening__c = false;
                fieldParameterMap.put( Intake__c.Screening__c, 'false' );
            }

            system.debug( 'Screening == ' + fieldParameterMap.get( Intake__c.Screening__c ));
            system.debug( 'screeningOption == ' + screeningOption);

            fieldParameterMap.put( Intake__c.Lead_Details__c, caseDetailsText + ( fieldParameterMap.containsKey( Intake__c.Lead_Details__c )
                                                                    ? '\n\n' + fieldParameterMap.get( Intake__c.Lead_Details__c )
                                                                    : null ) );

            fieldParameterMap.put(Intake__c.InputChannel__c, 'Attorney Portal');

            intakeRequest = intakesFactory.with( fieldParameterMap )
                                          .setClient( client )
                                          .setClientAsCaller()
                                          .setClientAsInjuredParty()
                                          .generate();


            if ( CASE_SIGN_UP_CONFIRMATION_EMAIL_BE_SENT )
            {
                list<Messaging.Email> emails = prepareEmailForRecipients( intakeRequest, additionalEmailAddressesToNotifySet );

                for ( Messaging.Email email : emails )
                {
                    if ( email instanceOf Messaging.SingleEmailMessage )
                    {
                        uow.registerRelationship( (Messaging.SingleEmailMessage)email, intakeRequest );
                    }
                }
                if (!Test.isRunningTest()) {
                	uow.registerEmails( emails );
                }
            }

            try
            {
                uow.commitWork();

                intakeRequest = mmintake_IntakesSelector.newInstance().selectById( new set<id>{ intakeRequest.id } )[0];
            }
            catch ( System.DMLException dmle )
            {
                throw new mmattyprtl_IntakeSignUpServiceException( dmle );
            }
        }

        return intakeRequest;
    }
}