/**
 * SlackLimitsSchedule
 * @description Scheduled push of limits to Slack
 * @author Matt Terrill
 * @date 8/21/2019
 */
global class SlackLimitsSchedule implements Schedulable, Database.AllowsCallouts {

    @future (callout=true) //so we can do the callout in a scheduled job
    private static void slackLimits() {

        String slackUrl = 'https://hooks.slack.com/services/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';

        GovernorLimitsApi gla = new GovernorLimitsApi();

        String message = gla.formatForSlack(true);

        Map<String,String> payload = new Map<String,String>();

        payload.put('text',message);

        Http h = new Http();
        HttpRequest req = new HttpRequest();

        req.setEndpoint(slackUrl);
        req.setMethod('POST');
        req.setBody( JSON.serialize(payload) );

        HttpResponse res = h.send(req);
    } //slackLimits

    global void execute(SchedulableContext SC) {

        slackLimits();

    } //execute

} //class