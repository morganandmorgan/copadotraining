@isTest
private class SpringMigrationStartTests {
    static testMethod void testSpringCMStartMigrationBatch () {
        SpringCMEos__EOS_Type__c oppObj = new SpringCMEos__EOS_Type__c();
        oppObj.Name = 'Opportunity';
        oppObj.SpringCMEos__Folder_Name__c = '{!Name}';
        oppObj.SpringCMEos__Folder_Name_Format__c = '{0}';
        oppObj.SpringCMEos__Variables__c = 'Name';
        oppObj.SpringCMEos__Path__c = '/Salesforce/Test';
        oppObj.SpringCMEos__Path_Format__c = '/Salesforce/Test';
        insert oppObj;
        
        SpringCMEos__EOS_Type__c accObj = new SpringCMEos__EOS_Type__c();
        accObj.Name = 'Account';
        accObj.SpringCMEos__Folder_Name__c = '{!Name}';
        accObj.SpringCMEos__Folder_Name_Format__c = '{0}';
        accObj.SpringCMEos__Variables__c = 'Name';
        accObj.SpringCMEos__Path__c = '/Salesforce/Test';
        accObj.SpringCMEos__Path_Format__c = '/Salesforce/Test';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        //  opp.Account = acc;
        opp.Name = 'SpringTestOpp';
        opp.StageName = 'In Progress';
        opp.CloseDate = DateTime.now().date();
        insert opp;
        
        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        
        List<string> s = new List<string> ();
        
        Test.startTest();
        try {
            SpringMigrationStart.Start('Account', s);
            s.add('StageName');
            SpringMigrationStart.StartMigration('Opportunity', s, true);
            }
        catch(Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            }
        Test.stopTest();
        
        System.assertEquals(1, attachments.size()); 
    }
    }