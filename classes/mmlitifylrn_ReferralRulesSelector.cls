public class mmlitifylrn_ReferralRulesSelector
    extends mmlib_SObjectSelector
    implements mmlitifylrn_IReferralRulesSelector
{
    public static mmlitifylrn_IReferralRulesSelector newInstance()
    {
        return (mmlitifylrn_IReferralRulesSelector) mm_Application.Selector.newInstance(ReferralRule__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return ReferralRule__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            ReferralRule__c.HandlingFirm__c,
            ReferralRule__c.OriginatingFirm__c,
            ReferralRule__c.Query__c
        };
    }

    public list<ReferralRule__c> selectById( Set<id> idSet )
    {
        return (List<ReferralRule__c>) selectSObjectsById(idSet);
    }

    public list<ReferralRule__c> selectAll()
    {
        fflib_QueryFactory referralRulesQueryFactory = newQueryFactory();

        new mmlitifylrn_FirmsSelector().addQueryFactoryParentSelect( referralRulesQueryFactory, ReferralRule__c.OriginatingFirm__c );

        return Database.query( referralRulesQueryFactory.toSOQL() );
    }

}