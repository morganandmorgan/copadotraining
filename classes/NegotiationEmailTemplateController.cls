/**
* @File Name          : NegotiationEmailTemplateController.cls
* @Description        : 
* @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
* @Group              : 
* @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
* @Last Modified On   : 15/11/2019, 5:40:25 pm
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    15/11/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class NegotiationEmailTemplateController {
    public litify_pm__Negotiation__c offerRecord{get;set;}
    public litify_pm__Negotiation__c demandRecord{get;set;}
    public String matterLink{get;set;}
    public String roleName{get;set;}
    public String approvingAttorny{get;set;}
    public String caseType{get;set;}
    public String coverageDetails{get;set;}
    public String negotiationHistory{get;set;}
    public String negotiationLink{get;set;}
    public List<String> offerApprovalList = new List<String>{'approved','rejected','approval in process'};
   	public List<String> demandOfferList = new List<String>{'draft','approved'};
    private Map<Integer, String> dateConversionMap = new Map<Integer, String>{01 => 'Jan', 02 => 'Feb', 03 => 'March',
        04=>'April', 05 => 'May', 06 => 'June',
        07=>'July', 08 => 'Aug', 09 => 'Sept',
        10=>'Oct', 11 => 'Nov', 12 => 'Dec'};
            
            public List<litify_pm__Negotiation__c> negotiationHistoryList{get;set;}
    
    public void setOfferRecordId(String offerId){
        roleName = '';
        approvingAttorny = '';
        caseType = '';
        negotiationHistoryList = new List<litify_pm__Negotiation__c>();
        demandRecord = new litify_pm__Negotiation__c();
        offerRecord = new litify_pm__Negotiation__c();
        offerRecord = [SELECT Id,Name,Matter_Name__c,litify_pm__Matter__c,Most_Recent_Demand__c,litify_pm__Amount__c,litify_pm__Negotiating_with__r.litify_pm__Party__c,litify_pm__Negotiating_with__r.litify_pm__Party__r.name,litify_pm__Negotiating_with__c,Approving_Attorney__r.name,CreatedBy.FirstName,CreatedBy.LastName,
                       litify_pm__Comments__c,Amount_of_Most_Recent_Demand__c,Date_of_Most_Recent_Demand__c,litify_pm__Matter__r.litify_pm__Incident_date__c,litify_pm__Date__c,Comments_from_Most_Recent_Demand__c,Approving_Attorney__c,
                       litify_pm__Matter__r.Last_Treatment_Date__c,litify_pm__Matter__r.litify_pm__Case_Type__c,litify_pm__Matter__r.litify_pm__Case_Type__r.name,litify_pm__Matter__r.County__c,litify_pm__Matter__r.Deceased_Elderly_75__c,
                       litify_pm__Matter__r.Description_of_Incident__c,litify_pm__Matter__r.Injured_Party_Deceased__c,litify_pm__Matter__r.Client_is_Minor__c,litify_pm__Matter__r.litify_pm__Matter_City__c,litify_pm__Matter__r.Incident_State__c,
                       litify_pm__Matter__r.litify_pm__Client__r.Client_s_Age__c,litify_pm__Matter__r.litify_pm__Client__r.Gender__c,litify_pm__Matter__r.Liability_Clear__c,
                       litify_pm__Matter__r.Ortho_Eval__c,litify_pm__Matter__r.Fracture__c,litify_pm__Matter__r.MRI_Completed__c,litify_pm__Matter__r.Positive_MRI_Finding__c,litify_pm__Matter__r.Similar_Prior_Injury__c,
                       litify_pm__Matter__r.Injections_Completed__c,litify_pm__Matter__r.Surgery_Completed__c,litify_pm__Matter__r.Surgery_and_or_Injections_Recommended__c,litify_pm__Matter__r.Cat_Case_Confirmed__c,
                       litify_pm__Matter__r.Total_Coverage__c,litify_pm__Matter__r.Total_Bills__c,litify_pm__Matter__r.Life_Care_Plan_Cost__c,litify_pm__Matter__r.Property_Damage__c                       
                       From litify_pm__Negotiation__c WHERE Id =:offerId Limit 1];
        
        if(offerRecord.Approving_Attorney__c != null){
            approvingAttorny = offerRecord.Approving_Attorney__r.name;
        }
        if(offerRecord.litify_pm__Matter__r.litify_pm__Case_Type__c != null){
           caseType =  offerRecord.litify_pm__Matter__r.litify_pm__Case_Type__r.name;
        }
        
        if(offerRecord.litify_pm__Negotiating_with__c != null){
            if(offerRecord.litify_pm__Negotiating_with__r.litify_pm__Party__c != null){
                roleName = offerRecord.litify_pm__Negotiating_with__r.litify_pm__Party__r.name;
            }
            List<litify_pm__Negotiation__c> demandRecordList = [SELECT Id,Amount_of_Most_Recent_Demand__c,litify_pm__Comments__c,Date_of_Most_Recent_Demand__c 
                                                                From litify_pm__Negotiation__c WHERE Id =: offerRecord.Most_Recent_Demand__c And 
                                                                litify_pm__Negotiating_with__c=: offerRecord.litify_pm__Negotiating_with__c AND litify_pm__Matter__c =: offerRecord.litify_pm__Matter__c Limit 1];
            if(demandRecordList.size() > 0){
                demandRecord = demandRecordList[0];
            }
            negotiationHistoryList = [SELECT Id,litify_pm__Date__c,litify_pm__Amount__c, Most_Recent_Demand__c, litify_pm__Type__c,Most_Recent_Demand__r.litify_pm__Amount__c,Approval_Status__c,Most_Recent_Demand__r.Approval_Status__c From litify_pm__Negotiation__c WHERE 
                                      litify_pm__Negotiating_with__c=: offerRecord.litify_pm__Negotiating_with__c AND litify_pm__Matter__c =: offerRecord.litify_pm__Matter__c AND recordTypeId =: recordTypeId('Offer') AND (Approval_Status__c IN: offerApprovalList ) ORDER BY litify_pm__Date__c ASC];
        }else{
            List<litify_pm__Negotiation__c>demandRecordList = [SELECT Id,litify_pm__Comments__c,Amount_of_Most_Recent_Demand__c,Date_of_Most_Recent_Demand__c 
                                                               From litify_pm__Negotiation__c WHERE Id =: offerRecord.Most_Recent_Demand__c And
                                                               litify_pm__Matter__c =: offerRecord.litify_pm__Matter__c Limit 1];
            if(demandRecordList.size() > 0){
                demandRecord = demandRecordList[0];
            }
            
            negotiationHistoryList = [SELECT Id,litify_pm__Date__c,litify_pm__Amount__c,Approval_Status__c, Most_Recent_Demand__c,litify_pm__Type__c,Most_Recent_Demand__r.litify_pm__Amount__c,Most_Recent_Demand__r.Approval_Status__c From litify_pm__Negotiation__c WHERE  
                                      litify_pm__Matter__c =: offerRecord.litify_pm__Matter__c AND recordTypeId =: recordTypeId('Offer') AND (Approval_Status__c IN: offerApprovalList ) ORDER BY litify_pm__Date__c ASC ];
        }
        
        negotiationHistory = returnNegotiationHistoryChdString(negotiationHistoryList);
        coverageDetails = returnCoverageDetailChdString(offerRecord);
       
        matterLink =  System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + offerRecord.litify_pm__Matter__c;
        negotiationLink =  System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + offerRecord.id;
    }
    public string getofferRecordId(){
        return '';
    } 
    
    public String returnCoverageDetailChdString(litify_pm__Negotiation__c offerRecord){
        Decimal totalCoverage = offerRecord.litify_pm__Matter__r.Total_Coverage__c != null?offerRecord.litify_pm__Matter__r.Total_Coverage__c:0.0;
        Decimal totalBills = offerRecord.litify_pm__Matter__r.Total_Bills__c != null?offerRecord.litify_pm__Matter__r.Total_Bills__c:0.0;
        Decimal Life_Care_Plan_Cost = offerRecord.litify_pm__Matter__r.Life_Care_Plan_Cost__c != null?offerRecord.litify_pm__Matter__r.Life_Care_Plan_Cost__c:0.0;
        Decimal Property_Damage = offerRecord.litify_pm__Matter__r.Property_Damage__c != null?offerRecord.litify_pm__Matter__r.Property_Damage__c:0.0;
        String range = '';
        range = findRange(totalCoverage, totalBills, Life_Care_Plan_Cost, Property_Damage);
        Double divider = Math.pow(10, range.length());
        return 'https://image-charts.com/chart?cht=bvg&chs=580x400&chd=t:'+Math.round(totalCoverage/divider)+','+Math.round(totalBills/divider)+','+
            Math.round(Life_Care_Plan_Cost/divider)+','+Math.round(Property_Damage/divider)+'&chco=ff8533&chxt=x,y&chxl=0:|Total Coverage |Total Bills|LifeCare Plan Cost|Property Damage |&chxs=1N*cUSD0sz*'+range+',000000&chl=$'+totalCoverage+
            '|$'+ totalBills+'|$'+Life_Care_Plan_Cost+'|$'+Property_Damage;
    }
    public String returnNegotiationHistoryChdString(List<litify_pm__Negotiation__c> offerRecordList){
        if(offerRecordList.size() > 0){
            String chdString ='chd=t:';
            String chxlString ='chxl=0:|';
            String offer ='';
            String demand = ''; 
            String range = '';
            Decimal maxNumber = 0.0;
            System.debug(offerRecordList.size());
            for(litify_pm__Negotiation__c negotiation : offerRecordList){
                Decimal mostRecentDemand = 0.0;
                if(negotiation.Most_Recent_Demand__c != null && demandOfferList.contains(negotiation.Most_Recent_Demand__r.Approval_Status__c.toLowerCase())){
                    mostRecentDemand = negotiation.Most_Recent_Demand__r.litify_pm__Amount__c != null ? negotiation.Most_Recent_Demand__r.litify_pm__Amount__c : 0.0;
                }
                if(negotiation.litify_pm__Amount__c >=  mostRecentDemand  && negotiation.litify_pm__Amount__c >= maxNumber  ){
                    maxNumber = negotiation.litify_pm__Amount__c;
                }else if(mostRecentDemand >= maxNumber ){
                    maxNumber = mostRecentDemand;
                }
            }
            range = findMaxRange(maxNumber);
            Decimal divider = Math.pow(10, range.length());
            for(litify_pm__Negotiation__c negotiation : offerRecordList){
                Decimal mostRecentDemandV2 = 0.0;
                if(negotiation.Most_Recent_Demand__c != null && demandOfferList.contains(negotiation.Most_Recent_Demand__r.Approval_Status__c.toLowerCase())){
                    mostRecentDemandV2 = negotiation.Most_Recent_Demand__r.litify_pm__Amount__c != null ? negotiation.Most_Recent_Demand__r.litify_pm__Amount__c : 0.0;
                }
                offer += negotiation.litify_pm__Amount__c == null?'0':Math.round(negotiation.litify_pm__Amount__c/divider)+',';
                demand += Math.round(mostRecentDemandV2/divider)+',';
                chxlString = chxlString+negotiation.litify_pm__Date__c.day()+'-'+
                    dateConversionMap.get(negotiation.litify_pm__Date__c.month())+'-'+ 
                    negotiation.litify_pm__Date__c.year()+'|';
            }
            
            chdString =chdString+ offer.removeEnd(',')+'|'+demand.removeEnd(',');
            chxlString = chxlString.removeEnd('|');
            return 'https://image-charts.com/chart?cht=bvg&chs=580x400&chdl=Offer|Demand&chdlp=b&'+chdString+'&chco=16BFE7,ff8533&chxt=x,y&chxs=1N*cUSD0sz*'+range+'&'+chxlString;
        }else{
            return '';
        }
    }
    public String recordTypeId(String recordTypeLabel){
        return Schema.Sobjecttype.litify_pm__Negotiation__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();
    }
    
    public String findRange(Decimal totalCoverage, Decimal totalBills, Decimal Life_Care_Plan_Cost, Decimal Property_Damage){
        Decimal maxNumber = 0.0;
        if(totalCoverage >= totalBills && totalCoverage >= Life_Care_Plan_Cost && totalCoverage >= Property_Damage){
            maxNumber = totalCoverage;
        }else if(totalBills >= totalCoverage && totalBills >= Life_Care_Plan_Cost && totalBills >= Property_Damage){
            maxNumber = totalBills;
        }else if(Property_Damage >= totalBills && Property_Damage >= Life_Care_Plan_Cost && Property_Damage >= totalCoverage){
            maxNumber = Property_Damage;
        }
        else if(Life_Care_Plan_Cost >= totalBills && Life_Care_Plan_Cost >= Property_Damage && Life_Care_Plan_Cost >= totalCoverage){
            maxNumber = Life_Care_Plan_Cost;
        }
        return findMaxRange(maxNumber);
    }
    public String findMaxRange(Decimal maxNumber){
        String range ='';
        while (maxNumber >= 100 ){
            maxNumber = maxNumber/10;
            range += '0';
        }
        return range; 
    }
}