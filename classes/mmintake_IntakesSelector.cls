/**
 *  Main Selector class for the Intake__c SOBject.
 *
 */
public with sharing class mmintake_IntakesSelector
    extends mmlib_SObjectSelector
    implements mmintake_IIntakesSelector
{
    public static mmintake_IIntakesSelector newInstance()
    {
        return (mmintake_IIntakesSelector) mm_Application.Selector.newInstance(Intake__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Intake__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            Intake__c.Caller__c,
            Intake__c.Caller_is_Injured_Party__c,
            Intake__c.Can_IP_sign__c,
            Intake__c.Case_Manager_Name__c,
            Intake__c.Case_Notes__c,
            Intake__c.Case_Type__c,
            Intake__c.Case_Subtype__c,
            Intake__c.Catastrophic__c,
            Intake__c.City_of_incident__c,
            Intake__c.Client__c,
            Intake__c.Client_Email_txt__c,
            Intake__c.Client_First_Last_Name__c,
            Intake__c.County_of_incident__c,
            Intake__c.DocusignTemplateID__c,
            Intake__c.Email__c,
            Intake__c.Gender__c,
            Intake__c.Generating_Attorney__c,
            Intake__c.Handling_Firm__c,
            Intake__c.Inbound_Referring_Attorney__c,
            Intake__c.Injured_Party__c,
            Intake__c.Injured_party_relationship_to_Caller__c,
            Intake__c.Injured_party_relationship_to_Client__c,
            Intake__c.Intake_Name__c,
            Intake__c.IsMatterActive__c,  // TODO: this field will most likely move out to the MMMATTER namespace
            Intake__c.Last_Dial_Time__c,
            Intake__c.Limited_tort__c,
            Intake__c.Lead_Details__c,
            Intake__c.Litigation__c,
            Intake__c.Marketing_Source__c,
            Intake__c.Marketing_Sub_source__c,
            Intake__c.Number_Outbound_Dials_Intake__c,
            Intake__c.Number_Outbound_Dials_Status__c,
            Intake__c.State_of_incident__c,
            Intake__c.Status__c,
            Intake__c.Status_Detail__c,
            Intake__c.Treating_Facility__c,
            Intake__c.Type_of_Policy__c,
			Intake__c.Where_did_the_injury_occur__c,
            Intake__c.Who_can_legally_sign_on_behalf_of_IP__c
        };
    }

    /**
     *  Primary selector for Intake__c SObject
     *
     *  @param idSet the Set of record ids to query
     *  @return list of Intake__c records
     */
    public List<Intake__c> selectById(Set<Id> idSet)
    {
        return (List<Intake__c>) selectSObjectsById(idSet);
    }

    public List<Intake__c> selectByIdWithCallerClientAndInjuredParty(Set<Id> idSet)
    {
        fflib_QueryFactory intakesQueryFactory = newQueryFactory().setCondition('Id in :IdSet');

        new mmcommon_AccountsSelector().addQueryFactoryParentSelect( intakesQueryFactory, Intake__c.Caller__c );
        new mmcommon_AccountsSelector().addQueryFactoryParentSelect( intakesQueryFactory, Intake__c.Client__c );
        new mmcommon_AccountsSelector().addQueryFactoryParentSelect( intakesQueryFactory, Intake__c.Injured_Party__c );

        return Database.query( intakesQueryFactory.toSOQL() );
    }

    public List<Intake__c> selectByIdWithCallerClientInjuredPartyAndAdditionalFields(Set<Id> idSet, list<Schema.FieldSet> intakeFieldSetList, Schema.FieldSet relatedEventFieldSet)
    {
        fflib_QueryFactory intakesQueryFactory = newQueryFactory().setCondition('Id in :IdSet');

        if ( intakeFieldSetList != null )
        {
            for ( Schema.FieldSet aFieldSet : intakeFieldSetList )
            {
                if ( aFieldSet != null )
                {
                    if ( aFieldSet.getSObjectType() != Intake__c.SObjectType )
                    {
                        throw new mmintake_IntakesSelectorExceptions.IncorrectFieldSetSObjectTypeExpectedException( aFieldSet.getName() + ' fieldset was supplied but it is not an Intake__c SObjectType.  It is a ' + aFieldSet.getSObjectType() + ' SObjectType.');
                    }
                    intakesQueryFactory.selectFieldSet( afieldSet, true );
                }
            }
        }

        if ( relatedEventFieldSet != null )
        {
            if ( relatedEventFieldSet.getSObjectType() != Event.SObjectType )
            {
                throw new mmintake_IntakesSelectorExceptions.IncorrectFieldSetSObjectTypeExpectedException( relatedEventFieldSet.getName() + ' fieldset was supplied but it is not an Event SObjectType.  It is a ' + relatedEventFieldSet.getSObjectType() + ' SObjectType.');
            }
            new mmcommon_EventsSelector().addQueryFactorySubselect( intakesQueryFactory, true ).selectFieldSet( relatedEventFieldSet, true ).setCondition('ActivityDate >= TODAY');
        }

        new mmcommon_AccountsSelector().addQueryFactoryParentSelect( intakesQueryFactory, Intake__c.Caller__c );
        new mmcommon_AccountsSelector().addQueryFactoryParentSelect( intakesQueryFactory, Intake__c.Client__c );
        new mmcommon_AccountsSelector().addQueryFactoryParentSelect( intakesQueryFactory, Intake__c.Injured_Party__c );

        return Database.query( intakesQueryFactory.toSOQL() );
    }

    public List<Intake__c> selectByClientId(Set<Id> idSet)
    {
        return
            (List<Intake__c>)
            Database.query(
                newQueryFactory()
                .setCondition(Intake__c.Client__c + ' in :idSet')
                .toSOQL()
            );
    }

    public List<Intake__c> selectWithFieldsetByClientId(Schema.FieldSet fs, Set<Id> idSet)
    {
        return
            (List<Intake__c>)
            Database.query(
                newQueryFactory()
                .selectFieldSet(fs)
                .setCondition(Intake__c.Client__c + ' in :idSet')
                .toSOQL()
            );
    }

    public List<Intake__c> selectByGeneratingAttorneyAndCreatedDateRange( set<id> generatingAttorneyIdSet, Date startDate, Date endDate )
    {
        set<string> ADDITIONAL_FIELDS_SET = new set<String>{ 'Client__r.FirstName', 'Client__r.LastName' };

        String queryCondition = Intake__c.Generating_Attorney__c + ' in :generatingAttorneyIdSet'
                                + ' and CreatedDate >= :startDate'
                                + ' and CreatedDate <= :endDate';

        return Database.query( newQueryFactory().setCondition( queryCondition ).selectFields( ADDITIONAL_FIELDS_SET ).toSOQL() );
    }

    public List<Intake__c> selectByUniqueSubmissionKey(Set<String> uniqueSubmissionKeySet)
    {
        System.debug('JLW:::' + JSON.serialize(uniqueSubmissionKeySet));
        return
            (List<Intake__c>)
            Database.query(
                newQueryFactory()
                .setCondition(Intake__c.UniqueSubmissionKey__c + ' in :uniqueSubmissionKeySet')
                .toSOQL()
            );
    }

    public List<Intake__c> selectBySubmissionId(Set<String> submissionIdSet)
    {
        return
            (List<Intake__c>)
            Database.query(
                newQueryFactory()
                .setCondition(Intake__c.SubmissionId__c + ' in :submissionIdSet')
                .toSOQL()
            );
    }

    public List<Intake__c> selectMostRecentMesoReferral()
    {
        fflib_QueryFactory qf =
            newQueryFactory()
                .setCondition(
                    Intake__c.Case_Type__c + ' = \'Asbestos\' and ' +
                    Intake__c.Status__c + ' = \'Referred Out\' and ' +
                    Intake__c.MESO_Firm_Referred__c + ' != null')
                .setlimit(1);

        qf.getOrderings().clear();
        qf.addOrdering(Intake__c.CreatedDate, fflib_QueryFactory.SortOrder.DESCENDING, false);

        return (List<Intake__c>) Database.query(qf.toSOQL());
    }

    public List<Intake__c> selectByCiscoCallId(String ciscoCallId) {
        return selectByCiscoCallId(ciscoCallId, -1);
    }

    public List<Intake__c> selectByCiscoCallId(String ciscoCallId, Integer deltaInHours) {
        Datetime deltaDatetime = Datetime.now().addHours(deltaInHours);
        return
                (List<Intake__c>)
                        Database.query(
                                newQueryFactory()
                                        .setCondition('Intake__c.LastModifiedDate > :deltaDatetime AND Intake__c.Cisco_Call_ID__c LIKE \'%' + ciscoCallId + '%\'')
                                        .toSOQL()
                        );
    }

} //class