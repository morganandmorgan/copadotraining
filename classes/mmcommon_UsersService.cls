public class mmcommon_UsersService {
    private static mmcommon_IUsersService service() {
        return (mmcommon_IUsersService)mm_Application.Service.newInstance(mmcommon_IUsersService.class);
    }
    public static List<user> searchUsersUsingFuzzyMatching(Map<String, String> data) {
        return service().searchUsersUsingFuzzyMatching(data);
    }
}