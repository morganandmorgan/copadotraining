/**
 * TriggerSettingsSelector
 * @description Trigger Settings Selector implementation.
 * @author Jeff Watson
 * @date 10/26/2018
 */
public with sharing class TriggerSettingsSelector {

    private static List<String> queryFields = new List<String> {
            'Id',
            'Accounts__c',
            'Docusign_Status__c',
            'Enable_All_Triggers__c',
            'Expense_Enable_Summaries__c',
            'Expense_Minutes_Back_To_Process__c',
            'Incident_Investigations__c',
            'Intake_Enable_TOG_Integration__c',
            'Intake_Investigations__c',
            'Intakes__c',
            'Investigations__c',
            'Litify_Expenses__c',
            'Litify_Matters__c',
            'Litify_Referrals__c',
            'Litify_Roles__c',
            'Litify_Team_Members__c',
            'Matter_Compute_SOL__c',
            'Matter_Task_Histories__c',
            'Matter_Update_Deceased__c',
            'Spring_Accounts__c'
    };

    public static Trigger_Settings__mdt selectDefaultSettings() {
        String queryStr = 'SELECT ' + String.join(queryFields, ',') + ' FROM Trigger_Settings__mdt LIMIT 1';
        List<Trigger_Settings__mdt> triggerSettings = Database.query(queryStr);

        if(triggerSettings.size() > 0) {
            return triggerSettings[0];
        }

        return null;
    }
}