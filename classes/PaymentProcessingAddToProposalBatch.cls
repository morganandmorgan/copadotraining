public class PaymentProcessingAddToProposalBatch implements Database.Batchable<SObject>, Database.Stateful {
    
    public Payment_Collection__c paymentCollection {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    public Id paymentCollectionId {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    
    public Boolean launchCreatePaymentMediaBatchProcess {get; set;}   //chain create-payment-media batch
    public Integer paymentMediaBatchScopeSize {get; set;} 

    public PaymentProcessingAddToProposalBatch(Payment_Collection__c paymentCollection){
        this.launchCreatePaymentMediaBatchProcess=true;   //default
        this.paymentMediaBatchScopeSize=2;   //default
        this.paymentCollection=paymentCollection;
        this.paymentCollectionId=paymentCollection.Id;
    }	
    
    public PaymentProcessingAddToProposalBatch(Id paymentCollectionId){
        this.launchCreatePaymentMediaBatchProcess=true;   //default
        this.paymentMediaBatchScopeSize=2;   //default
        this.paymentCollectionId=paymentCollectionId;
    }

    public PaymentProcessingAddToProposalBatch(Id paymentCollectionId, Boolean launchCreatePaymentMediaBatchProcess){
        this.launchCreatePaymentMediaBatchProcess=true;   //default
        this.paymentMediaBatchScopeSize=2;   //default
        this.paymentCollectionId=paymentCollectionId;
        if(launchCreatePaymentMediaBatchProcess!=null){
            this.launchCreatePaymentMediaBatchProcess=launchCreatePaymentMediaBatchProcess;
        }
    }
    
    public PaymentProcessingAddToProposalBatch(Id paymentCollectionId, Boolean launchCreatePaymentMediaBatchProcess, Integer paymentMediaBatchScopeSize){
        // this.launchCreatePaymentMediaBatchProcess=true;   //default
        // this.paymentMediaBatchScopeSize=2;   //default
        this.paymentCollectionId=paymentCollectionId;
        if(launchCreatePaymentMediaBatchProcess!=null){
            this.launchCreatePaymentMediaBatchProcess=launchCreatePaymentMediaBatchProcess;
            this.paymentMediaBatchScopeSize = paymentMediaBatchScopeSize;
        }
    }    

    //
    
    public Database.QueryLocator start(Database.BatchableContext batchContext) {
        String soql=null;
        soql= 'select ' + 
            'Id' + 
            ',Name ' + 
            ',Batch_Id__c ' + 
            ',Message__c ' + 
            ',Last_Error_Message__c ' + 
            ',Has_Error__c ' + 
            '' + 
            ',Payment_Collection__c ' + 
            ',Payment_Collection__r.Name ' + 
            ',Payment__c ' + 
            ',Payment__r.c2g__Status__c ' + 
            ',Payment__r.Name ' + 
            ',Transaction_Line_Item__c ' + 
            ',Transaction_Line_Item__r.Name ' + 
            '' + 
            ',Is_Added_To_Proposal__c ' + 
            ',Payment_Log__c ' + 
            
            'from Payment_Process_Add_To_Proposal_Status__c ' + 
            'where ' + 
            'Is_Added_To_Proposal__c != true ' + 
            'and Payment__r.c2g__Status__c in (\'New\', \'Proposed\') ' + 
            'and Payment_Collection__c = \'' + paymentCollectionId + '\' ';
        
        return Database.getQueryLocator(
            soql
        );
    }
    
    public void execute(Database.BatchableContext batchContext, List<Payment_Process_Add_To_Proposal_Status__c> scope){
        // process each batch of records
        //TODO:  Trap and register exceptions:  
        System.debug('PaymentProcessingAddToProposalBatch execute() - ' + scope.size() + ' records.'); 
        Map<Id, Id> paymentIdMap=null;	//unique list/set of payments for the batch scope.
        paymentIdMap=new Map<Id, Id>();
        
        List<Id> paymentErrorlogIds = null;
        paymentErrorlogIds=new List<Id>();
        
        //Index payment-level tracking records by paymentId:
        Map<String, Payment_Process_Add_To_Proposal_Status__c> addToProposalTrackingRecordsByPaymentAndTransLineId=null;
        addToProposalTrackingRecordsByPaymentAndTransLineId=new Map<String, Payment_Process_Add_To_Proposal_Status__c>();

        Map<Id, List<Id> > transactionLineIdsByPaymentId=null;
        transactionLineIdsByPaymentId=new Map<Id, List<Id> >();

        for (Payment_Process_Add_To_Proposal_Status__c addToProposalStatus : scope) {
			//Gather all the transaction lines that go with each payment Id:  
			Id paymentId = null;
            paymentId = addToProposalStatus.Payment__c;

            if(!paymentIdMap.containsKey(paymentId)){
                paymentIdMap.put(paymentId, paymentId);                
            }  

            if((addToProposalStatus.Payment__r.Name!=null)&&(addToProposalStatus.Transaction_Line_Item__r.Name!=null)){
                addToProposalStatus.Name = addToProposalStatus.Payment__r.Name + ' - ' + addToProposalStatus.Transaction_Line_Item__r.Name;
            }
            
			List<Id> transactionLineItemIds = null;
            
            if(transactionLineIdsByPaymentId.containsKey(paymentId)){
                //get it:
                transactionLineItemIds=transactionLineIdsByPaymentId.get(paymentId);
            }else{
                transactionLineItemIds = new List<Id>{addToProposalStatus.Transaction_Line_Item__c};	//one line at a time for now...
                transactionLineIdsByPaymentId.put(paymentId, transactionLineItemIds);
            }

            transactionLineItemIds.add(addToProposalStatus.Transaction_Line_Item__c);

            //Index into the map for later collation with error log records:  
            String key=null;
            key = addToProposalStatus.Payment__c + '|' + addToProposalStatus.Transaction_Line_Item__c;
            addToProposalTrackingRecordsByPaymentAndTransLineId.put(key, addToProposalStatus);

        }

        for (Id paymentId: paymentIdMap.keySet()) {
			//TODO:  c2g.PaymentsPlusService.addToProposal(pay.Id, transLineIdList);
			//TODO:  Batch these, make one call for the scope for each Payment Id.
			System.debug('Add to proposal batch:  ' + paymentId);
            
			List<Id> transactionLineItemIds = null;            
            transactionLineItemIds = transactionLineIdsByPaymentId.get(paymentId);
            
			List<Id> logIds = null;
            //Non-async - we're already in a batch context:  
            //This service returns a list containing all new log IDs generated during the Add to Proposal process.
            logIds = c2g.PaymentsPlusService.addToProposal(paymentId, transactionLineItemIds);

            for(Id transactionLineId:transactionLineItemIds){
                String key=null;
                key = paymentId + '|' + transactionLineId;

                Payment_Process_Add_To_Proposal_Status__c addToProposalStatus = null;
                addToProposalStatus=addToProposalTrackingRecordsByPaymentAndTransLineId.get(key);
                
                if(addToProposalStatus!=null){
                    //TODO:  Since we launched the batch, default to successful:                  
                    addToProposalStatus.Is_Added_To_Proposal__c = true;
                    addToProposalStatus.Message__c = 'Lines Added to Proposal';                
                }else{
                    System.debug('Error:  addToProposalStatus not found for ' + key);
                }

            }
                        
            if((logIds!=null)&&(logIds.size()>0)){
                System.debug('  logIds:  ' + logIds.size());
                
                /*
                for (Id logId : logIds) {
                    //Errors may be line-specific.
                    System.debug('  addToProposal() logId:  ' + logId);
                }
                */
                //One way to validate might be to query/assert new payment status after the call.                 
                paymentErrorlogIds.addAll(logIds);
                //At the moment, we're processing one Trans Line at a time:  
                //Get the entry for the payment and the trans line:  
                //batchStatus.Payment_Log__c=logIds[0];
            }                        
        }
               
        List<c2g__PaymentsPlusErrorLog__c> paymentLogRecords=null;

        if((paymentErrorlogIds!=null)&&(paymentErrorlogIds.size()>0)){
            //c2g__PaymentProposalNumber__c is the Payment Id.
            paymentLogRecords=
            [
                select 
                Id, 
                Name, 
                
                c2g__PaymentProposalNumber__c, 
                c2g__TransactionLineItem__c,
                
                c2g__Account__c, 
                c2g__AttemptNumber__c, 
                c2g__ErrorReason__c, 
                c2g__LogType__c, 
        
                c2g__PaymentStage__c, 
                c2g__SourceDocument__c 
                
                from c2g__PaymentsPlusErrorLog__c
                where 
                Id in :paymentErrorlogIds
            ];

        }
        
        //Index logs:
        Map<String, c2g__PaymentsPlusErrorLog__c> paymentLogsByPaymentAndTransLineId=null;
        paymentLogsByPaymentAndTransLineId=new Map<String, c2g__PaymentsPlusErrorLog__c>();
        
        Set<Id> paymentIds=null;
        paymentIds=paymentIdMap.keySet();
        
        //TODO:  Factor this into a method:  
        List<Payment_Processing_Batch_Status__c> paymentTaskTrackingRecords=null;
        paymentTaskTrackingRecords=
        [
            select 
            Id
            ,Name 
            
            ,Message__c 
            
            ,Payment_Collection__c
            ,Payment__c
            
            ,Are_Proposal_Lines_Added__c
            ,Is_Media_Data_Created__c
            
            from Payment_Processing_Batch_Status__c 
            where 
            Payment__c in :paymentIds
		]; 
            
        //Are_Proposal_Lines_Added__c = false              
        
        //Index payment-level tracking records by paymentId:
        Map<Id, Payment_Processing_Batch_Status__c> paymentTrackingRecordsByPaymentId=null;
        paymentTrackingRecordsByPaymentId=new Map<Id, Payment_Processing_Batch_Status__c>();
        
        //First pass to default payment status to success:  
        for(Payment_Processing_Batch_Status__c paymentStatus:paymentTaskTrackingRecords){
            paymentStatus.Are_Proposal_Lines_Added__c = true;
            paymentStatus.Message__c='Lines Added to Proposal';	//TODO:  Constant.
            //Index:  
            paymentTrackingRecordsByPaymentId.put(paymentStatus.Payment__c, paymentStatus);
        }
                       
        //TODO:  Collate error logs with tracking records, flag as error:
        if((paymentLogRecords!=null)&&(paymentLogRecords.size()>0)){
            for(c2g__PaymentsPlusErrorLog__c log: paymentLogRecords){
                //Index logs:
                Id paymentId=null;
                paymentId=log.c2g__PaymentProposalNumber__c;
                
                String key=null;
                key = log.c2g__PaymentProposalNumber__c + '|' + log.c2g__TransactionLineItem__c;
                paymentLogsByPaymentAndTransLineId.put(key, log);
                
                //Get the tracking records that go with the log: 
                Payment_Process_Add_To_Proposal_Status__c addToProposalStatus = null;
                Payment_Processing_Batch_Status__c paymentStatus = null; 
                
                addToProposalStatus=addToProposalTrackingRecordsByPaymentAndTransLineId.get(key);
                paymentStatus=paymentTrackingRecordsByPaymentId.get(paymentId);
                
                //TODO:  Hook up log Id, flag errors, set message, error message, un-check checkbox:
                addToProposalStatus.Payment_Log__c = log.Id;
                if(log.c2g__LogType__c=='Error'){
                    addToProposalStatus.Is_Added_To_Proposal__c = false;                
                    addToProposalStatus.Message__c = 'Add to Proposal Error';
                    addToProposalStatus.Has_Error__c = true;
                    addToProposalStatus.Last_Error_Message__c=log.c2g__ErrorReason__c;
                    
                    //TODO:  Loop through batchstatuses - a second pass to flag payments with any successful lines?
                    //Some lines might have been added for the payment:  
                    if(paymentStatus!=null){
                        paymentStatus.Are_Proposal_Lines_Added__c = false;
                        paymentStatus.Message__c='Add-to-Proposal Error:  ' + log.c2g__ErrorReason__c;
                        paymentStatus.Last_Error_Message__c='Add-to-Proposal Error:  ' + log.c2g__ErrorReason__c;
                        paymentStatus.Has_Error__c = true;
                    }else{
                        System.debug('Payment status record not found:  ' + paymentId);
                    }
                }
                
            }
        }
        
        //Second pass to default payment status:  
        for (Payment_Process_Add_To_Proposal_Status__c addToProposalStatus : scope) {
			
            if(!addToProposalStatus.Has_Error__c){
                //Flag the non-errored ones:  
                //No error for this line:  
                Id paymentId = null;
                paymentId = addToProposalStatus.Payment__c;
                
                Payment_Processing_Batch_Status__c paymentStatus = null;             
                
                paymentStatus=paymentTrackingRecordsByPaymentId.get(paymentId);
                
                if(paymentStatus!=null){
                    paymentStatus.Are_Proposal_Lines_Added__c = true;
                    paymentStatus.Message__c='Lines Added to Proposal';	//TODO:  Constant.
                }else{
                    //TODO:  Fix this bug.
                    System.debug('Payment status tracking record not found:  ' + paymentId);
                }

            }
            
        }        
        
        {	//Store the outcome:   
            
            //SetPoint?  So the two updates happen together or fail as a transaction?
            
            update scope;	//update the batch status records for this chunk of scope.
            
            //For the successfully updated payments, update the related Payment_Processing_Batch_Status__c payment-level records.
            //Payment_Processing_Batch_Status__c
            update paymentTaskTrackingRecords;
            
        }
        
    }
    
    public void finish(Database.BatchableContext batchContext){
        
        System.debug('PaymentProcessingAddToProposalBatch finished.');

        if(launchCreatePaymentMediaBatchProcess){

            //Create Payment Media:
            //Chain:  create payment media has to happen after add-to-proposal.
                            
            System.debug('Launching/chaining Create-Payment-Media.  Payment Collection:  ' + (paymentCollectionId));
            PaymentProcessingCreatePaymentMediaBatch batchProcessor = null;

            batchProcessor=new PaymentProcessingCreatePaymentMediaBatch(paymentCollectionId);

            Id batchProcessId = null;

            if((paymentMediaBatchScopeSize!=null)&&(paymentMediaBatchScopeSize>0)){
                //Use a specified batch size:  
                //Does it depend on checks or not?
                batchProcessId = Database.executeBatch(batchProcessor, paymentMediaBatchScopeSize);
            }else{
                batchProcessId = Database.executeBatch(batchProcessor);
            }

            System.debug('Create-Payment-Media batchProcessId:  ' + batchProcessId);                            
                        
        }

    }
    
}