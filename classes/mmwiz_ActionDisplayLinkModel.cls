public class mmwiz_ActionDisplayLinkModel
    extends mmwiz_AbstractActionModel
{
    private static set<String> MMWIZ_PARAMS_NOT_TO_PASS_THROUGH_SET = new set<String>{ 'questionnaire', 'page' };

    private string href;
    private Boolean isOriginalURLParametersIncludedInLink = false;

    public mmwiz_ActionDisplayLinkModel()
    {
        setType(mmwiz_ActionDisplayLinkModel.class);
        setIsUIBasedAction(true);
    }

    public override void initialize( Wizard__mdt wizard )
    {
        super.initialize( wizard );

        if ( this.serializedDataMap.containsKey( 'href' ))
        {
            setHref( (String)serializedDataMap.get( 'href' ) );
        }

        if ( this.serializedDataMap.containsKey( 'isOriginalURLParametersIncludedInLink' ) )
        {
            setIsOriginalURLParametersIncludedInLink( (String)serializedDataMap.get( 'isOriginalURLParametersIncludedInLink' ) );
        }
    }

    public void setHref( string hrefString )
    {
        this.href = hrefString;
    }

    public string getHref()
    {
        return this.href;
    }

    public void setIsOriginalURLParametersIncludedInLink( Boolean value )
    {
        this.isOriginalURLParametersIncludedInLink = (value == null ? false : value);
    }

    public void setIsOriginalURLParametersIncludedInLink( string value )
    {
        try
        {
            this.isOriginalURLParametersIncludedInLink = Boolean.valueOf( value );
        }
        catch (Exception e)
        {
            system.debug('Unable to use value supplied of \'' + value + '\'.  Defaulting to false.');
            this.isOriginalURLParametersIncludedInLink = false;
        }
    }

    public Boolean getIsOriginalURLParametersIncludedInLink()
    {
        return this.isOriginalURLParametersIncludedInLink;
    }

    public void addPageParametersToLink( PageReference pageRef )
    {
        if ( pageRef != null && this.href != null )
        {
            boolean isAParameterAlreadyPresent = this.href.contains('?');

            for ( string paramKey : pageRef.getParameters().keyset() )
            {
                if ( ! this.href.containsIgnoreCase( paramKey + '=' )   // don't add the url param a second time if it is already there.
                    && ! MMWIZ_PARAMS_NOT_TO_PASS_THROUGH_SET.contains( paramKey.toLowerCase() )  // don't add if the param is a mmwiz wizard page specific param.
                    )
                {
                    if ( ! isAParameterAlreadyPresent )
                    {
                        this.href += '?';
                        isAParameterAlreadyPresent = true;
                    }
                    else
                    {
                        this.href += '&';
                    }

                    this.href += paramKey + '=' + pageRef.getParameters().get(paramKey);
                }
            }
        }
        //system.debug( 'this.href == ' + this.href);
    }
}