public class mmlitifylrn_ReferralsSyncQueueable
        implements Queueable, Database.AllowsCallouts
{
    private Datetime lastSyncOverride = null;
    private mmlitifylrn_ThirdPartyCredential__c credential;

    public mmlitifylrn_ReferralsSyncQueueable(mmlitifylrn_ThirdPartyCredential__c credential)
    {
        this.Credential = credential;
    }

    public mmlitifylrn_ReferralsSyncQueueable setLastSyncOverride(Datetime lastSync)
    {
        this.lastSyncOverride = lastSync;
        return this;
    }

    public void execute(QueueableContext context)
    {
        // Override last sync if desired by calling code
        if (this.lastSyncOverride != null)
        {
            System.debug('TPR:::Overriding last sync for ' + credential.Name + ' with ' + this.lastSyncOverride);
            credential.Last_Sync__c = lastSyncOverride;
        }

        // Get creds, callout auth, etc setup to make calls to Litify.
        litify_pm__Firm__c firm = new litify_pm__Firm__c(Name = credential.Name);
        Set<String> firmSet = new Set<String>();
        firmSet.add(firm.Name);

        mmlitifylrn_ICredentials currentCreds = mmlitifylrn_Credentials.newInstance(mmlitifylrn_CredentialsSelector.newInstance().selectByName(firmSet));
        mmlitifylrn_CalloutAuthorization auth = new mmlitifylrn_CalloutAuthorization().setCrendentials(currentCreds).setCurrentFirmMakingCallout(firm);
        mmlitifylrn_RefIntkGetAll4SFDCGetCallout callout = (mmlitifylrn_RefIntkGetAll4SFDCGetCallout) new mmlitifylrn_RefIntkGetAll4SFDCGetCallout()
                .setLastSync(credential.Last_Sync__c)
                .setAuthorization(auth);
        mmlitifylrn_RefIntkGetAll4SFDCGetCallout.Response resp = null;

        // Ready some paging variables while we process paged results
        Set<Decimal> lrnExternalIds = new Set<Decimal>();
        do
        {
            // Get the page of referrals
            Integer currentPage = (resp == null) ? 1 : Integer.valueOf(resp.current_page) + 1;
            resp = (mmlitifylrn_RefIntkGetAll4SFDCGetCallout.Response) callout.setPage(currentPage).execute().getResponse();

            // Add the referral ids to the set
            for (mmlitifylrn_LRNModels.ReferralIntakeBasicData referral : resp.referrals)
            {
                lrnExternalIds.add(Decimal.valueOf(referral.id));
            }

            System.debug('TPR:::Processed page ' + resp.current_page);
        } while (Integer.valueOf(resp.current_page) < Integer.valueOf(resp.total_pages));

        // Finally, hand off all the referrals to be processed in batch
        if(lrnExternalIds.size() > 0)
        {
            new mmlib_GenericBatch(mmlitifylrn_ReferralsSyncExecutor.class, mmlitifylrn_ReferralsSelector.newInstance().selectWithFirmByExternalIdQuery(), lrnExternalIds)
                    .setBatchSizeTo(100)
                    .execute();
        }
    }

    private Boolean isANewTransactionNeeded(litify_pm__Referral__c referral, mmlitifylrn_LRNModels.ReferralIntakeBasicData returnedReferral)
    {
        if (referral.litify_pm__Referral_Transactions__r == null || referral.litify_pm__Referral_Transactions__r.size() == 0) return true;

        if (returnedReferral.handlingFirmHasChanged()) return true;

        return false;
    }

    private litify_pm__Referral_Transaction__c getReferralTransactionByExternalId(List<litify_pm__Referral__c> referrals, Decimal litifyTransactionId)
    {
        litify_pm__Referral_Transaction__c existingTransaction = null;
        for (litify_pm__Referral__c referral : referrals)
        {
            for (litify_pm__Referral_Transaction__c referralTransaction : referral.litify_pm__Referral_Transactions__r)
            {
                if (referralTransaction.litify_pm__ExternalId__c == litifyTransactionId)
                {
                    return referralTransaction;
                }
            }
        }

        return null;
    }
}