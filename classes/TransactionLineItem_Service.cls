/*============================================================================/
* TransactionLineItem_Service
* @description Service class for FFA Transaction Line Items.
* @author Brian Krynitsky
* @date 2/20/2019
=============================================================================*/

public class TransactionLineItem_Service {

	// Ctors
	public TransactionLineItem_Service() {

	}

	// Methods
	/*-----------------------------------------------------------------------------
    Method to create Trust Transactions on related matter
    -----------------------------------------------------------------------------*/
	public void createTrustTransactions(List<c2g__codaTransactionLineItem__c> newTrigger) {

		//boost the coverage since we cannot set the matter field on insert:
		/*if(Test.isRunningTest()){
			boostTest();
		}*/

		List<Trust_Transaction__c> trustTransactionList = new List<Trust_Transaction__c>();

		//get the ids so we can pass to the selector class
		Set<Id> newTriggerIds = new Set<Id>();
		for(c2g__codaTransactionLineItem__c trans : newTrigger){
			newTriggerIds.add(trans.Id);
		}

		TransactionLineItem_Selector selector = new TransactionLineItem_Selector();
		
		List<c2g__codaTransactionLineItem__c> tliList = selector.selectById_TrustTransaction(newTriggerIds);
		system.debug('tliList = ' + tliList);
 
		for(c2g__codaTransactionLineItem__c tli : tliList){
			
			//set some variables based on tli conditions
			String typeString = tli.c2g__HomeValue__c > 0 ? 'Deposit' : 'Withdrawal';
			String checkNumber = tli.Deposit__r.Check_Number__c != null ? tli.Deposit__r.Check_Number__c : tli.c2g__LineReference__c != null ?  tli.c2g__LineReference__c : ''; 
			String payeeName = tli.Deposit__c != null ? tli.Matter__r.AssignedToMMBusiness__r.Name + ' - Deposit' : tli.c2g__Account__r.Name != null ? tli.c2g__Account__r.Name : ''; 

			//init new Trust Transactions:
			Trust_Transaction__c newTT = new Trust_Transaction__c(
				Amount__c = tli.c2g__HomeValue__c,
				Description__c = tli.Deposit__r.Deposit_Description__c != null ? tli.Deposit__r.Deposit_Description__c : typeString + '-' + checkNumber,
				Bank_Account__c = tli.c2g__BankAccount__c,
				Date__c = tli.c2g__Transaction__r.c2g__TransactionDate__c,
				Matter__c = tli.Matter__c,
				Related_Deposit__c = tli.Deposit__c,
				Check_Number__c = checkNumber,
				Payee_Name__c = payeeName,
				FFA_Transaction_Line__c = tli.Id,
				Type__c = typeString);
			trustTransactionList.add(newTT);
		}
		insert trustTransactionList;
	}

	/*-----------------------------------------------------------------------------
    Method to derive the matter lookup on transaction lines for account balances and bank payments
    -----------------------------------------------------------------------------*/
	public void populateMatterField(List<c2g__codaTransactionLineItem__c> newTrigger) {
		
		Set<Id> transIdSet = new Set<Id>();
		Set<Id> paymentIdSet = new Set<Id>();
		Map<Id, c2g__codaTransaction__c> transMap = new Map<Id,c2g__codaTransaction__c>();
		Map<String, Id> transPmtMap = new Map<String,Id>();

		for(c2g__codaTransactionLineItem__c tli : newTrigger){
			transIdSet.add(tli.c2g__Transaction__c);
		}
		for(c2g__codaTransaction__c trans : [SELECT 
				Id, 
				c2g__CashEntry__r.c2g__PaymentNumber__c,
				c2g__CashEntry__r.c2g__Account__c,
				Matter__c 
			FROM c2g__codaTransaction__c
			WHERE Id in :transIdSet]){
			paymentIdSet.add(trans.c2g__CashEntry__r.c2g__PaymentNumber__c);
			transMap.put(trans.Id, trans);
		}

		for(c2g__codaPaymentMediaSummary__c pms : [SELECT 
				Id,
				c2g__Account__c,
				c2g__PaymentMediaControl__r.c2g__Payment__c,
				(SELECT Id, Payable_Invoice__r.Litify_Matter__c, Payable_Invoice__r.c2g__Transaction__c FROM c2g__PaymentMediaDetails__r
				WHERE Payable_Invoice__r.c2g__Transaction__c != null 
				AND Payable_Invoice__r.Litify_Matter__c != null)
			FROM c2g__codaPaymentMediaSummary__c
			WHERE c2g__PaymentMediaControl__r.c2g__Payment__c in :paymentIdSet]){
			String key = pms.c2g__Account__c + '|' + pms.c2g__PaymentMediaControl__r.c2g__Payment__c;
			for(c2g__codaPaymentMediaDetail__c pmd : pms.c2g__PaymentMediaDetails__r){
				transPmtMap.put(key, pmd.Payable_Invoice__r.Litify_Matter__c);
			}	
		}
		//set the matter from the related payment
		for(c2g__codaTransactionLineItem__c tli : newTrigger){
			String key = transMap.containsKey(tli.c2g__Transaction__c) && transMap.get(tli.c2g__Transaction__c).c2g__CashEntry__r.c2g__Account__c != null && transMap.get(tli.c2g__Transaction__c).c2g__CashEntry__r.c2g__PaymentNumber__c != null ? transMap.get(tli.c2g__Transaction__c).c2g__CashEntry__r.c2g__Account__c +'|'+ transMap.get(tli.c2g__Transaction__c).c2g__CashEntry__r.c2g__PaymentNumber__c : '';
			tli.Matter__c = tli.Matter__c == null && transPmtMap.containsKey(key) ? transPmtMap.get(key) : tli.Matter__c;
		}				

		for(c2g__codaTransactionLineItem__c tli : newTrigger){
			tli.Matter__c = tli.Matter__c == null && transMap.containsKey(tli.c2g__Transaction__c) ? transMap.get(tli.c2g__Transaction__c).Matter__c : tli.Matter__c;
		}		
	}

	

}