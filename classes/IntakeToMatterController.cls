global class IntakeToMatterController {
    
	public static void callIntakeToMatterWorkflow(string intakeId, string matterId) {
        SpringCMService service = new SpringCMService();

        SpringCMEos.SpringCMUtilities.EOSObject eosIntake = SpringCMEos.SpringCMUtilities.createEOSObject(intakeId, 'Intake__c');

        SpringCMEos.SpringCMUtilities.EOSObject eosMatter = SpringCMEos.SpringCMUtilities.createEOSObject(matterId, 'litify_pm__Matter__c');

        string xml = '<salesforce><from><id>' + eosIntake.getSfId() +'</id><type>' + eosIntake.getSfType() + '</type><foldername>' + eosIntake.getFoldername() + '</foldername><path>' + eosIntake.getPath() + '</path></from>';
        xml += '<to><id>' + eosMatter.getSfId() + '</id><type>' + eosMatter.getSfType() + '</type><foldername>' + eosMatter.getFoldername() + '</foldername><path>' + eosMatter.getPath() + '</path></to>';

        xml += '</salesforce>';
        SpringCMWorkflow workflow = new SpringCMWorkflow('Intake to Matter', xml);
        service.startWorkflow(workflow);
    }
}