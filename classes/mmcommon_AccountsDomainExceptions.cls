/**
 *  mmcommon_AccountsDomainExceptions
 */
public class mmcommon_AccountsDomainExceptions
{
    private mmcommon_AccountsDomainExceptions() { }

    public class ValidationException extends mmlib_BaseExceptions.ValidationException
    {
        public ValidationException( list<Account> records )
        {
            super( (list<SObject>)records );
        }
    }

}