public with sharing class mmlawsuit_OneLawsuitPerIntakeCriteria
    implements mmlib_ICriteria
{
    private List<Lawsuit__c> records = new List<Lawsuit__c>();

    public mmlib_ICriteria setRecordsToEvaluate(List<SObject> records)
    {
        if (records != null && Lawsuit__c.SObjectType == records.getSobjectType())
        {
            this.records.addAll((List<Lawsuit__c>) records);
        }

        return this;
    }

    public List<SObject> run()
    {
        List<SObject> qualifiedRecords = new List<SObject>();

        if (!records.isEmpty())
        {
            Map<Id, AggResult> arrResultMap = getLawsuitCountPerIntake(mmlib_Utils.generateIdSetFromField(records, Lawsuit__c.Intake__c, false));

            for (Lawsuit__c lawsuit : records)
            {
                AggResult ar = arrResultMap.get(lawsuit.Intake__c);

                if (ar != null && ar.count <= 1)
                {
                    qualifiedRecords.add(lawsuit);
                }
            }
        }

        return qualifiedRecords;
    }

    private static Map<Id, AggResult> getLawsuitCountPerIntake(Set<Id> intakeIdSet)
    {
        Map<Id, AggResult> result = new Map<Id, AggResult>();

        List<AggregateResult> aggregates = mmlawsuit_LawsuitsSelector.newInstance().selectAggregatesForLawsuitValidation(intakeIdSet);

        for (AggregateResult ar : aggregates)
        {
            // Not a SObject map.  Cannot use mmlib_Utils functions here.
            result.put((Id) ar.get('Intake__c'), new AggResult((Id) ar.get('Intake__c'), (Integer) ar.get('lawsuitcount')));
        }

        if (mock_AggResultList != null)
        {
            result.clear();
            for (AggResult ar : mock_AggResultList)
            {
                result.put(ar.intakeId, ar);
            }
        }

        return result;
    }

    @TestVisible
    private class AggResult
    {
        public Id intakeId = null;
        public Integer count = -1;

        public Aggresult(Id intakeId, Integer count)
        {
            this.intakeId = intakeId;
            this.count = count;
        }
    }

    // ============== Testing Context ==================================
    @TestVisible
    private static List<AggResult> mock_AggResultList = null;
}