public with sharing class ffaExpenseEntryController 
{

    public List<ExpenseHolder> expenseHolderList{get;set;}
    public Integer addRows{get;set;}
    public Integer startingNumber{get;set;}
    public Integer index{get;set;}
    public String matterId{get;set;}

	public ffaExpenseEntryController() 
    {
        expenseHolderList = new List<ExpenseHolder>();
        addRows = 1;
        startingNumber = 1;


        FOR (Integer i = 0; i < startingNumber; i++)
        {
            litify_pm__Expense__c expense = new litify_pm__Expense__c();
            expenseHolderList.add(new ExpenseHolder(expense));
        }
    }

    public PageReference addNewRows()
    {
        if (addRows != null)
        {
            FOR (Integer i = 0; i < addRows; i++)
            {
                litify_pm__Expense__c expense = new litify_pm__Expense__c(
                    Create_Payable_Invoice__c = true);
                expenseHolderList.add(new ExpenseHolder(expense));
            }
        }
        else
        {
            litify_pm__Expense__c expense = new litify_pm__Expense__c();
            expenseHolderList.add(new ExpenseHolder(expense));
        }
        return null;
    }

    public PageReference deleteRow()
    {
        if (index != null)
            expenseHolderList.remove(index);
        return null;
    }

    public PageReference save()
    {
        Boolean hasError = false;
        List<litify_pm__Expense__c> expensesToSave = new List<litify_pm__Expense__c>();
        // Validate first
        FOR (ExpenseHolder holder : expenseHolderList)
        {
            litify_pm__Expense__c expense = holder.expense;
            if (expense.litify_pm__Matter__c != null && holder.isClosedMatter)
            {
                expense.litify_pm__Matter__c.addError('Matter ' + holder.matterName + ' is closed');
                hasError = true;
            }
            if (expense.litify_pm__Matter__c != null)
            {
                // Verify there is a date and an amount.
                if (expense.litify_pm__Date__c == null)
                {
                    expense.litify_pm__Date__c.addError('Date needs to be set on expenses.');
                    hasError = true;
                }
                if (expense.litify_pm__Amount__c == null || expense.litify_pm__Amount__c == 0)
                {
                    expense.litify_pm__Amount__c.addError('Amount needs to be set on expenses.');
                    hasError = true;
                }
                if (hasError == false)
                    expensesToSave.add(expense);
            }
        }

        if (hasError)
            return null;
        try
        {

            Database.insert(expensesToSave, true);
        
            if (expenseHolderList.size() > 0)
            {
                // Currently going to the first expense in the list.
                litify_pm__Expense__c expense = expensesToSave.get(0);
                PageReference pr = new PageReference('/' + expense.Id);
                pr.setRedirect(false);
                return pr;
            }
        
        }
        catch (Exception e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'ERROR saving expenses: ' + e.getMessage()));
            return null;
        }
        return null;
    }

    public PageReference loadPayableTo()
    {
        System.debug('MATTER ID: ' + matterId);
        System.debug('INDEX: ' + index);

        if (matterId.length() == 0 || matterId  == '000000000000000')
            return null;

        List<Account> accounts = ffaExpenseHelper.getPayableToList(matterId);

        litify_pm__Matter__c matter = [SELECT Id, Finance_Status__c, Name FROM litify_pm__Matter__c WHERE Id = :matterId];
            
        ExpenseHolder holder = expenseHolderList.get(index);
        if (matter.Finance_Status__c == 'Closed')
        {
            holder.isClosedMatter = true;
            holder.matterName = matter.Name;
            holder.expense.litify_pm__Matter__c.addError('Matter ' + holder.matterName + ' is financially closed');
        }
        holder.showPayableTo = true;
        holder.payableList = accounts;
        return null;
    }

    public PageReference cancel()
    {
        PageReference pr = new PageReference('/');
        pr.setRedirect(false);
        return pr;
    }

    public class ExpenseHolder
    {
        public litify_pm__Expense__c expense{get;set;}
        public Boolean showPayableTo{get;set;}
        public LisT<Account> payableList;
        public Boolean isClosedMatter{get;set;}
        public String matterName{get;set;}
        public List<SelectOption> payableOptions{
            get
            {
                List<SelectOption> selOptions = new List<SelectOption>();
                selOptions.add(new SelectOption('', '-- Please select an account --'));
                FOR (Account account : payableList)
                {
                    SelectOption selOption = new SelectOption(account.Id, account.Name + 
                        (account.Last_Intake_Status__c != null ? ' - ' + account.Last_Intake_Status__c : ''));
                    selOptions.add(selOption);
                }
                return selOptions;
            }
            set;
        }

        public ExpenseHolder(litify_pm__Expense__c expense)
        {
            this.expense = expense;
            showPayableTo = false; 
            isClosedMatter = false;
        }


    }
}