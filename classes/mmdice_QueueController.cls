public with sharing class mmdice_QueueController {

    @AuraEnabled public static Model getModel(String queueId) {

        try {
            DICEService diceService = new DICEService();
            return new Model(diceService.getIntakes(queueId, 2000), diceService.SuppressedIntakes, diceService.BaseQuery);
        } catch(Exception e) {
           Utils.createDebugLog(e);
            return new Model();
        }
    }

    public class Model {

        @AuraEnabled public List<Intake> intakes;
        @AuraEnabled public List<Intake> suppressed_intakes;
        @AuraEnabled public String debug_query;

        public Model() {
            this.intakes = new List<Intake>();
            this.suppressed_intakes = new List<Intake>();
        }

        public Model(List<Intake__c> intakeSObjs, List<Intake__c> suppressedIntakeSObjs, String debugQuery) {

            this.intakes = new List<Intake>();
            for(Intake__c intakeSObj : intakeSObjs) {
                this.intakes.add(new Intake(intakeSObj));
            }

            this.suppressed_intakes = new List<Intake>();
            for(Intake__c suppressedIntakeSObj : suppressedIntakeSObjs) {
                this.suppressed_intakes.add(new Intake(suppressedIntakeSObj));
            }

            this.debug_query = debugQuery;
        }
    }

    public class Intake {

        @AuraEnabled public String intake_id { get; set; }
        @AuraEnabled public String intake_name { get; set; }
        @AuraEnabled public String account_name { get; set; }
        @AuraEnabled public String outbound_dials { get; set; }
        @AuraEnabled public String outbound_dial_status { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public String status_detail { get; set; }
        @AuraEnabled public String litigation { get; set; }
        @AuraEnabled public String case_type { get; set; }
        @AuraEnabled public String last_disposition { get; set; }
        @AuraEnabled public String next_call_date_and_time { get; set; }
        @AuraEnabled public String created_date { get; set; }
        @AuraEnabled public String record_type { get; set; }
        @AuraEnabled public String preferred_time_to_call { get; set; }
        @AuraEnabled public String input_channel { get; set; }

        public Intake(Intake__c intakeSObj) {
            this.intake_id = intakeSObj.Id;
            this.intake_name = intakeSObj.Name;
            this.account_name = intakeSObj.Client__r.Name;
            this.outbound_dials = String.valueOf(intakeSObj.Number_Outbound_Dials_Intake__c);
            this.outbound_dial_status = String.valueOf(intakeSObj.Number_Outbound_Dials_Status__c);
            this.status = intakeSObj.Status__c;
            this.status_detail = intakeSObj.Status_Detail__c;
            this.litigation = intakeSObj.Litigation__c;
            this.case_type = intakeSObj.Case_Type__c;
            this.last_disposition = intakeSObj.Last_Disposition__c;
            this.next_call_date_and_time = String.valueOf(intakeSObj.Client__r.NextCallDateTime__c);
            this.created_date = String.valueOf(intakeSObj.CreatedDate);
            this.record_type = intakeSObj.RecordType.Name;
            this.preferred_time_to_call = intakeSObj.Client__r.Preferred_Time_To_Call__c;
            this.input_channel = intakeSObj.InputChannel__c;
        }
    }
}