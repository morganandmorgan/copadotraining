/**
 *  Thrown to indicate that a block of code has not been implemented.  mmlib_NotImplementedException represents
 *  the case where the author has yet to implement the logic at this point in the program. This can act as an
 *  exception based TODO tag.
 *
 *  public void foo()
 *  {
 *      try
 *      {
 *          // do something that throws an Exception
 *      }
 *      catch (Exception ex)
 *      {
 *          // don't know what to do here yet
 *          throw new NotImplementedException("TODO", ex);
 *      }
 *  }
 *
 *  @author John M. Daniel <jdaniel@forthepeople.com>
 *  @since January 2017
 */
public class mmlib_NotImplementedException
    extends Exception
{

}