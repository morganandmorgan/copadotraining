/*==================================================================================
Name            : FFAPrepaidExpense_Job
Author          : CLD Partners
Created Date    : Aug 2018
Description     : Batch and implemenation for passing off expenses to create prepaid relief journals
==================================================================================*/
global class FFAPrepaidExpense_Job implements Database.Batchable<sObject>{

    global Date filterDate;
    global Id ffaCompanyId;
    global Boolean autoPost;

    /*============================================================================
    * CONSTRUCTOR
    =============================================================================*/
    global FFAPrepaidExpense_Job (Date selectedfilterDate, Id selectedffaCompanyId, Boolean selectedAutoPost) {
        filterDate = selectedfilterDate;
        ffaCompanyId = selectedffaCompanyId;
        autoPost = selectedAutoPost;
    }

    /*============================================================================
    * START
    =============================================================================*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,'+ 
           '(SELECT Id FROM litify_pm__Expenses__r WHERE litify_pm__Date__c <= :filterDate AND Prepaid_Expense_Type__c = true AND Prepaid_Relief_Processed__c = false AND Written_Off__c = false) '+
           'FROM litify_pm__Matter__c '+
           'WHERE AssignedToMMBusiness__r.FFA_Company__c = :ffaCompanyId';
        if(Test.isRunningTest() == TRUE)
        {
          query = query + ' LIMIT 1';
        }
        return Database.getQueryLocator(query);
    }

    /*============================================================================
    * EXECUTE
    =============================================================================*/
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<Id> expenseIds = new Set<Id>();
        for(Sobject s : scope){
            litify_pm__Matter__c matter = (litify_pm__Matter__c)s;
            for(litify_pm__Expense__c exp : matter.litify_pm__Expenses__r){
                expenseIds.add(exp.Id); 
            }
        }
        //create new labor journal reclasses:
        FFAPrepaidExpenseService.createPrepaidReliefJournals(expenseIds, ffaCompanyId, filterDate, autoPost);
    }

    /*============================================================================
    * FINISH
    =============================================================================*/
    global void finish(Database.BatchableContext BC) {
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id = :BC.getJobId()];

        // log Apex job details
        String apexJobDetails = '**** FFAPrepaidExpense_Job Finish()  status: ' + a.status + ' Total Jobs: ' + a.TotalJobItems + ' Errors: ' + a.NumberOfErrors;
        system.debug(apexJobDetails);
    }
}