/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBGiveUp {
    global PBGiveUp() {

    }
    @InvocableMethod(label='Give Up' description='Give up the given referral')
    global static void giveUp(List<litify_pm.PBGiveUp.ProcessBuilderGiveUpWrapper> giveUpItems) {

    }
global class ProcessBuilderGiveUpWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderGiveUpWrapper() {

    }
}
}
