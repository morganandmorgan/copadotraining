/**
 *  mmmarketing_LinkMFPToMrktngInfoAction
 */
public with sharing class mmmarketing_LinkMFPToMrktngInfoAction
    extends mmlib_AbstractAction
{
    public override Boolean runInProcess()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticFPLinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticFPLinkingEnabled__c )
        {
            // Query each of the parent objects
            Map<String, SObject> marketingSourceByNameMap = prepareMarketingSourceByNameMap();

            Map<String, Marketing_Campaign__c> marketingCampaignByTechnicalKeysMap = prepareMarketingCampaignByTechnicalKeysMap();

            boolean isDirty = false;

            integer i = 0;

            // loop through results and fill in gaps
            for (Marketing_Financial_Period__c record : (List<Marketing_Financial_Period__c>)this.records)
            {
                isDirty = false;

                if (record.Marketing_Source__c == null
                    && marketingSourceByNameMap.containsKey(record.Source_Value__c))
                {
                    record.Marketing_Source__c = marketingSourceByNameMap.get(record.Source_Value__c).id;
                    isDirty = true;
                }

                if (record.Marketing_Campaign__c == null)
                {
                    system.debug( record.id );

                    for (String campaignTechnicalKey : mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) )
                    {
                        if ( marketingCampaignByTechnicalKeysMap.containsKey( campaignTechnicalKey ) )
                        {
                            record.Marketing_Campaign__c = marketingCampaignByTechnicalKeysMap.get( campaignTechnicalKey ).id;
                            isDirty = true;
                            system.debug( ' one found ');
                            break;
                        }
                    }
                }

                if (this.uow != null
                    && isDirty
                    && record.id != null
                    )
                {
                    this.uow.registerDirty( record );
                }

                if (isDirty)
                {
                    i++;
                }
            }

            system.debug( 'total number of records registered : '+ i);
            system.debug( 'size of marketingSourceByNameMap : ' + marketingSourceByNameMap.size() );
            system.debug( 'size of marketingCampaignByTechnicalKeysMap : ' + marketingCampaignByTechnicalKeysMap.size() );
        }

        return true;
    }

    private Map<String, Marketing_Campaign__c> prepareMarketingCampaignByTechnicalKeysMap()
    {
        Set<String> parentMarketingCampaignTechnicalKeysSet = new Set<String>();

        // find campaign values only if that record does not yet have Marketing_Campaign__c lookup set
        for (Marketing_Financial_Period__c record : (List<Marketing_Financial_Period__c>)this.records)
        {
            if (record.Marketing_Campaign__c == null)
            {
                parentMarketingCampaignTechnicalKeysSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) );
            }
        }

        //system.debug( 'parentMarketingCampaignTechnicalKeysSet size : '+parentMarketingCampaignTechnicalKeysSet.size());
        //for ( String parentMarketingCampaignTechnicalKey :  parentMarketingCampaignTechnicalKeysSet)
        //{
        //    system.debug( 'parentMarketingCampaignTechnicalKey : ' + parentMarketingCampaignTechnicalKey);
        //}

        Map<String, Marketing_Campaign__c> marketingCampaignByTechnicalKeysMap = new Map<String, Marketing_Campaign__c>();

        // If there are no records that need to be updated, then don't bother with the SOQL query
        if ( ! parentMarketingCampaignTechnicalKeysSet.isEmpty() )
        {
            List<Marketing_Campaign__c> parentCampaignList = mmmarketing_CampaignsSelector.newInstance().selectByTechnicalKeys( parentMarketingCampaignTechnicalKeysSet );

            for (Marketing_Campaign__c parentCampaign : parentCampaignList)
            {
                for (String campaignTechnicalKey : mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( parentCampaign ) )
                {
                    marketingCampaignByTechnicalKeysMap.put( campaignTechnicalKey, parentCampaign );
                }
            }
        }

        return marketingCampaignByTechnicalKeysMap;
    }

    private Map<String, SObject> prepareMarketingSourceByNameMap()
    {
        Set<String> parentMarketingSourceValues = new Set<String>();

        // find source values only if that record does not yet have Marketing_Source__c lookup set
        for (Marketing_Financial_Period__c record : (List<Marketing_Financial_Period__c>)this.records)
        {
            if (record.Marketing_Source__c == null
                && String.isNotBlank( record.Source_Value__c ) )
            {
                 parentMarketingSourceValues.add( record.Source_Value__c );
            }
        }

        Map<String, SObject> outputMap = new Map<String, SObject>();

        // If there are no records that need to be updated, then don't bother with the SOQL query
        if ( ! parentMarketingSourceValues.isEmpty() )
        {
            outputMap = mmlib_Utils.generateSObjectMapByUniqueField( mmmarketing_SourcesSelector.newInstance().selectByUtm( parentMarketingSourceValues )
                                                                   , Marketing_Source__c.utm_source__c);
        }

        return outputMap;

    }
}