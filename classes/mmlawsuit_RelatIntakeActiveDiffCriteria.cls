/**
 *  Domain criteria used to determine if a Lawsuit__c record's related Intake__c record's IsMatterActive__c
 *  value is different from the Lawsuit__c's Lawsuit_Active__c field.
 */
public class mmlawsuit_RelatIntakeActiveDiffCriteria
    implements mmlib_ICriteria
{
    private list<Lawsuit__c> records = new list<Lawsuit__c>();

    public mmlib_ICriteria setRecordsToEvaluate(List<SObject> records)
    {
        this.records.clear();
        this.records.addAll( (list<Lawsuit__c>)records );

        return this;
    }

    public List<SObject> run()
    {
        list<Lawsuit__c> qualifiedRecords = new list<Lawsuit__c>();

        // find the related intakes to these lawsuits
        Map<Id, Intake__c> relatedIntakesIdMap = new Map<Id, Intake__c>( mmintake_IntakesSelector.newInstance().selectById( mmlib_Utils.generateIdSetFromField( this.records, Lawsuit__c.Intake__c) ) );

        // Loop through the Lawsuit__c records.
        for ( Lawsuit__c record : this.records )
        {
            // If there is a related intake and its IsMatterActive__c does not match Lawsuit_Active__c, then it qualifies
            if ( relatedIntakesIdMap.containsKey( record.Intake__c )
                && record.Lawsuit_Active__c != relatedIntakesIdMap.get( record.Intake__c ).IsMatterActive__c
                )
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}