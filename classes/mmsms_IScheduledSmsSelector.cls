public interface mmsms_IScheduledSmsSelector extends mmlib_ISObjectSelector
{
	List<tdc_tsw__Scheduled_Sms__c> getRecentRecords_AllRows(Integer timePeriodInMinutes);
}