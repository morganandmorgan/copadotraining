@isTest
private class mmlib_SObjectUpdateBatchTest
{

	@isTest
	private static void testBasicExecution()
	{
        String checkValue1 = 'Blue Man Group';

        SObjectType sobjType = Account.SObjectType;

        String query = 'select ' + Account.id
                             + ',' + Account.name
                             + ' from ' + sobjType;

        Map<SObjectField, Object> fieldsAndValuesMap = new Map<SObjectField, Object>();

        fieldsAndValuesMap.put( Account.Name, checkValue1 );

        system.debug( query );

        preloadDatabase();

        test.startTest();

        (new mmlib_SObjectUpdateBatch(sobjType, query, fieldsAndValuesMap))
                .setUpdateModeToAllOrNothing()
                .setBatchSizeTo(3)
                .execute();

        test.stopTest();

        list<Account> acctRecsFromDBList = [select id, name from Account];

        system.assert( ! acctRecsFromDBList.isempty() );
        system.assert( checkValue1.equalsIgnoreCase(acctRecsFromDBList[0].name) );

	}

	private static void preloadDatabase()
	{
	    Account newAcctRec = new Account( name = 'Blue Marlon' );

	    try
	    {
	        insert newAcctRec;

	        Account acctRecFromDB = [select id, name from Account where id = :newAcctRec.id];

	        system.assert( acctRecFromDB != null, 'mmlib_SObjectUpdateBatchTest failed to find data record inserted.' );
	    }
	    catch (System.DmlException dmle)
	    {
	        system.assert( false
	                     , 'mmlib_SObjectUpdateBatchTest failed to preload database ====> '
	                                + dmle.getDmlType(0)
	                                + ' --- ' + dmle.getDmlFieldNames(0));
	    }



	}
}