public class mmlib_CSVFileUtils
{
    private StaticResource csvFileAsStaticResource = null;

    private Schema.SObjectType recordSObjectType = null;

    private Schema.DescribeSObjectResult recordSObjectDescribe = null;

    private map<string, string> fieldMergesMap = new map<string, string>();

    private integer additionalNumberOfDaysToDateMerges = 0;

    // hide the constructor
    public mmlib_CSVFileUtils() { }

    public mmlib_CSVFileUtils forCSVFile( string csvFileName )
    {
        list<StaticResource> potentialResourcesList = [select Id, NamespacePrefix, Name, ContentType
                                                            , BodyLength, Body, Description, CreatedDate
                                                            , CreatedById, LastModifiedDate, LastModifiedById
                                                            , SystemModstamp, CacheControl
                                                         from StaticResource
                                                        where NamespacePrefix = null
                                                          and ContentType = 'text/csv'
                                                          and Name = :csvFileName];
        if ( potentialResourcesList.isEmpty() )
        {
            // throw an exception here
            system.debug( 'potentialResourcesList is empty');
        }

        this.csvFileAsStaticResource = potentialResourcesList[0];

        return this;
    }

    public mmlib_CSVFileUtils recordsAreSObjectType( Schema.SObjectType recordSObjectType )
    {
        this.recordSObjectType = recordSObjectType;
        this.recordSObjectDescribe = recordSObjectType.getDescribe();

        return this;
    }

    public mmlib_CSVFileUtils useFieldMerges( map<string, string> fieldMerges )
    {
        this.fieldMergesMap.putAll( fieldMerges );

        return this;
    }

    public mmlib_CSVFileUtils addAdditionalNumberOfDaysToDateMerges( integer additionalNumberOfDaysToDateMerges )
    {
        this.additionalNumberOfDaysToDateMerges = additionalNumberOfDaysToDateMerges == null ? 0 : additionalNumberOfDaysToDateMerges;

        return this;
    }

    private string processFieldMerges( string rawValue )
    {
        String mergedValue = rawValue.trim();
        //system.debug( mergedValue );

        string targetPattern = null;
        string replacement = null;

        for ( string fieldMergePattern : fieldMergesMap.keyset() )
        {
            //system.debug( 'fieldMergePattern == ' + fieldMergePattern );
            //system.debug( 'mergedValue before == ' + mergedValue );
            //system.debug( 'fieldMergesMap.get( fieldMergePattern ) now == ' + fieldMergesMap.get( fieldMergePattern ) );

            // For some unknown reason, this "replaceAll" approach does not work
            //mergedValue.replaceAll( fieldMergePattern, fieldMergesMap.get( fieldMergePattern ) );

            // ....but this approach does
            if ( mergedValue.contains(fieldMergePattern) )
            {
                mergedValue = mergedValue.substringBefore( fieldMergePattern )
                            + fieldMergesMap.get( fieldMergePattern )
                            + mergedValue.substringAfter( fieldMergePattern );
            }

            //system.debug( 'mergedValue after -- ' + mergedValue );
        }

        // process "TODAY" tokens
        if ( mergedValue.containsIgnoreCase('<<TODAY') )
        {
            string valueAfterMergeToken = mergedValue.substringAfter('>>'); // this could the a timestamp for the date field.  i.e. "T00:00:00.000Z"
            string remainingValue = mergedValue.toLowerCase().remove('<<today').substringBefore('>>');

            mergedValue = String.valueOf( date.today().addDays( ( integer.valueOf( remainingValue ) + additionalNumberOfDaysToDateMerges ) ) ) + valueAfterMergeToken;
        }

        return mergedValue;
    }

    private Object prepFieldValue( Schema.SObjectField sobjField, string rawValue )
    {
        //system.debug( sobjField );
        //system.debug( rawValue );
        return mmlib_Utils.convertValToType( sobjField.getDescribe().getSOAPType(), processFieldMerges( rawValue.remove('"') ) );
    }

    public list<SObject> consume()
    {
        list<SObject> records = new list<SObject>();

        list<String> recordLines = csvFileAsStaticResource.body.toString().split('\n');
        //system.debug( 'Found ' + recordLines.size() + ' record lines. ');

        boolean isCurrentLineTheHeaderLine = true;

        list<SObjectFieldWrapper> headerRowFields = new list<SObjectFieldWrapper>();

        SObject newRecord = null;
        SObject relatedSObject = null;
        String fieldValue = null;

        for ( String recordLine : recordLines )
        {
            if ( isCurrentLineTheHeaderLine )
            {
                // parse the header line for the SObjectFields and keep them in order.

                for ( String fieldName : recordLine.split(',') )
                {
                    headerRowFields.add( new SObjectFieldWrapper( this.recordSObjectDescribe, fieldName.remove('"') ) );
                }
                isCurrentLineTheHeaderLine = false;
            }
            else
            {
                // parse a data line
                list<string> fieldValues = recordLine.split(',');

                newRecord = recordSObjectType.newSObject();

                SObjectFieldWrapper fieldWrapper = null;

                for ( Integer i = 0; i < fieldValues.size(); i++ )
                {
                    fieldWrapper = headerRowFields[i];

                    fieldValue = fieldValues[i];

                    //system.debug( fieldValue );
                    if ( String.isBlank( fieldValue.remove('"') ) )
                    {
                        // just skip this field
                        continue;
                    }

                    // system.debug( 'fieldWrapper.field.getDescribe().isUpdateable() == ' + fieldWrapper.field.getDescribe().isUpdateable() );
                    if ( ! fieldWrapper.field.getDescribe().isUpdateable() )
                    {
                        // if it is not editable, then move on
                        continue;
                    }

                    if ( fieldWrapper.relatedToSObject == null )
                    {
                        newRecord.put( fieldWrapper.field, prepFieldValue( fieldWrapper.field, fieldValue ) );
                    }
                    else
                    {
                        relatedSObject = fieldWrapper.relatedToSObject.newSObject();

                        relatedSObject.put( fieldWrapper.relatedToSObjectField, prepFieldValue( fieldWrapper.relatedToSObjectField, fieldValue ) );
                        //system.debug( relatedSObject );

                        string relatesToSObjectFieldName = fieldWrapper.field.getDescribe().getRelationshipName().remove('__r');

                        //newRecord.putSObject( relatesToSObjectFieldName, relatedSObject );
                        //newRecord.putSObject( relatedSObject.getSobjectType(), relatedSObject );  /// does not work
                        newRecord.putSObject( fieldWrapper.field, relatedSObject );

                        //system.debug( logginglevel.error, fieldWrapper.field.getDescribe() );
                        //system.debug( logginglevel.error, fieldWrapper.field.getDescribe().getName() );
                        //system.debug( logginglevel.error, fieldWrapper.field.getDescribe().getRelationshipName() );
                        //system.debug( logginglevel.error, relatesToSObjectFieldName );
                    }
                }

                //system.debug( newRecord );

                records.add( newRecord );
            }
        }

        return records;
    }

    public class SObjectFieldWrapper
    {
        public Schema.SObjectField field;
        public Schema.SObjectType relatedToSObject;
        public Schema.SObjectField relatedToSObjectField;

        public SObjectFieldWrapper( Schema.DescribeSObjectResult recordSObjectDescribe, String fieldName )
        {
            //system.debug( 'fieldName == >>' + fieldName + '<< ');
            //Boolean isDebugOn = fieldName.trim().equalsIgnoreCase('Owner.username');
            Boolean isRelatedSObjectRequired = false;
            String recordSObjectFieldName = fieldName.toLowerCase().trim();
            //if ( isDebugOn ) system.debug( 'recordSObjectFieldName == ' + recordSObjectFieldName);
            if ( fieldName.contains('.') )
            {
                // this is a "relatedTo" field name
                recordSObjectFieldName = fieldName.substringBefore('.').replace('__r','__c').trim();
                isRelatedSObjectRequired = true;
            }

            //if ( isDebugOn ) system.debug( '>>' + recordSObjectFieldName + '<<');

            field = recordSObjectDescribe.fields.getMap().get( recordSObjectFieldName );

            //if ( isDebugOn ) system.debug( fieldName );
            //if ( isDebugOn ) system.debug( 'recordSObjectFieldName == ' + recordSObjectFieldName);
            //if ( isDebugOn ) system.debug( field );
            //if ( isDebugOn ) system.debug( isRelatedSObjectRequired );
            //if ( isDebugOn ) system.debug( recordSObjectFieldName.endsWithIgnoreCase('__c') );

            if ( field == null
                && isRelatedSObjectRequired
                && ! recordSObjectFieldName.endsWithIgnoreCase('__c')
                )
            {
                // This is probably a standard field
                recordSObjectFieldName = recordSObjectFieldName + 'Id';
                //if ( isDebugOn ) system.debug( 'recordSObjectFieldName is now == ' + recordSObjectFieldName);
                field = recordSObjectDescribe.fields.getMap().get( recordSObjectFieldName );
            }

            //if ( isDebugOn ) system.debug( 'field = ' + field );

            if ( field == null )
            {
                throw new FieldNotFoundException( recordSObjectFieldName );
            }

            if ( isRelatedSObjectRequired )
            {
                //if ( isDebugOn ) system.debug( fieldName );
                //if ( isDebugOn ) system.debug( this.field );
                List<Schema.sObjectType> referenceToList = this.field.getDescribe().getReferenceTo();
                //if ( isDebugOn ) system.debug( referenceToList );

                this.relatedToSObject = referenceToList[0]; // TODO: this approach will not work with Tasks for Matters.

                if ( Group.SObjectType == this.relatedToSObject )
                {
                    this.relatedToSObject = User.SObjectType;
                }

                if ( fieldName.equalsIgnoreCase( 'What.Name') )
                {
                    this.relatedToSObject = litify_pm__Matter__c.SObjectType;
                }

                this.relatedToSObjectField = this.relatedToSObject.getDescribe().fields.getMap().get( fieldName.substringAfter('.').trim() );
                //if ( isDebugOn ) system.debug( this.relatedToSObject );
                //if ( isDebugOn ) system.debug( this.relatedToSObjectField );
            }
            //system.debug( '\n\n\n' );
        }
    }

    public class FieldNotFoundException extends Exception { }
}