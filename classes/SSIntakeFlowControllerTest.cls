@isTest
private class SSIntakeFlowControllerTest {

    @TestSetup
    private static void setup() {
        Account client = TestUtil.createPersonAccount('Test', 'Person');
        Database.insert(client);

        Incident__c incident = new Incident__c();
        Database.insert(incident);

        Intake__c intake = new Intake__c(
                Incident__c = incident.Id,
                CLient__c = client.Id
            );
        Database.insert(intake);
    }

    private static Account getPersonAccount() {
        return [SELECT Id, PersonContactId FROM Account WHERE IsPersonAccount = true];
    }

    private static Intake__c getIntake() {
        return [SELECT Id FROM Intake__c];
    }

    private static List<Task> getTasks() {
        return [SELECT Id, WhoId, WhatId, Description, CallType FROM Task];
    }

	@isTest
    private static void finishLocation_KnownIntakeId() {
        Account personAccount = getPersonAccount();
        Intake__c intake = getIntake();

        SSIntakeFlowController controller = new SSIntakeFlowController();
        controller.IntakeId = intake.Id;
        controller.AccountID = personAccount.Id;

        Test.startTest();
        PageReference result = controller.FinishLocation;
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals('/' + intake.Id, result.getUrl());
    }

    @isTest
    private static void finishLocation_UnknownIntakeId() {
        Account personAccount = getPersonAccount();

        SSIntakeFlowController controller = new SSIntakeFlowController();
        controller.AccountID = personAccount.Id;

        Test.startTest();
        PageReference result = controller.FinishLocation;
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals('/', result.getUrl());
    }
}