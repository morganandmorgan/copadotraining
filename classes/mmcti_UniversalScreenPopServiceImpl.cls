public with sharing class mmcti_UniversalScreenPopServiceImpl
    implements mmcti_IUniversalScreenPopService
{
    // rank by profile + role, then profile + null role, then role + null profile, then null role and null profile
    public CTI_Screen_Pop_Redirect_Setting__mdt getScreenPopSetting()
    {
        mmcommon_UserInfo uinfo = new mmcommon_UserInfo(UserInfo.getUserId());
        UserRole role = uinfo.getUserRole();
        String roleName = role == null ? null : role.Name;
        String profileName = uinfo.getProfile().Name;

        Map<String, CTI_Screen_Pop_Redirect_Setting__mdt> settingsByProfileRole = new Map<String, CTI_Screen_Pop_Redirect_Setting__mdt>();
        for (CTI_Screen_Pop_Redirect_Setting__mdt sett : mmcti_CTIScreenPopRedirectSelector.newInstance().selectAll()) {
            settingsByProfileRole.put(
                (sett.Profile_Name__c == null ? '' : sett.Profile_Name__c) +
                '_' +
                (sett.Role_Name__c == null ? '' : sett.Role_Name__c),
                sett
            );
        }
        if (settingsByProfileRole.containsKey(profileName + '_' + roleName)) return settingsByProfileRole.get(profileName + '_' + roleName);
        if (settingsByProfileRole.containsKey(profileName + '_')) return settingsByProfileRole.get(profileName + '_');
        if (settingsByProfileRole.containsKey('_' + roleName)) return settingsByProfileRole.get('_' + roleName);
        return settingsByProfileRole.get('_');
    }
}