@isTest
private class renderDetailControllerTest {
	
	@isTest
    private static void getPrintableView() {
        Account a = TestUtil.createPersonAccount('Test', 'Person');
        Database.insert(a);

        ApexPages.currentPage().getParameters().put('id', a.Id);
        renderDetailController controller = new renderDetailController();

        controller.contentString = '<html><head></head><h1>Test Title</h1></html>';

        Test.startTest();
        String result = controller.getPrintableView();
        Test.stopTest();

        System.assertEquals(SObjectType.Account.getLabel(), controller.sObjectName);
        System.assertEquals(controller.sObjectName + ' - Test Title', result.substringAfter('<h1>').substringBefore('</h1>'));
    }
}