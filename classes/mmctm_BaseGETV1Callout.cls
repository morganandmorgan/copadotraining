/**
 *  mmctm_BaseGETV1Callout
 */
public abstract class mmctm_BaseGETV1Callout
    extends mmlib_BaseGETCallout
{
    public mmctm_BaseGETV1Callout()
    {
        super();

        getHeaderMap().put('Content-Type','application/json');
    }

    protected override string getHost()
    {
        return 'api.calltrackingmetrics.com';
    }

    protected override string getPath()
    {
        return '/api/v1' + (string.isNotBlank( getPathSuffix() ) ? getPathSuffix() : '');
    }

    protected abstract string getPathSuffix();

    protected override HttpResponse executeCallout()
    {
        HttpResponse resp = null;

        CallTrackingMetricsIntegrationRelated__c customSetting = CallTrackingMetricsIntegrationRelated__c.getInstance();

        if ( customSetting != null
            && customSetting.IsCTMIntegrationEnabled__c == true
            && ( ! mmlib_Utils.org.isSandbox
                || (mmlib_Utils.org.isSandbox && customSetting.IsIntegrationsEnabledInSandboxEnv__c == true)
                )
            )
        {
            resp = super.executeCallout();
        }
        else
        {
            resp = new HttpResponse();

            resp.setBody('{}');
            resp.setStatus('Callouts to CallTrackingMetrics are currently disabled.');
            resp.setStatusCode(203);

            system.debug( resp.getStatus() );
        }

        return resp;
    }
}