public with sharing class LaunchQuestionnaireRedirectExt
{
    private Intake__c intake;

    private static set<String> caseTypesForSkuidBaseMTQuestionnaireSet;

    static
    {
        caseTypesForSkuidBaseMTQuestionnaireSet = new set<String>();

        caseTypesForSkuidBaseMTQuestionnaireSet.add('Xarelto');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Pradaxa');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Eliquis');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Essure');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Bair Hugger');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Risperdal');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Invega');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Abilify');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Talcum Powder');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Invokana');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Hernia Mesh');
        caseTypesForSkuidBaseMTQuestionnaireSet.add('Breast Implants');
    }

    public Boolean isIframeDisplayed { get; private set; } { isIframeDisplayed = true; }
    public String iframeUrl
    {
        get
        {
            String iframeUrl = '/apex/skuid__ui';

            if ( intake.Litigation__c.equalsIgnoreCase('Mass Tort')
                && caseTypesForSkuidBaseMTQuestionnaireSet.contains( intake.Case_Type__c ) )
            {
                iframeUrl += '?page=MtQuestionnaire';
            }
            else
            {
                iframeUrl += '?page=CccQuestionnaire';
            }

            iframeUrl += '&intakeid=' + intake.Id;

            List<MM_Questionnaire__c> questionnaires = [SELECT Id
                                                          FROM MM_Questionnaire__c
                                                         WHERE Intake__c = :Intake.Id];

            if ( ! questionnaires.isEmpty())
            {
                iframeUrl += '&questionnaireId=' + questionnaires.get(0).Id;
            }

            return iframeUrl;
        }
    }

    private PageReference nextPage = null;

    public LaunchQuestionnaireRedirectExt(ApexPages.StandardController controller)
    {
        intake = (Intake__c)controller.getRecord();
        system.debug( intake );

        if ( intake.Litigation__c.equalsIgnoreCase('Mass Tort')
            && ! caseTypesForSkuidBaseMTQuestionnaireSet.contains( intake.Case_Type__c ) )
        {
            isIframeDisplayed = false;
        }
        else
        {
            isIframeDisplayed = true;
        }
    }

    public PageReference determinePageToDisplay()
    {
        if ( intake.Litigation__c.equalsIgnoreCase('Mass Tort')
            && ! caseTypesForSkuidBaseMTQuestionnaireSet.contains( intake.Case_Type__c ) )
        {
            String result = '';
            try
            {
                result = QuestionnaireCreationClass.questionnaireCreation( intake.id );

                // verify the string is actually an id
                id.valueOf( result );

                nextPage = Page.QuestionnaireForm;

                nextPage.getParameters().put('id', result);

                nextPage.setRedirect( true );

            }
            catch ( System.StringException se )
            {
                // if is not an id.  It is an error message
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, result.substringAfter( QuestionnaireCreationClass.MESSAGE_DELIMITER ) ) );
            }
            catch ( Exception e )
            {
                system.debug(e);
                isIframeDisplayed = false;
            }
        }

        return nextPage;
    }

}