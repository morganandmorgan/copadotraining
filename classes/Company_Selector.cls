/*============================================================================/
* Company_Selector
* @description Selector for FFA Companies
* @author Brian Krynitsky
* @date Apr 2019
=============================================================================*/
public class Company_Selector extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            c2g__codaCompany__c.Id,
            c2g__codaCompany__c.OwnerId,
            c2g__codaCompany__c.Name,
            c2g__codaCompany__c.c2g__IntercompanyAccount__c,
            c2g__codaCompany__c.Cost_Bank_Account__c,
            c2g__codaCompany__c.Operating_Cash_Bank_Account__c,
            c2g__codaCompany__c.Trust_Bank_Account__c,
            c2g__codaCompany__c.Intercompany_GLA__c,
            c2g__codaCompany__c.Operational_Queue_Id__c,
            c2g__codaCompany__c.c2g__CustomerSettlementDiscount__c
        };
    }

    // Constructor
    public Company_Selector() {
    }

    //required implemenation from fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return c2g__codaCompany__c.SObjectType;
    }

    // Methods
    public List<c2g__codaCompany__c> selectByAccountId(Set<Id> ids) {
        fflib_QueryFactory query = newQueryFactory();

        //where conditions:
        query.setCondition('c2g__IntercompanyAccount__c IN :ids AND c2g__IntercompanyAccount__c != null');

        return (List<c2g__codaCompany__c>) Database.query(query.toSOQL());
    }

    public List<c2g__codaCompany__c> selectById(Set<Id> ids) {
        fflib_QueryFactory query = newQueryFactory();

        //where conditions:
        query.setCondition('Id IN :ids');

        return (List<c2g__codaCompany__c>) Database.query(query.toSOQL());
    }
}