/**
 *  mmmarketing_TI_AutoLinkMFPBatch
 */
public class mmmarketing_TI_AutoLinkMFPBatch
    implements Database.Batchable<SObject>, mmlib_ISchedule
{
    private integer batchSize = 200;

    public mmmarketing_TI_AutoLinkMFPBatch () { }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return mmmarketing_TrackingInfosSelector.newInstance().selectQueryLocatorWhereMarketingFinancialPeriodNotLinkedAndEventRecent();
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance(new mmlib_AtomicDML());

            mmmarketing_TrackingInformations.newInstance( (list<Marketing_Tracking_Info__c>)scope ).autoLinkToMarketingInformationIfRequired( uow );

            uow.commitWork();
        }
        catch (Exception e)
        {
            System.debug(Logginglevel.ERROR, 'Error executing batch ' + e);
        }
    }

    public void finish(Database.BatchableContext context)
    {

    }

    public mmmarketing_TI_AutoLinkMFPBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public void execute()
    {
        Database.executeBatch( this, batchSize );
    }

    /**
     *  method implemented because mmlib_ISchedule
     *
     *  Allows for the scheduleable to be active but not maintain a lock on this and subsequent classes
     */
    public void execute(SchedulableContext sc)
    {
        integer theBatchSize = this.batchSize;

        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticFPLinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.TI_AutoLinkMFPBatch_Scope_Size__c != null )
        {
            theBatchSize = Integer.valueOf( customSetting.TI_AutoLinkMFPBatch_Scope_Size__c );
        }

        this.setBatchSizeTo( theBatchSize ).execute();
    }
}