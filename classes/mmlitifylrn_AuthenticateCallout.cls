/**
 *  @usage mmlitifylrn_AuthenticateCallout.Response resp = (mmlitifylrn_AuthenticateCallout.Response) new mmlitifylrn_AuthenticateCallout()
 *                                   .setUsername( lrnUsername )
 *                                   .setPassword( lrnPassword )
 *                                   .debug()
 *                                   .execute()
 *                                   .getResponse();
 *
 *  @usage system.debug( resp.getAccessToken() );
 *  @usage system.debug( resp.getRefreshToken() );
 */
public class mmlitifylrn_AuthenticateCallout
    extends mmlitifylrn_BasePOSTV1Callout
{
    private mmlitifylrn_AuthenticateCallout.Response resp = new mmlitifylrn_AuthenticateCallout.Response();
    private string username = null;
    private string password = null;

    public override map<string, string> getParamMap()
    {
        return new map<string, string>();
    }

    public override string getPathSuffix()
    {
        return '/authenticate';
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        mmlitifylrn_AuthenticateCallout.RequestBody requestBody = new mmlitifylrn_AuthenticateCallout.RequestBody();

        requestBody.email = this.username;
        requestBody.password = this.password;

        addPostRequestBody( requestBody );

        httpResponse httpResp = super.executeCallout();

        if ( this.isDebugOn )
        {
            system.debug( httpResp.getStatusCode() );
            system.debug( httpResp.getStatus() );
        }

        if ( httpResp.getStatusCode() == 200 )
        {
            resp = (mmlitifylrn_AuthenticateCallout.Response) json.deserialize( httpResp.getBody(), mmlitifylrn_AuthenticateCallout.Response.class );

            if ( this.isDebugOn )
            {
                resp.setDebugOn();
            }

            resp.setHTTPResponse( httpResp );

        }
        else
        {
            throw new mmlitifylrn_Exceptions.CalloutException( httpResp );
        }


        return this;
    }

    public class RequestBody
    {
        public string email;
        public string password;
    }

    public class Response
        implements CalloutResponse
    {
        private httpResponse httpResp = null;
        private map<string, string> httpResponseHeaderMap = new map<string, string>();
        private boolean isDebugOn = false;

        private void setHTTPResponse( httpResponse httpResp )
        {
            if ( this.isDebugOn == null )
            {
                this.isDebugOn = false;
            }

            if ( httpResp != null
                && ! httpResp.getHeaderKeys().isEmpty()
                )
            {
                this.httpResp = httpResp;

                if ( httpResponseHeaderMap == null )
                {
                    httpResponseHeaderMap  = new map<string, string>();
                }

                system.debug('response headers ____________________________________________________ ');

                for ( String headerKey : httpResp.getHeaderKeys() )
                {
                    if ( this.isDebugOn )
                    {
                        system.debug( 'headerkey = ' + headerKey);
                        system.debug( httpResp.getHeader( headerKey ) );
                    }

                    httpResponseHeaderMap.put( headerKey, httpResp.getHeader( headerKey ) );
                }
            }
        }

        private void setDebugOn()
        {
            this.isDebugOn = true;
        }

        public string getAccessToken()
        {
            return this.httpResponseHeaderMap != null && this.httpResponseHeaderMap.containsKey( mmlitifylrn_Constants.LRN_API_ACCESS_TOKEN_KEY ) ? httpResponseHeaderMap.get( mmlitifylrn_Constants.LRN_API_ACCESS_TOKEN_KEY ) : null;
        }

        public string getRefreshToken()
        {
            return this.httpResponseHeaderMap != null && httpResponseHeaderMap.containsKey( mmlitifylrn_Constants.LRN_API_REFRESH_TOKEN_KEY ) ? httpResponseHeaderMap.get( mmlitifylrn_Constants.LRN_API_REFRESH_TOKEN_KEY ) : null;
        }

        public integer getTotalNumberOfRecordsFound()
        {
            return -1;
        }
    }

    private void validateCallout()
    {
        if ( string.isBlank( this.username ) )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Litify Referral Network username has not been specified.  Please use the setUsername(string) method before calling the execute() method.');
        }

        if ( string.isBlank( this.password ) )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Litify Referral Network password has not been specified.  Please use the setPassword(string) method before calling the execute() method.');
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public mmlitifylrn_AuthenticateCallout setUsername( string username )
    {
        this.username = username;

        return this;
    }

    public mmlitifylrn_AuthenticateCallout setPassword( string password )
    {
        this.password = password;

        return this;
    }
}