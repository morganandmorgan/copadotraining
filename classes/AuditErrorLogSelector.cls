/**
 * AuditErrorLogSelector
 * @description Selector for Audit Error Log SObject.
 * @author Matt Terrill
 * @date 12/06/2019
 */
public without sharing class AuditErrorLogSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public AuditErrorLogSelector() {
        sObjectFields = new List<Schema.SObjectField> {
                Audit_Error_Log__c.Id,
                Audit_Error_Log__c.Message__c,
                Audit_Error_Log__c.Stack_Trace__c,
                Audit_Error_Log__c.Type__c,
                Audit_Error_Log__c.Notification_Sent__c
        };
    } //constructor


    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Audit_Error_Log__c.SObjectType;
    } //getSObjectType


    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields


    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList


    public List<Audit_Error_Log__c> selectById(Set<Id> ids) {
        return selectSObjectsById(ids);
    } //selectById


    public List<Audit_Error_Log__c> selectRecent(DateTime since) {
        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('createdDate >= :since');

        return Database.query(query.toSOQL());
    } //selectByActiveForObject

} //class