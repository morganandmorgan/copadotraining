/**
 * ExpenseSummaryDomainTest
 * @description Test for Expense Summary Domain class.
 * @author Jeff Watson
 * @date 1/18/2019
 */
@isTest
public with sharing class ExpenseSummaryDomainTest {

    @isTest
    public static void mainTest() {

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;
        
        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;

        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.name = 'Postage';
        expenseType.costType__c = 'HardCost';
        expenseType.externalID__c = 'Post';
        insert expenseType;

        litify_pm__Expense__c expense = new litify_pm__Expense__c();
        expense.litify_pm__Matter__c = matter.Id;
        expense.litify_pm__Amount__c = 1.00;
        expense.litify_pm__ExpenseType2__c = expenseType.id;
        expense.litify_pm__Date__c = Date.today();
        insert expense;

        expense = new litify_pm__Expense__c();
        expense.litify_pm__Matter__c = matter.Id;
        expense.litify_pm__Amount__c = 2.00;
        expense.litify_pm__ExpenseType2__c = expenseType.id;
        expense.litify_pm__Date__c = Date.today();
        insert expense;

        ExpenseSummaryDomain domain = new ExpenseSummaryDomain(new List<Expense_Summary__c>());

        ExpenseSummaryDomain.recalculateExpenseSummariesForMatter(matter.id);

        ExpenseSummaryDomain.Constructor constructionZone = new ExpenseSummaryDomain.Constructor();
        domain = (ExpenseSummaryDomain)constructionZone.construct(new List<Expense_Summary__c>());

        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {litify_pm__Expense__c.getSObjectType(), Expense_Summary__c.getSObjectType()});
        //ExpenseSummaryDomain.createExpenseSummaries(new Map<Id,Set<Id>>(), uow);
    } //mainTest

} //class