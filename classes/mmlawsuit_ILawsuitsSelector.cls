public interface mmlawsuit_ILawsuitsSelector
    extends mmlib_ISObjectSelector
{
    List<Lawsuit__c> selectById(Set<Id> idSet);
    List<Lawsuit__c> selectByIntake(Set<Id> idSet);
    List<Lawsuit__c> selectByMatter(Set<Id> idSet);
    List<Lawsuit__c> selectWithFieldsetByIntake(Schema.FieldSet fs, Set<Id> idSet);
    List<AggregateResult> selectAggregatesForLawsuitValidation(Set<Id> intakeIdSet);
}