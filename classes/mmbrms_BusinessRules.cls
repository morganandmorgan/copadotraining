public with sharing class mmbrms_BusinessRules
	implements mmbrms_IBusinessRules
{
	private String developerName = '';
	private String description = '';
	private String ruleDefinition = '';

	public mmbrms_BusinessRules(BusinessRule__mdt bizRule)
	{
		if (bizRule != null)
		{
			developerName = bizRule.DeveloperName;
			description = bizRule.Description__c;
			ruleDefinition = bizRule.RuleDefinition__c;
		}
	}

	public String getDeveloperName()
	{
		return developerName;
	}

	public String getRuleDefinition()
	{
		return ruleDefinition;
	}
}