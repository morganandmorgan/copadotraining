@isTest
private class IRN_QueueListControllerTest {
  static IntakeRepNavigatorWidgetQueue__c testQueue;
  static {
    testQueue = new IntakeRepNavigatorWidgetQueue__c(Name='Test Queue');
    insert testQueue;

    PageReference PageRef = Page.IntakeRepNavigatorQueueList;
    Test.setCurrentPage(PageRef);
  }

	@isTest static void SelectsNameAndIdFromQueues() {
		IntakeRepNavigator_QueueListController controller = new IntakeRepNavigator_QueueListController();
    System.assertEquals(1, controller.Queues.size());
    System.assertEquals(testQueue.id, controller.Queues[0].Id);
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}