public class mmlitifylrn_LRNModels
{
    // hide the default constructor
    private mmlitifylrn_LRNModels() { }

    public virtual class ReferralBasicData
    {
        public string klass = 'Referral';
        public string id;                       // populated by LRN in referral API call response -- data will present as an integer

        public string amount_cents;
        public string anonymous;
        public string assigned_to_organization_at;
        public string assigned_to_user_at;
        public string attorney_interested_at;
        public string case_closed_at;
        public string case_lost_reason;
        public string case_status;
        public string case_turn_down_reason;
        public string created_at;
        public string expires_at;
        public string handling_organization_id;
        public string reference_number;         // populated by LRN in referral API call response??
        public string suggested_referral_agreement_id;

        public OrganizationBasicData handling_organization;

        public UserBasicData handling_user;

        public ReferralIntakeBasicData referral_intake;

        public FirmToFirmRelationshipReferralAgreement suggested_referral_agreement;

        public TermsOfAgreement terms_of_agreement;

        public ReferralBasicData()
        {

        }

        public ReferralBasicData( litify_pm__Referral__c referralRecord, litify_pm__Firm__c handlingFirmRecord, FirmToFirmRelationshipReferralAgreement suggested_referral_agreement)
        {
            if ( referralRecord != null )
            {
                this.referral_intake = new ReferralIntakeBasicData( referralRecord );
            }

            if ( handlingFirmRecord != null )
            {
                this.handling_organization = new OrganizationBasicData( handlingFirmRecord );
                this.handling_organization_id = string.valueOf( handlingFirmRecord.litify_pm__ExternalId__c );
            }

            this.suggested_referral_agreement = suggested_referral_agreement;
            this.suggested_referral_agreement_id = suggested_referral_agreement.id;

        }

        public ReferralBasicData( litify_pm__Referral_Transaction__c referralTransactionRecord )
        {
            if ( referralTransactionRecord != null )
            {
                this.id = referralTransactionRecord.litify_pm__ExternalId__c == null ? null : string.valueOf( referralTransactionRecord.litify_pm__ExternalId__c );
                this.amount_cents = referralTransactionRecord.litify_pm__Amount__c == null ? null : string.valueOf( referralTransactionRecord.litify_pm__Amount__c * 100 ); // multiply the amount by 100 to get the "cents"
//                this.anonymous = referralTransactionRecord.litify_pm__Is_Anonymous__c;
                this.case_lost_reason = referralTransactionRecord.litify_pm__Case_lost_reason__c;
                this.case_status = referralTransactionRecord.litify_pm__Status__c;
                this.case_turn_down_reason = referralTransactionRecord.litify_pm__Case_turn_down_reason__c;
// TODO: Figure out best approach to convert strings to DateTime values
//                this.assigned_to_organization_at = referralTransactionRecord.litify_pm__Assigned_to_firm_date__c;
//                this.assigned_to_user_at = referralTransactionRecord.litify_pm__Assigned_to_user_date__c;
//                this.case_closed_at = referralTransactionRecord.litify_pm__Case_closed_at__c;
//                this.expires_at = referralTransactionRecord.litify_pm__Expires_at__c;

                if ( referralTransactionRecord.litify_pm__Referral__r != null )
                {
                    this.referral_intake = new ReferralIntakeBasicData( referralTransactionRecord.litify_pm__Referral__r );
                }

                if ( referralTransactionRecord.litify_pm__Handling_Firm__r != null )
                {
                    this.handling_organization = new OrganizationBasicData( referralTransactionRecord.litify_pm__Handling_Firm__r );
                }
            }
        }

        public ReferralBasicData addHandlingFirm( litify_pm__Firm__c handlingFirmRecord )
        {
            if ( handlingFirmRecord != null )
            {
                this.handling_organization = new OrganizationBasicData( handlingFirmRecord );
                this.handling_organization_id = string.valueOf( handlingFirmRecord.litify_pm__ExternalId__c );
            }

            return this;
        }

        // This method is used when hydrating a referral record, but the handling firm doesn't
        // actually exist on the ReferralIntakeBasicData object.  We have to use that object's active_referral
        // property to get access to the ReferralBasicData class to determine the handling firm.
        public void addToSObject(litify_pm__Referral__c referralRecord)
        {
            if (referralRecord != null && referralRecord.litify_pm__Handling_Firm__c == null)
            {
                referralRecord.litify_pm__Handling_Firm__r = (String.isNotBlank(this.handling_organization_id)) ? new litify_pm__Firm__c(litify_pm__ExternalId__c = Decimal.valueOf(this.handling_organization_id)) : null;
            }
        }

        public void addToSObject(litify_pm__Referral_Transaction__c referralTransaction)
        {
            referralTransaction.litify_pm__Amount__c = String.isNotEmpty(this.amount_cents) ? ( Decimal.valueOf( this.amount_cents ) / 100 ) : null; // divide the amount_cents by 100 to get the "dollars and cents"
            referralTransaction.litify_pm__Case_closed_at__c = String.isNotEmpty(this.case_closed_at) ? Date.valueOf(this.case_closed_at.replace('T',' ')) : null;
            referralTransaction.litify_pm__Case_lost_reason__c = String.isNotEmpty(this.case_lost_reason) ? this.case_lost_reason : null;
            referralTransaction.litify_pm__Case_turn_down_reason__c = String.isNotEmpty(this.case_turn_down_reason) ? this.case_turn_down_reason : null;
            referralTransaction.litify_pm__Expires_at__c = String.isNotEmpty(this.expires_at) ? Date.valueOf(this.expires_at.replace('T',' ')) : null;
            referralTransaction.litify_pm__ExternalId__c = String.isNotEmpty(this.id) ? Decimal.valueOf(this.id) : null;
            referralTransaction.litify_pm__Assigned_to_user_date__c = String.isNotEmpty(this.assigned_to_user_at) ? Datetime.valueOf(this.assigned_to_user_at.replace('T',' ')) : null;
            referralTransaction.litify_pm__Assigned_to_firm_date__c = String.isNotEmpty(this.assigned_to_organization_at) ? Datetime.valueOf(this.assigned_to_organization_at.replace('T',' ')) : null;
            referralTransaction.litify_pm__Status__c = String.isNotEmpty(this.case_status) ? this.case_status : null;

            try
            {
                if (referralTransaction.litify_pm__Handling_Firm__c == null)
                {
                    referralTransaction.litify_pm__Handling_Firm__r = (this.handling_organization != null && String.isNotBlank(this.handling_organization.id)) ? new litify_pm__Firm__c(litify_pm__ExternalId__c = Decimal.valueOf(this.handling_organization.id)) : null;
                }
            }
            catch (Exception ex)
            {
                System.debug('DEBUG:::Exception when trying to set referralTransaction.litify_pm__Handling_Firm__r');
                System.debug(ex.getMessage());
            }
        }

        public litify_pm__Referral_Transaction__c getTransactionSObject()
        {
            litify_pm__Referral_Transaction__c referralTransaction = new litify_pm__Referral_Transaction__c();

            this.addToSObject( referralTransaction );

            return referralTransaction;
        }
    }

    public virtual class ReferralIntakeBasicData
    {
        public string klass = 'ReferralIntake';
        public string id;                       // populated by LRN in referral API call response -- data will present as an integer
        public string case_type_id;             // litify_pm__Referral__c.mmlitifylrn_CaseTypeExternalId__c
        public string client_email;             // litify_pm__Referral__c.litify_pm__Client_email__c
        public string client_first_name;        // litify_pm__Referral__c.litify_pm__Client_First_Name__c
        public string client_last_name;         // litify_pm__Referral__c.litify_pm__Client_Last_Name__c
        public string client_phone;             // litify_pm__Referral__c.litify_pm__Client_phone__c
        public string created_at;               // litify_pm__Referral__c.litify_pm__LRN_Created_At__c
        public string descriptions;             // litify_pm__Referral__c.litify_pm__Description__c
        public string external_ref_number;      // litify_pm__Referral__c.litify_pm__External_Ref_Number__c
        public string extra_info;               // litify_pm__Referral__c.litify_pm__Extra_info__c
        public string incident_date;            // litify_pm__Referral__c.litify_pm__Incident_date__c
        public string last_active_referral_status;
        public string last_handling_organization_name;
        public string last_handling_user_full_name;
        public string reference_number;         // populated by LRN in referral API call response??
        public string referral_intake_status;   // litify_pm__Referral__c.litify_pm__Status__c
        public string referral_percentage_fee;  // litify_pm__Referral__c.litify_pm__Suggested_Percentage_fee__c  -- data will present as a float
        public string updated_at;               // litify_pm__Referral__c.litify_pm__Sync_last_updated_at__c

        public OrganizationWithLocation originating_organization;   // litify_pm__Referral__c.litify_pm__Originating_Firm__c

        public UserBasicData originating_user;  // populated by LRN in referral API call response

        public ClientLocation client_location;

        public CaseLocation case_location;

        public CaseType case_type;

        public list<StatusUpdate> status_updates;

        public ReferralBasicData active_referral;

        public list<ReferralBasicData> referrals;

        public ReferralIntakeBasicData()
        {

        }

        public Boolean handlingFirmHasChanged()
        {
            if (this.active_referral != null && this.active_referral.handling_organization != null)
            {
                if (this.last_handling_organization_name != this.active_referral.handling_organization.name)
                {
                    return true;
                }
            }

            return false;
        }

        public ReferralIntakeBasicData( litify_pm__Referral__c referralRecord )
        {
            this.id = referralRecord.litify_pm__ExternalId__c == null ? null : string.valueOf( referralRecord.litify_pm__ExternalId__c );

            this.client_first_name = referralRecord.litify_pm__Client_First_Name__c;
            this.client_last_name = referralRecord.litify_pm__Client_Last_Name__c;
            this.client_email = referralRecord.litify_pm__Client_email__c;
            this.client_phone = referralRecord.litify_pm__Client_phone__c;
            this.descriptions = referralRecord.litify_pm__Description__c;
            this.extra_info = referralRecord.litify_pm__Extra_info__c;
            this.external_ref_number = referralRecord.litify_pm__External_Ref_Number__c;
            // TODO: figure out any date/time conversion
            this.created_at = string.valueOf( referralRecord.litify_pm__LRN_Created_At__c );
            this.updated_at = string.valueOf( referralRecord.litify_pm__Sync_last_updated_at__c );
            this.incident_date = string.valueOf( referralRecord.litify_pm__Incident_date__c );
            this.referral_intake_status = referralRecord.litify_pm__Status__c;
            this.referral_percentage_fee = string.valueOf( referralRecord.litify_pm__Suggested_Percentage_fee__c );
            this.case_type_id = string.valueOf( referralRecord.mmlitifylrn_CaseTypeExternalId__c );
            this.client_location = new ClientLocation( referralRecord );
            this.case_location = new CaseLocation( referralRecord );

            if ( referralRecord.litify_pm__Case_Type__r != null )
            {
                this.case_type = new CaseType( referralRecord.litify_pm__Case_Type__r );
            }

            if ( referralRecord.litify_pm__Originating_Firm__r != null )
            {
                this.originating_organization = new OrganizationWithLocation( referralRecord.litify_pm__Originating_Firm__r );
            }
        }

        public ReferralBasicData getReferralTransactionByLRNExternalID( String externalId )
        {
            for (ReferralBasicData referralTrans : this.referrals )
            {
                if ( referralTrans != null && referralTrans.id != null && referralTrans.id.equalsIgnoreCase(externalId))
                {
                    return referralTrans;
                }
            }

            return null;
        }

        public void addToSObject(litify_pm__Referral__c referralRecord)
        {
            referralRecord.litify_pm__Client_First_Name__c = this.client_first_name;
            referralRecord.litify_pm__Client_Last_Name__c = this.client_last_name;
            referralRecord.litify_pm__Client_phone__c = this.client_phone;
            referralRecord.litify_pm__Client_email__c = this.client_email;
            referralRecord.litify_pm__Description__c = this.descriptions;
            referralRecord.litify_pm__External_Ref_Number__c = this.external_ref_number;
            referralRecord.litify_pm__Extra_info__c = this.extra_info;
            referralRecord.litify_pm__Incident_date__c = (this.incident_date != null) ? Date.valueOf(this.incident_date) : null;
            referralRecord.litify_pm__Status__c = this.referral_intake_status;
            referralRecord.litify_pm__Suggested_Percentage_fee__c = (this.referral_percentage_fee != null) ? Decimal.valueOf(this.referral_percentage_fee) : 0.0;

            // Related Objects
            if(this.active_referral != null && this.active_referral.handling_organization != null)
            {
                this.active_referral.addToSObject(referralRecord);
            }

            // Case Address Mapping
            if(this.case_location != null)
            {
                this.case_location.addToSObject(referralRecord);
            }

            // Client Address Mapping
            if(this.client_location != null)
            {
                this.client_location.addToSObject(referralRecord);
            }
        }
    }

    public class CaseLocation
    {
        public string address1;                 // litify_pm__Referral__c.litify_pm__Case_Address_1__c
        public string address2;                 // litify_pm__Referral__c.litify_pm__Case_Address_2__c
        public string city;                     // litify_pm__Referral__c.litify_pm__Case_City__c
        public string state;                    // litify_pm__Referral__c.litify_pm__Case_State__c
        public string postal_code;              // litify_pm__Referral__c.litify_pm__Case_Postal_Code__c

        public CaseLocation()
        {

        }

        public CaseLocation( litify_pm__Referral__c referralRecord )
        {
            this.address1 = referralRecord.litify_pm__Case_Address_1__c;
            this.address2 = referralRecord.litify_pm__Case_Address_2__c;
            this.city = referralRecord.litify_pm__Case_City__c;
            this.state = referralRecord.litify_pm__Case_State__c;
            this.postal_code = referralRecord.litify_pm__Case_Postal_Code__c;
        }

        public void addToSObject( litify_pm__Referral__c referralRecord )
        {
            if ( referralRecord != null )
            {
                referralRecord.litify_pm__Case_Address_1__c = this.address1;
                referralRecord.litify_pm__Case_Address_2__c = this.address2;
                referralRecord.litify_pm__Case_City__c = this.city;
                referralRecord.litify_pm__Case_State__c = this.state;
                referralRecord.litify_pm__Case_Postal_Code__c = this.postal_code;
            }
        }
    }

    public class CaseType
    {
        public string klass;
        public string id;
        public string name;

        public CaseType()
        {

        }

        public CaseType( litify_pm__Case_Type__c caseTypeRecord )
        {
            this.klass = 'CaseType';
            this.id = caseTypeRecord.litify_pm__ExternalId__c == null ? null : string.valueOf( caseTypeRecord.litify_pm__ExternalId__c );
            this.name = caseTypeRecord.Name;
        }

        public void addToSObject(litify_pm__Referral__c referralRecord)
        {
            if (referralRecord != null)
            {
                referralRecord.litify_pm__Case_Type__c = null;
                referralRecord.litify_pm__Case_Type__r = new litify_pm__Case_Type__c( litify_pm__ExternalId__c = Decimal.valueOf(this.id) );
            }
        }
    }

    public class ClientLocation
    {
        public string address1;                 // litify_pm__Referral__c.litify_pm__Client_Address_1__c
        public string address2;                 // litify_pm__Referral__c.litify_pm__Client_Address_2__c
        public string city;                     // litify_pm__Referral__c.litify_pm__Client_City__c
        public string state;                    // litify_pm__Referral__c.litify_pm__Client_State__c
        public string postal_code;              // litify_pm__Referral__c.litify_pm__Client_Postal_Code__c

        public ClientLocation()
        {

        }

        public ClientLocation( litify_pm__Referral__c referralRecord )
        {
            this.address1 = referralRecord.litify_pm__Client_Address_1__c;
            this.address2 = referralRecord.litify_pm__Client_Address_2__c;
            this.city = referralRecord.litify_pm__Client_City__c;
            this.state = referralRecord.litify_pm__Client_State__c;
            this.postal_code = referralRecord.litify_pm__Client_Postal_Code__c;
        }

        public void addToSObject( litify_pm__Referral__c referralRecord )
        {
            if ( referralRecord != null )
            {
                referralRecord.litify_pm__Client_Address_1__c = this.address1;
                referralRecord.litify_pm__Client_Address_2__c = this.address2;
                referralRecord.litify_pm__Client_City__c = this.city;
                referralRecord.litify_pm__Client_State__c = this.state;
                referralRecord.litify_pm__Client_Postal_Code__c = this.postal_code;
            }
        }
    }

    public virtual class FirmToFirmRelationshipReferralAgreement
    {
        public string klass;
        public string id;
        public string agreement_type;
        public string agreement_name;
        public string referral_percentage_fee;
        public string signed;

        public UserBasicData originating_firm_proposing_user;
        public UserBasicData handling_firm_signing_user;
    }

    public virtual class OrganizationBasicData
    {
        public string klass;
        public string id;
        public string name;
        public string phone;
        public string website_url;
        public string shadow;
        public string firm_size;
        public string deactivated_at;
        public string logo_thumb;
        public string logo_small;
        public string logo_medium;
        public string logo_large;

        public OrganizationBasicData()
        {

        }

        public OrganizationBasicData( litify_pm__Firm__c firmRecord )
        {
            this.klass = 'Organization';
            this.id = firmRecord.litify_pm__ExternalId__c == null ? null : string.valueOf( firmRecord.litify_pm__ExternalId__c );
            this.name = firmRecord.Name;
            this.phone = firmRecord.litify_pm__Primary_Contact_Phone__c;
        }

        public virtual litify_pm__Firm__c getSObject()
        {
            litify_pm__Firm__c firmRecord = new litify_pm__Firm__c();

            try
            {
                firmRecord.litify_pm__ExternalId__c = this.id == null ? null : decimal.valueOf( this.id );
            }
            catch ( System.TypeException te )
            {
                // no opt
            }

            firmRecord.Name = this.name;
            firmRecord.litify_pm__Primary_Contact_Phone__c = this.phone;

            return firmRecord;
        }


    }

    public class OrganizationWithLocation
            extends OrganizationBasicData
    {
        public OrganizationLocation location;
        public OrganizationLocation primary_location;
        public List<OrganizationLocation> locations;

        public OrganizationWithLocation()
        {

        }

        public OrganizationWithLocation( litify_pm__Firm__c firmRecord )
        {
            super( firmRecord );

            this.location = new OrganizationLocation( firmRecord );
            this.primary_location = new OrganizationLocation( firmRecord );
            this.locations = new List<OrganizationLocation>();
            this.locations.add( this.location );
        }

        public override litify_pm__Firm__c getSObject()
        {
            litify_pm__Firm__c firmRecord = super.getSObject();

            if ( this.location != null )
            {
                this.location.addToSObject( firmRecord );
            }

            return firmRecord;
        }
    }

    public class OrganizationLocation
    {
        public string klass;
        public string id;
        public string address1;
        public string address2;
        public string postal_code;
        public string city;
        public string state;

        public OrganizationLocation()
        {

        }

        public OrganizationLocation( litify_pm__Firm__c firmRecord )
        {
            this.klass = 'Location';
            this.id = firmRecord.litify_pm__ExternalId__c == null ? null : string.valueOf( firmRecord.litify_pm__ExternalId__c );
            this.city = firmRecord.litify_pm__City__c;
            this.state = firmRecord.litify_pm__State__c;
        }

        public void addToSObject( litify_pm__Firm__c firmRecord )
        {
            if ( firmRecord != null )
            {
                firmRecord.litify_pm__City__c = this.city;
                firmRecord.litify_pm__State__c = this.state;
            }
        }
    }

    public class StatusUpdate
    {
        public string id;
        public string klass;
        public string comment;
        public string created_at;
        public string humand_readable_status_to;
        public string status_from;
        public string status_to;
        public string status_update_type;
        public string status_updated_occurred_at;
        public string updated_at;
        public UserBasicData user;

        public OrganizationBasicData organization;
    }

    public class TermsOfAgreement
    {
        public string klass;
        public string id;
        public string referral_percentage_fee;
        public string created_at;
        public string updated_at;
    }

    public class UserBasicData
    {
        public string klass;
        public string id;
        public string first_name;
        public string last_name;
        public string email;
        public string phone;
        public string profile_picture_thumb;
        public string profile_picture_small;
        public string profile_picture_medium;
        public string profile_picture_large;
    }

}