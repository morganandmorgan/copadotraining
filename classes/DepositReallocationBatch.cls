global class DepositReallocationBatch implements Database.Batchable<SObject>, Database.Stateful
{   
    public Set<Id> specificMatters;
    
    public DepositReallocationBatch(Set<Id> specificMatters){
        this.specificMatters = specificMatters;
    }
	public Database.QueryLocator start(Database.BatchableContext context)
    {        
        String query = 'SELECT Id FROM litify_pm__Matter__c WHERE Id in :specificMatters';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
    	Savepoint sp1 = Database.setSavepoint();
		Try{
	        Set<Id> matterIds = new Set<Id>();
	        Set<Id> depositIds = new Set<Id>();
	        List<Deposit__c> depositUpdate = new List<Deposit__c>();
			List<litify_pm__Expense__c> expenseUpdate = new List<litify_pm__Expense__c>();
			List<Expense_to_Deposit__c> joinDeleteList = new List<Expense_to_Deposit__c>();

	        for (SObject so : scope){       
	        	litify_pm__Matter__c matter = (litify_pm__Matter__c)so;
	            matterIds.add(matter.Id);
	        }

			for(litify_pm__Matter__c matter : 
				[SELECT Id,
			     RecordType.Name,
			        (SELECT Id,
			         Amount__c,
			         Source__c,
			         Allocated_Soft_Cost__c,
			         Allocated_Hard_Cost__c,
			         Allocated_Fee__c,
			         Deposit_Allocated__c FROM Revenues__r WHERE RecordType.Name = 'Operating'),
			        (SELECT Id,
			         litify_pm__Amount__c,
			         Amount_Recovered_Settlement__c,
			         Amount_Recovered_Client__c,
			         Amount_Recovered__c,
			         Amount_Journaled__c,
			         Recover_From_Client__c,
			         CostType__c,
			         Fully_Recovered__c,
			         litify_pm__Status__c 
			        FROM litify_pm__Expenses__r)
			    FROM litify_pm__Matter__c 
			    WHERE Id IN :matterIds]){

				FOR (litify_pm__Expense__c expense : matter.litify_pm__Expenses__r)
			    {
			        expense.Amount_Recovered_Settlement__c = 0;
			        expense.Amount_Recovered_Client__c = 0;
			        expense.Fully_Recovered__c = false;
			        expenseUpdate.add(expense);
			    }

			    // Now go through all deposits...
			    FOR (Deposit__c deposit : matter.Revenues__r)
			    {
			    	deposit.Deposit_Allocated__c = false;
			        deposit.Allocated_Soft_Cost__c = 0;
			        deposit.Allocated_Hard_Cost__c = 0;
			        deposit.Allocated_Fee__c = 0;
			        depositUpdate.add(deposit);
			        depositIds.add(deposit.Id);
			    }

			    joinDeleteList = [SELECT ID FROM Expense_to_Deposit__c WHERE Deposit__c in : depositIds];
			}

		
			system.debug('Execute - expenseUpdate = ' + expenseUpdate);
			update expenseUpdate;
			system.debug('Execute - depositUpdate =' + depositUpdate);
			update depositUpdate;
			system.debug('Execute - joinDeleteList =' + joinDeleteList);
			delete joinDeleteList;

			for(Deposit__c deposit : depositUpdate){
				Set<Id> depIds = new Set<Id>{deposit.Id};
				MatterAllocationService.allocateItems_Deposit(depIds);	
			}
		}
		Catch(Exception e){
			system.debug('ERROR - '+e.getMessage() + e.getStackTraceString());
			Database.rollback(sp1);
		}
    }

    public void finish(Database.BatchableContext context)
    {
    }

}