public class mmattyprtl_JavascriptRemotingController
{
    @RemoteAction
    public static map<string, string> litigationTypes()
    {
        map<string, string> output = new map<string, string>();

        for ( PicklistEntry pe : Intake__c.Litigation__c.getDescribe().getPicklistValues() )
        {
            if ( pe.isActive() )
            {
                output.put( pe.getValue(), pe.getLabel() );
            }
        }

        return output;
    }

    @RemoteAction
    public static map<string, string> generatingLawyers()
    {
        map<string, string> output = new map<string, string>();

        list<Contact> generatingLawyersList = mmcommon_ContactsSelector.newInstance().selectAllMorganAndMorganAttorneys();

        for ( Contact lawyer : generatingLawyersList )
        {
            output.put( lawyer.id, lawyer.lastName + ', ' + lawyer.firstName );
        }

        return output;
    }

    private static map<string, Schema.sObjectField> webIdToSObjectFieldMap = new map<string, Schema.sObjectField>();

    static
    {
        webIdToSObjectFieldMap.put( 'combobox-generating-lawyer-select', Intake__c.Generating_Attorney__c );
        webIdToSObjectFieldMap.put( 'combobox-generating-source-select', Intake__c.Marketing_Sub_source__c );
        webIdToSObjectFieldMap.put( 'combobox-litigation-select', Intake__c.Litigation__c );
        webIdToSObjectFieldMap.put( 'textarea-case-details', Intake__c.Lead_Details__c );
        webIdToSObjectFieldMap.put( 'textarea-generating-source-details', Intake__c.Additional_Marketing_Source_Information__c );
        webIdToSObjectFieldMap.put( 'text-input-client-email-address', Account.PersonEmail );
        webIdToSObjectFieldMap.put( 'text-input-client-first-name', Account.FirstName );
        webIdToSObjectFieldMap.put( 'text-input-client-last-name', Account.LastName );
        webIdToSObjectFieldMap.put( 'text-input-client-home-phone', Account.Phone);
        webIdToSObjectFieldMap.put( 'text-input-client-mobile-phone', Account.PersonMobilePhone);
    }

    @RemoteAction
    public static IntakeDetails saveIntake(map<string, string> fieldParameterMap)
    {
        IntakeDetails output = new IntakeDetails();

        system.debug( fieldParameterMap );

        if ( fieldParameterMap != null
            && ! fieldParameterMap.isEmpty() )
        {
            map<Schema.sObjectField, string> serviceInputMap = new map<Schema.sObjectField, string>();

            mmattyprtl_Enums.SCREENING_OPTIONS screeningOption = null;

            set<string> additionalEmailAddressesToNotifyList = new set<String>();

            for ( String mapKey : fieldParameterMap.keyset() )
            {
                if ( webIdToSObjectFieldMap.containsKey( mapKey.toLowerCase() ) )
                {
                    // This is a field on the Intake__c or Account
                    if ( String.isNotBlank(fieldParameterMap.get( mapKey ))  )
                    {
                        // there is value present
                        serviceInputMap.put( webIdToSObjectFieldMap.get( mapKey.toLowerCase() ), fieldParameterMap.get( mapKey ));
                    }
                }
                else if ( 'radio-input-screening'.equalsIgnoreCase( mapKey ) )
                {
                    if ( '1'.equalsIgnoreCase( fieldParameterMap.get( mapKey ) ) )
                    {
                        screeningOption = mmattyprtl_Enums.SCREENING_OPTIONS.CALL_AND_SCREEN;
                    }
                    else if ( '2'.equalsIgnoreCase( fieldParameterMap.get( mapKey ) ) )
                    {
                        screeningOption = mmattyprtl_Enums.SCREENING_OPTIONS.CALL_AND_SIGN_UP;
                    }
                    else if ( '3'.equalsIgnoreCase( fieldParameterMap.get( mapKey ) ) )
                    {
                        screeningOption = mmattyprtl_Enums.SCREENING_OPTIONS.DO_NOT_CALL;
                    }
                }
                else if ( 'text-input-alternate-email-address'.equalsIgnoreCase( mapKey )
                        && String.isNotBlank( fieldParameterMap.get( mapKey ) ) )
                {
                    additionalEmailAddressesToNotifyList.addAll( fieldParameterMap.get( mapKey ).split(',') );
                }
            }

            try
            {
                Intake__c newIntake = mmattyprtl_IntakeSignUpService.processSignUpRequest( serviceInputMap, screeningOption, additionalEmailAddressesToNotifyList );

                output.intakeName = newIntake.name;
                output.intakeId = newIntake.id;
            }
            catch ( mmattyprtl_IntakeSignUpServiceException isuse )
            {
                // now what?
                system.debug( isuse );
                system.debug( isuse.getCause() );
                system.debug( isuse.getCause().getMessage() );
                system.debug( isuse.getCause().getStackTraceString() );

                if ( isuse.getCause() instanceOf System.DMLException )
                {
                    System.DMLException dmle = (System.DMLException)isuse.getCause();

                    if ( StatusCode.REQUIRED_FIELD_MISSING == dmle.getDmlType(0) )
                    {
                        throw new UXException( 'The following fields are required: ' + string.join( dmle.getDmlFieldNames(0), ', '));
                    }
                    else
                    {
                        throw new UXException( 'There was a problem saving the refereral.  Please try again and if the issue persists, contact IT Support.' );
                    }
                }
                else
                {
                    throw new UXException( isuse.getCause() == null ? isuse.getMessage() : isuse.getCause().getMessage() );
                }
            }
        }

        return output;
    }

    public class IntakeDetails
    {
        public string intakeName;
        public string intakeId;
    }

    public class UXException extends Exception { }
}