public class mmwiz_AnswerModel
    extends mmwiz_AbstractBaseModel
{
    private static final String TAGS_PROPERTY_NAME = 'tags';
    private List<String> tags;

    public mmwiz_AnswerModel()
    {
        setType(mmwiz_AnswerModel.class);
    }

    public override void initialize( Wizard__mdt wizard )
    {
        super.initialize( wizard );

        if ( this.serializedDataMap.containsKey( TAGS_PROPERTY_NAME )) {
            setTags ((List<String>)JSON.deserialize(JSON.serialize(serializedDataMap.get(TAGS_PROPERTY_NAME)), List<String>.class));
        }
    }

    public List<String> getTags()
    {
        return tags;
    }

    public List<String> setTags(List<String> value)
    {
        tags = value;
        return tags;
    }

    public override void prepareForSerialization()
    {
        super.prepareForSerialization();
        serializedDataMap.put(TAGS_PROPERTY_NAME, getTags());
    }
}