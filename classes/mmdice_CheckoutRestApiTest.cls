@isTest
public with sharing class mmdice_CheckoutRestApiTest
{
    static testMethod void mmdice_CheckoutRestApi_Ctor_Test()
    {
        // Arrange + Act
        Test.startTest();
        mmdice_CheckoutRestApi checkoutRestApi = new mmdice_CheckoutRestApi();
        Test.stopTest();

        // Assert
        System.assert(checkoutRestApi != null);
    }

    static testMethod void mmdice_CheckoutRestApi_Post_Test()
    {
        // Arrange
        Integer expectedStatusCode = 400;
        mmdice_CheckoutRestApi.PostRequestBody requestBody = new mmdice_CheckoutRestApi.PostRequestBody();

        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.requestURI = '/services/apexrest/dice/checkout/a003D000001dPZD';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf('{}');
        RestContext.request = req;
        RestContext.response = resp;

        // Act
        Test.startTest();
        mmdice_CheckoutRestApi.PostResponse postResponse = mmdice_CheckoutRestApi.post(requestBody);
        Test.stopTest();

        // Assert
        System.assertEquals(expectedStatusCode, resp.statusCode);
    }

    static testMethod void mmdice_CheckoutRestApi_Post_BadRequest_Test()
    {
        // Arrange
        Integer expectedStatusCode = 400;
        mmdice_CheckoutRestApi.PostRequestBody requestBody = new mmdice_CheckoutRestApi.PostRequestBody();

        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.requestURI = '/services/apexrest/dice/checkout/NotASalesforceId';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = resp;

        // Act
        Test.startTest();
        mmdice_CheckoutRestApi.PostResponse postResponse = mmdice_CheckoutRestApi.post(requestBody);
        Test.stopTest();

        // Assert
        System.assertEquals(expectedStatusCode, resp.statusCode);
        System.assert(postResponse == null);
    }
}