@isTest
private class IntakeAutoCreateFromTask_TrigAct_Test {

    private static Lead lead;
    private static Task task;
    private static Account account;
    private static List<Intake__c> intake;

    private static void setup() {
        lead = TestUtil.createLead();
        insert lead;

        String subject = 'Submitted Form "forthepeople Case Evaluation Form"';
        task = TestUtil.createTask(lead.Id,subject);
        task.put('Description','First Name: TEST');
        insert task;

        task = [select id, accountId from task where id = :task.id];
        account = [select id from account where id = :task.accountId];

    }

    private static void configure_settings() {
        List<Hubspot_Field_Mapping__c> fieldMappings = new List<Hubspot_Field_Mapping__c>();
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Case Details', Hubspot_Field_Label__c= 'Case Details', Intake_Field_API_Name__c = 'Lead_Details__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Case Handling Request', Hubspot_Field_Label__c= 'Case Handling Request', Intake_Field_API_Name__c = 'Screening__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Case Type', Hubspot_Field_Label__c= 'Salesforce Case Type', Intake_Field_API_Name__c = 'Case_Type__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Email', Hubspot_Field_Label__c= 'Email', Intake_Field_API_Name__c = 'Email__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='First Name', Hubspot_Field_Label__c= 'First Name', Intake_Field_API_Name__c = 'Caller_First_Name__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Generating Credit', Hubspot_Field_Label__c= 'Generating Credit', Intake_Field_API_Name__c = 'Generating_Attorney__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Generating Source', Hubspot_Field_Label__c= 'Generating Source', Intake_Field_API_Name__c = 'Marketing_Sub_Source__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Last Name', Hubspot_Field_Label__c= 'Last Name', Intake_Field_API_Name__c = 'Caller_Last_Name__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Litigation', Hubspot_Field_Label__c= 'Salesforce Litigation', Intake_Field_API_Name__c = 'Litigation__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='LitigationAlternate', Hubspot_Field_Label__c= 'Litigation', Intake_Field_API_Name__c = 'Litigation__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Marketing Source', Hubspot_Field_Label__c= 'Marketing Source', Intake_Field_API_Name__c = 'MarketingSourceGUID__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Phone Number', Hubspot_Field_Label__c= 'Phone Number', Intake_Field_API_Name__c = 'Phone__c'));
        fieldMappings.add(new Hubspot_Field_Mapping__c(Name ='Web Property', Hubspot_Field_Label__c= 'Web Property', Intake_Field_API_Name__c = 'marketing_source__c'));
        insert fieldMappings;
    }

    @isTest static void TestANotFailedHubspot() {
        configure_settings();
        Lead l = new Lead(FirstName = 'deacon', LastName = 'jones', Email = 'anonimized@gmail.com', Status = 'Lead', Phone = '7077777777', Description = '');
        insert l;

        Task t = new Task(WhoId = l.Id, Subject = 'Submitted Form "CA.com Sidebar Form - Takata"', Status = 'Completed', Priority = 'High', Description = 'First Name: deacon\nSalesforce Litigation: Product Liability\nPhone Number: 7077777777\nMarketing Source: 1B8DCA27-9AFA-E511-A2CB-00155D5D2601\nEmail: deaconjones@gmail.com\nWeb Property: classaction.com\nLast Name: jones\nCase Details: testing');
        insert t;

        //t.Priority = 'Normal';
        //update t;

        t = [select id, accountId from task where id = :t.id];
        Account a = [select id,PersonLeadSource,PersonLeadSubSource__c, phone from account where id = :t.accountId];
        System.debug('account: ' + a);
        Intake__c i = [select id, status__c, RecordType.Name, client__c from intake__c where client__c = :a.id];

        System.debug(i.id);
    }

    @isTest static void TestAFailedHubspot1() {
        configure_settings();
        Lead l1 = new Lead(FirstName = 'Dori', LastName = 'Ceasar', Email = 'anonimized@yahoo.com'
                            , Status = 'Lead', Phone = '9545486000', Description = 'if you work on sunday are you supposed to get paid regular overtime or doubletime pay?');
        Lead l2 = new Lead(FirstName = 'deacon', LastName = 'jones', Email = 'anonimized@gmail.com'
                            , Status = 'Lead', Phone = '7077777777', Description = '');

        List<Lead> ls = new List<Lead>();
        ls.add(l1);
        ls.add(l2);

        insert ls;

        system.debug('\n\n\n'+[select Id, FirstName, LastName, Phone, Email, LeadSource, Status, NumberOfEmployees, OwnerId, IsConverted, ConvertedDate, ConvertedAccountId, ConvertedContactId, ConvertedOpportunityId, IsUnreadByOwner, ConnectionReceivedId, ConnectionSentId, Lead_Sub_Source__c, Description from Lead]+'\n\n\n');

        Task t1 = new Task(WhoId = l1.Id, Subject = 'Submitted Form "forthepeople Case Evaluation Form_Overtime Lawsuit"', Status = 'Completed', Priority = 'High', Description = 'First Name: Dori\nKeep me informed about important consumer alerts.: false\nPhone Number: 9545486045\nMarketing Source: 22BFADFD-8CC4-E211-A930-00155D5D2603\nCase Details: if you work on sunday are you supposed to get paid regular overtime or doubletime pay?\nEmail: doriceasar3@yahoo.com\nWeb Property: ForThePeople-com-INTERNET\nLast Name: Ceasar');
        Task t2 = new Task(WhoId = l2.Id, Subject = 'Submitted Form "CA.com Sidebar Form - Takata"', Status = 'Completed', Priority = 'High', Description = 'First Name: deacon\nSalesforce Litigation: Product Liability\nPhone Number: 7077777777\nMarketing Source: 1B8DCA27-9AFA-E511-A2CB-00155D5D2601\nEmail: deaconjones@gmail.com\nWeb Property: classaction.com\nLast Name: jones\nCase Details: testing');

        List<Task> ts = new List<Task>();
        ts.add(t1);
        ts.add(t2);

        system.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
        system.debug(t1);
        insert ts;
        system.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

        //System.assert(t1.accountId != null);

        t1 = [select id, accountId from task where id = :t1.id];

        System.assert(t1.accountid != null);
        Account a1 = [select id,PersonLeadSource,PersonLeadSubSource__c, phone from account where id = :t1.accountId];
        system.debug(a1);
        system.debug([select id, status__c, RecordType.Name, client__c from intake__c ]);

        Intake__c i1 = [select id, status__c, RecordType.Name, client__c from intake__c where client__c = :a1.id];
        System.debug(i1.id);

        t2 = [select id, accountId from task where id = :t2.id];
        Account a2 = [select id,PersonLeadSource,PersonLeadSubSource__c, phone from account where id = :t2.accountId];
        Intake__c i2 = [select id, status__c, RecordType.Name, client__c from intake__c where client__c = :a2.id];
        System.debug(i2.id);
    }
    static testMethod void testIntakeCreate(){
        Test.startTest();
        setup();

        // update task to run trigger (this will happen normally when lead is converted)
        task.Priority = 'High';
        update task;

        // retrieve intake from newly converted account and check status and recordtype are set correctly
        intake = [select id, status__c, RecordType.Name, client__c
                from intake__c where client__c = :account.id];

        System.assertEquals('Lead', intake[0].status__c);
        System.assertEquals('PlaceHolder', intake[0].RecordType.Name);

        // test that trigger does NOT run again in same context and insert second intake
        task.Priority = 'Normal';
        update task;
        intake = [select id, status__c, RecordType.Name, client__c
                from intake__c where client__c = :account.id];
        System.assertEquals(1, intake.size());
        Test.stopTest();
    }
}