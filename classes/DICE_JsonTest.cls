@isTest
private class DICE_JsonTest {
	
	@isTest static void ParseQuery_PassedNull_ReturnsEmptyList() {
		List<DICE_Json.Query> qrs = DICE_Json.ParseQuery(null);
    System.assertEquals(0, qrs.size());
	}
	
	@isTest static void ParseSortOrder_PassedNull_ReturnsEmptyList() {
    List<DICE_Json.SortOrder> sos = DICE_Json.ParseSortOrder(null);
    System.assertEquals(0, sos.size());
	}
	
}