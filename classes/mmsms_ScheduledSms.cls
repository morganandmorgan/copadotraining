public with sharing class mmsms_ScheduledSms
{
	private List<tdc_tsw__Scheduled_Sms__c> records = new List<tdc_tsw__Scheduled_Sms__c>();
	private Map<Id, tdc_tsw__Scheduled_Sms__c> existingRecords = new Map<Id, tdc_tsw__Scheduled_Sms__c>();

	private mmsms_ScheduledSms()
	{
		// Hide from consumer.
	}

	private void setRecords(list<tdc_tsw__Scheduled_Sms__c> records)
	{
		if (records != null)
		{
			this.records = records;
		}
	}

	private void onBeforeInsert()
    {
		new mmsms_ScheduledSmsLogic().removeRecentlySentDuplicates(this.records);
    }

	public static void triggerHandler()
	{
		mmsms_ScheduledSms domain = new mmsms_ScheduledSms();

		if (Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete)
		{
			domain.setRecords( Trigger.new );
		}

		if (Trigger.isBefore && Trigger.isInsert)
		{
			domain.onBeforeInsert();
		}
	}
}