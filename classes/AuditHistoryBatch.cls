/*
    //code to schedule job
    String userEmail = 'rcolbert@forthepeople.com';
    Integer minutesAgo = 270; //4:30 hours
    String objectType = 'litify_pm__Matter__c';
    Integer batchSize = 1;

    AuditHistoryBatch ahb = new AuditHistoryBatch(userEmail, minutesAgo, objectType, batchSize);

    String chronExp = '0 30 4 ? * * *'; //4:30 every day
    System.schedule('AuditHistoryBatch', chronExp, ahb);

*/
global without sharing class AuditHistoryBatch implements Database.Batchable<SObject>, Schedulable {

    private String userEmail;
    private DateTime startDT;
    private DateTime endDT;
    private String objType;
    private Integer minutesAgo;
    private Integer batchSize;

    //constructor to set all parameters
    public AuditHistoryBatch(String usersEmail, DateTime startDateTime, DateTime endDateTime, String objectType, Integer sizeOfBatch) {
        this.userEmail = usersEmail;
        this.startDT = startDateTime;
        this.endDT = endDateTime;
        this.objType = objectType;
        this.batchSize = sizeOfBatch;
    } //constructor

    //constructor to calculate times based on execution time, for scheduling
    public AuditHistoryBatch(String usersEmail, Integer schedMinutesAgo, String objectType, Integer sizeOfBatch) {
        this.userEmail = usersEmail;
        this.objType = objectType;
        this.minutesAgo = schedMinutesAgo;
        this.batchSize = sizeOfBatch;
    } //constructor

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String historyObj = AuditProcessor.determineHistoryObject(objType);

        String historyQuery = 'SELECT parentId'
            +' FROM ' + historyObj
            +' WHERE createdDate >= :startDT AND createdDate <= :endDT'
            +' AND createdBy.email = :userEmail';

        Set<Id> objectIds = new Set<Id>();
        //build a unique list of objectIds that have been modified
        for (SObject history : Database.query(historyQuery)) {
            objectIds.add((Id)history.get('parentId'));
        }

        String batchQuery = 'SELECT id FROM ' + objType + ' WHERE id IN :objectIds';
        return Database.getQueryLocator(batchQuery);
    } //start (batch)

    global void execute(SchedulableContext sc) {
        //If minutesAgo was not specified, use the dates specified
        if (minutesAgo == null) {
            System.debug('Using pre-defined dates');
            Database.executeBatch(new AuditHistoryBatch(userEmail, startDT, endDT, objType, batchSize), batchSize);
        } else { //otherwise, calculated the date range now
            this.endDT = DateTime.now();
            this.startDT = endDT.addMinutes(-1*minutesAgo);
            System.debug('Calculating Dates');
            System.debug(startDT);
            System.debug(endDT);
            Database.executeBatch(new AuditHistoryBatch(userEmail, startDT, endDT, objType, batchSize), batchSize);
        }
    } //execute scheduled

    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        Set<Id> objectIds = new Set<Id>();
        for (SObject obj : scope) {
            objectIds.add(obj.id);
        }

        AuditProcessor.processHistory(userEmail, startDT, endDT, objType, objectIds);
    } //execute batch

    global void finish(Database.BatchableContext bc) {
        //
    } //finish

} //class