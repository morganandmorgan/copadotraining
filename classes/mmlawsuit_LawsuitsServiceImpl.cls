public class mmlawsuit_LawsuitsServiceImpl
    implements mmlawsuit_ILawsuitsService
{
    public void updateStatusForMatterChange(Map<Id, String> matterStatusMap)
    {
        if (matterStatusMap == null || matterStatusMap.isEmpty())
        {
            return;
        }

        List<Lawsuit__c> lawsuitList = mmlawsuit_LawsuitsSelector.newInstance().selectByMatter(matterStatusMap.keyset());

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        for (Lawsuit__c ls : lawsuitList)
        {
            String newMatterStatus = matterStatusMap.get(ls.Related_Matter__c);

            ls.CP_Status__c = newMatterStatus;
            uow.registerDirty(ls);
        }

        uow.commitWork();
    }

    public void updateTeamMemberAssignmentsForUserChange(Set<Id> userIdSet)
    {
        if (userIdSet == null || userIdSet.isEmpty())
        {
            return;
        }

        Set<Id> matterIdSet =
            mmlib_Utils.generateIdSetFromField(
                mmmatter_MatterTeamMembersSelector.newInstance().selectByUser(userIdSet),
                litify_pm__Matter_Team_Member__c.litify_pm__Matter__c);

        updateTeamMemberAssignmentsFromMatter(matterIdSet);
     }

    public void updateTeamMemberAssignmentsFromMatter(Set<Id> matterIdSet)
    {
        System.debug('JLW:::updateTeamMemberAssignmentsFromMatter');
        if (matterIdSet == null || matterIdSet.isEmpty())
        {
            System.debug('JLW:::matterIdSet is null or empty so quiting.');
            return;
        }

        // Payload is list of Matter IDs.
        Map<Id, List<litify_pm__Matter_Team_Member__c>> mtmMapByMatterId = new Map<Id, List<litify_pm__Matter_Team_Member__c>>();
        Map<Id, Lawsuit__c> lawsuitMapByMatterId = new Map<Id, Lawsuit__c>();

        for (Id matterId : matterIdSet)
        {
            mtmMapByMatterId.put(matterId, null);
            lawsuitMapByMatterId.put(matterId, null);
        }

        // Query all related MatterTeamMember records, organized as Map<Matter.Id, List<MTM>>.
        mtmMapByMatterId =
            (Map<Id, List<litify_pm__Matter_Team_Member__c>>)
            mmlib_Utils.generateSObjectMapByIdField(
                mmmatter_MatterTeamMembersSelector.newInstance().selectByMatter(mtmMapByMatterId.keyset()),
                litify_pm__Matter_Team_Member__c.litify_pm__Matter__c);

        // Query related Users
        Map<Id, User> userMap = new Map<Id, User>();

        for (List<litify_pm__Matter_Team_Member__c> mtmList : mtmMapByMatterId.values())
        {
            for (litify_pm__Matter_Team_Member__c mtm : mtmList)
            {
                userMap.put(mtm.litify_pm__User__c, null);
            }
        }

        userMap = new Map<Id, User>(mmcommon_UsersSelector.newInstance().selectById(userMap.keyset()));

        // Query related Lawsuits, organized as Map<Matter.Id, Lawsuit>.
        for (Lawsuit__c ls : mmlawsuit_LawsuitsSelector.newInstance().selectByMatter(lawsuitMapByMatterId.keyset()))
        {
            lawsuitMapByMatterId.put(ls.Related_Matter__c, ls);
        }

        Set<String> teamRoleNameSet = new Set<String>();
        for(Integer i = 0; i < mmlawsuit_LawsuitsService.RelevantTeamRoleName.values().size(); i++)
        {
            teamRoleNameSet.add(mmlawsuit_LawsuitsService.RelevantTeamRoleName.values().get(i).name().replace('_', ' '));
        }
        for(String r : teamRoleNameSet) {
            System.debug('teamRoleNameSet: ' + r);
        }

        // Query Team Roles for the significant names.
        Map<Id, litify_pm__Matter_Team_Role__c> roleMap =
            new Map<Id, litify_pm__Matter_Team_Role__c>(
                ((mmmatter_IMatterTeamRolesSelector)
                mm_Application.Selector
                    .newInstance(litify_pm__Matter_Team_Role__c.SObjectType))
                    .selectByName(teamRoleNameSet));

        for(litify_pm__Matter_Team_Role__c r : roleMap.values()) {
            System.debug('roleMap: ' + r.Name);
        }
        // For each event; for each Matter; for each MTM; per Role, copy data to Lawsuit.
        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        // Clear all of the relevant CP fields on the Lawsuits.
        for (Lawsuit__c lawsuit : lawsuitMapByMatterId.values())
        {
            if ( lawsuit != null )
            {
                lawsuit.CP_Case_Developer__c = '';
                lawsuit.CP_Case_Developer_Email__c = '';
                lawsuit.CP_Case_Manager__c = '';
                lawsuit.CP_Case_Manager_Email__c = '';

                lawsuit.CP_Assigned_Attorney__c = '';
                lawsuit.CP_Assigned_Attorney_Email__c = '';
                lawsuit.CP_Handling_Attorney__c = '';
                lawsuit.CP_Handling_Attorney_Email__c = '';
                lawsuit.CP_Managing_Attorney__c = '';
                lawsuit.CP_Managing_Attorney_Email__c = '';

                lawsuit.CP_Paralegal__c = '';
                lawsuit.CP_Paralegal_Email__c = '';

                uow.registerDirty(lawsuit);
            }
        }

        // Set the Lawsuits to match the CURRENT Team Member records.
        for (Id matterId : matterIdSet)
        {
            if ( mtmMapByMatterId.containsKey( matterId ) )
            {
                for (litify_pm__Matter_Team_Member__c mtm : mtmMapByMatterId.get(matterId))
                {
                    setLawsuitClientProfileFields(
                        lawsuitMapByMatterId.get(matterId),
                        roleMap.get(mtm.litify_pm__Role__c),
                        userMap.get(mtm.litify_pm__User__c),
                        uow);
                }
            }
        }
        /*for(Id matterId : matterIdSet) {
            Set<Id> teamMemberIds = new Set<Id>();
            for (litify_pm__Matter_Team_Member__c matterTeamMember : mtmMapByMatterId.get(matterId)) {
                teamMemberIds.add(matterTeamMember.Id);
            }
            System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(teamMemberIds));
        }*/

        uow.commitWork();
    }

    public void setLawsuitClientProfileFields(Lawsuit__c ls, litify_pm__Matter_Team_Role__c role, User u, mmlib_ISObjectUnitOfWork uow)
    {
        System.debug('JLW:::setLawsuitClientProfileFields');
        if (ls != null && role != null && u != null)
        {
            System.debug('JLW:::Inside IF');
            String name = '';
            String email = '';
            if (u.IsActive)
            {
                name = u.Name;
                email = u.Email;
            }

            String roleNameForComparison = role.Name.toUpperCase().replace(' ', '_');
            System.debug('JLW:::ROLE:' + roleNameForComparison);
            if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.CASE_DEVELOPER.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Case_Developer__c = name;
                ls.CP_Case_Developer_Email__c = email;
                uow.registerDirty(ls);
            }
            else if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.CASE_MANAGER.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Case_Manager__c = name;
                ls.CP_Case_Manager_Email__c = email;
                uow.registerDirty(ls);
            }
            else if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.HANDLING_ATTORNEY.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Handling_Attorney__c = name;
                ls.CP_Handling_Attorney_Email__c = email;
                uow.registerDirty(ls);
            }
            else if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.MANAGING_ATTORNEY.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Managing_Attorney__c = name;
                ls.CP_Managing_Attorney_Email__c = email;
                uow.registerDirty(ls);
            }
            else if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.PRINCIPAL_ATTORNEY.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Assigned_Attorney__c = name;
                ls.CP_Assigned_Attorney_Email__c = email;
                uow.registerDirty(ls);
            }
            else if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.PARALEGAL.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Paralegal__c = name;
                ls.CP_Paralegal_Email__c = email;
                uow.registerDirty(ls);
            }
            else if (mmlawsuit_LawsuitsService.RelevantTeamRoleName.LITIGATION_PARALEGAL.name().equalsIgnoreCase(roleNameForComparison))
            {
                ls.CP_Paralegal__c = name;
                ls.CP_Paralegal_Email__c = email;
                uow.registerDirty(ls);
            }
        } else {
            System.debug('No roles matched');
        }
    }
}