/**
 * AuditRuleSelector
 * @description Selector for Audit Rule SObject.
 * @author Matt Terrill
 * @date 9/10/2019
 */

public without sharing class AuditRuleSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public AuditRuleSelector() {
        sObjectFields = new List<Schema.SObjectField> {
                Audit_Rule__c.Id,
                Audit_Rule__c.Category__c,
                Audit_Rule__c.Custom_Scheduled_Date_Field__c,
                Audit_Rule__c.Days__c,
                Audit_Rule__c.Description__c,
                Audit_Rule__c.Email_Audit_Team__c,
                Audit_Rule__c.isActive__c,
                Audit_Rule__c.Lock_Record__c,
                Audit_Rule__c.Name,
                Audit_Rule__c.Object__c,
                Audit_Rule__c.Points__c,
                Audit_Rule__c.Related_Flagged_User__c,
                Audit_Rule__c.Rule_Criteria__c,
                Audit_Rule__c.Rule_Type__c,
                Audit_Rule__c.Scheduled_Flag_Notification__c,
                Audit_Rule__c.Scheduled_Flag_Notification_Days__c,
                Audit_Rule__c.Scheduled_Flag_Notification_Message__c
        };
    } //constructor


    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Audit_Rule__c.SObjectType;
    } //getSObjectType


    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields


    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList


    public List<AuditEvalEngine.Rule> selectById(Set<Id> ids) {
        return AuditEvalEngine.createRulesFromSObjects((List<Audit_Rule__c>)selectSObjectsById(ids));
    } //selectById


    public List<AuditEvalEngine.Rule> selectByActiveForObject(String obj) {
        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('object__c = :obj AND isActive__c = true');

        return AuditEvalEngine.createRulesFromSObjects((List<Audit_Rule__c>)Database.query(query.toSOQL()));
    } //selectByActiveForObject

} //class