@isTest
private class mmdocusign_ConfigurationSettingsTest {

    @isTest
    static void test() {

        String test;

        test = mmdocusign_ConfigurationSettings.getAccountId();
        test = mmdocusign_ConfigurationSettings.getHost();
        test = mmdocusign_ConfigurationSettings.getIntegratorKey();
        Boolean b = mmdocusign_ConfigurationSettings.getIsEnabled();
        b = mmdocusign_ConfigurationSettings.getIsEnabledInSandbox();
        test = mmdocusign_ConfigurationSettings.getPassword();
        test = mmdocusign_ConfigurationSettings.getReturnUrl();
        test = mmdocusign_ConfigurationSettings.getSigningCeremonyLandingPageTemplate();
        test = mmdocusign_ConfigurationSettings.getUsername();
        test = mmdocusign_ConfigurationSettings.getDocuSignUrlFailure();

    } //test
    
} //class