public interface mmcommon_IHolidaysSelector extends mmlib_ISObjectSelector
{
    List<Holiday> selectById(Set<Id> idSet);
    List<Holiday> selectAllFuture();
    List<Holiday> selectAll();
}