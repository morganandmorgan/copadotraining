/**
 *  mmlawsuit_LawsuitsSelector
 */
public class mmlawsuit_LawsuitsSelector
    extends mmlib_SObjectSelector
    implements mmlawsuit_ILawsuitsSelector
{
    public static mmlawsuit_ILawsuitsSelector newInstance()
    {
        return (mmlawsuit_ILawsuitsSelector) mm_Application.Selector.newInstance(Lawsuit__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Lawsuit__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Lawsuit__c.Case_Type__c,
                Lawsuit__c.CP_Assigned_Attorney__c,
                Lawsuit__c.CP_Case_Developer__c,
                Lawsuit__c.CP_Case_Manager__c,
                Lawsuit__c.CP_Matter_Turn_Down_Date__c,
                Lawsuit__c.CP_Status__c,
                Lawsuit__c.Intake__c,
                Lawsuit__c.Related_Matter__c
            };
    }

    public List<Lawsuit__c> selectById(Set<Id> idSet)
    {
        return (List<Lawsuit__c>) selectSObjectsById(idSet);
    }

    public List<Lawsuit__c> selectByIntake(Set<Id> idSet)
    {
        return
            (List<Lawsuit__c>)
            Database.query(
                newQueryFactory()
                .setCondition(Lawsuit__c.Intake__c + ' in :idSet')
                .toSOQL()
            );
    }

    public List<Lawsuit__c> selectByMatter(Set<Id> idSet)
    {
        return
            (List<Lawsuit__c>)
            Database.query(
                newQueryFactory()
                .setCondition(Lawsuit__c.Related_Matter__c + ' in :idSet')
                .toSOQL()
            );
    }

    public List<Lawsuit__c> selectWithFieldsetByIntake(Schema.FieldSet fs, Set<Id> idSet)
    {
        return
            (List<Lawsuit__c>)
            Database.query(
                newQueryFactory()
                .selectFieldSet(fs)
                .setCondition(Lawsuit__c.Intake__c + ' in :idSet')
                .toSOQL()
            );
    }

    public List<AggregateResult> selectAggregatesForLawsuitValidation(Set<Id> intakeIdSet)
    {
        return
            Database.query(
                'SELECT ' +
                '    Intake__c, ' +
                '    Count(Id) lawsuitcount ' +
                'FROM Lawsuit__c ' +
                'WHERE Intake__c IN :intakeIdSet AND Intake__c != null ' +
                'GROUP BY Intake__c ');
    }
}