/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBUpdateTaskChildren {
    global PBUpdateTaskChildren() {

    }
    @InvocableMethod(label='Create and Update Task Children' description='Creates and updates tasks who's due dates are based on the input tasks' completion dates.')
    global static void createAndUpdateTaskChildren(List<Task> parentTasks) {

    }
}
