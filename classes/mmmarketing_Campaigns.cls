/**
 *  mmmarketing_Campaigns
 */
public class mmmarketing_Campaigns
    extends fflib_SObjectDomain
    implements mmmarketing_ICampaigns
{
    private fflib_ISObjectUnitOfWork uow = null;
    private Map<Id, Marketing_Campaign__c> existingRecords = new Map<Id, Marketing_Campaign__c>();

    public static mmmarketing_ICampaigns newInstance(List<Marketing_Campaign__c> records)
    {
        return (mmmarketing_ICampaigns) mm_Application.Domain.newInstance(records);
    }

    public static mmmarketing_ICampaigns newInstance(Set<Id> recordIds)
    {
        return (mmmarketing_ICampaigns) mm_Application.Domain.newInstance(recordIds);
    }

    public mmmarketing_Campaigns(List<Marketing_Campaign__c> records)
    {
        super(records);
    }

    public override void onApplyDefaults()
    {
        defaultCampaignNameIfRequired();
    }

    public override void onAfterInsert()
    {
        autoLinkMarketingTrackingInformationToCampaignIfRequired();
        autoLinkMarketingFinancialPeriodsToCampaignIfRequired();
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
    {
        this.existingRecords = (Map<Id, Marketing_Campaign__c>)existingRecords;
        autoLinkMarketingTrackingInformationToCampaignIfRequired();
        autoLinkMarketingFinancialPeriodsToCampaignIfRequired();
    }

    public override void onBeforeInsert()
    {
        provisionTechnicalKeys();
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords)
    {
        this.existingRecords = (Map<Id, Marketing_Campaign__c>)existingRecords;
        provisionTechnicalKeys();
    }

    private void provisionTechnicalKeys()
    {
        for (Marketing_Campaign__c record : (List<Marketing_Campaign__c>)this.records)
        {
            record.Technical_Key_UTM__c = mmmarketing_Logic.assembleMarketingCampaignTechnicalKeyUTM( record );
            record.Technical_Key_Source_Campaign_ID__c = mmmarketing_Logic.assembleMarketingCampaignTechnicalKeySourceCampaignId( record );
        }
    }

    private void defaultCampaignNameIfRequired()
    {
        for (Marketing_Campaign__c record : (List<Marketing_Campaign__c>)this.records)
        {
            if (string.isBlank( record.Campaign_Name__c ))
            {
                record.Campaign_Name__c = String.isBlank( record.utm_campaign__c ) ? Label.DEFAULT_AUTO_GENERATED_CAMPAIGN_NAME : record.utm_campaign__c;
            }
        }
    }

    public void autoLinkMarketingTrackingInformationToCampaignIfRequired()
    {
        // find all utm_campaign__c and Source_Campaign_ID__c ids for the records in this domain layer
        Set<String> campaignValueSet = new Set<String>();

        boolean isExistingRecordsPresent = ! this.existingRecords.isEmpty();

        for (Marketing_Campaign__c record : (List<Marketing_Campaign__c>)this.records )
        {
            if ( string.isNotBlank( record.utm_campaign__c ) )
            {
                campaignValueSet.add( record.utm_campaign__c );
            }

            if ( string.isNotBlank( record.Source_Campaign_ID__c ) )
            {
                campaignValueSet.add( record.Source_Campaign_ID__c );
            }
        }

        new mmmarketing_TI_AutoLinkCampaignsBatch( campaignValueSet ).setBatchSizeTo(100).execute();
    }

    public void autoLinkMarketingFinancialPeriodsToCampaignIfRequired()
    {
        new mmmarketing_FP_AutoLinkCampaignsBatch().setBatchSizeTo(100).execute();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmmarketing_Campaigns(sObjectList);
        }
    }
}