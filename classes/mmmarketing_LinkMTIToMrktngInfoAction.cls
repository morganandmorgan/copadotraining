/**
 *  mmmarketing_LinkMTIToMrktngInfoAction
 */
public with sharing class mmmarketing_LinkMTIToMrktngInfoAction
    extends mmlib_AbstractAction
{
    private boolean isVerboseDebuggingActive = false;
    private set<String> newMfpTechnicalKeySet = new set<String>();

    public override Boolean runInProcess()
            {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticTILinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticTILinkingEnabled__c )
        {
            if (isVerboseDebuggingActive)
            {
                system.debug(Logginglevel.ERROR, '~~~Records count is '+this.records.size());
            }

            // Query each of the parent objects
            Map<String, SObject> marketingSourceByNameMap = prepareMarketingSourceByNameMap();

            Map<String, Marketing_Campaign__c> marketingCampaignByTechnicalKeysMap = prepareMarketingCampaignByTechnicalKeysMap();

            // date,      domain,     source,      campaign or Campaign_AdWords_ID_Value__c, keyword, MFP ID)
            map<Date, map<String, map<String, map<String, map<String, id>>>>> financialPeriodsMap = prepareMarketingFinancialPeriodsMap();

            boolean isDirty = false;

            // * loop through results and fill in gaps
            for (Marketing_Tracking_Info__c record : (list<Marketing_Tracking_Info__c>)this.records)
            {
                if ( record.Tracking_Event_Timestamp__c != null )
                {
                    isDirty = false;

                    if (record.Marketing_Source__c == null
                        && marketingSourceByNameMap.containsKey(record.Source_Value__c))
                    {
                        record.Marketing_Source__c = marketingSourceByNameMap.get(record.Source_Value__c).id;
                        isDirty = true;
                    }

                    if (record.Marketing_Campaign__c == null)
                    {
                        for (String campaignTechnicalKey : mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) )
                        {
                            if ( marketingCampaignByTechnicalKeysMap.containsKey( campaignTechnicalKey ) )
                            {
                                record.Marketing_Campaign__c = marketingCampaignByTechnicalKeysMap.get( campaignTechnicalKey ).id;
                                isDirty = true;
                                break;
                            }
                        }
                    }

                    if (record.Marketing_Financial_Period__c == null)
                    {
                        date clickDate = getClickDateForMTI( record );

                        if (isVerboseDebuggingActive)
                        {
                            system.debug( 'checking record id: '+ record.id);
                        }

                        /*
                        Marketing_Financial_Period__c.Period_Start_Date__c
                        Marketing_Financial_Period__c.Domain_Value__c
                        Marketing_Financial_Period__c.Source_Value__c ( raw value, not the lookup id )
                        Marketing_Financial_Period__c.Campaign_Value__c ( raw value, not the lookup id )
                        Marketing_Financial_Period__c.Keyword_Value__c
                        Marketing_Financial_Period__c.id
                         */
system.debug( 'runInProcess() mark 3');
system.debug( 'runInProcess() mark 3 -- record.Term_Value__c = ' + record.Term_Value__c);
system.debug( 'runInProcess() mark 3 -- clickDate = ' + clickDate);
system.debug( 'runInProcess() mark 3 -- financialPeriodsMap.containsKey(clickDate) = ' + financialPeriodsMap.containsKey(clickDate));

                        // date, domain, source, campaign, keyword,
                        if ( financialPeriodsMap.containsKey(clickDate)
                            && financialPeriodsMap.get(clickDate).containsKey(record.Domain_Value__c)
                            && financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).containsKey(record.Source_Value__c)
                            && financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).containsKey(record.Campaign_Value__c)
                            && ( financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).containsKey(record.Term_Value__c)
                                || financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).containsKey( mmmarketing_FinancialPeriodFactory.NO_KEYWORD )
                                || string.isBlank(record.Term_Value__c)
                                )
                            )
                        {
system.debug( 'runInProcess() mark 31');
system.debug( 'runInProcess() mark 31 -- check 1 = ' + financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).containsKey(record.Term_Value__c));
system.debug( 'runInProcess() mark 31 -- check 2 = ' + financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).containsKey( mmmarketing_FinancialPeriodFactory.NO_KEYWORD ));
                            // Is a term value present??
                            if ( financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).containsKey(record.Term_Value__c)
                                || ( string.isBlank(record.Term_Value__c)
                                    && financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).containsKey( mmmarketing_FinancialPeriodFactory.NO_KEYWORD )
                                    )
                                )
                            {
                                // then use that value.
                                //if (isVerboseDebuggingActive)
                                {
                                    system.debug( '~~~~~ FOUND ONE ~~~~~');
                                }
                                record.Marketing_Financial_Period__c = financialPeriodsMap.get(clickDate).get(record.Domain_Value__c).get(record.Source_Value__c).get(record.Campaign_Value__c).get( string.isNotBlank( record.Term_Value__c ) ? record.Term_Value__c : mmmarketing_FinancialPeriodFactory.NO_KEYWORD );
system.debug( 'runInProcess() mark 311');
                                // saving the MTI record assumes that there is an existing MFP record.
                                if (this.uow != null
                                    && record.id != null
                                    )
                                {
system.debug( 'runInProcess() mark 3111');
                                    this.uow.registerDirty( record );
                                }
                            }
                            else
                            {
                                // then there is no MFP available for this MTI.  A "NO_TERM_VALUE" MFP will need to be created.
                                // Create the new MFP on this save but don't link it yet.  Let the creation of that record kick off another process to link these MTIs later
                                //
system.debug( 'runInProcess() mark 312');
                                // If this is a brand new MTI record, inside the trigger context, then how would a new MFP record be created in the trigger context???
                                // if this is not in a MTI trigger context, then where does the new MFP get created??
                                // Question: Is there ever a time when this code would be executed and there is not a uow??

system.debug( 'runInProcess() mark 3121');
                                createNewMFPWithNoTermForMTI( record );
                            }
                        }
                        else
                        {
system.debug( 'runInProcess() mark 32');
                                system.debug( 'no match found for clickDate: '+ clickDate + '\n'
                                            + '                   domain: ' + record.Domain_Value__c + '\n'
                                            + '                   Marketing_Source__c: ' + record.Marketing_Source__c + '\n'
                                            + '                   Source_Value__c: ' + record.Source_Value__c + '\n'
                                            + '                   Marketing_Campaign__c: ' + record.Marketing_Campaign__c + '\n'
                                            + '                   Campaign_Value__c: ' + record.Campaign_Value__c + '\n'
                                            + '                   term: ' + record.Term_value__c + '\n'
                                            );

                            // Is the Term value on this MTI blank?
                            if ( string.isBlank(record.Term_Value__c) )
                            {
                                system.debug( 'Adding a "no keyword" MFP for this MTI' );
                                // Add a new MFP record with no term for this MTI
                                createNewMFPWithNoTermForMTI( record );
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    private void createNewMFPWithNoTermForMTI( Marketing_Tracking_Info__c record )
    {
        if (this.uow != null
            && record.id != null
            )
        {
            // Has a new MFPWithNoTerm already been setup that this MTI could use?
            string thisRecordsMFPTechnicalKey = getClickDateForMTI( record )
                                                   + record.Domain_Value__c
                                                   + record.Marketing_Source__c
                                                   + record.Source_Value__c
                                                   + record.Campaign_Value__c
                                                   ;
            system.debug( 'createNewMFPWithNoTermForMTI() mark 2');
            system.debug( 'createNewMFPWithNoTermForMTI() thisRecordsMFPTechnicalKey == ' + thisRecordsMFPTechnicalKey );
            system.debug( 'createNewMFPWithNoTermForMTI() ! newMfpTechnicalKeySet.contains( thisRecordsMFPTechnicalKey ) == ' + ( ! newMfpTechnicalKeySet.contains( thisRecordsMFPTechnicalKey )) );
            if ( ! newMfpTechnicalKeySet.contains( thisRecordsMFPTechnicalKey ) )
            {
                // this MFPWithNoTerm has not yet been created
                Marketing_Financial_Period__c mfpWithNoTerm = mmmarketing_FinancialPeriodFactory.getInstance().generateFromMTIWithNoTerm( record );

                this.uow.registerNew( mfpWithNoTerm );

                newMfpTechnicalKeySet.add( thisRecordsMFPTechnicalKey );
            }
        }
    }

    private date getClickDateForMTI( Marketing_Tracking_Info__c record )
    {
        return record.Tracking_Event_Timestamp__c.date();
    }

    private Map<String, Marketing_Campaign__c> prepareMarketingCampaignByTechnicalKeysMap()
    {
        Set<String> parentMarketingCampaignTechnicalKeysSet = new Set<String>();

        // find campaign values only if that record does not yet have Marketing_Campaign__c lookup set
        for (Marketing_Tracking_Info__c record : (list<Marketing_Tracking_Info__c>)this.records)
        {
            if (record.Marketing_Campaign__c == null)
            {
                parentMarketingCampaignTechnicalKeysSet.addAll( mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( record ) );
            }
        }

        Map<String, Marketing_Campaign__c> marketingCampaignByTechnicalKeysMap = new Map<String, Marketing_Campaign__c>();

        // If there are no records that need to be updated, then don't bother with the SOQL query
        if ( ! parentMarketingCampaignTechnicalKeysSet.isEmpty() )
        {
            List<Marketing_Campaign__c> parentCampaignList = mmmarketing_CampaignsSelector.newInstance().selectByTechnicalKeys( parentMarketingCampaignTechnicalKeysSet );

            for (Marketing_Campaign__c parentCampaign : parentCampaignList)
            {
                for (String campaignTechnicalKey : mmmarketing_Logic.assembleMarketingCampaignTechnicalKeys( parentCampaign ) )
                {
                    marketingCampaignByTechnicalKeysMap.put( campaignTechnicalKey, parentCampaign );
                }
            }
        }

        return marketingCampaignByTechnicalKeysMap;
    }

    private Map<String, SObject> prepareMarketingSourceByNameMap()
    {
        Set<String> parentMarketingSourceValues = new Set<String>();

        // find source values only if that record does not yet have Marketing_Source__c lookup set
        for (Marketing_Tracking_Info__c record : (list<Marketing_Tracking_Info__c>)this.records)
        {
            if (record.Marketing_Source__c == null
                && String.isNotBlank( record.Source_Value__c ) )
            {
                 parentMarketingSourceValues.add( record.Source_Value__c );
            }
        }

        Map<String, SObject> outputMap = new Map<String, SObject>();

        // If there are no records that need to be updated, then don't bother with the SOQL query
        if ( ! parentMarketingSourceValues.isEmpty() )
        {
            outputMap = mmlib_Utils.generateSObjectMapByUniqueField( mmmarketing_SourcesSelector.newInstance().selectByUtm( parentMarketingSourceValues )
                                                                   , Marketing_Source__c.utm_source__c);
        }

        return outputMap;

    }

    private map<Date, map<String, map<String, map<String, map<String, id>>>>> prepareMarketingFinancialPeriodsMap()
    {
        system.debug( 'prepareMarketingFinancialPeriodsMap() start');

        // date,      domain,     source,     campaign or google ad campaing id, keyword, MFP id
        map<Date, map<String, map<String, map<String, map<String, Id>>>>> bigMap = new map<Date, map<String, map<String, map<String, map<String, id>>>>>();

        /*
        Marketing_Financial_Period__c.Period_Start_Date__c
        Marketing_Financial_Period__c.Domain_Value__c
        Marketing_Financial_Period__c.Source_Value__c ( raw value, not the lookup id )
        Marketing_Financial_Period__c.Campaign_Value__c ( raw value, not the lookup id )
        Marketing_Financial_Period__c.Keyword_Value__c
        Marketing_Financial_Period__c.id
         */
        Set<Date> datesSet = new Set<date>();
        Set<String> domainSet = new Set<String>();
        Set<String> sourceSet = new Set<String>();
        Set<String> campaignSet = new Set<String>();
        Set<String> keywordSet = new Set<String>();

        for (Marketing_Tracking_Info__c record : (list<Marketing_Tracking_Info__c>)this.records)
        {
            if (record.Tracking_Event_Timestamp__c != null
                && String.isNotBlank(record.Domain_Value__c)
                && String.isNotBlank(record.Source_Value__c)
                && String.isNotBlank(record.Campaign_Value__c)
                )
            {
                datesSet.add(record.Tracking_Event_Timestamp__c.date());
                domainSet.add( record.Domain_Value__c );
                sourceSet.add( record.Source_Value__c );
                campaignSet.add( record.Campaign_Value__c );
                if ( String.isNotBlank(record.Term_Value__c) )
                {
                    keywordSet.add( record.Term_Value__c );
                }
                //  If there is no Term value, then there might be a "<no keyword>" version of the MFP record.
                else
                {
                    keywordSet.add( mmmarketing_FinancialPeriodFactory.NO_KEYWORD );
                }
            }
        }

        integer bigMapSize = 0;
        List<string> financialPeriodCombinationsList = new list<String>();

        if ( ! datesSet.isEmpty() )
        {
            mmmarketing_FinancialPeriodsSelectParam queryParam = new mmmarketing_FinancialPeriodsSelectParam().setKeywords(keywordSet)
                                                                    .setDates(datesSet).setCampaigns(campaignSet)
                                                                    .setSources(sourceSet).setDomains(domainSet);

            List<Marketing_Financial_Period__c> financialPeriodsList = mmmarketing_FinancialPeriodsSelector.newInstance().selectByCompositeParam( queryParam );

            Boolean isFoundAtLeastOneCampaignRelatedValue = false;

            // sort through the record list and create "the Big Map"
            // date, domain, source, campaign or google ad campaing id, keyword, MFP id
            // map<Date, map<String, map<String, map<String, map<String, id>>>>>
            for (Marketing_Financial_Period__c financialPeriod : financialPeriodsList)
            {
                if ( ! bigMap.containsKey(financialPeriod.Period_Start_Date__c) )
                {
                    bigMap.put(financialPeriod.Period_Start_Date__c, new map<String, map<String, map<String, map<String, id>>>>());
                }

                if ( ! bigMap.get(financialPeriod.Period_Start_Date__c).containsKey(financialPeriod.Domain_Value__c))
                {
                    bigMap.get(financialPeriod.Period_Start_Date__c).put(financialPeriod.Domain_Value__c, new map<String, map<String, map<String, id>>>());
                }

                if ( ! bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).containsKey(financialPeriod.Source_Value__c))
                {
                    bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).put(financialPeriod.Source_Value__c, new map<String, map<String, id>>());
                }

                if ( ! bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).containsKey(financialPeriod.Campaign_Value__c))
                {
                    bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).put(financialPeriod.Campaign_Value__c, new map<String, id>());
                }

                isFoundAtLeastOneCampaignRelatedValue = false;

                if ( ! bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).get(financialPeriod.Campaign_Value__c).containsKey(financialPeriod.Keyword_Value__c))
                {
                    bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).get(financialPeriod.Campaign_Value__c).put(financialPeriod.Keyword_Value__c, financialPeriod.id);

                    financialPeriodCombinationsList.add( financialPeriod.Period_Start_Date__c
                                        + ' | ' + financialPeriod.Domain_Value__c
                                        + ' | ' + financialPeriod.Source_Value__c
                                        + ' | ' + financialPeriod.Campaign_Value__c
                                        + ' | ' + financialPeriod.Keyword_Value__c
                                        + ' | ' + financialPeriod.id
                     );

                    if ( ! isFoundAtLeastOneCampaignRelatedValue )
                    {
                        ++bigMapSize;
                        isFoundAtLeastOneCampaignRelatedValue = true;
                    }
                }

                if ( ! bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).containsKey(financialPeriod.Campaign_AdWords_ID_Value__c))
                {
                    bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).put(financialPeriod.Campaign_AdWords_ID_Value__c, new map<String, id>());
                }

                if ( ! bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).get(financialPeriod.Campaign_AdWords_ID_Value__c).containsKey(financialPeriod.Keyword_Value__c))
                {
                    bigMap.get(financialPeriod.Period_Start_Date__c).get(financialPeriod.Domain_Value__c).get(financialPeriod.Source_Value__c).get(financialPeriod.Campaign_AdWords_ID_Value__c).put(financialPeriod.Keyword_Value__c, financialPeriod.id);

                    financialPeriodCombinationsList.add( financialPeriod.Period_Start_Date__c
                                        + ' | ' + financialPeriod.Domain_Value__c
                                        + ' | ' + financialPeriod.Source_Value__c
                                        + ' | ' + financialPeriod.Campaign_AdWords_ID_Value__c
                                        + ' | ' + financialPeriod.Keyword_Value__c
                                        + ' | ' + financialPeriod.id
                     );

                    if ( ! isFoundAtLeastOneCampaignRelatedValue )
                    {
                        ++bigMapSize;
                        isFoundAtLeastOneCampaignRelatedValue = true;
                    }
                }

            }

            // now that this bigMap is ready
        }

        if (isVerboseDebuggingActive)
        {
            system.debug( 'prepareMarketingFinancialPeriodsMap() bigMap.size ' + bigMapSize);

            String bigMapListString = '\n\n\nprepareMarketingFinancialPeriodsMap() bigMap : \n';

            for (String financialPeriodCombination : financialPeriodCombinationsList)
            {
                bigMapListString += financialPeriodCombination + '\n';
            }

            bigMapListString += '\n\n';

            system.debug( bigMapListString );
        }

        system.debug( 'prepareMarketingFinancialPeriodsMap() end');

        return bigMap;
    }

    private map<String, Marketing_Experiment__c> prepareMarketingExpermimentVariationByExpAndExpVariationMap()
    {
        // create a map where the key is the experiment name and the inner key is the variation and the value is the Marketing_Exp_Variation__c record.
        map<String, Marketing_Experiment__c> experimentMap = new map<String, Marketing_Experiment__c>();

        set<String> experimentSet = new set<String>();

        // find the experiment and experiment_variation values only if that record does not yet have Marketing_Exp_Variation__c lookup set
        for ( Marketing_Tracking_Info__c record : (list<Marketing_Tracking_Info__c>)this.records)
        {
            // find all of the experiment names listed in the trigger context records
            if (record.Marketing_Exp_Variation__c == null
                && String.isNotBlank(record.Experiment_Value__c)
                && String.isNotBlank(record.Experiment_Variation_Value__c) )
            {
                experimentSet.add( record.Experiment_Value__c );
            }
        }

        // If there are no records that need to be updated, then don't bother with the SOQL query
        if ( ! experimentSet.isEmpty() )
        {
            experimentMap = (map<String, Marketing_Experiment__c>)mmlib_Utils.generateSObjectMapByUniqueField( mmmarketing_ExperimentsSelector.newInstance().selectWithVariationsByExperimentName( experimentSet )
                                                                   , Marketing_Experiment__c.Experiment_Name__c );
        }

        return experimentMap;
    }
}