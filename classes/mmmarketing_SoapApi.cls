/**
 *  mmmarketing_SoapApi
 */
global with sharing class mmmarketing_SoapApi
{
    webservice static void reconcileWithCallTrackingMetrics(String mtiCallId)
    {
        if ( mtiCallId != null )
        {
            mmmarketing_TrackingInfosService.updateCallRelatedInformation( new set<Id>{ id.valueOf(mtiCallId) } );
        }
    }
}