public with sharing class mmdocusign_EnvelopeLinkCalloutResponse
	implements mmlib_BaseCallout.CalloutResponse
{
	private String envelopeLink = '';

	public mmdocusign_EnvelopeLinkCalloutResponse(String responseBody)
	{
		// { "url": "https://demo.docusign.net/Signing/startinsession.aspx?t=e5b544eb-1bf7-4cd5-a590-3ce296f32072" }

		System.debug('<ojs> responseBody:\n' + responseBody);

		Result r = (Result) JSON.deserialize(responseBody, Result.class);

		envelopeLink = r.url;
	}

	public String getEnvelopeLink()
	{
		return envelopeLink;
	}

	public Integer getTotalNumberOfRecordsFound()
	{
		return 1;
	}

	private class Result
	{
		public String url = '';
	}
}