/*============================================================================
Name            : PaymentCollection_Domain
Author          : CLD
Created Date    : Jun 2019
Description     : Domain class for Payment Collection
=============================================================================*/
public class PaymentCollection_Domain extends fflib_SObjectDomain {

    private List<Payment_Collection__c> paymentCollections;
    private PaymentCollection_Service paymentCollectionService;

    // Ctors
    public PaymentCollection_Domain() {
        super();
        this.paymentCollections = new List<Payment_Collection__c>();
        this.paymentCollectionService = new PaymentCollection_Service();
    }

    public PaymentCollection_Domain(List<Payment_Collection__c> paymentCollections) {
        super(paymentCollections);
        this.paymentCollections = (List<Payment_Collection__c>) records;
        this.paymentCollectionService = new PaymentCollection_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new PaymentCollection_Domain(records);
        }
    }

    // Trigger Handlers
    public override void onBeforeInsert() {
        //TO DO: check if specific specific trigger context is disabled

        paymentCollectionService.defaultPaymentFields(paymentCollections);
    }
   
}