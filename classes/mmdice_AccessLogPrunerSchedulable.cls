global with sharing class mmdice_AccessLogPrunerSchedulable
implements Schedulable
{
	global void execute(SchedulableContext sc)
	{
		mmdice_IAccessLogsSelector selector = mmdice_AccessLogsSelector.newInstance();

		new mmlib_GenericBatch(
			mmlib_SObjectDeleteExecutor.class,
			selector.selectForPeriodicDeletionQuery())
		.setBatchSizeTo(5000)
		.execute();
	}

	private static String rootJobName = mmdice_AccessLogPrunerSchedulable.class.getName() + ' scheduled job ({0})';
	private static List<Integer> executionTimes = new List<Integer> {0, 20, 40};
	private static Map<Integer, String> getRuntimeJobNameMap()
	{
		Map<Integer, String> result = new Map<Integer, String>();

		for (Integer i : executionTimes) {
			result.put(i, String.format(rootJobName, new List<String> {String.valueOf(i)}));
		}

		return result;
	}

	public static void deleteJobSchedule()
	{
		Map<Integer, String> runtimeJobNameMap = getRuntimeJobNameMap();

		for (
			CronTrigger currentCronjob :
			mmlib_CronTriggersSelector.newInstance().selectScheduledApexByName(new Set<String>(runtimeJobNameMap.values()))
		)
		{
			System.abortJob( currentCronjob.Id );
		}
	}

	public static void setupJobSchedule()
	{
		deleteJobSchedule();

		Map<Integer, String> runtimeJobNameMap = getRuntimeJobNameMap();

		for (Integer i : runtimeJobNameMap.keyset()) {
			String jobName = runtimeJobNameMap.get(i);
			System.schedule(jobName, '0  ' + String.valueOf(i) + ' * * * ?', new mmdice_AccessLogPrunerSchedulable());
		}
	}
}