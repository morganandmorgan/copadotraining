global class SpringMigrator implements Database.Batchable<sObject>, Database.Stateful {
    
    global final String query;
    global final String sfType;
    global final String[] variables;
    global final String[] attributes;
    global final SpringCMEos__EOS_Type__c eosObjectType;
    global final boolean allObjects;
    global String csvString = '';
    
    global SpringMigrator(String q, String sft, String[] v, SpringCMEos__EOS_Type__c eot, String[] a, boolean ao) {
        query=q;
        sfType=sft;
        variables=v;
        eosObjectType = eot;
        attributes=a;
        allObjects = ao;
        csvString = 'SFID,SFType,';
        for (String at : attributes) {
            csvString += '"'+at+'",';
        }
        csvString += 'FolderName,Path\n';
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext info, List<sObject> batch) {
        for(sObject obj : batch) {
            if ((!allObjects && obj.getSObjects('Attachments') != null) || allObjects) {
                List<string> substitutions = new List<String>();
                string objId = (string)obj.get('Id');
                boolean invalidRow = false;
                if (variables != null) {
                    for (string var : variables) {
                        System.debug(var);
                        object varSubstitution;
                        if (var.contains('.')) {
                            string [] parentSplit = var.split('\\.');
                            sObject parent = obj;

                            for (integer i=0; i<parentSplit.size() ; i++) {
                                if (i+1 < parentSplit.size()) {
                                    System.debug(parentSplit[i]);
                                    if (parentSplit[i] != parent.getSObjectType().getDescribe().getName())
                                        parent= parent.getSObject(parentSplit[i]);
                                    System.debug(parent);
                                    //if (parent == null)
                                    //{
                         
                                    //}
                                }
                                else {
                                    if (parent != null) {
                                        varSubstitution =  parent.get(parentSplit[i]);
                                        System.debug(parent);
                                        System.debug(parentSplit[i]);
                                    }
                                    else {
                                       invalidRow = true;
                                    }
                                }
                            }
                        }
                        else {
                            varSubstitution = obj.get(var);
                        }
                        if (varSubstitution instanceof DateTime) {
                             substitutions.add(String.valueOf(Date.valueOf(varSubstitution)));
                        }
                        else {
                            if (String.isNotBlank(String.valueOf(varSubstitution)) )
                            {
                                substitutions.add(CleanString(String.valueOf(varSubstitution)));   
                            }
                            else 
                            {
                                 invalidRow = true;
                            }
                        }
                    }
                }
                
                if (!invalidRow)
                {
                String path = RemoveTrailingPeriods(String.format((string)eosObjectType.SpringCMEos__Path_Format__c, substitutions));
                String foldername = String.format((string)eosObjectType.SpringCMEos__Folder_Name_Format__c , substitutions).removeEnd('.');
                
                String sts = '';
                for (String a : attributes) {
                    sts += '"'+(string)string.valueOf(obj.get(a))+'",';
                }
                csvString += objId + ',' + sfType + ',' + sts +'"' + CleanString(foldername) + '","' + path + '"\n';
                }
            }
        }        
    }

    global void finish(Database.BatchableContext BC) {
        Folder tempFolder = [Select Id from Folder where Name = 'LogoFolder' limit 1];
        
        Document newDoc = new Document();
        newDoc.Name = 'SpringCM Migration ' + sfType;
        newDoc.Body = Blob.valueOf(csvString);
        newDoc.ContentType = 'text/csv';
        newDoc.Type = 'csv';
        newDoc.FolderId = tempFolder.Id;
        insert newDoc;
    }

    private string CleanString(String input) {
        if (string.isBlank(input)){
            return '';
        }
        String regex = '[\\\\#&*|:"?<>/]';
        return input.replaceAll(regex, '_');
    }

    private string RemoveTrailingPeriods(String input) {
        String[] path = input.split('/');
        String newPath = '';
        for (String part : path) {
            if (part != '') {
                newPath += '/'+part.removeEnd('.');   
            }
        }
        return newpath+'/';
    }
}