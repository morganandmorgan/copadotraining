public class mmdocusign_CreateEnvelopeResponse
	implements mmlib_BaseCallout.CalloutResponse
{
	private String clientUserId = '';
	private String envelopeId = '';

	public mmdocusign_CreateEnvelopeResponse(String responseBody, String clientUserId)
	{
		//	{
		//		"envelopeId": "9ecd2c41-ccce-452b-b390-05c03ff0a0ef",
		//		"uri": "/envelopes/9ecd2c41-ccce-452b-b390-05c03ff0a0ef",
		//		"statusDateTime": "2017-08-11T18:39:24.9567774Z",
		//		"status": "sent"
		//	}

		this.clientUserId = clientUserId;

		System.debug('<ojs> responseBody:\n' + responseBody);

		Result r = (Result) JSON.deserialize(responseBody, Result.class);

		envelopeId = r.envelopeId;
	}

	public String getClientUserId()
	{
		return clientUserId;
	}

	public String getEnvelopeId()
	{
		return envelopeId;
	}

	public Integer getTotalNumberOfRecordsFound()
	{
		return 1;
	}

	private class Result
	{
		public String envelopeId = '';
		public String uri = '';
		public String statusDateTime = '';
		public String status = '';
	}
}