/**
 * GovernorLimitsApiTestMock
 * @description Mock for unit test for GovernorLimitsApi
 * @author Matt Terrill
 * @date 8/22/2019
 */
@isTest
global class GovernorLimitsApiTestMock implements HttpCalloutMock {

     global HTTPResponse respond(HTTPRequest req) {
         
        HttpResponse res = new HttpResponse();

        if (req.getEndpoint().containsIgnoreCase('/limits')) {
            res.setBody('{"Limit1":{"Max":40000,"Remaining":37000},"Limit2":{"Max":200,"Remaining":150}}');
        } else { //it is for the login
            res.setBody('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><loginResponse><result><metadataServerUrl>https://...</metadataServerUrl><passwordExpired>false</passwordExpired><sandbox>true</sandbox><serverUrl>https://...</serverUrl><sessionId>mock123Session456Id789</sessionId><userId>XXX</userId><userInfo><accessibilityMode>false</accessibilityMode><chatterExternal>false</chatterExternal><currencySymbol>$</currencySymbol><orgAttachmentFileSizeLimit>5242880</orgAttachmentFileSizeLimit><orgDefaultCurrencyIsoCode>USD</orgDefaultCurrencyIsoCode><orgDefaultCurrencyLocale>en_US</orgDefaultCurrencyLocale><orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments><orgHasPersonAccounts>true</orgHasPersonAccounts><organizationId>00D2a000000DLzbEAG</organizationId><organizationMultiCurrency>false</organizationMultiCurrency><organizationName>Morgan &amp; Morgan, P.A..</organizationName><profileId>XXX</profileId><roleId>XXX</roleId><sessionSecondsValid>7200</sessionSecondsValid><userDefaultCurrencyIsoCode xsi:nil="true"/><userEmail>mterrill@forthepeople.com</userEmail><userFullName>Test User</userFullName><userId>XXX</userId><userLanguage>en_US</userLanguage><userLocale>en_US</userLocale><userName>testUsername</userName><userTimeZone>America/New_York</userTimeZone><userType>Standard</userType><userUiSkin>Theme3</userUiSkin></userInfo></result></loginResponse></soapenv:Body></soapenv:Envelope>');
        }

        return res;

    }//respond

} //class