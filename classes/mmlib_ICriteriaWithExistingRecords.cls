/**
 *  mmlib_ICriteriaWithExistingRecords
 */
public interface mmlib_ICriteriaWithExistingRecords
    extends mmlib_ICriteria
{
    mmlib_ICriteria setExistingRecords( Map<Id, SObject> existingRecords );
}