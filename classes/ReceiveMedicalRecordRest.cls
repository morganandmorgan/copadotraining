@RestResource(urlMapping='/medicalrecords/*')
global with sharing class ReceiveMedicalRecordRest {
  @HttpPost
  global static String DoPost() {
    RestRequest req = RestContext.request;
    Blob jsonBody = req.requestBody;
    Map<String, Object> jsonMap = (Map<String, Object>)JSON.deserializeUntyped(jsonBody.toString());

    Id lawsuitId = String.valueOf(jsonMap.get('externalId'));
    String contents = String.valueOf(jsonMap.get('contents'));

    if (String.isBlank(lawsuitId) || String.isBlank(contents)) {
      RestContext.response.statusCode = 404;
      return 'not found';
    }
    Attachment att = new Attachment(Name = 'Medical Records', Body = EncodingUtil.base64Decode(contents), ParentId = lawsuitId);
    insert att;

    return 'ok';
  }
}