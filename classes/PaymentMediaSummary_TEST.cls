/*============================================================================
Name            : PaymentMediaSummary_TEST
Author          : CLD
Created Date    : July 2018
Description     : Test class for Payment Media Summary Trigger - note it must use seeAllData = true due to limitations in FFA API
=============================================================================*/
@isTest (seeAllData=true)
public class PaymentMediaSummary_TEST {

	@isTest static void testTriggerCode(){

		Set<Id> pmsIds = new Set<Id>();

		for(c2g__codaPaymentMediaDetail__c pmd : [
			SELECT Id, c2g__PaymentMediaSummary__c
			FROM c2g__codaPaymentMediaDetail__c 
			WHERE Payable_Invoice__r.Finance_Request__c != null
			AND c2g__PaymentMediaSummary__r.c2g__PaymentReference__c != null LIMIT 1]){
			pmsIds.add(pmd.c2g__PaymentMediaSummary__c);
		}

		List<c2g__codaPaymentMediaSummary__c> pmsList = [SELECT Id FROM c2g__codaPaymentMediaSummary__c WHERE Id in : pmsIds];
		update pmsList;
	}
	
}