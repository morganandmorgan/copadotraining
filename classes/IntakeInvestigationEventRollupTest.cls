@isTest
private class IntakeInvestigationEventRollupTest {

  private static final String INTAKE_HANDLING_FIRM = 'Morgan & Morgan';

  @TestSetup
  private static void setup() {
    List<SObject> toInsert = new List<SObject>();

    Incident__c incident = new Incident__c();
    toInsert.add(incident);

    Account client = TestUtil.createPersonAccount('Person', 'Test');
    toInsert.add(client);

    Database.insert(toInsert);
    toInsert.clear();

    List<Intake__c> intakes = new List<Intake__c>();
    for (Integer i = 0; i < 5; ++i) {
      Intake__c intake = new Intake__c(
          Incident__c = incident.Id,
          Client__c = client.Id,
          Handling_Firm__c = INTAKE_HANDLING_FIRM
        );
      intakes.add(intake);
    }
    toInsert.addAll((List<SObject>) intakes);

    List<IncidentInvestigationEvent__c> incidentEvents = new List<IncidentInvestigationEvent__c>();
    Datetime now = Datetime.now();
    for (Integer i = 0; i < 3; ++i) {
      IncidentInvestigationEvent__c incidentEvent = new IncidentInvestigationEvent__c(
          EndDateTime__c = now.addHours(2),
          Incident__c = incident.Id,
          StartDateTime__c = now.addHours(1)
        );
      incidentEvents.add(incidentEvent);
    }
    toInsert.addAll((List<SObject>) incidentEvents);

    Database.insert(toInsert);
    toInsert.clear();
  }

  private static List<Intake__c> getIntakes() {
    return [
      SELECT
        Name
      FROM
        Intake__c
    ];
  }

  private static List<IncidentInvestigationEvent__c> getIncidentEvents() {
    return [
      SELECT
        Id,
        Contracts_to_be_Signed__c,
        EndDateTime__c,
        StartDateTime__c,
        Handling_Firm__c
      FROM
        IncidentInvestigationEvent__c
    ];
  }

  private static IntakeInvestigationEvent__c createIntakeInvestigationEvent(Intake__c intake, IncidentInvestigationEvent__c incidentEvent) {
    IntakeInvestigationEvent__c intakeEvent = new IntakeInvestigationEvent__c();
    if (intake != null) {
      intakeEvent.Intake__c = intake.Id;
    }
    if (incidentEvent != null) {
      intakeEvent.IncidentInvestigation__c = incidentEvent.Id;
      intakeEvent.EndDateTime__c = incidentEvent.EndDateTime__c;
      intakeEvent.StartDateTime__c = incidentEvent.StartDateTime__c;
    }
    else {
      Datetime now = Datetime.now();
      intakeEvent.IncidentInvestigation__c = null;
      intakeEvent.EndDateTime__c = now.addHours(2);
      intakeEvent.StartDateTime__c = now.addHours(1);
    }
    return intakeEvent;
  }

  @isTest
  private static void afterInsert_ContractCountsUpdated() {
    List<Intake__c> intakes = getIntakes();
    IncidentInvestigationEvent__c incidentEvent = getIncidentEvents().get(0);

    List<IntakeInvestigationEvent__c> newIntakeEvents = new List<IntakeInvestigationEvent__c>();
    for (Intake__c intake : intakes) {
      newIntakeEvents.add(createIntakeInvestigationEvent(intake, incidentEvent));
    }

    Test.startTest();
    Database.insert(newIntakeEvents);
    Test.stopTest();

    IncidentInvestigationEvent__c requeriedIncidentEvent = new Map<Id, IncidentInvestigationEvent__c>(getIncidentEvents()).get(incidentEvent.Id);

    System.assertEquals(intakes.size(), requeriedIncidentEvent.Contracts_to_be_Signed__c);
  }

  @isTest
  private static void afterInsert_HandlingFirmSet() {
    List<Intake__c> intakes = getIntakes();
    IncidentInvestigationEvent__c incidentEvent = getIncidentEvents().get(0);

    List<IntakeInvestigationEvent__c> newIntakeEvents = new List<IntakeInvestigationEvent__c>();
    for (Intake__c intake : intakes) {
      newIntakeEvents.add(createIntakeInvestigationEvent(intake, incidentEvent));
    }

    Test.startTest();
    Database.insert(newIntakeEvents);
    Test.stopTest();

    IncidentInvestigationEvent__c requeriedIncidentEvent = new Map<Id, IncidentInvestigationEvent__c>(getIncidentEvents()).get(incidentEvent.Id);

    System.assertEquals(INTAKE_HANDLING_FIRM, requeriedIncidentEvent.Handling_Firm__c);
  }

  @isTest
  private static void afterDelete_ContractCountsUpdated() {
    List<Intake__c> intakes = getIntakes();
    IncidentInvestigationEvent__c incidentEvent = getIncidentEvents().get(0);

    List<IntakeInvestigationEvent__c> newIntakeEvents = new List<IntakeInvestigationEvent__c>();
    for (Intake__c intake : intakes) {
      newIntakeEvents.add(createIntakeInvestigationEvent(intake, incidentEvent));
    }
    Database.insert(newIntakeEvents);

    Test.startTest();
    IntakeInvestigationEvent__c toDelete = newIntakeEvents.get(0);
    Database.delete(toDelete);
    Test.stopTest();

    IncidentInvestigationEvent__c requeriedIncidentEvent = new Map<Id, IncidentInvestigationEvent__c>(getIncidentEvents()).get(incidentEvent.Id);

    System.assertEquals(intakes.size() - 1, requeriedIncidentEvent.Contracts_to_be_Signed__c);
    System.assertEquals(INTAKE_HANDLING_FIRM, requeriedIncidentEvent.Handling_Firm__c);
  }

  @isTest
  private static void afterUpdate_RemovedIncidentEvent() {
    List<Intake__c> intakes = getIntakes();
    IncidentInvestigationEvent__c incidentEvent = getIncidentEvents().get(0);

    List<IntakeInvestigationEvent__c> newIntakeEvents = new List<IntakeInvestigationEvent__c>();
    for (Intake__c intake : intakes) {
      newIntakeEvents.add(createIntakeInvestigationEvent(intake, incidentEvent));
    }
    Database.insert(newIntakeEvents);

    Test.startTest();
    IntakeInvestigationEvent__c toUpdate = newIntakeEvents.get(0);
    toUpdate.IncidentInvestigation__c = null;
    Database.update(toUpdate);
    Test.stopTest();

    IncidentInvestigationEvent__c requeriedIncidentEvent = new Map<Id, IncidentInvestigationEvent__c>(getIncidentEvents()).get(incidentEvent.Id);

    System.assertEquals(intakes.size() - 1, requeriedIncidentEvent.Contracts_to_be_Signed__c);
    System.assertEquals(INTAKE_HANDLING_FIRM, requeriedIncidentEvent.Handling_Firm__c);
  }

  @isTest
  private static void afterUpdate_AddedIncidentEvent() {
    List<Intake__c> intakes = getIntakes();
    IncidentInvestigationEvent__c incidentEvent = getIncidentEvents().get(0);

    List<IntakeInvestigationEvent__c> newIntakeEvents = new List<IntakeInvestigationEvent__c>();
    for (Intake__c intake : intakes) {
      newIntakeEvents.add(createIntakeInvestigationEvent(intake, null));
    }
    Database.insert(newIntakeEvents);

    Test.startTest();
    IntakeInvestigationEvent__c toUpdate = newIntakeEvents.get(0);
    toUpdate.IncidentInvestigation__c = incidentEvent.Id;
    Database.update(toUpdate);
    Test.stopTest();

    IncidentInvestigationEvent__c requeriedIncidentEvent = new Map<Id, IncidentInvestigationEvent__c>(getIncidentEvents()).get(incidentEvent.Id);

    System.assertEquals(1, requeriedIncidentEvent.Contracts_to_be_Signed__c);
    System.assertEquals(INTAKE_HANDLING_FIRM, requeriedIncidentEvent.Handling_Firm__c);
  }

  @isTest
  private static void afterUpdate_TransferredIncident() {
    List<Intake__c> intakes = getIntakes();
    List<IncidentInvestigationEvent__c> allIncidentEvents = getIncidentEvents();
    IncidentInvestigationEvent__c incidentEvent = allIncidentEvents.get(0);
    IncidentInvestigationEvent__c newIncidentEvent = allIncidentEvents.get(1);

    List<IntakeInvestigationEvent__c> newIntakeEvents = new List<IntakeInvestigationEvent__c>();
    for (Intake__c intake : intakes) {
      newIntakeEvents.add(createIntakeInvestigationEvent(intake, incidentEvent));
    }
    Database.insert(newIntakeEvents);

    for (IntakeInvestigationEvent__c intakeEvent : newIntakeEvents) {
      intakeEvent.IncidentInvestigation__c = newIncidentEvent.Id;
    }

    Test.startTest();
    Database.update(newIntakeEvents);
    Test.stopTest();

    Map<Id, IncidentInvestigationEvent__c> requeriedIncidentEvents = new Map<Id, IncidentInvestigationEvent__c>(getIncidentEvents());

    System.assertEquals(intakes.size(), requeriedIncidentEvents.get(newIncidentEvent.Id).Contracts_to_be_Signed__c);
    System.assertEquals(0, requeriedIncidentEvents.get(incidentEvent.Id).Contracts_to_be_Signed__c);

    System.assertEquals(INTAKE_HANDLING_FIRM, requeriedIncidentEvents.get(newIncidentEvent.Id).Handling_Firm__c);
    System.assertEquals(INTAKE_HANDLING_FIRM, requeriedIncidentEvents.get(incidentEvent.Id).Handling_Firm__c);
  }
}