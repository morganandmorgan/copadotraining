public class mmlitifylrn_Exceptions
{
    private mmlitifylrn_Exceptions() {}

    public class AuthenticationsServiceException extends Exception { }

    public class ParameterException extends Exception { }

    public class CalloutException
        extends Exception
    {
        private HttpResponse httpResponse = null;

        public boolean isCalloutMade()
        {
            return this.httpResponse != null;
        }

        public CalloutException( HttpResponse httpResponse )
        {
            this.httpResponse = httpResponse;
        }

        public integer getStatusCode()
        {
            return this.httpResponse == null ? null : this.httpResponse.getStatusCode();
        }

        public string getCalloutErrorMessage()
        {
            return this.httpResponse == null ? getMessage() : mmlitifylrn_Logic.processCalloutErrorMessage( this.httpResponse );
        }
    }

    public class InvalidConditionException extends Exception { }
}