public with sharing class mmwiz_ActionGenerateRecord_WizardAnswer
    extends mmwiz_ActionGenerateNewRecord
{
    public mmwiz_ActionGenerateRecord_WizardAnswer()
    {
        // code
    }

    public override void initialize(Wizard__mdt wiz)
    {
        super.initialize(wiz);

        super.setSObjectType( QuestionAndAnswer__c.SObjectType.getDescribe().getName() );
    }

    private String questionText = null;

    public String getQuestionText()
    {
        return questionText;
    }

    public mmwiz_ActionGenerateRecord_WizardAnswer setQuestionText(String value)
    {
        questionText = value;
        return this;
    }

    private String questionToken = null;

    public String getQuestionToken()
    {
        return questionToken;
    }

    public mmwiz_ActionGenerateRecord_WizardAnswer setQuestionToken(String value)
    {
        questionToken = value;
        return this;
    }

    private String answer = null;

    public String getAnswer()
    {
        return answer;
    }

    public mmwiz_ActionGenerateRecord_WizardAnswer setAnswer(String value)
    {
        answer = value;
        return this;
    }

    private String questionnaireToken = null;

    public String getQuestionnaireToken()
    {
        return questionnaireToken;
    }

    public mmwiz_ActionGenerateRecord_WizardAnswer setQuestionnaireToken(String value)
    {
        questionnaireToken = value;
        return this;
    }

    private Id targetRecordId = null;

    public Id getTargetRecordId()
    {
        return targetRecordId;
    }

    private String sessionGuid = null;

    public String getSessionGuid()
    {
        return sessionGuid;
    }

    public void setSessionGuid(String value)
    {
        sessionGuid = value;
    }

    public mmwiz_ActionGenerateRecord_WizardAnswer setTargetRecordId(Id value)
    {
        targetRecordId = value;
        return this;
    }

    public override SObject generateRecord()
    {
        SObject sobj = super.generateRecord();

        sobj.put(QuestionAndAnswer__c.Intake__c, getTargetRecordId());
        sobj.put(QuestionAndAnswer__c.QuestionnaireToken__c, getQuestionnaireToken());
        sobj.put(QuestionAndAnswer__c.QuestionText__c, getQuestionText());
        sobj.put(QuestionAndAnswer__c.QuestionToken__c, getQuestionToken());
        sobj.put(QuestionAndAnswer__c.Answer__c, getAnswer());
        sobj.put(QuestionAndAnswer__c.SessionGUID__c, getSessionGuid());

        return sobj;
    }
}