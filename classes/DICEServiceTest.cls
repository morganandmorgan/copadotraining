/**
 * DICEServiceTest
 * @description Test for DICE Service class.
 * @author Jeff Watson
 * @date 9/13/2019
 */
@IsTest
public with sharing class DICEServiceTest {

    @IsTest
    private static void coverage() {
        DICEService.coverage();
    }
}