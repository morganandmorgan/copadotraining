@isTest
private class mmdocusign_CreateEnvelopeExtensionTest
{
	@isTest
	static void NormalPath()
	{
		Account client = new Account(
			PersonMobilePhone = '987-654-3210'
		);
		client.Id = fflib_IDGenerator.generate(Account.SObjectType);

		mm_Application.Selector.setMock(new mock_AccountSelector(client));
 
		Intake__c intake = new Intake__c(
			Client__c = client.Id,
			Handling_Firm__c = 'Morgan & Morgan'
		);
		intake.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);


		mm_Application.Service.setMock(mmdocusign_IDocusignRestApiService.class, new mock_DocusignRestApi());

		mmdocusign_CreateEnvelopeExtension ext = new mmdocusign_CreateEnvelopeExtension(intake);
		ApexPages.PageReference pr = ext.execute();

		System.assert(pr != null);
	}

	@isTest
	static void IntakeNotHandledByMorganAndMorgan()
	{
		Account client = new Account(
			PersonMobilePhone = '987-654-3210'
		);
		client.Id = fflib_IDGenerator.generate(Account.SObjectType);

		mm_Application.Selector.setMock(new mock_AccountSelector(client));
        
		Intake__c intake = new Intake__c(
			Client__c = client.Id,
			Client__r = client,
			Handling_Firm__c = 'Nation Law'
		);
		intake.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);


		mm_Application.Service.setMock(mmdocusign_IDocusignRestApiService.class, new mock_DocusignRestApi());

		mmdocusign_CreateEnvelopeExtension ext = new mmdocusign_CreateEnvelopeExtension(intake);

		ApexPages.PageReference pr = ext.execute();

		System.assert(String.isNotBlank(ext.getDisplayText()));
	}

	@isTest
	static void ClientDoesNotHaveMobilePhone()
	{
		Account client = new Account(
			PersonMobilePhone = ''
		);
		client.Id = fflib_IDGenerator.generate(Account.SObjectType);

		mm_Application.Selector.setMock(new mock_AccountSelector(client));
        
		Intake__c intake = new Intake__c(
			Client__c = client.Id,
			Client__r = client,
			Handling_Firm__c = 'Morgan & Morgan'
		);
		intake.Id = fflib_IDGenerator.generate(Intake__c.SObjectType);


		mm_Application.Service.setMock(mmdocusign_IDocusignRestApiService.class, new mock_DocusignRestApi());

		mmdocusign_CreateEnvelopeExtension ext = new mmdocusign_CreateEnvelopeExtension(intake);

		ApexPages.PageReference pr = ext.execute();

		System.assert(String.isNotBlank(ext.getDisplayText()));
	}

	public class mock_DocusignRestApi
		implements mmdocusign_IDocusignRestApiService
	{
		public mmdocusign_CreateEnvelopeResponseData createEnvelope(Intake__c record)
		{
			return
				new mmdocusign_CreateEnvelopeResponseData(mmlib_Utils.generateGuid(), mmlib_Utils.generateGuid());
		}

		public Task createTasksForEnvelope(Intake__c intake, String envelopeId, String clientUserId)
		{
			Task newTask = new Task();
			newTask.Id = fflib_IDGenerator.generate(Task.SObjectType);
			newTask.WhatId = Intake.Id;

			return newTask;
		}

		public String getRecipientLinkToEnvelope(String envelopeId, String clientEmail, String clientName, String clientUserId)
		{
			return null;
		}
	}

	public class mock_AccountSelector
		implements mmcommon_IAccountsSelector
	{
		private Account record = null;

		public mock_AccountSelector(Account acct)
		{
			record = acct;
		}

		public List<Account> selectById( Set<Id> idSet )
		{
			return selectSObjectsById(idSet);
		}

		public List<Account> selectWithFieldsetById(Schema.FieldSet fs, Set<Id> idSet )
		{
			return null;
		}

        public List<Account> selectByFuzzyMatching(mmcommon_IAccountsSelectorPersonRequest request)
        {
            return null;
        }

		public List<Account> selectByPersonFieldRequest( mmcommon_IAccountsSelectorPersonRequest request )
		{
			return null;
		}

		public List<Account> selectTreatmenCentersCloseToLocation(Location originatingLocation, integer distanceInMiles, String treatmentCenterType)
		{
			return null;
		}

        public List<Account> selectByOfficeLocationCode(Set<String> stringSet)
        {
            return null;
        }

		// ----------------------------------

		public Schema.SObjectType sObjectType()
		{
			return Account.SObjectType;
		}

		public List<SObject> selectSObjectsById(Set<Id> idSet)
		{
			return new List<Account> {record};
		}

		public String selectSObjectsByIdQuery()
		{
			return null;
		}
	}
}