public class mmmatter_Lawsuits
//    extends fflib_SObjectDomain
{
//    private Map<Id,Lawsuit__c> existingRecordsIdMap = new Map<Id,Lawsuit__c>();
//
//    public mmmatter_Lawsuits(List<Lawsuit__c> lawsuits)
//    {
//        super(lawsuits);
//    }
//
//    public override void onValidate()
//    {
//        markMultipleLawsuitsPerIntakeAsError();
//    }
//
//    public override void onValidate(Map<Id,SObject> existingRecords)
//    {
//        markMultipleLawsuitsPerIntakeAsError();
//    }
//
//    public override void onAfterInsert()
//    {
//        // Updating related intakes "lawsuit active" property
//        setRelatedIntakeLawsuitActiveProperty();
//    }
//
//    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
//    {
//        this.existingRecordsIdMap = (Map<Id,Lawsuit__c>) existingRecords;
//        // Updating related intakes "lawsuit active" property
//        setRelatedIntakeLawsuitActiveProperty();
//    }
//
//    private void markMultipleLawsuitsPerIntakeAsError()
//    {
//        // Process only the Lawsuits that have been associated with a different Intake.
//        List<Lawsuit__c> lawsuitsHavingIntakeAdded = new List<Lawsuit__c>();
//
//        Lawsuit__c oldLawsuit = null;
//        Lawsuit__c lawsuit = null;
//
//        for (SObject record : this.records)
//        {
//            lawsuit = (Lawsuit__c)record;
//
//            if ( this.existingRecordsIdMap.containsKey(lawsuit.id) )
//            {
//                oldLawsuit = this.existingRecordsIdMap.get(lawsuit.Id);
//            }
//
//            if (oldLawsuit == null || (lawsuit.Intake__c != oldLawsuit.Intake__c && lawsuit.Intake__c != null))
//            {
//                lawsuitsHavingIntakeAdded.add(lawsuit);
//            }
//        }
//
//        // Get a map of proper Lawsuits -- those exclusively related to the Intake.
//        Map<Id, Lawsuit__c> validLawsuitMap =
//            new Map<Id, Lawsuit__c>(
//                (List<Lawsuit__c>)
//                new mmmatter_OneMatterPerIntakeCriteria()
//                .setRecordsToEvaluate(lawsuitsHavingIntakeAdded)
//                .run());
//
//        // Create an SObject error for those that were not contained in the valid Lawsuit structure.
//        for (Lawsuit__c l : lawsuitsHavingIntakeAdded)
//        {
//            // Production data and some legacy tests implement Lawsuits
//            // that are not related to an Intake.
//            if ( ! validLawsuitMap.containsKey(l.Id) && l.Intake__c != null)
//            {
//                l.addError('The operation would result in more than one Lawsuit associated to the Intake.');
//            }
//        }
//    }
//
//    private void setRelatedIntakeLawsuitActiveProperty()
//    {
//        // Criteria not necessary.  Action touches all records.
//        mmintake_SetLawsuitIsActiveAction act = new mmintake_SetLawsuitIsActiveAction();
//        act.setRecordsToActOn( this.records );
//        act.setActionToRunInQueue( true );
//        act.run();
//    }
//
//    public class Constructor
//        implements fflib_SObjectDomain.IConstructable
//    {
//        public fflib_SObjectDomain construct(List<SObject> sObjectList)
//        {
//            return new mmmatter_Lawsuits(sObjectList);
//        }
//    }
}