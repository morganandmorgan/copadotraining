global class IntakeConversionClass {

    webservice static String convertIntake(Id intakeId) {

        String result;
        List<Intake__c> intakeList;
        Set<String> genderCaseTypeSet = new set<String> {'Lipitor', 'Risperdal', 'Invega', 'Talcum Powder', 'Propecia'};
        if(intakeId != NULL) {
           String qryStr = 'SELECT Client__r.Gender__c, Client__r.Name, Client__r.FirstName, Client__r.LastName,Client__r.PersonAssistantName, Client__r.PersonAssistantPhone, Client__r.PersonBirthdate, Client__r.PersonContactId, Caller_Party__r.FirstName, Caller_Party__r.LastName,Client__r.PersonDepartment, Client__r.PersonEmail, Client__r.PersonEmailBouncedDate, Client__r.PersonEmailBouncedReason,';
                   qryStr += 'Client__r.PersonHomePhone, Client__r.PersonLastCURequestDate, Client__r.PersonLastCUUpdateDate, Client__r.PersonLeadSource, Client__r.PersonMailingCity, Client__r.PersonMailingCountry, Client__r.PersonMailingPostalCode, Client__r.PersonMailingState, Client__r.PersonMailingStreet,';
                   qryStr += 'Client__r.PersonMobilePhone, Client__r.PersonOtherCity, Client__r.PersonOtherCountry, Client__r.PersonOtherPhone, Client__r.PersonOtherPostalCode, Client__r.PersonOtherState, Client__r.PersonOtherStreet, Client__r.PersonTitle,';
                   qryStr += 'Client__r.ShippingAddress, Client__r.ShippingCity, Client__r.ShippingCountry, Client__r.ShippingPostalCode, Client__r.ShippingState, Client__r.ShippingStreet, Client__r.Fax, Client__r.PersonMailingAddress,';
                   qryStr += 'Client__r.Maiden_Name__c, Client__r.Mailing_Address_2__c, Client__r.Marital_Status__c, Client__r.Other_Names_Used__c, Client__r.Physical_Address_2__c, Client__r.Social_Security_Number__c, Client__r.Spouse_Partner_Name__c, Client__r.Work_Phone__c, ';

            Map <string, Schema.SObjectField> fieldMap = Schema.SObjectType.Intake__c.fields.getMap();
            for(Schema.SObjectField fld : fieldmap.values()) {
                qryStr += fld + ', ';
            }
            qryStr = qryStr.subString(0, qryStr.lastIndexOf(','));
            qryStr += ' FROM Intake__c WHERE Id = \'' + intakeId + '\'';
            System.debug(':::::::::qryStr:::::::::'+qryStr);
            intakeList = Database.Query(qryStr);
        }

        if(intakeList.size() > 0) {
            Intake__c intakeRcd = intakeList[0];
            System.debug(':::::::intakeRcd:::::'+intakeRcd);
            System.debug(':::::::intakeRcd.Case_Type__c:::::'+intakeRcd.Case_Type__c);

            if(intakeRcd.Status__c != 'Retainer Received') {
                result = 'Error~Intake Status should be "Retainer Received" to convert';
                return result;
            }

            Lawsuit__c LwSt;
            try {
                LwSt = getLawsuit(intakeRcd);
                result = LwSt.Id;
                RecordType rcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Intake__c' AND Name = 'Converted'];
                intakeRcd.Status__c = 'Converted';
                intakeRcd.RecordTypeId = rcdType.Id;
                update intakeRcd;
            } catch(DMLException e) {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    result = 'Error~Please correct the following error(s): \n-'+e.getDmlMessage(i);
                    return result;
                }
            }

            List<Questionnaire__c> questList = [SELECT Id FROM Questionnaire__c WHERE Intake__c = :intakeRcd.Id];
            Questionnaire__c questionnaireRcd;
            if ((questList != NULL && questList.size() > 0)) {
                questionnaireRcd = new Questionnaire__c (Id=questList[0].Id, Lawsuits__c= LwSt.Id);
            }
            else {
            //if(!(questList != NULL && questList.size() > 0)) {

                //String questName = intak.Client__r.Name + '-' + intak.Case_Type__c + '-' + DateTime.now().format('MM/dd/yyyy');
                questionnaireRcd = new Questionnaire__c (
                    Intake__c = intakeRcd.Id,
                    //Questionnaire_Name__c = questName,
                    Status__c = 'New',
                    Lawsuits__c = LwSt.Id,
                    First_Name__c = intakeRcd.Client__r.FirstName,
                    Last_Name__c = intakeRcd.Client__r.LastName,
                    Mailing_Address_1__c = String.valueOf(intakeRcd.Client__r.PersonMailingAddress),
                    Mailing_City__c = intakeRcd.Client__r.PersonMailingCity,
                    Mailing_County__c = intakeRcd.Client__r.PersonMailingCountry,
                    Mailing_State__c = intakeRcd.Client__r.PersonMailingState,
                    Mailing_Zip_Code__c = (intakeRcd.Client__r.PersonMailingPostalCode != NULL) ? Decimal.valueOf(intakeRcd.Client__r.PersonMailingPostalCode) : NULL,
                    Physical_Address_1__c = String.valueOf(intakeRcd.Client__r.ShippingAddress),
                    Physical_City__c = intakeRcd.Client__r.ShippingCity,
                    Physical_County__c = intakeRcd.Client__r.ShippingCountry,
                    Physical_Zip_Code__c = (intakeRcd.Client__r.ShippingPostalCode != NULL) ? Decimal.valueOf(intakeRcd.Client__r.ShippingPostalCode) : NULL,
                    Physical_State__c = intakeRcd.Client__r.ShippingState,
                    Home_Phone__c = intakeRcd.Client__r.PersonHomePhone,
                    Mobile_Phone__c = intakeRcd.Client__r.PersonMobilePhone,
                    Fax_Number__c = intakeRcd.Client__r.Fax,
                    Email__c = intakeRcd.Client__r.PersonEmail,
                    Date_of_Birth__c = intakeRcd.Client__r.PersonBirthdate,
                    Party_calling_different_than_client__c =  intakeRcd.Caller_different_than_Injured_party__c,
                    //Caller_First_Name__c = intakeRcd.Caller_Party__r.FirstName,
                    //Caller_Last_Name__c = intakeRcd.Caller_Party__r.LastName,
                    Caller_First_Name__c = intakeRcd.Caller_First_Name__c,
                    Caller_Last_Name__c = intakeRcd.Caller_Last_Name__c,
                    Caller_relationship_to_client__c = intakeRcd.Injured_party_relationship_to_Caller__c,
                    Injured_party_deceased__c = intakeRcd.Injured_party_deceased__c,
                    Client__c = intakeRcd.Client__c,
                    //Caller__c = intakeRcd.Caller_Party__c,
                    Maiden_Name__c = intakeRcd.Client__r.Maiden_Name__c,
                    Mailing_Address_2__c = intakeRcd.Client__r.Mailing_Address_2__c,
                    Marital_Status__c = intakeRcd.Client__r.Marital_Status__c,
                    Other_Names_Used__c = intakeRcd.Client__r.Other_Names_Used__c,
                    Physical_Address_2__c = intakeRcd.Client__r.Physical_Address_2__c,
                    Social_Security__c = (intakeRcd.Client__r.Social_Security_Number__c != NULL) ? intakeRcd.Client__r.Social_Security_Number__c : NULL,
                    Spouse_Partner_Name__c = intakeRcd.Client__r.Spouse_Partner_Name__c,
                    Work_Phone__c = intakeRcd.Client__r.Work_Phone__c

                );
            }
            try {

                upsert questionnaireRcd;

                /// Code to Update comments with questionarrie id

                List<Comments__c> commlist = new list<Comments__c>();
                if(intakeId != null){
                    for(Comments__c comm:[select Questionnaire__c from Comments__c where Intake__c =:intakeId]){
                        comm.Questionnaire__c = questionnaireRcd.id;
                        commlist.add(comm);
                    }
                }
                if(commlist.size() > 0)
                upsert commlist;

            } catch(DMLException e) {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    result = 'Error~Please correct the following error(s): \n-'+e.getDmlMessage(i);
                    system.debug('Error~Please correct the following error(s):'+e.getDmlMessage(i));
                    return result;
                }
            }
        }
        return result;
    }

    private static Lawsuit__c getLawsuit(Intake__c intakeRcd) {
        List<Lawsuit__c> existingLawsuits = [
            SELECT Id,
                Name,
                AccountId__c,
                Intake__c
            FROM
                Lawsuit__c
            WHERE
                Intake__c = :intakeRcd.Id
            ORDER BY
                CreatedDate DESC
            LIMIT 1
        ];

        if (!existingLawsuits.isEmpty()) {
            return existingLawsuits.get(0);
        }

        Lawsuit__c LwSt = new Lawsuit__c(
                AccountId__c = intakeRcd.Client__c,
                Name = intakeRcd.Client__r.Name,
                Intake__c = intakeRcd.Id
            );
        Database.insert(LwSt);
        return LwSt;
    }
}