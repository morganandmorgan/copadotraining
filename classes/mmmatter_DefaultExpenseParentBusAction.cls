/**
 *  Domain action used to default the litify_pm__Expense__c records ParentBusiness__c field with the parent matter's AssignedToMMBusiness__c value.
 */
public class mmmatter_DefaultExpenseParentBusAction
    extends mmlib_AbstractAction
{
    public override Boolean runInProcess()
    {
        Set<Id> parentMatterIdSet = mmlib_Utils.generateIdSetFromField( this.records, litify_pm__Expense__c.litify_pm__Matter__c );

        map<Id, litify_pm__Matter__c> mattersByIdMap = new map<Id, litify_pm__Matter__c>( mmmatter_MattersSelector.newInstance().selectById( parentMatterIdSet ) );

        for (SObject record : this.records)
        {
            litify_pm__Expense__c expense = (litify_pm__Expense__c) record;

            //system.debug( 'record matter ' + expense.litify_pm__Matter__c);
            //if ( mattersByIdMap.containsKey( expense.litify_pm__Matter__c) )
            //{
            //    system.debug( 'mattersByIdMap.containsKey( expense.litify_pm__Matter__c ) == true' );
            //    system.debug( 'mattersByIdMap.get( expense.litify_pm__Matter__c ).AssignedToMMBusiness__c == ' + mattersByIdMap.get( expense.litify_pm__Matter__c ).AssignedToMMBusiness__c);
            //}

            if ( mattersByIdMap.containsKey( expense.litify_pm__Matter__c )
                && mattersByIdMap.get( expense.litify_pm__Matter__c ).AssignedToMMBusiness__c != null
                )
            {
                expense.ParentBusiness__c = mattersByIdMap.get( expense.litify_pm__Matter__c ).AssignedToMMBusiness__c;
                if ( this.uow != null )
                {
                    this.uow.registerDirty( expense );
                }
            }
        }

        return true;
    }
}