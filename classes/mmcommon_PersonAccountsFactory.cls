public class mmcommon_PersonAccountsFactory
{
    private fflib_ISObjectUnitOfWork uow = null;

    private Account person = null;

    private map<Schema.sObjectField, string> fieldParameterMap = new map<Schema.sObjectField, string>();

    public mmcommon_PersonAccountsFactory( )
    {
    }

    public mmcommon_PersonAccountsFactory( fflib_ISObjectUnitOfWork uow )
    {
        this();

        this.uow = uow;
    }

    public mmcommon_PersonAccountsFactory with( map<Schema.sObjectField, string> fieldParameterMap )
    {
        if ( fieldParameterMap != null )
        {
            this.fieldParameterMap.putAll( fieldParameterMap );
        }

        return this;
    }

    public Account generate()
    {
        if (this.uow == null )
        {
            throw new FactoryException('mmcommon_PersonAccountsFactory.uow was not populated.');
        }

        this.person = new Account();

        for ( Schema.SObjectField accountField : fieldParameterMap.keyset() )
        {
            // try to put the Schema.SObjectField on to the Account object
            try
            {
                this.person.put( accountField, mmlib_Utils.convertValToDisplayType( accountField.getDescribe().getType(), fieldParameterMap.get( accountField ) ) );
            }
            catch ( System.SObjectException soe )
            {
                // if this occurs, then there is another SObject type's field in the list
                //   which is fine.  We simply will not use it here and we will do nothing
                //   with the exception.
            }
        }

        if ( this.person.RecordTypeId == null )
        {
            this.person.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName( Account.SOBjectType, 'Person_Account' );
        }

        uow.registerNew( this.person );

        return this.person;
    }

    public class FactoryException extends Exception { }
}