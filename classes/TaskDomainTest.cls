/**
 * TaskDomainTest
 * @description Test for TaskDomain class.
 * @author Jeff Watson
 * @date 8/22/2019
 */
@IsTest
public with sharing class TaskDomainTest {

    @IsTest
    private static void taskTrigger() {
        Test.startTest();
        insert new Task(Subject = 'Email: 1 Hour Retainer Received - with Link');
        Test.stopTest();
    }

    @IsTest
    private static void ctor() {
        Test.startTest();
        TaskDomain taskDomain = new TaskDomain();
        System.assert(taskDomain != null);
        taskDomain = new TaskDomain(new List<Task>());
        System.assert(taskDomain != null);
        Test.stopTest();
    }

    @IsTest
    private static void onBeforeInsert() {
        Test.startTest();
        Task task = TestUtil.createTask();
        task.WhoId = null;
        insert task;
        Test.stopTest();
    }

    @IsTest
    private static void coverage() {
        Test.startTest();
        TaskDomain.coverage();
        Test.stopTest();
    }
}