@isTest
private class DICE_DiceQueueGroupControllerTest {

    @isTest
    static void test_happyPath() {
    	RecordType rt = [select Id from RecordType where DeveloperName='Person_Account' limit 1];

        //customer 1
        Account acct = new Account();
        acct.RecordTypeId = rt.Id;
        acct.LastName = 'Dent';
        acct.NextCallDateTime__c = null;
        acct.Last_Call_Date_Time__c = null;
        insert acct;

        Intake__c intake = new Intake__c();
        intake.Client__c = acct.Id;
        intake.Number_Outbound_Dials_Intake__c = 0;
        intake.Number_Outbound_Dials_Status__c = 0;
        intake.Last_Dial_Time__c = null;
        intake.litigation__c = 'MASS TORT';
        intake.Phone__c = '710-111-1111';
        intake.InputChannel__c = 'Attorney Portal';
        insert intake;
        
        //customer 2
        Account acct2 = new Account();
        acct2.RecordTypeId = rt.Id;
        acct2.LastName = 'Ron';
        acct2.NextCallDateTime__c = null;
        acct2.Last_Call_Date_Time__c = null;
        insert acct2;

        Intake__c intake2 = new Intake__c();
        intake2.Client__c = acct2.Id;
        intake2.Number_Outbound_Dials_Intake__c = 0;
        intake2.Number_Outbound_Dials_Status__c = 0;
        intake2.Last_Dial_Time__c = null;
        intake2.litigation__c = 'MASS TORT';
        intake2.Phone__c = '710-111-1113';
        intake2.InputChannel__c = 'Attorney Portal';
        insert intake2;
        
        system.assertEquals(
            2,
            [SELECT id FROM Intake__c].size(),
            'Incorrect Intake size'
        );
        
        //Dice Queue 1
        DICE_Queue__c dq1 = new DICE_Queue__c(
            				Name='Test Queue One', 
            				Query__c = '[]', 
            				Sorting__c = '[]', 
            				EarliestCallTime__c = 7,
            				LatestCallTime__c = 7
        );
        insert dq1;
        
        //Dice Queue 2
        DICE_Queue__c dq2 = new DICE_Queue__c(
            				Name='Test Queue Two', 
            				Query__c = '[]', 
            				Sorting__c = '[]', 
            				EarliestCallTime__c = 7,
            				LatestCallTime__c = 7
        );
        insert dq2;
        
        //Dice Queue Group
        DICE_Queue_Group__c dqg = new DICE_Queue_Group__c(
            name = 'Main Group'
        );
       	insert dqg;
        
        //Junction 1
        DICE_Queue_Group_Junction__c j1 = new DICE_Queue_Group_Junction__c(
            DICE_Queue__c = dq1.id,
            DICE_Queue_Group__c = dqg.id
        );
        insert j1;
        
        //Junction 1
        DICE_Queue_Group_Junction__c j2 = new DICE_Queue_Group_Junction__c(
            DICE_Queue__c = dq2.id,
            DICE_Queue_Group__c = dqg.id
        );
        insert j2;
        
        //test
        Test.startTest();
        
        	DICE_DiceQueueGroupController controller = new DICE_DiceQueueGroupController(
				new ApexPages.StandardController(dqg)
            );
        
        Test.stopTest();
        
        //asserts
        System.assertNotEquals(
            null,
            controller.queues,
            'Wrong queues container'
        );

		//check queues container        
        System.assertNotEquals(
            0,
            controller.queues.size(),
            'Wrong queues container size'
        );
        
        System.assertEquals(
            2,
            controller.queues.size(),
            'Wrong queues container size'
        );
        
        //check queues names
        System.assertEquals(
            dq1.Name,
            controller.queues[0].name,
            'Wrong queues name'
        );
        
        System.assertEquals(
            dq2.Name,
            controller.queues[1].name,
            'Wrong queues name'
        );
        
        //check queues count
        System.assertEquals(
            2,
            controller.queues[0].count,
            'Wrong queues count #1'
        );
        
        System.assertEquals(
            2,
            controller.queues[1].count,
            'Wrong queues count #2'
        );
        
    }
}