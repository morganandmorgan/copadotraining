/*============================================================================/
* FinanceRequest_Service
* @description Service class for Finance Requests
* @author CLD
* @date 5/15/2019
=============================================================================*/
public class FinanceRequest_Service {

    // Variable Declaration
    Id checkRequestRecordTypeId;
    Id currentCompanyId;

    // Ctors
    public FinanceRequest_Service() {

        //get the check request record type Id
        checkRequestRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Finance_Request__c.SObjectType, 'Cost_Check_Request');

        //get the users current company:
        List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if (companies != null && !companies.isEmpty()) {
            currentCompanyId = companies.get(0).Id;
        }
    }

    /*-----------------------------------------------------------------------------
    Method to create Expenses from Finance Request Approval
    -----------------------------------------------------------------------------*/
    public void createExpensesFromCheckRequest(List<Finance_Request__c> newTrigger, Map<Id, Finance_Request__c> oldTriggerMap) {
        Set<Id> matterIds = new Set<Id>();

        List<Finance_Request__c> checkRequestsToProcess = new List<Finance_Request__c>();
        List<litify_pm__Expense__c> newExpenseList = new List<litify_pm__Expense__c>();

        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();

        //extract the subset of finance request records
        for (Finance_Request__c fr : newTrigger) {
            Finance_Request__c fr_old = oldTriggerMap.get(fr.Id);
            if (fr.RecordTypeId == checkRequestRecordTypeId && fr.Approved_Date__c != null && fr_old.Approved_Date__c == null) {
                checkRequestsToProcess.add(fr);
            }
        }

        //loop to get the related matter info:
        for (Finance_Request__c fr : checkRequestsToProcess) {
            matterIds.add(fr.Matter__c);
        }

        //query the related matter:
        for (litify_pm__Matter__c m : matterSelector.selectById(matterIds)) {
            matterMap.put(m.Id, m);
        }

        //loop to create the expense records
        for (Finance_Request__c fr : checkRequestsToProcess) {
            litify_pm__Expense__c newExpense = new litify_pm__Expense__c(
                    litify_pm__Amount__c = fr.Check_Amount__c,
                    litify_pm__Date__c = Date.today(),
                    litify_pm__Matter__c = fr.Matter__c,
                    Auto_Create_PIN__c = true,
                    Finance_Request__c = fr.Id,
                    ParentBusiness__c = matterMap.containsKey(fr.Matter__c) ? matterMap.get(fr.Matter__c).AssignedToMMBusiness__c : null,
                    PayableTo__c = fr.Vendor_Account__c,
                    RequestedBy__c = fr.CreatedById,
                    Vendor_Invoice_Number__c = fr.Invoice_Number__c,
                    litify_pm__ExpenseType2__c = fr.Expense_Type__c
            );
            newExpenseList.add(newExpense);
        }

        //Wrap the insert expense call in Try Catch to rollback any errors
        Savepoint sp1 = Database.setSavepoint();
        try {
            //inserting this expense should create a new Payable Invoice as well
            insert newExpenseList;
        } catch (Exception e) {
            System.debug('ERROR - ' + e.getMessage() + e.getStackTraceString());
            Database.rollback(sp1);
            throw e;
        }
    }

    /*-----------------------------------------------------------------------------
    Method to create Trust Transactions on related matter
    -----------------------------------------------------------------------------*/
    public void validateCurrentCompany(List<Finance_Request__c> newTrigger, Map<Id, Finance_Request__c> oldTriggerMap) {

        System.debug('FinanceRequest_Service - validateCurrentCompany - ENTER METHOD');
        Set<Id> matterIds = new Set<Id>();
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();
        mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();

        //loop through the Finance Requests and get the matter Ids:
        for (Finance_Request__c fr : newTrigger) {
            matterIds.add(fr.Matter__c);
        }
        for (litify_pm__Matter__c m : matterSelector.selectById(matterIds)) {
            matterMap.put(m.Id, m);
        }

        //loop through the Finance Requests and perform the validation
        for (Finance_Request__c fr : newTrigger) {
            Finance_Request__c fr_old = oldTriggerMap.get(fr.Id);
            Id matterCompanyId = matterMap.containsKey(fr.Matter__c) ? matterMap.get(fr.Matter__c).AssignedToMMBusiness__r.FFA_Company__c : null;

            if (fr.RecordTypeId == checkRequestRecordTypeId && fr.Approved_Date__c != null && fr_old.Approved_Date__c == null) {

                if (currentCompanyId == null || matterCompanyId == null || currentCompanyId != matterCompanyId) {
                    fr.addError('In order to approve this finance request, your current company must match the company defined on the Matter');
                }
            }
        }
    }
}