/************************************************************************************************************************
// Name         BankFileGenerationCompanyController
// Description  Controller extension for the BankFileGenerationCompany Visualforce page
// Revisions    2019-Jun-02  bkrynitsky@cldpartners.com     Initial version
************************************************************************************************************************/
public without sharing class BankFileGenerationCompanyController   
{
    /************************************************************************************************************
    // Constants
    *************************************************************************************************************/
    private static final string ERROR_PAYMENT_STATUS = 'Note: You must first generate the Payment Media before you can generate a bank file';
    private static final Set<String> VALID_PAYMENT_STATUSES = new Set<String>{'Media Prepared','Matched','Ready to Post','Part Canceled'};
    private static final string INFO_FILE_GENERATED = 'File generated and attached to the payment.';
    private static final string FILE_ERROR_ENCOUNTERED = 'No Data Found for parameters selected, please try again';


    /************************************************************************************************************
    // Properties
    *************************************************************************************************************/
    public c2g__codaCompany__c companyObj  {get;set;}
	public c2g__codaCashEntry__c dummyStart{get;set;}
	public c2g__codaCashEntry__c dummyEnd  {get;set;}
    public id docTemplateId                {get;set;}
    public id bankAccountId                {get;set;}
    public string loadingMessage           {get;set;}
    public boolean loadError               {get;set;}
    public boolean fileError               {get;set;}
    public boolean disableProcessButton    {get;set;}

    /************************************************************************************************************
    // Name         BankFileGenerationCompanyController
    // Description  Constructor
    *************************************************************************************************************/
    public BankFileGenerationCompanyController(ApexPages.StandardController controller){
        this.companyObj = (c2g__codaCompany__c)controller.getRecord(); 
		dummyStart = new c2g__codaCashEntry__c();
		dummyEnd = new c2g__codaCashEntry__c();
        loadingMessage = 'Please wait';
        loadError = false;
        fileError = false;
        disableProcessButton = false;
        
        // string errMessage = validatePayment(companyObj);
        
        // if (loadError == true){
        //     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, errMessage));
        // }       
    }


    /************************************************************************************************************
    // Name         validatePayment
    // Description  Validates whether the page should load; called from the constructor
    *************************************************************************************************************/
    // public string validatePayment(c2g__codaCompany__c pmt){
    //     string returnString = '';

    //     pmt = [SELECT Id, 
    //                   c2g__Status__c 
    //              FROM c2g__codaCompany__c 
    //             WHERE id = :pmt.id];

    //         loadError = VALID_PAYMENT_STATUSES.contains(pmt.c2g__Status__c) == false && Test.isRunningTest() == false ? true : loadError;
    //         returnString = VALID_PAYMENT_STATUSES.contains(pmt.c2g__Status__c) == false && Test.isRunningTest() == false ? ERROR_PAYMENT_STATUS : returnString;

    //     return returnString;
    // }


    /************************************************************************************************************
    // Name         generateFile
    // Description  Event handler for the Process button
    *************************************************************************************************************/
    public void generateFile(){
        try{
			system.debug('generateFile - companyObj.id = ' + companyObj.id);
            BankFileHandler fileHandler = new BankFileHandler(null, companyObj.id, dummyStart.c2g__Date__c, dummyEnd.c2g__Date__c, bankAccountId, docTemplateId);
            fileError = fileHandler.renderBankFile();
            system.debug('generateFile - fileError = ' + fileError);
            disableProcessButton = true;
            if(fileError == false){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, INFO_FILE_GENERATED));
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, FILE_ERROR_ENCOUNTERED));   
            }
        }
        catch (Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + '\n Line: '+e.getLineNumber() +'\n Cause:'+e.getCause())); 
        }
    }

    /************************************************************************************************************
    // Name         getDocOptions
    // Description  Returns the picklist options for document template
    *************************************************************************************************************/
    public list<SelectOption> getDocOptions(){
        list<SelectOption> docOptions = new list<SelectOption> ();

        // Start with first option of none:
        docOptions.add(new selectOption('', '- None -'));

        for (Doc_Generation_Template__c docTemp : [SELECT Id,
                                                          Name
                                                     FROM Doc_Generation_Template__c
                                                 ORDER BY Name ASC]){
            if (docTemplateId == null){
                docTemplateId = docTemp.id;
            }
            docOptions.add(new SelectOption(docTemp.id, docTemp.Name));
        }

        return docOptions;
    }

    /************************************************************************************************************
    // Name         getBankAccountOptions
    // Description  Returns the picklist options for Bank Accounts to run on
    *************************************************************************************************************/
    public list<SelectOption> getBankAccountOptions(){
        list<SelectOption> bankAccountOptions = new list<SelectOption> ();

        for (c2g__codaBankAccount__c bankAccount : [SELECT Id,
                                                          Name
                                                     FROM c2g__codaBankAccount__c
                                                     WHERE c2g__OwnerCompany__c = :companyObj.Id
                                                 ORDER BY Name ASC]){
            if (bankAccountId == null){
                bankAccountId = bankAccount.id;
            }
            bankAccountOptions.add(new SelectOption(bankAccount.id, bankAccount.Name));
        }

        return bankAccountOptions;
    }

    /************************************************************************************************************
    // Name         backToCompany
    // Description  Returns a page reference to the payment record
    *************************************************************************************************************/
    public PageReference backToCompany(){
        PageReference ref = new PageReference('/' + companyObj.Id); 
        return ref; 
    }

}