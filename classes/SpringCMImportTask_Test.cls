/**
 * SpringCMImportTask_Test
 * @description Test for Spring CM Import Task class.
 * @author Jeff Watson
 * @date 5/5/2019
 */
@IsTest
public with sharing class SpringCMImportTask_Test {

    @IsTest
    private static void ctor() {

        Test.startTest();
        SpringCMImportTask springCMImportTask = new SpringCMImportTask();
        springCMImportTask.Folder = new SpringCMFolder();
        springCMImportTask.Href = '';
        springCMImportTask.Message = '';
        springCMImportTask.Status = '';
        Test.stopTest();
    }
}