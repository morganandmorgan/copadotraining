@isTest
private class mmcommon_PersonUtilsTest {

  @isTest
  private static void userIsInvestigator_TerritorySet() {
    String territory = 'test territory';
    User testUser = new User(
        Territory__c = territory
      );

    Test.startTest();
    Boolean result = mmcommon_PersonUtils.isInvestigator(testUser);
    Test.stopTest();

    System.assert(result);
  }

  @isTest
  private static void userIsInvestigator_TerritoryNull() {
    String territory = null;
    User testUser = new User(
        Territory__c = territory
      );

    Test.startTest();
    Boolean result = mmcommon_PersonUtils.isInvestigator(testUser);
    Test.stopTest();

    System.assert(!result);
  }
}