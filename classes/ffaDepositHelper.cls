/*=======================================================================================
Name            : ffaDepositHelper
Author          : CLD
Created Date    : Aug 2018
Description     : Contains various methods to help with Deposits.
Revisions       : 08-09-2018 - CLD-RW   
                : Changed query logic to only pull in deposits that are of type Social Security.
                : 08-24-2018 - CLD-RW
                : Added the ability to reverse a deposit allocation
=======================================================================================*/
global class ffaDepositHelper 
{
    /* public static void recordDeposits(List<Id> depositIds)
    {
        Set<Id> matterIds = new Set<Id>();
        List<Deposit__c> depositsToUpdate = new List<Deposit__c>();
        List<Expense_to_Deposit__c> newEtDList = new List<Expense_to_Deposit__c>();
        Map<Id, litify_pm__Expense__c> expensesToUpdate = new Map<Id, litify_pm__Expense__c>();

        // Get the list of all matter IDs associated with the deposits first.
        for(Deposit__c deposit : [SELECT Id, Matter__c FROM Deposit__c WHERE Id IN :depositIds
            AND Deposit_Allocated__c = FALSE
            AND RecordType.Name in ('Operating','Trust Payout - Costs')]){
            matterIds.add(deposit.Matter__c);
        }

        // Now pull in all of the matter info we need to update (Expenses, Matters, and Deposits)
        for(litify_pm__Matter__c matter : [
            SELECT Id,
             RecordType.Name,
                (SELECT Id,
                 Amount__c,
                 Source__c,
                 Hard_Cost_Limit__c,
                 Allocated_Fee__c,
                 Unallocated_Cost__c,
                 Soft_Cost_Limit__c,
                 Deposit_Allocated__c FROM Revenues__r 
                WHERE Deposit_Allocated__c = false 
                AND Id in :depositIds
                AND RecordType.Name in ('Operating','Trust Payout - Costs')
                ORDER BY Amount__c, Check_Date__c ),
                (SELECT Id,
                 litify_pm__Amount__c,
                 Amount_Recovered_Settlement__c,
                 Amount_Recovered_Client__c,
                 Amount_Remaining_Client__c,
                 Amount_Remaining_Settlement__c,
                 CostType__c,
                 Fully_Recovered__c,
                 litify_pm__Status__c 
            FROM litify_pm__Expenses__r 
            WHERE ((Amount_Remaining_Settlement__c != null AND Amount_Remaining_Settlement__c > 0) OR (Amount_Remaining_Client__c != null AND Amount_Remaining_Client__c > 0))
            AND Don_t_Recover_This_Cost__c = FALSE 
            AND Don_t_Recover_This_Cost__c = FALSE
            AND litify_pm__ExpenseType2__r.Exclude_from_Allocation__c = false 
            ORDER BY CostType__c, litify_pm__Date__c)
            FROM litify_pm__Matter__c 
            WHERE Id IN :matterIds]){
            // First loop through all of the expenses and categorize them by CostType__c
            Map<String, List<litify_pm__Expense__c>> costTypeToExpenseMap = new Map<String, List<litify_pm__Expense__c>>();
            for(litify_pm__Expense__c expense : matter.litify_pm__Expenses__r){
                if (costTypeToExpenseMap.get(expense.CostType__c) == null){
                    costTypeToExpenseMap.put(expense.CostType__c, new List<litify_pm__Expense__c>{expense}); 
                }
                else{
                    List<litify_pm__Expense__c> expList = costTypeToExpenseMap.get(expense.CostType__c);    
                    expList.add(expense);
                    costTypeToExpenseMap.put(expense.CostType__c, expList); 
                }
            }

            // Now go Loop through the deposits and peform the allocations
            for(Deposit__c deposit : matter.Revenues__r){
                system.debug('IM HERE!');
                Decimal definedFeeAmount = deposit.Allocated_Fee__c != null ? deposit.Allocated_Fee__c : 0; 
                Decimal unallocatedCostAmount = deposit.Unallocated_Cost__c != null ? deposit.Unallocated_Cost__c : 0;
                Decimal remainingAmount = deposit.Amount__c - definedFeeAmount - unallocatedCostAmount;
                Decimal feeAmount = remainingAmount;
                Decimal totalSoftCost = 0;
                Decimal totalHardCost = 0;

                // Recover all hard costs first, then soft costs.
                // The leftover is used for fee.

                //if the expense is already been recovered from settlement and this is just a fee we'll update the recovered amount only

                Boolean isSourceClient = false;
                if (deposit.Source__c == 'Client' || deposit.Source__c == 'Operating'){
                    isSourceClient = true;
                }

                // HARD COSTS
                if(costTypeToExpenseMap.containsKey('HardCost')){
                    Decimal hcLimit = deposit.Hard_Cost_Limit__c != null && deposit.Hard_Cost_Limit__c != 0 ? deposit.Hard_Cost_Limit__c : null;
                    Decimal remainingHC = hcLimit != null ? hcLimit : remainingAmount;

                    System.debug('HC: ' + costTypeToExpenseMap.get('HardCost'));
                    for(litify_pm__Expense__c expense : costTypeToExpenseMap.get('HardCost')){
                        //set the values to 0 if for some reason they are null:
                        expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
                        expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
                        expense.Amount_Written_Off__c = expense.Amount_Written_Off__c == null ? 0 : expense.Amount_Written_Off__c;

                        if (expensesToUpdate.get(expense.Id) != null){
                            expense = expensesToUpdate.get(expense.Id);
                        }

                        // Check to verify that there is enough money left to pay off the expense.
                        Decimal currentExpenseAmount = isSourceClient == false ? (expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Amount_Written_Off__c).setScale(2) : (expense.litify_pm__Amount__c - expense.Amount_Recovered_Client__c - expense.Amount_Written_Off__c).setScale(2);

                        system.debug('**** ffaDepositHelper - expense.Amount = ' +expense.litify_pm__Amount__c);
                        system.debug('**** ffaDepositHelper - remainingAmount = ' +remainingAmount);
                        system.debug('**** ffaDepositHelper - currentExpenseAmount = ' +currentExpenseAmount);
                        system.debug('**** ffaDepositHelper - isSourceClient = ' +isSourceClient);

                        Expense_to_Deposit__c expenseToDeposit = new Expense_to_Deposit__c();
                        expenseToDeposit.Expense__c = expense.Id;
                        expenseToDeposit.Deposit__c = deposit.Id;
                        expenseToDeposit.Amount_Allocated__c = 0;    

                        if (remainingHC >= currentExpenseAmount && currentExpenseAmount > 0){
                            //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                            Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : currentExpenseAmount;

                            expense.Fully_Recovered__c = true;
                            system.debug('**** ffaDepositHelper - expense.Fully_Recovered__c = ' +expense.Fully_Recovered__c);

                            if (isSourceClient){
                                expense.Amount_Recovered_Client__c += currentExpenseAmount;
                            }
                            else{
                                expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
                            }

                            totalHardCost += feeReductionAmount;
                            remainingAmount -= currentExpenseAmount;
                            remainingHC -= currentExpenseAmount;
                            feeAmount -= feeReductionAmount;
                            expense.litify_pm__Status__c = 'Paid';

                            expenseToDeposit.Amount_Allocated__c = currentExpenseAmount;
                        }
                        else if (currentExpenseAmount > 0){
                            //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                            Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : remainingHC;

                            if (isSourceClient){
                                expense.Amount_Recovered_Client__c += remainingHC;
                            }
                            else{
                                expense.Amount_Recovered_Settlement__c += remainingHC;
                            }

                            expenseToDeposit.Amount_Allocated__c = remainingHC;

                            totalHardCost += feeReductionAmount;
                            feeAmount -= feeReductionAmount;
                            remainingAmount = 0;
                            remainingHC = 0;
                        }
                        expensesToUpdate.put(expense.Id, expense);
                        if(expenseToDeposit.Amount_Allocated__c != 0){
                            newEtDList.add(expenseToDeposit);    
                        }
                    }
                }
                
                // SOFT COSTS
                if(costTypeToExpenseMap.containsKey('SoftCost')){
                    Decimal scLimit = deposit.Soft_Cost_Limit__c != null && deposit.Soft_Cost_Limit__c != 0 ? deposit.Soft_Cost_Limit__c : null;
                    Decimal remainingSC = scLimit != null ? scLimit : remainingAmount;

                    System.debug('SC: ' + costTypeToExpenseMap.get('SoftCost'));
                    for(litify_pm__Expense__c expense : costTypeToExpenseMap.get('SoftCost')){
                        //set the values to 0 if for some reason they are null:
                        expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
                        expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
                        expense.Amount_Written_Off__c = expense.Amount_Written_Off__c == null ? 0 : expense.Amount_Written_Off__c;
                        
                        if (expensesToUpdate.get(expense.Id) != null)
                            expense = expensesToUpdate.get(expense.Id);

                        // Check to verify that there is enough money left to pay off the expense.
                        Decimal currentExpenseAmount = isSourceClient == false ? (expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Amount_Written_Off__c).setScale(2) : (expense.litify_pm__Amount__c - expense.Amount_Recovered_Client__c - expense.Amount_Written_Off__c).setScale(2);

                        system.debug('**** ffaDepositHelper - expense.Amount = ' +expense.litify_pm__Amount__c);
                        system.debug('**** ffaDepositHelper - remainingAmount = ' +remainingAmount);
                        system.debug('**** ffaDepositHelper - currentExpenseAmount = ' +currentExpenseAmount);
                        system.debug('**** ffaDepositHelper - isSourceClient = ' +isSourceClient);
                        
                        Expense_to_Deposit__c expenseToDeposit = new Expense_to_Deposit__c();
                        expenseToDeposit.Expense__c = expense.Id;
                        expenseToDeposit.Deposit__c = deposit.Id;
                        expenseToDeposit.Amount_Allocated__c = 0;

                        if (remainingSC >= currentExpenseAmount && currentExpenseAmount > 0){
                            //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                            Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : currentExpenseAmount;

                            expense.Fully_Recovered__c = true;
                            system.debug('**** ffaDepositHelper - expense.Fully_Recovered__c = ' +expense.Fully_Recovered__c);

                            if (isSourceClient){
                                expense.Amount_Recovered_Client__c += currentExpenseAmount;
                            }
                            else{
                                expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
                            }

                            totalSoftCost += feeReductionAmount;
                            remainingAmount -= currentExpenseAmount;
                            remainingSC -= currentExpenseAmount;
                            feeAmount -= feeReductionAmount;
                            expense.litify_pm__Status__c = 'Paid';
                            expenseToDeposit.Amount_Allocated__c = currentExpenseAmount;

                        }
                        else if (currentExpenseAmount > 0){
                            //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                            Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : remainingSC;

                            if (isSourceClient){
                                expense.Amount_Recovered_Client__c += remainingSC;
                            }
                            else{
                                expense.Amount_Recovered_Settlement__c += remainingSC;
                            }

                            totalSoftCost += feeReductionAmount;
                            expenseToDeposit.Amount_Allocated__c = remainingSC;
                            remainingAmount = 0;
                            remainingSC = 0;
                            feeAmount -= feeReductionAmount;
                        }
                        expensesToUpdate.put(expense.Id, expense);
                        if(expenseToDeposit.Amount_Allocated__c != 0){
                            newEtDList.add(expenseToDeposit);    
                        }
                    }
                }

                deposit.Deposit_Allocated__c = true;
                deposit.Allocated_Soft_Cost__c = totalSoftCost;
                deposit.Allocated_Hard_Cost__c = totalHardCost;
                deposit.Allocated_Fee__c = feeAmount;

                System.debug('Deposit Helper : deposit.Allocated_Soft_Cost__c = ' + deposit.Allocated_Soft_Cost__c);
                System.debug('Deposit Helper : deposit.Allocated_Hard_Cost__c = ' + deposit.Allocated_Hard_Cost__c);
                System.debug('Deposit Helper : deposit.Allocated_Fee__c = ' + deposit.Allocated_Fee__c);
                depositsToUpdate.add(deposit);
            }
        }

        Savepoint sp1 = Database.setSavepoint();
		Try{
            UPDATE depositsToUpdate;
            UPDATE expensesToUpdate.values();
            INSERT newEtDList;
        }
        Catch(Exception e){
			system.debug('ERROR - '+e.getMessage() + e.getStackTraceString());
			Database.rollback(sp1);
		}
    } */

    /**
     * Reverses the allocation of a deposit. 
     * Includes deleting the expense to deposit records, clearing the 
     * flags and statuses on the expenses, and clearing the 
     * flags and amounts on the deposit record. 
    */
    public static void reverseDeposits(List<Id> depositIds, Date journalDate){
        List<Deposit__c> depositUpdate = new List<Deposit__c>();
        Map<String, litify_pm__Expense__c> expenseUpdateMap = new Map<String, litify_pm__Expense__c>();
        List<Expense_to_Deposit__c> joinDeleteList = new List<Expense_to_Deposit__c>();
        ProcessDepositsController.JournalRequest jRequest = new ProcessDepositsController.JournalRequest();

        List<Deposit__c> depositList = [Select Id, 
                                    Deposit_Allocated__c,
                                    Deposit_Created__c,
                                    Allocated_Hard_Cost__c,
                                    Allocated_Soft_Cost__c,
                                    Allocated_Fee__c,
                                    Journal__c,
                                    Journal__r.c2g__JournalDate__c,
                                    Source__c,
                                    AssignedToMMBusiness__r.FFA_Company__c,
                                    (SELECT Id, 
                                     Expense__c, 
                                     Amount_Allocated__c FROM Expense_to_Deposits__r)
                              FROM Deposit__c
                              WHERE Id IN :depositIds];

        Set<Id> expenseIds = new Set<Id>();
        for(Deposit__c deposit : depositList){
            for(Expense_to_Deposit__c expToDep : deposit.Expense_to_Deposits__r){
                expenseIds.add(expToDep.Expense__c);
            }
        }

        Map<Id, litify_pm__Expense__c> idToExpenseMap = new Map<Id, litify_pm__Expense__c>(
            [SELECT Id, 
                    Amount_Journaled__c,
                    Amount_Recovered_Settlement__c,
                    Amount_Recovered_Client__c,
                    Fully_Recovered__c,
                    litify_pm__Status__c
             FROM litify_pm__Expense__c
             WHERE Id IN :expenseIds]);

        List<ProcessDepositsController.DepositWrapper> depositWrappers = new List<ProcessDepositsController.DepositWrapper>();

        for(Deposit__c deposit : depositList)
        {
            Boolean isSourceClient = false;
            if (deposit.Source__c == 'Client' || deposit.Source__c == 'Operating'){
                isSourceClient = true;
            }

            for(Expense_to_Deposit__c expToDep : deposit.Expense_to_Deposits__r)
            {
                litify_pm__Expense__c expense = idToExpenseMap.get(expToDep.Expense__c);
                expense.Fully_Recovered__c = false;
                expense.Amount_Journaled__c = expense.Amount_Journaled__c == null ? 0 : expense.Amount_Journaled__c - expToDep.Amount_Allocated__c;
                expense.litify_pm__Status__c = 'Unpaid';
                if (expense.Amount_Recovered_Client__c == null)
                    expense.Amount_Recovered_Client__c = 0;
                if (expense.Amount_Recovered_Settlement__c == null)
                    expense.Amount_Recovered_Settlement__c = 0;
                
                if (isSourceClient)
                    expense.Amount_Recovered_Client__c -= expToDep.Amount_Allocated__c;
                else
                    expense.Amount_Recovered_Settlement__c -= expToDep.Amount_Allocated__c;

                expenseUpdateMap.put(expense.Id, expense);
                joinDeleteList.add(expToDep);
            }
            deposit.Deposit_Allocated__c = false;
            deposit.Deposit_Created__c = false;
            // deposit.Journal__c = null;
            deposit.Allocated_Soft_Cost__c = 0;
            deposit.Allocated_Hard_Cost__c = 0;
            deposit.Allocated_Fee__c = 0;
            depositUpdate.add(deposit);

            if (deposit.Journal__c != null)
            {
                ProcessDepositsController.DepositWrapper wrapper = new ProcessDepositsController.DepositWrapper();
                wrapper.depositId = deposit.Id;

                depositWrappers.add(wrapper);
                deposit.Journal__c = null;

                jRequest = new ProcessDepositsController.JournalRequest();
                jRequest.autoPostJournal = true;
            }
        }

        if (depositWrappers.size() > 0)
        {
            ProcessDepositsController.ActionResult result = ProcessDepositsController.createJournalFromDeposits(depositWrappers, jRequest, true);
        }
        update expenseUpdateMap.values();
        update depositUpdate;
        delete joinDeleteList;
    }
}