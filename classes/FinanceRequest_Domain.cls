/*============================================================================
Name            : FinanceRequest_Domain
Author          : CLD
Created Date    : May 2019
Description     : Domain class for Finance Requests
=============================================================================*/
public class FinanceRequest_Domain extends fflib_SObjectDomain {

    private List<Finance_Request__c> finRequestList;
    private FinanceRequest_Service frService;

    // Ctors
    public FinanceRequest_Domain() {
        super();
        this.finRequestList = new List<Finance_Request__c>();
        this.frService = new FinanceRequest_Service();
    }

    public FinanceRequest_Domain(List<Finance_Request__c> finRequestList) {
        super(finRequestList);
        this.finRequestList = (List<Finance_Request__c>) records;
        this.frService = new FinanceRequest_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new FinanceRequest_Domain(records);
        }
    }

    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {

        Map<Id, Finance_Request__c> oldTriggerMap = (Map<Id, Finance_Request__c>) existingRecords;

        //validate that the current company matches the matter company.
        frService.validateCurrentCompany(finRequestList, oldTriggerMap);
        frService.createExpensesFromCheckRequest(finRequestList, oldTriggerMap);
    }
}