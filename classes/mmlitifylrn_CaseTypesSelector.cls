public class mmlitifylrn_CaseTypesSelector
    extends mmlib_SObjectSelector
    implements mmlitifylrn_ICaseTypesSelector
{
    public static mmlitifylrn_ICaseTypesSelector newInstance()
    {
        return (mmlitifylrn_ICaseTypesSelector) mm_Application.Selector.newInstance(litify_pm__Case_Type__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Case_Type__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            litify_pm__Case_Type__c.litify_pm__ExternalId__c,
            litify_pm__Case_Type__c.litify_pm__Is_Available__c,
            litify_pm__Case_Type__c.litify_pm__Is_Local__c,
            litify_pm__Case_Type__c.litify_pm__LRN_Case_Type__c,
            litify_pm__Case_Type__c.litify_pm__Litify_com_ID__c,
            litify_pm__Case_Type__c.litify_pm__Litigation_Type__c,
            litify_pm__Case_Type__c.litify_pm__Mapped_to_ExternalId__c
        };
    }

    public List<litify_pm__Case_Type__c> selectById( Set<id> idSet )
    {
        return (List<litify_pm__Case_Type__c>) selectSObjectsById(idSet);
    }
}