public class EmailAlertsController {

    public Intake__c thisIntake {get;set;}    
    public String intakeFieldSet {get;set;}
    
    public Intake__c thisIntakeBasicInfo {get;set;}
    public String intakeDefaultFieldSet {get;set;}
    
    public Intake__c marketingInfo {get;set;}
    public String marketingInfoFieldSet {get;set;}
    
    public Event thisEvent {get;set;}
    
    public Account thisAccount {get;set;}
    
    public String getBaseUrl() {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/';
    }

    public Task thisIntakeComment {get;set;}
    
    public String selectedInvestigation {get;set;}
    
    public Id relatedIntakeId {
        get {
            return relatedIntakeId;
        }
        set {
            relatedIntakeId = value;
            
            intakeFieldSet = getFieldSet(); 
            thisIntake = queryIntake(intakeFieldSet);
            
            intakeDefaultFieldSet = 'Email_Alerts_Intake_Info';
            thisIntakeBasicInfo = queryIntake(intakeDefaultFieldSet);
            
            marketingInfoFieldSet = 'Email_Alerts_Marketing_Info';
            marketingInfo = queryIntake(marketingInfoFieldSet);

            Id thisAccountId = findPerson();
            thisAccount = queryPerson(thisAccountId);
            
            Id thisIntakeCommentId = findCommentTask();
            thisIntakeComment = queryTask(thisIntakeCommentId);
            
            Id thisEventId = findEvent();
            thisEvent = queryEvent(thisEventId);

            DefaultFieldsetNonEmpty = GetFieldsetNotEmpty(intakeDefaultFieldSet, thisIntakeBasicInfo);
            IntakeFieldsetNonEmpty = GetFieldsetNotEmpty(intakeFieldSet, thisIntake);
            MarketingInfoFieldsetNonEmpty = GetFieldsetNotEmpty(marketingInfoFieldSet, marketingInfo);
        }
    }

    public Id relatedIncidentInvestigationEventId {
        get {
            return relatedIncidentInvestigationEventId;
        }
        set {
            relatedIncidentInvestigationEventId = value;
            Id incieID = value;
            List<IntakeInvestigationEvent__c> inties = [SELECT Intake__c FROM IntakeInvestigationEvent__c WHERE IncidentInvestigation__c = :incieID];
            relatedIntakeId = inties[0].Intake__c;
        }
    }

    public Id relatedIntakeInvestigationEventId {
        get {
            return relatedIntakeInvestigationEventId;
        }
        set {
            Id intieID = value;
            List<IntakeInvestigationEvent__c> inties = [SELECT Intake__c FROM IntakeInvestigationEvent__c WHERE ID = :intieID];
            
            relatedIntakeId = inties[0].Intake__c;
        }
    }

    public Id relatedIncidentId {
        get {
            return relatedIncidentId;
        }
        set {
            Id incidentId = value;
            List<Intake__c> intakes = [SELECT Id FROM Intake__c WHERE Incident__c = :incidentId];

            relatedIntakeId = intakes[0].Id;
        }
    }

    public List<Schema.FieldsetMember> DefaultFieldsetNonEmpty { get; set; }
    public List<Schema.FieldsetMember> IntakeFieldsetNonEmpty { get; set; }
    public List<Schema.FieldsetMember> MarketingInfoFieldsetNonEmpty { get; set; }

    public static List<Schema.FieldSetMember> GetFieldsetNotEmpty(String fieldsetName, Intake__c intake) {
        List<Schema.FieldSetMember> result = new List<Schema.FieldSetMember>();

        List<Schema.FieldSetMember> members = readFieldSet(fieldSetName, 'Intake__c');

        for (Schema.FieldSetMember m : members) {
            String path = m.getFieldPath();
            Object o = intake.get(path);

            if (o != null) {
                result.add(m);
            }
        }

        return result;
    }
    public EmailAlertsController() {

    }
    
    private Intake__c IntakeSummaryBackingStore;
    public Intake__c getIntakeSummary() {
        if (IntakeSummaryBackingStore == null) {
            IntakeSummaryBackingStore = [SELECT Id, Name, Case_Type__c, Case_Notes__c, Case_Subtype__c, Catastrophic__c, Client__r.Name, Where_did_the_injury_occur__c, Status__c, Handling_Firm__c FROM Intake__c WHERE Id = :relatedIntakeId];
        }
        return IntakeSummaryBackingStore;
    }
    
    public List<IntakeInvestigationEvent__c> getAllIncidentsForThisIntake() {
        List<IntakeInvestigationEvent__c> incidents;
        incidents = [SELECT Name, Location__c, StartDateTime__c, EndDateTime__c, Client__c, Client__r.Name, Contact__c, Contact__r.Name, Intake__c, Intake__r.Name, Intake__r.Client__c, Intake__r.Client__r.Name, Intake__r.Status__c, Intake__r.Status_Detail__c, Intake__r.Case_Type__c, Intake__r.Case_Manager_Name__c, Intake__r.Handling_Firm__c, Intake__r.Case_Notes__c, Catastrophic__c, IncidentInvestigation__c, Reason_for_Delayed_Signup__c, Owner.Name, Contracts_to_be_Signed__c, Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c = :relatedIntakeId AND Investigation_Status__c ='Scheduled' ORDER BY LastModifiedDate DESC];        
        for(IntakeInvestigationEvent__c i: incidents){
            selectedInvestigation = i.IncidentInvestigation__c;
        }
        return incidents;
    }
    
    public List<IntakeInvestigationEvent__c> getAllIntakeInvestigationsForThisIncident() {
        List<IntakeInvestigationEvent__c> intakeEvents;
        intakeEvents = [SELECT Id, Name, Intake__c, Intake__r.Name, Intake__r.Client__c, Intake__r.Client__r.Name, Intake__r.Status__c, Intake__r.Status_Detail__c, Intake__r.Case_Type__c, Intake__r.Case_Manager_Name__c, Intake__r.Handling_Firm__c, Intake__r.Case_Notes__c, Client__c, Client__r.Name, Contact__c, Contact__r.Name, Location__c, StartDateTime__c, EndDateTime__c, Reason_for_Delayed_Signup__c, Owner.Name, Contracts_to_be_Signed__c, Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE IncidentInvestigation__c =:selectedInvestigation AND Intake__c != :relatedIntakeId];
        return intakeEvents;
    }
    
    public List<IntakeInvestigationEvent__c> getAllCanceledIncidentsForThisIntake() {
        String statusCancelled = InvestigationEventTriggerHandler.CANCEL_STATUS;
        String statusRescheduled = InvestigationEventTriggerHandler.RESCHEDULE_STATUS;
        List<IntakeInvestigationEvent__c> canceledIncidents;
        canceledIncidents = [SELECT Id, Name, Location__c, StartDateTime__c, EndDateTime__c, Client__c, Client__r.Name, Contact__c, Contact__r.Name, Intake__c, Intake__r.Name, Intake__r.Client__c, Intake__r.Client__r.Name, Intake__r.Status__c, Intake__r.Status_Detail__c, Intake__r.Case_Type__c, Intake__r.Case_Manager_Name__c, Intake__r.Handling_Firm__c, Intake__r.Case_Notes__c, Catastrophic__c, IncidentInvestigation__c, Reason_for_Delayed_Signup__c, Owner.Name, Contracts_to_be_Signed__c, Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c = :relatedIntakeId AND (Investigation_Status__c = :statusCancelled OR Investigation_Status__c = :statusRescheduled) ORDER BY LastModifiedDate DESC LIMIT 1];        
        return canceledIncidents;
    }
    
    public List<Event> getAdditionalCaseNotes() {
        List<Event> notes;
        notes = [SELECT Id, WhatId, Description, Type FROM Event WHERE Type='Investigation' AND WhatId= :relatedIncidentInvestigationEventId LIMIT 1];
        return notes;
    }
    
    public List<Intake__c> getInjuredPartyInfo() {
        List<Intake__c> injuredParty;
        injuredParty = [SELECT Id, Name, Client__c, Client__r.Name, Client__r.Date_of_Birth_mm__c, Client__r.Marital_Status__c, Can_IP_sign__c, Injured_Party__c, Injured_Party__r.Name, Injured_Party__r.Date_of_Birth_mm__c, Injured_Party__r.Marital_Status__c, Injured_party_relationship_to_Client__c, Injured_party_relationship_to_Caller__c, Caller_is_Injured_Party__c, Who_can_legally_sign_on_behalf_of_IP__c, Handling_Firm__c, Case_Notes__c FROM Intake__c WHERE Id = :relatedIntakeId LIMIT 1];
        return injuredParty;
    }
    
    public List<IntakeInvestigationEvent__c> getInjuredPartyInfoForThisIncident() {
        List<IntakeInvestigationEvent__c> injuredPartyIncidentIE;
        injuredPartyIncidentIE = [SELECT Id, Name, Intake__r.Client__c, Intake__r.Client__r.Name, Intake__r.Client__r.Date_of_Birth_mm__c, Intake__r.Client__r.Marital_Status__c, Intake__r.Can_IP_sign__c, Intake__r.Injured_Party__c, Intake__r.Injured_Party__r.Name, Intake__r.Injured_Party__r.Date_of_Birth_mm__c, Intake__r.Injured_Party__r.Marital_Status__c, Intake__r.Injured_party_relationship_to_Client__c, Intake__r.Injured_party_relationship_to_Caller__c, Intake__r.Caller_is_Injured_Party__c, Intake__r.Who_can_legally_sign_on_behalf_of_IP__c, Intake__r.Handling_Firm__c, Intake__r.Case_Notes__c, IncidentInvestigation__c, Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c = :relatedIntakeId AND Investigation_Status__c ='Scheduled' ORDER BY LastModifiedDate DESC LIMIT 1 ];
        for(IntakeInvestigationEvent__c i: injuredPartyIncidentIE){
            selectedInvestigation = i.IncidentInvestigation__c;
        }
        return injuredPartyIncidentIE;
    }
    
    public String getFieldSet() {
        Intake__c dummyIntake = [SELECT Id, Case_Type__c FROM Intake__c WHERE Id = :relatedIntakeId];
        String relatedIntakeCT = dummyIntake.Case_Type__c;
        String fieldSetResult = null;

        Map<String,Case_Type_Field_Set_Mappings__c> ctCustomSettingMap = Case_Type_Field_Set_Mappings__c.getAll();
        Map<String, String> CTMap = new Map<String, String>();
        for(Case_Type_Field_Set_Mappings__c ctcs : ctCustomSettingMap.values()) {
            CTMap.put(ctcs.Case_Type__c, ctcs.Field_Set_Name__c);
        }

        fieldSetResult = CTMap.get(relatedIntakeCT);

        if(fieldSetResult != null) {
            return fieldSetResult;
        } else {
            return 'Email_Alerts_Default';
        }
    }

    //Get Fieldset Fields in Apex dynamically when Object Name and FieldSet name is supplied at runtime
    
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields();
    }
    
    public Intake__c queryIntake(String intakeFieldSetName) {
        List<Schema.FieldSetMember> fieldSetMemberList = readFieldSet(intakeFieldSetName, 'Intake__c');

        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, client__c from Intake__c WHERE Id = \''+relatedIntakeId+'\' LIMIT 1';
        try {
            List<Intake__c> dummyIntake = Database.query(query);
            return dummyIntake[0];
        } catch (Exception e) {
            //error
            return null;
        }
    }
    
    public Id findPerson() {
        List<Account> queriedAccount = [select id from Account where id = :thisIntake.client__c];
        if (queriedAccount.size() > 0) {
            return queriedAccount[0].Id;
        } else {
            //error could not query account from intake
            return null;
        }
    }

    public Account queryPerson(Id queriedAccount) {
        List<Schema.FieldSetMember> fieldSetMemberList = readFieldSet('General_Account_Info', 'Account');
        try {
            String query = 'SELECT ';
            for(Schema.FieldSetMember f : fieldSetMemberList) {
                query += f.getFieldPath() + ', ';
            }
            query += 'Id from Account WHERE Id = \''+queriedAccount+'\'';
            List<Account> dummyAccount = Database.query(query);
            return dummyAccount[0];
        } catch (Exception e) {
            //System.addError('There is error while Fetching existing Account using Dynamic SOQL in Field Set. Error Detail - '+e.getMessage());
        }
        return null;
    }
    
    public Id findCommentTask() {
        List<Task> queriedTask = [SELECT Id,WhatId FROM Task WHERE WhatId = :thisIntake.Id AND Subject = 'Intake Comment' LIMIT 1];
        if (queriedTask.size() > 0) {
            return queriedTask[0].Id;
        } else {
            return null;
        }
    }

    public Task queryTask(Id queriedTask) {
        List<FieldSetMember> fieldSetMemberList = readFieldSet('Email_Intake_Comments', 'Task');
        try {
            string query = 'SELECT ';
            for(Schema.FieldSetMember f : fieldSetMemberList) {
                query += f.getFieldPath() + ', ';
            }
            query += 'Id from Task WHERE Id = \''+queriedTask+'\'';
            List<Task> dummyTask = Database.query(query);
            return dummyTask[0];
        } catch (Exception e) {

        }
        return null;
    }
    
    public Id findEvent() {
        List<Event> queriedEvent = [SELECT Id,WhatId FROM Event WHERE WhatId = :thisIntake.Id AND ActivityDate >= TODAY LIMIT 1];
        if (queriedEvent.size() > 0) {
            return queriedEvent[0].Id;
        } else {
            return null;
        }
    }
    
    public Event queryEvent(Id queriedEvent) {
        List<FieldSetMember> fieldSetMemberList = readFieldSet('Investigation_Info', 'Event');
        try {
            string query = 'SELECT ';
            for(Schema.FieldSetMember f : fieldSetMemberList) {
                query += f.getFieldPath() + ', ';
            }
            query += 'Id from Event WHERE Id = \''+queriedEvent+'\'';
            List<Event> dummyEvent = Database.query(query);
            return dummyEvent[0];
        } catch (Exception e) {

        }
        return null;
    }

    // Todo: Remove along with DateOfBirthCoverageTest.cls once real tests are created.
    @TestVisible
    private static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}