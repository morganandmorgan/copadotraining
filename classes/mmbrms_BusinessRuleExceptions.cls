public class mmbrms_BusinessRuleExceptions
{
	private mmbrms_BusinessRuleExceptions()
	{
		// Hide from consumer.
	}

	public class EvaluationException
		extends Exception
	{
		// No code.
	}

	public class ParameterException
		extends Exception
	{
		// No code.
	}
}