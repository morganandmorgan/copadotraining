public class mmwiz_Exceptions
{
	private mmwiz_Exceptions()
	{
		// Hide from consumer.
	}

    public virtual class ServiceException
        extends Exception
    {
		// No code.
    }

    public class ParameterException
        extends mmwiz_Exceptions.ServiceException
    {
        // No code.
    }
}