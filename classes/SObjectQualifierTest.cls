@isTest
private class SObjectQualifierTest {
  private static List<SObjectQualifier.Query> AllQueries = new List<SObjectQualifier.Query>();
  private static Account testAccount1;
  private static Contact testContact1;
  private static SObjectQualifier accountQualifier = new SObjectQualifier(Account.sObjectType);
  private static SObjectQualifier contactQualifier = new SObjectQualifier(Contact.sObjectType);
  static {
    testAccount1 = new Account(Name = 'Test Account', AccountNumber = '111111', AnnualRevenue = 100000);
    insert testAccount1;

    testContact1 = new Contact(Account = testAccount1, BirthDate = Date.newInstance(1990, 1, 1), FirstName = 'Test', LastName = 'Contact', Phone = '1112223333', HasOptedOutOfEmail = true);
    insert testContact1;
  }

  private static SObjectQualifier.Query AddQuery(String column, String operator, String argument) {
    return AddQuery(column, operator, argument, null);
  }

  private static SObjectQualifier.Query AddQuery(String column, String operator, String argument, String formula) {
    SObjectQualifier.Query qry = new SObjectQualifier.Query();
    qry.column = column;
    qry.operator = operator;
    qry.argument = argument;
    qry.formula = formula;

    AllQueries.add(qry);
    return qry;
  }

  @isTest static void Test_Boolean() {
    SObjectQualifier.Query query = AddQuery('HasOptedOutOfEmail', '=', 'true');
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = 'false';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = 'true';
    query.operator = '<>';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = 'false';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);
  }

  @isTest static void Test_Date_GreaterThan() {
    SObjectQualifier.Query query = AddQuery('BirthDate', '>', testContact1.BirthDate.AddDays(-30).format());
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.format();
    query.formula = null;
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = testContact1.BirthDate.AddDays(1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = '36500';
    query.formula = 'days_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = '1200';
    query.formula = 'months_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = '100';
    query.formula = 'years_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = '1';
    query.formula = 'days_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = '1';
    query.formula = 'months_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = '1';
    query.formula = 'years_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Date_LessThan() {
    SObjectQualifier.Query query = AddQuery('BirthDate', '<', testContact1.BirthDate.AddDays(30).format());
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.format();
    query.formula = null;
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = testContact1.BirthDate.AddDays(-1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = '1';
    query.formula = 'days_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = '1';
    query.formula = 'months_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = '1';
    query.formula = 'years_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = '36500';
    query.formula = 'days_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = '1200';
    query.formula = 'months_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = '100';
    query.formula = 'years_ago';
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Date_GreaterThanEqual() {
    SObjectQualifier.Query query = AddQuery('BirthDate', '>=', testContact1.BirthDate.AddDays(-30).format());
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.format();
    query.formula = null;
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.AddDays(1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Date_LessThanEqual() {
    SObjectQualifier.Query query = AddQuery('BirthDate', '<=', testContact1.BirthDate.AddDays(30).format());
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.format();
    query.formula = null;
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.AddDays(-1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Date_Equal() {
    SObjectQualifier.Query query = AddQuery('BirthDate', '=', testContact1.BirthDate.format());
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.AddDays(-1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = testContact1.BirthDate.AddDays(1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Date_NotEqual() {
    SObjectQualifier.Query query = AddQuery('BirthDate', '<>', testContact1.BirthDate.AddDays(1).format());
    Boolean isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);

    query.argument = testContact1.BirthDate.format();
    query.formula = null;
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(false, isMatch);

    query.argument = testContact1.BirthDate.AddDays(-1).format();
    isMatch = contactQualifier.CriteriaMatches(AllQueries, testContact1);
    System.assertEquals(true, isMatch);
  }

  @isTest static void Test_Number_LessThan() {
    SObjectQualifier.Query query = AddQuery('AnnualRevenue', '<', String.valueOf(testAccount1.AnnualRevenue + 1));
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue - 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Number_LessThanEqual() {
    SObjectQualifier.Query query = AddQuery('AnnualRevenue', '<=', String.valueOf(testAccount1.AnnualRevenue + 1));
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue - 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Number_GreaterThan() {
    SObjectQualifier.Query query = AddQuery('AnnualRevenue', '>', String.valueOf(testAccount1.AnnualRevenue - 1));
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue + 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Number_GreaterThanEqual() {
    SObjectQualifier.Query query = AddQuery('AnnualRevenue', '>=', String.valueOf(testAccount1.AnnualRevenue - 1));
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue + 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Number_Equals() {
    SObjectQualifier.Query query = AddQuery('AnnualRevenue', '=', String.valueOf(testAccount1.AnnualRevenue));
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue - 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue + 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_Number_NotEqual() {
    SObjectQualifier.Query query = AddQuery('AnnualRevenue', '<>', String.valueOf(testAccount1.AnnualRevenue - 1));
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue + 1);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = String.valueOf(testAccount1.AnnualRevenue);
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }

  @isTest static void Test_String() {
    SObjectQualifier.Query query = AddQuery('Name', '=', testAccount1.Name);
    Boolean isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = testAccount1.Name + 'Nope';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);

    query.argument = testAccount1.Name + 'Nope';
    query.operator = '<>';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = testAccount1.Name;
    query.operator = '<>';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);

    query.argument = testAccount1.Name.Substring(0,5);
    query.operator = 'contains';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = testAccount1.Name.Substring(0,5).reverse();
    query.operator = 'contains';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);

    query.argument = testAccount1.Name.Substring(0,5).reverse();
    query.operator = 'does_not_contain';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(true, isMatch);

    query.argument = testAccount1.Name.Substring(0,5);
    query.operator = 'does_not_contain';
    isMatch = accountQualifier.CriteriaMatches(AllQueries, testAccount1);
    System.assertEquals(false, isMatch);
  }
}