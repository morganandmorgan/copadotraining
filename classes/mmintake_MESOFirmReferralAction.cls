public class mmintake_MESOFirmReferralAction
    implements mmlib_IAction
{
    private List<Intake__c> records = new List<Intake__c>();

    public mmlib_IAction setRecordsToActOn(List<SObject> records)
    {
        this.records = records;
        return this;
    }

    public Boolean run()
    {
        if (records == null || records.isEmpty())
        {
            return false;
        }

        List<String> picklistEntryList = getPicklistEntries();

        Integer currentIndex = 
            incrementIndex(
                picklistEntryList, 
                getCurrentIndex(
                    picklistEntryList, 
                    getMostRecentlyAssignedFirm()));

        for (Intake__c intake : records)
        {
            intake.MESO_Firm_Referred__c = picklistEntryList.get(currentIndex);
            currentIndex = incrementIndex(picklistEntryList, currentIndex);
        }

        return true;
    }

    private Integer getCurrentIndex(List<String> picklistEntryList, String value)
    {
        Integer result = 0;

        for (Integer i = 0; i < picklistEntryList.size(); i++)
        {
            if (value.equalsIgnoreCase(picklistEntryList.get(i)))
            {
                result = i;
                break;
            }
        }

        return result;
    }

    private String getMostRecentlyAssignedFirm()
    {
        String result = '';

        List<Intake__c> intakeList = mmintake_IntakesSelector.newInstance().selectMostRecentMesoReferral();

        if (!intakeList.isEmpty())
        {
            result = intakeList.get(0).MESO_Firm_Referred__c;
        }

        return result;
    }

    private List<String> getPicklistEntries()
    {
        List<String> result = new List<String>();
        
        for (PicklistEntry ple : Intake__c.MESO_Firm_Referred__c.getDescribe().getPicklistValues())
        {
            if (ple.isActive()) result.add(ple.getValue());
        }

        if (mock_picklistEntryList != null)
        {
            result = mock_picklistEntryList;
        }

        return result;
    }

    private Integer incrementIndex(List<String> picklistEntryList, Integer currentIndex)
    {
        currentIndex++;

        if (picklistEntryList.size() <= currentIndex)
        {
            currentIndex = 0;
        }

        return currentIndex;
    }

    // ================= Testing Context ==============================================
    @TestVisible
    private static List<String> mock_picklistEntryList = null;
}