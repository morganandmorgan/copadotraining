/**
 *  mmlib_SObjectDeleteExecutor
 */
public class mmlib_SObjectDeleteExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        database.delete(scope, false);
    }
}