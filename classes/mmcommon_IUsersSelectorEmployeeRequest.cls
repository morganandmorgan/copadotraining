public interface mmcommon_IUsersSelectorEmployeeRequest
{
    mmcommon_IUsersSelectorEmployeeRequest setFirstName( String firstName );
    mmcommon_IUsersSelectorEmployeeRequest setLastName( String lastName );
    mmcommon_IUsersSelectorEmployeeRequest setDepartment( String department );
    mmcommon_IUsersSelectorEmployeeRequest setTags( Set<String> tags );
    mmcommon_IUsersSelectorEmployeeRequest setExtension( String extension );
    mmcommon_IUsersSelectorEmployeeRequest setPhoneNumber( String phoneNumber );
    mmcommon_IUsersSelectorEmployeeRequest setLocation( String location );

    String getFirstName();
    String getLastName();
    String getDepartment();
    Set<String> getTags();
    String getExtension();
    String getPhoneNumber();
    String getLocation();
}