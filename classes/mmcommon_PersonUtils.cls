public class mmcommon_PersonUtils {

    private mmcommon_PersonUtils() {}

    public static Boolean isInvestigator(User person)
    {
        return String.isNotBlank(person.Territory__c);
    }

    public static Boolean isActive(User person)
    {
        return person.IsActive == true;
    }
}