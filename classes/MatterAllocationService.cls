public without sharing class MatterAllocationService {
    
    public static Set<String> writeOffExpenseTypeNames = new Set<String>{'Write Offs (Hard Costs)','Write Offs (Soft Costs)'};
    
    public MatterAllocationService() {

    }

    //Wrapper class to collectively allocate amounts from both deposits and expenses
    public class AllocationWrapper implements Comparable{

        public Deposit__c depositObj;
        public litify_pm__Expense__c expenseObj;
        public Date effectiveDate; 
        public Decimal amountToAllocate;
        
        public AllocationWrapper(Deposit__c dep){
            depositObj = dep;
        }
        public AllocationWrapper(litify_pm__Expense__c exp){
            expenseObj = exp;
        }
        // Provides sorting by effective date
        public Integer compareTo(Object compareTo) {
            // Cast argument to AllocationWrapper
            AllocationWrapper compareToWrapper = (AllocationWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (effectiveDate > compareToWrapper.effectiveDate) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (effectiveDate < compareToWrapper.effectiveDate) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            return returnValue;       
        } 
    }

    //provide just a set of deposit ids to allocate
    public static void allocateItems_Deposit(Set<Id> depositIds){
        List<Deposit__c> depositList = [
            SELECT Id,
                 Amount__c,
                 Source__c,
                 Matter__c,
                 Matter__r.RecordType.Name,
                 Hard_Cost_Limit__c,
                 Soft_Cost_Limit__c,
                 Deposit_Allocated__c,
                 Allocated_Fee__c,
                 Unallocated_Costs__c,
                 Check_Date__c
            FROM Deposit__c 
            WHERE Deposit_Allocated__c = false 
            AND Id in :depositIds
            AND RecordType.Name in ('Operating','Trust Payout - Costs')
            AND Matter__c != null
            ORDER BY Amount__c, Check_Date__c];
        
        Set<Id> matterIds = new Set<Id>();
        for(Deposit__c d : depositList){
            matterIds.add(d.Matter__c);
        }
        allocateItems(matterIds, depositList, null);
    }

    public static void allocateItems(Set<Id> matterIds){
        List<Deposit__c> depositList = [
            SELECT Id,
                 Amount__c,
                 Source__c,
                 Matter__c,
                 Matter__r.RecordType.Name,
                 Hard_Cost_Limit__c,
                 Soft_Cost_Limit__c,
                 Deposit_Allocated__c,
                 Allocated_Fee__c,
                 Unallocated_Costs__c,
                 Check_Date__c
            FROM Deposit__c 
            WHERE Deposit_Allocated__c = false 
            AND Matter__c in :matterIds
            AND RecordType.Name in ('Operating','Trust Payout - Costs')
            ORDER BY Amount__c, Check_Date__c];

            List<litify_pm__Expense__c> negativeExpenses = [
            SELECT Id,
                CostType__c,
                Create_Payable_Invoice__c,
                Invoice__c,
                PayableTo__c,
                Payable_Invoice_Created__c,
                Payable_Invoice__c,
                RequestedBy__c,
                Vendor_Invoice_Number__c,
                litify_pm__Amount__c,
                litify_pm__Date__c,
                litify_pm__ExpenseType2__c,
                litify_pm__Expense_Description__c,
                litify_pm__Matter__c,
                litify_pm__Note__c,
                litify_pm__Status__c,
                litify_pm__TaskType__c,
                Amount_Recovered_Settlement__c,
                Amount_Recovered_Client__c,
                Written_Off_Amount__c,
                Expense_Written_Off__c,
                Fully_Recovered__c,
                Amount_Remaining_Settlement__c,
                Amount_Remaining_Client__c,
                Amount_Remaining_to_Recover__c,
                Amount_Recovered__c,
                litify_pm__ExpenseType2__r.Name
            FROM litify_pm__Expense__c
            WHERE litify_pm__Matter__c in :matterIds 
            AND Fully_Recovered__c = false
            AND litify_pm__Amount__c < 0];  

        allocateItems(matterIds, depositList, negativeExpenses);
    }

    public static void allocateItems(Set<Id> matterIds, Set<Id> depositIds, Set<Id> negativeExpenseIds){
        List<Deposit__c> depositList = [
        SELECT Id,
                Amount__c,
                Source__c,
                Matter__c,
                Matter__r.RecordType.Name,
                Hard_Cost_Limit__c,
                Soft_Cost_Limit__c,
                Deposit_Allocated__c,
                Allocated_Fee__c,
                Unallocated_Costs__c,
                Check_Date__c
        FROM Deposit__c 
        WHERE Deposit_Allocated__c = false 
        AND Id in :depositIds
        AND RecordType.Name in ('Operating','Trust Payout - Costs')
        ORDER BY Amount__c, Check_Date__c];

        List<litify_pm__Expense__c> negativeExpenses = [
        SELECT Id,
            CostType__c,
            Create_Payable_Invoice__c,
            Invoice__c,
            PayableTo__c,
            Payable_Invoice_Created__c,
            Payable_Invoice__c,
            RequestedBy__c,
            Vendor_Invoice_Number__c,
            litify_pm__Amount__c,
            litify_pm__Date__c,
            litify_pm__ExpenseType2__c,
            litify_pm__Expense_Description__c,
            litify_pm__Matter__c,
            litify_pm__Note__c,
            litify_pm__Status__c,
            litify_pm__TaskType__c,
            Amount_Recovered_Settlement__c,
            Amount_Recovered_Client__c,
            Written_Off_Amount__c,
            Expense_Written_Off__c,
            Fully_Recovered__c,
            Amount_Remaining_Settlement__c,
            Amount_Remaining_Client__c,
            Amount_Remaining_to_Recover__c,
            Amount_Recovered__c,
            litify_pm__ExpenseType2__r.Name
        FROM litify_pm__Expense__c
        WHERE Id in :negativeExpenseIds 
        AND Fully_Recovered__c = false
        AND litify_pm__Amount__c < 0];       
        
        allocateItems(matterIds, depositList, negativeExpenses);
    }

    public static void allocateItems(Set<Id> matterIds, List<Deposit__c> depositList, List<litify_pm__Expense__c> negativeExpenses){
    
        system.debug('Calling allocateItems');
        List<Deposit__c> depositsToUpdate = new List<Deposit__c>();
        List<Expense_to_Deposit__c> newEtDList = new List<Expense_to_Deposit__c>();
        List<Expense_to_Expense__c> newEtEList = new List<Expense_to_Expense__c>();
        Map<Id, litify_pm__Expense__c> expensesToUpdate = new Map<Id, litify_pm__Expense__c>();
        Map<String, List<litify_pm__Expense__c>> costTypeToExpenseMap = new Map<String, List<litify_pm__Expense__c>>();
        List<AllocationWrapper> allocationWrappers = new List<AllocationWrapper>();
        
        List<litify_pm__Expense__c> eligibleExpenseList = [
            SELECT Id,
                 litify_pm__Amount__c,
                 Amount_Recovered_Settlement__c,
                 Amount_Recovered_Client__c,
                 Written_Off_Amount__c,
                 Amount_Remaining_Client__c,
                 Amount_Remaining_Settlement__c,
                 CostType__c,
                 Fully_Recovered__c,
                 litify_pm__Status__c,
                 litify_pm__Matter__c,
                 litify_pm__Date__c
            FROM litify_pm__Expense__c
            WHERE litify_pm__Matter__c in : matterIds
            AND ((Amount_Remaining_Settlement__c != null AND Amount_Remaining_Settlement__c > 0) OR (Amount_Remaining_Client__c != null AND Amount_Remaining_Client__c > 0))
            AND Don_t_Recover_This_Cost__c = FALSE 
            AND Don_t_Recover_This_Cost__c = FALSE
            AND litify_pm__ExpenseType2__r.Exclude_from_Allocation__c = false 
            ORDER BY CostType__c, litify_pm__Date__c];
  
        // Loop through the eligible expenses for allocation and categorize them into hard / soft cost
        for(litify_pm__Expense__c exp : eligibleExpenseList){
            String key = exp.litify_pm__Matter__c + '|' + exp.CostType__c;
            if (!costTypeToExpenseMap.containsKey(key)){
                costTypeToExpenseMap.put(key, new List<litify_pm__Expense__c>{exp}); 
            }
            else{
                List<litify_pm__Expense__c> expList = costTypeToExpenseMap.get(key);    
                expList.add(exp);
                costTypeToExpenseMap.put(key, expList); 
            }
        }

        //Build the wrapper list from deposits and expenses:
        if(depositList != null){
            for(Deposit__c deposit : depositList){
                //subtract any predefined fee or unallocated cost amounts:
                Decimal definedFeeAmount = deposit.Allocated_Fee__c != null ? deposit.Allocated_Fee__c : 0; 
                Decimal unallocatedCostAmount = deposit.Unallocated_Costs__c != null ? deposit.Unallocated_Costs__c : 0;
                Decimal remainingAmount = deposit.Amount__c - definedFeeAmount - unallocatedCostAmount;

                AllocationWrapper aw = new AllocationWrapper(deposit);
                aw.amountToAllocate = remainingAmount;
                aw.effectiveDate = deposit.Check_Date__c;
                allocationWrappers.add(aw);
            }
        }
        
        if(negativeExpenses != null){
            for(litify_pm__Expense__c exp : negativeExpenses){
            
                //find the max write off date
                //load up the max expense date of any write offs
                // if(writeOffExpenseTypeNames.contains(exp.litify_pm__ExpenseType2__r.Name)){
                // 	if(!maxNegativeWriteOffDateMap.containsKey(exp.litify_pm__Matter__c)){
                // 		maxNegativeWriteOffDateMap.put(exp.litify_pm__Matter__c, exp.litify_pm__Date__c);
                // 	}
                // 	else{
                // 		Date maxExpDate = exp.litify_pm__Date__c > maxNegativeWriteOffDateMap.get(exp.litify_pm__Matter__c) ? exp.litify_pm__Date__c : maxNegativeWriteOffDateMap.get(exp.litify_pm__Matter__c);
                // 		maxNegativeWriteOffDateMap.put(exp.litify_pm__Matter__c, maxExpDate);
                // 	}
                // }

                AllocationWrapper aw = new AllocationWrapper(exp);
                aw.amountToAllocate = exp.litify_pm__Amount__c;
                aw.effectiveDate = exp.litify_pm__Date__c;
                allocationWrappers.add(aw);
            }
        }
        

        //sort the allocation wrapper list by date
        allocationWrappers.sort();
        system.debug('MatterAllocationService - allocationWrappers after sort = ' + allocationWrappers);

        // Start allocation loop through the wrappers
        for(AllocationWrapper aw : allocationWrappers){
            system.debug('MatterAllocationService - aw.effectiveDate = '+ aw.effectiveDate.format());
            
            //DEPOSITS
            if(aw.depositObj != null){
                system.debug('MatterAllocationService - Processing Deposit');

                // Recover all hard costs first, then soft costs.
                // The leftover is used for fee.
                Decimal remainingAmount = aw.amountToAllocate;
                Decimal feeAmount = remainingAmount;
                Decimal totalSoftCost = 0;
                Decimal totalHardCost = 0;
            
                Boolean isSourceClient = false;
                if (aw.depositObj.Source__c == 'Client' || aw.depositObj.Source__c == 'Operating'){
                    isSourceClient = true;
                }

                Id matterId = aw.depositObj.Matter__c;
                String hcKey = matterId + '|HardCost';
                String scKey = matterId + '|SoftCost';

                // HARD COSTS
                if(costTypeToExpenseMap.containsKey(hcKey)){
                    Decimal hcLimit = aw.depositObj.Hard_Cost_Limit__c != null && aw.depositObj.Hard_Cost_Limit__c != 0 ? aw.depositObj.Hard_Cost_Limit__c : null;
                    Decimal remainingHC = hcLimit != null ? hcLimit : remainingAmount;

                    for(litify_pm__Expense__c expense : costTypeToExpenseMap.get(hcKey)){

                        //only allocated to expenses that are less than the date of the deposit:
                        if(aw.effectiveDate >= expense.litify_pm__Date__c){
                            //retrieve the expense from the update map if applicable
                            if (expensesToUpdate.containsKey(expense.Id)){
                                expense = expensesToUpdate.get(expense.Id);
                            }
                            
                            //set the values to 0 if for some reason they are null:
                            expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
                            expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
                            expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

                            // Check to verify that there is enough money left to pay off the expense.
                            Decimal currentExpenseAmount = isSourceClient == false ? (expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Written_Off_Amount__c).setScale(2) : (expense.litify_pm__Amount__c - expense.Amount_Recovered_Client__c - expense.Written_Off_Amount__c).setScale(2);

                            // system.debug('**** MatterAllocationService - expense.Amount = ' +expense.litify_pm__Amount__c);
                            // system.debug('**** MatterAllocationService - remainingAmount = ' +remainingAmount);
                            // system.debug('**** MatterAllocationService - currentExpenseAmount = ' +currentExpenseAmount);
                            // system.debug('**** MatterAllocationService - isSourceClient = ' +isSourceClient);

                            Expense_to_Deposit__c expenseToDeposit = new Expense_to_Deposit__c();
                            expenseToDeposit.Expense__c = expense.Id;
                            expenseToDeposit.Deposit__c = aw.depositObj.Id;
                            expenseToDeposit.Amount_Allocated__c = 0;    

                            if (remainingHC >= currentExpenseAmount && currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : currentExpenseAmount;

                                expense.Fully_Recovered__c = true;
                                // system.debug('**** MatterAllocationService - expense.Fully_Recovered__c = ' +expense.Fully_Recovered__c);

                                if (isSourceClient){
                                    expense.Amount_Recovered_Client__c += currentExpenseAmount;
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
                                }

                                totalHardCost += feeReductionAmount;
                                remainingAmount -= currentExpenseAmount;
                                remainingHC -= currentExpenseAmount;
                                feeAmount -= feeReductionAmount;
                                expense.litify_pm__Status__c = 'Paid';

                                expenseToDeposit.Amount_Allocated__c = currentExpenseAmount;
                            }
                            else if (currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : remainingHC;

                                if (isSourceClient){
                                    expense.Amount_Recovered_Client__c += remainingHC;
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += remainingHC;
                                }

                                expenseToDeposit.Amount_Allocated__c = remainingHC;

                                totalHardCost += feeReductionAmount;
                                feeAmount -= feeReductionAmount;
                                remainingAmount = 0;
                                remainingHC = 0;
                            }
                            expensesToUpdate.put(expense.Id, expense);
                            if(expenseToDeposit.Amount_Allocated__c != 0){
                                newEtDList.add(expenseToDeposit);    
                            }
                        }
                        
                        
                    }
                } 
                // SOFT COSTS
                if(costTypeToExpenseMap.containsKey(scKey)){
                    Decimal scLimit = aw.depositObj.Soft_Cost_Limit__c != null && aw.depositObj.Soft_Cost_Limit__c != 0 ? aw.depositObj.Soft_Cost_Limit__c : null;
                    Decimal remainingSC = scLimit != null ? scLimit : remainingAmount;

                    for(litify_pm__Expense__c expense : costTypeToExpenseMap.get(scKey)){

                        //only allocated to expenses that are less than the date of the deposit:
                        if(aw.effectiveDate >= expense.litify_pm__Date__c){

                            //set the values to 0 if for some reason they are null:
                            expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
                            expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
                            expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;
                            
                            if (expensesToUpdate.get(expense.Id) != null)
                                expense = expensesToUpdate.get(expense.Id);

                            // Check to verify that there is enough money left to pay off the expense.
                            Decimal currentExpenseAmount = isSourceClient == false ? (expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Written_Off_Amount__c).setScale(2) : (expense.litify_pm__Amount__c - expense.Amount_Recovered_Client__c - expense.Written_Off_Amount__c).setScale(2);

                            // system.debug('**** MatterAllocationService - expense.Amount = ' +expense.litify_pm__Amount__c);
                            // system.debug('**** MatterAllocationService - remainingAmount = ' +remainingAmount);
                            // system.debug('**** MatterAllocationService - currentExpenseAmount = ' +currentExpenseAmount);
                            // system.debug('**** MatterAllocationService - isSourceClient = ' +isSourceClient);
                            
                            Expense_to_Deposit__c expenseToDeposit = new Expense_to_Deposit__c();
                            expenseToDeposit.Expense__c = expense.Id;
                            expenseToDeposit.Deposit__c = aw.depositObj.Id;
                            expenseToDeposit.Amount_Allocated__c = 0;

                            if (remainingSC >= currentExpenseAmount && currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : currentExpenseAmount;

                                expense.Fully_Recovered__c = true;
                                // system.debug('**** MatterAllocationService - expense.Fully_Recovered__c = ' +expense.Fully_Recovered__c);

                                if (isSourceClient){
                                    expense.Amount_Recovered_Client__c += currentExpenseAmount;
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
                                }

                                totalSoftCost += feeReductionAmount;
                                remainingAmount -= currentExpenseAmount;
                                remainingSC -= currentExpenseAmount;
                                feeAmount -= feeReductionAmount;
                                expense.litify_pm__Status__c = 'Paid';
                                expenseToDeposit.Amount_Allocated__c = currentExpenseAmount;

                            }
                            else if (currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : remainingSC;

                                if (isSourceClient){
                                    expense.Amount_Recovered_Client__c += remainingSC;
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += remainingSC;
                                }

                                totalSoftCost += feeReductionAmount;
                                expenseToDeposit.Amount_Allocated__c = remainingSC;
                                remainingAmount = 0;
                                remainingSC = 0;
                                feeAmount -= feeReductionAmount;
                            }
                            expensesToUpdate.put(expense.Id, expense);
                            if(expenseToDeposit.Amount_Allocated__c != 0){
                                newEtDList.add(expenseToDeposit);    
                            }
                        }
                    }
                }

                //update deposit fields
                aw.depositObj.Deposit_Allocated__c = true;
                aw.depositObj.Allocated_Soft_Cost__c = totalSoftCost;
                aw.depositObj.Allocated_Hard_Cost__c = totalHardCost;
                aw.depositObj.Allocated_Fee__c = feeAmount;

                System.debug('MatterAllocationService : aw.depositObj.Allocated_Soft_Cost__c = ' + aw.depositObj.Allocated_Soft_Cost__c);
                System.debug('MatterAllocationService : aw.depositObj.Allocated_Hard_Cost__c = ' + aw.depositObj.Allocated_Hard_Cost__c);
                System.debug('MatterAllocationService : aw.depositObj.Allocated_Fee__c = ' + aw.depositObj.Allocated_Fee__c);
                depositsToUpdate.add(aw.depositObj);
            }

            //NEGATIVE EXPENSES
            if(aw.expenseObj != null){
                
                system.debug('MatterAllocationService - Processing Negative Expense');
                String costType = aw.expenseObj.CostType__c;
                Id MatterId = aw.expenseObj.litify_pm__Matter__c;
                Decimal remainingAmountAbs = aw.expenseObj.litify_pm__Amount__c.abs();
        
                String expKey = matterId + '|' + costType;
                Boolean isWriteOffType = writeOffExpenseTypeNames.contains(aw.expenseObj.litify_pm__ExpenseType2__r.Name) ? true : false;

                Decimal totalWrittenOff = 0;
                Decimal totalNegAllocated = 0;

                // HARD COSTS and SOFT COSTS
                if(costTypeToExpenseMap.containsKey(expKey)){
                    Decimal hcLimit = null;
                    Decimal remainingCost = remainingAmountAbs;

                    for(litify_pm__Expense__c expense : costTypeToExpenseMap.get(expKey)){

                        //only allocated to expenses that are less than the date of the negative expense:
                        if(aw.effectiveDate >= expense.litify_pm__Date__c){
                            //retrieve the expense from the update map if applicable
                            if (expensesToUpdate.containsKey(expense.Id)){
                                expense = expensesToUpdate.get(expense.Id);
                            }
                            
                            //set the values to 0 if for some reason they are null:
                            expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
                            expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
                            expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

                            // get the current remaining amount (note this is normally stored in a formula but we recreate it here)
                            Decimal currentExpenseAmount = (expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Written_Off_Amount__c).setScale(2);

                            // system.debug('**** MatterAllocationService - expense.Amount = ' +expense.litify_pm__Amount__c);
                            // system.debug('**** MatterAllocationService - remainingAmountAbs = ' +remainingAmountAbs);
                            // system.debug('**** MatterAllocationService - currentExpenseAmount = ' +currentExpenseAmount);

                            Expense_to_Expense__c expenseToExpense = new Expense_to_Expense__c();
                            expenseToExpense.Target_Expense__c = expense.Id;
                            expenseToExpense.Source_Expense__c = aw.expenseObj.Id;
                            expenseToExpense.Amount_Allocated__c = 0;    

                            if (remainingCost >= currentExpenseAmount && currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : currentExpenseAmount;
                                expense.Fully_Recovered__c = true;
                                // system.debug('**** MatterAllocationService - expense.Fully_Recovered__c = ' +expense.Fully_Recovered__c);
        
                                //set either the amount recovered or the written off field depending on what type of negative expense it is
                                if(isWriteOffType){
                                    expense.Written_Off_Amount__c += currentExpenseAmount;
                                    totalWrittenOff -= currentExpenseAmount; 
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
                                    totalNegAllocated -= currentExpenseAmount;
                                }
                                
                                remainingAmountAbs -= currentExpenseAmount;
                                remainingCost -= currentExpenseAmount;
                                expense.litify_pm__Status__c = 'Paid';
                                expenseToExpense.Amount_Allocated__c = currentExpenseAmount;
                            }
                            else if (currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : remainingCost;

                                if(isWriteOffType){
                                    expense.Written_Off_Amount__c += remainingCost;
                                    totalWrittenOff -= remainingCost; 
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += remainingCost;
                                    totalNegAllocated -= remainingCost;
                                }

                                expenseToExpense.Amount_Allocated__c = remainingCost;
                                remainingAmountAbs = 0;
                                remainingCost = 0;
                            }
                            expensesToUpdate.put(expense.Id, expense);
                            if(expenseToExpense.Amount_Allocated__c != 0){
                                newEtEList.add(expenseToExpense);    
                            }
                        }
                    }
                } 
                // SOFT COSTS
                /* if(costTypeToExpenseMap.containsKey(expKey)){
                    Decimal scLimit = null;
                    Decimal remainingSC = remainingAmountAbs;

                    for(litify_pm__Expense__c expense : costTypeToExpenseMap.get(hcKey)){

                        //only allocated to expenses that are less than the date of the negative expense:
                        if(aw.expenseObj.litify_pm__Date__c >= expense.litify_pm__Date__c){
                            //retrieve the expense from the update map if applicable
                            if (expensesToUpdate.containsKey(expense.Id)){
                                expense = expensesToUpdate.get(expense.Id);
                            }
                            
                            //set the values to 0 if for some reason they are null:
                            expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
                            expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
                            expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

                            // get the current remaining amount (note this is normally stored in a formula but we recreate it here)
                            Decimal currentExpenseAmount = (expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Written_Off_Amount__c).setScale(2);

                            system.debug('**** MatterAllocationService - expense.Amount = ' +expense.litify_pm__Amount__c);
                            system.debug('**** MatterAllocationService - remainingAmountAbs = ' +remainingAmountAbs);
                            system.debug('**** MatterAllocationService - currentExpenseAmount = ' +currentExpenseAmount);

                            Expense_to_Expense__c expenseToExpense = new Expense_to_Expense__c();
                            expenseToExpense.Target_Expense__c = expense.Id;
                            expenseToExpense.Source_Expense__c = aw.expenseObj.Id;
                            expenseToExpense.Amount_Allocated__c = 0;    

                            if (remainingSC >= currentExpenseAmount && currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : currentExpenseAmount;

                                expense.Fully_Recovered__c = true;
                                system.debug('**** MatterAllocationService - expense.Fully_Recovered__c = ' +expense.Fully_Recovered__c);
        
                                //set either the amount recovered or the written off field depending on what type of negative expense it is
                                if(isWriteOffType){
                                    expense.Written_Off_Amount__c += currentExpenseAmount;
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
                                }
                                
                                remainingAmountAbs -= currentExpenseAmount;
                                remainingSC -= currentExpenseAmount;
                                expense.litify_pm__Status__c = 'Paid';

                                expenseToExpense.Amount_Allocated__c = currentExpenseAmount;
                            }
                            else if (currentExpenseAmount > 0){
                                //This is the variable to reduce the remaining fee - will be set to zero if the cost has already been recovered from another source
                                Decimal feeReductionAmount = expense.Fully_Recovered__c == true ? 0 : remainingSC;

                                if(isWriteOffType){
                                    expense.Written_Off_Amount__c += remainingSC;
                                }
                                else{
                                    expense.Amount_Recovered_Settlement__c += remainingSC;
                                }

                                expenseToExpense.Amount_Allocated__c = remainingSC;

                                remainingAmountAbs = 0;
                                remainingSC = 0;
                            }
                            expensesToUpdate.put(expense.Id, expense);
                            if(expenseToExpense.Amount_Allocated__c != 0){
                                newEtEList.add(expenseToExpense);    
                            }
                        }
                    }
                } */

                //update the negative expense only if they have been allocated to something:
                if(aw.expenseObj.litify_pm__Amount__c.abs() != remainingAmountAbs){
                    aw.expenseObj.Written_Off_Amount__c = totalWrittenOff;
                    aw.expenseObj.Amount_Recovered_Settlement__c = totalNegAllocated;
                    aw.expenseObj.Fully_Recovered__c = aw.expenseObj.litify_pm__Amount__c == (totalWrittenOff + totalNegAllocated) ? true : false;
                    expensesToUpdate.put(aw.expenseObj.Id, aw.expenseObj);
                }
            }
        }

        Savepoint sp1 = Database.setSavepoint();
		Try{
            if(!depositsToUpdate.isEmpty()){
                UPDATE depositsToUpdate;
            }
            if(!expensesToUpdate.isEmpty()){
                UPDATE expensesToUpdate.values();
            }
            if(!newEtDList.isEmpty()){
                INSERT newEtDList;
            }
            if(!newEtEList.isEmpty()){
                INSERT newEtEList;
            }
        }
        Catch(Exception e){
			system.debug('ERROR - '+e.getMessage() + e.getStackTraceString());
            Database.rollback(sp1);
            throw e;
		}
    }
}