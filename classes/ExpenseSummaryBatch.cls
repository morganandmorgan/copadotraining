/**
 * ExpenseSummaryBatch
 * @description Batch class for Expense Summaries.
 * @author Jeff Watson
 * @date 1/18/2019
 */
// Todo: This class is obsolete with the introduction of trigger on litify_pm__Expense__c to compute summaries realtime.
// Todo: This class and test class should be removed.
global with sharing class ExpenseSummaryBatch implements Database.Batchable<SObject>, Schedulable {

    private Datetime sinceDatetime;

    public ExpenseSummaryBatch() {
        sinceDatetime = Datetime.now().addMinutes(-4320);
    }

    public ExpenseSummaryBatch(Integer minutesToAdd) {
        sinceDatetime = Datetime.now().addMinutes(minutesToAdd);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, litify_pm__Matter__c FROM litify_pm__Expense__c WHERE LastModifiedDate > :sinceDatetime]);
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new ExpenseSummaryBatch());
    }

    global void execute(Database.BatchableContext bc, List<litify_pm__Expense__c> scope) {
        Map<Id,Set<Id>> matterIds = new Map<Id,Set<Id>>();
        for (litify_pm__Expense__c expense : scope) {
            matterIds.put(expense.litify_pm__Matter__c, null);
        }
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {litify_pm__Expense__c.getSObjectType(), Expense_Summary__c.getSObjectType()});
        ExpenseSummaryDomain.createExpenseSummaries(matterIds, uow);
    }

    global void finish(Database.BatchableContext bc) { }

} //class