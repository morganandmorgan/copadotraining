/**
 * SlackLimitsScheduleTest
 * @description Unit test for SlackLimitsSchedule
 * @author Matt Terrill
 * @date 8/22/2019
 */
@isTest
public with sharing class SlackLimitsScheduleTest {

    @isTest
    private static void execute() {

        Test.StartTest();

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new GovernorLimitsApiTestMock());

        SlackLimitsSchedule sls = new SlackLimitsSchedule();

        String sch = '0 0 23 * * ?';
        System.schedule('Test SlackLimitsSchedule', sch, sls);

        Test.stopTest();

    } //execute

} //class