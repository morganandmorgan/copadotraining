/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBNotifyLRN {
    global PBNotifyLRN() {

    }
    @InvocableMethod(label='Notify LRN' description='Notify LRN for the given Matter')
    global static void notifyLRN(List<litify_pm.PBNotifyLRN.ProcessBuilderNotifyLRNWrapper> notifyLRNItems) {

    }
global class ProcessBuilderNotifyLRNWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderNotifyLRNWrapper() {

    }
}
}
