public interface mmlitifylrn_IReferralTransSelector extends mmlib_ISObjectSelector
{
    List<litify_pm__Referral_Transaction__c> selectById( Set<id> idSet );
}