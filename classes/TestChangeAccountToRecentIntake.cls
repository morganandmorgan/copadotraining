@isTest
private class TestChangeAccountToRecentIntake {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        List<Account> clients = new List<Account>();
        for (Integer i = 0; i < 5; ++i) {
            clients.add(TestUtil.createPersonAccount('Test', 'Client ' + i));
        }
        toInsert.addAll((List<SObject>) clients);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Account> getClients() {
        return [
            SELECT
                (SELECT Id FROM Intake_Surveys__r ORDER BY CreatedDate DESC, SystemModStamp DESC LIMIT 1),
                (SELECT Id FROM Questionnares__r ORDER BY CreatedDate DESC, SystemModStamp DESC LIMIT 1)
            FROM
                Account
        ];
    }

    private static Incident__c getIncident() {
        return [SELECT Id FROM Incident__c];
    }

    private static List<Task> getTasks() {
        return [SELECT WhatId FROM Task];
    }

    private static List<Intake__c> buildIntakes(Incident__c incident, List<Account> clients, String status) {
        List<Intake__c> intakes = new List<Intake__c>();
        for (Account client : clients) {
            Intake__c intake = new Intake__c(
                    Client__c = client.Id,
                    Incident__c = incident.Id,
                    Status__c = status
                );
            intakes.add(intake);
        }
        return intakes;
    }

    private static List<Questionnaire__c> buildQuestionnares(List<Account> clients) {
        List<Questionnaire__c> questionnaires = new List<Questionnaire__c>();
        for (Account client : clients) {
            Questionnaire__c questionnaire = new Questionnaire__c(
                    Client__c = client.Id
                );
            questionnaires.add(questionnaire);
        }
        return questionnaires;
    }

    private static List<Task> buildTasks(List<Account> clients) {
        List<Task> tasks = new List<Task>();
        for (Account client : clients) {
            Task t = new Task(
                    Subject = 'Call',
                    WhatId = client.Id
                );
            tasks.add(t);
        }
        return tasks;
    }

    @isTest
    private static void changeTaskAccountToIntake() {
        Incident__c incident = getIncident();
        List<Account> clients = getClients();

        List<Intake__c> intakes = buildIntakes(incident, clients, 'Under Review');
        Database.insert(intakes);

        List<Task> tasks = buildTasks(clients);

        Test.startTest();
        Database.insert(tasks);
        Test.stopTest();

        Map<Id, Task> taskMap = new Map<Id, Task>(tasks);
        Map<Id, Task> requeriedTasks = new Map<Id, Task>(getTasks());
        Map<Id, Account> requeriedClients = new Map<Id, Account>(getClients());

        System.assertEquals(taskMap.keySet(), requeriedTasks.keySet());
        for (Task requeriedTask : requeriedTasks.values()) {
            Task originalTask = taskMap.get(requeriedTask.Id);

            System.assertEquals(requeriedClients.get(originalTask.WhatId).Intake_Surveys__r.get(0).Id, requeriedTask.WhatId);
        }
    }

//    @isTest
//    private static void changeTaskAccountToQuestionnaire() {
//        Incident__c incident = getIncident();
//        List<Account> clients = getClients();
//
//        List<Intake__c> intakes = buildIntakes(incident, clients, 'Converted - Retainer Received');
//        Database.insert(intakes);
//
//        List<Questionnaire__c> questionnaires = buildQuestionnares(clients);
//        Database.insert(questionnaires);
//
//        List<Task> tasks = buildTasks(clients);
//
//        Test.startTest();
//        Database.insert(tasks);
//        Test.stopTest();
//
//        Map<Id, Task> taskMap = new Map<Id, Task>(tasks);
//        Map<Id, Task> requeriedTasks = new Map<Id, Task>(getTasks());
//        Map<Id, Account> requeriedClients = new Map<Id, Account>(getClients());
//
//        System.assertEquals(taskMap.keySet(), requeriedTasks.keySet());
//        for (Task requeriedTask : requeriedTasks.values()) {
//            Task originalTask = taskMap.get(requeriedTask.Id);
//
//            System.assertEquals(requeriedClients.get(originalTask.WhatId).Questionnares__r.get(0).Id, requeriedTask.WhatId);
//        }
//    }
}