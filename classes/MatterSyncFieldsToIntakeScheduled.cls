/*
    //code to schedule first job
    Integer nextRunInMinutes = 5;

    MatterSyncFieldsToIntakeScheduled syncSched = new MatterSyncFieldsToIntakeScheduled(nextRunInMinutes, DateTime.now().addMinutes(-1*nextRunInMinutes));

    DateTime nextShed = System.now();
    nextShed = nextShed.addMinutes(nextRunInMinutes);
    String chronExp = '' + nextShed.second() + ' ' + nextShed.minute() + ' ' + nextShed.hour() + ' ' + nextShed.day() + ' ' + nextShed.month() + ' ? ' + nextShed.year();
    System.schedule('MatterSyncFieldsToIntakeScheduled ' + nextShed.getTime(), chronExp, syncSched);
*/

global without sharing class MatterSyncFieldsToIntakeScheduled implements Database.Batchable<SObject>, Schedulable {

    private Integer nextRunInMinutes;
    private DateTime startDate;
    private DateTime endDate;

    public MatterSyncFieldsToIntakeScheduled(Integer nextRun, DateTime endOfLastRun) {
        this.nextRunInMinutes = nextRun;
        this.startDate = endOfLastRun;
        this.endDate = DateTime.now().addMinutes(-1*nextRunInMinutes);
    } //constructor

    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('MatterSyncFieldsToIntakeScheduled.start');
        System.debug(DateTime.now());
        System.debug(nextRunInMinutes);
        System.debug(this.startDate);
        System.debug(endDate);

        return Database.getQueryLocator([SELECT parentId, oldValue, newValue, field, createdDate 
            FROM litify_pm__Matter__History 
            WHERE field IN ('litify_pm__Principal_Attorney__c', 'case_manager__c') AND createdDate > :startDate AND createdDate <= :endDate]
        );
    } //start (batch)

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new MatterSyncFieldsToIntakeScheduled(nextRunInMinutes, startDate), 5);
    } //execute scheduled

    global void execute(Database.BatchableContext bc, List<litify_pm__Matter__History> scope) {
        System.debug('MatterSyncFieldsToIntakeScheduled.execute');
        System.debug(DateTime.now());
        System.debug(nextRunInMinutes);
        System.debug(this.startDate);
        System.debug(endDate);

        //try {
            Set<Id> matterIds = new Set<Id>();
            for (litify_pm__Matter__History matterHistory : scope) {
                matterIds.add(matterHistory.parentId);
            }
            //get the matters
            List<litify_pm__Matter__c> matters = [SELECT id, litify_pm__Open_Date__c, litify_pm__Principal_Attorney__c, case_manager__c, intake__c FROM litify_pm__Matter__c WHERE id IN :matterIds];

            //inject the matters
            Matter_Domain domain = new Matter_Domain(matters);

            domain.syncPersonnelToIntake();
        /*} catch (Exception e) {
            //this is to make sure the this batch doesn't keep the next job from getting scheduled
            //this should probably log the error?  alert someone??
        }*/

    } //execute batch

    global void finish(Database.BatchableContext bc) {
        //reschedule the batch job
        MatterSyncFieldsToIntakeScheduled syncSched = new MatterSyncFieldsToIntakeScheduled(this.nextRunInMinutes, endDate);

        DateTime nextShed = System.now();
        nextShed = nextShed.addMinutes(this.nextRunInMinutes);
        String chronExp = '' + nextShed.second() + ' ' + nextShed.minute() + ' ' + nextShed.hour() + ' ' + nextShed.day() + ' ' + nextShed.month() + ' ? ' + nextShed.year();
        if (!Test.isRunningTest()) {
            System.schedule('MatterSyncFieldsToIntakeScheduled ' + nextShed.getTime(), chronExp, syncSched);
        }
    } //finish

} //class