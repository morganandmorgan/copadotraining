/**
 *  This class manages the updates to a Client record to based on Intake record inserts and updates
 */
public class UpdateClientFromIntakeLogic
{
    private static map<SObjectField, SObjectField> fieldComparisonMap;
    private static string accountsSelectorFields;

    static
    {
        fieldComparisonMap = new map<SObjectField, SObjectField>();

        fieldComparisonMap.put( Intake__c.Case_Type__c, Account.Hubspot_Case_Type__pc );
        fieldComparisonMap.put( Intake__c.Litigation__c, Account.Hubspot_Litigation__pc );
        fieldComparisonMap.put( Intake__c.Marketing_Sub_source__c, Account.Last_Intake_Marketing_Subsource__c );
        fieldComparisonMap.put( Intake__c.Marketing_Source__c, Account.Last_Intake_Markting_Source__c );
        fieldComparisonMap.put( Intake__c.Retainer_Received__c, Account.Last_Intake_Retainer_Received__c );
        fieldComparisonMap.put( Intake__c.Retainer_Sent__c, Account.Last_Intake_Retainer_Sent__c );
        fieldComparisonMap.put( Intake__c.Status__c, Account.Last_Intake_Status__c );
        fieldComparisonMap.put( Intake__c.Caller_First_Name__c, Account.Intake_Caller_First_Name__c );
        fieldComparisonMap.put( Intake__c.Caller_Last_Name__c, Account.Intake_Caller_Last_Name__c );
        fieldComparisonMap.put( Intake__c.CreatedDate, Account.Intake_Create_Date__c );

        accountsSelectorFields = '';

        for (SObjectField accountField : fieldComparisonMap.values())
        {
            if ( ! String.isBlank( accountsSelectorFields ) )
            {
                accountsSelectorFields += ', ';
            }
            accountsSelectorFields += accountField;
        }

   }

    public UpdateClientFromIntakeLogic()
    {

    }

    public void execute()
    {
        // if this is not executing in Trigger context, and the trigger is not an AfterInsert or AfterUpdate, then do nothing
        if (! ( trigger.isExecuting
                && trigger.isAfter
                && ( trigger.isInsert || trigger.isUpdate )
              )
           )
        {
            system.debug('execute - abort');
            return;
        }

        system.debug('execute - setup started');

        if (trigger.isUpdate)
        {
            oldVersionIntakeMap.putAll( (map<id, Intake__c>) trigger.oldMap );
        }

        newVersionIntakeMap.putAll( (map<id, Intake__c>) trigger.newMap );

        populateRecordsToWorkWith();

        updatesToParentClientRecords();
    }

    private map<id, Intake__c> oldVersionIntakeMap = new map<id, Intake__c>();

    private map<id, Intake__c> newVersionIntakeMap = new map<id, Intake__c>();

    private map<id, list<Intake__c>> intakesToPerformFieldUpdatesToParentClientByClientIdMap = new map<id, list<Intake__c>>();

    private map<id, Account> clientsToUpdate = new map<id, Account>();

    @TestVisible
    private void populateRecordsToWorkWith()
    {
        system.debug('populateRecordsToWorkWith starts');
        for (Intake__c record : newVersionIntakeMap.values())
        {
            if (oldVersionIntakeMap.containsKey(record.id))
            {
                system.debug( 'record id \''+record.id +'\' is an update.');
                compareToOldAndAddIfNeeded( record );
            }
            else
            {
                // its a new record.
                system.debug( 'record id \''+record.id +'\' is new.');
                addToIntakeByClientIdMap( record );
            }
        }
        system.debug('populateRecordsToWorkWith ends');
    }

    private void compareToOldAndAddIfNeeded( Intake__c record )
    {
        // determine if one of the fields from the field comparsion map has changed
        Intake__c oldVersionOfIntake = oldVersionIntakeMap.get( record.id );

        Boolean isDifferenceFound = false;

        for (SObjectField intakeField : fieldComparisonMap.keySet())
        {
            if ( record.get( intakeField ) != oldVersionOfIntake.get( intakeField ) )
            {
                isDifferenceFound = true;
                break;
            }
        }

        if (isDifferenceFound)
        {
            addToIntakeByClientIdMap( record );
        }
    }

    private void addToIntakeByClientIdMap( Intake__c record )
    {
        if ( ! intakesToPerformFieldUpdatesToParentClientByClientIdMap.containsKey( record.Client__c ))
        {
            intakesToPerformFieldUpdatesToParentClientByClientIdMap.put( record.Client__c, new list<Intake__c>() );
        }

        intakesToPerformFieldUpdatesToParentClientByClientIdMap.get( record.Client__c ).add( record );
    }

    private void compareIntakeFieldsToClientFieldsAndUpdate(Intake__c currentIntake, Account currentAccount)
    {
        system.debug('compareIntakeFieldsToClientFieldsAndUpdate started.' );
        SObjectField accountField = null;

        Boolean isDifferenceFound = false;

        for (SObjectField intakeField : fieldComparisonMap.keySet())
        {
            accountField = fieldComparisonMap.get( intakeField );

            if (Account.Intake_Create_Date__c == accountField
                    && ! oldVersionIntakeMap.containsKey(currentIntake.id) // this is only true if the record is inserted
                    )
            {
                currentAccount.Intake_Create_Date__c = datetime.now();
                isDifferenceFound = true;
            }
            else if ( currentIntake.get( intakeField ) != currentAccount.get( accountField ) )
            {
                currentAccount.put( accountField, currentIntake.get( intakeField ) );
                isDifferenceFound = true;
            }
        }

        if (isDifferenceFound)
        {
            system.debug('isDifferenceFound found.' );
            clientsToUpdate.put( currentAccount.id, currentAccount );
        }
        system.debug('compareIntakeFieldsToClientFieldsAndUpdate ends.' );
    }

    @TestVisible
    private void updatesToParentClientRecords()
    {
        system.debug('updatesToParentClientRecords started.' );
		if ( ! intakesToPerformFieldUpdatesToParentClientByClientIdMap.isEmpty() )
		{
		    system.debug('intakesToPerformFieldUpdatesToParentClientByClientIdMap is not empty.' );
		    // organize the intakes in to a map by client id
		    // find the account ids
		    // query for the accounts with required fields
		    map<id, Account> clientsOfIntakesMap = new map<id, Account>( selectAccountsById( intakesToPerformFieldUpdatesToParentClientByClientIdMap.keyset() ) );

		    Account currentAccount = null;

		    // loop through the account ids and update as necessary
		    for (Id accountId : intakesToPerformFieldUpdatesToParentClientByClientIdMap.keyset())
		    {
		        if ( clientsOfIntakesMap.containsKey( accountId ) )
		        {
		            currentAccount = clientsOfIntakesMap.get( accountId );

		            for (Intake__c anIntake : intakesToPerformFieldUpdatesToParentClientByClientIdMap.get( accountId ))
		            {
		                compareIntakeFieldsToClientFieldsAndUpdate(anIntake, currentAccount);
		            }
		        }
		    }

		    system.debug('clientsToUpdate size = '+clientsToUpdate.size());
		    update clientsToUpdate.values();
		}
        system.debug('updatesToParentClientRecords ends.' );
    }

    @TestVisible
    private list<Account> selectAccountsById( Set<id> idSet )
    {
        string query = 'select Id, {0} from Account where id in :idSet'.replace('{0}', accountsSelectorFields);

        return (list<Account>) database.query( query );
    }
}