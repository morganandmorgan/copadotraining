public interface mmmarketing_ISources extends fflib_ISObjectDomain
{
    void autoLinkMarketingInformationToSourceIfRequired();
    void autoLinkMarketingFinancialPeriodsToSourceIfRequired();
}