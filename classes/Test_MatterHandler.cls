@isTest
private class Test_MatterHandler {

    @testSetup static void setupRecords() {
        RecordType matterRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Matter__c'
                                        AND DeveloperName = 'Personal_Injury'
                                    ];
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account'
                                    AND DeveloperName = 'Person_Account'
                                    LIMIT 1
                                ];

        List < Account > acc = new List < account > {
            new Account(LastName = 'TestAcc 1', RecordTypeId = acctRcdType.Id)
        };
        Insert acc;

        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c(Name = 'Slip and Fall'); 
        insert caseType;

        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c(Name = 'Slip and Fall');
        matterPlan.litify_pm__Case_Type__c = caseType.Id;
        insert matterPlan;

        litify_pm__Case_Type__c caseType1 = new litify_pm__Case_Type__c(Name = 'Automobile Accident');
        insert caseType1;

        litify_pm__Matter_Plan__c matterPlan1 = new litify_pm__Matter_Plan__c(Name = 'Automobile Accident');
        matterPlan.litify_pm__Case_Type__c = caseType1.Id;
        insert matterPlan1;

        User testUser = createUser();
        insert testUser;

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = matterRcdType.Id;
        matter.ReferenceNumber__c = '999999056';
        matter.litify_pm__Client__c = acc[0].Id;
        matter.litify_pm__Principal_Attorney__c = testUser.Id;
        matter.litify_pm__Open_Date__c = Date.today();
        matter.litify_pm__Case_Type__c = caseType.Id;
        insert matter;

        matter.litify_pm__Case_Type__c = caseType1.Id;
        update matter;

        RecordType roleRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Role__c'
                                    AND DeveloperName = 'Matter_Role'
                                ];  
        litify_pm__Role__c role = new litify_pm__Role__c();
        role.RecordTypeId = roleRcdType.Id;
        role.litify_pm__Matter__c = matter.Id;
        role.litify_pm__Party__c = acc[0].Id;
        insert role;

        litify_pm__Request__c infoObj = new litify_pm__Request__c(Request__c = '911 Tapes', litify_pm__Matter__c = matter.Id);
        insert infoObj;

    }

    @isTest static void test_matter_handler() {

        Test.startTest();

        litify_pm__Matter__c matterObj = [SELECT Id, Case_Type_Name__c FROM litify_pm__Matter__c][0];
        litify_pm__Case_Type__c caseType = [SELECT Id, Name FROM litify_pm__Case_Type__c WHERE Name = 'Automobile Accident'][0];
        litify_pm__Matter_Plan__c matterPlan = [SELECT Id, Name FROM litify_pm__Matter_Plan__c WHERE Name = 'Automobile Accident'][0];

        List < Matter_Information_Request_Setting__mdt > mappingList = [SELECT Id, MasterLabel, Document_Link__c, Case_Type__c from Matter_Information_Request_Setting__mdt ORDER BY MasterLabel];
        
        litify_pm__Request__c infoObj = [SELECT Id, Request__c, Status__c, litify_pm__Matter__c, litify_pm__Document__c, litify_pm__Date_Requested__c FROM litify_pm__Request__c WHERE litify_pm__Matter__c =: matterObj.Id LIMIT 1];
        
        //getFieldsToMapFromCMT

        Map < String, String > fieldsToMap = MatterHandler.getFieldsToMapFromCMT();

        // fetching Matched values from CMT 

        MatterHandler.fetchCMT(matterObj, new Map < String, Set < String >> (), new Map < String, Map < String, String >> (), fieldsToMap);

        // creating new informationrecords if they doesn't already Exist.
        MatterHandler.checkAndCreateNewInfos(mappingList,
            new List < litify_pm__Request__c > {
                infoObj
            },
            new Set < String > {
                '911 Tapes',
                'Police Report'
            },
            matterObj.Id);

        // fetch and create info records
        MatterHandler.fetchRecords(matterObj, true);

        // Creating Info records on Matter insert

        List < litify_pm__Matter__c > matterList1 = new List < litify_pm__Matter__c > {
            matterObj
        };

        MatterHandler.createInfoRecords(
            mappingList,
            new Map < String, Map < String, String >> {
                matterObj.Id => new Map < String,
                String > {
                    'Case_Type_Name__c,Case_Type__c' => 'Slip and Fall'
                }
            },
            matterList1,
            false
        );

        MatterHandler.createInfoRecords(
            mappingList,
            new Map < String, Map < String, String >> {
                matterObj.Id => new Map < String,
                String > {
                    'Case_Type_Name__c,Case_Type__c' => 'XYZ'
                }
            },
            new List < litify_pm__Matter__c > {
                matterObj
            },
            false
        );

        MatterHandler.createInfoRecords(
            mappingList,
            new Map < String, Map < String, String >> {
                matterObj.Id => new Map < String,
                String > {
                    'Case_Type_Name__c,Case_Type__c' => 'Slip and Fall'
                }
            },
            new List < litify_pm__Matter__c > {
                matterObj
            },
            true
        );

        matterObj.litify_pm__Case_Type__c = caseType.Id;
        update matterObj;

        Test.stopTest();
    }

    public static User createUser() {
        Integer userNum = 1;
        return new User(
            IsActive = true,
            FirstName = 'FirstName ' + userNum,
            LastName = 'LastName ' + userNum,
            //Username = 'apex.temp.test.user@sample.com.' + userNum,
            Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@xyz.com',
            Email = 'apex.temp.test.user@sample.com.' + userNum,
            Alias = 'alib' + userNum,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            Territory__c = null
        );
    }

}