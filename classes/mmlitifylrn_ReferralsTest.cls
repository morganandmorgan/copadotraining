@isTest
private class mmlitifylrn_ReferralsTest
{
    @isTest
    private static void simpleInsertOfReferralTest()
    {


        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c();

        caseType.Name = 'Dog Bite';
        caseType.litify_pm__ExternalId__c = 17.0;
        caseType.litify_pm__Litigation_Type__c = null;
        //caseType.litify_pm__Litify_com_ID__c = 17;
        caseType.litify_pm__Is_Available__c = true;
        //caseType.litify_pm__Is_Local__c = false;
        caseType.litify_pm__LRN_Case_Type__c = null;
        //caseType.litify_pm__Mapped_to_ExternalId__c = 17.0;

        insert caseType;



        litify_pm__Referral__c referral = new litify_pm__Referral__c();

        referral.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName( litify_pm__Referral__c.SObjectType, 'Outgoing_Referral' );

        referral.litify_pm__Case_Type__c = caseType.id;

        referral.litify_pm__Client_First_Name__c = 'John';
        referral.litify_pm__Client_Last_Name__c = 'Morgan';
        referral.litify_pm__Client_email__c = 'info@forthepeople.com';
        referral.litify_pm__Client_phone__c = '(407) 420-1414';

        referral.litify_pm__Client_Address_1__c = '20 N Orange Ave';
        referral.litify_pm__Client_Address_2__c = 'Suite 1600';
        referral.litify_pm__Client_City__c = 'Orlando';
        referral.litify_pm__Client_State__c = 'FL';
        referral.litify_pm__Client_Postal_Code__c = '32801';
        referral.litify_pm__Description__c = 'blah blah blah';
        //referral.litify_pm__ExternalId__c
        //referral.litify_pm__External_Ref_Number__c
        //referral.litify_pm__Extra_info__c
        referral.litify_pm__Handling_Firm__c = null;
        referral.litify_pm__Incident_date__c = date.today();
        referral.litify_pm__Originating_Firm__c = null;
        //referral.litify_pm__Referral_Kind__c
        referral.litify_pm__Status__c = 'draft';
        referral.litify_pm__Suggested_Percentage_fee__c = 0.4;
        //referral.litify_pm__sync_fail_count__c
        //referral.litify_pm__sync_msg__c
        //referral.litify_pm__sync_status__c
        //referral.Intake__c
        //referral.litify_pm__Status_Description__c
        //referral.litify_pm__Sync_last_updated_at__c
        //referral.Check_Amount__c
        //referral.Check_Number__c
        //referral.Check_Received_Date__c
        //referral.Referral_Transaction_Status__c
        //referral.litify_pm__Intake__c
        //referral.litify_pm__LRN_Created_At__c
        //referral.litify_pm__Litify_Status__c
        //referral.litify_pm__Referrals__c
        //referral.litify_pm__Sync_Status_Description__c
        //referral.litify_pm__Agreement_Name__c
        //referral.litify_pm__Agreement_Type__c
        //referral.litify_pm__Expires_At__c
        //referral.litify_pm__Investigated_at__c
        //referral.litify_pm__Is_Anonymous__c
        //referral.litify_pm__Matter__c
        //referral.litify_pm__Signed_up_at__c
        //referral.mmlitifylrn_CaseTypeExternalId__c
        //referral.mmlitifylrn_CaseTypeName__c

        insert referral;
    }
}