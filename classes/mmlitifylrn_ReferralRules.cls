public class mmlitifylrn_ReferralRules
    extends fflib_SObjectDomain
    implements mmlitifylrn_IReferralRules
{
    public static mmlitifylrn_IReferralRules newInstance(List<ReferralRule__c> records)
    {
        return (mmlitifylrn_IReferralRules) mm_Application.Domain.newInstance(records);
    }

    public static mmlitifylrn_IReferralRules newInstance(Set<Id> recordIds)
    {
        return (mmlitifylrn_IReferralRules) mm_Application.Domain.newInstance(recordIds);
    }

    public mmlitifylrn_ReferralRules(List<ReferralRule__c> records)
    {
        super(records);
    }

    public id findHandlingFirmIdForIntake( Intake__c intake )
    {
        id handlingFirmId = null;

        mmlitifylrn_ReferralRulesMatchLogic matchLogic = new mmlitifylrn_ReferralRulesMatchLogic( Intake__c.SObjectType );

        // loop through the referral records
        for ( ReferralRule__c rule : (list<ReferralRule__c>)this.records )
        {
            // only evaluate the referral rule if the rule's originating firm matches the intake's ????????????

            //given the intake query string
            if ( matchLogic.criteriaMatches( rule, intake ) )
            {
                handlingFirmId = rule.HandlingFirm__c;
                break;
            }
        }

        return handlingFirmId;
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new mmlitifylrn_ReferralRules(sObjectList);
        }
    }
}