/*============================================================================
Name            : TransactionLineItem_Domain
Author          : CLD
Created Date    : Sep 2018
Description     : Domain class for Transaction Line Item
=============================================================================*/
public class TransactionLineItem_Domain extends fflib_SObjectDomain {

    private List<c2g__codaTransactionLineItem__c> transLineItems;
    private TransactionLineItem_Service tliService;

    // Ctors
    public TransactionLineItem_Domain() {
        super();
        this.transLineItems = new List<c2g__codaTransactionLineItem__c>();
        this.tliService = new TransactionLineItem_Service();
    }

    public TransactionLineItem_Domain(List<c2g__codaTransactionLineItem__c> transLineItems) {
        super(transLineItems);
        this.transLineItems = (List<c2g__codaTransactionLineItem__c>) records;
        this.tliService = new TransactionLineItem_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new TransactionLineItem_Domain(records);
        }
    }

    // Trigger Handlers
    public override void onBeforeInsert() {
        //TO DO: check if specific specific trigger context is disabled

        tliService.populateMatterField(transLineItems);
    }

    public override void onAfterInsert() {
        //TO DO: check if specific specific trigger context is disabled
        tliService.createTrustTransactions(transLineItems);
    }
   
}