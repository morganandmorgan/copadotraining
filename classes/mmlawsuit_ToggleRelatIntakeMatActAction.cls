/**
 *  Domain action used to take the qualified Lawsuit records and find
 *  their related intake records and then use the Intakes domain method
 *  to toggle the intakes' related matter active flag
 */
public class mmlawsuit_ToggleRelatIntakeMatActAction
    extends mmlib_AbstractAction
{
    public override Boolean runInProcess()
    {
        mmlib_ISobjectDomain intakes = (mmlib_ISobjectDomain) mm_Application.Domain.newInstance( mmlib_Utils.generateIdSetFromField( this.records, Lawsuit__c.Intake__c) );

        intakes.processDomainLogicInjections( 'toggleIsMatterActive', this.uow );

        return true;
    }
}