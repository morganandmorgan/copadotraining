@isTest
global class mmlib_TinyUrlMock
	implements HttpCalloutMock
{
	private String responseUrl = '';

	public mmlib_TinyUrlMock(String responseUrl)
	{
		this.responseUrl = responseUrl;
	}

    global HTTPResponse respond(HTTPRequest req)
	{
        HttpResponse res = new HttpResponse();
        res.setBody(responseUrl);
        res.setStatusCode(200);
        return res;
    }
}