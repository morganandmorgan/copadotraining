public class mmsms_SmsIntegrationExceptions
{
    private mmsms_SmsIntegrationExceptions()
	{
		// Hide from consumer.
	}

    public virtual class ServiceException
        extends Exception
    {
		// No code.
    }

	public class ExecutionException
        extends mmsms_SmsIntegrationExceptions.ServiceException
    {
		// No code.
    }
}