public with sharing class mmcommon_UsersSelector
    extends mmlib_SObjectSelector
    implements mmcommon_IUsersSelector
{
    public static mmcommon_IUsersSelector newInstance()
    {
        return (mmcommon_IUsersSelector) mm_Application.Selector.newInstance(User.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return User.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            User.Language__c,
            User.Location__c,
            User.Notes__c,
            User.Phone_Unformatted_Formula__c,
            User.Skill__c,
            User.Tags__c,
            User.Territory__c
        };
    }

    public List<User> selectById(Set<Id> idSet)
    {
        return (List<User>) selectSObjectsById(idSet);
    }

    public List<User> selectAllActiveInvestigators()
    {
        return Database.query( newQueryFactory().setCondition('IsActive = true AND '+ User.Territory__c + ' != null').toSOQL() );
    }

    /**
     *  Note: Using direct inline SOQL in the query.  The reason is that the fflib_QueryFactory will try to pull
     *      certain FieldDescribes on the Profile and other system objects.  That is fine for System Admin level
     *      profile users but it fails for non-system admin users.  This inline SOQL is more direct and bypasses
     *      that check in fflib_QueryFactory that would fail for non-system admins.
     */
    public List<User> selectByIdWithProfileRolePermissionSets( Set<Id> idSet )
    {
        return [select id, name, username, email, Skill__c
                     , ( select Id, PermissionSetId, PermissionSet.Id, PermissionSet.Name, PermissionSet.Label, PermissionSet.Description, PermissionSet.CreatedDate
                              , PermissionSet.CreatedById, PermissionSet.LastModifiedDate, PermissionSet.LastModifiedById, PermissionSet.SystemModstamp, PermissionSet.NamespacePrefix
                              , PermissionSet.IsOwnedByProfile, PermissionSet.Profile.Name
                           from PermissionSetAssignments )
                     , Profile.Id, Profile.Name, Profile.CreatedById, Profile.CreatedDate, Profile.Description, Profile.LastModifiedById, Profile.LastModifiedDate
                     , Profile.LastReferencedDate, Profile.LastViewedDate, Profile.SystemModstamp, Profile.UserLicenseId, Profile.UserType
                     , UserRole.Id, UserRole.Name, UserRole.ParentRoleId, UserRole.RollupDescription, UserRole.OpportunityAccessForAccountOwner
                     , UserRole.CaseAccessForAccountOwner, UserRole.ContactAccessForAccountOwner, UserRole.ForecastUserId, UserRole.MayForecastManagerShare
                     , UserRole.LastModifiedDate, UserRole.LastModifiedById, UserRole.SystemModstamp, UserRole.DeveloperName, UserRole.PortalAccountId
                     , UserRole.PortalType, UserRole.PortalAccountOwnerId
                  from user
                 where id in :idSet
                ];

    }
    
    public List<User> selectByFuzzyMatching(mmcommon_IUsersSelectorEmployeeRequest request) {
        List<String> whereClauseComponents = new List<String>();

        if (!String.isEmpty(request.getFirstName()))
        {
            whereClauseComponents.add(User.FirstName + ' like \'' + String.escapeSingleQuotes(request.getFirstName()) + '%\'');
        }

        if (!String.isEmpty(request.getLastName()))
        {
            whereClauseComponents.add(User.LastName + ' like \'' + String.escapeSingleQuotes(request.getLastName()) + '%\'');
        }

        if (!String.isEmpty(request.getPhoneNumber()))
        {
            whereClauseComponents.add(User.Phone_Unformatted_Formula__c + ' = \'' + String.escapeSingleQuotes(request.getPhoneNumber()) + '\'');
        }

        if (!String.isEmpty(request.getExtension()))
        {
            whereClauseComponents.add(User.Extension + ' = \'' + String.escapeSingleQuotes(request.getExtension()) + '\'');
        }

        if (!String.isEmpty(request.getDepartment()))
        {
            whereClauseComponents.add(User.Department + ' like \'%' + String.escapeSingleQuotes(request.getDepartment()) + '%\'');
        }

        if (!String.isEmpty(request.getLocation()))
        {
            whereClauseComponents.add(User.Location__c + ' like \'' + String.escapeSingleQuotes(request.getLocation()) + '%\'');
        }

        if (request.getTags() != null && !request.getTags().isEmpty())
        {
            List<String> tagClause = new List<String>();
            for (String s : request.getTags()) {
                tagClause.add(User.Tags__c + ' like \'%' + String.escapeSingleQuotes(s) + '%\'');
            }
            whereClauseComponents.add('(' + String.join(tagClause, ' or ') + ')');
        }

        return
            (List<User>)
            Database.query(
                newQueryFactory()
                .setCondition(String.join(whereClauseComponents, ' AND '))
                .setLimit(10)
                .toSOQL()
            );
    }

    public List<User> selectByUsernameLikeFields(Set<String> userNames)
    {
        return 
            Database.query(
                newQueryFactory()
                    .setCondition(
                        User.IsActive + ' = true AND ' + 
                        '(' + User.Username + ' in :userNames OR ' +
                        User.Email + ' in :userNames OR ' +
                        User.FederationIdentifier + ' in :userNames)')
                    .toSOQL());
    }
}