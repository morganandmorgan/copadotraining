/**
 * TriggerControlsSelector
 * @description Selector for Trigger Controls.
 * @author Jeff Watson
 * @date 1/2/2019
 */

public with sharing class TriggerControlsSelector {

    private static List<String> queryFields = new List<String>{
            'Id',
            'Accounts__c',
            'All_Triggers__c',
            'Coverage__c',
            'Damage_Payment__c',
            'Docusign_Status__c',
            'Expense_Summaries__c',
            'Expense_Minutes_Back_To_Process__c',
            'Incident_Investigations__c',
            'Intake_Investigations__c',
            'Intakes__c',
            'Investigations__c',
            'Litify_Expenses__c',
            'Litify_Matters__c',
            'Litify_Referrals__c',
            'Litify_Roles__c',
            'Litify_Team_Members__c',
            'Matter_Compute_SOL__c',
            'Matter_Deceased_Per_Role__c',
            'Matter_Task_Histories__c',
            'Platform_Events__c',
            'Spring_Accounts__c',
            'Tasks__c'
    };

    public static Trigger_Control__c selectDefaultControls() {

        String queryStr = 'SELECT ' + String.join(queryFields, ',') + ' FROM Trigger_Control__c where Name = \'Default Controls\'';
        List<Trigger_Control__c> triggerControls = Database.query(queryStr);

        if(triggerControls.size() > 0) {
            return triggerControls[0];
        }

        return null;
    }
}