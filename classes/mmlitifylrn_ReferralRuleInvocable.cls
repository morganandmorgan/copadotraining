public with sharing class mmlitifylrn_ReferralRuleInvocable
{
    @InvocableMethod( label='Run Referral Rule Processor'
                      description='Processes Referral Rules and returns the ID of the handling firm that matches the rules or null')
    public static list<Id> ProcessRules(List<Id> intakeIds)
    {
        set<Id> handlingFirmIdSet = new set<Id>();

        if (intakeIds == null || intakeIds.isEmpty())
        {
            handlingFirmIdSet.add( null );
        }
        else
        {
            mmlitifylrn_IReferralRules referralRules = mmlitifylrn_ReferralRules.newInstance( mmlitifylrn_ReferralRulesSelector.newInstance().selectAll() );

            list<SObject> intakes = mm_Application.Selector.selectById( new set<Id>( intakeIds ) );

            for (SObject intake : intakes)
            {
                handlingFirmIdSet.add( referralRules.findHandlingFirmIdForIntake( (Intake__c)intake ) );
            }
        }

        return new list<id>( handlingFirmIdSet );
    }
}