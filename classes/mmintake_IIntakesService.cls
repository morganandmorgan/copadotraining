/**
 *  mmintake_IIntakesService
 */
public interface mmintake_IIntakesService
{
    Map<SObjectType, List<SObject>> createWithRelatedAccount(Map<SObjectField, String> dataMap);
    void saveRecords( list<Intake__c> records, Boolean validateAccountRoles );
}