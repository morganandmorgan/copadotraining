@isTest
private class IntakeRepNavigator_JsonTest {
	
	@isTest static void ParseQuery_PassedNull_ReturnsEmptyList() {
		List<IntakeRepNavigator_Json.Query> qrs = IntakeRepNavigator_Json.ParseQuery(null);
    System.assertEquals(0, qrs.size());
	}
	
	@isTest static void ParseSortOrder_PassedNull_ReturnsEmptyList() {
    List<IntakeRepNavigator_Json.SortOrder> sos = IntakeRepNavigator_Json.ParseSortOrder(null);
    System.assertEquals(0, sos.size());
	}
	
}