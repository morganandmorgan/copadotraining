public class mmesc_EmployeeSearchInputController
{
    @AuraEnabled
    public static List<UserResult> searchForUsers(Map<String, String> data)
    {
        List<UserResult> ret = new List<UserResult>();
        for (User u : mmcommon_UsersService.searchUsersUsingFuzzyMatching(data)) {
            ret.add(new UserResult(u));
        }

        return ret;
    }

    public class UserResult {
        @AuraEnabled public Id userId;
        @AuraEnabled public String name;
        @AuraEnabled public String department;
        @AuraEnabled public String keyword;
        @AuraEnabled public String extension;
        @AuraEnabled public String phoneNumber;
        @AuraEnabled public String location;
        @AuraEnabled public String title;
        @AuraEnabled public String notes;
        public UserResult(User u) {
            this.userId = u.Id;
            this.name = u.Name;
            this.department = u.Department;
            this.location = u.Location__c;
            this.extension = u.Extension;
            this.phoneNumber = u.Phone;
            this.title = u.Title;
            this.notes = u.Notes__c;
        }
    }
}