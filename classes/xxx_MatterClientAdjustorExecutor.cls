/**
 *  simple script to correctly realign the client field on a a matter record based on a specific set of circumstances
 *
 *  @usage String query = 'select Id, name, litify_pm__Client__c, litify_pm__Client__r.CreatedById, litify_pm__Client__r.CreatedDate, Related_Lawsuit__c, Related_Lawsuit__r.AccountID__c, Intake__c, Intake__r.Client__c from litify_pm__Matter__c where Related_Lawsuit__c != null and Intake__c != null';
 *         new mmlib_GenericBatch( xxx_MatterClientAdjustorExecutor.class, query).setBatchSizeTo(100).execute();
 */
public class xxx_MatterClientAdjustorExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        list<litify_pm__Matter__c> mattersToUpdate  = new list<litify_pm__Matter__c>();
        litify_pm__Matter__c matter = null;
        for ( SObject record : scope )
        {
            matter = (litify_pm__Matter__c)record;
            if ( matter.litify_pm__Client__c != '0011J00001EAdja' )
            {
                continue;
            }
            if ( matter.Related_Lawsuit__r.AccountID__c == null
                && matter.litify_pm__Client__c != matter.Intake__r.Client__c
                )
            {
                matter.litify_pm__Client__c = matter.Intake__r.Client__c;
                mattersToUpdate.add( matter );
            }
            else if ( matter.litify_pm__Client__c != matter.Related_Lawsuit__r.AccountID__c
                && matter.litify_pm__Client__c != matter.Intake__r.Client__c
                && matter.Related_Lawsuit__r.AccountID__c == matter.Intake__r.Client__c
                )
            {
                matter.litify_pm__Client__c = matter.Intake__r.Client__c;
                mattersToUpdate.add( matter );
            }

            //if ( mattersToUpdate.size() >= 100 )
            //{
            //  break;
            //}
        }

        system.debug( mattersToUpdate.size() );

        database.update( mattersToUpdate, false );
    }
}