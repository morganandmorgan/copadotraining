public class DocsHelper {

    public String sessionId; //if this is called from lightning or @future, we'll have to pass a valid sessionId through to the API
    
    /*
		Source Ids must include a 'litify_docs__Template__c', 'litify_pm__Matter__c' and any other object required by the template
		A copy of the template will be created for each roleId
    */   
    public void multiMergeOnRoles(Map<String,Id> sourceIds, Set<Id> roleIds) {
        //get the inputs for the request template
    	List<litify_docs__Input__c> inputs = getTemplateInputs( sourceIds.get('litify_docs__Template__c'));
        
        //get a mapping from tag to field for each object
        Map<String,List<TagInformation>> tagInfo = buildTagMaps(inputs);
                
		List<DocsApi.MergeData> mergeData = new List<DocsApi.MergeData>();
        
        for (Id roleId : roleIds) {

            sourceIds.put('litify_pm__Role__c', roleId);

            DocsApi.MergeData md = new DocsApi.mergeData();
            md.relatedToId = sourceIds.get('litify_pm__Matter__c');
            md.relatedToApiName = 'litify_pm__Matter__c';
            md.category = 'Correspondence';
            md.subcategory = '';
            md.fileType = DocsApi.defaultFileType;
            md.name = generateFileName(sourceIds);
            md.templateId = sourceIds.get('litify_docs__Template__c');

            //build the queries to get the data for this merge
            Map<String,String> queries = buildQueries(tagInfo, sourceIds);
            
            //generate all the tag data from the data, for the merge
            md.tags = generateTags(tagInfo, queries, sourceIds);

            mergeData.add(md);
        
        } //for roles
        
        if (sessionId != null) {
            DocsApi.sessionId = sessionId;
        }
        DocsApi.multiMerge(mergeData);
        
    } //multiMergeOnRoles
    

    public Id singleMerge(Map<String,Id> sourceIds, String filename) {
        //get the inputs for the request template
    	List<litify_docs__Input__c> inputs = getTemplateInputs( sourceIds.get('litify_docs__Template__c'));
        
        //get a mapping from tag to field for each object
        Map<String,List<TagInformation>> tagInfo = buildTagMaps(inputs);
                
		List<DocsApi.MergeData> mergeData = new List<DocsApi.MergeData>();

        DocsApi.MergeData md = new DocsApi.mergeData();
        md.relatedToId = sourceIds.get('litify_pm__Matter__c');
        md.relatedToApiName = 'litify_pm__Matter__c';
        md.category = 'Correspondence';
        md.subcategory = '';
        md.fileType = DocsApi.defaultFileType;
        md.name = filename;
        md.templateId = sourceIds.get('litify_docs__Template__c');

        //build the queries to get the data for this merge
        Map<String,String> queries = buildQueries(tagInfo, sourceIds);
        
        //generate all the tag data from the data, for the merge
        md.tags = generateTags(tagInfo, queries, sourceIds);

        mergeData.add(md);

        if (sessionId != null) {
            DocsApi.sessionId = sessionId;
        }

        List<Id> recordIds = DocsApi.multiMerge(mergeData);

        return recordIds[0];
    } //singleMerge

    
    private String generateFileName(Map<String,Id> sourceIds) {

        String fileName;

        //we need the To name
        Id roleId = sourceIds.get('litify_pm__Role__c');
        List<litify_pm__Role__c> toRole = [SELECT id, litify_pm__Party__r.Name FROM litify_pm__Role__c WHERE id = :roleId];
        if (toRole.size() == 1) {
            fileName = toRole[0].litify_pm__Party__r.Name;
        } else {
            fileName = '';
        }

        //we need the From name
        Id tmId = sourceIds.get('litify_pm__Matter_Team_Member__c');
        list<litify_pm__Matter_Team_Member__c> fromTm = [SELECT id, litify_pm__User__r.Name FROM litify_pm__Matter_Team_Member__c WHERE id = :tmId];
        if (fromTm.size() == 1) {
            fileName = fileName + '||' + fromTm[0].litify_pm__User__r.Name;
        } else {
            fileName = fileName + '';
        }

        return fileName + '.docx';

    } //generateFileName

    private List<litify_docs__Input__c> getTemplateInputs(Id templateId) {

        //get a set of input groups attached to this template
        Set<id> inputGroupIds = new Set<id>();
        for (litify_docs__Template_Node_Junction__c t2ig : [SELECT id, litify_docs__Node__c FROM litify_docs__Template_Node_Junction__c WHERE litify_docs__Template__c = :templateId]) {
            inputGroupIds.add(t2ig.litify_docs__Node__c);
        }
        
        //get a set of the inputs
        Set<id> inputIds = new Set<id>();
        for (litify_docs__Node_Input_Junction__c ig2i : [SELECT id, litify_docs__Input__c FROM litify_docs__Node_Input_Junction__c WHERE litify_docs__Node__c IN :inputGroupIds]) {
			inputIds.add(ig2i.litify_docs__Input__c);
        }
        
        //look for parent inputs
        Set<Id> parentIds = new Set<Id>();
        Set<Id> rootIds = new Set<Id>();
        for (litify_docs__Input__c input: [SELECT id, name, litify_docs__Type__c, litify_docs__Input_Number__c, litify_docs__Input_Type__c, litify_docs__Object_Field_Name__c FROM litify_docs__Input__c WHERE id IN :inputIds]) {
            if (input.litify_docs__Input_Type__c.left(17) == 'Salesforce Record') {
                parentIds.add(input.id);
            } else {
                rootIds.add(input.id);
            }
        }
        
		return [SELECT id, name, litify_docs__Child_Relationship_Api_Name__c, litify_docs__Current_Date__c, litify_docs__Type__c, litify_docs__Input_Number__c, litify_docs__Object_Field_Name__c, litify_docs__Starting_Object__c FROM litify_docs__Input__c WHERE id IN :rootIds OR litify_docs__Parent_Input__c IN :parentIds];
        
    } //getTemplateInputs
    

    private Map<Id,String> getFormatInformation(List<litify_docs__Input__c> inputs) {

        Map<Id,litify_docs__Input__c> inputMap = new Map<Id,litify_docs__Input__c>(inputs);

        Map<Id,String> formatInfo = new Map<Id,String>();
            
        for (litify_docs__Input_Format_Option__c format : [SELECT id, litify_docs__Format_Type__c, litify_docs__Input__c FROM litify_docs__Input_Format_Option__c WHERE litify_docs__Input__c IN :inputMap.keySet()]) {
            formatInfo.put(format.litify_docs__Input__c, format.litify_docs__Format_Type__c);
        }

        return formatInfo;

    } //getFormatInformation


    private class TagInformation {
        public String tag {get;set;}
        public String field {get;set;}
        public String format {get;set;}
    }

    /* builds a map, of lists of TagInformation
        Map<'baseobject', List<TagInformation>>
    */
    private Map<String,List<TagInformation>> buildTagMaps(List<litify_docs__Input__c> inputs) {
        Map<Id,String> formatInfo = getFormatInformation(inputs);
        Map<String,List<TagInformation>> tagInfo = new Map<String,List<TagInformation>>();
        String baseObject;
        for (litify_docs__Input__c input : inputs) {
			//build the tag
			String tag = input.name.replaceAll('[^a-zA-Z0-9\\s+]', '');
			tag = tag.replace(' ','_');
            tag = tag.toLowerCase();            
            Integer tagNumber = Integer.valueOf(input.litify_docs__Input_Number__c); //this should remove the leading 0's
            tag = tag + '_' + String.valueOf(tagNumber);
            
            
            if (String.isBlank(input.litify_docs__Child_Relationship_Api_Name__c)) {
				baseObject = input.litify_docs__Starting_Object__c;
            } else {
                // Due to some bad naming, we have to get hacky here...
                baseObject = input.litify_docs__Child_Relationship_Api_Name__c;

				//fix for team members
				baseObject = baseObject.replace('Teams__r','Team_Member__c');
                
                //fix for generic pluralization
                baseObject = baseObject.replace('s__r','__c');
                
                //just in case they didn't pluralize it
                baseObject = baseObject.replace('__r','__c');
            }

            if (!tagInfo.containsKey(baseObject)) {
                tagInfo.put(baseObject, new List<TagInformation>());
            }
            
            TagInformation oneTag = new TagInformation();

            oneTag.tag = tag;
            if (!input.litify_docs__Current_Date__c) {
                oneTag.field = input.litify_docs__Object_Field_Name__c;
            } else {
                oneTag.field = 'currentDate';
            }

            if (formatInfo.containsKey(input.id)) {
                oneTag.format = formatInfo.get(input.id);
            }

            tagInfo.get(baseObject).add(oneTag);

        } //for inputs
        
        return tagInfo;
    } //buildTagInfo
    

    private Map<String,String> buildQueries(Map<String,List<TagInformation>> tagInfo, Map<String,Id> sourceIds) {
        Map<String,String> queries = new Map<String,String>();
        for (String baseObject : tagInfo.keySet()) {
            //get a unique set of the fields
            Set<String> fields = new Set<String>();
            for (TagInformation oneTag : tagInfo.get(baseObject)) {
                if (!String.isBlank(oneTag.field) && oneTag.field != 'currentDate') {
                	fields.add(oneTag.field);
                }
            }
            //build the select
            String query = 'SELECT ';
            String delimiter = '';
            for (String field : fields) {
                query = query + delimiter + field;
                delimiter = ',';
            }
            //build the rest of the query
            query = query + ' FROM ' + baseObject + ' WHERE id = \'' + sourceIds.get(baseObject) + '\'';
            queries.put(baseObject,query);
        }
        return queries;
    } //buildQueries
    
    
    //stolen from the AuditEvalEngine
    private Object getField(SObject obj, String fieldName) {

        if ( fieldName.contains('.') ) {
            String objName = fieldName.split('\\.')[0];
            //Out of the box relationships work... wierd
            //OwnerId is the fields name, but Owner.name is the related fields reference, so lets account for both
            if ( objName.right(2).toLowerCase() == 'id') {
                objName = objName.toLowerCase().replace('id','');
            }
            String objField = fieldName.split('\\.')[1];

            //make sure there is a related record
            if ( obj.getSObject(objName) != null) {
                return obj.getSObject(objName).get(objField);
            } else {
                return null;
            }
        } else {
            return obj.get(fieldName);
        }

    } //getField


    private Map<String,Object> generateTags(Map<String,List<TagInformation>> tagInfo, Map<String,String> queries, Map<String,Id> sourceIds) {
        
        Map<String,SObject> data = new Map<String,SObject>();
        for (String baseObject : queries.keySet()) {
            SObject obj = Database.query(queries.get(baseObject));
            data.put(baseObject, obj);
        }
             
        Map<String,Object> tags = new Map<String,Object>();
        for (String baseObject : tagInfo.keySet()) {
            for (TagInformation oneTag : tagInfo.get(baseObject)) {
                SObject obj = data.get(baseObject);
                
                if (oneTag.field != null) {
                    Object value;                    
                    if (oneTag.field != 'currentDate') {
                        value = getField(obj, oneTag.field);
                    } else {
                        value = Date.today();
                    }

                    if ( oneTag.format != 'format_pdf417' ) {
                        
                        if (value instanceof Date) {
                            Date dateValue = (Date)value;
                            if (oneTag.tag.containsIgnoreCase('long')) {
                                DateTime dtValue = DateTime.newInstance(dateValue, Time.newInstance(0,0,0,0));
                                tags.put(oneTag.tag, dtValue.format('MMMM d, YYYY'));
                            } else {
                                tags.put(oneTag.tag, dateValue.format());
                            }  
                        } else if (value instanceof DateTime) {
                            DateTime dtValue = (DateTime)value;
                            tags.put(oneTag.tag, dtValue.format('M/d/YYYY hh:mm aa'));
                        } else {
                            tags.put(oneTag.tag, String.valueOf(value));
                        }
                    } else {
                        Map<String,String> arguments = new Map<String,String>{'BarcodeType'=>'pdf417'};
                        Map<String,Object> barcodeType = new Map<String,Object>{'Type'=>'Barcode','Arguments'=>arguments};
                        List<Object> formatting = new List<Object>();
                        formatting.add(barcodeType);
                        Map<String,Object> barcodeTag = new Map<String,Object>();
                        barcodeTag.put('_Formatting', formatting);
                        barcodeTag.put('_Value',String.valueOf(value));

                        tags.put(oneTag.tag,barcodeTag);
                    } //if barcode
                } else {
                    tags.put(oneTag.tag,'[no data]');
                    System.debug('No field name : ' + oneTag.tag);
                } //if tag null
                
            }
        } //for baseObject
        
        return tags;
        
    } //generateTags

    
} //class