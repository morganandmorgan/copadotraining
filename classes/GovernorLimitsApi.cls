/**
 * GovernorLimitsApi
 * @description Get govenor limits from Salesforces limits REST api
 * @author Matt Terrill
 * @date 8/20/2019
 */
public without sharing class GovernorLimitsApi {

    //prive setting for Slack output
    private final Integer cautionLimit = 50;
    private final String cautionSlackEmoji = ':warning:';
    private final Integer warningLimit = 80;
    private final String warningSlackEmoji = ':rotating_light:';

    public GovernorLimitsApi() {
    } //constructor

    //class used to return parsed/calculated api results
    public class LimitsClass {
        public Integer max {get;set;}
        public Integer remaining {get;set;}
        public Integer used {get;set;}
        public Integer percentUsed {get;set;}
    }

    public Map<String,LimitsClass> getLimits() {

        String limitsJson = makeCallout();

        //our first level if deserialization
        Map<String,Object> mso1 = (Map<String,Object>)JSON.deserializeUntyped(limitsJson);

        //this will be what we return
        Map<String,LimitsClass> limits = new Map<String,limitsClass>();

        for (String key: mso1.keySet()) {
            //conver the nested objects to maps of string,object
            Map<String,Object> mso2 = (Map<String,Object>)mso1.get(key);

            //look for the expected keys
            if ( mso2.containsKey('Max') && mso2.containsKey('Remaining') ) {

                LimitsClass lc = new LimitsClass();
                //grab the parts to store, and calculate the rest
                lc.max = (Integer)mso2.get('Max');
                lc.remaining = (Integer)mso2.get('Remaining');
                lc.used = lc.max - lc.remaining;

                Double remaingPct = ((Double)lc.remaining/(Double)lc.max)*100;
                Long usedPct = 100 - remaingPct.round();

                lc.percentUsed = Integer.valueOf(usedPct);

                limits.put(key,lc);
            } //if keys
        } //for key

        //return all of our hard work
        return limits;

    } //getLimits


    public String formatForSlack(Boolean alertChannel) {

        //get the structure of limits
        Map<String,LimitsClass> limits = getLimits();

        //this will be what we return;
        String result = '';

        //this will store the @channel markup if needed
        String atChannel = '';

        //loop over the limits and build the return string
        for (String key: limits.keySet()) {

            //only output if we actually used any
            if (limits.get(key).max != limits.get(key).remaining) {

                String emoji = '';
                //dertermine if we need to prepend an emoji
                if (limits.get(key).percentUsed > warningLimit) {
                    emoji = warningSlackEmoji;
                    atChannel = '\n<!channel>';
                } else {
                    if (limits.get(key).percentUsed > cautionLimit) {
                        emoji = cautionSlackEmoji;
                    }
                } //if caution/warning checks

                //put everything together
                result = result
                    + emoji + ' '
                    + key
                    + ': Max=' + String.valueOf(limits.get(key).max)
                    + ', Remaining=' + String.valueOf(limits.get(key).remaining)
                    + ', Used=' + String.valueOf(limits.get(key).percentUsed) + '%'
                    + '\n';
            } //if used
        } //for limits

        //was an @channel requested?
        if (alertChannel) {
            result = result + atChannel;
        }

        return result;

    } //formatForSlack

    //logs in with SOAP API to get a valid sessionId
    private String getSessionId() {

        //set upt the request
		HttpRequest req = new HttpRequest();
		req.setEndpoint('callout:Governor_Limits_Login'); //named credential
		//req.setEndpoint('https://login.salesforce.com/services/Soap/u/46.0');        
		req.setMethod('POST');

        req.setBody('' //also references the named credential 
		+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">'
		+'   <soapenv:Header>'
		+'   </soapenv:Header>'
		+'   <soapenv:Body>'
		+'      <urn:login>'
		+'         <urn:username>{!$Credential.UserName}</urn:username>'
		+'         <urn:password>{!$Credential.Password}</urn:password>'
		+'      </urn:login>'
		+'   </soapenv:Body>'
		+'</soapenv:Envelope>');

		//req.setBody(rBody);
		req.setHeader('Content-Type', 'text/xml');
		req.setHeader('SOAPAction', '""');

		Http http = new Http();
		HTTPResponse res = http.send(req);

		//System.debug(res.getBody());

		//dig through the xml response to get to the good stuff
		Dom.Document xDoc = new Dom.Document();
		xDoc.load(res.getBody());

		Dom.XmlNode xEnv = xDoc.getRootElement();
		Dom.XmlNode xBody = xEnv.getChildElements()[0];
		Dom.XmlNode xResp = xBody.getChildElements()[0];
		Dom.XmlNode xResult = xResp.getChildElements()[0];

		String namespace = xResult.getChildElements()[0].getNamespace();

        String sessionId = xResult.getChildElement('sessionId',namespace).getText();

        //System.debug(sessionId);

        return sessionId;

    } //getSessionId


    private String makeCallout() {

        //limitsUrl = 'https://forthepeople--mterrill.cs47.my.salesforce.com/services/data/v46.0/limits/';
        String limitsUrl = 'https://forthepeople.my.salesforce.com/services/data/v46.0/limits/';

        HttpRequest limitsReq = new HttpRequest();

        limitsReq.setMethod('GET');  
        //limitsReq.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID()); //doesn't work as a site guest user
        limitsReq.setHeader('Authorization', 'Bearer ' + getSessionId());

        limitsReq.setEndpoint(limitsUrl); 

        Http limitsHttp = new Http();

        HttpResponse limitsRes = limitsHttp.send(limitsReq);

        //System.debug(limitsRes.getBody());

        return limitsRes.getBody();

    } //makeCallout

} //class