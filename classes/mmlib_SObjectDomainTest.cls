@isTest
private class mmlib_SObjectDomainTest 
{
    private static List<Account> testRecords = new List<Account>();

    private static final String DOMAIN_METHOD_TOKEN_01 = 'foobar';

    //@ //testSetup
    private static void setup()
    {
        testRecords.add( new Account( id=fflib_IDGenerator.generate( Account.SObjectType ), name='red' ) );
        testRecords.add( new Account( id=fflib_IDGenerator.generate( Account.SObjectType ), name='white' ) );
        testRecords.add( new Account( id=fflib_IDGenerator.generate( Account.SObjectType ), name='blue' ) );
        system.debug( testRecords.size() );

        // setup a fake test domain class
        // setup a fake ApplicationFactory__mdt record to support the fake test domain class
        // setup enough DomainProcess__mdt records to support the test 
        
    }

    @isTest
    private static void testTriggerExecution()
    {
        setup();

        system.debug( testRecords.size() );
        System.assertNotEquals(0, testRecords.size(), 'testRecords list was not populated.');

        // Note: for a trigger execution tests, the DomainProcess__mdt records need to be constucted in the Constructor method of this test class.

		// You also need to push the testing records into the fflib_SObjectDomain.Test.Database 
        //  so that the framework will use these records instead of looking for them from Trigger.new / Trigger.old
        fflib_SObjectDomain.Test.Database.onInsert( testRecords );

        Test.startTest();
        fflib_SObjectDomain.triggerHandler(  mmlib_SObjectDomainTest.TestMMLIBSObjectDomainConstructor.class );		
        Test.stopTest();

        System.assert( testRecords.isEmpty(), 'The test is valid if the mmlib_SObjectDomainTest.TestDomainAction class was executed and cleared this testRecords list. ' );        
    }

    @isTest
    private static void testDomainExecution()
    {
        setup();

        system.debug( testRecords.size() );
        System.assertNotEquals(0, testRecords.size(), 'testRecords list was not populated.');

        mmlib_SObjectDomainTest.TestMMLIBSObjectDomain testDomain = new mmlib_SObjectDomainTest.TestMMLIBSObjectDomain( testRecords );

        testDomain.mockDomainProcesses.add( mmlib_DomainProcessDataFactory.newInstanceForClass(mmlib_SObjectDomainTest.TestDomainCriteria.class)
                                                .criteria()
                                                .domainMethodExecution( DOMAIN_METHOD_TOKEN_01 )
                                                .generate() );

        testDomain.mockDomainProcesses.add( mmlib_DomainProcessDataFactory.newInstanceForClass(mmlib_SObjectDomainTest.TestDomainAction.class)
                                                .action()
                                                .domainMethodExecution( DOMAIN_METHOD_TOKEN_01 )
                                                .generate() );

        Test.startTest();
        testDomain.performSomeReallyCoolDomainMethodProcess();
        Test.stopTest();

        System.assert( testRecords.isEmpty(), 'The test is valid if the mmlib_SObjectDomainTest.TestDomainAction class was executed and cleared this testRecords list. ' );
    }

    private interface ITestMMLIBSObjectDomain extends fflib_ISObjectDomain
    {
        void performSomeReallyCoolDomainMethodProcess();
    }

    private with sharing class TestMMLIBSObjectDomain 
        extends mmlib_SObjectDomain
        implements mmlib_SObjectDomainTest.ITestMMLIBSObjectDomain
    {
        public TestMMLIBSObjectDomain(List<Account> sObjectList)
		{
			// Domain classes are initialised with lists to enforce bulkification throughout
			super(sObjectList);
            system.debug('mmlib_SObjectDomainTest.TestMMLIBSObjectDomain constructor has been called.' );
            system.assertEquals(sObjectList.size(), mmlib_SObjectDomainTest.testRecords.size(), 'The record count handed off to the mmlib_SObjectDomainTest.TestMMLIBSObjectDomain did not match the mmlib_SObjectDomainTest.testRecords count.');
		}

        public override void onAfterInsert()
        {
            system.debug( 'mmlib_SObjectDomainTest.TestMMLIBSObjectDomain.onAfterInsert is called');
        }

        public void performSomeReallyCoolDomainMethodProcess()
        {
            system.debug('mmlib_SObjectDomainTest.TestMMLIBSObjectDomain.performSomeReallyCoolDomainMethodProcess() method called');
            processDomainLogicInjections( DOMAIN_METHOD_TOKEN_01 );
        }
    }

    /**
	 * Typically an inner class to the domain class, supported here for test purposes
     * This "constructor" is specifically set to the default testing trigger execution of AfterInsert on Accounts based on the
     *  default values found in the mmlib_DomainProcessDataFactory class
     *
     *  @see mmlib_DomainProcessDataFactory
	 **/	
	public class TestMMLIBSObjectDomainConstructor implements fflib_SObjectDomain.IConstructable
	{
		public fflib_SObjectDomain construct(List<SObject> sObjectList)
		{
            system.debug('mmlib_SObjectDomainTest.TestMMLIBSObjectDomainConstructor.construct(List<SObject>) method called');

            mmlib_SObjectDomainTest.TestMMLIBSObjectDomain testDomain = new mmlib_SObjectDomainTest.TestMMLIBSObjectDomain( sObjectList );

            testDomain.mockDomainProcesses.add( mmlib_DomainProcessDataFactory.newInstanceForClass(mmlib_SObjectDomainTest.TestDomainCriteria.class)
                                                .criteria()
                                                .triggerExecution( mmlib_DomainProcessConstants.TRIGGER_OPERATION_TYPE.AfterInsert )
                                                .generate() );

            testDomain.mockDomainProcesses.add( mmlib_DomainProcessDataFactory.newInstanceForClass(mmlib_SObjectDomainTest.TestDomainAction.class)
                                                .action()
                                                .triggerExecution( mmlib_DomainProcessConstants.TRIGGER_OPERATION_TYPE.AfterInsert )
                                                .generate() );

			return testDomain;
		}				
	}

    public class TestDomainCriteria
        implements mmlib_ICriteria
    {
        private List<SObject> records = new List<SObject>();

        public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
        {
            system.debug('mmlib_SObjectDomainTest.TestDomainCriteria.setRecordsToEvaluate(List<SObject>) method called');
            this.records.addAll( records );
            System.assert(records != null, 'Records passed to mmlib_SObjectDomainTest.TestDomainCriteria.setRecordsToEvaluate() method was null');
            System.assert( ! records.isEmpty(), 'Records passed to mmlib_SObjectDomainTest.TestDomainCriteria.setRecordsToEvaluate() method was empty list');
            return this;
        }

        public list<SObject> run()
        {
            system.debug('mmlib_SObjectDomainTest.TestDomainCriteria.run() method called');

            // remove the first list item and return the rest to simulate the "qualified records"
            this.records.remove(0);

            return this.records;
        }
    }

    public class TestDomainAction
        extends mmlib_AbstractAction
    {
        public override Boolean runInProcess()
        {
            system.debug( 'The TestDomainAction is running with these records --- ' + this.records );
            system.debug('mmlib_SObjectDomainTest.TestDomainAction.runInProcess() method called');

            System.assert( this.records.size() == (mmlib_SObjectDomainTest.testRecords.size() -1)
                            , 'The number of records delivered to the mmlib_SObjectDomainTest.TestDomainAction class should be 1 less than the number of mmlib_SObjectDomainTest.testRecords' );

            //clear the test records for the final check by the test method.
            mmlib_SObjectDomainTest.testRecords.clear();

            return true;
        }
    }
}