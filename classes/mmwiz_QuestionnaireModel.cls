public class mmwiz_QuestionnaireModel
    extends mmwiz_AbstractBaseModel
{
    private static final String TITLE_PROPERTY_NAME = 'title';

    public mmwiz_QuestionnaireModel()
    {
        setType(mmwiz_QuestionnaireModel.class);
    }

    public override void initialize( Wizard__mdt wizard )
    {
        super.initialize( wizard );

        if ( this.serializedDataMap.containsKey( TITLE_PROPERTY_NAME ))
        {
            setTitle( (String)serializedDataMap.get( TITLE_PROPERTY_NAME ) );
        }
    }

    private String title;

    public void setTitle(String title)
    {
        this.title = title;
    }

    public string getTitle()
    {
        return this.title;
    }

    public override void prepareForSerialization()
    {
        super.prepareForSerialization();

        // Place subclass properties to the datamodel.
        serializedDataMap.put(TITLE_PROPERTY_NAME, getTitle());
    }

    public list<mmwiz_AbstractSetupModel> setupModels { get; set; } { setupModels = new list<mmwiz_AbstractSetupModel>(); }

    public mmwiz_QuestionnaireModel addSetupModel( mmwiz_AbstractSetupModel setupModel)
    {
        this.setupModels.add( setupModel );

        return this;
    }
}