public with sharing class mmintake_CiscoQueueSettingsSelector
    extends mmlib_SObjectSelector 
    implements mmintake_ICiscoQueueSettingsSelector
{
    public static mmintake_ICiscoQueueSettingsSelector newInstance()
    {
        return (mmintake_ICiscoQueueSettingsSelector) mm_Application.Selector.newInstance(Cisco_Queue_Setting__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Cisco_Queue_Setting__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            Cisco_Queue_Setting__mdt.Cisco_Queue__c,
            Cisco_Queue_Setting__mdt.County__c,
            Cisco_Queue_Setting__mdt.GeneratingAttorney__c,
            Cisco_Queue_Setting__mdt.Handling_Firm__c,
            Cisco_Queue_Setting__mdt.Marketing_Source__c,
            Cisco_Queue_Setting__mdt.Venue__c
        };
    }

    public List<Cisco_Queue_Setting__mdt> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }

    public List<Cisco_Queue_Setting__mdt> selectByDnis(Set<String> dnisSet)
    {
        return 
            Database.query(
                newQueryFactory()
                    .setCondition(Cisco_Queue_Setting__mdt.Cisco_Queue__c + ' in :dnisSet')
                    .toSOQL());
    }
}