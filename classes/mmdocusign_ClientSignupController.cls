public with sharing class mmdocusign_ClientSignupController {

	private String displayText_m = 'Contacting Docusign to get recipient view URL.';

	public mmdocusign_ClientSignupController() {
		// Nothing to do.
	}

	public String getDisplayText() {
		return displayText_m;
	}

	public ApexPages.PageReference execute() {

		// Extract data from URL.
		ApexPages.PageReference currentPage = ApexPages.currentPage();
		Map<String, String> paramMap = currentPage.getParameters();

		if (!paramMap.containsKey('mid')) { //process or display the message

			System.debug('<ojs> paramMap:\n' + paramMap);

			if ( !paramMap.containsKey('env') || !paramMap.containsKey('email') || !paramMap.containsKey('name') || !paramMap.containsKey('cuid') ) {
				currentPage.getParameters().put('mid','0'); //stop processing and looping
				return currentPage;
			}

			String envelopeId = paramMap.get('env');
			String clientEmail = EncodingUtil.urlDecode(paramMap.get('email'), 'UTF-8');
			String clientName = EncodingUtil.urlDecode(paramMap.get('name'), 'UTF-8');
			String clientUserId = EncodingUtil.urlDecode(paramMap.get('cuid'), 'UTF-8');

			String recipientUrl = '';

			try {
				if (!runAsTest) {
					recipientUrl = mmdocusign_DocusignRestApiService.getRecipientLinkToEnvelope(envelopeId, clientEmail, clientName, clientUserId);
				} else {
					test_urlParameterMap.put('env', envelopeId);
					test_urlParameterMap.put('email', clientEmail);
					test_urlParameterMap.put('name', clientName);
					test_urlParameterMap.put('cuid', clientUserId);
				}
			} catch(Exception exc) {
				//handled below when recipientUrl is still blank
			}

			if (recipientUrl == '') {
				currentPage.getParameters().put('mid','1'); //stop processing and looping
				return currentPage;
			} else {
				return new ApexPages.PageReference(recipientUrl);
			}

		} else {

			if ( paramMap.get('mid') == '0') {
				displayText_m = 'Missing parameters for processing';
			} else {
				displayText_m = mmdocusign_ConfigurationSettings.getDocusignUrlFailure();
			}

			return null;
		} //if 
	} //execute

	// ================= Testing Context =============================================

	@TestVisible
	private static Boolean runAsTest = false;
	
	@TestVisible
	private static Map<String, String> test_urlParameterMap = new Map<String, String>();
}