@isTest
private class mmsms_ScheduledSmsSelectorTest
{
	@isTest
	static void NormalTest()
	{
		SMS_Integration__c settings = new SMS_Integration__c();
		settings.Integation_enabled__c = true;
		settings.Integration_Enabled_In_Sandbox__c = true;

		insert settings;

		tdc_tsw__Scheduled_Sms__c sms1 = new tdc_tsw__Scheduled_Sms__c();
		tdc_tsw__Scheduled_Sms__c sms2 = new tdc_tsw__Scheduled_Sms__c();
		sms2.tdc_tsw__Phone_Api__c = '5482365794';

		insert new List<tdc_tsw__Scheduled_Sms__c> {sms1, sms2};

		Test.setCreatedDate(sms2.Id, DateTime.now().addMinutes(-100));

		mmsms_IScheduledSmsSelector selector = mmsms_ScheduledSmsSelector.newInstance();
		List<tdc_tsw__Scheduled_Sms__c> result = selector.getRecentRecords_AllRows(60);

		System.assertEquals(1, result.size(), 'Resulting list should have 1 record.');
	}
}