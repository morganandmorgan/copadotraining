/**
 *  Domain criteria used to find all User records where the User's isActive status has changed or is new and is active
 */
public class mmcommon_UserIsActiveChangedCriteria
    implements mmlib_ICriteriaWithExistingRecords
{
    private list<User> records = new list<User>();
    private Map<Id, User> existingUserRecordsMap = new Map<Id, User>();

    public mmlib_ICriteria setExistingRecords( Map<Id, SObject> existingRecords )
    {
        this.existingUserRecordsMap.clear();
        this.existingUserRecordsMap.putAll( (Map<Id, User>)existingRecords );

        return this;
    }

    public mmlib_ICriteria setRecordsToEvaluate(List<SObject> records)
    {
        this.records.clear();
        this.records.addAll( (list<User>)records );

        return this;
    }

    public List<SObject> run()
    {
        list<User> qualifiedRecords = new list<User>();

        // Loop through the User records.
        for ( User record : this.records )
        {
            //system.debug( 'record.id == ' + record.id );
            //system.debug( 'existingUserRecordsMap.containsKey( record.id ) == ' + existingUserRecordsMap.containsKey( record.id ) );
            //system.debug( 'existingUserRecordsMap.get( record.id ) == ' + existingUserRecordsMap.get( record.id ) );
            //system.debug( 'mmcommon_PersonUtils.isActive( record ) == ' + mmcommon_PersonUtils.isActive( record ));
            //system.debug( 'mmcommon_PersonUtils.isActive( existingUserRecordsMap.get( record.id ) ) == ' + mmcommon_PersonUtils.isActive( existingUserRecordsMap.get( record.id ) ));

            if ( record.id != null
                && existingUserRecordsMap.containsKey( record.id )
                && mmcommon_PersonUtils.isActive( record )
                && ! mmcommon_PersonUtils.isActive( existingUserRecordsMap.get( record.id ) )
                )
            {
                //system.debug( 'foo');
                qualifiedRecords.add( record );
            }
            else if ( mmcommon_PersonUtils.isActive( record )           // is the record active?
                    && ! existingUserRecordsMap.containsKey( record.id ) // if the record was not seen previously, then it is a new record.
                    )
            {
                //system.debug( 'bar');
                qualifiedRecords.add( record );
            }
            //system.debug( qualifiedRecords );
        }

        return qualifiedRecords;
    }
}