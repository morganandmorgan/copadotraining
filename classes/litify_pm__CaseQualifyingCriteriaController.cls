/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CaseQualifyingCriteriaController {
    global CaseQualifyingCriteriaController() {

    }
    @AuraEnabled
    global static String deleteItems(List<SObject> items) {
        return null;
    }
    @AuraEnabled
    global static Map<String,List<SObject>> fetchQuestions(List<String> type) {
        return null;
    }
    @AuraEnabled
    global static List<SObject> fetchRecords(List<String> ids) {
        return null;
    }
    @AuraEnabled
    global static Map<String,List<Map<String,List<String>>>> getFieldValues(List<String> objects) {
        return null;
    }
    @AuraEnabled
    global static Map<String,SObject> updateCQC(litify_pm__Case_Qualify_Criteria__c cqc) {
        return null;
    }
}
