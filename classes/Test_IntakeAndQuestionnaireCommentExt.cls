@isTest
private class Test_IntakeAndQuestionnaireCommentExt {

    static testMethod void IntakeCommentExtensionFun() {
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account'];        
        Account acc = new Account(LastName = 'Test Account', RecordTypeId = acctRcdType.Id);
        insert acc;
        
        Intake__c intakeRec = new Intake__c(Client__c = acc.Id);
        insert intakeRec;       
        
        ApexPages.StandardController ctlr = new ApexPages.StandardController(intakeRec);
        IntakeCommentExtension intakeCmtExt = new IntakeCommentExtension(ctlr);              
        intakeCmtExt.saveComment();
        
        //To cover the catch statement
        ApexPages.StandardController ctlr2 = new ApexPages.StandardController(new Intake__c());
        IntakeCommentExtension intakeCmtExt2 = new IntakeCommentExtension(ctlr2);              
        intakeCmtExt2.saveComment();
    }
    
    static testMethod void QuestionnaireCommentExtensionFun() {
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account'];        
        Account acc = new Account(LastName = 'Test Account', RecordTypeId = acctRcdType.Id);
        insert acc;
        
        Intake__c intakeRec = new Intake__c(Client__c = acc.Id);
        insert intakeRec; 
        
        Questionnaire__c questRec = new Questionnaire__c(Intake__c = intakeRec.Id);
        insert questRec;     
        
        ApexPages.StandardController ctlr = new ApexPages.StandardController(questRec);
        QuestionnaireCommentExtension questCmtExt = new QuestionnaireCommentExtension(ctlr); 
        questCmtExt.saveComment();
        
        //To cover the catch statement
        Questionnaire__c questRec2 = new Questionnaire__c();
        insert questRec2;
        ApexPages.StandardController ctlr2 = new ApexPages.StandardController(questRec2);
        QuestionnaireCommentExtension questCmtExt2 = new QuestionnaireCommentExtension(ctlr2); 
        questCmtExt2.saveComment();
    }
}