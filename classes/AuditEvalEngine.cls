public without sharing class AuditEvalEngine {

    public class InvalidExpressionException extends Exception {}

     public AuditEvalEngine() {

    } //constructor

    //sub-class with audit_rule__c and it's deserialized expressions
    public class Rule {
        public Audit_Rule__c auditRule {get;set;}
        public List<RuleExpression> expressions {get;set;}
    }//rule

    //sub-class to build/parse the rules json
    public class RuleExpression {
        public String argument1 {get;set;}
        public String operator {get;set;}
        public Double percent {get;set;}
        public String argument2 {get;set;}
        public String dateModifier {get;set;}
    } // RuleExpression:sub-class

    //sub-class for returning the results
    public class EvaluationResult {
        public SObject obj {get;set;}
        public Rule rule {get;set;}
        public Audit_Scheduled_Flag__c scheduledFlag {get;set;} //for re-evaluations
        public Boolean criteriaMet {get;set;}
    } // EvaluationResult:sub-class

    // Static methods to support sub-classes

    public static AuditEvalEngine.Rule createRuleFromSObject(Audit_Rule__c auditRule) {
        AuditEvalEngine.Rule rule = new AuditEvalEngine.Rule();
        rule.auditRule = auditRule;
        rule.expressions = (List<AuditEvalEngine.RuleExpression>)JSON.deserialize(auditRule.rule_criteria__c, List<AuditEvalEngine.RuleExpression>.class);
        return rule;
    } //createRuleFromSObject


    public static List<AuditEvalEngine.Rule> createRulesFromSObjects(List<Audit_Rule__c> auditRules) {
        List<AuditEvalEngine.Rule> rules = new List<AuditEvalEngine.Rule>();        

        for (Audit_Rule__c ar: auditRules) {
            rules.add( createRuleFromSObject(ar) );
        } //for audit rules

        return rules;
    } //createRulesFromSObjects


    private String getRelatedFieldName(String relatedField) {

        String relatedName = relatedField;

        if ( relatedName.containsIgnoreCase('__r')) {
            relatedName = relatedName.toLowerCase().replace('__r','__c');
        } else {
            //Out of the box relationship work... wierd
            //OwnerId is the fields name, but Owner.name is the related fields reference, so lets account for both
            if ( relatedName.right(2).toLowerCase() != 'id' ) {
                relatedName = relatedName + 'Id';
            }
        }

        return relatedName;
    } //getRelatedFieldName


    private Map<Id,Set<String>> ruleFields;
    //builds a list of fields required for each rule, for avoidance during triggers
    private void addFieldToRule(Id ruleId, String field) {

        if ( !ruleFields.containsKey(ruleId) ) {
            Set<String> fieldSet = new Set<String>();
            ruleFields.put(ruleId, fieldSet);
        }

        if ( !field.contains('.') ) {
            ruleFields.get(ruleId).add(field);
        } else {
            ruleFields.get(ruleId).add(getRelatedFieldName(field.split('\\.')[0]));
        }

    } //addFieldToRule


    //build a list of fields required to run a rule, also builds the ruleFields map
    public String buildFieldList(List<Rule> rules, String objectType) {

        ruleFields = new Map<Id,Set<String>>();

        Set<String> fieldSet = new Set<String>(); //to keep the list unique

        for (Rule rule: rules) {
            for (RuleExpression exp : rule.expressions) {
                if (rule.auditRule.object__c == objectType) {
                    if (exp.argument1.left(2) == '{!') {
                        String fieldName = exp.argument1.replace('{!','').replace('}','');
                        fieldSet.add(fieldName.toLowerCase());
                        addFieldToRule(rule.auditRule.id, fieldName);
                    }
                    if (exp.argument2.left(2) == '{!') {
                        String fieldName = exp.argument2.replace('{!','').replace('}','');
                        fieldSet.add(fieldName.toLowerCase());
                        addFieldToRule(rule.auditRule.id, fieldName);
                    }
                } //if rule for objectName
            }//for expressions
            //we will also need the custom scheduled date
            if (rule.auditRule.object__c == objectType && rule.auditRule.custom_scheduled_date_field__c != null && rule.auditRule.custom_scheduled_date_field__c != '') {
                fieldSet.add(rule.auditRule.custom_scheduled_date_field__c.toLowerCase());
            } //if custom scheduled date
            //and we need the userId they are going to flag;
            if (rule.auditRule.object__c == objectType && rule.auditRule.related_flagged_user__c != null && rule.auditRule.related_flagged_user__c != '') {
                fieldSet.add(rule.auditRule.related_flagged_user__c.toLowerCase());
            } // if related flagged user
        } //for rule

        String results = '';
        String delimiter = '';

        for (String fieldName : fieldSet) {
            results = results + delimiter + fieldName;
            delimiter = ',';
        } //for

        return results;
    } //buildFieldList


    private Boolean weNeedToRunRule(Id ruleId, SObject obj, SObject oldObj) {

        //we are only avoiding running rules on insert triggers
        if ( ruleFields == null || ruleFields.size() == 0) {
            return true;
        } else {
            //look for a change in a rule field
            for (String field : ruleFields.get(ruleId)) {
                if (obj.get(field) != oldObj.get(field)) {
                    return true;
                } //if field changed
            } //for rule fields
        } //if checking avoidance

        return false;
    } //weNeedToRunRule


    //evaluate method for trigger (new and old maps)
    public List<EvaluationResult> evaluate(Map<Id,SObject> objects, Map<Id,SObject> oldObjects) {

        List<EvaluationResult> results = new List<EvaluationResult>();

        if ( objects.size() != 0) {
            //Get the type of these objects
            String objType = objects.values()[0].getsObjectType().getDescribe().getName();

            //get the rules for these objects
            AuditRuleSelector ars = new AuditRuleSelector();
            List<Rule> rules = ars.selectByActiveForObject(objType);

            //make sure we have rules to run
            if ( rules.size() != 0) {
                //build a list of fields we need to query for these rules
                String queryFields = buildFieldList(rules,objType);

                //get the fields for all the objects
                Set<Id> idSet = objects.keySet();
                String query = 'SELECT ' + queryFields + ' FROM ' + objType + ' WHERE id IN :idSet';
                List<SObject> objectList = Database.query(query);

                //for each object
                for (SObject obj : objectList) {

                    //for each rule
                    for (Rule rule : rules) {
                        //if oldObjects is null, this is an insert, otherwise we do an avoidance check
                        if (oldObjects == null || weNeedToRunRule(rule.auditRule.id, obj, oldObjects.get(obj.id)) ) {
                            EvaluationResult result = new EvaluationResult();
                            result.criteriaMet = evaluate(obj,rule.expressions, false);
                            result.obj = obj;
                            result.rule = rule;
                            results.add(result);
                        } //if weNeedToRunRule
                    } //for rule

                } //for SObject
            } //if rules
        } //if objects

        return results;

    } //evaluate (trigger)


    //evaluate method for a list of results (rerun)
    public Map<Id,EvaluationResult> evaluate(List<EvaluationResult> evalResults, Map<Id,SObject> oldObjects) {

        Map<Id,EvaluationResult> results = new Map<Id,EvaluationResult>();

        if ( evalResults.size() != 0) {
            //Get the type of these objects
            String objType = evalResults[0].obj.getsObjectType().getDescribe().getName();

            //get the rules for these objects, and a list of old objects
            List<Rule> rules = new List<Rule>();
            List<SObject> objectList = new List<SObject>();
            for (EvaluationResult result : evalResults) {
                rules.add(result.rule);
                objectList.add( oldObjects.get((Id)result.obj.get('id')) );
            }

            //for each object
            for (SObject obj : objectList) {

                //for each rule
                for (Rule rule : rules) {
                    EvaluationResult result = new EvaluationResult();
                    result.criteriaMet = evaluate(obj,rule.expressions,true);
                    result.obj = obj;
                    result.rule = rule;
                    results.put((Id)obj.get('id'),result);
                } //for rule

            } //for SObject
        } //if objects

        return results;

    } //evaluate (rerun)


    public String fixRelatedFieldName(String relatedFieldName) {

        //strip the __c off the end, if it is there, so we don't clean it
        if (relatedFieldName.right(3) == '__c') {
            relatedFieldName = relatedFieldName.left( relatedFieldName.length()-3);
        }

        //custom fields cannot have '__' double underscores in them
        relatedFieldName = relatedFieldName.replace('__','_');

        return relatedFieldName + '__c';
    } //fixRelatedFieldName


    //evaluate method for scheduled flags
    public List<EvaluationResult> evaluate(List<Audit_Scheduled_Flag__c> scheduledFlags) {

        List<EvaluationResult> results = new List<EvaluationResult>();

        if ( scheduledFlags.size() != 0) {

            //get a list of all the rules we need to run
            Set<Id> ruleIds = new Set<Id>();
            for (Audit_Scheduled_Flag__c asf : scheduledFlags) {
                ruleIds.add(asf.audit_rule__c);
            } //for scheduledFlags

            //get the rules
            AuditRuleSelector ars = new AuditRuleSelector();
            List<Rule> rules = ars.selectById(ruleIds);

            //get a unique set of objects these rules are for, and build a map of all the rules
            Set<String> objTypes = new Set<String>();
            Map<Id,Rule> ruleMap = new Map<Id,Rule>();
            for (Rule rule: rules) {
                objTypes.add(rule.auditRule.object__c);
                ruleMap.put(rule.AuditRule.id, rule);
            } //for rules

            //loop over all the object types we need to run rules for
            for (String objType : objTypes) {

                //build a list of fields we need to query for these rules
                String queryFields = buildFieldList(rules,objType);

                //get a set of id's for this object type
                Set<Id> idSet = new Set<Id>();
                for (Audit_Scheduled_Flag__c asf : scheduledFlags) {
                    if ( ruleMap.get(asf.audit_rule__c).auditRule.object__c == objType) {
                        idSet.add((Id)asf.get(fixRelatedFieldName(objType)));
                    } //if objType
                } //for flags

                //get the fields for all the objects
                String query = 'SELECT ' + queryFields + ' FROM ' + objType + ' WHERE id IN :idSet';
                Map<Id,SObject> objectMap = new Map<Id,SOBject>(Database.query(query));

                //now run back over the flags and re-run the required rule(s)
                for (Audit_Scheduled_Flag__c asf : scheduledFlags) {
                    if ( ruleMap.get(asf.audit_rule__c).auditRule.object__c == objType) {

                        //get the object and expressions form the rule we need to run
                        Id thisId = (Id)asf.get(fixRelatedFieldName(objType));
                        SObject thisObject = objectMap.get(thisId);
                        List<RuleExpression> expressions = ruleMap.get(asf.audit_rule__c).expressions;

                        //evaluate the rule and store the results
                        EvaluationResult result = new EvaluationResult();
                        result.criteriaMet = evaluate(thisObject,expressions, false);
                        result.obj = thisObject;
                        result.rule = ruleMap.get(asf.audit_rule__c);
                        result.scheduledFlag = asf;
                        results.add(result);
                    } //if objType
                } //for flags
            } //for objectTypes

        } //if objects

        return results;

    } //evaluate scheduled flags


    //used in the tool to make sure a rule will run without error
    public String test(Rule rule) {
        String result = '';
        try {
            //what type of object is this rule for?
            String objType = rule.auditRule.object__c;

            //build a list of fields we need to query for this rule
            String queryFields = buildFieldList(new List<Rule>{rule}, objType);

            //get some records to test on
            String query = 'SELECT ' + queryFields + ' FROM ' + objType + ' ORDER BY createdDate DESC LIMIT 25';
            List<SObject> objectList = Database.query(query);

            if (objectList.size() != 0) {
                //grab the last one in the list
                SObject obj = objectList[ objectList.size()-1];

                String delimiter = '';
                //run all the expressions against it
                for (RuleExpression expression: rule.expressions) {
                    try {
                        evaluate(obj, expression, false);
                    } catch (Exception e) {
                        result = result + delimiter;
                        delimiter = '<br/>';
                        result = result + 'Expression: ' + expression.argument1 + ' ' + expression.operator + ' ' + expression.percent + ' ' + expression.argument2 + '  ' + expression.dateModifier;
                        result = result + '<br/> Failed.';
                        //result = result + '<br/>' + e.getMessage();
                        //result = result + '<br/>' + e.getStackTraceString();
                        //result = result + '<br/>' + e.getTypeName();
                    }
                } //for expressions
                if (result == '') result = 'All expressions ran successfully.';
            } else {
                result = 'No records of type ' + objType + ' found to test with. No tests run.';
            } //if objects
        } catch (Exception e) {
            result = 'Rule Failed.';
        }
        return result;
    } //test


    //evaluate one object, a list of expressions
    //skipRelated will not evaluate related object fields (for evaluations against old objects)
    public Boolean evaluate(SObject obj, List<RuleExpression> expressions, Boolean skipRelated) {
        try { //lets try to catch errors earlier so a whole batch doesn't fail
            for (RuleExpression expression: expressions) {
                if ( !evaluate(obj,expression,skipRelated) ) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            //make sure this doesn't error silently
            ErrorLogDomain errorLog = new ErrorLogDomain();
            errorlog.logError(e, 'AuditEvalEngine');
            //we don't know what the outcome would have been, but let's not raise any false flags
            return false;
        } //try/catch
    } //evaluate one record, rule


    public Schema.SoapType getFieldType(SObject obj, String fieldName ) {

        Schema.SoapType result;
        Schema.SObjectType objType;
        String objField;

        if ( fieldName.contains('.') ) {
            //get the relational field name
            String relatedName = fieldName.split('\\.')[0];

            relatedName = getRelatedFieldName(relatedName);

            // ???? Could we check to see what object (if any) is currently linked, instead of the schema check ????

            //check to see if this relationship is to multiple types, like OwnerId -> User or Queue
            if ( obj.getsObjectType().getDescribe().fields.getMap().get(relatedName).getDescribe().isNamePointing() ) {
                throw new InvalidExpressionException('Polymorphic relationships are not supported in field lookups, "' + fieldName + '"'); 
            }
            //get the object it is related to
            objType = obj.getsObjectType().getDescribe().fields.getMap().get(relatedName).getDescribe().getReferenceTo()[0];
            //get the name of the field on that object
            objField = fieldName.split('\\.')[1];
        } else {
            objType = obj.getsObjectType();
            objField = fieldName;
        } //contains .(dot)

        if (objType.getDescribe().fields.getMap().get(objField) != null) {
            return objType.getDescribe().fields.getMap().get(objField).getDescribe().getSOAPType();
        } else {
            throw new InvalidExpressionException('Field, "' + objField + '" does not exist on Object, "' + objType.getDescribe().getName() + '", "' + fieldName + '"'); 
        }

    } //determineType


    private Object getField(SObject obj, String fieldName) {

        if ( fieldName.contains('.') ) {
            String objName = fieldName.split('\\.')[0];
            //Out of the box relationships work... wierd
            //OwnerId is the fields name, but Owner.name is the related fields reference, so lets account for both
            if ( objName.right(2).toLowerCase() == 'id') {
                objName = objName.toLowerCase().replace('id','');
            }
            String objField = fieldName.split('\\.')[1];

            //make sure there is a related record
            if ( obj.getSObject(objName) != null) {
                return obj.getSObject(objName).get(objField);
            } else {
                return null;
            }
        } else {
            return obj.get(fieldName);
        }

    } //getField

    //evaluate one object, one expression
    public Boolean evaluate(SObject obj, RuleExpression expression, Boolean skipRelated) {

        //skipRelated will skip evaluations of related object fields and return 'true'
        //this is for running the rules a 2nd time against the old data
        if (skipRelated) {
            //check to see if this expression is based on a related field, and if so, assume it matches
            if ( expression.argument1.contains('.') || expression.argument2.contains('.')) {
                return true;
            }
        } //skipRelated

        List<Schema.SoapType> argType = new List<Schema.SoapType> {null,null};
        List<Object> arg = new List<Object> {null,null};
        if (expression.argument1.left(2) == '{!') {
            String fieldName = expression.argument1.replace('{!','').replace('}','');
            argType[0] = getFieldType(obj,fieldName);
            arg[0] = getField(obj,fieldName);
        } else {
            arg[0] = expression.argument1;
        }
        
        if (expression.argument2.left(2) == '{!') {
            String fieldName = expression.argument2.replace('{!','').replace('}','');
            argType[1] = getFieldType(obj,fieldName);
            arg[1] = getField(obj,fieldName);
        } else {
            arg[1] = expression.argument2;
        }

        if (argType[0] == null && argType[1] != null) {
            argType[0] = argType[1];
        }
        if (argType[1] == null && argType[0] != null) {
            argType[1] = argType[0];
        }
        if (argType[0] == null && argType[1] == null) {
            argType[0] = Schema.SoapType.String;
            argType[1] = Schema.SoapType.String;
        }                

        if (argType[0] != argType[1]) {
            throw new InvalidExpressionException('Incompatible data types in expression, "' + expression.argument1 + '":"' + String.valueOf(argType[0]) + '" vs "' + expression.argument2 + '":"' + String.valueOf(argType[1]) + '"'); 
        }

        if (argType[0] == Schema.SoapType.Boolean) return compare(Boolean.valueOf(arg[0]), expression.operator, Boolean.valueOf(arg[1]));
        if (argType[0] == Schema.SoapType.Date) return compare(convertDate(arg[0]), expression, arg[1]);
        if (argType[0] == Schema.SoapType.DateTime) return compare(convertDateTime(arg[0]), expression, arg[1]);
        if (argType[0] == Schema.SoapType.Double) return compare(convertDouble(arg[0]), expression, convertDouble(arg[1]));
        if (argType[0] == Schema.SoapType.Integer) return compare(convertInteger(arg[0]), expression, convertInteger(arg[1]));
        if (argType[0] == Schema.SoapType.String) return compare(String.valueOf(arg[0]), expression.operator, String.valueOf(arg[1]));
        //if (argType[0] == Schema.SoapType.Time) return compare(Time.valueOf(arg[0]), expression.operator, Time.valueOf(arg[1]));

        throw new InvalidExpressionException('Data Type, "' + String.valueOf(argType[0]) + '" not supported');

    } //evaluate one record, one expression(row)

    /* Compare methods */

    private Boolean compare(Boolean argument1, String operator, Boolean argument2) {

        if (operator == '=') return (argument1 == argument2);

        if (operator == '<>') return (argument1 != argument2);
 
        throw new InvalidExpressionException('Operator, "' + operator + '" is not valid for Boolean');
    } //compare (Boolean)


    private Date convertDate(Object dObj) {
        if (dObj instanceof Date) {
            return Date.valueOf(dObj);
        } else {
            if ( dObj == null || String.valueOf(dObj) == '') {
                return null;
            } else {
                return Date.parse(String.valueOf(dObj));
            }
        }
    } //convertDate


    private Boolean compare(Date argument1, RuleExpression expression, Object argument2) {

        DateTime dtArgument1;
        if (argument1 != null) {
            dtArgument1 = DateTime.newInstance(argument1, Time.newInstance(0,0,0,0));
        }
        Object dtArgument2;
        if ( expression.dateModifier == null || expression.dateModifier == '') {
            if (argument2 != null) {
                Date dArgument2 = convertDate(argument2);
                if (dArgument2 == null) {
                    dtArgument2 = null;
                } else {
                    dtArgument2 = DateTime.newInstance(dArgument2, Time.newInstance(0,0,0,0));
                }
            }
        } else {
            dtArgument2 = argument2;
        }

        return compare(dtArgument1, expression, dtArgument2);
    } //compare (Date)


    private DateTime convertDateTime(Object dtObj) {
        if (dtObj instanceof DateTime) {
            return DateTime.valueOf(dtObj);
        } else {
            if ( dtObj == null ||  String.valueOf(dtObj) == '') {
                return null;
            } else {
                return DateTime.parse(String.valueOf(dtObj));
            }
        }
    } //convertDateTime


    private Boolean compare(DateTime argument1, RuleExpression expression, Object argument2) {

        DateTime finalDt;

        if ( expression.dateModifier == null || expression.dateModifier == '') {
            finalDt = convertDateTime(argument2);
        } else {
            if (expression.dateModifier == 'days_ago')
                finalDt = DateTime.now().addDays(-1 * Integer.valueOf(argument2));
            else if (expression.dateModifier =='months_ago')
                finalDt = DateTime.now().addMonths(-1 * Integer.valueOf(argument2));
            else if (expression.dateModifier == 'years_ago')
                finalDt = DateTime.now().addYears(-1 * Integer.valueOf(argument2));
        }

        if (expression.operator == '=') return argument1 == finalDt;

        if (expression.operator == '<>') return argument1 != finalDt;
         
        if (expression.operator == '>') return argument1 > finalDt;
  
        if (expression.operator == '<') return argument1 < finalDt;

        if (expression.operator == '>=') return argument1 >= finalDt;

        if (expression.operator == '<=') return argument1 <= finalDt;

        throw new InvalidExpressionException('Operator, "' + expression.operator + '" is not valid for Date/Time');
    } //compare (DateTime)


    private Double convertDouble(Object dObj) {
        if (dObj instanceof Double) {
            return Double.valueOf(dObj);
        } else {
            if ( dObj == null || String.valueOf(dObj) == '') {
                return null;
            } else {
                return Double.valueOf(dObj);
            }
        }
    } //convertDouble


    private Boolean compare(Double argument1, RuleExpression expression, Double argument2) {

        Double finalArg2;
        if (expression.percent == null || expression.percent == 0) {
            finalArg2 = argument2;
        } else {
            if (argument2 != null) {
                finalArg2 = (expression.percent/100) * argument2;
            } else {
                finalArg2 = argument2;
            }
        }

        if (expression.operator == '<') return (argument1 < finalArg2);

        if (expression.operator == '<=') return (argument1 <= finalArg2);

        if (expression.operator == '=') return (argument1 == finalArg2);

        if (expression.operator == '<>') return (argument1 != finalArg2);

        if (expression.operator == '>') return (argument1 > finalArg2);

        if (expression.operator == '>=') return (argument1 >= finalArg2);

        throw new InvalidExpressionException('Operator, "' + expression.operator + '" is not valid for Number');
    } //compare (Double)


    //converts the integers to doubles and uses the same code to compare doubles
    private Double convertInteger(Object dObj) {
        if (dObj instanceof Integer) {
            return Double.valueOf(dObj);
        } else {
            if ( dObj == null || String.valueOf(dObj) == '') {
                return null;
            } else {
                return Double.valueOf(dObj);
            }
        }
    } //convertInteger


    private Boolean containsOnly(String argument1, List<String> arguments) {
        for (String arg1 : argument1.split(';')) {
            boolean contains = false;

            for (String arg2 : arguments) {
                if (arg2 == arg1.trim()) {
                    contains = true;
                    break;
                }
            }

            if ( !contains ) return false;
        }

        return true;
    } //containsOnly


    private Boolean compare(String argument1, String operator, String argument2) {

        //treat NULL strings as '' (blank)
        if (argument1 == null) {
            argument1 = '';
        }
        if (argument2 == null) {
            argument2 = '';
        }

        List<String> arguments;
        //constants are delimited by line breaks, multi-select lists are delimited by semi-colons
        if (argument2.contains('\n')) {
            arguments = argument2.split('\n');
        } else {
            arguments = argument2.split(';');
        }

        if (operator == 'contains_only')
            return containsOnly(argument1, arguments);

        Boolean match = false;

        for (String arg : arguments) {

            if (operator == '<')
                match = (argument1 < arg);
            else if (operator == '<=')
                match = (argument1 <= arg);
            else if (operator == '=')
                match = (argument1 == arg);
            else if (operator == '<>')
                match = (argument1 != arg);
            else if (operator == '>')
                match = (argument1 > arg);
            else if (operator == '>=')
                match = (argument1 >= arg);
            else if (operator == 'contains')
                match = (argument1.contains(arg));
            else if (operator == 'does_not_contain')
                match = (!argument1.contains(arg));
            else throw new InvalidExpressionException('Operator, "' + operator + '" is not valid for Text');

            if (match) return match;
        } //for arguments

        return match;

    } //compare (String)

/*
    private Boolean compare(Time argument1, String operator, Time argument2) {

        Date constant = Date.today();

        DateTime dtArgument1;
        if (argument1 != null) {
            dtArgument1 = DateTime.newInstance(constant, argument1);
        }
        DateTime dtArgument2;
        if (argument2 != null) {
            dtArgument2 = DateTime.newInstance(constant, argument2);
        }

        return compare(dtArgument1, operator, dtArgument2);
    } //compare (Time)
*/

    public static Id determinWhoToFlag(EvaluationResult result) {

        Id userToFlag;

        if (result.rule.auditRule.related_flagged_user__c != null && result.rule.auditRule.related_flagged_user__c != '') {
            userToFlag = (Id)result.obj.get(result.rule.auditRule.related_flagged_user__c);
            //if there is no one assigned, there will be no flag
            if (userToFlag != null) {
                //we will only flag users (not queues or groups)
                if (userToFlag.getSObjectType().getDescribe().getName() != 'User') {
                    userToFlag = null;
                }
            }
        } else {
            userToFlag = UserInfo.getUserId();
        }

        return userToFlag;

    } //determinWhoToFlag

} //class