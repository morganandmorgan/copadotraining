/*============================================================================/
* TrustTransaction_Selector_Test
* @description Test for Deposit Selector
* @author Brian Krynitsky
* @date 2/26/2019
=============================================================================*/

@isTest
private class TrustTransaction_Selector_Test {
	
	public static litify_pm__Matter__c matter;

	static void setupData()
    {
    	/*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaBankAccount__c operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
        update operatingBankAccount;
       
		/*--------------------------------------------------------------------
        LITIFY Data Setup
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = TestDataFactory_FFA.company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;


		// create new Trust Transaction
        Trust_Transaction__c tt1 = new Trust_Transaction__c();
        tt1.Date__c = date.today();
        tt1.Matter__c = matter.id;
        tt1.Amount__c = 100;
        INSERT tt1;

		Trust_Transaction__c tt2 = new Trust_Transaction__c();
        tt2.Date__c = date.today();
        tt2.Matter__c = matter.id;
        tt2.Amount__c = 100;
        INSERT tt2;

    }

	@IsTest
    private static void ctor() {
        TrustTransaction_Selector ttSelector = new TrustTransaction_Selector();
        System.assert(ttSelector != null);
    }
	
	@IsTest
    private static void test_selectByMatterId() {
        setupData();

		Set<Id> matterIds = new Set<Id>{matter.Id};
		TrustTransaction_Selector ttSelector = new TrustTransaction_Selector();
	 	List<Trust_Transaction__c> ttList = ttSelector.selectByMatterId(matterIds);
    }
}