/**
 *  mmlitifylrn_ReferralsSyncSchedule class allows for the service to
 *  be called/referenced by the mmlitifylrn_ReferralsSyncSchedulable class
 *  but not maintain a lock on the class so that it can't be updated.
 *
 *  @see mmlitifylrn_ReferralsSyncSchedulable
 */
public class mmlitifylrn_ReferralsSyncSchedule
    implements mmlib_ISchedule
{
    public void execute(SchedulableContext sc)
    {
        try
        {
            mmlitifylrn_ReferralsService.syncFromLitify();
        }
        catch (Exception e)
        {
            system.debug(e);
            if ( e.getCause() != null )
            {
                system.debug( e.getCause() );

                if ( e.getCause().getCause() != null )
                {
                     system.debug( e.getCause().getCause() );
                }
            }
        }
    }
}