@RestResource(urlMapping='/intake-legacy/*')
global with sharing class IntakePost {
  @HttpPost
  global static String create() {
    RestRequest req = RestContext.request;
    Blob reqBody = req.requestBody;
    String decodedBody = EncodingUtil.urlDecode(reqBody.toString(),'UTF-8');

    IntakePost post = new IntakePost();
    try {
      post.CreateUpdateIntake(decodedBody);
      } catch(Exception ex) {
        System.System.debug(loggingLevel.Error, '*** exception: ' + ex);
        return 'error';
      }

    return 'ok';
  }

  //@HttpGet
  //global static String lookup() {
  //  RestRequest req = RestContext.request;
  //  String idString = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
  //  String[] ids = idString.split(',');
  //  ID intakeId = ids[0];
  //  ID accountId = ids[1];
  //  List<Intake__c> intake = [SELECT ID FROM Intake__c WHERE ID = :intakeId AND Client__c = :accountId];

  //  if (intake.size() == 0) {
  //    RestContext.response.statusCode = 404;
  //    return 'not found';
  //  } else {
  //    RestContext.response.statusCode = 200;
  //    return 'ok';
  //  }
  //}

  public class MalformedRequestException extends Exception {}
  public void CreateUpdateIntake(String jsonString) {
    // '{"account": {"name": "bill gates", "intake": { "Case_Description__c": "i was hurt"} } }'

    JSONParser parser = JSON.createParser(jsonString);
    Account objAccount;
    Intake__c objIntake;
    JSONToken token;

    while((token = parser.nextToken()) != null) {
      if (token == JSONToken.FIELD_NAME) {
        if (parser.getText() == 'account') {
          if (objAccount != null) { throw new MalformedRequestException(); }
          parser.nextToken();
          objAccount = (Account)parser.readValueAs(Account.class);
        } else if (parser.getText() == 'intake') {
          if (objIntake != null) { throw new MalformedRequestException(); }
          parser.nextToken();
          objIntake = (Intake__c)parser.readValueAs(Intake__c.class);
        }

      }
    }

    if (objAccount == null || objIntake == null) {
      throw new MalformedRequestException();
    }

    if (objAccount.ID == null) {
      insert objAccount;
    }

    objIntake.Client__c = objAccount.id;
    objIntake.Caller__c = objAccount.id;
    objIntake.Injured_Party__c = objAccount.id;

    objIntake.InputChannel__c = 'IntakeLegacyAPIEndpoint';

    if (objIntake.ID == null) {
      insert objIntake;
    } else {
      update objIntake;
    }
  }
}