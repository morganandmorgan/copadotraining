public interface mmgmap_IGeoCodingService
{
	Location getLatitudeLongitudeForAddress(mmgmap_Address addr);
}