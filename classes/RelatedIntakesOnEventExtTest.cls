@isTest
private class RelatedIntakesOnEventExtTest {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < 3; ++i) {
            intakes.add(new Intake__c(
                    Incident__c = incident.Id,
                    Client__c = client.Id
                ));
        }
        toInsert.addAll((List<SObject>) intakes);

        IncidentInvestigationEvent__c incidentInvestigation = new IncidentInvestigationEvent__c(
                Incident__c = incident.Id,
                EndDateTime__c = Datetime.now().addHours(1),
                StartDateTime__c = Datetime.now()
            );
        toInsert.add(incidentInvestigation);

        Database.insert(toInsert);
        toInsert.clear();

        Event calendarEvent = new Event(
                Subject = 'Test Subject',
                WhatId = incidentInvestigation.Id,
                StartDateTime = incidentInvestigation.StartDateTime__c,
                EndDateTime = incidentInvestigation.EndDateTime__c
            );
        toInsert.add(calendarEvent);

        List<IntakeInvestigationEvent__c> intakeInvestigations = new List<IntakeInvestigationEvent__c>();
        for (Intake__c intake : intakes) {
            intakeInvestigations.add(new IntakeInvestigationEvent__c(
                    IncidentInvestigation__c = incidentInvestigation.Id,
                    Intake__c = intake.Id,
                    StartDateTime__c = incidentInvestigation.StartDateTime__c,
                    EndDateTime__c = incidentInvestigation.EndDateTime__c
                ));
        }
        toInsert.addAll((List<SObject>) intakeInvestigations);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Event getEvent() {
        return [SELECT Id, WhatId FROM Event];
    }
	
	@isTest
    private static void eventNotRelatedToIncident() {
        Event investigationEvent = new Event();

        Test.startTest();
        RelatedIntakesOnEventExt controller = new RelatedIntakesOnEventExt(new ApexPages.StandardController(investigationEvent));
        Test.stopTest();

        System.assert(!controller.getHasRelatedIntakes());
    }

    @isTest
    private static void incidentDoesNotHaveRelatedIntakes() {
        Event investigationEvent = getEvent();

        Database.delete([SELECT Id FROM IntakeInvestigationEvent__c]);

        Test.startTest();
        RelatedIntakesOnEventExt controller = new RelatedIntakesOnEventExt(new ApexPages.StandardController(investigationEvent));
        Test.stopTest();

        System.assert(!controller.getHasRelatedIntakes());
    }

    @isTest
    private static void incidentHasRelatedIntakes() {
        Event investigationEvent = getEvent();

        Test.startTest();
        RelatedIntakesOnEventExt controller = new RelatedIntakesOnEventExt(new ApexPages.StandardController(investigationEvent));
        Test.stopTest();

        System.assert(controller.getHasRelatedIntakes());
    }
}