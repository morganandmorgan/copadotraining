public without sharing class BatchManagerController {

    // ------------------------------------------------------------------------
    // Variables:
    // ------------------------------------------------------------------------
        // page alerts - when true renders page success message
        public boolean hasPageSuccess { get; set; }

        // page alerts - when true render page error message.
        public boolean hasPageError { get; set; }

        // page alerts - rendered when has page success is true
        public String pageSuccessMessage { get; set; }

        // page alerts - rendered when has page error is true
        public String pageErrorMessage { get; set; }

        // true when pending batches exists
        public boolean hasPendingBatches {get; set;}

        // true when generate statement button is enabled false otherwise
        public boolean enableBatchExecuteButton {get; set;}

        // true when the batch option calls for the batch size to be locked
        public boolean lockSuggestedBatchSize {get; set;}

        // last batch processed for display
        public BatchJob lastProcessedJob {get; set;}

        // standard parameters
        public List<SelectOption> batchNameOptions {get; set;}
        public List<SelectOption> batchSizeOptions {get; set;}
        public Batch_Option__c selectedBatchOption {get; set;}
        public Map<String, Batch_Option__c> batchOptionMap;
        public String selectedBatchName {get; set;}
        public String selectedBatchSize {get; set;}
        public String defaultBatchSize {get; set;}
        
        /**
        OPTIONAL PARAMETERS - these can be extended as needed by adding variables in the controller / page / and Batch_Option__c custom object
        **/
        /*
            Parameter 1 = FFA Period
            Parameter 2 = Start Date
            Parameter 3 = End Date
            Parameter 4 = FFA Company
            Parameter 5 = FFA General Ledger Account
            Parameter 6 = Generic Boolean value
        */

        public List<SelectOption> param1_Options {get; set;}
        public String param1Value {get; set;}
        public String param1Label {get; set;}
        public Boolean showParameter1 {get; set;}

        public Date param2Value {get; set;}
        public String param2Label {get; set;}
        public Boolean showParameter2 {get; set;}
        public Deposit__c param2holder {get; set;}

        public Date param3Value {get; set;}
        public String param3Label {get; set;}
        public Boolean showParameter3 {get; set;}
        public Deposit__c param3holder {get; set;}

        public List<SelectOption> param4_Options {get; set;}
        public String param4Value {get; set;}
        public String param4Label {get; set;}
        public Boolean showParameter4 {get; set;}

        public List<SelectOption> param5_Options {get; set;}
        public String param5Value {get; set;}
        public String param5Label {get; set;}
        public Boolean showParameter5 {get; set;}

        public Boolean param6Value {get; set;}
        public String param6Label {get; set;}
        public Boolean showParameter6 {get; set;}
        /*
        End optional Parameters
        */

        public Map<Id,String> companyIdNameMap = new Map<Id,String>();


    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    public BatchManagerController() {
        initPage();
    }

    // ------------------------------------------------------------------------
    // Initializes Page
    // ------------------------------------------------------------------------
    private void initPage()
    {
        defaultBatchSize = '200';
        enableBatchExecuteButton = true;
        lockSuggestedBatchSize = false;
        showParameter1 = false;
        showParameter2 = false;
        showParameter3 = false;
        showParameter4 = false;
        showParameter5 = false;
        showParameter6 = false;
        param2holder = new Deposit__c(Check_Date__c = Date.today().toStartofMonth());
        param3holder = new Deposit__c(Check_Date__c = Date.today());
        param1_Options = new List<SelectOption>();
        param4_Options = new List<SelectOption>();
        param5_Options = new List<SelectOption>();
        param6Value = false;
        batchNameOptions = new List<SelectOption>();
        batchSizeOptions = new List<SelectOption>();
        batchOptionMap = new Map<String, Batch_Option__c>(); //key = batch api name / value = Batch_Option__c
        companyIdNameMap = new Map<Id, String>();

        Set<String> allBatchApiOptions = new Set<String>();
        
        for(Batch_Option__c batchOpt : [
                SELECT Batch_API_Name__c, 
                    Name,  
                    Requires_Parameter_1__c,
                    Requires_Parameter_2__c,
                    Requires_Parameter_3__c,
                    Requires_Parameter_4__c,
                    Requires_Parameter_5__c,
                    Requires_Parameter_6__c,
                    Parameter_1_Label__c,
                    Parameter_2_Label__c,
                    Parameter_3_Label__c,
                    Parameter_4_Label__c,
                    Parameter_5_Label__c,
                    Parameter_6_Label__c,
                    Suggested_Batch_Size__c,
                    Lock_Suggested_Batch_Size__c
                FROM Batch_Option__c 
                ORDER BY Name]){
            batchOptionMap.put(batchOpt.Batch_API_Name__c, batchOpt);
            allBatchApiOptions.add(batchOpt.Batch_API_Name__c);
        }

        Set<String> authorizedApexClassNames = getApexClassAccess(allBatchApiOptions);

        for(Batch_Option__c bo : batchOptionMap.values()){
            if(authorizedApexClassNames.contains(bo.Batch_API_Name__c)){
                batchNameOptions.add(new SelectOption(bo.Batch_API_Name__c, bo.Name));
            }
            if(!batchNameOptions.isEmpty()){
                selectedBatchName = batchNameOptions[0].getValue();
            }
        }

        //Populate Parameter Select Options:
        for (c2g__codaPeriod__c period: [SELECT Id, Name FROM c2g__codaPeriod__c WHERE c2g__Closed__c = false AND c2g__Description__c = 'Trading Period' ORDER BY c2g__StartDate__c asc]){
            param1_Options.add(new SelectOption(period.Id, period.Name));
        }
        for(c2g__codaCompany__c ffaCompany : FFAUtilities.getCurrentCompanies()){
            param4_Options.add(new SelectOption(ffaCompany.Id, ffaCompany.Name));
            companyIdNameMap.put(ffaCompany.Id, ffaCompany.Name);
        }
        for(c2g__codaGeneralLedgerAccount__c gla : [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c ORDER BY Name]){
            param5_Options.add(new SelectOption(gla.Id, gla.Name));
        }

        
        //batch size:
        batchSizeOptions.add(new SelectOption('2000', '2000'));
        batchSizeOptions.add(new SelectOption('1000', '1000'));
        batchSizeOptions.add(new SelectOption('200', '200'));
        batchSizeOptions.add(new SelectOption('100', '100'));
        batchSizeOptions.add(new SelectOption('50', '50'));
        batchSizeOptions.add(new SelectOption('25', '25'));
        batchSizeOptions.add(new SelectOption('5', '5'));
        batchSizeOptions.add(new SelectOption('1', '1'));
        selectedBatchSize = defaultBatchSize;

        selectedBatchOption = selectedBatchName != null && selectedBatchName != '' ?  batchOptionMap.get(selectedBatchName) : null;
        showParameter1 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_1__c : false;
        showParameter2 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_2__c : false;
        showParameter3 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_3__c : false;
        showParameter4 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_4__c : false;
        showParameter5 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_5__c : false;
        showParameter6 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_6__c : false;
        param1Label = selectedBatchOption != null  && selectedBatchOption.Parameter_1_Label__c != null ? selectedBatchOption.Parameter_1_Label__c : 'FFA Period';
        param2Label = selectedBatchOption != null  && selectedBatchOption.Parameter_2_Label__c != null ? selectedBatchOption.Parameter_2_Label__c : 'Start Date';
        param3Label = selectedBatchOption != null  && selectedBatchOption.Parameter_3_Label__c != null ? selectedBatchOption.Parameter_3_Label__c : 'End Date';
        param4Label = selectedBatchOption != null  && selectedBatchOption.Parameter_4_Label__c != null ? selectedBatchOption.Parameter_4_Label__c : 'FFA Company (limited to current companies)';
        param5Label = selectedBatchOption != null  && selectedBatchOption.Parameter_5_Label__c != null ? selectedBatchOption.Parameter_5_Label__c : 'GLA';
        param6Label = selectedBatchOption != null  && selectedBatchOption.Parameter_6_Label__c != null ? selectedBatchOption.Parameter_6_Label__c : 'True / False';
        selectedBatchSize = selectedBatchOption != null && selectedBatchOption.Suggested_Batch_Size__c != null ? String.valueOf(selectedBatchOption.Suggested_Batch_Size__c) : defaultBatchSize;
        lockSuggestedBatchSize = selectedBatchOption != null && selectedBatchOption.Lock_Suggested_Batch_Size__c != null ? selectedBatchOption.Lock_Suggested_Batch_Size__c : false;

        hasPendingBatches = false;

        // look for pending or running batches!
        checkforPendingOrRunningBatches(false);
    }

    // ------------------------------------------------------------------------
    // action behind the Generate button
    // ------------------------------------------------------------------------
    public PageReference processJob()
    {
        
        clearPageErrorMessage();

        //cast any paramater values that came form sObject Fields
        param2Value = param2holder.Check_Date__c;
        param3Value = param3holder.Check_Date__c;

        system.debug(' ***** >>>> \n selectedBatchName  = ' + selectedBatchName);
        system.debug(' ***** >>>> \n param1Value  = ' + param1Value);
        system.debug(' ***** >>>> \n param2Value  = ' + param2Value);
        system.debug(' ***** >>>> \n param3Value  = ' + param3Value);
       
        try{
            /**
            IMPORTANT:
            Update the code here to provide specific batch code handling
            **/
            
            if(selectedBatchName == 'ffaDepositAllocationBatchSchedule'){
                ffaDepositAllocationBatchSchedule b = new ffaDepositAllocationBatchSchedule(param3Value, param4Value);
                Id batchId = Database.executeBatch(b, Integer.valueOf(selectedBatchSize));
            }
            else if(selectedBatchName == 'FFAPrepaidExpense_Job'){
                FFAPrepaidExpense_Job b = new FFAPrepaidExpense_Job(param3Value, param4Value, param6Value);
                Id batchId = Database.executeBatch(b, Integer.valueOf(selectedBatchSize));
            }
            
            checkforPendingOrRunningBatches(true);
        }
        catch(Exception e)
        {
            System.debug('EXCEPTION: ' + e.getMessage() + e.getLineNumber() + e.getStackTraceString()); 
            addPageErrorMessage(e.getMessage());
        }
        return null;
    }

    // ------------------------------------------------------------------------
    // action behind changes to selected batch job
    // ------------------------------------------------------------------------
    public void evalParamsAndReloadJobs(){
        clearPageErrorMessage();

        selectedBatchOption = selectedBatchName != null && selectedBatchName != '' ?  batchOptionMap.get(selectedBatchName) : null;
        showParameter1 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_1__c : false;
        showParameter2 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_2__c : false;
        showParameter3 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_3__c : false;
        showParameter4 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_4__c : false;
        showParameter5 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_5__c : false;
        showParameter6 = selectedBatchOption != null ? selectedBatchOption.Requires_Parameter_6__c : false;
        param1Label = selectedBatchOption != null  && selectedBatchOption.Parameter_1_Label__c != null ? selectedBatchOption.Parameter_1_Label__c : 'FFA Period';
        param2Label = selectedBatchOption != null  && selectedBatchOption.Parameter_2_Label__c != null ? selectedBatchOption.Parameter_2_Label__c : 'Start Date';
        param3Label = selectedBatchOption != null  && selectedBatchOption.Parameter_3_Label__c != null ? selectedBatchOption.Parameter_3_Label__c : 'End Date';
        param4Label = selectedBatchOption != null  && selectedBatchOption.Parameter_4_Label__c != null ? selectedBatchOption.Parameter_4_Label__c : 'FFA Company (limited to current companies)';
        param5Label = selectedBatchOption != null  && selectedBatchOption.Parameter_5_Label__c != null ? selectedBatchOption.Parameter_5_Label__c : 'GLA';
        param6Label = selectedBatchOption != null  && selectedBatchOption.Parameter_6_Label__c != null ? selectedBatchOption.Parameter_6_Label__c : 'True / False';
        selectedBatchSize = selectedBatchOption != null && selectedBatchOption.Suggested_Batch_Size__c != null ? String.valueOf(selectedBatchOption.Suggested_Batch_Size__c) : defaultBatchSize;
        lockSuggestedBatchSize = selectedBatchOption != null && selectedBatchOption.Lock_Suggested_Batch_Size__c != null ? selectedBatchOption.Lock_Suggested_Batch_Size__c : false;

        // if last status is not completed, reload the last job Completed
        Boolean renderLast = false;
        if (lastProcessedJob != null && lastProcessedJob.status != 'Completed')
        {
            renderLast = true;
        }

        //if a batch job is selected which requires an ffa company and the company listing is blank, display a message to the user
        if(showParameter4 == true && param4_Options.isEmpty()){
            addPageErrorMessage('Attention: the selected batch job requires the selection of an FFA company. Currently, your user is not set up with any current companies. Navigate to the "Select Company" tab and make a selection in order to continue.');
        }

        checkforPendingOrRunningBatches(renderLast);
    }

    // ------------------------------------------------------------------------
    // action behind reloading pending jobs
    // ------------------------------------------------------------------------
    public void reloadPendingJobs()
    {
        clearPageErrorMessage();
        // if last status is not completed, reload the last job Completed
        Boolean renderLast = false;
        if (lastProcessedJob != null && lastProcessedJob.status != 'Completed')
        {
            renderLast = true;
        }

        checkforPendingOrRunningBatches(renderLast);
    }

    // ------------------------------------------------------------------------
    // -- page alerts
    // ------------------------------------------------------------------------
    public void clearPageErrorMessage()
    {
        hasPageError = false;
        pageErrorMessage = '';

        hasPageSuccess = false;
        pageSuccessMessage = '';

    }

    public void addPageErrorMessage(String msg)
    {
        hasPageError = true;
        pageErrorMessage = msg;
    }

    public void addPageSuccessMessage(String msg)
    {
        hasPageSuccess = true;
        pageSuccessMessage = msg;
    }

    // ------------------------------------------------------------------------
    // Looks to see if any pending jobs are running
    //
    // renderLastProcess -boolean when true renders last process job regardless of pending....
    // if so updates page indicator
    // and loads last batch processed info!
    // ------------------------------------------------------------------------
    public void checkforPendingOrRunningBatches(boolean renderLastProcess)
    {
        // count the number of jobs processing, queued, holding, or preparing
        Integer pendingJobs = 0;
        AggregateResult[] aggrRslts = [
                SELECT count(Id)
                  FROM AsyncApexJob
                 WHERE ApexClass.Name = :selectedBatchName
                   AND Status in ('Processing', 'Queued', 'Preparing', 'Holding')];
        if (aggrRslts != null && aggrRslts.size() > 0)
        {
            pendingJobs = Integer.valueOf(aggrRslts[0].get('expr0'));
        }

        // when we have pending jobs, or want to render the last process info...
        if (pendingJobs > 0 || renderLastProcess)
        {
            // either pull all totals for all pending jobs....or grab counts from last element
            Integer pendingTotalJobsProcessed = 0;
            Integer pendingTotalJobItems = 0;

            // load totals for pending jobs....
            if (pendingJobs > 0)
            {
                AggregateResult[] aggrTotals = [
                        SELECT sum(JobItemsProcessed), sum(TotalJobItems)
                          FROM AsyncApexJob
                         WHERE ApexClass.Name = :selectedBatchName
                           AND Status in ('Processing', 'Queued', 'Preparing', 'Holding')];

                if (aggrTotals != null && aggrTotals.size() > 0)
                {
                    pendingTotalJobsProcessed = Integer.valueOf(aggrTotals[0].get('expr0'));
                    pendingTotalJobItems = Integer.valueOf(aggrTotals[0].get('expr1'));
                }
            }

            hasPendingBatches = true;

            // disable the generate button if there are pending jobs present

            enableBatchExecuteButton = pendingJobs <= 0;

            // create the last process job for rendering in UI
            lastProcessedJob = new BatchJob();
            lastProcessedJob.status = '-loading info-';

            // load last processed job.
            AsyncApexJob[] asynApexJobs = [
                 SELECT Id,
                        Status,
                        NumberOfErrors,
                        JobItemsProcessed,
                        TotalJobItems,
                        CreatedDate,
                        ApexClassId,
                        ApexClass.Name
                   FROM AsyncApexJob
                  WHERE ApexClass.Name = :selectedBatchName
                 order by CreatedDate DESC
                 limit 1 ];

            if (asynApexJobs != null && asynApexJobs.size() > 0)
            {
                lastProcessedJob.status            = asynApexJobs[0].Status;
                lastProcessedJob.createdDate       = asynApexJobs[0].CreatedDate;
                lastProcessedJob.numberOfErrors    = asynApexJobs[0].NumberOfErrors;

                // ---- processed and totals.....(from last job)
                Integer jobItemsProcessed = asynApexJobs[0].JobItemsProcessed;
                Integer totalJobItems     = asynApexJobs[0].TotalJobItems;

                // --- processed and totals from all pending ...
                if (pendingJobs > 0 && pendingTotalJobItems > 0)
                {
                    jobItemsProcessed = pendingTotalJobsProcessed;
                    totalJobItems = pendingTotalJobItems;
                }

                lastProcessedJob.jobItemsProcessed = jobItemsProcessed;
                lastProcessedJob.totalJobItems     = totalJobItems;

                Decimal percent = 1.00;
                if (jobItemsProcessed != null && totalJobItems != null && totalJobItems > 0)
                {
                    Decimal jobItemsProcessed_dec = jobItemsProcessed;
                    Decimal totalJobItems_dec = totalJobItems;
                    percent = (jobItemsProcessed_dec / totalJobItems_dec)*100;
                    system.debug(' ***** >>>> \n jobItemsProcessed  = ' + jobItemsProcessed);
                    system.debug(' ***** >>>> \n totalJobItems  = ' + totalJobItems);
                    system.debug(' ***** >>>> \n percent  = ' + percent);
                    percent = (percent > 0.00) ? percent : 1;

                }
                lastProcessedJob.percentComplete = percent + '%';

                system.debug(' ***** >>>> \n totalJobItems  = ' + totalJobItems);
                system.debug(' ***** >>>> \n jobItemsProcessed  = ' + jobItemsProcessed);
                system.debug(' ***** >>>> \n percentComplete  = ' + lastProcessedJob.percentComplete);
                }
        }
        else
        {
            hasPendingBatches = false;
            enableBatchExecuteButton = true;
        }
    }

    // ------------------------------------------------------------------------
    // Wrapper class for info about the last async apex job
    // provides the following attributes:
    //  * status            - String value of the status associated to the last processed job
    //  * createdDate       - DateTime value of when the job was created
    //  * jobItemsProcessed - Number of items processed
    //  * totalJobItems     - Number of items to process
    //  * numberOfErrors    - Number of errors
    //  * createdDateFmt    - String value of a formatted created date
    //  * percentComplete   - String value for calculation based on job items process and total job items
    //  * generateMessage   - String value for message to render in pending box
    // ------------------------------------------------------------------------
    public class BatchJob
    {
        public String  status {get; set;}


        public DateTime createdDate {get; set;}
        public Integer jobItemsProcessed {get; set;}
        public Integer totalJobItems {get; set;}
        public Integer numberOfErrors {get; set;}

        // format the created date
        public String createdDateFmt {get{
            if (createdDateFmt == null && createdDate != null) {
                createdDateFmt = createdDate.format('yyyy-MM-dd hh:mm');
            }
            return createdDateFmt; } set;}

        // calculate percent complete based on job items process and total job items
        public String percentComplete{get;
            /*Decimal percent = 1;
            if (jobItemsProcessed != null && totalJobItems != null && totalJobItems > 0)
            {
                percent = ((jobItemsProcessed / totalJobItems) * 100);
                percent = (percent > 0) ? percent : 1;

            }
            percentComplete = percent + '%';
            system.debug(' ***** >>>> \n totalJobItems  = ' + totalJobItems);
            system.debug(' ***** >>>> \n jobItemsProcessed  = ' + jobItemsProcessed);
            system.debug(' ***** >>>> \n percentComplete  = ' + percentComplete);
            return percentComplete;*/

            set;}


        public String generateMessage {get {
            if (generateMessage == null) {
                if (status == 'Completed')
                {
                    generateMessage =  'Selected Batch Job has Completed.';
                }
                else
                {
                    generateMessage = 'Selected Batch Job is currently in progress...';
                }
            }
            return generateMessage;
            } set; }


        public BatchJob()
        {

        }
    }

    //-----------------------------------------------------------------------------------
    // Check for Apex Class Access
    // - returns a set of apex class strings that the current user has access to.
    //-----------------------------------------------------------------------------------
    public Set<String> getApexClassAccess (Set<String> filterSet){

        Set<String> returnString = new Set<String>();
        Id currentUserId = UserInfo.getUserId();
        Set<Id> userProfilePermissionSetIds = new Set<Id>();
        userProfilePermissionSetIds.add(UserInfo.getProfileId());


        for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId 
            FROM PermissionSetAssignment 
            WHERE AssigneeId = :currentUserId]){
            userProfilePermissionSetIds.add(psa.PermissionSetId);
        }

        for(ApexClass classobj : [SELECT NamespacePrefix, Name FROM ApexClass 
            WHERE Id IN 
                (SELECT SetupEntityId 
                 FROM SetupEntityAccess 
                 WHERE ParentId in :userProfilePermissionSetIds)
            AND Name in : filterSet
            AND NamespacePrefix = null]){
            returnString.add(classobj.Name);
        }
        return returnString;
    }
}