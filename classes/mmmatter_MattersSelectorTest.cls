@isTest
private class mmmatter_MattersSelectorTest
{
    public static testMethod void mmmatter_MattersSelector_selectById() {
        mmmatter_MattersSelector.newInstance().selectById(new Set<Id>());
    }

    public static testMethod void mmmatter_MattersSelector_selectMyOpenWithoutFutureActivities() {
        mmmatter_MattersSelector.newInstance().selectMyOpenWithoutFutureActivities();
    }

    public static testMethod void mmmatter_MattersSelector_selectByIntake() {
        mmmatter_MattersSelector.newInstance().selectByIntake(new Set<Id>());
    }

    public static testMethod void mmmatter_MattersSelector_selectWithFieldsetByIntake() {
        try {
            Schema.FieldSet fieldSet;
            mmmatter_MattersSelector.newInstance().selectWithFieldsetByIntake(fieldSet, new Set<Id>());
        } catch (Exception e) {

        }
    }

    public static testMethod void mmmatter_MattersSelector_selectByName() {
        mmmatter_MattersSelector.newInstance().selectByName(new Set<String>());
    }

    public static testMethod void mmmatter_MattersSelector_selectByReferenceNumber() {
        mmmatter_MattersSelector.newInstance().selectByReferenceNumber(new Set<String>());
    }

    public static testMethod void codeCoverage() {
        mmmatter_MattersSelector.codeCoverage();
    }
}