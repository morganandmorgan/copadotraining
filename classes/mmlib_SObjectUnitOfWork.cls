/**
 *  mmlib_SObjectUnitOfWork
 */
public class mmlib_SObjectUnitOfWork
    extends fflib_SObjectUnitOfWork
    implements mmlib_ISObjectUnitOfWork
{
    public mmlib_SObjectUnitOfWork( List<Schema.SObjectType> sObjectTypes )
    {
        super(sObjectTypes);
    }

    public mmlib_SObjectUnitOfWork(List<Schema.SObjectType> sObjectTypes, fflib_SObjectUnitOfWork.IDML dml)
    {
        super(sObjectTypes, dml);
    }

    public list<SObject> getNewRecordsByType( Schema.SObjectType typeToFind )
    {
        return typeToFind == null ? null : m_newListByType.get( typeToFind.getDescribe().getName() );
    }

    public Map<Id, SObject> getDirtyRecordsByType( Schema.SObjectType typeToFind )
    {
        return typeToFind == null ? null : m_dirtyMapByType.get( typeToFind.getDescribe().getName() );
    }

    public Map<Id, SObject> getDeletedRecordsByType( Schema.SObjectType typeToFind )
    {
        return typeToFind == null ? null : m_deletedMapByType.get( typeToFind.getDescribe().getName() );
    }

    public void register(SObject record)
    {
        if ( record != null )
        {
            if ( record.id == null )
            {
                this.registerNew( record );
            }
            else
            {
                this.registerDirty( record );
            }
        }
    }

    public void register(List<SObject> records)
    {
        if ( records != null )
        {
            for ( SObject record : records )
            {
                if ( record.id == null )
                {
                    this.registerNew( record );
                }
                else
                {
                    this.registerDirty( record );
                }
            }
        }
    }

    public void register(SObject record, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord)
    {
        if ( record != null )
        {
            if ( record.id == null )
            {
                this.registerNew( record, relatedToParentField, relatedToParentRecord);
            }
            else
            {
                this.registerDirty( record, relatedToParentField, relatedToParentRecord);
            }
        }

    }

    /**
     * Registers a group of emails to be sent during the commitWork
     **/
    public void registerEmails( list<Messaging.Email> emails )
    {
        if ( emails != null )
        {
            for ( Messaging.Email email : emails )
            {
                this.registerEmail(email);
            }
        }
    }
}