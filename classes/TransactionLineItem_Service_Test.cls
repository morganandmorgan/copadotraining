/**
 * TransactionLineItem_Service_Test 
 * @description Test for Transaction Line Service
 * @author CLD Partners
 * @date 2/20/2019
 */
@IsTest
public with sharing class TransactionLineItem_Service_Test {

    private static Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals;

    @IsTest
    private static void ctor() {
        TransactionLineItem_Service tliService = new TransactionLineItem_Service();
        System.assert(tliService != null);
    }

    @IsTest
    private static void test_createTrustTransactions() {

        //post the journal
        Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.createJournals(1, true, 'Bank Account', 10, Date.today(), TestDataFactory_FFA.balanceSheetGLAs[0].Id, null, null, null, null, null, TestDataFactory_FFA.bankAccounts[0].Id);
        TestDataFactory_FFA.postJournals(journals.keySet());

        
        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.RecordTypeId = mmBusinessAccount;
        insert mmAccount;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
    
        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = mmAccount.Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = mmAccount.Id;
        matter.litify_pm__Status__c = 'Open';
        insert matter;

        //select the trans line items:
        List<c2g__codaTransactionLineItem__c> tliList = [SELECT Id, Matter__c, c2g__BankAccount__c, c2g__BankAccount__r.Bank_Account_Type__c FROM c2g__codaTransactionLineItem__c];
        for(c2g__codaTransactionLineItem__c tli : tliList){
            tli.Matter__c = matter.Id;   
        }
        update tliList;
        system.debug('TEST CONTEXT - tliList'+tliList);

        TransactionLineItem_Service tliService = new TransactionLineItem_Service();
        tliService.createTrustTransactions(tliList);
    }

    @IsTest
    private static void populateMatterField() {

        Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.createJournals(1, true, 'Bank Account', 10, Date.today(), TestDataFactory_FFA.balanceSheetGLAs[0].Id, null, null, null, null, null, TestDataFactory_FFA.bankAccounts[0].Id);
        TestDataFactory_FFA.postJournals(journals.keySet());

        //select the trans line items:
        List<c2g__codaTransactionLineItem__c> tliList = [SELECT Id, c2g__Transaction__c, Matter__c FROM c2g__codaTransactionLineItem__c];

        TransactionLineItem_Service tliService = new TransactionLineItem_Service();
        tliService.populateMatterField(tliList);
    }
}