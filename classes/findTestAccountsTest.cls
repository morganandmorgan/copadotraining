@isTest
private class findTestAccountsTest {
	
	@isTest
    private static void testAccountsRetrieved() {
        List<Account> matchingAccounts = new List<Account>();
        for (Integer i = 0; i < 3; ++i) {
            Account a = TestUtil.createAccount('test_precededbytest_');
            matchingAccounts.add(a);
        }

        List<Account> nonMatchingAccounts = new List<Account>();
        for (Integer i = 0; i < 3; ++i) {
            Account a = TestUtil.createAccount('notprecededbytest_' + i);
            nonMatchingAccounts.add(a);
        }

        List<Account> toInsert = new List<Account>();
        toInsert.addAll(matchingAccounts);
        toInsert.addAll(nonMatchingAccounts);
        Database.insert(toInsert);

        Test.startTest();
        findTestAccounts service = new findTestAccounts();
        List<Account> result = service.getTestAccounts();
        Test.stopTest();

        Map<Id, Account> resultMap = new Map<Id, Account>(result);
        Map<Id, Account> matchingMap = new Map<Id, Account>(matchingAccounts);

        System.assertEquals(matchingMap.keySet(), resultMap.keySet());
    }
	
}