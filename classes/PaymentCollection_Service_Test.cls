/*============================================================================/
* PaymentCollection_Service_Test
* @description Test for PaymentCollectionTrigger / Domain / Service
* @author Brian Krynitsky
* @date Jun 2019
=============================================================================*/
@isTest
private without sharing class PaymentCollection_Service_Test {
	
	private static c2g__codaCompany__c company;
    private static c2g__codaBankAccount__c operatingBankAccount;

	private static void setupData(){
    	/*--------------------------------------------------------------------
        FFA data setup
        --------------------------------------------------------------------*/
		company = TestDataFactory_FFA.company;

        operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
        update operatingBankAccount;
	} 

	@isTest
    private static void testPaymentCollection_DomainConstructor() {
        PaymentCollection_Domain pcDomain = new PaymentCollection_Domain();
        System.assert(pcDomain != null);
    }
	
	@isTest
    private static void testInsertPaymentCollection() {
        setupData();

		Payment_Collection__c testPC = new Payment_Collection__c(
			Company__c = company.Id,
			Bank_Account__c = operatingBankAccount.Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		insert testPC;
    }
	
}