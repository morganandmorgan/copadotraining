/**
 *  mmmarketing_ExperimentsServiceImpl
 */
public with sharing class mmmarketing_ExperimentsServiceImpl
    implements mmmarketing_IExperimentsService
{
    public void setupExperimentsAndVariationsForMTIs( list<Marketing_Tracking_Info__c> mtiRecords )
    {
        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        // get all of the experiment values from the mtiRecords
        set<string> experimentValuesSet = mmlib_Utils.generateStringSetFromField( mtiRecords, Marketing_Tracking_Info__c.Experiment_Value__c );

        list<Marketing_Experiment__c> meRecords = mmmarketing_ExperimentsSelector.newInstance().selectWithVariationsByExperimentName( experimentValuesSet );

        map<String, SObject> marketingExperimentsByExpNameMap = mmlib_Utils.generateSObjectMapByUniqueField( meRecords, Marketing_Experiment__c.Experiment_Name__c );

        Marketing_Experiment__c marketingExperiment = null;
        Marketing_Exp_Variation__c marketingExperimentVariation = null;

        map<String, SObject> marketingExperimentVariationByNameMap = new map<String, SObject>();

        for ( Marketing_Tracking_Info__c record : mtiRecords )
        {
            if ( ! marketingExperimentsByExpNameMap.containsKey( record.Experiment_Value__c ) )
            {
                marketingExperiment = generateMarketingExperiment( record.Experiment_Value__c );
                uow.registerNew( marketingExperiment );
                marketingExperimentsByExpNameMap.put( record.Experiment_Value__c, marketingExperiment );
            }
            else
            {
                marketingExperiment = (Marketing_Experiment__c)marketingExperimentsByExpNameMap.get( record.Experiment_Value__c );
            }

            // now find the correct Marketing_Exp_Variation__c record.
            marketingExperimentVariationByNameMap = mmlib_Utils.generateSObjectMapByUniqueField( marketingExperiment.Marketing_Exp_Variations__r, Marketing_Exp_Variation__c.Experiment_Variation_Name__c );

            if ( ! marketingExperimentVariationByNameMap.containsKey( record.Experiment_Variation_Value__c ) )
            {
                // no Marketing_Exp_Variation__c record exists for this variation yet.  create one
                marketingExperimentVariation = generateMarketingExperimentVariation( record.Experiment_Variation_Value__c );

                uow.registerNew( marketingExperimentVariation );

                uow.registerRelationship( marketingExperimentVariation, Marketing_Exp_Variation__c.Marketing_Experiment__c, marketingExperiment);
            }
            else
            {
                marketingExperimentVariation = (Marketing_Exp_Variation__c) marketingExperimentVariationByNameMap.get( record.Experiment_Variation_Value__c );
            }

            // now connect the Marketing_Exp_Variation__c to the Marketing_Tracking_Info__c record
            uow.registerRelationship( record, Marketing_Tracking_Info__c.Marketing_Exp_Variation__c, marketingExperimentVariation);

            uow.registerDirty( record );
        }

        uow.commitWork();
    }

    private Marketing_Experiment__c generateMarketingExperiment( String experimentName )
    {
        return new Marketing_Experiment__c( Experiment_Name__c = experimentName );
    }

    private Marketing_Exp_Variation__c generateMarketingExperimentVariation( String experimentVariationName )
    {
        return new Marketing_Exp_Variation__c( Experiment_Variation_Name__c = experimentVariationName );
    }
}