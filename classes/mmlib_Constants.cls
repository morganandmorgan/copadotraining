/**
 *  mmlib_Constants
 */
public class mmlib_Constants
{
    public static final String SOQL_DATE_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSZ';

    public static final String MORGAN_AND_MORGAN_FIRM_NAME = 'Morgan & Morgan';

    private mmlib_Constants() {  }
}