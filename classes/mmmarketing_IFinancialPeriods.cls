public interface mmmarketing_IFinancialPeriods extends fflib_ISObjectDomain
{
	void autoCreateMarketingCampaignsIfRequired();
	void autoCreateMarketingSourcesIfRequired();
    void autoLinkToMarketingInformationIfRequired( mmlib_ISObjectUnitOfWork uow );
	void autoLinkToMarketingInformationIfRequired();
	void autoLinkToMarketingInformationIfRequiredInQueueableMode();
}