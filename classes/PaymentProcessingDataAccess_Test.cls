@isTest
private without sharing class PaymentProcessingDataAccess_Test {

    static Payment_Collection__c createAndInsertPaymentCollection(){
        //Note:  These are better candidates for reuse.
        System.debug('Creating payment collection...');

        Payment_Collection__c record = null;
     
		record = new Payment_Collection__c(
			Company__c = TestDataFactory_FFA.company.Id,
			Bank_Account__c = TestDataFactory_FFA.bankAccounts[0].Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		insert record;
        return record;

    }
    
    static c2g__codaPayment__c createAndInsertPayment(){
        return createAndInsertPayment(null);
    }    
    
    static c2g__codaPayment__c createAndInsertPayment(Payment_Collection__c paymentCollection){
        
        c2g__codaPayment__c record = null;

        System.debug('Creating payment object...');

        if(paymentCollection==null){
            paymentCollection=createAndInsertPaymentCollection();
        }

        {

            //Note:  Looks like cnt param doesn't do anything
            record=TestDataFactory_FFA.createPayment(1, false);

            record.Payment_Collection__c = paymentCollection.Id;

            System.assert((record.Payment_Collection__c!=null), 'Payment.Payment_Collection__c is null.');
            
            {
                Database.SaveResult result = null;

                System.debug('Inserting payment object...');               
                
                result=Database.insert(record);

                System.assert(result.isSuccess(), 'Payment insert failed.');

            }

            System.assert(record!=null, 'Payment is null.');
            System.assert(record.Id!=null, 'Payment Id is null.');

        }

        return record;

    }     
    
    @isTest 
    static void test00(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
    }

    @isTest 
    static void testCreate0(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
    }
    
    @isTest 
    static void test0(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);

        //List<c2g__codaPaymentAccountLineItem__c> getPaymentSummaryAccounts(String paymentCollectionId){
        List<c2g__codaPaymentAccountLineItem__c> results = null;
        results = data.getPaymentSummaryAccounts(paymentCollectionId);
        results = data.getPaymentSummaryAccounts_Void(paymentCollectionId);

        System.assert(paymentCollectionId!=null);
                 
    }

    @isTest 
    static void testGetTransLines0(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        List<c2g__codaTransactionLineItem__c> results = null;
        results = data.getTransactionLineItems(TestDataFactory_FFA.company.Id, null, null, null, null, null, null, null, null);

        System.assert(data!=null);
                 
    }    

    // @isTest 
    // static void testGetTransLines1(){
    //     PaymentProcessingDataAccess data = null;
    //     data = new PaymentProcessingDataAccess();
    //     System.assert(data!=null);
    //     Id paymentCollectionId = null;
    //     paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        
    //     List<c2g__codaTransactionLineItem__c> results = null;
    //     results = data.getTransactionLineItems(paymentCollectionId);

    //     System.assert(paymentCollectionId!=null);
                 
    // }     
        
    @isTest 
    static void test2(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
                
        List<c2g__codaPayment__c> results = null;
        results = data.getPayments(paymentCollectionId);

        System.assert(paymentCollectionId!=null);
                 
    }
       
    @isTest 
    static void test3(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        
        Payment_Collection__c results = null;
        results = data.getPaymentCollection(paymentCollectionId);

        System.assert(paymentCollectionId!=null);
                 
    }
     
    @isTest 
    static void test4(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
                
        PaymentProcessing.PaymentCollectionWrapper results = null;
        results = data.loadPaymentCollectionData(paymentCollectionId);

        System.assert(paymentCollectionId!=null);
                 
    }

    @isTest 
    static void test5(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        
        List<Payment_Processing_Batch_Status__c> results = null;
        results = data.findBatchStatusMonitor(paymentCollectionId);

        System.assert(paymentCollectionId!=null);
        
    }          
     
    @isTest 
    static void test6(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        
        List<c2g__codaPaymentMediaSummary__c> results = null;
        results = data.getPaymentMediaSummaries(paymentCollectionId);
        results = data.getPaymentMediaSummaries_Void(paymentCollectionId);
        
        System.assert(paymentCollectionId!=null);
        
    }          
          
    @isTest 
    static void test7(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

        Set<Id> ids = null;
        ids = new Set<Id>();
        ids.add(paymentId);
        
        List<c2g__codaPayment__c> results = null;
        results = data.getPaymentsReadyToPost(ids);

        System.assert(paymentId!=null);
        
    }          
    
    @isTest
    static void test8(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        //List<c2g__codaPayment__c> getPaymentsByIds(Set<String> paymentIds){
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

        Set<Id> ids = null;
        ids = new Set<Id>();
        ids.add(paymentId);
        
        List<c2g__codaPayment__c> results = null;
        results = data.getPaymentsByIds(ids);

        System.assert(paymentId!=null);
        
    }          

    @isTest
    static void test9(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

        Set<Id> ids = null;
        ids = new Set<Id>();
        ids.add(paymentId);
        
        List<c2g__codaPaymentAccountLineItem__c> results = null;
        results = data.getPaymentSummaryAccounts(ids);

        System.assert(paymentId!=null);
        
    }
    
    @isTest
    static void test10(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

        List<Id> ids = null;
        ids = new List<Id>();
        ids.add(paymentId);
        
        List<c2g__codaPaymentAccountLineItem__c> results = null;
        results = data.getPaymentSummaryAccounts(ids);

        System.assert(paymentId!=null);
        
    }

    @isTest
    static void test11(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

        List<Id> ids = null;
        ids = new List<Id>();
        ids.add(paymentId);
        
        List<c2g__codaPaymentMediaSummary__c> results = null;
        results = data.getPaymentMediaSummaries(ids);

        System.assert(paymentId!=null);
        
    }

    @isTest
    static void testGetCheckRanges(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id bankAccountId = null;
        bankAccountId = TestDataFactory_FFA.getFakeRecordId(c2g__codaBankAccount__c.sObjectType);
            
        List<c2g__codaCheckRange__c> results = null;
        results = data.getCheckRanges(bankAccountId);

        System.assert(bankAccountId!=null);
        
    }

    @isTest
    static void testGetCheckRanges1(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id bankAccountId = null;
        bankAccountId = TestDataFactory_FFA.bankAccounts[0].Id;
            
        List<c2g__codaCheckRange__c> results = null;
        results = data.getCheckRanges(bankAccountId);

        System.assert(bankAccountId!=null);
        
    }
    
    @isTest    
    static void testGetCheckRanges2(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
                    
        List<c2g__codaCheckRange__c> results = null;
        results = data.getCheckRanges(TestDataFactory_FFA.bankAccounts[0]);

        System.assert(TestDataFactory_FFA.bankAccounts[0]!=null);
        
    }

    //
    @isTest
    static void testGetCheckRanges3(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id bankAccountId = null;
        bankAccountId = TestDataFactory_FFA.bankAccounts[0].Id;
        TestDataFactory_FFA.createCheckRange();
            
        List<c2g__codaCheckRange__c> results = null;
        results = data.getCheckRanges(bankAccountId);

        System.assert(bankAccountId!=null);
        
    }
    
    @isTest    
    static void testGetCheckRanges4(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);

        Id bankAccountId = null;
        bankAccountId = TestDataFactory_FFA.bankAccounts[0].Id;        

        TestDataFactory_FFA.createCheckRange();
                    
        List<c2g__codaCheckRange__c> results = null;
        results = data.getCheckRanges(TestDataFactory_FFA.bankAccounts[0]);

        System.assert(TestDataFactory_FFA.bankAccounts[0]!=null);
        
    }    
    
    @isTest
    static void test13(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id bankAccountId = null;
        bankAccountId = TestDataFactory_FFA.getFakeRecordId(c2g__codaBankAccount__c.sObjectType);
            
        Integer results = null;
        results = data.getNextCheckNumber(bankAccountId);

        System.assert(bankAccountId!=null);
        
    }

    @isTest
    static void test14(){
        PaymentProcessingDataAccess data = null;
        data = new PaymentProcessingDataAccess();
        System.assert(data!=null);
        
        Id recordId = null;
        recordId = TestDataFactory_FFA.getFakeRecordId(c2g__codaCheckRange__c.sObjectType);

        c2g__codaCheckRange__c checkRange = null;
        checkRange = new c2g__codaCheckRange__c();
        checkRange.Id = recordId;
            
        Integer results = null;
        results = data.getNextCheckNumber(checkRange);

        System.assert(recordId!=null);
        
    }

    @isTest 
    static void testLoadPaymentCollectionData0(){
        PaymentProcessingDataAccess target = null;
        target = new PaymentProcessingDataAccess();
        System.assert(target!=null);
        
        //c2g__codaPayment__c.sObjectType
        //PaymentProcessing.PaymentCollectionWrapper loadPaymentCollectionData(Id paymentCollectionId){
        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);
        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper=null;

        paymentCollectionWrapper=target.loadPaymentCollectionData(paymentCollectionId);

        System.assert(paymentCollectionId!=null);

    }

    @isTest 
    static void testLoadPaymentCollectionData1(){
        PaymentProcessingDataAccess target = null;
        target = new PaymentProcessingDataAccess();
        System.assert(target!=null);
        
        Payment_Collection__c paymentCollection = null;
        paymentCollection=createAndInsertPaymentCollection();
    
        c2g__codaPayment__c payment = null;
        payment=createAndInsertPayment(paymentCollection);

        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;

        paymentCollectionWrapper=target.loadPaymentCollectionData(paymentCollection.Id);

        System.assert(paymentCollection.Id!=null);
        System.assert(paymentCollectionWrapper!=null);

    }        
    
}