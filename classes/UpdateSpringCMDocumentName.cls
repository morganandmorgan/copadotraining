public class UpdateSpringCMDocumentName {
    
    @future(callout=true)
    public static void sendDocnametoSpringCM(string recordId, string newName, string docType)
    {
        SpringCMService springcm = new SpringCMService(UserInfo.getSessionId());
        string xml = '';
            xml += '<document>';
            xml += '<id>' + recordId + '</id>';
        	xml += '<name>' + newName +'</name>'; 
        	xml += '<docType>' + docType + '</docType>';
            xml += '</document>';
       		SpringCMWorkflow workflow = new SpringCMWorkflow('Update Document Name', xml);
            workflow = springcm.startWorkflow(workflow);
    }

}