@isTest
private class UpdateIntakeFromTaskTest {

    private static final Integer BULK_SIZE = 20;
    private static final String TASK_STATUS_COMPLETED = 'Completed';
    private static final String TASK_STATUS_INCOMPLETE = 'In Progress';
    private static final String TASK_CALLTYPE_INBOUND = 'Inbound';
    private static final String TASK_CALLTYPE_OUTBOUND = 'Outbound';

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        List<Account> intakeClients = new List<Account>();
        List<Account> otherPersonAccounts = new List<Account>();
        for (Integer i = 0; i < 3; ++i) {
            intakeClients.add(TestUtil.createPersonAccount('Intake', 'Person' + i));
            otherPersonAccounts.add(TestUtil.createPersonAccount('Other', 'Person' + i));
        }
        toInsert.addAll((List<SObject>) intakeClients);
        toInsert.addAll((List<SObject>) otherPersonAccounts);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Account client : intakeClients) {
            for (Integer i = 0; i < 5; ++i) {
                Intake__c intake = new Intake__c(
                        Client__c = client.Id,
                        Last_Dial_Time__c = null,
                        Number_Outbound_Dials_Intake__c = 0
                    );
                intakes.add(intake);
            }
        }
        toInsert.addAll((List<SObject>) intakes);

        Database.insert(intakes);
        toInsert.clear();

        List<Task> intakeTasks = new List<Task>();
        List<Task> accountTasks = new List<Task>();
        List<Task> inboundCallTasks = new List<Task>();
        for (Intake__c intake : intakes) {
            for (Integer i = 0; i < BULK_SIZE; ++i) {
                Task t = new Task(
                        CallType = TASK_CALLTYPE_OUTBOUND,
                        Status = TASK_STATUS_INCOMPLETE,
                        Subject = 'Test Outbound Call',
                        WhatId = intake.Id
                    );
                intakeTasks.add(t);
            }

            inboundCallTasks.add(new Task(
                    CallType = TASK_CALLTYPE_INBOUND,
                    Status = TASK_STATUS_INCOMPLETE,
                    Subject = 'Test Inbound Call',
                    WhatId = intake.Id
                ));
        }
        for (Account a : otherPersonAccounts) {
            for (Integer i = 0; i < BULK_SIZE; ++i) {
                Task t = new Task(
                        CallType = TASK_CALLTYPE_OUTBOUND,
                        Status = TASK_STATUS_INCOMPLETE,
                        Subject = 'Test Outbound Call',
                        WhatId = a.Id
                    );
                accountTasks.add(t);
            }

            inboundCallTasks.add(new Task(
                    CallType = TASK_CALLTYPE_INBOUND,
                    Status = TASK_STATUS_INCOMPLETE,
                    Subject = 'Test Inbound Call',
                    WhatId = a.Id
                ));
        }
        toInsert.addAll((List<SObject>) intakeTasks);
        toInsert.addAll((List<SObject>) accountTasks);
        toInsert.addAll((List<SObject>) inboundCallTasks);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Task> getTasksRelatedToAccounts() {
        return [
            SELECT
                Id,
                CallType,
                Status,
                Subject,
                WhatId
            FROM
                Task
            WHERE
                WhatId IN (SELECT Id FROM Account)
                AND CallType = :TASK_CALLTYPE_OUTBOUND
        ];
    }

    private static List<Task> getTasksRelatedToIntakes() {
        return [
            SELECT
                Id,
                CallType,
                Status,
                Subject,
                WhatId
            FROM
                Task
            WHERE
                WhatId IN (SELECT Id FROM Intake__c)
                AND CallType = :TASK_CALLTYPE_OUTBOUND
        ];
    }

    private static List<Task> getInboundTasks() {
        return [
            SELECT
                Id,
                CallType,
                Status,
                Subject,
                WhatId
            FROM
                Task
            WHERE
                CallType = :TASK_CALLTYPE_INBOUND
        ];
    }

    private static Map<Id, Account> getAllAccounts() {
        return new Map<Id, Account>([
                SELECT
                    Id,
                    OB_Dials__c,
                    Last_Call_Date_Time__c,
                    (SELECT Id FROM Intake_Surveys__r)
                FROM
                    Account
            ]);
    }

    private static Map<Id, Intake__c> getAllIntakes() {
        return new Map<Id, Intake__c>([
                SELECT
                    Id,
                    Number_Outbound_Dials_Intake__c,
                    Number_Outbound_Dials_Status__c,
                    Last_Dial_Time__c
                FROM
                    Intake__c
            ]);
    }

    @isTest
    private static void inboundCallTypesCompleted_Insert() {
        List<Task> inboundTasks = getInboundTasks();
        inboundTasks = inboundTasks.deepClone(false);
        for (Task t : inboundTasks) {
            t.Status = TASK_STATUS_COMPLETED;
        }

        Map<Id, Account> accounts = getAllAccounts();
        Map<Id, Intake__c> intakes = getAllIntakes();

        Datetime currentTimePriorToTest = Datetime.now();

        Test.startTest();
        Database.insert(inboundTasks);
        Test.stopTest();

        Map<Id, Account> resultAccounts = getAllAccounts();
        Map<Id, Intake__c> resultIntakes = getAllIntakes();

        System.assertEquals(accounts.keySet(), resultAccounts.keySet());
        for (Account a : accounts.values()) {
            Account resultAccount = resultAccounts.get(a.Id);

            System.assertEquals(a.OB_Dials__c, resultAccount.OB_Dials__c);
            assertDateTimeIsMoreRecent(currentTimePriorToTest, resultAccount);
        }

        System.assertEquals(intakes.keySet(), resultIntakes.keySet());
        for (Intake__c intake : intakes.values()) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            System.assertEquals(intake.Number_Outbound_Dials_Intake__c, resultIntake.Number_Outbound_Dials_Intake__c);
        }
    }

    @isTest
    private static void inboundCallTypesCompleted_Update() {
        List<Task> inboundTasks = getInboundTasks();
        for (Task t : inboundTasks) {
            t.Status = TASK_STATUS_COMPLETED;
        }

        Map<Id, Account> accounts = getAllAccounts();
        Map<Id, Intake__c> intakes = getAllIntakes();

        Datetime currentTimePriorToTest = Datetime.now();

        Test.startTest();
        Database.update(inboundTasks);
        Test.stopTest();

        Map<Id, Account> resultAccounts = getAllAccounts();
        Map<Id, Intake__c> resultIntakes = getAllIntakes();

        System.assertEquals(accounts.keySet(), resultAccounts.keySet());
        for (Account a : accounts.values()) {
            Account resultAccount = resultAccounts.get(a.Id);

            System.assertEquals(a.OB_Dials__c, resultAccount.OB_Dials__c);
            assertDateTimeIsMoreRecent(currentTimePriorToTest, resultAccount);
        }

        System.assertEquals(intakes.keySet(), resultIntakes.keySet());
        for (Intake__c intake : intakes.values()) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            System.assertEquals(intake.Number_Outbound_Dials_Intake__c, resultIntake.Number_Outbound_Dials_Intake__c);
        }
    }

    @isTest
    private static void incompleteTasksIgnored_Insert() {
        List<Task> outboundTasks = getTasksRelatedToAccounts().deepClone(false);
        outboundTasks.addAll(getTasksRelatedToIntakes().deepClone(false));
        for (Task t : outboundTasks) {
            System.assertEquals(TASK_STATUS_INCOMPLETE, t.Status);
        }

        Map<Id, Account> accounts = getAllAccounts();
        Map<Id, Intake__c> intakes = getAllIntakes();

        Datetime currentTimePriorToTest = Datetime.now();

        Test.startTest();
        Database.insert(outboundTasks);
        Test.stopTest();

        Map<Id, Account> resultAccounts = getAllAccounts();
        Map<Id, Intake__c> resultIntakes = getAllIntakes();

        System.assertEquals(accounts.keySet(), resultAccounts.keySet());
        for (Account a : accounts.values()) {
            Account resultAccount = resultAccounts.get(a.Id);

            System.assertEquals(a.OB_Dials__c, resultAccount.OB_Dials__c);
        }

        System.assertEquals(intakes.keySet(), resultIntakes.keySet());
        for (Intake__c intake : intakes.values()) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            System.assertEquals(intake.Number_Outbound_Dials_Intake__c, resultIntake.Number_Outbound_Dials_Intake__c);
        }
    }

    @isTest
    private static void incompleteTasksIgnored_Update() {
        List<Task> outboundTasks = getTasksRelatedToAccounts();
        outboundTasks.addAll(getTasksRelatedToIntakes());
        for (Task t : outboundTasks) {
            System.assertEquals(TASK_STATUS_INCOMPLETE, t.Status);
        }

        Map<Id, Account> accounts = getAllAccounts();
        Map<Id, Intake__c> intakes = getAllIntakes();

        Datetime currentTimePriorToTest = Datetime.now();

        Test.startTest();
        Database.update(outboundTasks);
        Test.stopTest();

        Map<Id, Account> resultAccounts = getAllAccounts();
        Map<Id, Intake__c> resultIntakes = getAllIntakes();

        System.assertEquals(accounts.keySet(), resultAccounts.keySet());
        for (Account a : accounts.values()) {
            Account resultAccount = resultAccounts.get(a.Id);

            System.assertEquals(a.OB_Dials__c, resultAccount.OB_Dials__c);
        }

        System.assertEquals(intakes.keySet(), resultIntakes.keySet());
        for (Intake__c intake : intakes.values()) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            System.assertEquals(intake.Number_Outbound_Dials_Intake__c, resultIntake.Number_Outbound_Dials_Intake__c);
        }
    }

    @isTest
    private static void outboundCallsCompleted_Insert() {
        List<Task> outboundTasks = getTasksRelatedToAccounts().deepClone(false);
        outboundTasks.addAll(getTasksRelatedToIntakes().deepClone(false));
        for (Task t : outboundTasks) {
            System.assertEquals(TASK_STATUS_INCOMPLETE, t.Status);
            t.Status = TASK_STATUS_COMPLETED;
        }

        Map<Id, Account> accounts = getAllAccounts();
        Map<Id, Intake__c> intakes = getAllIntakes();

        Datetime currentTimePriorToTest = Datetime.now();

        Test.startTest();
        Database.insert(outboundTasks);
        Test.stopTest();

        Map<Id, Account> resultAccounts = getAllAccounts();
        Map<Id, Intake__c> resultIntakes = getAllIntakes();

        System.assertEquals(accounts.keySet(), resultAccounts.keySet());
        for (Account a : accounts.values()) {
            Account resultAccount = resultAccounts.get(a.Id);

            assertNumberOfDialsHasIncreased(a, resultAccount);
            assertDateTimeIsMoreRecent(currentTimePriorToTest, resultAccount);
            if (a.Intake_Surveys__r.isEmpty()) {
                System.assertEquals(BULK_SIZE, resultAccount.OB_Dials__c);
            }
            else {
                System.assertEquals(a.Intake_Surveys__r.size() * BULK_SIZE, resultAccount.OB_Dials__c);
            }
        }

        System.assertEquals(intakes.keySet(), resultIntakes.keySet());

        for (Intake__c intake : intakes.values()) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            assertNumberOfDialsHasIncreased(intake, resultIntake);
            System.assertEquals(BULK_SIZE, resultIntake.Number_Outbound_Dials_Intake__c, 'Number of Dials is incorrect.\n' + String.valueOf(resultIntake));
            assertDateTimeIsMoreRecent(currentTimePriorToTest, resultIntake);
        }
    }

    @isTest
    private static void outboundCallsCompleted_Update() {
        List<Task> outboundTasks = getTasksRelatedToAccounts();
        outboundTasks.addAll(getTasksRelatedToIntakes());
        for (Task t : outboundTasks) {
            System.assertEquals(TASK_STATUS_INCOMPLETE, t.Status);
            t.Status = TASK_STATUS_COMPLETED;
        }

        Map<Id, Account> accounts = getAllAccounts();
        Map<Id, Intake__c> intakes = getAllIntakes();

        Datetime currentTimePriorToTest = Datetime.now();

        Test.startTest();
        Database.update(outboundTasks);
        Test.stopTest();

        Map<Id, Account> resultAccounts = getAllAccounts();
        Map<Id, Intake__c> resultIntakes = getAllIntakes();

        System.assertEquals(accounts.keySet(), resultAccounts.keySet());
        for (Account a : accounts.values()) {
            Account resultAccount = resultAccounts.get(a.Id);

            assertNumberOfDialsHasIncreased(a, resultAccount);
            assertDateTimeIsMoreRecent(currentTimePriorToTest, resultAccount);
            if (a.Intake_Surveys__r.isEmpty()) {
                System.assertEquals(BULK_SIZE, resultAccount.OB_Dials__c);
            }
            else {
                System.assertEquals(a.Intake_Surveys__r.size() * BULK_SIZE, resultAccount.OB_Dials__c);
            }
        }

        System.assertEquals(intakes.keySet(), resultIntakes.keySet());
        for (Intake__c intake : intakes.values()) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            assertNumberOfDialsHasIncreased(intake, resultIntake);
            System.assertEquals(BULK_SIZE, resultIntake.Number_Outbound_Dials_Intake__c, 'Number of Dials is incorrect.\n' + String.valueOf(resultIntake));
            assertDateTimeIsMoreRecent(currentTimePriorToTest, resultIntake);
        }
    }

    private static void assertNumberOfDialsHasIncreased(Account before, Account after)
    {
        if (after.OB_Dials__c < before.OB_Dials__c) {
            String msg = 'expectedValue: ' + before.OB_Dials__c + '\n' +
                'actualValue: ' + after.OB_Dials__c + '\n' +
                'Account:\n' + after;

            System.assert(false, msg);
        }
    }

    private static void assertNumberOfDialsHasIncreased(Intake__c before, Intake__c after)
    {
        if (after.Number_Outbound_Dials_Intake__c < before.Number_Outbound_Dials_Intake__c) {
            String msg = 'expectedValue: ' + before.Number_Outbound_Dials_Intake__c + '\n' +
                'actualValue: ' + after.Number_Outbound_Dials_Intake__c + '\n' +
                'Intake:\n' + after;

            System.assert(false, msg);
        }
    }

    private static void assertDateTimeIsMoreRecent(DateTime expectedValue, Account acct)
    {
        if (timeDifferenceInSeconds(expectedValue, acct.Last_Call_Date_Time__c) < 2) {
            String msg = 'expectedValue: ' + expectedValue.getTime() + '\n' +
                'actualValue: ' + acct.Last_Call_Date_Time__c.getTime() + '\n' +
                'Account:\n' + acct;

            System.assert(false, msg);
        }
    }

    private static void assertDateTimeIsMoreRecent(DateTime expectedValue, Intake__c intake)
    {
        if (timeDifferenceInSeconds(expectedValue, intake.Last_Dial_Time__c) < 2) {
            String msg = 'expectedValue: ' + expectedValue.getTime() + '\n' +
                'actualValue: ' + intake.Last_Dial_Time__c.getTime() + '\n' +
                'Intake:\n' + intake;

            System.assert(false, msg);
        }
    }

    private static Decimal timeDifferenceInSeconds(DateTime alpha, DateTime beta)
    {
        return Math.abs(alpha.getTime() - beta.getTime());
    }
}