/**
 *  mmmarketing_ExperimentSetupScheduleable
 */
public class mmmarketing_ExperimentSetupScheduleable
    implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        Type targettype = Type.forName('mmmarketing_ExperimentSetupBatchable');

        if ( targettype != null )
        {
            mmlib_ISchedule obj = (mmlib_ISchedule)targettype.newInstance();
            obj.execute(sc);
        }
        else
        {
            system.debug( 'mmlib_ISchedule implementation of mmmarketing_ExperimentSetupBatchable not found');
        }
    }

    public static String setupSchedule( String scheduleJobName, String crontabSchedule )
    {
        if ( string.isblank(crontabSchedule) )
        {
            crontabSchedule = '0 15 * * * ?';
        }

        if ( string.isblank(scheduleJobName) )
        {
            scheduleJobName = mmmarketing_ExperimentSetupScheduleable.class.getName() + ' scheduled job';
        }

        String jobID = system.schedule( scheduleJobName, crontabSchedule, new mmmarketing_ExperimentSetupScheduleable() );

        system.debug( 'the jobID == '+ jobID);

        return jobID;
    }

    public static void setupScheduleEvery6Hours()
    {
        String scheduleJobString = mmmarketing_ExperimentSetupScheduleable.class.getName() + ' scheduled job';

        system.debug( 'the jobID == '+ setupSchedule( scheduleJobString + ' - every 8 hours', '0 0 0,8,16 * * ?' ));
    }
}