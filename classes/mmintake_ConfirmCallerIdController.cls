/**
 *  mmintake_ConfirmCallerIdController
 */
public with sharing class mmintake_ConfirmCallerIdController
{
    private static final string YES = 'Yes';
    private static final string NO = 'No';

    public static final string PARAM_ID = 'id';
    public static final string PARAM_CALLSTARTTIME = 'cst';

    private id intakeId = null;
    private datetime callStartTime = null;

    public Intake__c intake { get; private set; } { intake = new Intake__c(); }

    public Account client { get; private set; } { client = new Account(); }

    public list<SelectOption> accountPhoneNumberOptionList { get; private set; } { accountPhoneNumberOptionList = new list<SelectOption>(); }

    public string selectedPhoneNumber { get; set; }

    public string alternatePhoneNumber { get; set; }

    public string isNewIntakeCreatedFromInboundCall { get; set; } { isNewIntakeCreatedFromInboundCall = Yes; }

    public list<SelectOption> yesNoOptions
    {
        get
        {
            list<SelectOption> options = new list<SelectOption>();

            options.add( new SelectOption(Yes,Yes) );
            options.add( new SelectOption(No,No) );

            return options;
        }
    }

    public mmintake_ConfirmCallerIdController()
    {
        processPageParmeters();

        loadIntake();

        loadRelatedClient();

        findAvailablePhoneNumbers();
    }

    private void findAvailablePhoneNumbers()
    {
        map<string, Object> populatedFieldMap = client.getPopulatedFieldsAsMap();

        map<string, Schema.SobjectField> accountFieldMap = Account.SObjectType.getDescribe().fields.getMap();

        Schema.DescribeFieldResult describeField = null;

        SelectOption option = null;

        for ( string populatedFieldName : populatedFieldMap.keyset() )
        {
            describeField = accountFieldMap.get( populatedFieldName ).getDescribe();

            //system.debug( '------------------------------------------------------------' );
            //system.debug( describeField.getName() );
            //system.debug( describeField.getType() );
            //system.debug( client.get( populatedFieldName ) );

            if ( Schema.DisplayType.Phone == describeField.getType()
                && client.get( populatedFieldName ) != null
                )
            {
                option = new SelectOption( (string)client.get( populatedFieldName )                                     // value
                                         , describeField.getLabel() + ' : ' + (string)client.get( populatedFieldName )  // label
                                         );

                accountPhoneNumberOptionList.add( option );
            }
        }
    }

    private void loadIntake()
    {
        if ( intakeId != null )
        {
            list<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectById( new set<id>{ intakeId } );

            if ( ! intakes.isEmpty() )
            {
                intake = intakes[0];
            }
        }
    }

    private void loadRelatedClient()
    {
        if ( intake != null
            && intake.client__c != null
            )
        {
            list<Account> accounts = mmcommon_AccountsSelector.newInstance().selectById( new set<id>{ intake.client__c } );

            if ( ! accounts.isEmpty() )
            {
                client = accounts[0];
            }
        }
    }

    private void processPageParmeters()
    {
        string callStartTime_string = ApexPages.currentPage().getParameters().get(PARAM_CALLSTARTTIME);

        system.debug( callStartTime_string );

        if (string.isNotBlank(callStartTime_string))
        {
            try
            {
                // yyyyMMddHHmmss
                integer yearValue = integer.valueOf(callStartTime_string.left(4));
                integer monthValue = integer.valueOf(callStartTime_string.mid(4,2));
                integer dateValue = integer.valueOf(callStartTime_string.mid(6,2));
                integer hourValue = integer.valueOf(callStartTime_string.mid(8,2));
                integer minuteValue = integer.valueOf(callStartTime_string.mid(10,2));
                integer secondValue = integer.valueOf(callStartTime_string.mid(12,2));

                //system.debug( 'yearValue = ' + yearValue );
                //system.debug( 'monthValue = ' + monthValue );
                //system.debug( 'dateValue = ' + dateValue );
                //system.debug( 'hourValue = ' + hourValue );
                //system.debug( 'minuteValue = ' + minuteValue );
                //system.debug( 'secondValue = ' + secondValue );

                callStartTime = datetime.newInstanceGmt(yearValue, monthValue, dateValue, hourValue, minuteValue, secondValue);

                //system.debug( callStartTime );
            }
            catch (Exception e)
            {
                system.debug( e );
            }
        }

        string intakeId_string = ApexPages.currentPage().getParameters().get(PARAM_ID);

        system.debug( intakeId_string );

        if ( string.isNotBlank( intakeId_string ))
        {
            try
            {
                intakeId = id.valueOf( intakeId_string );
            }
            catch (System.StringException se)
            {
                system.debug( se );
            }
        }
    }

    public PageReference saveCallerId()
    {
//        if ( string.isBlank( alternatePhoneNumber )
//            && string.isBlank( selectedPhoneNumber ) )
//        {
//            // nothing was selected
//            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a phone number or add one.'));
//        }
//        else if (  ) )
//        {
//
//        }
//

        PageReference nextPage = null;

        system.debug( isNewIntakeCreatedFromInboundCall );
        system.debug( selectedPhoneNumber );
        system.debug( alternatePhoneNumber );

        if ( string.isBlank( selectedPhoneNumber )
            && string.isBlank( alternatePhoneNumber )
            && Yes.equalsIgnoreCase( isNewIntakeCreatedFromInboundCall )
            )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a phone number or add one.'));
        }
        else
        {
            nextPage = mmintake_PageReferenceUtils.getIntakeFlow( intake );


            if ( Yes.equalsIgnoreCase( isNewIntakeCreatedFromInboundCall ) )
            {
                //which phone number should be used?
                string phoneNumberChosen = null;

                try
                {
                    if ( string.isNotBlank( alternatePhoneNumber ) )
                    {
                        phoneNumberChosen = mmlib_Utils.stripPhoneNumber( alternatePhoneNumber );
                    }
                    else
                    {
                        phoneNumberChosen = mmlib_Utils.stripPhoneNumber( selectedPhoneNumber );
                    }

                    system.debug( 'phoneNumberChosen == ' + phoneNumberChosen);
                    system.debug( 'callStartTime == ' + callStartTime);
                    system.debug( 'intakeId == ' + intakeId);

                    mmmarketing_ITrackInfoRecordCallRequest recordCallRequest = mmmarketing_TrackInfoRecordCallRequest.newInstance()
                                                                                                                      .setCallingNumber( phoneNumberChosen )
                                                                                                                      .setCallStartTime( callStartTime )
                                                                                                                      .setRelatedIntakeId( intakeId );

                    mmmarketing_TrackingInfosService.recordCallsForIntakes( new list<mmmarketing_ITrackInfoRecordCallRequest>{ recordCallRequest } );
                }
                catch (Exception e)
                {
                    system.debug(e);
                }
            }
        }

        return nextPage;
    }
}