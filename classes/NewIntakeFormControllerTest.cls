@isTest
public class NewIntakeFormControllerTest {

   
    @IsTest
    private static void createIntake() {
        
        NewIntakeFormController intakeController = new NewIntakeFormController();
        List<Cisco_Queue_Setting__mdt> ciscoSettings = [SELECT GeneratingAttorney__c,Cisco_Queue__c,Handling_Firm__c,Marketing_Source__c FROM Cisco_Queue_Setting__mdt];
        NewIntakeFormController.intakeDetails(ciscoSettings[0].Cisco_Queue__c);

        Map<String, String> webIdToSObjectFieldMap = new Map<String,String>();


        webIdToSObjectFieldMap.put( 'litigationSelect','Criminal' );
        webIdToSObjectFieldMap.put( 'handlingFirmSelect', 'Morgan & Morgan' );
        webIdToSObjectFieldMap.put( 'marketingSourceSelect', '# LAW' );
        webIdToSObjectFieldMap.put( 'optOutSelect', 'true');
        webIdToSObjectFieldMap.put( 'languageSelect', 'English');
        webIdToSObjectFieldMap.put( 'caseDetails', 'For testing');
        webIdToSObjectFieldMap.put( 'textarea-generating-source-details', '');
        webIdToSObjectFieldMap.put( 'emailAddress','test@gmail.com' );
        webIdToSObjectFieldMap.put( 'clientFirstName', 'Test' );
        webIdToSObjectFieldMap.put( 'clientLastName', 'Testing' );
        webIdToSObjectFieldMap.put( 'clientPhone', '232342');
        webIdToSObjectFieldMap.put( 'clientMobile','321432341');

        NewIntakeFormController.saveIntake(webIdToSObjectFieldMap);
         NewIntakeFormController.saveIntake(null);
    }

}