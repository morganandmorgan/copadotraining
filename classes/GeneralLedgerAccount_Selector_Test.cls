/**
 * GeneralLedgerAccount_Selector_Test
 * @description Test for GLA Selector
 * @author CLD Partners
 * @date 5/5/2019
 */
@IsTest
public with sharing class GeneralLedgerAccount_Selector_Test {

    static {
        c2g__codaGeneralLedgerAccount__c gla = TestDataFactory_FFA.balanceSheetGLAs[0];

    }

    @IsTest
    private static void ctor() {
        GeneralLedgerAccount_Selector glaSelector = new GeneralLedgerAccount_Selector();
        System.assert(glaSelector != null);
    }

    @IsTest
    private static void selectByIds() {
        GeneralLedgerAccount_Selector glaSelector = new GeneralLedgerAccount_Selector();
        Set<String> rptCodeSet = new Set<String>{'10000'};
        
        List<c2g__codaGeneralLedgerAccount__c> glaList = glaSelector.selectByReportingCodes(rptCodeSet);
    }
}