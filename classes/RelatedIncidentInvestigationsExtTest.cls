@isTest
private class RelatedIncidentInvestigationsExtTest {
	
	@TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        IncidentInvestigationEvent__c incidentInvestigation = new IncidentInvestigationEvent__c(
                Incident__c = incident.Id,
                EndDateTime__c = Datetime.now().addHours(1),
                StartDateTime__c = Datetime.now()
            );
        toInsert.add(incidentInvestigation);

        Database.insert(toInsert);
        toInsert.clear();

        Event calendarEvent = new Event(
                Subject = 'Test Subject',
                WhatId = incidentInvestigation.Id,
                StartDateTime = incidentInvestigation.StartDateTime__c,
                EndDateTime = incidentInvestigation.EndDateTime__c,
                Type = ScheduleInvestigatorService.EVENT_TYPE
            );
        toInsert.add(calendarEvent);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Incident__c getIncident() {
        return [SELECT Id FROM Incident__c];
    }

    @isTest
    private static void incidentInvestigationEventNotFound() {
        Incident__c incident = getIncident();

        List<IncidentInvestigationEvent__c> toDelete = [SELECT Id FROM IncidentInvestigationEvent__c];

        Database.delete(toDelete);

        Test.startTest();
        RelatedIncidentInvestigationsExt controller = new RelatedIncidentInvestigationsExt(new ApexPages.StandardController(incident));
        Test.stopTest();

        System.assert(!controller.HasEvents);
    }

    @isTest
    private static void incidentDoesNotHaveEvent() {
        Incident__c incident = getIncident();

        List<Event> toDelete = [SELECT Id FROM Event];

        Database.delete(toDelete);

        Test.startTest();
        RelatedIncidentInvestigationsExt controller = new RelatedIncidentInvestigationsExt(new ApexPages.StandardController(incident));
        Test.stopTest();

        System.assert(controller.HasEvents);
        System.assertEquals(toDelete.size(), controller.IncidentInvestigations.size());

        for (RelatedIncidentInvestigationsExt.InvestigationWrapper wrapper : controller.IncidentInvestigations) {
            System.assertNotEquals(null, wrapper.INCIE);
            System.assertEquals(null, wrapper.Event);
        }
    }

    @isTest
    private static void incidentHasEvent() {
        Incident__c incident = getIncident();

        Test.startTest();
        RelatedIncidentInvestigationsExt controller = new RelatedIncidentInvestigationsExt(new ApexPages.StandardController(incident));
        Test.stopTest();

        System.assert(controller.HasEvents);
    }
}