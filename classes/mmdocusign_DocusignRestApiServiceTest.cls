@isTest
private class mmdocusign_DocusignRestApiServiceTest
{
	@isTest
	static void createTask()
	{
		Intake__c intake = new Intake__c(
			Id = fflib_IDGenerator.generate(Intake__c.SObjectType),
			Client_Email_txt__c = 'bob@thebuilder.com',
			Client_First_Last_Name__c = 'Simply Bob',
			Handling_Firm__c = 'Morgan & Morgan'
		);

		mmdocusign_DocusignRestApiServiceImpl.mock_landingPageUrlTempl = 'http://hownowbrowncow.com/levelone/leveltwo/levelthree/levelfour/page.html';
		mmdocusign_DocusignRestApiServiceImpl.mock_shortenedLandingPageUrl = 'http://short.com/eivb90ur32j';

		String envelopeId = mmlib_Utils.generateGuid();
		String clientUserId = mmlib_Utils.generateGuid();

		Task t = mmdocusign_DocusignRestApiServiceImpl.createTasksForEnvelope(intake, envelopeId, clientUserId);

		PageReference pr = new PageReference(mmdocusign_DocusignRestApiServiceImpl.test_landingPageUrl);

		System.assertEquals(envelopeId, pr.getParameters().get('env'));
		System.assertEquals(intake.Client_Email_txt__c, pr.getParameters().get('email'));
		System.assertEquals(intake.Client_First_Last_Name__c, pr.getParameters().get('name'));
		System.assertEquals(clientUserId, pr.getParameters().get('cuid'));

		System.assert(t != null);
		System.assertEquals(mmdocusign_DocusignRestApiServiceImpl.mock_shortenedLandingPageUrl, t.PC_Signup_Url__c);
	}
}