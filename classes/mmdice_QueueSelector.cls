public with sharing class mmdice_QueueSelector
	extends mmlib_SObjectSelector
	implements mmdice_IQueueSelector
{
	public static mmdice_IQueueSelector newInstance()
	{
		return (mmdice_IQueueSelector) mm_Application.Selector.newInstance(DICE_Queue__c.SObjectType);
	}

	private Schema.sObjectType getSObjectType()
	{
		return DICE_Queue__c.SObjectType;
	}

	private List<Schema.SObjectField> getAdditionalSObjectFieldList()
	{
		return new List<Schema.SObjectField> { };
	}

	public List<DICE_Queue__c> selectById(Set<Id> idSet)
	{
		return (List<DICE_Queue__c>) selectSObjectsById(idSet);
	}

	public List<DICE_Queue__c> selectAll()
	{
        Boolean assertCRUD = false;
        Boolean enforceFLS = false;
        Boolean includeSelectorFields = true;

        return (List<DICE_Queue__c>) Database.query(newQueryFactory(assertCRUD, enforceFLS, includeSelectorFields).toSOQL());
	}
}