public class PaymentProcessingUpdateCheckNumbersBatch implements Database.Batchable<SObject>, Database.Stateful {

    //User selecting/setting new check range start...
    public PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper {get; set;}
    public Payment_Collection__c paymentCollection {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    public Id paymentCollectionId {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    public Id bankAccountID {get; set;}

    protected Set<Id> paymentIds=null;   //Using?

    protected Integer initialNextCheckNumber{get; set;} //The next check number available in the range for the bank account
    protected Integer lastCheckNumber{get; set;} //The last specified incoming check number.  We'll use up this whole range of numbers.
    protected Integer currentCheckNumber{get; set;} //You guessed it.
    protected Integer currentExecutionNumber{get; set;} //1-based
        
    protected List<Integer> checkNumbers{get; set;} //The iterator, in order.  The entire input, across batch scopes.
    protected public Map<Integer, Integer> usedCheckNumbers {get; set;}   //The actual real non-voided checks.  The rest in the range will be voided.
    protected public Map<Integer, PaymentProcessing.PaymentSummaryWrapper> paymentSummariesByCheckNumber {get; set;}  //Container for Account Id and check number
    protected public Map<Id, List<PaymentProcessing.PaymentSummaryWrapper>> paymentSummariesByPaymentId {get; set;}  //

    protected List<Payment_Processing_Batch_Status__c> paymentStatuses = null;
    //Currently not used:  
    Map<Id, Payment_Processing_Batch_Status__c> paymentStatusesById = null;
    Map<Id, Payment_Processing_Batch_Status__c> paymentStatusesByPaymentId = null;    

    //protected PaymentProcessingDataAccess dataAccess=null; 

    public PaymentProcessingUpdateCheckNumbersBatch(PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper){
        this.paymentCollectionWrapper=paymentCollectionWrapper;
        if(paymentCollectionWrapper!=null){
            this.paymentCollection=paymentCollectionWrapper.paymentCollection;
            if(paymentCollectionWrapper.paymentCollection!=null){
                this.paymentCollectionId=paymentCollectionWrapper.paymentCollection.Id;
            }
        }
        //dataAccess=new PaymentProcessingDataAccess();
    }    

    public Database.QueryLocator start(Database.BatchableContext batchContext) {

        if(paymentCollectionWrapper!=null){
            if(paymentCollectionWrapper.paymentCollection.Payment_Method__c=='Check'){
                //This probably isn't all needed:  just get the initial next check number for the bank account.
                calculateCheckNumbers();
            }else{
                System.debug('Payment method/media is not Check.  Skipping check number update.');
            }
        }else{
            System.debug('No paymentCollectionWrapper.  Cannot update payment check numbers.');
            //TODO:  Pre-Calculate based on payment summaries.  
        }

        String soql = null;
        if(paymentCollectionId!=null){
            soql = 
            'select  ' + 
            'Id ' + 
            ',Name  ' + 
            ',Payment_Collection__c ' + 
            ',c2g__BankAccount__c ' + 
            ',c2g__Status__c ' + 
            'from c2g__codaPayment__c  ' + 
            'where  ' + 
            'c2g__Status__c = \'Media Prepared\' ' + 
            'and Payment_Collection__c = \'' + paymentCollectionId + '\' ' + 
            'order by Id '
            ;
        }

        //return checkNumbers;
        return Database.getQueryLocator(soql);
    }

    private void calculateCheckNumbers(){
        System.debug('calculateCheckNumbers() - initializing...');

        //If the order of the payments in this object graphy are different than the order of the payments returned in the query locator, there will be check number order errors.

        currentExecutionNumber= 0;

        if(this.bankAccountID==null){
            this.bankAccountID=paymentCollection.Bank_Account__c;
        }       

        if(paymentIds==null){
            paymentIds=new Set<Id>();
        }

        //initialization routine:  Incoming user-specified check numbers.  Possibly with gaps that have to be filled in.
        if((paymentCollectionWrapper!=null)&&(paymentCollectionWrapper.payments!=null)){

            //Apply the custom sort order by Payment Id: 
            paymentCollectionWrapper.payments.sort();
            
            lastCheckNumber=0;

            PaymentProcessingDataAccess payData=null;
            payData=new PaymentProcessingDataAccess();

            checkNumbers=new List<Integer>();
            usedCheckNumbers=new Map<Integer, Integer>();
            paymentSummariesByCheckNumber = new Map<Integer, PaymentProcessing.PaymentSummaryWrapper>(); 
            paymentSummariesByPaymentId = new Map<Id, List<PaymentProcessing.PaymentSummaryWrapper>>(); 

            initialNextCheckNumber = getNextCheckNumber(paymentCollectionWrapper.paymentCollection.Bank_Account__c);
            this.currentCheckNumber = initialNextCheckNumber;

            System.debug('Bank_Account__c:  ' + paymentCollectionWrapper.paymentCollection.Bank_Account__c);
            System.debug('initialNextCheckNumber:  ' + initialNextCheckNumber);

            Integer paymentSummaryCount=0;

            for(PaymentProcessing.PaymentWrapper paymentWrapper: paymentCollectionWrapper.payments){
                if(paymentWrapper.paymentSummaryWrappers!=null){
                    paymentSummariesByPaymentId.put(paymentWrapper.payment.Id, paymentWrapper.paymentSummaryWrappers);
                    for(PaymentProcessing.PaymentSummaryWrapper paymentSummaryWrapper: paymentWrapper.paymentSummaryWrappers){
                        paymentSummaryCount++;                        
                        if(paymentSummaryWrapper.checkNumber!=null){
                            Integer checkNumber = null;
                            checkNumber= integer.valueof(paymentSummaryWrapper.checkNumber);
                            usedCheckNumbers.put(checkNumber, checkNumber);
                            paymentSummariesByCheckNumber.put(checkNumber, paymentSummaryWrapper);
                            
                            if(checkNumber>lastCheckNumber){
                                lastCheckNumber = checkNumber;
                            }
                        }
                    }
                }
            }

            System.debug('paymentSummaryCount (check count):  ' + paymentSummaryCount);

            if(lastCheckNumber==0){
                System.debug('No incoming check numbers.  Using check numbers sequentially...');
                //No incoming checknumbers were not provided.
                //TODO:  Consider noting in class state that incoming checknumbers were not provided.
                lastCheckNumber=(initialNextCheckNumber + (paymentSummaryCount-1));
                
                //Retro-fit:  fill in the check numbers:  
                Integer checkNumber = initialNextCheckNumber;
                for(PaymentProcessing.PaymentWrapper paymentWrapper: paymentCollectionWrapper.payments){
                    System.debug('Payment Name:  ' + paymentWrapper.payment.Name);
                    if(paymentWrapper.paymentSummaryWrappers!=null){
                        for(PaymentProcessing.PaymentSummaryWrapper paymentSummaryWrapper: paymentWrapper.paymentSummaryWrappers){                            
                            if(paymentSummaryWrapper.checkNumber==null){    //This is true - all or nothing
                                usedCheckNumbers.put(checkNumber, checkNumber);
                                paymentSummariesByCheckNumber.put(checkNumber, paymentSummaryWrapper);
                                paymentSummaryWrapper.checkNumber=String.valueOf(checkNumber);
                                paymentIds.add(paymentSummaryWrapper.paymentId);
                                checkNumber++;
                            }
                        }
                    }
                }
                
            }


            //Save the numbers to iterate through:  
            for(Integer i=initialNextCheckNumber;i<=lastCheckNumber;i++){
                checkNumbers.add(i);
            }
            
            System.debug('lastCheckNumber:  ' + lastCheckNumber);
            System.debug('checks count:  ' + checkNumbers.size());

            {
                System.debug('getting paymentStatuses...');

                //TODO:  Factor this out into a method:  
                paymentStatuses = 
                [
                select   
                Id   
                ,Name   
                
                ,Message__c   
                ,Last_Error_Message__c   
                ,Has_Error__c               
                
                ,Payment_Collection__c   
                ,Payment_Collection__r.Name 
                ,Payment_Collection__r.Bank_Account__c 
                ,Payment_Collection__r.Bank_Account__r.Name 
                ,Payment_Collection__r.Payment_Method__c

                ,Payment__c   
                ,Payment__r.c2g__Status__c   
                ,Payment__r.Name   
                
                ,Are_Proposal_Lines_Added__c   
                ,Is_Media_Data_Created__c   
                ,Are_Check_Numbers_Updated__c   

                ,Update_Check_Numbers_Last_Error_Message__c
                ,Update_Check_Numbers_Job_Id__c
                ,Has_Update_Check_Numbers_Error__c
                
                from Payment_Processing_Batch_Status__c   
                where   
                Payment_Collection__r.Payment_Method__c = 'Check'   
                and Payment__r.c2g__Status__c  = 'Media Prepared'  
                and Are_Proposal_Lines_Added__c = true   
                and Is_Media_Data_Created__c = true   
                and Are_Check_Numbers_Updated__c = false   
                and Payment_Collection__c = :paymentCollectionId 
                and Payment__c in :paymentIds 
                ]     
                ;
            }

            //Currently not used:  

            paymentStatusesById=new Map<Id, Payment_Processing_Batch_Status__c>(paymentStatuses);
            
            paymentStatusesByPaymentId=new Map<Id, Payment_Processing_Batch_Status__c>();

            for(Payment_Processing_Batch_Status__c paymentStatus: paymentStatuses){
                paymentStatusesByPaymentId.put(paymentStatus.Payment__c, paymentStatus);
            }            

        }else{
            System.debug('No payments.');
        }

    }

    public void execute(Database.BatchableContext batchContext, List<c2g__codaPayment__c> scope){
        // process each batch of records
        System.debug('PaymentProcessingUpdateCheckNumbersBatch execute() - ' + scope.size() + ' records.');

        execute_ConsumeCheckRange_Implementation(batchContext, scope);

    }

    public void execute_ConsumeCheckRange_Implementation(Database.BatchableContext batchContext, List<c2g__codaPayment__c> scope){
        // process each batch of records
        currentExecutionNumber++;
        System.debug('execute_ConsumeCheckRange_Implementation execute(' + currentExecutionNumber + ') - ' + scope.size() + ' records.');

        // if(scope.size()>1){
        //     System.debug('  ' + scope[0] + ' to ' + scope[scope.size()-1] );
        // }

        System.debug('currentExecutionNumber:  ' + this.currentExecutionNumber);
        System.debug('this.bankAccountID:  ' + this.bankAccountID);
        System.debug('paymentCollection.Bank_Account__c:  ' + paymentCollection.Bank_Account__c);
        System.debug('  initial initialNextCheckNumber:  ' + initialNextCheckNumber);
        System.debug('      initial currentCheckNumber:  ' + this.currentCheckNumber);
        System.debug('lastCheckNumber in range:  ' + lastCheckNumber);
        System.debug('checks count:  ' + checkNumbers.size());        

        if(this.bankAccountID==null){
            this.bankAccountID=paymentCollection.Bank_Account__c;
        }

        //Exclude electronic if called with an electronic payment collection
        
        //NOTE:  Sets of SObjects don't work for uniqueness or contains():
        // Set<Payment_Processing_Batch_Status__c> paymentStatusesToUpdate=null;
        // paymentStatusesToUpdate=new Set<Payment_Processing_Batch_Status__c>();

        System.debug('Beginning check creation Payment loop...');

        for(c2g__codaPayment__c payment: scope){

            Id paymentId = null;
            paymentId=payment.Id;
                
            System.debug('Payment:  ' + paymentId);

            //The current batch of checks to submit for the current/next payment:  
            List<c2g.PaymentsPlusService.Check> checks = null;
            checks = new List<c2g.PaymentsPlusService.Check>();

            Map<Id, List<c2g.PaymentsPlusService.Check>> checksByPaymentId = null;
            checksByPaymentId = new Map<Id, List<c2g.PaymentsPlusService.Check>>();

            Integer lastCheckThisRound=0;

            List<PaymentProcessing.PaymentSummaryWrapper> currentPaymentSummaryWrappers = null;
            currentPaymentSummaryWrappers=paymentSummariesByPaymentId.get(paymentId);
            //TODO:  check for null payment wrapper?

            lastCheckThisRound=(currentCheckNumber + currentPaymentSummaryWrappers.size() - 1);

            System.debug('Calculating check numbers range for this payment...');

            Integer maxCheckNumberForThisPayment = 0;
            if(currentPaymentSummaryWrappers!=null){
                System.debug('currentPaymentSummaryWrappers:  ' + currentPaymentSummaryWrappers.size());
                for(PaymentProcessing.PaymentSummaryWrapper paymentSummaryWrapper: currentPaymentSummaryWrappers){                            
                    if(paymentSummaryWrapper.checkNumber!=null){    //This is true - all or nothing
                        Integer summaryCheckNumber = 0;
                        summaryCheckNumber = Integer.valueOf(paymentSummaryWrapper.checkNumber);
                        System.debug('paymentSummaryWrapper.checkNumber:  ' + paymentSummaryWrapper.checkNumber);
                        if(summaryCheckNumber>maxCheckNumberForThisPayment){
                            maxCheckNumberForThisPayment=summaryCheckNumber;
                        }                        
                    }
                }
            }else{
                System.debug('Error:  null currentPaymentSummaryWrappers' );
            }
            
            System.debug('maxCheckNumberForThisPayment:  ' + maxCheckNumberForThisPayment);
            lastCheckThisRound=maxCheckNumberForThisPayment;

            System.debug('  Beginning check creation check number inner loop...');

            // for(Integer currentCheckNumber:scope){
            for( ;currentCheckNumber<=lastCheckThisRound;currentCheckNumber++){

                //Early iterations of this loop could be for voided checks.

                System.debug('currentCheckNumber:  ' + currentCheckNumber);
                
                PaymentProcessing.PaymentSummaryWrapper paymentSummaryWrapper=null;
                paymentSummaryWrapper=paymentSummariesByCheckNumber.get(currentCheckNumber);

                Boolean checkIsCancelled=false;

                if(paymentSummaryWrapper==null){

                    //Voided Check: 
                    checkIsCancelled=true;

                    //Add a cancelled/voided check, which will have to be updated with AccountId later when we encounter a real payment-line-check:  

                    System.debug('Voided Check:  ' + currentCheckNumber);
                    
                    c2g.PaymentsPlusService.Check cancelledCheck = null;
                    cancelledCheck = new c2g.PaymentsPlusService.Check();

                    //cancelledCheck.AccountId = paymentAccountId;	//Pay-to vendor account
                    //Error:  Void or manual checks cannot include an account ID.	
                    cancelledCheck.CheckNumber = currentCheckNumber;
                    
                    cancelledCheck.Status = c2g.PaymentsPlusService.CheckStatus.StatusVoid;
                    
                    checks.add(cancelledCheck);                

                }else if(paymentSummaryWrapper!=null){
                    
                    //real check:  
                
                    Id paymentAccountId = null;
                    paymentAccountId=paymentSummaryWrapper.accountId;

                    {

                        System.debug('Payment:  ' + paymentId + ', paymentMediaSummary:  ' + paymentSummaryWrapper.paymentMediaSummary.Id + ', Account:  ' + paymentAccountId + ', valid check number:  ' + currentCheckNumber);
                        System.debug('Valid Check:  ' + currentCheckNumber);
                        c2g.PaymentsPlusService.Check check = null;
                        check = new c2g.PaymentsPlusService.Check();
                        //check.AccountId = paymentStatus.Payment_Collection__r.Bank_Account__c;	//TODO:  Account or Bank Account???
                        //Account a5v1J000000TNGyQAO cannot be paid. It is not part of the current payment proposal.
                        check.AccountId = paymentAccountId;	//Pay-to vendor account
                        check.CheckNumber = currentCheckNumber;                            
                        check.Status = c2g.PaymentsPlusService.CheckStatus.StatusValid;
                        
                        checks.add(check);
                                                
                    }

                }

            }

            //Anything in the buffer?  
            if((checks!=null)&&(checks.size()>0)){

                System.debug('Running updatePaymentCheckNumbers after loop...');
                
                Payment_Processing_Batch_Status__c paymentStatus=null;
                paymentStatus = paymentStatusesByPaymentId.get(paymentId);

                updatePaymentCheckNumbers(paymentId, checks, paymentStatus, batchContext);

            }

            checks=null;    //clear state for the next payment

        }
        
    }    

    private void updatePaymentCheckNumbers(Id paymentId, List<c2g.PaymentsPlusService.Check> checks, Payment_Processing_Batch_Status__c paymentStatus, Database.BatchableContext batchContext){
        
        if((checks!=null)&&(checks.size()>0)){
 
            try{
                
                System.debug('Calling updateCheckNumbers():  ' + paymentId + ', checks:  ' + checks.size());
                //All the checks for a payment have to be sent to this call at once.
                c2g.PaymentsPlusService.updateCheckNumbers(paymentId, checks);

                System.debug('c2g.PaymentsPlusService.updateCheckNumbers() succeeded.');

                if(paymentStatus!=null)
                {
                    paymentStatus.Update_Check_Numbers_Job_Id__c=batchContext.getJobId(); 
                    paymentStatus.Are_Check_Numbers_Updated__c = true;
                    paymentStatus.Message__c = 'Check Numbers Updated';
                    paymentStatus.Has_Update_Check_Numbers_Error__c=false;
                    paymentStatus.Has_Error__c=false;                    

                }else{
                    System.debug('No paymentStatus tracking record for payment ' + paymentId);
                }

            }catch(Exception e){

                String errorMessage=null;
                errorMessage=e.getMessage();
                //System.debug('updateCheckNumbers() error:  ' + errorMessage + ' - payment:  ' + paymentId);
                
                handleExceptionStatus(e, paymentStatus, paymentId, batchContext);

                if(paymentStatus!=null)
                {   
                    update paymentStatus;
                }
                throw new PaymentProcessing.PaymentProcessException('Check numbering error:  ' + errorMessage, e);
                 
            }

        }else{
            System.debug('No checks!');
        }
    }

    public void handleExceptionStatus(Exception e, Payment_Processing_Batch_Status__c paymentStatus, Id paymentId, Database.BatchableContext batchContext){

        String errorMessage=null;
        errorMessage=e.getMessage();
        System.debug('updateCheckNumbers() error:  ' + errorMessage + ' - payment:  ' + paymentId);
        
        if(paymentStatus!=null)
        {
            if(batchContext!=null){
                paymentStatus.Update_Check_Numbers_Job_Id__c=batchContext.getJobId();
            }
            paymentStatus.Are_Check_Numbers_Updated__c = false;
            paymentStatus.Message__c = 'Check Number Update Error:  ' + errorMessage;
            paymentStatus.Last_Error_Message__c = 'Check Number Update Error:  ' + errorMessage;
            paymentStatus.Update_Check_Numbers_Last_Error_Message__c=errorMessage;
            paymentStatus.Has_Update_Check_Numbers_Error__c=true;
            paymentStatus.Has_Error__c=true;

            PaymentProcessingStatusUpdater statusUtility=new PaymentProcessingStatusUpdater();
            statusUtility.trimMessages(paymentStatus);

        }
            
    }
        
    // public Integer getNextCheckNumber(c2g__codaCheckRange__c checkRange){
        
    //     Integer initialNextCheckNumber = null;
          
    //     String nextCheckNumberText=null;
    //     nextCheckNumberText=checkRange.c2g__NextCheckNumber__c;                
        
    //     initialNextCheckNumber = integer.valueof(nextCheckNumberText);
		
    //     return initialNextCheckNumber;
        
    // }    
    
    public Integer getNextCheckNumber(Id bankAccountId){
        
        Integer initialNextCheckNumber = null;
        
        PaymentProcessingDataAccess payData=null;
        payData=new PaymentProcessingDataAccess();
        
        List<c2g__codaCheckRange__c> checkRanges = null;
        
        checkRanges=payData.getCheckRanges(bankAccountId);
        
        c2g__codaCheckRange__c checkRange = null;
        if((checkRanges!=null)&&(checkRanges.size()>0)){
            checkRange = checkRanges[0];
        }
        
        String nextCheckNumberText=null;
        if(checkRange!=null){
            nextCheckNumberText=checkRange.c2g__NextCheckNumber__c;
            initialNextCheckNumber = integer.valueof(nextCheckNumberText);
        }

        return initialNextCheckNumber;
        
    }
    
    // private List<c2g__codaPaymentAccountLineItem__c> getPaymentSummaries(List<Id> paymentIds){
        
    //     List<c2g__codaPaymentAccountLineItem__c> paymentSummaries=null;

    //     PaymentProcessingDataAccess payData=null;
    //     payData=new PaymentProcessingDataAccess();
        
    //     paymentSummaries=payData.getPaymentSummaryAccounts(paymentIds);
        
    //     return paymentSummaries;
        
    // }    
    
    public void finish(Database.BatchableContext batchContext){

        if(paymentStatuses!=null){
            update paymentStatuses;
        }

        System.debug('PaymentProcessingUpdateCheckNumbersBatch finished.');     

    }
    
}