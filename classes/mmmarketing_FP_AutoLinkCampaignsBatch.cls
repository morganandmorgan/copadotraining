/**
 *  mmmarketing_FP_AutoLinkCampaignsBatch
 */
public class mmmarketing_FP_AutoLinkCampaignsBatch
    implements mmlib_GenericBatch.IGenericExecuteBatch, Database.Batchable<SObject>
{
    public void run( List<SObject> scope )
    {
        mmmarketing_FinancialPeriods financialPeriods = new mmmarketing_FinancialPeriods( (list<Marketing_Financial_Period__c>)scope );

        financialPeriods.autoLinkToMarketingInformationIfRequiredInQueueableMode();
    }

    public mmmarketing_FP_AutoLinkCampaignsBatch () { }

    private integer batchSize = 200;

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return mmmarketing_FinancialPeriodsSelector.newInstance().selectQueryLocatorWhereMarketingCampaignNotLinked();
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            mmmarketing_FinancialPeriods financialPeriods = new mmmarketing_FinancialPeriods( (list<Marketing_Financial_Period__c>)scope );

            financialPeriods.autoLinkToMarketingInformationIfRequiredInQueueableMode();
        }
        catch (Exception e)
        {
            System.debug('Error executing batch ' + e);
        }
    }

    public void finish(Database.BatchableContext context)
    {

    }

    public mmmarketing_FP_AutoLinkCampaignsBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public void execute()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticFPLinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticFPLinkingEnabled__c )
        {
            Database.executeBatch( this, batchSize );
        }
    }

}