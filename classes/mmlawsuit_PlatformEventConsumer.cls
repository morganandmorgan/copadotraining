public class mmlawsuit_PlatformEventConsumer
        implements mmlib_IEventsConsumer {
    private List<SObject> eventSObjects = new List<SObject>();

    public void setEvents(List<SObject> eventSObjects) {
        this.eventSObjects = eventSObjects;
    }

    public void execute(QueueableContext ctx) {
        System.debug('Begin mmlawsuit_PlatformEventConsumer.execute');
        if (eventSObjects != null && !eventSObjects.isEmpty()) {
            mmlib_Event__e evt = (mmlib_Event__e) eventSObjects.get(0);

            System.debug('JLW:::Event Consumed: ' + evt.EventName__c);
            if ('MatterStatusChanged'.equalsIgnoreCase(evt.EventName__c)) {
                matterStatusChangedEvent(eventSObjects);
            } else if ('UsersChanged'.equalsIgnoreCase(evt.EventName__c)) {
                userChangedEvent(eventSObjects);
            } else {
                System.debug('Platform Events named "' + evt.EventName__c + '" arrived but are not handled.\nData:' + JSON.serialize(eventSObjects));
            }
        }
    }

    // ========= Matter Status Changed ========================================================
    private void matterStatusChangedEvent(List<mmlib_Event__e> eventList) {
        if (eventList == null || eventList.isEmpty()) return;

        Map<Id, String> matterStatusMap = new Map<Id, String>();

        // Extract all Matter IDs from the Events.
        for (mmlib_Event__e evt : eventList) {
            List<mmmatter_Matters.StatusChangedPlatformEventData> dataList =
                    (List<mmmatter_Matters.StatusChangedPlatformEventData>)
                            JSON.deserialize(evt.Payload__c, List<mmmatter_Matters.StatusChangedPlatformEventData>.class);

            for (mmmatter_Matters.StatusChangedPlatformEventData data : dataList) {
                matterStatusMap.put(data.matter_id, data.new_status);
            }
        }

        mmlawsuit_LawsuitsService.updateStatusForMatterChange(matterStatusMap);
    }

    // ========= User Changed =================================================================
    private void userChangedEvent(List<mmlib_Event__e> eventList) {
        if (eventList == null || eventList.isEmpty()) return;

        // Payload is list of User IDs.
        Set<Id> userIdSet = new Set<Id>();
        for (mmlib_Event__e evt : eventList) {
            for (Id userId : ((List<Id>) JSON.deserialize(evt.Payload__c, List<Id>.class))) {
                userIdSet.add(userId);
            }
        }

        mmlawsuit_LawsuitsService.updateTeamMemberAssignmentsForUserChange(userIdSet);
    }
}