/**
 *  @usage mmlitifylrn_ReferralIntakesCallout.Response resp = (mmlitifylrn_ReferralIntakesCallout.Response) new mmlitifylrn_ReferralIntakesCallout()
 *                                   .setReferral( litify_pm__Referral__c )
 *                                   .setAuthorization( mmlitifylrn_CalloutAuthorization )
 *                                   .debug()
 *                                   .execute()
 *                                   .getResponse();
 *
 *  @usage system.debug( resp.getFooBar() );
 */
public class mmlitifylrn_ReferralIntakesPostCallout
        extends mmlitifylrn_BasePOSTV1Callout
{
    private mmlitifylrn_ReferralIntakesPostCallout.Response resp = new mmlitifylrn_ReferralIntakesPostCallout.Response();

    private litify_pm__Referral__c referral = null;

    public override map<string, string> getParamMap()
    {
        return new map<string, string>();
    }

    public override string getPathSuffix()
    {
        return '/referral_intakes';
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        addPostRequestBody(getRequestBody());

        system.debug('RequestBody______________________________________________________');
        system.debug(JSON.serializePretty(getRequestBody(), true));
        system.debug('End of RequestBody______________________________________________________');

        httpResponse httpResp = super.executeCallout();

        if (this.isDebugOn)
        {
            system.debug(httpResp.getStatusCode());
            system.debug(httpResp.getStatus());
        }

        string jsonResponseBody = httpResp.getBody();

        if (this.isDebugOn)
        {
            system.debug(json.deserializeUntyped(jsonResponseBody));
        }

        if (httpResp.getStatusCode() >= 200
                && httpResp.getStatusCode() < 300)
        {
            resp = (mmlitifylrn_ReferralIntakesPostCallout.Response) json.deserialize('{"referral_intake":' + httpResp.getBody() + '}', mmlitifylrn_ReferralIntakesPostCallout.Response.class);
        } else
        {
            throw new mmlitifylrn_Exceptions.CalloutException(httpResp);
        }

        return this;
    }

    public mmlitifylrn_LRNModels.ReferralIntakeBasicData getRequestBody()
    {
        return new mmlitifylrn_LRNModels.ReferralIntakeBasicData(this.referral);
    }

    public class Response
            implements mmlib_BaseCallout.CalloutResponse
    {
        public mmlitifylrn_LRNModels.ReferralIntakeBasicData referral_intake;

        public integer getTotalNumberOfRecordsFound()
        {
            return referral_intake == null ? 0 : 1;
        }
    }

    private void validateCallout()
    {
        if (this.referral == null)
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The referral record has not been specified.  Please use the setReferral(litify_pm__Referral__c) method before calling the execute() method.');
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public mmlitifylrn_ReferralIntakesPostCallout setReferral(litify_pm__Referral__c referral)
    {
        this.referral = referral;

        return this;
    }
}