/**
 * Event_Trigger_Test
 * @description Test for Event Trigger.
 * @author Jeff Watson
 * @date 5/15/2019
 */
@isTest
public with sharing class Event_Trigger_Test {

    @isTest
    private static void eventTrigger() {
        Test.startTest();
        Event event = TestUtil.createEvent();
        insert event;
        event.Subject = 'Updated Subject';
        update event;
        delete event;
        Test.stopTest();
    }
}