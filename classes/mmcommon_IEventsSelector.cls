/**
 *  mmcommon_IEventsSelector
 */
public interface mmcommon_IEventsSelector extends mmlib_ISObjectSelector
{
    List<Event> selectById(Set<Id> idSet);
    List<Event> selectByUserIdAndDate(Set<Id> idSet, Set<Date> dateSet);
    List<Event> selectByUserOrderByDate(Set<Id> idSet);
    List<Event> selectAll();

    List<Event> selectMyTodayStartEvents();
    List<Event> selectFutureOpenTasks(Integer numDays);
    list<Event> selectByTypeAndWhat( Set<String> typeSet, Set<Id> relatedWhatIdSet);
}