@isTest
private class EnsureIncidentCreatedTest {
    
    static Account client;
    static {
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        client = new Account(FirstName = 'Bill', LastName = 'Gates', RecordType = personAccountRecordType);
        insert client;
    }
    
    @isTest static void IntakeCreated_WithoutIncident_IncidentIsCreated() {
        List<Incident__c> incidents = [SELECT Id FROM Incident__c];
        System.assert(incidents.size() == 0);

        Test.startTest();

        Intake__c intake = new Intake__c(Case_Type__c = 'Xarelto', Client__c = client.Id);
        insert intake;

        Test.stopTest();
        incidents = [SELECT Id FROM Incident__c];
        intake = [SELECT Incident__c FROM Intake__c WHERE ID = :intake.id];

        System.assert(incidents.size() == 1);
        System.assertEquals(incidents.get(0).Id, intake.Incident__c);
    }
    
    @isTest static void IntakeCreated_WithIncident_NoNewIncidentIsCreated() {
        Test.startTest();
        Incident__c inc = new Incident__c();
        insert inc;
        Intake__c intake = new Intake__c(Case_Type__c = 'Xarelto', Client__c = client.Id, Incident__c = inc.Id);
        insert intake;

        Test.stopTest();

        List<Incident__c> incidents = [SELECT Id FROM Incident__c];
        System.assert(incidents.size() == 1);
        System.assertEquals(inc.Id, incidents.get(0).Id);
        
        intake = [SELECT Incident__c FROM Intake__c WHERE ID = :intake.id];
        System.assertEquals(inc.Id, intake.Incident__c);
    }

    @isTest
    private static void bulkIntakeCreation_AllBlankIncidentRelationship() {
        Integer bulkSize = Limits.getLimitDMLStatements() + 1;
        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < bulkSize; ++i) {
            Intake__c intake = new Intake__c(Case_Type__c = 'Xarelto', Client__c = client.Id, Incident__c = null);
            intakes.add(intake);
        }

        Test.startTest();
        Database.insert(intakes);
        Test.stopTest();

        Map<Id, Intake__c> resultIntakes = new Map<Id, Intake__c>([
                SELECT Id, Incident__c
                FROM Intake__c
                WHERE Id IN :intakes
            ]);

        Set<Id> uniqueIncidentIds = new Set<Id>();
        for (Intake__c intake : intakes) {
            Intake__c resultIntake = resultIntakes.get(intake.Id);

            System.assertNotEquals(null, resultIntake, intake);
            System.assertNotEquals(null, resultIntake.Incident__c);
            System.assert(uniqueIncidentIds.add(resultIntake.Incident__c));
        }
    }
}