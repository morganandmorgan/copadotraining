/**
 * Trigger_Helper_Test
 * @description Test for Trigger Helper class.
 * @date 5/10/2019 10K Initial
 * @date 5/14/2019 Jeff Revised and ported to Trigger Helper Test class and added code coverage.
 */
@isTest
public with sharing class Trigger_Helper_Test {

    @isTest
    private static void isDisabled() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'M&M System Administrator Modified'];

        Triggers_Setting__c ts = new Triggers_Setting__c(SetupOwnerId = p.Id, Object_API_Name__c = 'Task');
        insert ts;

        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        User u = new User(Alias = 'standt',
                Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                UserName = uniqueUserName);

        System.runAs(u) {
            Boolean isDisabled = Trigger_Helper.isDisabled('Task');
            System.assertEquals(true, isDisabled);
            System.assertEquals(true, Trigger_Helper.isFirstRun);
        }
    }
}