@isTest
private class FinanceRequest_Service_Test {

    private static litify_pm__Matter__c matter;
    private static Finance_Request__c testFinanceRequest;
    private static List<Account> testAccounts;
    private static litify_pm__Expense_Type__c expenseType;
    private static c2g__codaCompany__c company;

    static void setupData() {
        company = TestDataFactory_FFA.company;

        /*--------------------------------------------------------------------
        LITIFY Data Setup
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        testAccounts = new List<Account>();

        Account vendor = new Account();
        vendor.Name = 'TEST CONTACT';
        vendor.litify_pm__First_Name__c = 'TEST';
        vendor.litify_pm__Last_Name__c = 'CONTACT';
        vendor.litify_pm__Email__c = 'test@testcontact.com';
        vendor.c2g__CODAAccountsPayableControl__c = TestDataFactory_FFA.balanceSheetGLAs[0].Id;
        vendor.c2g__CODAAccountTradingCurrency__c = 'USD';
        vendor.c2g__CODADefaultExpenseAccount__c = TestDataFactory_FFA.balanceSheetGLAs[0].Id;
        vendor.c2g__CODATaxCalculationMethod__c = 'Gross';

        testAccounts.add(vendor);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = TestDataFactory_FFA.company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        insert testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        insert matter;

        expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        insert expenseType;

    }

    @isTest static void test_FinanceRequestDomain() {
        FinanceRequest_Domain frDom = new FinanceRequest_Domain();
    }

    @isTest static void test_executeAllocationReset() {
        setupData();
        testFinanceRequest = new Finance_Request__c();
        testFinanceRequest.RecordTypeId = Schema.SObjectType.Finance_Request__c.RecordTypeInfosByName.get('Cost Check Request').RecordTypeId;
        testFinanceRequest.Matter__c = matter.Id;
        testFinanceRequest.Check_Amount__c = 10.0;
        testFinanceRequest.Expense_Type__c = expenseType.Id;
        testFinanceRequest.Vendor_Account__c = testAccounts[0].Id;
        insert testFinanceRequest;

        testFinanceRequest.Approved_Date__c = Date.today();
        update testFinanceRequest;
    }
}