@isTest 
private without sharing class PaymentProcessingStatusUpdater_Test {

	private static c2g__codaCompany__c company=null;
    private static c2g__codaBankAccount__c operatingBankAccount=null;

    //Cache rules everything around me:  
	private static Payment_Collection__c paymentCollection = null; 
    private static c2g__codaPayment__c payment = null; 
    private static Payment_Processing_Batch_Status__c trackingRecord = null; 

    
	static void setupTestData(){
        //TODO:  Reconsider this approach:  Maybe don't cache records in static state.
        //TODO:  Consider rolling setup methods into test data factory.
        System.debug('Creating setup test fixture data if needed...');
    	setupPaymentCollectionData();
        
        if(payment==null){
            System.debug('Creating payment, etc...');
            
            paymentCollection=createAndInsertPaymentCollection();
            payment=createAndInsertPayment(paymentCollection.Id);      
            trackingRecord=createPaymentProcessTrackingRecord(paymentCollection.Id, payment.Id);
        }

	}
    
	static void setupPaymentCollectionData(){

        if(company==null){

            System.debug('Creating payment collection prerequisite data...');
            /*--------------------------------------------------------------------
            FFA data setup
            --------------------------------------------------------------------*/
            company = TestDataFactory_FFA.company;

            operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
            operatingBankAccount.Bank_Account_Type__c = 'Operating';
            update operatingBankAccount;

            // Map<c2g__codaPurchaseInvoice__c, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> payableInvoices_ExpenseMap = TestDataFactory_FFA.createPayableInvoices_Expense(2);
            // TestDataFactory_FFA.postPayableInvoices(payableInvoices_ExpenseMap.keySet());

        }

        System.assert(company!=null, 'Company is null.');
        System.assert(operatingBankAccount!=null, 'operatingBankAccount is null.');
        System.assert(operatingBankAccount.Bank_Account_Type__c!=null, 'operatingBankAccount.Bank_Account_Type__c is null.');

	}
    
    static Payment_Collection__c createAndInsertPaymentCollection(){

        System.debug('Creating payment collection...');

        Payment_Collection__c record = null;

        System.assert(company!=null, 'Company is null.');
        System.assert(operatingBankAccount!=null, 'operatingBankAccount is null.');

		record = new Payment_Collection__c(
			Company__c = company.Id,
			Bank_Account__c = operatingBankAccount.Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		insert record;
        return record;

    }      
    
    static c2g__codaPayment__c createAndInsertPayment(Id paymentCollectionId){
        
        c2g__codaPayment__c record = null;

        System.debug('Creating payment object...');

        {

            //Note:  Looks like cnt param doesn't do anything
            record=TestDataFactory_FFA.createPayment(1, false);

            // if(paymentCollectionId==null){
            //     paymentCollectionId=paymentCollection.Id;
            // }

            record.Payment_Collection__c = paymentCollectionId;

            System.assert((record.Payment_Collection__c!=null), 'Payment.Payment_Collection__c is null.');
            
            {
                Database.SaveResult result = null;

                System.debug('Inserting payment object...');               
                
                result=Database.insert(record);

                System.assert(result.isSuccess(), 'Payment insert failed.');

            }

            System.assert(record!=null, 'Payment is null.');
            System.assert(record.Id!=null, 'Payment Id is null.');

        }

        return record;

    } 
    
    static Payment_Processing_Batch_Status__c createPaymentProcessTrackingRecord(Id paymentCollectionId, Id paymentId){
        Payment_Processing_Batch_Status__c tracker=null;
        {
            tracker=new Payment_Processing_Batch_Status__c();
            tracker.Payment_Collection__c = paymentCollectionId;
            tracker.Payment__c = paymentId;
            insert tracker;
        }

        PaymentProcessingStatusDataAccess data=null;
        data=new PaymentProcessingStatusDataAccess();
        List<Payment_Processing_Batch_Status__c> records=null;
        //Get the full data - all the fields we need:  
        records=data.getAllPaymentCollectionTrackingRecords(paymentCollection.Id);
        //records=data.getAllPaymentTrackingRecords(payment.Id);
        //getAllPaymentCollectionTrackingRecords
        if((records!=null)&&(records.size()>0)){
            tracker=records[0];
        }else{
            System.assert(false, 'Tracking records no retrieved using data access utility.');
        }

        System.assert((tracker!=null), 'Tracking record is null.');
        System.assert((tracker.Payment__r.Name!=null), 'Tracker payment name is null.');
        
        return tracker;
    }    
    
    static List<Payment_Status_Update__e> createPaymentStatusEvents_0(){
        //Not called?
        List<Payment_Status_Update__e> paymentEvents=null;
        paymentEvents=new List<Payment_Status_Update__e>();

        Payment_Status_Update__e event=null;
        event=new Payment_Status_Update__e();

        //Consumers assume this is a record Id, so I had to put a filter on this:  
        String dateString = null;
        dateString = String.valueOf(Datetime.now());

        //Exceeding a limit in the field length quietly causes the event to fail.  No error happens at publish time.
        
        event.Payment_Id__c='Testing';
 
        //event.Payment_Collection_Id__c=event.Payment_Id__c;
        //event.Payment_Collection_Id__c='TestPayCollection';

        //System.debug(event.Payment_Id__c);
        //System.debug(event.Payment_Collection_Id__c);

        event.Status__c = 'Unit Test';                   
        event.Message__c = 'Unit Testing - ' + dateString;

        paymentEvents.add(event); 

        return paymentEvents;
        
    }  
    
    static List<c2g__PaymentsPlusErrorLog__c> testInsertPaymentsPlusErrorLogRecord(){
        setupTestData();
        System.debug('Creating payments plus error log record...');

        List<c2g__PaymentsPlusErrorLog__c> paymentErrorLogRecords = null;
        paymentErrorLogRecords=new List<c2g__PaymentsPlusErrorLog__c>();
 
        System.debug('Retrieving transaction line item...');

        c2g__codaTransactionLineItem__c transactionLineItem = null; 

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = 
        [
            select
            Id
            ,Name
            from c2g__codaTransactionLineItem__c 
            order by CreatedDate desc 
            limit 1
        ];

        if(transactionLineItems.size()>0){
            transactionLineItem=transactionLineItems[0];
            System.assert(transactionLineItem!=null);
            System.assert(transactionLineItem.Id!=null);            
        }else{
            System.debug('No transaction line item.');
        }
        
        System.debug('Creating payment error log record...');   

        c2g__PaymentsPlusErrorLog__c record = null;     

        record = new c2g__PaymentsPlusErrorLog__c();

        record.c2g__PaymentProposalNumber__c = payment.Id;   //Master-detail lookup to parent pament
        if(transactionLineItem!=null){
            record.c2g__TransactionLineItem__c = transactionLineItem.Id;
        }

        record.c2g__LogType__c = 'Error';
        record.c2g__PaymentStage__c = 'Add to Proposal';

        System.debug('Inserting payment error log record...');

        insert record;  //Do we have to insert it for the test?

        System.debug('Inserted.');

        paymentErrorLogRecords.add(record);

        return paymentErrorLogRecords;
        
    }    
    
    static List<Payment_Status_Update__e> createPaymentStatusEvents_1(){

        setupTestData();
  
        List<Payment_Status_Update__e> paymentEvents=null;
        paymentEvents=new List<Payment_Status_Update__e>();

        Payment_Status_Update__e event=null;
        event=new Payment_Status_Update__e();

        //Consumers assume this is a record Id, so I had to put a filter on this:  
        String dateString = null;
        dateString = String.valueOf(Datetime.now());

        //Exceeding a limit in the field length quietly causes the event to fail.  No error happens at publish time.
        
        event.Payment_Id__c=payment.Id;
 
        event.Payment_Collection_Id__c=paymentCollection.ID;
 
        event.Status__c = 'Unit Test';                   
        event.Message__c = 'Unit Testing - ' + dateString;

        paymentEvents.add(event); 

        return paymentEvents;
        
    }       



    //-----             


    @isTest 
    static void test00(){
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);
    }


    @isTest 
    static void testCreateTarget(){
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);
    }  
  
    @isTest 
    static void test0(){

        setupTestData();

        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        c2g__codaPayment__c record = null;

        //record = createAndInsertPayment();
        //Have to re-use the one instance?  To avoid You cannot modify this invoice?
        record=payment;

        System.assert(record!=null);
        System.assert(record.Id!=null);

        //System.assert(trackingRecord!=null);

        Test.startTest();
        target.updatePaymentPostAndMatchOperationStatuses(record.Id);
        Test.stopTest();

        System.assert(true);

    }     

    @isTest 
    static void test1(){
        setupTestData();
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        // c2g__codaPayment__c payment = null;

        //payment = createAndInsertPayment();
        // record=payment;
        System.assert(payment!=null);
        System.assert(payment.Id!=null);

        System.assert(trackingRecord!=null);

        Test.startTest();
        target.updateSinglePaymentStatus(payment.Id);
        Test.stopTest();

        System.assert(true);

    }    

    @isTest 
    static void test2(){

        setupTestData();

        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        // c2g__codaPayment__c payment = null;

        //payment = createAndInsertPayment();
        // record=payment;

        System.assert(payment!=null);
        System.assert(payment.Id!=null);

        System.assert(trackingRecord!=null);

        Test.startTest();
        target.updateSinglePaymentPostAndMatchOperationStatuses(payment.Id);
        Test.stopTest();

        System.assert(true);

    }  

    @isTest 
    static void testPaymentCollection0(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        Payment_Collection__c record = null;

        record = paymentCollection;
        System.assert(record!=null);
        System.assert(record.Id!=null);

        System.assert(trackingRecord!=null);

        Test.startTest();
        target.updatePaymentProcessingStatuses(record.Id);
        Test.stopTest();

        System.assert(true);    //Be assertive.

    }

    @isTest 
    static void testPaymentCollection1(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        Payment_Collection__c record = null;

        record = paymentCollection;
        System.assert(record!=null);
        System.assert(record.Id!=null);

        System.assert(trackingRecord!=null);

        Test.startTest();
        target.updatePostAndMatchOperationStatuses(record.Id);
        Test.stopTest();

        System.assert(true);    //Be assertive.

    }

    @isTest 
    static void testPaymentCollection2(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        Payment_Collection__c record = null;

        record = paymentCollection;
        System.assert(record!=null);
        System.assert(record.Id!=null);

        //TODO:  Consider storing this tracking record also?
        // Payment_Processing_Batch_Status__c tracker = null;
        // tracker=createPaymentProcessTrackingRecord(paymentCollection, payment);

        System.assert(trackingRecord!=null);

        Id apexBatchProcessId = null;

        Test.startTest();
        apexBatchProcessId = target.launchStatusUpdateBatchProcess(record.Id);
        Test.stopTest();

        System.assert(apexBatchProcessId!=null);    //Be assertive.

    }    

    @isTest 
    static void testTrackingRecord0(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);        

        List<Payment_Processing_Batch_Status__c> trackingRecords = null; 
        trackingRecords=new List<Payment_Processing_Batch_Status__c> {trackingRecord};

        Id apexBatchProcessId = null;

        Test.startTest();
        target.updatePaymentProcessingStatuses(trackingRecords);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    }  

    @isTest 
    static void testTrackingRecord0_New(){

        testTrackingRecord_PaymentStatus('New');

        System.assert(trackingRecord!=null);    //Be assertive.

    }   

    @isTest 
    static void testTrackingRecord0_Proposed(){

        testTrackingRecord_PaymentStatus('Proposed');

        System.assert(trackingRecord!=null);    //Be assertive.

    }

    @isTest 
    static void testTrackingRecord0_Media_Updated(){

        testTrackingRecord_PaymentStatus('Media Updated');

        System.assert(trackingRecord!=null);    //Be assertive.

    }    
    
    @isTest 
    static void testTrackingRecord0_Ready_to_Post(){

        testTrackingRecord_PaymentStatus('Ready to Post');

        System.assert(trackingRecord!=null);    //Be assertive.

    }   
     
    @isTest 
    static void testTrackingRecord0_Background_Posting(){

        testTrackingRecord_PaymentStatus('Background Posting');

        System.assert(trackingRecord!=null);    //Be assertive.

    }

    @isTest 
    static void testTrackingRecord0_Preparing_for_Media(){

        testTrackingRecord_PaymentStatus('Preparing for Media');

        System.assert(trackingRecord!=null);    //Be assertive.

    }        

    @isTest 
    static void testTrackingRecord0_Posted(){

        testTrackingRecord_PaymentStatus('Posted');

        System.assert(trackingRecord!=null);    //Be assertive.

    }

    @isTest 
    static void testTrackingRecord0_Matched(){

        testTrackingRecord_PaymentStatus('Matched');

        System.assert(trackingRecord!=null);    //Be assertive.

    }    

    @isTest 
    static void testTrackingRecord0_Error(){

        testTrackingRecord_PaymentStatus('Error');

        System.assert(trackingRecord!=null);    //Be assertive.

    }  

    @isTest 
    static void testTrackingRecord0_Preparing_Media_Error(){

        testTrackingRecord_PaymentStatus('Preparing Media Error');

        System.assert(trackingRecord!=null);    //Be assertive.

    }  

    @isTest 
    static void testTrackingRecord0_Matched_WithApexJobs_0(){

        testTrackingRecord_PaymentStatus_WithApexJobs('Matched', 'Completed');

        System.assert(trackingRecord!=null);    //Be assertive.

    }
    
    @isTest 
    static void testTrackingRecord0_Matched_WithApexJobs_1(){

        testTrackingRecord_PaymentStatus_WithApexJobs('Matched', 'Failed');

        System.assert(trackingRecord!=null);    //Be assertive.

    }    
    
    static void testTrackingRecord_PaymentStatus_WithApexJobs(String paymentStatus, String jobStatus){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);   

        Payment_Processing_Batch_Status__c trackingRecord2=null;
        //trackingRecord2=trackingRecord.clone(true, true, true, true);
        
        //NOTE:  This could affect a shared payment record:  
        trackingRecord.Payment__r.c2g__Status__c = paymentStatus;
        
        String extendedStatus=null;

        //TODO:  
        AsyncApexJob checkApexJob=null;
        checkApexJob = createApexJobRecord(jobStatus, extendedStatus);
        trackingRecord.Update_Check_Numbers_Job_Id__c = checkApexJob.Id;
        
        //TODO:  
        AsyncApexJob apexJob=null;
        apexJob = createApexJobRecord(jobStatus, extendedStatus);
        trackingRecord.Post_Job_Id__c = apexJob.Id;

        List<Payment_Processing_Batch_Status__c> trackingRecords = null; 
        trackingRecords=new List<Payment_Processing_Batch_Status__c> {trackingRecord};
       
        Test.startTest();
        target.updatePaymentProcessingStatuses(trackingRecords);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    }    
     
    static void testTrackingRecord_PaymentStatus(String paymentStatus){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);   

        Payment_Processing_Batch_Status__c trackingRecord2=null;
        //trackingRecord2=trackingRecord.clone(true, true, true, true);
        
        //NOTE:  This could affect a shared payment record:  
        trackingRecord.Payment__r.c2g__Status__c = paymentStatus;

        List<Payment_Processing_Batch_Status__c> trackingRecords = null; 
        trackingRecords=new List<Payment_Processing_Batch_Status__c> {trackingRecord};
       
        Test.startTest();
        target.updatePaymentProcessingStatuses(trackingRecords);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    }

   @isTest 
    static void test_updatePaymentProcessingStatuses_checkApexJob_0(){
        test_updatePaymentProcessingStatuses_checkApexJob('Completed', null);
    }

   @isTest 
    static void test_updatePaymentProcessingStatuses_checkApexJob_00(){
        test_updatePaymentProcessingStatuses_checkApexJob('Completed', 'There was an error.');
    }    
    
   @isTest 
    static void test_updatePaymentProcessingStatuses_checkApexJob_Failed_0(){
        test_updatePaymentProcessingStatuses_checkApexJob('Failed', 'There was an error.');
    }
    
   @isTest 
    static void test_updatePaymentProcessingStatuses_checkApexJob_Failed_1(){
        test_updatePaymentProcessingStatuses_checkApexJob('Failed', null);
    }  

   @isTest 
    static void test_updatePaymentProcessingStatuses_ApexJob_0(){
        test_updatePaymentProcessingStatuses_ApexJob('Completed', null);
    }

   @isTest 
    static void test_updatePaymentProcessingStatuses_ApexJob_00(){
        test_updatePaymentProcessingStatuses_ApexJob('Completed', 'There was an error.');
    }    
    
   @isTest 
    static void test_updatePaymentProcessingStatuses_ApexJob_Failed_0(){
        test_updatePaymentProcessingStatuses_ApexJob('Failed', 'There was an error.');
    }
    
   @isTest 
    static void test_updatePaymentProcessingStatuses_pexJob_Failed_1(){
        test_updatePaymentProcessingStatuses_ApexJob('Failed', null);
    }  
      
   
    static void test_updatePaymentProcessingStatuses_checkApexJob(String status, String extendedStatus){
        //updatePaymentProcessingStatuses_checkApexJob
        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);   

        // Payment_Processing_Batch_Status__c trackingRecord2=null;
        //trackingRecord2=trackingRecord.clone(true, true, true, true);
        
        //NOTE:  This could affect a shared payment record:  
        trackingRecord.Payment__r.c2g__Status__c = 'Ready to Post';

        List<Payment_Processing_Batch_Status__c> trackingRecords = null; 
        trackingRecords=new List<Payment_Processing_Batch_Status__c> {trackingRecord};

        Id apexBatchProcessId = null;

        Boolean updateStatus = false;
        AsyncApexJob checkApexJob=null;
        //checkApexJob=new AsyncApexJob();
        //Field is not writeable: AsyncApexJob.Status
        //checkApexJob.Status = 'Completed';
        checkApexJob = createApexJobRecord(status, extendedStatus);

        // Id jobId = null;
        // jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType); 

        // checkApexJob.Id = jobId;       

        Test.startTest();
        target.updatePostAndMatchOperationStatuses(trackingRecords);

        //checkApexJob, tracker, updateStatus
        target.updatePaymentProcessingStatuses_UpdateChecksApexJob(checkApexJob, trackingRecord, updateStatus);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    }
    
    static AsyncApexJob createApexJobRecord(String status, String extendedStatus){
        AsyncApexJob record=null;
        record=new AsyncApexJob();
        if(status!=null){
            //Hack to get around //Field is not writeable: AsyncApexJob.Status
            String jobJSON = null;
            jobJSON = '{"Status": "'+ status + '"}';
            if(extendedStatus!=null){
                jobJSON = '{"Status": "'+ status + '", "ExtendedStatus": "'+ extendedStatus + '"}';
            }
            record = (AsyncApexJob) JSON.deserialize(jobJSON, AsyncApexJob.class);
            System.assert(record.Status!=null);
        }
        System.assert(record!=null);

        Id jobId = null;
        jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType);  

        record.Id = jobId;      

        System.assert(record.Id!=null);

        return record;
    }

    static void test_updatePaymentProcessingStatuses_ApexJob(String status, String extendedStatus){
        //updatePaymentProcessingStatuses_checkApexJob
        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);   

        // Payment_Processing_Batch_Status__c trackingRecord2=null;
        //trackingRecord2=trackingRecord.clone(true, true, true, true);
        
        //NOTE:  This could affect a shared payment record:  
        trackingRecord.Payment__r.c2g__Status__c = 'Ready to Post';

        List<Payment_Processing_Batch_Status__c> trackingRecords = null; 
        trackingRecords=new List<Payment_Processing_Batch_Status__c> {trackingRecord};

        Id apexBatchProcessId = null;

        Boolean updateStatus = false;
        AsyncApexJob apexJob=null;
        apexJob=new AsyncApexJob();
        // Field is not writeable: AsyncApexJob.Status
        // apexJob.Status = 'Completed';
        apexJob = createApexJobRecord(status, extendedStatus);

        Id jobId = null;
        jobId = TestDataFactory_FFA.getFakeRecordId(AsyncApexJob.sObjectType); 

        apexJob.Id = jobId;       

        Test.startTest();
        target.updatePostAndMatchOperationStatuses(trackingRecords);

        //checkApexJob, tracker, updateStatus
        target.updatePaymentProcessingStatuses_ApexJob(apexJob ,trackingRecord, updateStatus);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    } 

    
    @isTest 
    static void testUpdatePostTrackingRecord0_Ready_to_Post(){

        testUpdatePostTrackingRecord_PaymentStatus('Ready to Post');

        System.assert(trackingRecord!=null);    //Be assertive.

    }   
     
    @isTest 
    static void testUpdatePostTrackingRecord0_Background_Posting(){

        testUpdatePostTrackingRecord_PaymentStatus('Background Posting');

        System.assert(trackingRecord!=null);    //Be assertive.

    }
     
    @isTest 
    static void testUpdatePostTrackingRecord0_Posted(){

        testUpdatePostTrackingRecord_PaymentStatus('Posted');

        System.assert(trackingRecord!=null);    //Be assertive.

    }

    @isTest 
    static void testUpdatePostTrackingRecord0_Matched(){

        testUpdatePostTrackingRecord_PaymentStatus('Matched');

        System.assert(trackingRecord!=null);    //Be assertive.

    }    

    @isTest 
    static void testUpdatePostTrackingRecord0_Error(){

        testUpdatePostTrackingRecord_PaymentStatus('Error');

        System.assert(trackingRecord!=null);    //Be assertive.

    }       

    static void testUpdatePostTrackingRecord_PaymentStatus(String paymentStatus){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);   

        // Payment_Processing_Batch_Status__c trackingRecord2=null;
        //trackingRecord2=trackingRecord.clone(true, true, true, true);
        
        //NOTE:  This could affect a shared payment record:  
        trackingRecord.Payment__r.c2g__Status__c = paymentStatus;

        List<Payment_Processing_Batch_Status__c> trackingRecords = null; 
        trackingRecords=new List<Payment_Processing_Batch_Status__c> {trackingRecord};

        Id apexBatchProcessId = null;

        Test.startTest();
        target.updatePostAndMatchOperationStatuses(trackingRecords);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    }             

    @isTest 
    static void testTrimTrackingRecordMessageFields1(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        System.assert(trackingRecord!=null);

        Test.startTest();
        target.trimMessages(trackingRecord);
        Test.stopTest();

        System.assert(trackingRecord!=null);    //Be assertive.

    } 

    @isTest 
    static void testHandlePaymentStatusUpdateEvent_0(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null);

        List<Payment_Status_Update__e> events = createPaymentStatusEvents_1();

        Test.startTest();
        target.handlePaymentStatusUpdateEventTrigger(events);
        Test.stopTest();

        System.assert(events!=null);    //Be assertive.

    }    

    @isTest 
    static void testHandlePaymentsPlusErrorLogRecords_0(){

        //TODO:  Also insert a tracking record for the payment collection.

        setupTestData();

        List<c2g__PaymentsPlusErrorLog__c> errorRecords = testInsertPaymentsPlusErrorLogRecord();
        System.assert(errorRecords!=null);
        
        PaymentProcessingStatusUpdater target = null;
        target=new PaymentProcessingStatusUpdater();
        System.assert(target!=null); 

        Test.startTest();
        target.handlePaymentErrorLogTrigger(errorRecords);
        Test.stopTest();

        System.assert(errorRecords!=null);    //Be assertive.

    }  

}