public class mmbrms_DuringHoursForDefaultTimezone
    implements mmbrms_IRuleEvaluation
{
    public String startTime = null;

    public String endTime = null;

    public Boolean evaluate()
    {
        Time sTime = parseTimeInput(startTime);
        Time eTime = parseTimeInput(endTime);

        if (sTime == null || eTime == null)
        {
            return false;
        }

        BusinessHours defaultBusinessHours = mmcommon_BusinessHoursSelector.newInstance().selectDefault().get(0);
        Timezone tz = Timezone.getTimeZone(defaultBusinessHours.TimeZoneSidKey);

        DateTime mark = DateTime.now();

        if (mock_mark != null)
        {
            mark = mock_mark;
        }

        Integer offset = tz.getOffset(mark.dateGmt()) * -1;

        Time sTimeGmt = sTime.addMilliseconds(offset);

        Time eTimeGmt = eTime.addMilliseconds(offset);

        return 
            (sTimeGmt < eTimeGmt && sTimeGmt <= mark.timeGmt() && mark.timeGmt() <= eTimeGmt)
            ||
            (eTimeGmt < sTimeGmt
                && (mark.timeGmt() <= eTimeGmt) || (sTimeGmt <= mark.timeGmt()));
    }

    private Time parseTimeInput(String value)
    {
        if (value == null || String.isEmpty(value))
        {
            throw new mmbrms_BusinessRuleExceptions.ParameterException('Starting and ending time values are required.');
        }

        List<String> parts = value.split(':');

        if (parts.size() != 2)
        {
            throw new mmbrms_BusinessRuleExceptions.ParameterException('The proper time format is "HH:MM" in 24-hour format, having positive Integer components.');
        }

        Time t = null;

        try
        {
            Integer hours = Integer.valueOf(parts.get(0));
            Integer minutes = Integer.valueOf(parts.get(1));

            t = Time.newInstance(hours, minutes, 0, 0);
        }
        catch(Exception exc)
        {
            throw new mmbrms_BusinessRuleExceptions.ParameterException('The proper time format is "HH:MM" in 24-hour format, having positive Integer components.', exc);
        }

        return t;
    }

    // =========== Test Context =========================================
    @TestVisible
    private DateTime mock_mark = null;
}