/**
 * mmmarketing_TrackingInformations_Test
 * @description Test for mmmarketing_TrackingInformations class.
 * @author Jeff Watson
 * @date 4/11/2019
 */
@IsTest
public with sharing class mmmarketing_TrackingInformations_Test {

    // Todo: This should be removed after providing actual unit tests for mmmarketing_TrackingInformations.
    @IsTest
    private static void codeCoverage() {
        mmmarketing_TrackingInformations.coverage();
    }

    @IsTest
    private static void triggerCoverage() {
        Account personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        Intake__c intake = TestUtil.createIntake(personAccount);
        insert intake;

        Marketing_Tracking_Info__c marketingTrackingInfo = TestUtil.createMarketingTrackingInfo(intake);
        insert marketingTrackingInfo;
    }
}