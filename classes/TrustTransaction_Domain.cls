/*============================================================================
Name            : TrustTransaction_Domain
Author          : CLD
Created Date    : May 2019
Description     : Domain class for Trust Transactions
=============================================================================*/
public class TrustTransaction_Domain extends fflib_SObjectDomain {

    private List<Trust_Transaction__c> trustTransList;
    private TrustTransaction_Service ttService;

    // Ctors
    public TrustTransaction_Domain() {
        super();
        this.trustTransList = new List<Trust_Transaction__c>();
        this.ttService = new TrustTransaction_Service();
    }

    public TrustTransaction_Domain(List<Trust_Transaction__c> trustTransList) {
        super(trustTransList);
        this.trustTransList = (List<Trust_Transaction__c>) records;
        this.ttService = new TrustTransaction_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new TrustTransaction_Domain(records);
        }
    }

    // Trigger Handlers
    public override void onAfterInsert() {
        //TO DO: check if specific specific trigger context is disabled
        ttService.updateMatterRollups(trustTransList);
    }
	public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        //TO DO: check if specific specific trigger context is disabled
        ttService.updateMatterRollups(trustTransList);
    }
   
}