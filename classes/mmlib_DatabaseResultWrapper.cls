public class mmlib_DatabaseResultWrapper
{
    private Boolean isSave = true;
    private Database.DeleteResult deleteResult = null;
    private Database.SaveResult saveResult = null;

    private Boolean isTest = false;
    private Id sobjectId = null;
    private Boolean isSuccess = false;
    private List<mmlib_DatabaseErrorWrapper> errorWrapperList = new List<mmlib_DatabaseErrorWrapper>();

    public Id getId()
    {
        if (isTest)
        {
            return sobjectId;
        }
        else
        {
            return isSave ? saveResult.getId() : deleteResult.getId();
        }
    }

    public Boolean isSuccess()
    {
        if (isTest)
        {
            return this.isSuccess;
        }
        else
        {
            return isSave ? saveResult.isSuccess() : deleteResult.isSuccess();
        }
    }

    public List<mmlib_DatabaseErrorWrapper> getErrors()
    {
        if (isTest)
        {
            // Do nothing;
        }
        else if (issave)
        {
            for(Database.Error err : saveResult.getErrors())
            {
                errorWrapperList.add(new mmlib_DatabaseErrorWrapper(err));
            }
        }
        else
        {
            for(Database.Error err : deleteResult.getErrors())
            {
                errorWrapperList.add(new mmlib_DatabaseErrorWrapper(err));
            }
        }

        return errorWrapperList;
    }

    public mmlib_DatabaseResultWrapper(Database.DeleteResult dr)
    {
        isSave = false;
        deleteResult = dr;
    }

    public mmlib_DatabaseResultWrapper(Database.SaveResult sr)
    {
        saveResult = sr;
    }

    public mmlib_DatabaseResultWrapper(Id sobjectId, Boolean isSuccess, List<mmlib_DatabaseErrorWrapper> errorWrapperList)
    {
        isTest = true;
        this.sobjectId = sobjectId;
        this.isSuccess = isSuccess;
        this.errorWrapperList = errorWrapperList;
    }
}