public class mmwiz_SetupPreviousSelectionModel
    extends mmwiz_AbstractSetupModel
{
    public Map<String, list<String>> answerMapByQuestionToken { get; set; } { answerMapByQuestionToken = new Map<String, list<String>>(); }

    public mmwiz_SetupPreviousSelectionModel()
    {
        setType(mmwiz_SetupPreviousSelectionModel.class);
    }

    public mmwiz_SetupPreviousSelectionModel(String sessionGuid)
    {
        this();

        for (QuestionAndAnswer__c qna : mmwiz_QuestionAndAnswerSelector.newInstance().selectBySessionGuid(new Set<String> { sessionGuid }))
        {
            // TODO - based on the question type, split or don't split based on the token instead of always splitting
            List<String> vals = new List<String>();
            if (!String.isEmpty(qna.Answer__c)) {
                //vals = qna.Answer__c.split(';');
                vals.add(qna.Answer__c);
            }
            answerMapByQuestionToken.put( qna.QuestionToken__c, vals);
        }
    }
}