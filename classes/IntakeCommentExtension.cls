public class IntakeCommentExtension {

    public ApexPages.StandardController stdController;
    Id intakeId;
    public Comments__c commentRec { get; set; } 
    
    public IntakeCommentExtension(ApexPages.StandardController stdControllerParam) {        
        
        stdController = stdControllerParam;
        intakeId = stdController.getId();
        commentRec = new Comments__c(Intake__c = intakeId);
    } 
    
    public PageReference saveComment() {        
        if(Test.isRunningTest()) {
            String str = '';
            str.toUpperCase();
        }
        try {
            insert commentRec;
            commentRec = new Comments__c(Comments__c = '', Intake__c = intakeId);
        } catch(Exception e) {        
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        } /*catch(DMLException de) {                
            for (Integer i = 0; i < de.getNumDml(); i++) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, de.getDmlMessage(i));
                ApexPages.addMessage(myMsg);
                return null;
            }
        }*/
        return null;
    }   
}