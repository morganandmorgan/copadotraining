public with sharing class DateTimeFormatComponentController {
  public DateTime theDateTime { get; set; }
  public String theFormat { get; set; }

  public String getFormattedDateTime() {
    return theDateTime.format(theFormat);
  }

	public DateTimeFormatComponentController() {
	}
}