public interface mmlitifylrn_IReferralRulesSelector extends mmlib_ISObjectSelector
{
    list<ReferralRule__c> selectById( Set<id> idSet );
    list<ReferralRule__c> selectAll();
}