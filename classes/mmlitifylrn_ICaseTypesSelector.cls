public interface mmlitifylrn_ICaseTypesSelector extends mmlib_ISObjectSelector
{
    List<litify_pm__Case_Type__c> selectById( Set<id> idSet );
}