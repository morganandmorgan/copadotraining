/**
 *  mmlib_UtilsTest is all unit tests pertaining to the mmlib_Utils class
 *
 *  @see mmlib_Utils
 */
@isTest
private class mmlib_UtilsTest
{
    @isTest
    static void testPrivateConstructor()
    {
        // this is mostly just to "exercise" the code to get the code coverage.
        //  there is no other purpose for this test.

        mmlib_Utils utils = new mmlib_Utils();
    }

    @isTest
    static void testGenerateStringSetFromField()
    {
        String testString = 'qwertyuiop';

        Contact c = new Contact(firstname = testString);

        Set<String> returnStringSet = mmlib_Utils.generateStringSetFromField( new list<SObject>{ c }, Contact.FirstName);
        System.assertEquals(1, returnStringSet.size());
        System.assertEquals(new Set<String>{ testString }, returnStringSet);

        c = new Contact(firstname = null);

        returnStringSet = mmlib_Utils.generateStringSetFromField( new list<SObject>{ c }, Contact.FirstName, false);
    }

    @isTest
    static void testGenerateIdSetFromField()
    {
        Id accountId = fflib_IDGenerator.generate( Account.SObjectType );

        Contact c = new Contact(accountId = accountId);

        Set<Id> returnIds = mmlib_Utils.generateIdSetFromField( new list<SObject>{ c }, Contact.AccountId);
        System.assertEquals(1, returnIds.size());
        System.assertEquals(new Set<Id>{accountId}, returnIds);

        c = new Contact(accountId = null);

        returnIds = mmlib_Utils.generateIdSetFromField( new list<SObject>{ c }, Contact.AccountId, false);
    }

    @isTest
    static void testGenerateSObjectMapByIdField()
    {
        list<Contact> testContactList = new list<Contact>();

        Id account1Id = fflib_IDGenerator.generate( Account.SObjectType );
        Id account2Id = fflib_IDGenerator.generate( Account.SObjectType );

        Id contact1AId = fflib_IDGenerator.generate( Contact.SObjectType );
        testContactList.add( new Contact( accountId = account1Id, id = contact1AId ) );
        testContactList.add( new Contact( accountId = account1Id, id = fflib_IDGenerator.generate( Contact.SObjectType ) ) );
        testContactList.add( new Contact( accountId = account1Id, id = fflib_IDGenerator.generate( Contact.SObjectType ) ) );

        testContactList.add( new Contact( accountId = account2Id, id = fflib_IDGenerator.generate( Contact.SObjectType ) ) );
        Id contact2BId = fflib_IDGenerator.generate( Contact.SObjectType );
        testContactList.add( new Contact( accountId = account2Id, id = contact2BId ) );

        map<Id, list<SObject>> testResults = null;

        test.startTest();

        testResults = mmlib_Utils.generateSObjectMapByIdField( testContactList, Contact.AccountId );

        test.stopTest();

        System.assert( testResults != null );
        System.assertEquals( testResults.keyset().size(), 2 );
        System.assertEquals( testResults.get(account1Id).size(), 3);
        System.assertEquals( testResults.get(account2Id).size(), 2);
        System.assertEquals( testResults.get(account1Id)[0].id, contact1AId);
        System.assertEquals( testResults.get(account2Id)[1].id, contact2BId);
    }

    @isTest
    static void testGenerateSObjectMapByUniqueField()
    {
        list<Contact> testContactList = new list<Contact>();

        String name1 = 'foobar1';
        String name2 = 'foobar2';
        String name3 = 'foobar3';

        id contactId1 = fflib_IDGenerator.generate( Contact.SObjectType );
        id contactId2 = fflib_IDGenerator.generate( Contact.SObjectType );
        id contactId3 = fflib_IDGenerator.generate( Contact.SObjectType );

        Id accountId = fflib_IDGenerator.generate( Account.SObjectType );

        testContactList.add( new Contact( accountId = accountId, id = contactId1, firstName = name1 ) );
        testContactList.add( new Contact( accountId = accountId, id = contactId2, firstName = name2 ) );
        testContactList.add( new Contact( accountId = accountId, id = contactId3, firstName = name3 ) );

        map<String, SObject> testResults = null;

        test.startTest();

        testResults = mmlib_Utils.generateSObjectMapByUniqueField( testContactList, Contact.firstName );

        test.stopTest();

        System.assert( testResults != null );
        System.assertEquals( testResults.keyset().size(), 3 );

        System.assert( testResults.containsKey( name1 ) );
        System.assert( testResults.containsKey( name2 ) );
        System.assert( testResults.containsKey( name3 ) );

        System.assertEquals( testResults.get( name1 ).id, contactId1 );
        System.assertEquals( testResults.get( name2 ).id, contactId2 );
        System.assertEquals( testResults.get( name3 ).id, contactId3 );
    }

    @isTest
    static void testGenerateOneToOneSObjectMapByIdField()
    {
        List<Opportunity> opportunityList = new List<Opportunity>();

        Opportunity opp = new Opportunity();
        opp.Name = 'apple';
        opp.AccountId = fflib_IDGenerator.generate(Account.SObjectType);
        opportunityList.add(opp);

        opp = new Opportunity();
        opp.Name = 'pear';
        opp.AccountId = fflib_IDGenerator.generate(Account.SObjectType);
        opportunityList.add(opp);

        opp = new Opportunity();
        opp.Name = 'banana';
        opp.AccountId = fflib_IDGenerator.generate(Account.SObjectType);
        opportunityList.add(opp);

        Map<Id, SObject> result = mmlib_Utils.generateOneToOneSObjectMapByIdField(opportunityList, Opportunity.AccountId);

        System.assert(result != null);
        System.assertEquals(3, result.size());
    }

    @isTest
    static void testGenerateGuid()
    {
        system.assert( String.isNotBlank(mmlib_Utils.generateGuid()) );
    }

    @isTest
    static void testGenerateCronTabStringFromDatetime()
    {
        DateTime testDateTime = DateTime.newInstanceGMT(2017, 1, 7, 2, 2, 3);
        System.assertEquals(  '3 2 ' +  testDateTime.hour() + ' ' + testDateTime.day() + ' 1 ? 2017', mmlib_Utils.generateCronTabStringFromDatetime( testDateTime ) );
    }

    @isTest
    static void testFormatSoqlDatetimeUTC()
    {
        Datetime userTimezoneDatetime = datetime.now();

        String testResult = mmlib_Utils.formatSoqlDatetimeUTC(userTimezoneDatetime);
    }

    @isTest
    static void testFormatSoqlDate()
    {
        Date userDate = Date.newInstance(2017, 9, 14);
        String testResult = mmlib_Utils.formatSoqlDate(userDate);
        System.assertEquals('2017-09-14', testResult);
    }

    @isTest
    static void testConvertValToTypel()
    {

        System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.Boolean, 'false'), false);
        System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.Boolean, 'true'), true);
        System.assertNotEquals(mmlib_Utils.convertValToType(Schema.SoapType.Boolean, 'false'), true);

        System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.Date, String.valueOf(date.today())), date.today());

        DateTime dt = datetime.now();
        // System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.DateTime, String.valueOf(dt)), dt);

        System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.Double, String.valueOf(0.003)), 0.003);

        System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.Integer, String.valueOf(3)), 3);

        System.assertEquals(mmlib_Utils.convertValToType(Schema.SoapType.String, 'foobar'), 'foobar');

        try
        {
            mmlib_Utils.convertValToType(null, 'false') ;

            System.assert(false, 'Unit test fails because no mmlib_Utils.UtilsArgumentException was thrown as expected.');
        }
        catch ( mmlib_Utils.UtilsArgumentException uae )
        {
            System.assert(true);
        }
        catch ( Exception e )
        {
            System.assert(false, 'Unit test fails because another exception was returned instead of mmlib_Utils.UtilsArgumentException ::: ' + e);
        }
    }

    @isTest
    static void testClean()
    {
        System.assertEquals( '>' + mmlib_Utils.clean( ' 223  3445       ') + '<' , '>223 3445<');
    }

    @isTest
    static void testClean_DigitsOnly()
    {
        System.assertEquals('42', mmlib_Utils.clean_DigitsOnly('abc4&%%^   2    '));
    }

    @isTest
    static void testStripPhoneNumber()
    {
        System.assertEquals( '>' + mmlib_Utils.stripPhoneNumber( '+64 (804) 555-1212') + '<' , '>648045551212<');
    }

    @isTest
    static void testOrgValueIsAvailable()
    {
        System.assert( mmlib_Utils.org != null );
    }

    @isTest
    static void testPossiblyNullStringsAreEqual()
    {
        String one = null;
        String two = null;
        System.assert(mmlib_Utils.possiblyNullStringsAreEqual(one, two));

        one = 'hello world';
        two = 'hello world';
        System.assert(mmlib_Utils.possiblyNullStringsAreEqual(one, two));

        one = 'hello world';
        two = 'Hello World';
        System.assert(mmlib_Utils.possiblyNullStringsAreEqual(one, two));

        one = null;
        two = 'Hello World';
        System.assert(!mmlib_Utils.possiblyNullStringsAreEqual(one, two));

        one = 'hello world';
        two = null;
        System.assert(!mmlib_Utils.possiblyNullStringsAreEqual(one, two));

        one = 'Bonjour le monde';
        two = 'hello world';
        System.assert(!mmlib_Utils.possiblyNullStringsAreEqual(one, two));
    }

    @isTest
    static void testdateTimesMatchWithinNSeconds()
    {
        DateTime one = DateTime.now();
        DateTime two = one.addSeconds(5);
        Integer secondRange = 2;
        System.assertEquals(false, mmlib_Utils.dateTimesMatchWithinNSeconds(one, two, secondRange), 'Times should not be within the range.');

        secondRange = 6;
        System.assertEquals(true, mmlib_Utils.dateTimesMatchWithinNSeconds(one, two, secondRange), 'Times should be within the range.');

        secondRange = 5;
        System.assertEquals(true, mmlib_Utils.dateTimesMatchWithinNSeconds(one, two, secondRange), 'Times should be within the range (edge case).');
    }

    @isTest
    static void getSelectOptionList()
    {
        List<SelectOption> optionList = mmlib_Utils.getSelectOptionList(Account.AccountSource);
        System.assert(!optionList.isEmpty(), '');
    }

    @isTest
    static void testGetSelectOptionsMethod()
    {
//        mmlib_Utils.getSelectOptions( new list<Account>{ mmlib_TestDataFactory.instance().mockAccount() }  );
//        mmlib_Utils.getSelectOptions( new list<Account>{ mmlib_TestDataFactory.instance().mockAccount() }, true  );

        mmlib_Utils.getSelectOptions( Account.SObjectType.getDescribe().getRecordTypeInfos() );
    }

    @isTest
    static void testExtractFieldNamesFromFieldset()
    {
        List<String> result = new List<String>();

        List<Fieldset> fieldsetList = Schema.SObjectType.Account.fieldSets.getMap().values();

        if (fieldsetList != null && !fieldsetList.isEmpty())
        {
            System.debug('Account has at least one fieldset.');
            result = mmlib_Utils.extractFieldNamesFromFieldset(fieldsetList.get(0));
            System.debug(result);
            System.assert(!result.isEmpty());
        }
    }
}