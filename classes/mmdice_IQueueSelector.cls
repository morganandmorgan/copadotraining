public interface mmdice_IQueueSelector extends mmlib_ISObjectSelector
{
	List<DICE_Queue__c> selectAll();
}