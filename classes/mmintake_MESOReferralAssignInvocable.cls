public class mmintake_MESOReferralAssignInvocable
{
    @InvocableMethod
    (
        label = 'MESO Referral Firm Assignment'
        description = 'Assigns the next MESO referral firm to the "MESO Firm Referred" field.'
    )
    public static void AssignNextMesoReferral(List<Intake__c> intakeList)
    {
        new mmintake_MESOFirmReferralAction().setRecordsToActOn(intakeList).run();
    }
}