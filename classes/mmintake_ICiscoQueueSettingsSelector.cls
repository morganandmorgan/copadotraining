public interface mmintake_ICiscoQueueSettingsSelector
{
	List<Cisco_Queue_Setting__mdt> selectAll();
    List<Cisco_Queue_Setting__mdt> selectByDnis(Set<String> dnisSet);
}