@isTest
private class mmcommon_UsersSelectorTest {

  @isTest
  private static void selectExistingUsers() {
    List<User> usersToQuery = new List<User>{
      TestUtil.createActiveInvestigator(),
      TestUtil.createInactiveInvestigator(),
      TestUtil.createUser()
    };

    List<User> otherUsers = new List<User>{
      TestUtil.createActiveInvestigator(),
      TestUtil.createInactiveInvestigator(),
      TestUtil.createUser()
    };

    List<User> toInsert = new List<User>();
    toInsert.addAll(usersToQuery);
    toInsert.addAll(otherUsers);
    Database.insert(toInsert);

    Set<Id> userIds = new Map<Id, User>(usersToQuery).keySet();

    Test.startTest();
    List<User> result = mmcommon_UsersSelector.newInstance().selectById(userIds);
    Test.stopTest();

    System.assertEquals(userIds, new Map<Id, User>(result).keySet());
  }

  @isTest
  private static void selectActiveInvestigators() {
    List<User> usersToQuery = new List<User>{
      TestUtil.createActiveInvestigator(),
      TestUtil.createActiveInvestigator()
    };

    List<User> otherUsers = new List<User>{
      TestUtil.createInactiveInvestigator(),
      TestUtil.createUser()
    };

    List<User> toInsert = new List<User>();
    toInsert.addAll(usersToQuery);
    toInsert.addAll(otherUsers);
    Database.insert(toInsert);

    Test.startTest();
    List<User> result = mmcommon_UsersSelector.newInstance().selectAllActiveInvestigators();
    Test.stopTest();

    Map<Id, User> testUsersMap = new Map<Id, User>(usersToQuery);
    Map<Id, User> resultMap = new Map<Id, User>(result);

    System.assert(resultMap.keySet().containsAll(testUsersMap.keySet()));

    for (User u : resultMap.values()) {
      System.assert(mmcommon_PersonUtils.isActive(u));
    }
  }
}