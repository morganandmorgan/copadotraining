/*============================================================================
Name            : ProcessDepositsController
Author          : CLD
Created Date    : July 2018
Description     : Test class for Batch Manager Page
=============================================================================*/
@isTest
public with sharing class ProcessDepositsController_TEST {

    public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;
    public static c2g__codaAccountingSettings__c accountingSettings;

    static void setupData(String typeOfDeposit)
    {
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        accountingSettings = new c2g__codaAccountingSettings__c();
        insert accountingSettings;

        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        c2g__codaCompany__c company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__CustomerSettlementDiscount__c = testGLA.Id;
        update company;
        List<c2g__codaBankAccount__c> bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        c2g__codaBankAccount__c costBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Cost');
        bankAccountList.add(costBankAccount);
        c2g__codaBankAccount__c trustBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Trust');
        bankAccountList.add(trustBankAccount);
        insert bankAccountList;

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        expenseType.General_Ledger_Account__c = testGLA.Id;
        INSERT expenseType;

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Internet';
        expenseType2.ExternalID__c = 'INTERNET1';
        expenseType.General_Ledger_Account__c = testGLA.Id;

        INSERT expenseType2;

        litify_pm__Expense__c testExpense = new litify_pm__Expense__c();

        testExpense.litify_pm__Matter__c = matter.Id;
        testExpense.litify_pm__Amount__c = 100.0;
        testExpense.litify_pm__Date__c = date.today();
        testExpense.litify_pm__ExpenseType2__c = expenseType.Id;
        INSERT testExpense;

        litify_pm__Expense__c testExpense2 = new litify_pm__Expense__c();

        testExpense2.litify_pm__Matter__c = matter.Id;
        testExpense2.litify_pm__Amount__c = 10.0;
        testExpense2.litify_pm__Date__c = date.today();
        testExpense2.litify_pm__ExpenseType2__c = expenseType2.Id;

        INSERT testExpense2;    

        testDeposit = new Deposit__c();
        testDeposit.RecordTypeId = Schema.SObjectType.Deposit__c.RecordTypeInfosByName.get(typeOfDeposit).RecordTypeId;
        testDeposit.Matter__c = matter.Id;
        testDeposit.Amount__c = 10.0;
        testDeposit.Check_Date__c = date.today();
        testDeposit.Source__c = 'Client';
        testDeposit.AssignedToMMBusiness__c = testAccounts[1].Id;
        testDeposit.Operating_Cash_Account__c = typeOfDeposit == 'Operating' || typeOfDeposit == 'Trust Payout - Fees' ? operatingBankAccount.Id : null;
        testDeposit.Trust_Account__c = typeOfDeposit == 'Trust Deposit' || typeOfDeposit == 'Trust Payout - Fees' || typeOfDeposit == 'Trust Payout - Costs' ? trustBankAccount.Id : null;
        testDeposit.Cost_Account__c = typeOfDeposit == 'Trust Payout - Costs' ? costBankAccount.Id : null;
        INSERT testDeposit;
        if(typeOfDeposit != 'Trust Payout - Costs'){
            Set<Id> depositIds = new Set<Id>{testDeposit.Id};
            MatterAllocationService.allocateItems_Deposit(depositIds);    
        }
        else{
            testDeposit.Deposit_Allocated__c = true;
            update testDeposit;
        }
        
    }

    /*--------------------------------------------------------------------
        START TEST METHODS
    --------------------------------------------------------------------*/
    @isTest static void testProcessDespositsController_Operating(){

        setupData('Operating');

        ProcessDepositsController ctlr = new ProcessDepositsController();
            
        ProcessDepositsController.QueryRequest qrequest = new ProcessDepositsController.QueryRequest();
        qrequest.matterRTName = 'All';
        qrequest.depositRTName = 'All';
        qrequest.checkDate = null;
        ProcessDepositsController.ActionResult fetchAR = ProcessDepositsController.fetchDepositLines(qrequest);

        ProcessDepositsController.JournalRequest jrequest = new ProcessDepositsController.JournalRequest();
        jrequest.journalDescription = '';
        jrequest.autoPostJournal = true;

        List<ProcessDepositsController.DepositWrapper> depositsToProcess = (List<ProcessDepositsController.DepositWrapper>)fetchAR.records;
        //List<ProcessDepositsController.DepositWrapper> depositsToProcess = new List<ProcessDepositsController.DepositWrapper>();
        //ProcessDepositsController.DepositWrapper dWrapper = new ProcessDepositsController.DepositWrapper();
        //dWrapper.depositId = testDeposit.Id;
        //depositsToProcess.add(dWrapper);

        ProcessDepositsController.ActionResult processAR = ProcessDepositsController.createJournalFromDeposits(depositsToProcess,jrequest);

    }

    @isTest static void testProcessDesposits_Batch(){

        setupData('Operating');
        testDeposit.Unallocated_Costs__c = 10;

        ProcessDepositsController ctlr = new ProcessDepositsController();
            
        ProcessDepositsController.QueryRequest qrequest = new ProcessDepositsController.QueryRequest();
        qrequest.matterRTName = 'All';
        qrequest.depositRTName = 'All';
        qrequest.checkDate = null;
        ProcessDepositsController.ActionResult fetchAR = ProcessDepositsController.fetchDepositLines(qrequest);

        ProcessDepositsController.JournalRequest jrequest = new ProcessDepositsController.JournalRequest();
        jrequest.journalDescription = '';
        jrequest.autoPostJournal = true;

        accountingSettings.Process_Deposits_Batch_Threshold__c = 0;
        update accountingSettings;

        List<ProcessDepositsController.DepositWrapper> depositsToProcess = (List<ProcessDepositsController.DepositWrapper>)fetchAR.records;
        //List<ProcessDepositsController.DepositWrapper> depositsToProcess = new List<ProcessDepositsController.DepositWrapper>();
        //ProcessDepositsController.DepositWrapper dWrapper = new ProcessDepositsController.DepositWrapper();
        //dWrapper.depositId = testDeposit.Id;
        //depositsToProcess.add(dWrapper);

        ProcessDepositsController.ActionResult processAR = ProcessDepositsController.createJournalFromDeposits(depositsToProcess,jrequest);

    }

    @isTest static void testProcessDespositsController_Trust(){

        setupData('Trust Deposit');

        ProcessDepositsController ctlr = new ProcessDepositsController();
        
        ProcessDepositsController.QueryRequest qrequest = new ProcessDepositsController.QueryRequest();
        qrequest.matterRTName = 'All';
        qrequest.depositRTName = 'All';
        qrequest.checkDate = null;
        ProcessDepositsController.ActionResult fetchAR = ProcessDepositsController.fetchDepositLines(qrequest);

        ProcessDepositsController.JournalRequest jrequest = new ProcessDepositsController.JournalRequest();
        jrequest.journalDescription = '';
        jrequest.autoPostJournal = false;

        List<ProcessDepositsController.DepositWrapper> depositsToProcess = (List<ProcessDepositsController.DepositWrapper>)fetchAR.records;

        //List<ProcessDepositsController.DepositWrapper> depositsToProcess = new List<ProcessDepositsController.DepositWrapper>();
        //ProcessDepositsController.DepositWrapper dWrapper = new ProcessDepositsController.DepositWrapper();
        //dWrapper.depositId = testDeposit.Id;
        //depositsToProcess.add(dWrapper);

        ProcessDepositsController.ActionResult processAR = ProcessDepositsController.createJournalFromDeposits(depositsToProcess,jrequest);

    }

    @isTest static void testProcessDespositsController_TrustFees(){

        setupData('Trust Payout - Fees');

        ProcessDepositsController ctlr = new ProcessDepositsController();
        
        ProcessDepositsController.QueryRequest qrequest = new ProcessDepositsController.QueryRequest();
        qrequest.matterRTName = 'All';
        qrequest.depositRTName = 'All';
        qrequest.checkDate = null;
        ProcessDepositsController.ActionResult fetchAR = ProcessDepositsController.fetchDepositLines(qrequest);

        ProcessDepositsController.JournalRequest jrequest = new ProcessDepositsController.JournalRequest();
        jrequest.journalDescription = '';
        jrequest.autoPostJournal = false;

        List<ProcessDepositsController.DepositWrapper> depositsToProcess = (List<ProcessDepositsController.DepositWrapper>)fetchAR.records;

        //List<ProcessDepositsController.DepositWrapper> depositsToProcess = new List<ProcessDepositsController.DepositWrapper>();
        //ProcessDepositsController.DepositWrapper dWrapper = new ProcessDepositsController.DepositWrapper();
        //dWrapper.depositId = testDeposit.Id;
        //depositsToProcess.add(dWrapper);

        ProcessDepositsController.ActionResult processAR = ProcessDepositsController.createJournalFromDeposits(depositsToProcess,jrequest);

    }

    @isTest static void testProcessDespositsController_TrustCosts(){

        setupData('Trust Payout - Costs');

        ProcessDepositsController ctlr = new ProcessDepositsController();
        
        ProcessDepositsController.QueryRequest qrequest = new ProcessDepositsController.QueryRequest();
        qrequest.matterRTName = 'All';
        qrequest.depositRTName = 'All';
        qrequest.checkDate = null;
        ProcessDepositsController.ActionResult fetchAR = ProcessDepositsController.fetchDepositLines(qrequest);
        
        ProcessDepositsController.JournalRequest jrequest = new ProcessDepositsController.JournalRequest();
        jrequest.journalDescription = '';
        jrequest.autoPostJournal = false;

        List<ProcessDepositsController.DepositWrapper> depositsToProcess = (List<ProcessDepositsController.DepositWrapper>)fetchAR.records;

        //List<ProcessDepositsController.DepositWrapper> depositsToProcess = new List<ProcessDepositsController.DepositWrapper>();
        //ProcessDepositsController.DepositWrapper dWrapper = new ProcessDepositsController.DepositWrapper();
        //dWrapper.depositId = testDeposit.Id;
        //depositsToProcess.add(dWrapper);

        ProcessDepositsController.ActionResult processAR = ProcessDepositsController.createJournalFromDeposits(depositsToProcess,jrequest);

        Try{
            List<Id> jnlIds = new List<Id>();
            for(c2g__codaJournal__c jnl : [Select id from c2g__codaJournal__c]){
                jnlIds.add(jnl.Id);
            }
            List<c2g__codaJournal__c> results = FFAUtilities.postJournals(jnlIds);
        }
        Catch(Exception e){
            
        }

    }

}