public class mmcommon_HolidaysSelector
    extends mmlib_SObjectSelector
    implements mmcommon_IHolidaysSelector
{
    public static mmcommon_IHolidaysSelector newInstance()
    {
        return (mmcommon_IHolidaysSelector) mm_Application.Selector.newInstance(Holiday.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Holiday.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
        };
    }

    public List<Holiday> selectById(Set<Id> idSet)
    {
        return (List<Holiday>) selectSObjectsById(idSet);
    }

    public List<Holiday> selectAllFuture()
    {
        return Database.query( newQueryFactory().setCondition('ActivityDate >= TODAY').toSOQL() );
    }

    public List<Holiday> selectAll()
    {
        return Database.query( newQueryFactory().toSOQL() );
    }
}