public abstract class mmlib_AbstractTestDataFactory
{
    protected mmlib_ISObjectUnitOfWork uow = null;
    protected boolean isMock = false;
    protected boolean isDataToBePersisted = false;
    protected boolean isConfigurationStillAccepted = true;
    protected SObject record;
    protected Map<Schema.SObjectField, Object> fieldsAndValues = new Map<Schema.SObjectField, Object>();

    protected void configAcceptanceChecker( String methodName )
    {
        if ( ! isConfigurationStillAccepted )
        {
            throw new FactoryException('Factory configuration method of ' + methodName + ' must be called prior to generate() method.');
        }
    }

    protected void setRecordType( string recordTypeName )
    {
        if ( string.isNotBlank( recordTypeName )
            && getSObjectType().getDescribe().getRecordTypeInfosByName().containsKey( recordTypeName.toLowercase() ) )
        {
            this.record.put( 'RecordTypeId', mmlib_RecordTypeUtils.getRecordTypeIDByDevName(getSObjectType(), recordTypeName) );
        }
    }

    abstract Schema.SObjectType getSObjectType();

    abstract void configurationDuringGeneration();

    public mmlib_AbstractTestDataFactory generate()
    {
        this.isConfigurationStillAccepted = false;

        this.record = getSObjectType().newSObject();

        this.configurationDuringGeneration();

        if ( ! this.fieldsAndValues.isEmpty() )
        {
            for ( Schema.SObjectField fieldKey : this.fieldsAndValues.keySet() )
            {
                this.record.put(fieldKey, this.fieldsAndValues.get(fieldKey));
            }
        }

        if ( this.isMock )
        {
            this.record.Id = fflib_IDGenerator.generate( getSObjectType() );
        }

        if ( this.isDataToBePersisted )
        {
            if ( this.uow == null )
            {
                database.insert( this.record );
            }
            else
            {
                this.uow.registerNew( this.record );
            }

        }

        return this;
    }

    public mmlib_AbstractTestDataFactory presetFields( Map<Schema.SObjectField, Object> fieldsAndValues )
    {
        configAcceptanceChecker('presetFields( Map<Schema.SObjectField, Object> )');

        if ( fieldsAndValues != null )
        {
            this.fieldsAndValues.putAll( fieldsAndValues );
        }

        return this;
    }

    public mmlib_AbstractTestDataFactory setUnitOfWork( mmlib_ISObjectUnitOfWork uow )
    {
        configAcceptanceChecker('setUnitOfWork( mmlib_ISObjectUnitOfWork )');

        this.uow = uow;

        return this;
    }

    public mmlib_AbstractTestDataFactory mock()
    {
        configAcceptanceChecker('mock()');

        this.isMock = true;

        return this;
    }

    public mmlib_AbstractTestDataFactory persist()
    {
        configAcceptanceChecker('persist()');

        this.isDataToBePersisted = true;

        return this;
    }

    public SObject getRecord()
    {
        if ( this.isConfigurationStillAccepted == true )
        {
            throw new FactoryException('Factory getRecord() method must be called after to generate() method.');
        }

        return this.record;
    }



    public class FactoryException extends Exception { }
}