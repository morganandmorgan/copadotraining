@isTest
private class ScheduledEventMergeControllerTest {
	
	@TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        Intake__c intake = new Intake__c(
                Client__c = client.Id,
                Incident__c = incident.Id
            );
        toInsert.add(intake);

        Database.insert(toInsert);
        toInsert.clear();

        Event investigationEvent = new Event(
                WhatId = intake.Id,
                Subject = 'Test Subject',
                StartDateTime = Datetime.now(),
                EndDateTime = Datetime.now().addHours(1),
                IsAllDayEvent = false
            );
        toInsert.add(investigationEvent);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Intake__c getIntake() {
        return [SELECT Id, Client__c FROM Intake__c];
    }

    private static Event getEvent() {
        return [SELECT Id FROM Event];
    }

    private static ScheduledEventMergeController buildController(Event relatedEvent) {
        ScheduledEventMergeController controller = new ScheduledEventMergeController();
        controller.relatedEventId = relatedEvent.Id;
        return controller;
    }

    @isTest
    private static void testInit() {
        Intake__c relatedIntake = getIntake();
        Event relatedEvent = getEvent();

        Test.startTest();
        ScheduledEventMergeController controller = buildController(relatedEvent);
        Test.stopTest();

        System.assertEquals(relatedIntake.Id, controller.thisIntake.Id);
        System.assertEquals(relatedIntake.Id, controller.relatedIntakeId);
        System.assertEquals(relatedEvent.Id, controller.thisEvent.Id);
        System.assertEquals(relatedIntake.Client__c, controller.thisAccount.Id);
        System.assertEquals(relatedIntake.Client__c, controller.relatedAccountId);
    }
}