/**
 *  mmlib_ISObjectUnitOfWork
 */
public interface mmlib_ISObjectUnitOfWork
    extends fflib_ISObjectUnitOfWork
{
    list<SObject> getNewRecordsByType( Schema.SObjectType typeToFind );
    Map<Id, SObject> getDirtyRecordsByType( Schema.SObjectType typeToFind );
    Map<Id, SObject> getDeletedRecordsByType( Schema.SObjectType typeToFind );

    /**
     * Register a SObject instance to be either inserted or updated when commitWork is called
     *
     * @param record A SObject instance to be inserted or updated during commitWork
     **/
    void register(SObject record);

    /**
     * Register a list of SObject instances to be either inserted or updated when commitWork is called
     *
     * @param records A list SObject instances to be either inserted or updated during commitWork
     **/
    void register(List<SObject> records);

    void register(SObject record, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord);

    /**
     * Registers a group of emails to be sent during the commitWork
     **/
    void registerEmails( list<Messaging.Email> emails );

}