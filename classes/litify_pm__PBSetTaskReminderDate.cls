/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBSetTaskReminderDate {
    global PBSetTaskReminderDate() {

    }
    @InvocableMethod(label='Set Task Reminder Date' description='Manually sets the tasks' reminder dates to the dates given.')
    global static void setTaskReminderDate(List<litify_pm.PBSetTaskReminderDate.PBSetTaskReminderDateWrapper> input) {

    }
global class PBSetTaskReminderDateWrapper {
    @InvocableVariable( required=false)
    global Task inputTask;
    @InvocableVariable( required=false)
    global Datetime reminderDate;
    global PBSetTaskReminderDateWrapper() {

    }
}
}
