/*============================================================================
Name            : ClosingPayableInvoiceControllerTest
Author          : CLD
Created Date    : June 2019
Description     : Test class for closing pin creation
=============================================================================*/
@isTest
private class ClosingPayableInvoiceControllerTest {

    private static litify_pm__Matter__c matter;
	private static c2g__codaCompany__c company;
	private static List<Account> testAccounts;
	private static Finance_Request__c testFinanceRequest;

	static void setupData()
    {
        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');
		Id mmOfficeAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Office_Locations');

        testAccounts = new List<Account>();

        Account vendorAccount = new Account();
        vendorAccount.Name = 'TEST CONTACT';
        vendorAccount.litify_pm__First_Name__c = 'TEST';
        vendorAccount.litify_pm__Last_Name__c = 'CONTACT';
		vendorAccount.c2g__CODATaxCalculationMethod__c = 'Gross';
        vendorAccount.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(vendorAccount);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY SOURCE';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.c2g__CODATaxCalculationMethod__c = 'Gross';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

    	/*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        // c2g__codaBankAccount__c operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        // operatingBankAccount.Bank_Account_Type__c = 'Operating';
        // update operatingBankAccount;
		
        c2g__codaGeneralLedgerAccount__c testGLA = TestDataFactory_FFA.balanceSheetGLAs[0];

        // c2g__codaAccountingSettings__c acctSettings = new c2g__codaAccountingSettings__c(
        //     Client_Cost_Receivable_GLA_Code__c = testGLA.Id
        // );
        // insert acctSettings;`

		company = TestDataFactory_FFA.createCompany('company');
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__IntercompanyAccount__c = testAccounts[1].Id;
        update company;

        //update the accounts
		testAccounts[0].c2g__CODAAccountsPayableControl__c = testGLA.Id;
		testAccounts[0].c2g__CODAAccountTradingCurrency__c = 'USD';

        testAccounts[1].FFA_Company__c = company.Id;
        testAccounts[1].Vendor_Verification__c = 'Verified';
        update testAccounts;

       
		/*--------------------------------------------------------------------
        LITIFY Data Setup
        --------------------------------------------------------------------*/

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';
        INSERT matter;

		testFinanceRequest = new Finance_Request__c();
        testFinanceRequest.RecordTypeId = Schema.SObjectType.Finance_Request__c.RecordTypeInfosByName.get('Closing Statement Request').RecordTypeId;
        testFinanceRequest.Matter__c = matter.Id;
        testFinanceRequest.Amount_of_Settlement__c = 100.0;
        INSERT testFinanceRequest;

		litify_pm__Role__c role = new litify_pm__Role__c();
        role.litify_pm__Matter__c = matter.Id;
        role.litify_pm__Party__c = testAccounts[0].Id;
        INSERT role;
		
		litify_pm__Damage__c damage = new litify_pm__Damage__c();
        damage.litify_pm__Matter__c = matter.Id;
		damage.litify_pm__Amount_Billed__c = 100;
		damage.litify_pm__Provider__c = role.Id;
        INSERT damage;

		Lien_LOP__c lien = new Lien_LOP__c();
		lien.Matter__c = matter.Id;
		lien.Account_Role__c = role.Id;
		lien.Lien_Amount__c = 100;
        INSERT lien;
    }

    //================= TEST METHODS ====================

	@isTest static void test_Controller() {
		setupData();

		Test.startTest();
			ClosingPayableInvoiceController controller = new ClosingPayableInvoiceController(new ApexPages.StandardController(matter));
			Map<String, Object> result1 = ClosingPayableInvoiceController.fetchMatterDetailsAndValidate(String.valueOf(matter.Id));

			ClosingPayableInvoiceController.ActionResult ar1 = ClosingPayableInvoiceController.fetchEligibleTranscations(String.valueOf(matter.Id));
			List<ClosingPayableInvoiceController.ClosingTransactionWrapper> wrapperList = new List<ClosingPayableInvoiceController.ClosingTransactionWrapper>();
			wrapperList = (List<ClosingPayableInvoiceController.ClosingTransactionWrapper>)ar1.records;

			ClosingPayableInvoiceController.PayableInvoiceRequest req = new ClosingPayableInvoiceController.PayableInvoiceRequest();
			req.pinDate = Date.today();
            req.pinDueDate = Date.today();
			req.autoPostInvoice = true;
			req.wrappersToProcess = wrapperList;

			litify_pm__Matter__c testMatter = ClosingPayableInvoiceController.fetchMatterDetails(matter.Id);

			Map<String, Object> result2 = ClosingPayableInvoiceController.createPayableInvoices(req, testMatter);

            ClosingPayableInvoiceController.ActionResult ar = ClosingPayableInvoiceController.searchForAccounts('MM COMPA');
            SYstem.assert(ar.records.size() > 0, 'Accounts not returned as anticipated!');

		Test.stopTest();
	}

}