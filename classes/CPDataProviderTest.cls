@isTest 
public class CPDataProviderTest
{
    public class MockHttpRequests implements HttpCalloutMock
    {
        public HttpResponse respond(HttpRequest request)
        {
            String endpoint = request.getEndpoint();

            if (endpoint == 'callout:CPDataProvider/oauth/token')
            {
                System.assertEquals('POST', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'text/plain');
                response.setBody('valid-token');
                response.setStatusCode(200);

                return response;
            }
            // GET Fin Info GOOD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/casefininfo?office=ORL&matter=9828224')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');

                string jsonStr = '{"caseExpenseData": {"caseExpenses": [{"effDate": "3/6/2020 12:00:00 AM"' +
                    ',"checkNo": "","invoice": "","bank": "Orl - Operating Account","costCode": "COSTS",' +
                    '"payee": "[Soft Cost Recovered]","comment": "","amount": -9.5,"index": 1}],' +
                    '"totalCosts": 9.5,"totalDeposits": -9.5,"totalPayables": 0,"balance": 0},"caseTrustData":' +
                    ' {"caseTrusts": [{"effDate": "3/2/2020 12:00:00 AM","checkNo": "685527","invoice": ' +
                    '"adj 2.29.2020 4.csv","bank": "Orl - Trust (Active)","costCode": "FEE","payee": "Morgan ' +
                    '& Morgan, P.A. [Firm - Fees]","comment": "adj F","amount": 3333.33,"index": 4}],"totalTrust":' +
                    ' -50  },"caseTrustSummary": {"expensesToDate": 9.5,"outstandingExpenses": 0,"feesBilled"' +
                    ': "","feesReceived": "","balance": 0,"trustBalance": -50}}';

                response.setBody(jsonStr);
                response.setStatusCode(200);

                return response;
            }
            // GET Fin Info BAD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/casefininfo?office=BAD&matter=BAD')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{"caseExpenseData":null,"caseTrustData":null,"caseTrustSummary":null}');
                response.setStatusCode(200);

                return response;
            }
            // GET Mail Data GOOD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/maildata?office=ORL&matter=9828224')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');

                string jsonStr = '{"parties": [{"name": "Theresa . Williams","address": "4528 Eden Woods CIR","city": ' +
                    '"Orlando","state": "FL","zip": "32810","role": "Plaintiff"}],"documents": ' +
                    '[{"documentId": "ORL-29314664","docNo": 48,"createUser": "ANDREAA","dateSent": ' +
                    '"2/26/2020 11:07:44 AM","from": "(Caccippio, Ronald)","to": "Andrea McKeon x5240",' +
                    '"re": "DEF AFFIDAVITS.pdf","maintUser": "ANDREAA","maintDate": "2/26/2020 11:08:06 AM",' +
                    '"docType": "Correspondence","subType": "","filePath": "some-fake-file-path"}]}';

                response.setBody(jsonStr);
                response.setStatusCode(200);

                return response;
            }
            // GET Mail Data BAD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/maildata?office=BAD&matter=BAD')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{"parties":[],"documents":[]}');
                response.setStatusCode(200);

                return response;
            }
            // POST Check Request GOOD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/checkrequest')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('POST', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setStatusCode(200);

                return response;                
            }
            // GET Check Request Case Data GOOD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/checkrequest/casedata?office=ORL&matter=9880872')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');

                String jsonStr = '{"approvedBy": "Christopher J. Hinckley","approvedByAD": "CHinckley@forthepeople.com"' +
                    ',"transactionCodes": [{"tranCodeSk": 2097,"code": "ABC","description": "*DO NOT USE*"}]}';
                
                response.setBody(jsonStr);
                response.setStatusCode(200);

                return response;
            }
            // GET Check Request Case Data BAD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/checkrequest/casedata?office=BAD&matter=BAD')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setStatusCode(204);    // No Content

                return response;
            }
            // GET Check Request Case Vendors GOOD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/checkrequest/vendors?office=ORL&term=abc')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('[{"legalEntitySk": 747195,"companyName": "ABC Bus Inc","street": "1506 30th Street NW","city": "Faribult","state": "MN","zip": "55021"}]');
                response.setStatusCode(200);

                return response;
            }
            // GET Check Request Case Vendors BAD
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/checkrequest/vendors?office=BAD&term=BAD')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/json');
                response.setBody('[]');
                response.setStatusCode(200);

                return response;
            }
            else
            {
                System.assert(false, 'Unexpected Endpoint \'' + endpoint + '\'');
                return null;
            }
        }
    }

    @isTest
    public static void TestGetMatterInfo()
    {
        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9828224', litify_pm__Client__c = acct.Id, CP_Office__c = 'ORL');
        insert matter;

        Test.startTest();

        litify_pm__Matter__c result = CPDataProvider.GetMatterInfo(matter.Id);
        System.assertEquals(result.ReferenceNumber__c, matter.ReferenceNumber__c);
        System.assertEquals(result.CP_Office__c, matter.CP_Office__c);

        Test.stopTest();
    }

    @isTest
    public static void TestGetMatterInfoInvalidRecordId()
    {
        Test.startTest();

        litify_pm__Matter__c result = CPDataProvider.GetMatterInfo('Invalid');
        System.assertEquals(result, null);

        Test.stopTest();
    }

    @isTest
    public static void TestGetUserInfo()
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userAcct = new User(Alias = 'ftuser', Email='ftuser@forthepeople.com', 
            EmailEncodingKey='UTF-8', FirstName = 'Fake Test', LastName='User', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/New_York', UserName='ftuser@forthepeople.com');
        insert userAcct;

        Test.startTest();

        User result = CPDataProvider.GetUserInfo(userAcct.Id);
        System.assertEquals(result.Username, userAcct.Username);

        Test.stopTest();
    }

    @isTest
    public static void TestGetUserInfoInvalidRecordId()
    {
        Test.startTest();

        User result = CPDataProvider.GetUserInfo('Invalid');
        System.assertEquals(result, null);

        Test.stopTest();
    }

    @isTest
    public static void TestGetToken()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());
        System.assertEquals('valid-token', CPDataProvider.GetAccessToken());
    }

    @isTest
    public static void TestGetFinInfo()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        // NEED TO BE ABLE TO ADD OFFICE
        //Account location = new Account(Name = 'Orlando', OfficeLocationCode__c = 'ORL', RecordTypeId = '0121J000000o4awQAA', Type = 'MM Office');
        //insert location;

        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9828224', litify_pm__Client__c = acct.Id, CP_Office__c = 'ORL');
        insert matter;
        
        Test.startTest();

        Map<String, Object> finInfo = (Map<String, Object>)JSON.deserializeUntyped(CPDataProvider.GetFinInfo(matter.Id));
        System.assertNotEquals(null, finInfo);
        System.assertNotEquals(null, finInfo.get('caseExpenseData'));
        System.assertNotEquals(null, finInfo.get('caseTrustData'));
        System.assertNotEquals(null, finInfo.get('caseTrustSummary'));

        Map<String, Object> caseExpenseData = (Map<String, Object>)finInfo.get('caseExpenseData');
        System.assertNotEquals(null, caseExpenseData.get('caseExpenses'));
        System.assertEquals(9.5, caseExpenseData.get('totalCosts'));
        System.assertEquals(-9.5, caseExpenseData.get('totalDeposits'));
        System.assertEquals(0, caseExpenseData.get('totalPayables'));
        System.assertEquals(0, caseExpenseData.get('balance'));

        List<Object> caseExpenses = (List<Object>)caseExpenseData.get('caseExpenses');
        Map<String, Object> caseExpense = (Map<String, Object>)caseExpenses[0];
        System.assertEquals('3/6/2020 12:00:00 AM', caseExpense.get('effDate'));
        System.assertEquals('Orl - Operating Account', caseExpense.get('bank'));
        System.assertEquals('COSTS', caseExpense.get('costCode'));
        System.assertEquals('[Soft Cost Recovered]', caseExpense.get('payee'));
        System.assertEquals(-9.5, caseExpense.get('amount'));

        Map<String, Object> caseTrustData = (Map<String, Object>)finInfo.get('caseTrustData');
        System.assertNotEquals(null, caseTrustData.get('caseTrusts'));
        System.assertEquals(-50, caseTrustData.get('totalTrust'));

        List<Object> caseTrusts = (List<Object>)caseTrustData.get('caseTrusts');
        Map<String, Object> caseTrust = (Map<String, Object>)caseTrusts[0];
        System.assertEquals('685527', caseTrust.get('checkNo'));
        System.assertEquals('adj 2.29.2020 4.csv', caseTrust.get('invoice'));
        System.assertEquals('adj F', caseTrust.get('comment'));
        System.assertEquals(1, caseExpense.get('index'));

        Map<String, Object> caseTrustSummary = (Map<String, Object>)finInfo.get('caseTrustSummary');
        System.assertEquals(9.5, caseTrustSummary.get('expensesToDate'));
        System.assertEquals(0, caseTrustSummary.get('outstandingExpenses'));
        System.assertEquals('', caseTrustSummary.get('feesBilled'));
        System.assertEquals('', caseTrustSummary.get('feesReceived'));
        System.assertEquals(0, caseTrustSummary.get('balance'));
        System.assertEquals(-50, caseTrustSummary.get('trustBalance'));

        Test.stopTest();
    }

    @isTest 
    public static void TestGetFinInfoInvalidMatter()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Test.startTest();

        System.assert(CPDataProvider.GetFinInfo('Invalid') == '');

        Test.stopTest();
    }

    @isTest
    public static void TestGetFinInfoBadParameters()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Bad Boy');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = 'BAD', litify_pm__Client__c = acct.Id, CP_Office__c = 'BAD');
        insert matter;
        
        Test.startTest();

        Map<String, Object> finInfo = (Map<String, Object>)JSON.deserializeUntyped(CPDataProvider.GetFinInfo(matter.Id));

        System.assertEquals(null, finInfo.get('caseExpenseData'));
        System.assertEquals(null, finInfo.get('caseTrustData'));
        System.assertEquals(null, finInfo.get('caseTrustSummary'));

        Test.stopTest();
    }

    /*
        Check Request Tests
    */
    @isTest
    public static void TestSendCheckReq()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        String request = '{"Office": "ORL","Matter": "9880872","CaseName": "Lindsay, Denna v. Morgan, Donald","RequestedBy": ' +
            '"Ricardo Tejada","RequestedByAD": "rtejada@forthepeople.com.rtejada","Amount": 123456789,"Due": "2020-05-27",' +
            '"ApprovedBy": "Christopher J. Hinckley","ApprovedByAD": "rtejada@forthepeople.com","CaseVendor": {"LegalEntitySk": ' +
            '781002,"CompanyName": "ABC Imaging of Washington, Inc.","Street": "1155 21st Street NW, Suite M400","City": ' +
            '"Washington","State": "DC","Zip": "20036"},"Memo": "This is a memo","Stub": "This is a stub","Invoice": ' +
            '"FAKE-123456","Comment": "Wow that is a big number! On just a phone call?","TransCodeRecord": {"TransCodeSk": 2061,' +
            '"Code": "CEL_INV   ","Description": "Cellular Phone Call By Investigator"}}';

        Test.startTest();

        System.assert(CPDataProvider.SendCheckReq(request));

        Test.stopTest();
    }

    @isTest
    public static void TestGetCheckReqData()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9880872', litify_pm__Client__c = acct.Id, CP_Office__c = 'ORL');
        insert matter;

        Test.startTest();

        Map<String, Object> checkReqData = (Map<String, Object>)JSON.deserializeUntyped(CPDataProvider.GetCheckReqData(matter.Id));
        System.assertNotEquals(null, checkReqData);
        System.assertEquals('Christopher J. Hinckley', checkReqData.get('approvedBy'));
        System.assertEquals('CHinckley@forthepeople.com', checkReqData.get('approvedByAD'));
        System.assertNotEquals(null, checkReqData.get('transactionCodes'));

        List<Object> transCodes = (List<Object>)checkReqData.get('transactionCodes');

        Map<String, Object> transCode = (Map<String, Object>)transCodes[0];
        System.assertEquals(2097, transCode.get('tranCodeSk'));
        System.assertEquals('ABC', transCode.get('code'));
        System.assertEquals('*DO NOT USE*', transCode.get('description'));

        Test.stopTest();
    }

    @isTest
    public static void TestGetCheckReqDataInvalidMatter()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Test.startTest();

        System.assert(CPDataProvider.GetCheckReqData('Invalid') == '');

        Test.stopTest();
    }

    @isTest
    public static void TestGetCheckReqDataBadParameters()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Bad Boy');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = 'BAD', litify_pm__Client__c = acct.Id, CP_Office__c = 'BAD');
        insert matter;

        Test.startTest();

        System.assert(CPDataProvider.GetCheckReqData(matter.Id) == '');

        Test.stopTest();
    }

    @isTest
    public static void TestFindVendors()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9880872', litify_pm__Client__c = acct.Id, CP_Office__c = 'ORL');
        insert matter;

        Test.startTest();

        List<Object> caseVendors = (List<Object>)JSON.deserializeUntyped(CPDataProvider.FindVendors(matter.Id, 'abc'));
        System.assertNotEquals(null, caseVendors);

        Map<String, Object> vendor = (Map<String, Object>)caseVendors[0];
        System.assertEquals(747195, vendor.get('legalEntitySk'));
        System.assertEquals('ABC Bus Inc', vendor.get('companyName'));
        System.assertEquals('1506 30th Street NW', vendor.get('street'));
        System.assertEquals('Faribult', vendor.get('city'));
        System.assertEquals('MN', vendor.get('state'));
        System.assertEquals('55021', vendor.get('zip'));

        Test.stopTest();
    }

    @isTest
    public static void TestFindVendorsInvalidMatter()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Test.startTest();

        System.assert(CPDataProvider.FindVendors('Invalid', 'BAD') == '');

        Test.stopTest();
    }

    @isTest
    public static void TestFindVendorBadParameters()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Bad Boy');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = 'BAD', litify_pm__Client__c = acct.Id, CP_Office__c = 'BAD');
        insert matter;

        Test.startTest();

        System.assert(CPDataProvider.FindVendors(matter.Id, 'BAD') == '[]');

        Test.stopTest();
    }

    /*
    Unit tests for MailData
    */
    @isTest
    public static void TestGetMailData()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        //Account location = new Account(Name = 'Orlando', OfficeLocationCode__c = 'ORL');
        //insert location;
        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9828224', litify_pm__Client__c = acct.Id, CP_Office__c = 'ORL');
        insert matter;
        
        Test.startTest();

        Map<String, Object> mailData = (Map<String, Object>)JSON.deserializeUntyped(CPDataProvider.GetMailData(matter.Id));
        System.assertNotEquals(null, mailData);
        System.assertNotEquals(null, mailData.get('parties'));
        System.assertNotEquals(null, mailData.get('documents'));

        List<Object> parties = (List<Object>)mailData.get('parties');
        Map<String, Object> party = (Map<String, Object>)parties[0];
        System.assertNotEquals(null, party);
        System.debug((String)party.get('name'));
        System.assertEquals('Theresa . Williams', party.get('name'));
        System.assertEquals('4528 Eden Woods CIR', party.get('address'));
        System.assertEquals('Orlando', party.get('city'));
        System.assertEquals('FL', party.get('state'));
        System.assertEquals('32810', party.get('zip'));
        System.assertEquals('Plaintiff', party.get('role'));

        List<Object> documents = (List<Object>)mailData.get('documents');
        Map<String, Object> document = (Map<String, Object>)documents[0];
        System.assertNotEquals(null, document);
        System.assertEquals('ORL-29314664', document.get('documentId'));
        System.assertEquals(48, document.get('docNo'));
        System.assertEquals('ANDREAA', document.get('createUser'));
        System.assertEquals('2/26/2020 11:07:44 AM', document.get('dateSent'));
        System.assertEquals('(Caccippio, Ronald)', document.get('from'));
        System.assertEquals('Andrea McKeon x5240', document.get('to'));
        System.assertEquals('DEF AFFIDAVITS.pdf', document.get('re'));
        System.assertEquals('ANDREAA', document.get('maintUser'));
        System.assertEquals('2/26/2020 11:08:06 AM', document.get('maintDate'));
        System.assertEquals('Correspondence', document.get('docType'));
        System.assertEquals('', document.get('subType'));
        System.assertEquals('some-fake-file-path', document.get('filePath'));

        Test.stopTest();
    }

    @isTest 
    public static void TestGetMailDataInvalidMatter()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Test.startTest();
        
        System.assert(CPDataProvider.GetMailData('Invalid') == '');
        
        Test.stopTest();
    }

    @isTest
    public static void TestGetMailDataBadParameters()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Bad Boy');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = 'BAD', litify_pm__Client__c = acct.Id, CP_Office__c = 'BAD');
        insert matter;
        
        Test.startTest();

        System.assertEquals('{"parties":[],"documents":[]}', CPDataProvider.GetMailData(matter.Id));

        Test.stopTest();
    }
}