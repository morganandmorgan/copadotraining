/**
 *  mmcommon_PersonAccountsServiceTest
 */
@isTest
private class mmcommon_PersonAccountsServiceTest
{
    private static final Id RECORDTYPEID_PERSONACCOUNT = RecordTypeUtil.getRecordTypeIDByDevName('Account', 'Person_Account');

    @TestSetup
    private static void setup() {
        Account person1 = createPersonAccount();
        person1.FirstName = 'Jeremy';
        person1.LastName = 'Pickles';
        person1.PersonEmail = 'jpickles@apextest.com';
        person1.Phone = '4567890190';
        person1.Date_of_Birth_mm__c = Date.newInstance(1980, 3, 15);
        person1.Gender__c = 'Male';

        Account person2 = createPersonAccount();
        person2.FirstName = 'Jeremiah';
        person2.LastName = 'Sampleperson';
        person2.PersonEmail = 'jersamper@apextest.com';
        person2.Phone = '4256760295';
        person2.Date_of_Birth_mm__c = Date.newInstance(1985, 1, 24);
        person2.Gender__c = 'Male';

        Account person3 = createPersonAccount();
        person3.FirstName = 'Angeline';
        person3.LastName = 'Persontest';
        person3.PersonEmail = 'angiep@apextest.com';
        person3.Phone = '0000000000';
        person3.Date_of_Birth_mm__c = Date.newInstance(1983, 6, 7);
        person3.Gender__c = 'Female';

        Account person4 = createPersonAccount();
        person4.FirstName = 'Caroline';
        person4.LastName = 'Bradford';
        person4.PersonEmail = 'carbrad@apextest.com';
        person4.Phone = '4567378210';
        person4.Date_of_Birth_mm__c = Date.newInstance(1980, 3, 15);
        person4.Gender__c = 'Female';

        Account person5 = createPersonAccount();
        person5.FirstName = 'Brad';
        person5.LastName = 'Theman';
        person5.PersonEmail = 'bradtheman@apextest.com';
        person5.Phone = '1111111111';
        person5.Date_of_Birth_mm__c = Date.newInstance(1983, 6, 7);
        person5.Gender__c = 'Male';

        Account personWithMissingData = createPersonAccount();
        personWithMissingData.FirstName = null;
        personWithMissingData.LastName = 'Theman';
        personWithMissingData.PersonEmail = null;
        personWithMissingData.Phone = null;
        personWithMissingData.Date_of_Birth_mm__c = null;
        personWithMissingData.Gender__c = null;

        List<Account> testPersonAccounts = new List<Account>{
            person1,
            person2,
            person3,
            person4,
            person5,
            personWithMissingData
        };
        Database.insert(testPersonAccounts);
    }

    private static Account createPersonAccount() {
        return new Account(RecordTypeId = RECORDTYPEID_PERSONACCOUNT);
    }

    @isTest
    private static void searchAccounts_FirstNameLikeMatch() {
        String firstNameArg = 'jerem';
        String lastNameArg = '';
        String emailArg = '';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(2, result.size());

        Account result1 = result.get(0);
        Account result2 = result.get(1);

        System.assertEquals('Jeremy', result1.FirstName);
        System.assertEquals('Pickles', result1.LastName);

        System.assertEquals('Jeremiah', result2.FirstName);
        System.assertEquals('Sampleperson', result2.LastName);
    }

    @isTest
    private static void searchAccounts_FirstNameNotSpecified() {
        String firstNameArg = '';
        String lastNameArg = '';
        String emailArg = '';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(0, result.size());
    }

    @isTest
    private static void searchAccounts_LastNameLikeMatch() {
        String firstNameArg = '';
        String lastNameArg = 'perso';
        String emailArg = '';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(1, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Angeline', result1.FirstName);
        System.assertEquals('Persontest', result1.LastName);
    }

    @isTest
    private static void searchAccounts_LastNameNotSpecified() {
        String firstNameArg = '';
        String lastNameArg = null;
        String emailArg = '';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(0, result.size());
    }

    @isTest
    private static void searchAccounts_ExactMatchArguments() {
        String firstNameArg = '';
        String lastNameArg = '';
        String emailArg = 'bradtheman@apextest.com';
        String phoneNumArg = '1111111111';
        Date birthDateArg = Date.newInstance(1983, 6, 7);

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        // two results will be found
        System.assertEquals(2, result.size());

        // the first one should be the one we were looking for
        Account result1 = result.get(0);

        System.assertEquals('Brad', result1.FirstName);
        System.assertEquals('Theman', result1.LastName);
    }

    @isTest
    private static void searchAccounts_MissingExactMatchArguments() {
        String firstNameArg = '';
        String lastNameArg = '';
        String emailArg = 'bradtheman@apextest.com';
        String phoneNumArg = '';
        Date birthDateArg = Date.newInstance(1983, 6, 7);

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        // two results will be found
        System.assertEquals(2, result.size());

        // the first one should be the one we were looking for
        Account result1 = result.get(0);

        System.assertEquals('Brad', result1.FirstName);
        System.assertEquals('Theman', result1.LastName);
    }

    @isTest
    private static void searchAccounts_PhoneSearch_Unformatted() {
        String firstNameArg = '';
        String lastNameArg = '';
        String emailArg = '';
        String phoneNumArg = ' (456)  737-8210  ';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(1, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Caroline', result1.FirstName);
        System.assertEquals('Bradford', result1.LastName);
    }

    @isTest
    private static void searchAccount_PhoneSpecified_NullIncluded() {
        String firstNameArg = '';
        String lastNameArg = 'Theman';
        String emailArg = '';
        String phoneNumArg = '1111111111';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(2, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Brad', result1.FirstName);
        System.assertEquals('Theman', result1.LastName);
        System.assertEquals(phoneNumArg, result1.Phone);

        Account result2 = result.get(1);

        System.assertEquals(null, result2.FirstName);
        System.assertEquals('Theman', result2.LastName);
        System.assertEquals(null, result2.Phone);
    }

    @isTest
    private static void searchAccount_PhoneSpecified_NullExcluded() {
        String firstNameArg = '';
        String lastNameArg = '';
        String emailArg = '';
        String phoneNumArg = '1111111111';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(1, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Brad', result1.FirstName);
        System.assertEquals('Theman', result1.LastName);
        System.assertEquals(phoneNumArg, result1.Phone);
    }

    @isTest
    private static void searchAccount_CleanFirstName() {
        String firstNameArg = ' \t    jeremiah   \r\n   ';
        String lastNameArg = '';
        String emailArg = '';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(1, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Jeremiah', result1.FirstName);
        System.assertEquals('Sampleperson', result1.LastName);
    }

    @isTest
    private static void searchAccount_CleanLastName() {
        String firstNameArg = '';
        String lastNameArg = '  sampleperson \t  ';
        String emailArg = '';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(1, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Jeremiah', result1.FirstName);
        System.assertEquals('Sampleperson', result1.LastName);
    }

    @isTest
    private static void searchAccount_CleanEmail() {
        String firstNameArg = '';
        String lastNameArg = '';
        String emailArg = ' \r\njersamper@apextest.com   ';
        String phoneNumArg = '';
        Date birthDateArg = null;

        Test.startTest();
        List<Account> result = mmcommon_PersonAccountsService.searchAccounts(firstNameArg, lastNameArg, emailArg, phoneNumArg, birthDateArg);
        Test.stopTest();

        System.assertEquals(1, result.size());

        Account result1 = result.get(0);

        System.assertEquals('Jeremiah', result1.FirstName);
        System.assertEquals('Sampleperson', result1.LastName);
    }

    @isTest
    private static void geocodeAccounts()
    {

        mockPersonAccountService mockService = new mockPersonAccountService();
        mm_Application.Service.setMock(mmcommon_IPersonAccountsService.class, mockService);

        List<Account> accountList =
            createAndChangeAccounts(
                new AccountDataContainer(
                    'SomeRandomLastName',
                    null,
                    null,
                    null,
                    null,
                    null),
                new AccountDataContainer(
                    null,
                    '1600 Amphitheatre Pkwy.',
                    'Mountain View',
                    'CA',
                    null,
                    'US'),
                new AccountDataContainer(
                    'SomeRandomLastName',
                    null,
                    null,
                    null,
                    null,
                    null),
                new AccountDataContainer(
                    'ADifferentLastName',
                    null,
                    null,
                    null,
                    null,
                    null)
            );

        System.debug(accountList);

        List<Id> results = new List<Id>(mockService.resultsFromDetermination);
        System.debug(results);

        // System.assertEquals(1, results.size(), 'Only one Account should be processed.');
        // System.assertEquals(accountList.get(0).Id, results.get(0), 'The anticipated Account was not processed.');
    }

    private static List<Account> createAndChangeAccounts(
        AccountDataContainer acct1_insertData,
        AccountDataContainer acct1_updateData,
        AccountDataContainer acct2_insertData,
        AccountDataContainer acct2_updateData
    )
	{
		Id personAccountRecordType = [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;

		Account acct1 = new Account();
		acct1.RecordTypeID = personAccountRecordType;
		acct1.FirstName = 'Test FName';
        acct1.PersonEmail = 'test@forthepeople.com';
        acct1.PersonHomePhone = '1234567';
        acct1.PersonMobilePhone = '12345678';
        acct1.External_ID__c = mmlib_Utils.generateGuid();
        assignDataToAccount(acct1, acct1_insertData);

        Account acct2 = new Account();
		acct2.RecordTypeID = personAccountRecordType;
		acct2.FirstName = 'Test FName';
        acct2.PersonEmail = 'test@forthepeople.com';
        acct2.PersonHomePhone = '1234567';
        acct2.PersonMobilePhone = '12345678';
        acct2.External_ID__c = mmlib_Utils.generateGuid();
        assignDataToAccount(acct2, acct2_insertData);

        insert new List<Account> {acct1, acct2};

        Set<Id> idSet = new Set<Id>();
        idSet.add(acct1.Id);
        idSet.add(acct2.Id);

        List<Account> accountList = [select Id, PersonMailingAddress from Account where Id in :idSet];
        System.debug('accountList ins:\n' + accountList);

        assignDataToAccount(acct1, acct1_updateData);

        assignDataToAccount(acct2, acct2_updateData);

        update new List<Account> {acct1, acct2};

        accountList = [select Id, PersonMailingAddress from Account where Id in :idSet];
        System.debug('accountList upd:\n' + accountList);

        return accountList;
	}

    private static void assignDataToAccount(Account acct, AccountDataContainer data)
    {
        if (data.name != null) acct.LastName = data.name;
        if (data.street != null) acct.PersonMailingStreet = data.street;
        if (data.city != null) acct.PersonMailingCity = data.city;
        if (data.state != null) acct.PersonMailingState = data.state;
        if (data.postalCode != null) acct.PersonMailingPostalCode = data.postalCode;
        if (data.country != null) acct.PersonMailingCountry = data.country;
    }

    private class mockPersonAccountService
        implements mmcommon_IPersonAccountsService
    {
        public Set<Id> resultsFromDetermination  = null;

        public List<Account> searchAccounts( String firstName, String lastName, String email, String phoneNum, Date birthDate )
        {
            return null;  // Not needed.
        }

        public List<Account> searchAccountsUsingHighPrecision(String firstName, String lastName, String email, String phoneNum, Date birthDate)
        {
            return null;
        }

        public List<Account> searchAccountsUsingFuzzyMatching(Map<String, String> data)
        {
            return null;  // Not needed.
        }

        public void saveRecords( list<Account> records )
        {
            // Not needed.
        }

        public void geocodeAddresses(Set<Id> idSet)
        {
            resultsFromDetermination = idSet;
        }
    }

    private class AccountDataContainer
    {
        public String name = null;
        public String street = null;
        public String city = null;
        public String state = null;
        public String postalCode = null;
        public String country = null;

        public AccountDataContainer(
            String name,
            String street,
            String city,
            String state,
            String postalCode,
            String country
        )
        {
            this.name = name;
            this.street = street;
            this.city = city;
            this.state = state;
            this.postalCode = postalCode;
            this.country = country;
        }
    }
}