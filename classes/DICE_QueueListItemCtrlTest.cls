@isTest
private class DICE_QueueListItemCtrlTest {
  static DICE_Queue__c queue;
  static {
    queue = new DICE_Queue__c(Name = 'Test queue');
    insert queue;


    PageReference PageRef = Page.DICE_QueueListItem;
    Test.setCurrentPage(PageRef);
    ApexPages.currentPage().getParameters().put('q', queue.id);
  }

  @isTest static void Initialize_PopulatesIntakeColumnsOption() {
    Test.startTest();
    DICE_QueueListItemController ctrl = new DICE_QueueListItemController(new ApexPages.StandardController(new DICE_Queue__c()));
    Test.stopTest();

    //System.assertNotEquals(0, ctrl.IntakeColumns.size());
  }
}