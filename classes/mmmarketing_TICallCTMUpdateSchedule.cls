/**
 *  mmmarketing_TICallCTMUpdateSchedule
 *
 *  @see mmmarketing_TICallCTMUpdateSchedulable
 */
public class mmmarketing_TICallCTMUpdateSchedule
    implements mmlib_ISchedule
{
    public void execute(SchedulableContext sc)
    {
        try
        {
            // find MTI-Call records that were created over the past 6 hours
            list<Marketing_Tracking_Info__c> records = mmmarketing_TrackingInfosSelector.newInstance().selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecently();

            // pass the MTI-Call records to the MTI service call.
            mmmarketing_TrackingInfosService.updateCallRelatedInformation( (new map<id, Marketing_Tracking_Info__c>( records )).keyset() );


            // find MTI-Call records that were created < 20 min ago
            records = mmmarketing_TrackingInfosSelector.newInstance().selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecentlyForShallowResolution();

            // pass the MTI-Call records to the MTI service call
            mmmarketing_TrackingInfosService.updateCallRelatedInformation_ShallowReconciliation( (new map<id, Marketing_Tracking_Info__c>( records )).keyset() );
        }
        catch (Exception e)
        {
            system.debug(e);
            if ( e.getCause() != null )
            {
                system.debug( e.getCause() );

                if ( e.getCause().getCause() != null )
                {
                     system.debug( e.getCause().getCause() );
                }
            }
        }
    }
}