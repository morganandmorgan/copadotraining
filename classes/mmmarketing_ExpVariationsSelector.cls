/**
 *  mmmarketing_ExpVariationsSelector
 */
public class mmmarketing_ExpVariationsSelector
    extends mmlib_SObjectSelector
    implements mmmarketing_IExpVariationsSelector
{
    public static mmmarketing_IExpVariationsSelector newInstance()
    {
        return (mmmarketing_IExpVariationsSelector) mm_Application.Selector.newInstance(Marketing_Exp_Variation__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Marketing_Exp_Variation__c.SObjectType;
    }

    public override String getOrderBy()
    {
        return Marketing_Exp_Variation__c.Experiment_Variation_Name__c.getDescribe().getName();
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Marketing_Exp_Variation__c.Description__c,
                Marketing_Exp_Variation__c.Experiment_Variation_Name__c,
                Marketing_Exp_Variation__c.Marketing_Experiment__c,
                Marketing_Exp_Variation__c.Optimizely_Id__c
            };
    }

    public List<Marketing_Exp_Variation__c> selectById(Set<Id> idSet)
    {
        return (List<Marketing_Exp_Variation__c>) selectSObjectsById(idSet);
    }
}