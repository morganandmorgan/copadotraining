public with sharing class mmcommon_Accounts {
    private List<Account> records = new List<Account>();
    private Map<Id, Account> existingRecords = new Map<Id, Account>();
    private mmcommon_AccountsDomainExceptions.ValidationException validationExcep = null;
    private static Set<Id> personAccountIDSet = new map<id, RecordType>( [select id from RecordType where SObjectType = 'Account' and IsPersonType = true] ).keyset();

    private mmcommon_Accounts() {
    }

    public mmcommon_Accounts(List<Account> records) {
        this();
        this.setRecords(records);
    }

    public static void triggerHandler() {
        mmcommon_Accounts domain = new mmcommon_Accounts();

        if (Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete) {
            domain.setRecords( Trigger.new );
        }

        if (Trigger.isUpdate || Trigger.isDelete) {
            domain.setExistingRecords( Trigger.oldMap );
        }

        if (Trigger.isBefore && Trigger.isInsert) {
            domain.onBeforeInsert();
        } else if (Trigger.isBefore && Trigger.isUpdate) {
            domain.onBeforeUpdate();
        } else if (Trigger.isAfter && Trigger.isInsert) {
            domain.onAfterInsert();
        } else if (Trigger.isAfter && Trigger.isUpdate) {
            domain.onAfterUpdate();
        } else if (Trigger.isBefore && Trigger.isDelete) {
            domain.onBeforeDelete();
        }

    }

    private mmcommon_AccountsDomainExceptions.ValidationException getValidationException() {
        system.debug( 'getValidationException() call' );
        if ( validationExcep == null ) {
            validationExcep = new mmcommon_AccountsDomainExceptions.ValidationException( this.records );
        }

        return validationExcep;
    }

    private boolean isValidationExceptionsPresent() {
        return validationExcep != null;
    }

    private void setRecords( list<Account> records ) {
        if (records != null) {
            this.records = records;
        }
    }

    private void setExistingRecords( Map<Id, SObject> existingRecords) {
        this.existingRecords = (Map<Id, Account>)existingRecords;
    }

    private void onBeforeDelete() {
        AccountUpdateIncident_TrigAct updateIncidentAction = new AccountUpdateIncident_TrigAct();
        if (updateIncidentAction.shouldRun()) {
            updateIncidentAction.doAction();
        }
    }

    private void onBeforeInsert() {
        synchronizeSSNFields();
    }

    private void onBeforeUpdate() {
        resetGeocodingUpdateIfRequired();
        synchronizeSSNFields();
    }

    private void onAfterInsert() {
		mdVerifyEmail();
    }

    private void onAfterUpdate() {
        AccountUpdateIncident_TrigAct updateIncidentAction = new AccountUpdateIncident_TrigAct();
        if (updateIncidentAction.shouldRun()) {
            updateIncidentAction.doAction();
        }
        mdVerifyEmail();
    }

    public void validate() {
        system.debug( 'mmcommon_Accounts.validate() method called.');

        Set<Schema.DisplayType> stringDisplayTypes = new Set<Schema.DisplayType>{
            Schema.DisplayType.Email,
            Schema.DisplayType.EncryptedString,
            Schema.DisplayType.MultiPicklist,
            Schema.DisplayType.Phone,
            Schema.DisplayType.Picklist,
            Schema.DisplayType.String,
            Schema.DisplayType.TextArea,
            Schema.DisplayType.URL
        };

        Pattern noPunctuationPhonePattern = Pattern.compile('[0-9]{10}');
        Pattern noParenthesesPhonePattern = Pattern.compile('[0-9]{3}-[0-9]{3}-[0-9]{4}');
        Pattern standardPhonePattern = Pattern.compile('[(][0-9]{3}[)] [0-9]{3}-[0-9]{4}');

        Object fieldValue = null;
        Boolean isRequired = null;
        Boolean isBlankValue = null;
        Boolean isPhoneType = null;

        Integer numberOfRequiredPhoneFields = 0;
        Integer numberOfPopulatedRequiredPhoneFields = 0;

        Schema.DescribeSObjectResult sObjectDescribe = this.records.getSobjectType().getDescribe();

        for ( Account record : this.records ) {
            numberOfRequiredPhoneFields = 0;
            numberOfPopulatedRequiredPhoneFields = 0;

            for (FieldSetMember fsm : SObjectType.Account.FieldSets.Contact_Create_Screen.getFields()) {
                fieldValue = record.get(fsm.getFieldPath());

                isRequired = fsm.getDBRequired() || fsm.getRequired();

                isBlankValue = fieldValue == null || (stringDisplayTypes.contains(fsm.getType()) && String.isBlank((String) fieldValue));

                isPhoneType = fsm.getType() == Schema.DisplayType.Phone;

                if (isPhoneType) {
                    numberOfRequiredPhoneFields += isRequired ? 1 : 0;

                    if ( ! isBlankValue ) {
                        numberOfPopulatedRequiredPhoneFields += isRequired ? 1 : 0;
                        if ( ! noPunctuationPhonePattern.matcher((String) fieldValue).matches()
                                && ! noParenthesesPhonePattern.matcher((String) fieldValue).matches()
                                && ! standardPhonePattern.matcher((String) fieldValue).matches()) {
                            //record.addError('Phone number must be formatted as: (xxx) xxx-xxxx, xxxxxxxxxx or xxx-xxx-xxxx');
                            //hasValidationError = true;
                            getValidationException().addMessage(record, sObjectDescribe.fields.getMap().get( fsm.getFieldPath() ), 'Phone number must be formatted as: (xxx) xxx-xxxx, xxxxxxxxxx or xxx-xxx-xxxx');
                        }
                    }
                } else if (isBlankValue && isRequired) {
                    //record.addError('Field is required: ' + fsm.getLabel());
                    //hasValidationError = true;
                    getValidationException().addMessage(record, sObjectDescribe.fields.getMap().get( fsm.getFieldPath() ), 'Field is required: ' + fsm.getLabel());
                }
            }

            if (numberOfRequiredPhoneFields > 0 && numberOfPopulatedRequiredPhoneFields == 0) {
                //record.addError('At least one phone field is required');
                //hasValidationError = true;
                getValidationException().addMessage(record, 'At least one phone field is required');
            }


        }

        if ( isValidationExceptionsPresent() ) {
            throw getValidationException();
        }

    }

    /**
     * Alternative to the Records property, provided to support mocking of Domain classes
     **/
    public List<Account> getRecords() {
        return records;
    }

    public void resetGeocodingUpdateIfRequired() {
        //system.debug( 'resetGeocodingUpdateIfRequired area start');
        List<String> addressGroups = new List<String>{'Billing', 'PersonMailing', 'PersonOther', 'Shipping'};

        List<String> addressItems = new List<String>{'Street', 'City', 'State', 'PostalCode', 'Country'};

        Boolean isStillTesting = true;

        set<string> populatedFieldsOnRecordSet = new set<String>();

        String workingAddressFieldName = null;

        for ( Account record : records ) {
            //system.debug( 'resetGeocodingUpdateIfRequired area - mark 2a - id:'+ record.id);
            //system.debug( 'resetGeocodingUpdateIfRequired area - mark 2b - '+ record);

            // Do we need to process this record?
            if ( personAccountIDSet.contains( record.RecordTypeId ) // is this record a PersonAccount type?
                && existingRecords.containsKey( record.id )         // is this record present in the existingRecords Map
                ) {
                //system.debug( 'resetGeocodingUpdateIfRequired area - mark 3');
                isStillTesting = true;

                populatedFieldsOnRecordSet = record.getPopulatedFieldsAsMap().keyset();
                //system.debug( 'resetGeocodingUpdateIfRequired area - mark 4 - populatedFieldsOnRecordSet : ' + populatedFieldsOnRecordSet);

                // Determine if one of the watched address fields is present
                for (String grp : addressGroups) {
                    //system.debug( 'resetGeocodingUpdateIfRequired area - mark 5 - grp : ' + grp);

                    for (String item : addressItems) {
                        // system.debug( 'resetGeocodingUpdateIfRequired area - mark 6 - item : ' + item);

                        workingAddressFieldName = grp + item;
                        //system.debug( 'resetGeocodingUpdateIfRequired area - mark 7a - workingAddressFieldName : ' + workingAddressFieldName);
                        //system.debug( 'resetGeocodingUpdateIfRequired area - mark 7b - populatedFieldsOnRecordSet.contains( workingAddressFieldName ) : ' + populatedFieldsOnRecordSet.contains( workingAddressFieldName ));

                        //is the address field present on this record?  Was it queried?
                        if ( populatedFieldsOnRecordSet.contains( workingAddressFieldName ) ) {
                            String one = String.valueOf( record.get( workingAddressFieldName ) );
                            String two = String.valueOf( existingRecords.get(record.id).get( workingAddressFieldName ) );
                            //system.debug( 'resetGeocodingUpdateIfRequired area - mark 8a - one : ' + one);
                            //system.debug( 'resetGeocodingUpdateIfRequired area - mark 8b - two : ' + two);

                            // Is there a change for this address field?
                            if ( ! mmlib_Utils.possiblyNullStringsAreEqual(one, two)) {
                                // At least one address field has changed....
                                // .... reset the Last_Geocoding_Update__c field
                                record.Last_Geocoding_Update__pc = null;
                                record.put(grp + 'Latitude', null);
                                record.put(grp + 'Longitude', null);

                                // .... no further need to check this record's fields
                                isStillTesting = false;
                                break;
                            }
                        }
                    }

                    if ( ! isStillTesting ) {
                        break;
                    }
                }
            }
        }
    }

    public void synchronizeSSNFields() {
        for ( Account record : records ) {
            if (String.isBlank(record.Social_Security_Number__c)) {
                record.SocialSecurityNumberSearch__c = '';
                record.SocialSecurityNumber_Last4__c = '';
                continue;
            }

            if (record.Social_Security_Number__c.length() <= 20) {
                record.SocialSecurityNumberSearch__c = record.Social_Security_Number__c;
            } else {
                record.SocialSecurityNumberSearch__c = record.Social_Security_Number__c.substring(0, 20);
            }
            
            if (4 <= record.Social_Security_Number__c.length()) {
                record.SocialSecurityNumber_Last4__c = 
                    record.Social_Security_Number__c.substring(
                        record.Social_Security_Number__c.length() - 4,
                        record.Social_Security_Number__c.length()
                    );
            }
        }
    }

    private void mdVerifyEmail() {
		for (Account a : records) {
			if (!Test.isRunningTest()) {
				if(!System.isFuture() && !System.isBatch()) {
                    //if insert or personEmail changed
                    if (existingRecords.isEmpty() || existingRecords.get(a.id).personEmail != a.personEmail) {
                        if (a.personEmail != null && a.personEmail.toLowerCase() != 'none@none.com') {
             				MDPERSONATOR.MD_GlobalEmailWSExt.doOneGlobalEmail(a.id);
                        }
                	}
            	}
        	}
        } //for accounts
    } //mdVerifyEmail

} //class