/**
 * mmmarketing_TrackingInfosServiceImpl_Te
 * @description 
 * @author Jeff Watson
 * @date 2/21/2019
 */
@IsTest
public with sharing class mmmarketing_TrackingInfosServiceImpl_Te {

    private static mmmarketing_ITrackInfoRecordCallRequest trackInfoRecordCallRequest;
    private static Intake__c intake;
    private static Account personAccount;
    private static Marketing_Tracking_Info__c marketingTrackingInfo;

    static {
        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        marketingTrackingInfo = new Marketing_Tracking_Info__c();
        marketingTrackingInfo.Intake__c = intake.Id;
        insert marketingTrackingInfo;

        trackInfoRecordCallRequest = mmmarketing_TrackInfoRecordCallRequest.newInstance();
    }

    @IsTest
    private static void ctor() {
        mmmarketing_TrackingInfosServiceImpl trackingInfosServiceImpl = new mmmarketing_TrackingInfosServiceImpl();
        System.assert(trackingInfosServiceImpl != null);
    }

    @IsTest
    private static void recordCallsForIntakes() {
        mmmarketing_TrackingInfosServiceImpl trackingInfosServiceImpl = new mmmarketing_TrackingInfosServiceImpl();
        trackingInfosServiceImpl.recordCallsForIntakes(new List<mmmarketing_ITrackInfoRecordCallRequest> {trackInfoRecordCallRequest});
    }

    @IsTest
    private static void updateCallRelatedInformation() {
        mmmarketing_TrackingInfosServiceImpl trackingInfosServiceImpl = new mmmarketing_TrackingInfosServiceImpl();
        trackingInfosServiceImpl.updateCallRelatedInformation(new Set<Id> {marketingTrackingInfo.Id});
    }

    @IsTest
    private static void updateCallRelatedInformation_ShallowReconciliation() {
        mmmarketing_TrackingInfosServiceImpl trackingInfosServiceImpl = new mmmarketing_TrackingInfosServiceImpl();
        trackingInfosServiceImpl.updateCallRelatedInformation_ShallowReconciliation(new Set<Id> {marketingTrackingInfo.Id});
    }

    @IsTest
    private static void updateAllCallRelatedInformation() {
        Test.startTest();
        mmmarketing_TrackingInfosServiceImpl trackingInfosServiceImpl = new mmmarketing_TrackingInfosServiceImpl();
        trackingInfosServiceImpl.updateAllCallRelatedInformation();
        Test.stopTest();
    }

    @IsTest
    private static void coverage() {
        mmmarketing_TrackingInfosServiceImpl trackingInfosServiceImpl = new mmmarketing_TrackingInfosServiceImpl();
        trackingInfosServiceImpl.coverage();
    }
}