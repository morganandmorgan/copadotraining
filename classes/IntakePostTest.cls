@isTest
private class IntakePostTest {

  @isTest static void json_with_account_and_intake() {
    IntakePost post = new IntakePost();
    
    post.CreateUpdateIntake('{"intake": {"Case_Notes__c": "i was hurt"}, "account": {"firstName": "Bill", "lastName": "Gates"}}');

    Account objAccount = [select Id, firstName, lastName from Account order by CreatedDate desc limit 1];
    Intake__c objIntake = [select Id, Case_Notes__c from Intake__c order by CreatedDate desc limit 1];

    System.assertNotEquals(null, objAccount);
    System.assertNotEquals(null, objIntake);
    System.assertEquals('Bill', objAccount.firstName);
    System.assertEquals('Gates', objAccount.lastName);
    System.assertEquals('i was hurt', objIntake.Case_Notes__c);
  }

  @isTest static void json_with_accountid_intakeid_litigationtype() {
    IntakePost post = new IntakePost();
    
    Account act = new Account(LastName = 'test');
    insert act;

    Intake__c itc = new Intake__c(Client__c = act.id);
    insert itc;

    post.CreateUpdateIntake('{"intake": {"id": "' + itc.id + '", "Litigation__c": "labor"}, "account": {"id": "' + act.id + '"}}');

    Intake__c objIntake = [select Litigation__c from Intake__c WHERE ID = :itc.id];

    System.assertEquals('Labor', objIntake.Litigation__c);
  }

  @isTest
  private static void noAccountThrowsException() {
    IntakePost post = new IntakePost();
    try {
      post.CreateUpdateIntake('{"intake": {"Case_Notes__c": "i was hurt"}}');
      System.assert(false, 'Exception should have been thrown');
    }
    catch (IntakePost.MalformedRequestException e) {
      System.assert(true);
    }
    catch (Exception e) {
      System.assert(false, 'Unexpected exception thrown');
    }
  }

  @isTest
  private static void noIntakeThrowsException() {
    IntakePost post = new IntakePost();
    try {
      post.CreateUpdateIntake('{"account": {"firstName": "Bill", "lastName": "Gates"}}');
      System.assert(false, 'Exception should have been thrown');
    }
    catch (IntakePost.MalformedRequestException e) {
      System.assert(true);
    }
    catch (Exception e) {
      System.assert(false, 'Unexpected exception thrown');
    }
  }

  @isTest
  private static void create_Success() {
    RestRequest request = new RestRequest();
    request.requestBody = Blob.valueOf('{"intake": {"Case_Notes__c": "i was hurt"}, "account": {"firstName": "Bill", "lastName": "Gates"}}');

    RestContext.request = request;
    
    String result = IntakePost.create();

    System.assertEquals('ok', result);

    Account objAccount = [select Id, firstName, lastName from Account order by CreatedDate desc limit 1];
    Intake__c objIntake = [select Id, Case_Notes__c from Intake__c order by CreatedDate desc limit 1];

    System.assertEquals('Bill', objAccount.firstName);
    System.assertEquals('Gates', objAccount.lastName);
    System.assertEquals('i was hurt', objIntake.Case_Notes__c);
  }

  @isTest
  private static void create_Failure() {
    RestRequest request = new RestRequest();
    request.requestBody = Blob.valueOf('{"intake": {"Case_Notes__c": "i was hurt"}}');

    RestContext.request = request;
    
    String result = IntakePost.create();

    System.assertEquals('error', result);

    System.assertEquals(0, [SELECT COUNT() FROM Account]);
    System.assertEquals(0, [SELECT COUNT() FROM Intake__c]);
  }
}