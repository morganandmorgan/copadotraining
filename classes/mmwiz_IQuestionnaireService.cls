/**
 *  mmwiz_IQuestionnaireService
 */
public interface mmwiz_IQuestionnaireService
{
    mmwiz_QuestionnaireModel findQuestionnaire( string questionnaireToken, PageReference pageRef, String sessionGuid );
    list<mmwiz_AbstractActionModel> processResponseActions( String QuestionnaireToken, String PageToken, map<string, list<string>> questionResponseMap, PageReference pageRef, String sessionGuid );
}