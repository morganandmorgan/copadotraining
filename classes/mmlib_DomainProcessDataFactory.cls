public class mmlib_DomainProcessDataFactory 
{
    private mmlib_DomainProcessDataFactory() { }
    
    map<String, Object> domainProcessTestRecordFieldMap = new map<String, Object>();

    private mmlib_DomainProcessDataFactory(Type clazzToInjectType)
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.DeveloperName.getDescribe().getName(), clazzToInjectType.getName() );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.MasterLabel.getDescribe().getName(), clazzToInjectType.getName() );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.ClassToInject__c.getDescribe().getName(), clazzToInjectType.getName() );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.Description__c.getDescribe().getName(), 'blah blah blah' );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.IsActive__c.getDescribe().getName(), true );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.OrderOfExecution__c.getDescribe().getName(), 1.1 );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.RelatedDomain__c.getDescribe().getName(), 'Account' );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.LogicalInverse__c.getDescribe().getName(), false );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.ExecuteAsynchronous__c.getDescribe().getName(), false );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.PreventRecursive__c.getDescribe().getName(), null );

    }

    public static mmlib_DomainProcessDataFactory newInstanceForClass( Type clazzToInjectType )
    {
        return new mmlib_DomainProcessDataFactory( clazzToInjectType );
    }

    public mmlib_DomainProcessDataFactory criteria()
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.Type__c.getDescribe().getName(), mmlib_DomainProcessConstants.PROCESS_TYPE.CRITERIA.name() );
        
        return this;
    }

    public mmlib_DomainProcessDataFactory action()
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.Type__c.getDescribe().getName(), mmlib_DomainProcessConstants.PROCESS_TYPE.ACTION.name() );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.PreventRecursive__c.getDescribe().getName(), true );

        return this;
    }

    public mmlib_DomainProcessDataFactory triggerExecution( mmlib_DomainProcessConstants.TRIGGER_OPERATION_TYPE triggerOperationType )
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.ProcessContext__c.getDescribe().getName(), mmlib_DomainProcessConstants.PROCESS_CONTEXT.TriggerExecution.name() );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.TriggerOperation__c.getDescribe().getName(), triggerOperationType.name() );

        return this;
    }

    public mmlib_DomainProcessDataFactory domainMethodExecution( String domainMethodToken )
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.ProcessContext__c.getDescribe().getName(), mmlib_DomainProcessConstants.PROCESS_CONTEXT.DomainMethodExecution.name() );
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.DomainMethodToken__c.getDescribe().getName(), domainMethodToken );

        return this;
    }

    public mmlib_DomainProcessDataFactory executeAsynchronous()
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.ExecuteAsynchronous__c.getDescribe().getName(), true );

        return this;
    }

    public mmlib_DomainProcessDataFactory logicalInverse()
    {
        this.domainProcessTestRecordFieldMap.put( DomainProcess__mdt.LogicalInverse__c.getDescribe().getName(), true );

        return this;
    }

    public DomainProcess__mdt generate()
    {
        return (DomainProcess__mdt) JSON.deserialize( json.serializePretty( this.domainProcessTestRecordFieldMap ), DomainProcess__mdt.class );
    }

}