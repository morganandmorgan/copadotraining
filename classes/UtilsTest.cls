/**
 * UtilsTest
 * @description Tests for shared utilities.
 * @author Jeff Watson
 * @date 7/26/2018
 */
@IsTest
public with sharing class UtilsTest {

    public static testMethod void createDebugLog() {

        // Arrange/Act/Assert
        try {
            Double result = 5/0;
        } catch (Exception e) {
            Utils.createDebugLog(e);
            System.assert(true);
        }
    }
}