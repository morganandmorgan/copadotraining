public interface mmintake_IApprovalAssignmentsSelector extends mmlib_ISObjectSelector
{
    List<Approval_Assignment__c> selectForAssignmentOperation
    (
        Set<String> caseTypeSet,
        Set<String> venueSet,
        Set<String> handlingFirmSet
    );
}