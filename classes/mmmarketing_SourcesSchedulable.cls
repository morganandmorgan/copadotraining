/**
 *  mmmarketing_SourcesSchedulable
 */
global class mmmarketing_SourcesSchedulable
    implements Schedulable
{
    private static final String scheduleJobString = mmmarketing_SourcesSchedulable.class.getName() + ' scheduled job';

    global void execute(SchedulableContext sc)
    {
        list<Marketing_Financial_Period__c> records = mmmarketing_FinancialPeriodsSelector.newInstance().selectWhereSourceNotLinked();

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        mmmarketing_FinancialPeriods financialPeriods = new mmmarketing_FinancialPeriods( records );

        financialPeriods.setUnitOfWork( uow );

        financialPeriods.autoCreateMarketingSourcesIfRequired();

        uow.commitWork();

        // now that the job has executed, abort it from the schedule table.
        System.abortJob( sc.getTriggerId() );

        // are there more records that should be processed?
        if ( ! mmmarketing_FinancialPeriodsSelector.newInstance().selectOneWhereSourceNotLinked().isEmpty() )
        {
            scheduleNextRun( 2 );
        }
    }

    private static void scheduleNextRun( final integer numberOfMinutesDelay )
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticSourceSetupEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticSourceSetupEnabled__c )
        {
            mmmarketing_SourcesSchedulable clazz = new mmmarketing_SourcesSchedulable();

            // execute this run 3 minutes from now
            datetime datetimeToRun = datetime.now().addMinutes( numberOfMinutesDelay );

            String sch = mmlib_utils.generateCronTabStringFromDatetime( datetimeToRun );

            try
            {
                String jobID = system.schedule(scheduleJobString, sch, clazz);

                system.debug( 'the jobID == '+ jobID);
            }
            catch ( Exception e )
            {
                system.debug( e );
            }
        }
    }

    public static void setupCreateSourcesFromMFP()
    {
//        list<CronTrigger> currentCronjobs = mmlib_CronTriggersSelector.newInstance().selectWaitingScheduledApexByName( new Set<String>{ scheduleJobString } );
//
//        if ( ! currentCronjobs.isEmpty() )
//        {
//            // there is at least one other version of this job already scheduled.  Delete those occurances and setup this one.
//            for ( CronTrigger currentCronjob : currentCronjobs )
//            {
//                try
//                {
//                    System.abortJob( currentCronjob.id );
//                }
//                catch ( Exception e )
//                {
//                    system.debug( e );
//                }
//            }
//        }

        list<CronTrigger> currentCronjobs = mmlib_CronTriggersSelector.newInstance().selectScheduledApexByName( new Set<String>{ scheduleJobString } );

        if ( currentCronjobs.isEmpty() )
        {
            CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

            integer numberOfMinutesDelay = customSetting == null ? 10 : integer.valueOf( customSetting.DelaySourceSetupScheduleMinutes__c );

            scheduleNextRun( numberOfMinutesDelay );
        }
    }
}