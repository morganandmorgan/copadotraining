@isTest
private class IntakeDocusignAssignmentTest {

    private static Integer numberOfIntakesToBuild = 10;

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        List<Docusign_Template_Manager__c> docusignManagerSettings = new List<Docusign_Template_Manager__c>();
        docusignManagerSettings.add(new Docusign_Template_Manager__c(
                Name = 'Setting A',
                Case_Type__c = 'Abilify',
                Contract_Venue__c = null,
                Docusign_ID__c = 'DOCUSIGN_ID_VALUE_A',
                State_Abbreviation__c = 'CA',
                Venue__c = 'Out of Area',
                Handling_Firm__c = 'Morgan & Morgan',
                Where_did_the_injury_occur__c = 'Standard Workers Compensation Claim'
            ));
        docusignManagerSettings.add(new Docusign_Template_Manager__c(
                Name = 'Setting B',
                Case_Type__c = 'Abilify',
                Contract_Venue__c = 'Test Contract Venue',
                Docusign_ID__c = 'DOCUSIGN_ID_VALUE_B',
                State_Abbreviation__c = 'CA',
                Venue__c = 'Nationwide',
                Handling_Firm__c = 'Nation Law',
                Where_did_the_injury_occur__c = 'Standard Workers Compensation Claim'
            ));
        docusignManagerSettings.add(new Docusign_Template_Manager__c(
                Name = 'Setting C',
                Case_Type__c = 'General Class Action',
                Class_Action_Type__c = 'Class Action 1',
                Contract_Venue__c = 'Test Contract Venue',
                Docusign_ID__c = 'DOCUSIGN_ID_VALUE_C',
                State_Abbreviation__c = 'CA',
                Venue__c = 'Nationwide',
                Handling_Firm__c = 'Nation Law'
            ));
        docusignManagerSettings.add(new Docusign_Template_Manager__c(
                Name = 'Setting D',
                Case_Type__c = 'General Class Action',
                Class_Action_Type__c = 'Class Action 2',
                Contract_Venue__c = 'Test Contract Venue',
                Docusign_ID__c = 'DOCUSIGN_ID_VALUE_D',
                State_Abbreviation__c = 'CA',
                Venue__c = 'Nationwide',
                Handling_Firm__c = 'Nation Law'
            ));
        toInsert.addAll((List<SObject>) docusignManagerSettings);

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Account getClient() {
        return [SELECT Id FROM Account WHERE IsPersonAccount = true];
    }

    private static Incident__c getIncident() {
        return [SELECT Id FROM Incident__c];
    }

    private static Map<Id, Intake__c> getIntakesMap(List<Intake__c> intakes) {
        return new Map<Id, Intake__c>([SELECT Id, DocusignTemplateID__c FROM Intake__c WHERE Id IN :intakes]);
    }

    private static List<Intake__c> buildIntakes(Account client, Incident__c incident, Docusign_Template_Manager__c toMatch) {
        return buildIntakes(client, incident, toMatch, numberOfIntakesToBuild);
    }

    private static List<Intake__c> buildIntakes(Account client, Incident__c incident, Docusign_Template_Manager__c toMatch, Integer numberToBuild) {
        List<Intake__c> intakes = new List<Intake__c>();
        Date today = Date.today();
        for (Integer i = 0; i < numberToBuild; ++i) {
            intakes.add(new Intake__c(
                    Client__c = client.Id,
                    Incident__c = incident.Id,
                    Litigation__c = 'Workers Compensation',
                    Case_Type__c = toMatch.Case_Type__c,
                    Contract_Venue__c = toMatch.Contract_Venue__c,
                    DocusignTemplateID__c = null,
                    State_of_incident__c = toMatch.State_Abbreviation__c,
                    Venue__c = toMatch.Venue__c,
                    Handling_Firm__c = toMatch.Handling_Firm__c,
                    Where_did_the_injury_occur__c = toMatch.Where_did_the_injury_occur__c,
                    What_type_of_Class_Action_case_is_it__c = toMatch.Class_Action_Type__c
                ));
        }
        return intakes;
    }

    @isTest
    private static void testInsert_SuccessfulMatch() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchCaseType() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            intake.Case_Type__c = settingA.Case_Type__c + ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.Case_Type__c = settingB.Case_Type__c + ' MISMATCH';
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

//    @isTest
//    private static void testInsert_MismatchHandlingFirm() {
//        Account client = getClient();
//        Incident__c incident = getIncident();
//        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
//        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');
//
//        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
//        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);
//
//        for (Intake__c intake : intakesA) {
//            intake.Handling_Firm__c = settingA.Handling_Firm__c + ' MISMATCH';
//        }
//
//        for (Intake__c intake : intakesB) {
//            intake.Handling_Firm__c = settingB.Handling_Firm__c + ' MISMATCH';
//        }
//
//        List<Intake__c> allIntakes = new List<Intake__c>();
//        allIntakes.addAll(intakesA);
//        allIntakes.addAll(intakesB);
//
//        Test.startTest();
//        Database.insert(allIntakes);
//        Test.stopTest();
//
//        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);
//
//        for (Intake__c intake : intakesA) {
//            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
//            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
//        }
//
//        for (Intake__c intake : intakesB) {
//            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
//            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
//        }
//    }

    @isTest
    private static void testInsert_MatchBlankState() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        settingA.State_Abbreviation__c = null;
        settingB.State_Abbreviation__c = null;
        Database.update(new List<Docusign_Template_Manager__c>{ settingA, settingB });

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchState() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            intake.State_of_incident__c = 'OH';
        }

        for (Intake__c intake : intakesB) {
            intake.State_of_incident__c = 'OH';
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MatchBlankInjuryLocation() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        settingA.Where_did_the_injury_occur__c = null;
        settingB.Where_did_the_injury_occur__c = null;
        Database.update(new List<Docusign_Template_Manager__c>{ settingA, settingB });

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchInjuryLocation() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            intake.Where_did_the_injury_occur__c += ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.Where_did_the_injury_occur__c += ' MISMATCH';
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchVenue_BlankSetting() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        System.assertNotEquals(null, settingA.Venue__c);
        settingA.Venue__c = null;
        Database.update(settingA);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchVenue_BlankIntake() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            System.assertNotEquals(null, intake.Venue__c);
            intake.Venue__c = null;
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchContractVenue_BlankSetting() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        System.assertNotEquals(null, settingB.Contract_Venue__c);
        settingB.Contract_Venue__c = null;
        Database.update(settingB);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            // now matches on Venue, not Contract Venue
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testInsert_MismatchContractVenue_BlankIntake() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesB) {
            System.assertNotEquals(null, intake.Contract_Venue__c);
            intake.Contract_Venue__c = null;
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);

        Test.startTest();
        Database.insert(allIntakes);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_SuccessfulMatch() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchCaseType() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            intake.Case_Type__c = settingA.Case_Type__c + ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.Case_Type__c = settingB.Case_Type__c + ' MISMATCH';
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

//    @isTest
//    private static void testUpdate_MismatchHandlingFirm() {
//        Account client = getClient();
//        Incident__c incident = getIncident();
//        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
//        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');
//
//        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
//        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);
//
//        for (Intake__c intake : intakesA) {
//            intake.Handling_Firm__c = settingA.Handling_Firm__c + ' MISMATCH';
//        }
//
//        for (Intake__c intake : intakesB) {
//            intake.Handling_Firm__c = settingB.Handling_Firm__c + ' MISMATCH';
//        }
//
//        List<Intake__c> allIntakes = new List<Intake__c>();
//        allIntakes.addAll(intakesA);
//        allIntakes.addAll(intakesB);
//        Database.insert(allIntakes);
//
//        Test.startTest();
//        Database.update(getIntakesMap(allIntakes).values());
//        Test.stopTest();
//
//        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);
//
//        for (Intake__c intake : intakesA) {
//            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
//            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
//        }
//
//        for (Intake__c intake : intakesB) {
//            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
//            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
//        }
//    }

    @isTest
    private static void testUpdate_MatchBlankState() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        settingA.State_Abbreviation__c = null;
        settingB.State_Abbreviation__c = null;
        Database.update(new List<Docusign_Template_Manager__c>{ settingA, settingB });

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchState() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            intake.State_of_incident__c = 'OH';
        }

        for (Intake__c intake : intakesB) {
            intake.State_of_incident__c = 'OH';
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MatchBlankInjuryLocation() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        settingA.Where_did_the_injury_occur__c = null;
        settingB.Where_did_the_injury_occur__c = null;
        Database.update(new List<Docusign_Template_Manager__c>{ settingA, settingB });

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchInjuryLocation() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            intake.Where_did_the_injury_occur__c += ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.Where_did_the_injury_occur__c += ' MISMATCH';
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchVenue_BlankSetting() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        System.assertNotEquals(null, settingA.Venue__c);
        settingA.Venue__c = null;
        Database.update(settingA);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchVenue_BlankIntake() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesA) {
            System.assertNotEquals(null, intake.Venue__c);
            intake.Venue__c = null;
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchContractVenue_BlankSetting() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        System.assertNotEquals(null, settingB.Contract_Venue__c);
        settingB.Contract_Venue__c = null;
        Database.update(settingB);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            // now matches on Venue, not Contract Venue
            System.assertEquals(settingB.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }

    @isTest
    private static void testUpdate_MismatchContractVenue_BlankIntake() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingA = Docusign_Template_Manager__c.getInstance('Setting A');
        Docusign_Template_Manager__c settingB = Docusign_Template_Manager__c.getInstance('Setting B');

        List<Intake__c> intakesA = buildIntakes(client, incident, settingA);
        List<Intake__c> intakesB = buildIntakes(client, incident, settingB);

        for (Intake__c intake : intakesB) {
            System.assertNotEquals(null, intake.Contract_Venue__c);
            intake.Contract_Venue__c = null;
        }

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesA);
        allIntakes.addAll(intakesB);
        Database.insert(allIntakes);

        Test.startTest();
        Database.update(getIntakesMap(allIntakes).values());
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = getIntakesMap(allIntakes);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingA.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.DocusignTemplateID__c);
        }
    }

    // ============== Class Action Type testing ===========================
    @isTest
    private static void ClassActionTest_SuccessfulMatch() {
        Account client = getClient();
        Incident__c incident = getIncident();
        Docusign_Template_Manager__c settingC = Docusign_Template_Manager__c.getInstance('Setting C');
        Docusign_Template_Manager__c settingD = Docusign_Template_Manager__c.getInstance('Setting D');

        List<Intake__c> intakesC = buildIntakes(client, incident, settingC, 1);
        List<Intake__c> intakesD = buildIntakes(client, incident, settingD, 1);

        List<Intake__c> allIntakes = new List<Intake__c>();
        allIntakes.addAll(intakesC);
        allIntakes.addAll(intakesD);

        Test.startTest();

        insert allIntakes;

        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>(allIntakes);
        requeriedIntakes = new Map<Id, Intake__c>([select Id, DocusignTemplateID__c from Intake__c where Id in :requeriedIntakes.keyset()]);

        for (Intake__c intake : intakesC) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingC.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }

        for (Intake__c intake : intakesD) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(settingD.Docusign_ID__c, requeriedIntake.DocusignTemplateID__c);
        }
    }
}