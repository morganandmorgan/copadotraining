/**
 * AuditRuleToolContrtollerTest
 * @description Units tests for AuditRuleToolControler
 * @author Matt Terrill
 * @date 10/15/2019
*/
@isTest
public class AuditRuleToolControllerTest {

    @testSetup
    public static void setup() {
    }


    @isTest
    public static void mainTest() {

        Audit_Rule__c auditRule = new Audit_Rule__c();

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(auditRule);
        AuditRuleToolController artCtrl = new AuditRuleToolController(stdCtrl);

        List<Schema.FieldSetMember> dynamicFields = artCtrl.getDynamicFields();

        List<SelectOption> baseObjects = artCtrl.getBaseObjects();
        System.assertNotEquals(0, baseObjects.size());

        //choose a base object
        artCtrl.theRule.auditRule.object__c = 'Account';

        List<SelectOption> dtFields = artCtrl.getDateTimeFields();
        System.assertNotEquals(0, dtFields.size());

        List<SelectOption> userFields = artCtrl.getUserFields();
        System.assertNotEquals(0, userFields.size());

        List<SelectOption> baseFields = artCtrl.getBaseFields();
        System.assertNotEquals(0, baseFields.size());

        //select a base field that is not relationship field
        for (SelectOption baseField : baseFields) {
            if ( !baseField.getValue().contains('^')) {
                artCtrl.baseField = baseField.getValue();
                artCtrl.selectBaseField();
                artCtrl.copyBaseFieldToNextArgument();
                break;
            }
        }

        List<SelectOption> relatedFields;
        //select a base field that IS a relationship field
        for (SelectOption baseField : baseFields) {
            if ( baseField.getValue().contains('^')) {
                artCtrl.baseField = baseField.getValue();
                artCtrl.selectBaseField();
                relatedFields = artCtrl.getRelatedFields();
                System.assertNotEquals(0, relatedFields.size());
                break;
            }
        }

        artCtrl.relatedField = relatedFields[0].getValue();
        artCtrl.selectRelatedField();
        artCtrl.copyRelatedFieldToNextArgument();

        artCtrl.addExpression();
        artCtrl.deleteIndex = 1;
        artCtrl.deleteExpression();

        artCtrl.testRule();
        artCtrl.saveRule();
        artCtrl.cloneRule();
        artCtrl.scheduleJob();
                
    } //mainTest


} //class