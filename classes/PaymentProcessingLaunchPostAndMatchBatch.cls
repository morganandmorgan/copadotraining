public class PaymentProcessingLaunchPostAndMatchBatch implements Database.Batchable<Id>, Database.Stateful {
    
    public Id paymentId {get; set;}
    public Payment_Processing_Batch_Status__c paymentStatusTracker {get; set;}
    
    //TODO:  Consider optionally passing in the tracking record if retrieved at a higher level.
    
    public PaymentProcessingLaunchPostAndMatchBatch(Id paymentId){
        this.paymentId=paymentId;
    }
    
    public PaymentProcessingLaunchPostAndMatchBatch(Id paymentId, Payment_Processing_Batch_Status__c paymentStatusRecord){
        this.paymentId=paymentId;
        this.paymentStatusTracker=paymentStatusRecord;
    }    
          
    public Iterable<Id> start(Database.BatchableContext batchContext) {
        List<Id> paymentIds=null;
        paymentIds=new List<Id>{paymentId};
        return paymentIds;
    }
    
    public void execute(Database.BatchableContext batchContext, List<Id> scope){
        //Who cares.
        // if(scope.size()>0){
        //     System.debug('PaymentProcessingLaunchPostAndMatchBatch execute() :' + scope[0]); 
        // }else{
        //     System.debug('PaymentProcessingLaunchPostAndMatchBatch execute() : no records');
        // }
    }
    
    public void finish(Database.BatchableContext batchContext){

        System.debug('PaymentProcessingLaunchPostAndMatchBatch finish() method.  Chaining post & match job...');
      
        //if(paymentStatusTracker==null)
        {
            //Get the current state of the payment status - in case another instance was launched:  
            PaymentProcessingStatusDataAccess statusData=null;
            statusData=new PaymentProcessingStatusDataAccess();            
                
            List<Payment_Processing_Batch_Status__c> trackingRecords=null;  //Should be one record.
            
            trackingRecords=statusData.getPendingPaymentPostAndMatchTrackingRecords(paymentId);
        
            if((trackingRecords!=null)&&(trackingRecords.size()>0)){
                paymentStatusTracker=trackingRecords[0];        
            }
            
        }

        Boolean canRun=true;

        if(paymentStatusTracker!=null){
            //The payment must have a status of "Ready to Post" or "Error".
            if(
                (paymentStatusTracker.Payment__r.c2g__Status__c == 'Posted') 
                ||(paymentStatusTracker.Payment__r.c2g__Status__c == 'Matched') 
                ||(paymentStatusTracker.Payment__r.c2g__Status__c == 'Background Posting')                
            ){
                canRun=false;
            }
            
        }        

        if(canRun){

            System.debug('Launching postAndMatchAsync()...  ' + paymentId);

            Id batchProcessId = null;
            
            //TODO:  catch AsyncException
            //}catch(System.AsyncException ae){

            //NOTE:  If successful, this calls Database.execute() and goes asynchronous:
            //Launches a PaymentsPlusPostAndMatchBatch instance:  
            batchProcessId = c2g.PaymentsPlusService.postAndMatchAsync(paymentId);

            //Fake job test:
            //batchProcessId=RunTestBatchJob();
            
            if(paymentStatusTracker!=null){
                paymentStatusTracker.Post_Job_Id__c = batchProcessId;
                paymentStatusTracker.Message__c = 'Running Post and Match...';
            }
            
            //TODO:  Store the batch Id on the processing record.
            System.debug('batchProcessId:  ' + batchProcessId);

            if(paymentStatusTracker!=null){
                update paymentStatusTracker;        
            }

        }else{
            //Avoid re-entrancy:  probably already running:  
            System.debug('Skipping postAndMatchAsync()...  ' + paymentId + '(' + paymentStatusTracker.Payment__r.c2g__Status__c + ')');
        }

    }
 
    
}