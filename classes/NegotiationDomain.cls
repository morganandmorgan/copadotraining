public without sharing class NegotiationDomain extends fflib_SObjectDomain {

    private List<litify_pm__Negotiation__c> negotiations;

    public NegotiationDomain() {
        super();
        this.negotiations = new List<litify_pm__Negotiation__c>();
    } //constructor

    public NegotiationDomain(List<litify_pm__Negotiation__c> negotiations) {
        super(negotiations);
        this.negotiations = negotiations;
    } //constructor()

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new NegotiationDomain(records);
        }
    } //class Constructor

    public override void onBeforeInsert() {
        flagNegotiations(null);
    } //onBeforeInsert
    
    public override void onAfterInsert() {
        AuditProcessor.triggerHandler(records, null);
    } //onAfterInsert

    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        flagNegotiations(existingRecords);
    } //onBeforeUpdate
    
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        AuditProcessor.triggerHandler(records, existingRecords);
    } //onAfterUpdate

    public override void onAfterDelete() {
        flagNegotiations(null);
    } //onAfterDelete

    private class NegWithClass {
        litify_pm__Negotiation__c first {get;set;} 
        litify_pm__Negotiation__c last {get;set;}
        public NegWithClass(litify_pm__Negotiation__c init) {
            first = init;
            last = init;
        }
    }

    private void checkNegWith(litify_pm__Negotiation__c neg, Map<String,NegWithClass> negWith) {

        String negWithName;
        //determinie who the negotiation is with (CP vs Litify)
        if (!String.IsBlank(neg.insurance_Company__c)) {
            negWithName = neg.insurance_Company__c + ' - ' + neg.insurance_Type__c;
        }
        if (neg.litify_pm__Negotiating_with__c != null) {
            negWithName = neg.litify_pm__Negotiating_with__c;
        }
        
        //if we know who we are negotiating with
        if (negWithName != null) {
            //is this the first negotiation with this party we've found?
            if (!negWith.containsKey(negWithName)) {
                //this will be the first and last neg
                negWith.put(negWithName, new NegWithClass(neg));
            } else {
                //see if this is before/after the first/last neg
                if (neg.litify_pm__Date__c < negWith.get(negWithName).first.litify_pm__Date__c) {
                    negWith.get(negWithName).first = neg;
                }
                if (neg.litify_pm__Date__c > negWith.get(negWithName).last.litify_pm__Date__c) {
                    negWith.get(negWithName).last = neg;
                }
            }
        }

    } //checkNegWith


    private void flagNegotiations(Map<Id, SObject> existingRecords) {
        //This is complicated because we will be updating a mix of records, some involved in the trigger and others not
        //Because this is before..., we can't update records involved in the trigger, so extra code is there to avoid it
        
        System.debug('new/old');
        System.debug(negotiations);
        System.debug(existingRecords);
        
        Id demandTypeId;
        Id offerTypeId;
        //find the 2 negotitaion types
        for (RecordType rt : [SELECT id, name, sObjectType FROM RecordType WHERE sObjectType = 'litify_pm__Negotiation__c' AND name IN ('Demand','Offer')]) {
            if ( rt.name == 'Demand') demandTypeId = rt.id;
            if ( rt.name == 'Offer') offerTypeId = rt.id;
        }

        Set<Id> matterIds = new Set<Id>();
        Set<Id> deleteNegIds = new Set<Id>();
        //build a set of unique Matters involved in these negotiations
        for (litify_pm__Negotiation__c neg : negotiations) {
            if (!Trigger.isDelete) {
                //is this an insert
                if (existingRecords == null) {
                    matterIds.add(neg.litify_pm__Matter__c);
                } else {
                    litify_pm__Negotiation__c thisNeg = (litify_pm__Negotiation__c)existingRecords.get(neg.id);
                    //care if the date changed
                    if (existingRecords == null || thisNeg.litify_pm__Date__c != neg.litify_pm__Date__c) {
                        matterIds.add(neg.litify_pm__Matter__c);
                    } //if date updated
                } //if insert
            } else {
                matterIds.add(neg.litify_pm__Matter__c);
                deleteNegIds.add(neg.id);
            }
        } //for negotiations
        
        //get all the negotiations involved with all the matters, minus the ones we are deleting
        List<litify_pm__Negotiation__c> negs = [SELECT id, first__c, last__c, insurance_Company__c, insurance_Type__c, litify_pm__Date__c, litify_pm__Matter__c,litify_pm__Negotiating_with__c, recordTypeId FROM litify_pm__Negotiation__c WHERE litify_pm__Matter__c IN :matterIds AND id NOT IN :deleteNegIds];

        Map<Id,litify_pm__Negotiation__c> triggerNegs;
        // if this is an update
        if (existingRecords != null || Trigger.isDelete) {
            //build a map of negotiations in this trigger, we don't need to update them onBeforeUpdate
             triggerNegs = new Map<Id,litify_pm__Negotiation__c>(negotiations);
        } else {
            negs.addAll(negotiations); //add in the new ones being inserted
            triggerNegs = new Map<Id,litify_pm__Negotiation__c>();
        }

        Map<Id,litify_pm__Negotiation__c> updateMap = new Map<Id,litify_pm__Negotiation__c>();

        //lets do this by matter
        for (Id matterId : matterIds) {

            // there can be a first/last for each party we are negotiating with
            // Map<'demand/offer',Map<'with-party-name',NegWith>>
            Map<String,Map<String,NegWithClass>> negWithType = new Map<String,Map<String,NegWithClass>>();
            negWithType.put('demand', new Map<String,NegWithClass>());
            negWithType.put('offer', new Map<String,NegWithClass>());

            for (litify_pm__Negotiation__c neg : negs) {
                if (neg.litify_pm__Matter__c == matterId) {
					//we can't update negotiations involved in the onBeforeUpdate trigger
                    if (neg.recordTypeId == demandTypeId) {
						//if this is an update, work with the trigger record
                        if ( triggerNegs.containsKey(neg.id) ) {
							checkNegWith(triggerNegs.get(neg.id),negWithType.get('demand'));
							triggerNegs.get(neg.id).first__c = false;
                            triggerNegs.get(neg.id).last__c = false;
                        } else {
                        	checkNegWith(neg,negWithType.get('demand'));
                            neg.first__c = false;
                            neg.last__c = false;
                            //we can't update records being inserted
                            if (neg.id != null) {
                            	updateMap.put(neg.id, neg);
                            }
                        }
                    } //if demand
                    if (neg.recordTypeId == offerTypeId) {
						//we can't update negotiations involved in the onBeforeUpdate trigger
                        if ( triggerNegs.containsKey(neg.id) ) {
							checkNegWith(triggerNegs.get(neg.id),negWithType.get('offer'));
							triggerNegs.get(neg.id).first__c = false;
                            triggerNegs.get(neg.id).last__c = false;
                        } else {
                        	checkNegWith(neg,negWithType.get('offer'));
                            neg.first__c = false;
                            neg.last__c = false;
                            //we can't update records being inserted
                            if (neg.id != null) {
                            	updateMap.put(neg.id, neg);
                            }
                        }
                    } //if offer
                } //if current matter
            } //for negotiations

            for (String type : new List<String>{'demand','offer'}) {
                for (NegWithClass negWith : negWithType.get(type).values()) {
                    negWith.first.first__c = true;
                    //we can't update records being inserted or updated
                    if ( negWith.first.id != null && !triggerNegs.containsKey(negWith.first.id) ) {
                    	updateMap.put(negWith.first.id, negWith.first);
                    }
                    negWith.last.last__c = true;
                    //we can't update records being inserted or updated
                    if ( negWith.last.id != null && !triggerNegs.containsKey(negWith.last.id)) {
                    	updateMap.put(negWith.last.id, negWith.last);
                    }
                    System.debug('...flags set.');
                } //for negWith values
            } //for negWith types

        } //for matterts

        System.debug('doing update...');
        System.debug(updateMap.values());
        update updateMap.values();

    } //flagNegotiations

} //class