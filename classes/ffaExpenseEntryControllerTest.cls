@IsTest
global class ffaExpenseEntryControllerTest
{
    public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;
    public static c2g__codaCompany__c company;
    @testSetup
    static void setup()
    {
       /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        update company;
        List<c2g__codaBankAccount__c> bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        c2g__codaBankAccount__c costBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Cost');
        bankAccountList.add(costBankAccount);
        c2g__codaBankAccount__c trustBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Trust');
        bankAccountList.add(trustBankAccount);
        insert bankAccountList;

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;
        
        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan; 

        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        INSERT expenseType;

        litify_pm__Role__c role = new litify_pm__Role__c();
        role.litify_pm__Party__c = client.Id;
        role.litify_pm__Role__c = 'Plaintiff';
        role.litify_pm__Matter__c = matter.Id;

        INSERT role;
    }

    @IsTest
    public static void testHappyPath() 
    {
        ffaExpenseEntryController controller = new ffaExpenseEntryController();
        controller.addRows = null;
        controller.addNewRows();
        controller.addRows = 3;
        controller.addNewRows();

        //System.assertEquals(14, controller.expenseHolderList.size()); // 10 rows to start + 4 added = 14

        controller.index = 0;
        controller.deleteRow();
        controller.deleteRow();
        controller.deleteRow();
        controller.deleteRow();

        litify_pm__Matter__c matter = [Select Id FROM litify_pm__Matter__c LIMIT 1];
        litify_pm__Expense_Type__c expType = [Select Id FROM litify_pm__Expense_Type__c LIMIT 1];

        controller.index = 0;
        controller.matterId = matter.Id;
        // controller.expenseHolderList[0].expense.litify_pm__Matter__c = matter.Id;
        controller.loadPayableTo();

        System.assertEquals(true, controller.expenseHolderList[0].payableOptions.size() > 0);

        litify_pm__Expense__c expense = controller.expenseHolderList[0].expense;
        expense.litify_pm__Matter__c = matter.Id;
        expense.litify_pm__ExpenseType2__c = expType.Id;
        expense.litify_pm__Date__c = date.today();

        controller.cancel();
        PageReference pr = controller.save();
        System.assertEquals(null, pr);


        expense.litify_pm__Amount__c = 0.25;
        pr = controller.save();

    }
}