/**
 *  mmlib_BaseGETCallout
 */
public abstract class mmlib_BaseGETCallout
    extends mmlib_BaseCallout
{
    protected override CALLOUT_METHODS getMethod()
    {
        return CALLOUT_METHODS.GET;
    }

    protected override String getPostRequestBody()
    {
        return null;
    }
}