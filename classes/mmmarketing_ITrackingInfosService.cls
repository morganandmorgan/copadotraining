/**
 *  mmmarketing_ITrackingInfosService
 */
public interface mmmarketing_ITrackingInfosService
{
    void recordCallsForIntakes( list<mmmarketing_ITrackInfoRecordCallRequest> requests );
    void updateCallRelatedInformation( set<id> mtiCallRecordIdSet );
    void updateCallRelatedInformation_ShallowReconciliation( set<id> mtiCallRecordIdSet );
}