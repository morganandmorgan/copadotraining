/*============================================================================
Name            : FFAPrepaidExpenseService
Author          : CLD
Created Date    : Aug 2018
Description     : Test class for Batch Manager Page
=============================================================================*/
public class FFAPrepaidExpenseService {
	public FFAPrepaidExpenseService() {
		
	}

	//-----------------------------------------------
	// Record Prepaid Relief Journals from Expenses
	//-----------------------------------------------
	public static void createPrepaidReliefJournals(Set<Id> expenseIds, Id companyId, Date filterDate, Boolean autoPost){


		Set<Id> itemIds = new Set<Id>();
		String COMPANY_NAME = '';
		List<c2g__codaJournalLineItem__c> jnliInsertList = new List<c2g__codaJournalLineItem__c>();
		Map<String, c2g__codaJournal__c> journalMap = new Map<String, c2g__codaJournal__c>();
		Map<String, List<c2g__codaJournalLineItem__c>> jnlLineMap = new Map<String, List<c2g__codaJournalLineItem__c>>();

		//variable for controlling the number of journals created
		Integer journalLineCounter = 0;
		Integer journalCounter = 1;
		Integer maxJournalLines = 240;

		List<litify_pm__Expense__c> expenseList = [SELECT Id,
				litify_pm__Amount__c,
                Prepaid_Relief_Processed__c,
                Prepaid_Relief_Journal__c,
                FFA_Linking_Key__c,
                litify_pm__Matter__c,
                litify_pm__Matter__r.Name,
                litify_pm__Matter__r.Assigned_Office_Location__r.c2g__codaDimension1__c,
                litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_2__c,
                litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_3__c,
                litify_pm__ExpenseType2__c,
                litify_pm__ExpenseType2__r.Name, 
                litify_pm__ExpenseType2__r.CostType__c, 
                litify_pm__ExpenseType2__r.General_Ledger_Account__c,
                litify_pm__ExpenseType2__r.Prepaid_Expense_Reverse_GLA__c
            FROM litify_pm__Expense__c
            WHERE Id in : expenseIds
            AND Prepaid_Expense_Type__c = true 
            AND Written_Off__c = false
            AND Prepaid_Relief_Processed__c = false];

		c2g__codaCompany__c COMPANY;
		for(c2g__codaCompany__c compObj : [SELECT ID, OwnerId FROM c2g__codaCompany__c WHERE Id = :companyId]){
			COMPANY = compObj;
			system.debug('COMPANY = ' + COMPANY);
		}

		//loop through expenses and create journals
		for(litify_pm__Expense__c exp : expenseList){
			
			String jnlKey = String.valueOf(journalCounter);
			String jnlLineKey = exp.litify_pm__ExpenseType2__c +'|'+ exp.litify_pm__Matter__c +'|'+ journalCounter;

			exp.FFA_Linking_Key__c = jnlKey;

			if(!journalMap.containsKey(jnlKey)){
				c2g__codaJournal__c journal = new c2g__codaJournal__c(
					c2g__ownerCompany__c = COMPANY.id,
					OwnerId = COMPANY.OwnerId,
					c2g__Reference__c = 'Prepaid Expense Reversal',
					c2g__JournalDescription__C = 'Prepaid Expense Reversal for costs recorded on matters',
					c2g__JournalDate__c = filterDate,
					c2g__Type__c = 'Manual Journal');
				journalMap.put(jnlKey, journal);
			}
			//JOURNAL LINES
			if(jnlLineMap.containsKey(jnlLineKey)){
				//update the existing value of the journal Lines
				List<c2g__codaJournalLineItem__c> tempList = jnlLineMap.get(jnlLineKey);
				for(c2g__codaJournalLineItem__c jnlLine : tempList){
					jnlLine.c2g__Value__c += jnlLine.c2g__DebitCredit__c == 'Debit' ? exp.litify_pm__Amount__c.setScale(2) : (exp.litify_pm__Amount__c*-1).setScale(2);
				}
				jnlLineMap.put(jnlLineKey, tempList);
			}
			else{
				List<c2g__codaJournalLineItem__c> tempList = new List<c2g__codaJournalLineItem__c>();
				//DEBIT LINE:
				c2g__codaJournalLineItem__c debitLine = new c2g__codaJournalLineItem__c(
					Linking_Key__c = jnlKey,
					c2g__LineType__c = 'General Ledger Account',
					c2g__DebitCredit__c = 'Debit',
					c2g__Value__c = exp.litify_pm__Amount__c.setScale(2),
					c2g__Dimension1__c = exp.litify_pm__Matter__r.Assigned_Office_Location__r.c2g__codaDimension1__c,
					c2g__Dimension2__c = exp.litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_2__c,
					c2g__Dimension3__c = exp.litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_3__c,
					Matter__c = exp.litify_pm__Matter__c,
					Expense_Type__c = exp.litify_pm__ExpenseType2__c,
					Litify_Expense_Created__c = true,
					c2g__LineDescription__c = 'Prepaid Expense Relief Journal | Exp Type : '+ exp.litify_pm__ExpenseType2__r.Name +' Matter : '+exp.litify_pm__Matter__r.Name,
					c2g__GeneralLedgerAccount__c = exp.litify_pm__ExpenseType2__r.General_Ledger_Account__c,
					c2g__DeriveLineNumber__c = true);
				tempList.add(debitLine);
				journalLineCounter++;

				//CREDIT LINE:
				c2g__codaJournalLineItem__c creditLine = new c2g__codaJournalLineItem__c(
					Linking_Key__c = jnlKey,
					c2g__LineType__c = 'General Ledger Account',
					c2g__DebitCredit__c = 'Credit',
					c2g__Value__c = (exp.litify_pm__Amount__c*-1).setScale(2),
					c2g__Dimension1__c = exp.litify_pm__Matter__r.Assigned_Office_Location__r.c2g__codaDimension1__c,
					c2g__Dimension2__c = exp.litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_2__c,
					c2g__Dimension3__c = exp.litify_pm__Matter__r.litify_pm__Case_Type__r.Dimension_3__c,
					Matter__c = exp.litify_pm__Matter__c,
					Expense_Type__c = exp.litify_pm__ExpenseType2__c,
					Litify_Expense_Created__c = true,
					c2g__LineDescription__c = 'Prepaid Expense Relief Journal | Exp Type : '+ exp.litify_pm__ExpenseType2__r.Name +' Matter : '+exp.litify_pm__Matter__r.Name,
					c2g__GeneralLedgerAccount__c = exp.litify_pm__ExpenseType2__r.Prepaid_Expense_Reverse_GLA__c,
					c2g__DeriveLineNumber__c = true);
				tempList.add(creditLine);
				journalLineCounter++;

				//add these new lines to the jnl line map
				jnlLineMap.put(jnlLineKey, tempList);
				
			}
			//evaluate the journal line counter, if it's over the max then start a new Journal and reset the counters:
			if(journalLineCounter >= maxJournalLines){
				journalCounter++;
				journalLineCounter = 0;
			}
		}

		//DML operations:
		List<Id> jnlIdList = new List<Id>();
		Savepoint sp1 = Database.setSavepoint();
		Try{
			
			insert journalMap.values();
			for(c2g__codaJournal__c jnl : journalMap.values()){
				jnlIdList.add(jnl.Id);
			}

			//collect the journal lines and relate them back to the journal headers
			List<c2g__codaJournalLineItem__c> jnlLineInsertList = new List<c2g__codaJournalLineItem__c>();
			for(List<c2g__codaJournalLineItem__c> jnlLineList : jnlLineMap.values()){
				jnlLineInsertList.addAll(jnlLineList);
			}
			for(c2g__codaJournalLineItem__c jnlLine : jnlLineInsertList){
				jnlLine.c2g__Journal__c = journalMap.containsKey(jnlLine.Linking_Key__c) ? journalMap.get(jnlLine.Linking_Key__c).Id : null;
			}
			insert jnlLineInsertList;

			//update the expenses
			for(litify_pm__Expense__c exp : expenseList){
				exp.Prepaid_Relief_Journal__c = journalMap.containsKey(exp.FFA_Linking_Key__c) ? journalMap.get(exp.FFA_Linking_Key__c).Id : null;
				exp.Prepaid_Relief_Processed__c = true;
			}
			update expenseList;

			//do the posting if the parameter indicates
			if(autoPost == true){			
				FFAUtilities.postJournals(jnlIdList);
			}
		}
		Catch(Exception e){
			throw e;
			Database.rollback(sp1);
		}
	}




}