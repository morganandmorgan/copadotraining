public class IntakeInvestigationCopyParent_TrigAct extends TriggerAction {

    // mapped by parent Intake__c Id
    private final Map<Id, List<IntakeInvestigationEvent__c>> intakeInvestigationEventsToSet = new Map<Id, List<IntakeInvestigationEvent__c>>();

	public IntakeInvestigationCopyParent_TrigAct() {
	   super();	
	}

    public override Boolean shouldRun() {
        intakeInvestigationEventsToSet.clear();

        List<IntakeInvestigationEvent__c> intakeInvestigationEventNewList = (List<IntakeInvestigationEvent__c>) triggerList;

        for (IntakeInvestigationEvent__c intakeInvestigationEvent : intakeInvestigationEventNewList) {
            Id intakeId = intakeInvestigationEvent.Intake__c;
            if (intakeId != null) {
                List<IntakeInvestigationEvent__c> mappedInvestigations = intakeInvestigationEventsToSet.get(intakeId);
                if (mappedInvestigations == null) {
                    mappedInvestigations = new List<IntakeInvestigationEvent__c>();
                    intakeInvestigationEventsToSet.put(intakeId, mappedInvestigations);
                }
                mappedInvestigations.add(intakeInvestigationEvent);
            }
        }

        return !intakeInvestigationEventsToSet.isEmpty();
    }

    public override void doAction() {
        if (!intakeInvestigationEventsToSet.isEmpty()) {
            for (Intake__c intake : [
                SELECT
                    Id,
                    Approving_Attorney_1__c,
                    Approving_Attorney_2__c,
                    Approving_Attorney_3__c,
                    Approving_Attorney_4__c,
                    Approving_Attorney_5__c,
                    Approving_Attorney_6__c,
                    Assigned_Attorney__c,
                    New_Case_Email__c
                FROM
                    Intake__c
                WHERE
                    Id IN :intakeInvestigationEventsToSet.keySet()
            ]) {
                for (IntakeInvestigationEvent__c intakeInvestigationEvent : intakeInvestigationEventsToSet.get(intake.Id)) {
                    intakeInvestigationEvent.Approving_Attorney_1__c = intake.Approving_Attorney_1__c;
                    intakeInvestigationEvent.Approving_Attorney_2__c = intake.Approving_Attorney_2__c;
                    intakeInvestigationEvent.Approving_Attorney_3__c = intake.Approving_Attorney_3__c;
                    intakeInvestigationEvent.Approving_Attorney_4__c = intake.Approving_Attorney_4__c;
                    intakeInvestigationEvent.Approving_Attorney_5__c = intake.Approving_Attorney_5__c;
                    intakeInvestigationEvent.Approving_Attorney_6__c = intake.Approving_Attorney_6__c;
                    intakeInvestigationEvent.Assigned_Attorney__c = intake.Assigned_Attorney__c;
                    intakeInvestigationEvent.New_Case_Email__c = intake.New_Case_Email__c;
                }
            }
        }
    }
}