/**
 *  Callout for the Litify Referral Network "referral_agreements" API
 *
 *  @see http://litify-swagger.bitballoon.com/#!/referral_agreements/getReferralAgreementsForOFandHF
 *
 *  @usage mmlitifylrn_ReferralAgreementsCallout.Response resp = (mmlitifylrn_ReferralAgreementsCallout.Response) new mmlitifylrn_ReferralAgreementsCallout()
 *                                   .setHandlingOrganization( litify_pm__Firm__c )
 *                                   .setAuthorization( mmlitifylrn_CalloutAuthorization )
 *                                   .debug()
 *                                   .execute()
 *                                   .getResponse();
 *
 *  @usage system.debug( resp.getFooBar() );
 */
public class mmlitifylrn_ReferralAgreementsCallout
    extends mmlitifylrn_BaseGETV1Callout
{
    private static final string HANDLING_ORGANIZATION_ID = 'handling_organization_id';

    private mmlitifylrn_ReferralAgreementsCallout.Response resp = new mmlitifylrn_ReferralAgreementsCallout.Response();

    private string originatingOrganizationId = null;

    private map<string, string> paramMap = new map<string, string>();

    private void validateCallout()
    {
        if ( string.isBlank( this.originatingOrganizationId ) )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Originating Organization Id has not been specified.  Please ensure that the id is specified via the setAuthorization( mmlitifylrn_CalloutAuthorization ) method before calling the execute() method.');
        }

        if ( paramMap.isEmpty()
            || ! paramMap.containsKey( HANDLING_ORGANIZATION_ID )
            || String.isBlank( paramMap.get(HANDLING_ORGANIZATION_ID) )
            )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Handling Organization has not been specified.  Please use the setHandlingOrganization(litify_pm__Firm__c) method to specify this before calling the execute() method.');
        }
    }

    public override map<string, string> getParamMap()
    {
        return this.paramMap;
    }

    public override string getPathSuffix()
    {
        return '/organizations/' + this.originatingOrganizationId + '/referral_agreements';
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        httpResponse httpResp = super.executeCallout();

        if (this.isDebugOn)
        {
            system.debug(httpResp.getStatusCode());
            system.debug(httpResp.getStatus());
        }

        string jsonResponseBody = httpResp.getBody();

        if (this.isDebugOn)
        {
            system.debug(json.deserializeUntyped(jsonResponseBody));
        }

        if (httpResp.getStatusCode() >= 200
            && httpResp.getStatusCode() < 300)
        {
            System.debug(httpResp.getBody());

            if ('{}'.equalsIgnoreCase( httpResp.getBody() ) )
            {
                resp = new mmlitifylrn_ReferralAgreementsCallout.Response();
            }
            else
            {
                resp = (mmlitifylrn_ReferralAgreementsCallout.Response) json.deserialize( '{"agreements":' + jsonResponseBody + '}', mmlitifylrn_ReferralAgreementsCallout.Response.class );
            }
        }
        else
        {
            throw new mmlitifylrn_Exceptions.CalloutException(httpResp);
        }

        return this;
    }

    public override mmlib_BaseCallout setAuthorization( mmlib_ICalloutAuthorizationable auth )
    {
        super.setAuthorization( auth );

        if ( auth instanceOf mmlitifylrn_CalloutAuthorization )
        {
            litify_pm__Firm__c originatingFirm = ((mmlitifylrn_CalloutAuthorization)auth).getFirmMakingTheCallout();

            this.originatingOrganizationId = string.valueOf( originatingFirm.litify_pm__ExternalId__c );
        }

        return this;
    }

    public mmlitifylrn_ReferralAgreementsCallout setHandlingOrganization( litify_pm__Firm__c handlingOrganization )
    {
        this.paramMap.put( HANDLING_ORGANIZATION_ID, string.valueOf( handlingOrganization.litify_pm__ExternalId__c ) );

        return this;
    }

    public class Response
        implements mmlib_BaseCallout.CalloutResponse
    {
        public list<mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement> agreements;

        public integer getTotalNumberOfRecordsFound()
        {
            return agreements == null ? 0 : agreements.size();
        }

    }
}