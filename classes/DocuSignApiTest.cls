@isTest
public  class DocuSignApiTest {

    @isTest
    public static void mainTest() {

        DocuSignApi dsApi = new DocuSignApi();

        List<String> templateIds = new List<String>{'1234abcd'};
        Map<String,String> recipient = new Map<String,String>();
        recipient.put('email','utest@forthepeople.com');
        recipient.put('name','Unit Test');
        Map<String,String> tokens = new Map<String,String>();
        tokens.put('Name1','Unit Test');

        String envelopeId = dsApi.createCompositeEnvelope(templateIds, '1234', recipient, tokens);

        String signingUrl = dsApi.getSigningUrl('1234', recipient, envelopeId, 'https://forthepeople.com', 'https://forthepeople.com');

    } //mainTest

} //class