public with sharing class mmwiz_QuestionAndAnswerSelector
	extends mmlib_SObjectSelector
	implements mmwiz_IQuestionAndAnswerSelector
{
	public static mmwiz_IQuestionAndAnswerSelector newInstance()
	{
		return (mmwiz_IQuestionAndAnswerSelector) mm_Application.Selector.newInstance(QuestionAndAnswer__c.SObjectType);
	}

	private Schema.sObjectType getSObjectType()
	{
		return QuestionAndAnswer__c.SObjectType;
	}

	private List<Schema.SObjectField> getAdditionalSObjectFieldList()
	{
		return new List<Schema.SObjectField>
		{
			QuestionAndAnswer__c.Answer__c,
			QuestionAndAnswer__c.Intake__c,
			QuestionAndAnswer__c.QuestionnaireToken__c,
			QuestionAndAnswer__c.QuestionText__c,
			QuestionAndAnswer__c.QuestionToken__c,
            QuestionAndAnswer__c.SessionGUID__c
		};
	}

	public List<QuestionAndAnswer__c> selectBySessionGuid(Set<String> sessionGuidSet)
    {
        if (sessionGuidSet == null || sessionGuidSet.isEmpty())
        {
            return new List<QuestionAndAnswer__c>();
        }

        return
            Database.query
            (
                newQueryFactory()
                    .setCondition
                    (
                        QuestionAndAnswer__c.SessionGUID__c + ' in :sessionGuidSet'
                    )
					.addOrdering(new fflib_QueryFactory.Ordering(QuestionAndAnswer__c.CreatedDate, fflib_QueryFactory.SortOrder.ASCENDING))
                    .toSOQL()
            );
    }
}