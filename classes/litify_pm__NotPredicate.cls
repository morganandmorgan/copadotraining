/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class NotPredicate implements litify_pm.IPredicate {
    global static litify_pm.IPredicate NewInstance(litify_pm.IPredicate predicate) {
        return null;
    }
    global override String toString() {
        return null;
    }
}
