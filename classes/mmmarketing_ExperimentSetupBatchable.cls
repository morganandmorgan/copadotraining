/**
 *  mmmarketing_ExperimentSetupBatchable
 */
public with sharing class mmmarketing_ExperimentSetupBatchable
    implements Database.Batchable<SObject>, mmlib_ISchedule
{
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        // find all MTI records that need to have Marketing_Exp_Variation__c value set
        return mmmarketing_TrackingInfosSelector.newInstance().selectQueryLocatorWhereMarketingExperienceVariationNotLinkedAndEventRecent();

    }

    public void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        // take the records from scope and pass it to the service....
        mmmarketing_ExperimentsService.setupExperimentsAndVariationsForMTIs( (list<Marketing_Tracking_Info__c>)scope );
    }

    public void finish(Database.BatchableContext BC)
    {

    }

    /**
     *  method implemented because mmlib_ISchedule
     *
     *  Allows for the scheduleable to be active but not maintain a lock on this and subsequent classes
     */
    public void execute(SchedulableContext sc)
    {
        Database.executeBatch( new mmmarketing_ExperimentSetupBatchable() );
    }
}