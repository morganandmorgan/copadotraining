/**
 *  mmmarketing_TISchedulable
 */
global class mmmarketing_TISchedulable
    implements Schedulable
{
    private static final String scheduleJobString = mmmarketing_TISchedulable.class.getName() + ' scheduled job';

    global void execute(SchedulableContext sc)
    {
        Type targettype = Type.forName('mmmarketing_TI_AutoLinkMFPBatch');

        if ( targettype != null )
        {
            mmlib_ISchedule obj = (mmlib_ISchedule)targettype.newInstance();
            obj.execute(sc);
        }
        else
        {
            system.debug( 'mmlib_ISchedule implementation of mmmarketing_TI_AutoLinkMFPBatch not found');
        }

        // now that the job has executed, abort it from the schedule table.
        System.abortJob( sc.getTriggerId() );
    }

    public static void setupAutoLinkingFromMTIToMFP()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticTILinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticTILinkingEnabled__c )
        {
            list<CronTrigger> currentCronjobs = mmlib_CronTriggersSelector.newInstance().selectWaitingScheduledApexByName( new Set<String>{ scheduleJobString } );

            if ( currentCronJobs.isEmpty() )
            {
                mmmarketing_TISchedulable clazz = new mmmarketing_TISchedulable();

                integer delayInMinutes = customSetting == null ? 2 : integer.valueOf( customSetting.DelayTIAutoLinkScheduleMinutes__c );

                // execute this run 8 minutes from now
                datetime datetimeToRun = datetime.now().addMinutes( delayInMinutes );

                String sch = mmlib_utils.generateCronTabStringFromDatetime( datetimeToRun );

                try
                {
                    String jobID = system.schedule(scheduleJobString, sch, clazz);

                    system.debug( 'the jobID == '+ jobID);
                }
                catch ( Exception e )
                {
                    system.debug( e );
                }
            }
        }
    }
}