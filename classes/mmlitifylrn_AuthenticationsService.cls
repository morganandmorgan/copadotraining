public class mmlitifylrn_AuthenticationsService
{
    private static mmlitifylrn_IAuthenticationsService service()
    {
        return (mmlitifylrn_IAuthenticationsService) mm_Application.Service.newInstance( mmlitifylrn_IAuthenticationsService.class );
    }

    public static void authenticateUser(string lrnUsername, string lrnPassword, id lrnFirmid)
    {
        service().authenticateUser(lrnUsername, lrnPassword, lrnFirmid);
    }

}