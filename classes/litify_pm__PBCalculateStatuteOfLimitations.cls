/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBCalculateStatuteOfLimitations {
    global PBCalculateStatuteOfLimitations() {

    }
    @InvocableMethod(label='Calculate Statute Of Limitations' description='Calculates the statute of limitations for each matter using the Statute of Limitations objects that match the matters' states and case types.')
    global static void calculateStatuteOfLimitations(List<litify_pm__Matter__c> matters) {

    }
}
