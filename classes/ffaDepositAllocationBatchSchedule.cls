global class ffaDepositAllocationBatchSchedule implements Database.Batchable<SObject>, Database.Stateful, Schedulable
{

    
    public Date depositCreatedDate;
    public Id ffaCompanyId;
    public Set<Id> specificDeposits;

    public ffaDepositAllocationBatchSchedule()
    {
        List<c2g__codaCompany__c> currentCompanies = FFAUtilities.getCurrentCompanies();
        this.ffaCompanyId = currentCompanies.isEmpty() ? null : currentCompanies[0].Id;
        this.depositCreatedDate = Date.today().addDays(-1);
    }
    public ffaDepositAllocationBatchSchedule(Date depositCreatedDate, Id ffaCompanyId)
    {
        this.ffaCompanyId = ffaCompanyId;
        this.depositCreatedDate = depositCreatedDate;
    }
    public ffaDepositAllocationBatchSchedule(Set<Id> specificDeposits, Id ffaCompanyId)
    {
        this.ffaCompanyId = ffaCompanyId;
        this.specificDeposits = specificDeposits;
    }
	public Database.QueryLocator start(Database.BatchableContext context)
    {
        String query = '';
        if(this.specificDeposits == null){
            query = 'SELECT Id FROM Deposit__c WHERE Deposit_Allocated__c = FALSE AND Check_Date__c = :depositCreatedDate AND Matter__r.AssignedToMMBusiness__r.FFA_Company__c = :ffaCompanyId';    
        }
        else{
            query = 'SELECT Id FROM Deposit__c WHERE Deposit_Allocated__c = FALSE AND Id in :specificDeposits AND Matter__r.AssignedToMMBusiness__r.FFA_Company__c = :ffaCompanyId';    
        }        
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        Set<Id> depositIds = new Set<Id>();
        FOR (SObject so : scope)
        {
            Deposit__c deposit = (Deposit__c)so;
            depositIds.add(deposit.Id);
        }

        MatterAllocationService.allocateItems_Deposit(depositIds);	
    }

    public void finish(Database.BatchableContext context)
    {
    }

    global void execute(SchedulableContext sc)
    {
        ffaDepositAllocationBatchSchedule schedule = new ffaDepositAllocationBatchSchedule();
        Database.executeBatch(schedule, 50);
    }

}