@isTest
public class depositCreateControllerTest {
    public static litify_pm__Matter__c matter;
    public static c2g__codaAccountingSettings__c accountingSettings;
    public static List<c2g__codaBankAccount__c> bankAccountList;

    static void setupData(){
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        accountingSettings = new c2g__codaAccountingSettings__c();
        insert accountingSettings;

        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        c2g__codaCompany__c company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__CustomerSettlementDiscount__c = testGLA.Id;
        update company;
        bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        c2g__codaBankAccount__c costBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Cost');
        bankAccountList.add(costBankAccount);
        c2g__codaBankAccount__c trustBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Trust');
        bankAccountList.add(trustBankAccount);
        insert bankAccountList;

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        
        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;
        
    }

    /*--------------------------------------------------------------------
        START TEST METHODS
    --------------------------------------------------------------------*/
    @isTest static void testDepositCreateController(){

        setupData();
        depositCreateController testController = new depositCreateController();
        depositCreateController.DepositWrapper depositWrapper = new depositCreateController.DepositWrapper();
        depositWrapper.depositAmount = 100;
        depositWrapper.matterId = matter.Id;
        List<depositCreateController.DepositWrapper> createList = new List<depositCreateController.DepositWrapper>{depositWrapper};

        Map<String, Object> headerFields = new Map<String, Object>();
        headerFields.put('Check_Date__c' , '2019-01-01');
        headerFields.put('Operating_Cash_Account__c' , String.valueOf(bankAccountList[0].Id));

        Map<String, Object> results1 = depositCreateController.getMatterDetails(matter.Id);

        Id trustRecordTypeId = Schema.SObjectType.Deposit__c.getRecordTypeInfosByName().get('Trust Deposit').getRecordTypeId();

        //test add row
        List<depositCreateController.DepositWrapper> testList = new List<depositCreateController.DepositWrapper>();
        depositCreateController.addDepositRow(testList,1,0);

        //test get record type
        List<RecordType> testRTList = depositCreateController.getRecordTypeOptions(bankAccountList[0].Id);

        depositCreateController.getBankAccountDetails(bankAccountList[0].Id);

        //test get bank account defaults
        Map<String,Object> testMap = depositCreateController.getBankAccountDefaults(String.valueOf(bankAccountList[0].Id));


        //test save
        depositCreateController.SaveResult results = depositCreateController.saveDeposits(headerFields, createList, trustRecordTypeId);

    }
}