public with sharing class IntakeRepNavigator_Json {
	public IntakeRepNavigator_Json() {
		
	}
  private class QueryContainer {
    List<Query> container {get; set;}
  }
  public class Query {
    public string column { get; set; }
    public string operator { get; set; }
    public string argument { get; set; }
  }
  private class SortOrderContainer { 
    List<SortOrder> container { get; set; }
  }
  public class SortOrder {
    public string column { get; set; }
    public string order { get; set; }
  }

  public static List<Query> ParseQuery(String strJson) {
    if (strJson == null || strJson.length() < 2) {
      return new List<Query>();
    }
    QueryContainer queryItemsContainer = (QueryContainer) JSON.deserialize('{"container":' + strJson + '}', QueryContainer.class);
    return queryItemsContainer.container;
  }
  public static List<SortOrder> ParseSortOrder(String strJson) {
    if (strJson == null || strJson.length() < 2) {
      return new List<SortOrder>();
    }
    SortOrderContainer sortOrdersContainer = (SortOrderContainer) JSON.deserialize('{"container":' + strJson + '}', SortOrderContainer.class);
    return sortOrdersContainer.container;
  }
}