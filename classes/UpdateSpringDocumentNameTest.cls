@isTest(SeeAllData=true)
public class UpdateSpringDocumentNameTest {
    
    static testMethod void testUploadForm() {
        litify_pm__Matter__c matter = [select Id from litify_pm__Matter__c limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(matter);
        SpringSingleUpload su = new SpringSingleUpload(sc);
        su.redirect();

    }

    static testMethod void testUpdateNameTrigger() {
        MM_Document__c mmDoc = new MM_Document__c();
        mmDoc.External_ID__c = '0000-1111-2222-3333-4444';
        mmDoc.Document_Name__c = 'LitifyTest.docx';
        insert mmDoc;

        mmDoc.Document_Name__c = 'LitifyTest2.docx';
        update mmDoc;
    }
}