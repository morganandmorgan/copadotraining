/**
 * ExpenseSummaryBatchTest
 * @description
 * @author Jeff Watson
 * @date 1/18/2019
 */
@isTest
public with sharing class ExpenseSummaryBatchRecalcTest {

    @TestSetup
    static void setup() {
        litify_pm__Expense_Type__c softExpenseType = new litify_pm__Expense_Type__c(Name = 'Soft Cost Recovered', CostType__c = 'SoftCost', ExternalID__c = 'SOFTCOSTRECOVERED');
        insert softExpenseType;

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;


        litify_pm__Expense__c expense = TestUtil.createExpense(matter);
        expense.litify_pm__ExpenseType2__c = softExpenseType.Id;
        insert expense;
    } //setup

    @isTest
    public static void ExpenseSummaryBatchRecalc_Execute() {
        Database.executeBatch(new ExpenseSummaryBatchRecalc(), 50);

        List<litify_pm__Matter__c> matters = [SELECT id FROM litify_pm__Matter__c];

        ExpenseSummaryBatchRecalc esBatch = new ExpenseSummaryBatchRecalc();
        esBatch.execute(null, matters);
    } //ExpenseSummaryBatchRecalc_Execute

    @isTest
    public static void ExpenseSummaryBatchRecalc_Execute_Schedulable() {

        // Arrange + Act + Assert
        Test.startTest();
        system.schedule('Test check', '0 0 23 * * ?', new ExpenseSummaryBatchRecalc());
        Test.stopTest();
    } //ExpenseSummaryBatchRecalc_Execute_Schedulable

} //class