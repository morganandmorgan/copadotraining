public without sharing class createPayableInvoiceController {
    public createPayableInvoiceController() {
    }

	public class SaveResult {
		@AuraEnabled public Boolean hasValidationError {get; set;}
		@AuraEnabled public String validationMessage {get; set;}
		@AuraEnabled public Boolean isSuccess {get; set;}
		@AuraEnabled public String sucessMessage {get; set;}
		
		public SaveResult () {
			this.hasValidationError = false;
			this.validationMessage = '';
			this.isSuccess = false;
			this.sucessMessage = '';
		}
	}

    public class PayableInvoiceLineWrapper {
		@AuraEnabled public String key {get; set;}
		@AuraEnabled public String matterId {get; set;}
		@AuraEnabled public String glaId {get; set;}
        @AuraEnabled public String expenseTypeId {get; set;}
		@AuraEnabled public Decimal lineAmount {get; set;}
        @AuraEnabled public String lineDescription {get; set;}
        @AuraEnabled public String pinId {get; set;}
		@AuraEnabled public String dim1Id {get; set;}
		@AuraEnabled public String dim2Id {get; set;}
		@AuraEnabled public String dim3Id {get; set;}
		@AuraEnabled public String dim4Id {get; set;}

		public PayableInvoiceLineWrapper () {
			this.key = '';
			this.matterId = null;
            this.expenseTypeId = null;
			this.glaId = null;
			this.lineAmount = 0.00;
			this.lineDescription = null;
            this.pinId = null;
		}
	}

	@AuraEnabled(cacheable=true)
	public static List<PayableInvoiceLineWrapper> addPinRow(List<PayableInvoiceLineWrapper> pinLineWrappers, Integer numberToAdd, Integer keyValue) {
        System.debug('CALLING addPinRow');
		System.debug('addDepositRow - numberToAdd '+ numberToAdd);
		System.debug('addDepositRow - keyValue:  '+ keyValue);

		System.debug('addDepositRow - pinLineWrappers'+ pinLineWrappers);


        for(Integer i = 0; i < numberToAdd; i++){
			PayableInvoiceLineWrapper dWrap = new PayableInvoiceLineWrapper();
			dWrap.key = String.valueOf(keyValue);
            pinLineWrappers.add(dWrap);
			keyValue ++;
        }
		return pinLineWrappers;
	}

	@AuraEnabled(cacheable=true)
    public static Map<String,Object> getMatterDetails(String matterId){
		Map<String,Object> result = new Map<String,Object>();
		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
		
		if(matterId != '' && matterId !=null){
			for(litify_pm__Matter__c matter :  [
				SELECT Id, 
					Name,
					litify_pm__Client__c, 
					Assigned_Office_Location__r.c2g__codaDimension1__c,
					litify_pm__Case_Type__r.Dimension_2__c,
					litify_pm__Case_Type__r.Dimension_3__c,
					AssignedToMMBusiness__r.FFA_Company__c,
                    Finance_Status__c
				FROM litify_pm__Matter__c
				WHERE Id = :matterId]){
				result.put('client', matter.litify_pm__Client__c);
				result.put('dim1', matter.Assigned_Office_Location__r.c2g__codaDimension1__c);
				result.put('dim2', matter.litify_pm__Case_Type__r.Dimension_2__c);
				result.put('dim3', matter.litify_pm__Case_Type__r.Dimension_3__c);

				if(companies == null){
					result.put('Error', 'You must select a current company to continue');
				}
				else if(companies[0].Id != matter.AssignedToMMBusiness__r.FFA_Company__c || companies.size()>1){
					result.put('Error', 'The selected matter company does not match your current company - '+matter.Name);
				}
				else if(companies[0].Id == matter.AssignedToMMBusiness__r.FFA_Company__c && companies.size() == 1){
					result.put('Error', '');
				}

                if (matter.Finance_Status__c == 'Closed')
                {
                    result.put('Error', matter.name + ' has been finanically closed. ');
                }
			}
		}
		return result;
    }

	@AuraEnabled(cacheable=true)
    public static Map<String,Object> checkCurrentCompany(String bankAccountId){
		Map<String,Object> results = new Map<String,Object>();

		system.debug('bankAccountId = '+bankAccountId);
		if(bankAccountId != '' && bankAccountId !=null){
			for(c2g__codaBankAccount__c ba :  [
				SELECT Id, 
					c2g__OwnerCompany__c
				FROM c2g__codaBankAccount__c
				WHERE Id = :bankAccountId]){

				//check for current company
				List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
				if(companies == null || companies.size() == 0){
					results.put('Error', 'You must select a current company to continue');
				}
				else if(companies[0].Id != ba.c2g__OwnerCompany__c || companies.size()>1){
					results.put('Error', 'You must select a single current company and that company must match the bank account company');
				}
				else if(companies[0].Id == ba.c2g__OwnerCompany__c && companies.size() == 1){
					results.put('Error', '');
				}
			}
		}
		return results;		
    }

	@AuraEnabled(cacheable=true)
    public static String getAccountDefaultExpenseTypeId(String accountId){
		String result = '';
		if(accountId != '' && accountId !=null){
			for(Account acct :  [
				SELECT Id, Default_Expense_Type__c
				FROM Account
				WHERE Id = :accountId]){
                    if (acct.Default_Expense_Type__c != null)
				        result = acct.Default_Expense_Type__c;
			}
		}
		return result;
    }

	@AuraEnabled(cacheable=true)
    public static Account getAccountVerifiedInfo(String accountId){
		Account account = new Account();
		if(accountId != '' && accountId !=null){
			for(Account acct :  [
				SELECT Id, Name, Approved_Vendor__c, Vendor_Verification__c
				FROM Account
				WHERE Id = :accountId]){
               account = acct;
			}
		}
		return account;
    }

	@AuraEnabled
    public static SaveResult savePins(Map<String, Object> headerFields, List<PayableInvoiceLineWrapper> pinLineWrapperList, String bankAccountId, Boolean autoPost){
		system.debug('headerFields = '+ headerFields);
		system.debug('pinLineWrapperList = '+ pinLineWrapperList);
		system.debug('bankAccountId = '+ bankAccountId);
		system.debug('autoPost = '+ autoPost);

		SaveResult sr = new SaveResult();
		c2g__codaCompany__c company = null;
		List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLineInsertList = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();

		Id headerDim1 = null;
		Id headerDim2 = null;
		Id headerDim3 = null;
		Id headerDim4 = null;

		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
		if(companies != null && !companies.isEmpty()){
			company = companies[0];
		}

		Boolean validationError = false;
		Boolean saveComplete = false;
		String saveMessage = 'Sucess';
		String valMessage = '';

		//ENHANCE: validate the save:
		
		//return if there is an error
		if(validationError == true){
			sr.hasValidationError = validationError;
			sr.validationMessage = valMessage;
			sr.isSuccess = false;
			sr.sucessMessage = '';
			return sr;
		}

		c2g__codaBankAccount__c bankAcct = [SELECT Id, Bank_Account_Type__c FROM c2g__codaBankAccount__c WHERE Id = :bankAccountId];

		System.debug('***headerFields 1: ' + headerFields);
		System.debug('***headerFields.get(Litify_Matter__c): ' + headerFields.get('Litify_Matter__c'));
		
		if(headerFields.containsKey('Litify_Matter__c') && headerFields.get('Litify_Matter__c') != null && headerFields.get('Litify_Matter__c') != ''){
			

			String matterId = (String)headerFields.get('Litify_Matter__c');
			System.debug('***matterId' + matterId);
			litify_pm__Matter__c matter = [SELECT Id,
					Assigned_Office_Location__r.c2g__codaDimension1__c,	
					litify_pm__Case_Type__r.Dimension_2__c,
					litify_pm__Case_Type__r.Dimension_3__c
				FROM litify_pm__Matter__c
				WHERE Id = :matterId];
			System.debug('***matter' + matter);
			headerDim1 = matter.Assigned_Office_Location__r.c2g__codaDimension1__c;
			headerDim3 = matter.litify_pm__Case_Type__r.Dimension_2__c;
			headerDim3 = matter.litify_pm__Case_Type__r.Dimension_3__c;
		}

		List<c2g__codaPurchaseInvoice__c> pinInsertList = new List<c2g__codaPurchaseInvoice__c>();

		String invoiceDateString = headerFields.containsKey('c2g__InvoiceDate__c') ? (String)headerFields.get('c2g__InvoiceDate__c') : null;
		List<String> invoiceDateStringResult = invoiceDateString.split('-');
		Date invoiceDate = Date.newInstance(Integer.valueOf(invoiceDateStringResult[0]), Integer.valueOf(invoiceDateStringResult[1]), Integer.valueOf(invoiceDateStringResult[2]));
		
		String dueDateString = headerFields.containsKey('c2g__DueDate__c') ? (String)headerFields.get('c2g__DueDate__c') : null;
		List<String> dueDateStringResult = dueDateString.split('-');
		Date dueDate = Date.newInstance(Integer.valueOf(dueDateStringResult[0]), Integer.valueOf(dueDateStringResult[1]), Integer.valueOf(dueDateStringResult[2]));

		Try{
			//Create the PIN Header
			c2g__codaPurchaseInvoice__c pinHeader = new c2g__codaPurchaseInvoice__c(
				c2g__OwnerCompany__c         = company.Id,
				c2g__Dimension1__c           = headerDim1,
				c2g__Dimension2__c           = headerDim2,
				c2g__Dimension3__c           = headerDim3,
				OwnerId                      = company.OwnerId,
				Override_Account_Name__c     = (String)headerFields.get('Override_Account_Name__c') != '' ? (String)headerFields.get('Override_Account_Name__c') : null,
				Litify_Matter__c             = (String)headerFields.get('Litify_Matter__c') != '' ? (String)headerFields.get('Litify_Matter__c') : null,
				c2g__Account__c              = (String)headerFields.get('c2g__Account__c') != '' ? (String)headerFields.get('c2g__Account__c') : null,
				c2g__AccountInvoiceNumber__c = (String)headerFields.get('c2g__AccountInvoiceNumber__c') != '' ? (String)headerFields.get('c2g__AccountInvoiceNumber__c') : null,
				c2g__InvoiceDescription__c   = (String)headerFields.get('c2g__InvoiceDescription__c') != '' ? (String)headerFields.get('c2g__InvoiceDescription__c') : null,
				c2g__DeriveCurrency__c       = true,
				c2g__Period__c				 = (String)headerFields.get('c2g__Period__c') != '' ? (String)headerFields.get('c2g__Period__c') : null,
				c2g__DerivePeriod__c         = (String)headerFields.get('c2g__Period__c') != null ? false : true,
				Create_Litify_Expenses__c    = (Boolean)headerFields.get('Create_Litify_Expenses__c'),
				Check_Memo__c                = (String)headerFields.get('Check_Memo__c'),
				Payment_Bank_Account_Type__c = bankAcct.Bank_Account_Type__c,
				c2g__DeriveDueDate__c        = false,
				c2g__InvoiceDate__c          = invoiceDate,
				c2g__DueDate__c              = dueDate,
				Requested_By__c 			 = (String)headerFields.get('Requested_By__c') != '' ? (String)headerFields.get('Requested_By__c') : null
			);
			insert pinHeader;
			System.debug('What is Derive Period: ' + pinHeader.c2g__DerivePeriod__c);
			
			//Create the PIN Lines
			for(PayableInvoiceLineWrapper pinLineWrapper : pinLineWrapperList){
				c2g__codaPurchaseInvoiceExpenseLineItem__c expLine = new c2g__codaPurchaseInvoiceExpenseLineItem__c(
				c2g__PurchaseInvoice__c      = pinHeader.Id,
				c2g__GeneralLedgerAccount__c = pinLineWrapper.glaId,
				c2g__SetGLAToDefault__c      = false,
				c2g__LineDescription__c      = pinLineWrapper.lineDescription,
				Matter__c                    = pinLineWrapper.matterId,
				Expense_Type__c              = pinLineWrapper.expenseTypeId,
				c2g__Dimension1__c           = pinLineWrapper.dim1Id,
				c2g__Dimension2__c           = pinLineWrapper.dim2Id,
				c2g__Dimension3__c           = pinLineWrapper.dim3Id,
				c2g__Dimension4__c           = pinLineWrapper.dim4Id,
				c2g__NetValue__c             = pinLineWrapper.lineAmount
				);
				System.debug('pinWrapper : ' + pinLineWrapper);
				// System.debug('pinLineWrapper.lineDescription: ' + pinLineWrapper.lineDescription);
				System.debug('expLine.c2g__LineDescription__c: ' + expLine.c2g__LineDescription__c);
				pinLineInsertList.add(expLine);
			}
			insert pinLineInsertList;
				
			if(autoPost){
				List<c2g__codaPurchaseInvoice__c> pstedPins = FFAUtilities.postPINs(new List<ID>{pinHeader.Id});
			}
			pinHeader = [SELECT Id, Name FROM c2g__codaPurchaseInvoice__c WHERE ID = :pinHeader.Id];

			sr.hasValidationError = false;
			sr.validationMessage = '';
			sr.isSuccess = true;
			sr.sucessMessage = 'Sucessfully created Payable Invoice - ' + pinHeader.Name;
			return sr;
			
		}
		Catch(Exception e){
			System.debug('createPayableInvoiceController - error' + e.getMessage());
            // throw new AuraHandledException('That didn\'n work:'+'\n'+e.getMessage());
			throw new AuraHandledException(e.getMessage());
		}
    }
}