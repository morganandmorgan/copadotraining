@istest
public class TESTQuestionnarieClass{

    static string personRecordTypeId = '';
    static string businessRecordTypeId = '';

    static void setrecordTypes(){
       for(RecordType temp:[select id,ispersontype,DeveloperName  from recordType where sobjectType='account']){
          if(temp.ispersontype == true && temp.DeveloperName  == 'Person_Account')
                personRecordTypeId = temp.Id;
          else if(temp.ispersontype == false && temp.DeveloperName  == 'Business_Account')
                businessRecordTypeId = temp.id;
        }
    }

    static id createPersonAccount(string AccountLastName){
        setrecordTypes();
        system.debug('personRecordTypeId Main============='+personRecordTypeId);
        Account perAccountInst = New Account();
        perAccountInst.PersonHomePhone = '3232323232';
        perAccountInst.BillingPostalCode = '787878';
        perAccountInst.BillingStreet = 'PerAccountStreet';
        perAccountInst.BillingCity = 'PerAccountCity';
        perAccountInst.BillingState = 'PerAccountState';
        perAccountInst.FirstName = 'PerAccountFirstName';
        perAccountInst.LastName = AccountLastName;
        perAccountInst.BillingCountry = 'PerAccountCountry';
        perAccountInst.PersonEmail  = 'person@test.com';
        perAccountInst.recordtypeid  = personRecordTypeId;
        insert perAccountInst;
    return perAccountInst.id;
    }

    static testmethod void TESTQuestionnarieClassAction(){

        setrecordTypes();
        system.debug('personRecordTypeId ============='+personRecordTypeId);
        Account perAccountInst = New Account();
        perAccountInst.PersonHomePhone = '3232323232';
        perAccountInst.BillingPostalCode = '787878';
        perAccountInst.BillingStreet = 'PerAccountStreet';
        perAccountInst.BillingCity = 'PerAccountCity';
        perAccountInst.BillingState = 'PerAccountState';
        perAccountInst.FirstName = 'PerAccountFirstName';
        perAccountInst.LastName = 'PerAccountLastName';
        perAccountInst.BillingCountry = 'PerAccountCountry';
        perAccountInst.recordtypeid  = personRecordTypeId;
        insert perAccountInst;


        Account busAccountInst = New Account();
        busAccountInst.Name = 'busAccountName';
        busAccountInst.Phone = '4949494949';
        busAccountInst.BillingPostalCode = '787878';
        busAccountInst.BillingCity = 'busAccountCity';
        busAccountInst.BillingState = 'busAccountState';
        busAccountInst.BillingStreet = 'busAccountStreet';
        busAccountInst.BillingCountry = 'busAccountCountry';
        busAccountInst.recordtypeid  = businessRecordTypeId;
        insert busAccountInst;

        Intake__c IntakeInst = New Intake__c();
        IntakeInst.Case_Type__c = 'Benicar';
        IntakeInst.Client__c = perAccountInst.id;
        Insert IntakeInst;

        Lawsuit__c LWinst = new Lawsuit__c();
        LWinst.Intake__c = IntakeInst.id;
        insert LWinst;

        Questionnaire__c quesInst = new Questionnaire__c();
        quesInst.Lawsuits__c = LWinst.id;
        insert quesInst;

        PageReference pref = Page.QuestionnaireForm;
        pref.getParameters().put('id',quesInst.id);
        Test.setCurrentPage(pref);

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(quesInst);
        QuestionnarieClass ext = new QuestionnarieClass(stdCtrl);

        ext.ques.Client__c = createPersonAccount('ClientPerAccount');
        ext.fillclientdetails();

        ext.saveQuest();
        ext.cancelCustom();

        ext.ques.Client__c = LWinst.Id;
        ext.fillclientdetails();
        ext.saveQuest();

        System.currentPageReference().getParameters().put('retURL', 'test');
        QuestionnarieClass extcancelelsePart = new QuestionnarieClass(stdCtrl);
        ext.cancelCustom();

        //// LawsuitId Passed in the Constructor
        System.currentPageReference().getParameters().put('PassedLawsuitId', LWinst.Id);
        System.currentPageReference().getParameters().put('FromWhere', 'Opportunity');
        Questionnaire__c quesInstNew = new Questionnaire__c();
        ApexPages.StandardController stdCtrlNew = new ApexPages.StandardController(quesInstNew);
        QuestionnarieClass extNew = new QuestionnarieClass(stdCtrlNew);




    }



}