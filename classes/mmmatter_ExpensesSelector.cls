public class mmmatter_ExpensesSelector
    extends mmlib_SObjectSelector
    implements mmmatter_IExpensesSelector
{
    public static mmmatter_IExpensesSelector newInstance()
    {
        return (mmmatter_IExpensesSelector) mm_Application.Selector.newInstance(litify_pm__Expense__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Expense__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            litify_pm__Expense__c.CostType__c
            , litify_pm__Expense__c.Create_Payable_Invoice__c
            , litify_pm__Expense__c.DateInvoiced__c
            , litify_pm__Expense__c.ExternalSourceId__c
            , litify_pm__Expense__c.FFA_Company_Name__c
            , litify_pm__Expense__c.FFA_Company__c
            , litify_pm__Expense__c.GL_Account_Mapping__c
            , litify_pm__Expense__c.Invoice__c
            , litify_pm__Expense__c.Litify_Load_Id__c
            , litify_pm__Expense__c.MatterReferenceNumber__c
            , litify_pm__Expense__c.NotBillable__c
            , litify_pm__Expense__c.ParentBusiness__c
            , litify_pm__Expense__c.PayableTo__c
            , litify_pm__Expense__c.Payable_Invoice_Created__c
            , litify_pm__Expense__c.Payable_Invoice__c
            , litify_pm__Expense__c.RequestedBy__c
            , litify_pm__Expense__c.Timestamp__c
            , litify_pm__Expense__c.Vendor_Invoice_Number__c
            , litify_pm__Expense__c.litify_pm__Amount__c
            , litify_pm__Expense__c.litify_pm__Date__c
            , litify_pm__Expense__c.litify_pm__ExpenseType2__c
            , litify_pm__Expense__c.litify_pm__Expense_Description__c
            , litify_pm__Expense__c.litify_pm__Matter__c
            , litify_pm__Expense__c.litify_pm__Note__c
            , litify_pm__Expense__c.litify_pm__Status__c
            , litify_pm__Expense__c.litify_pm__TaskType__c
            , litify_pm__Expense__c.Amount_Recovered_Settlement__c
            , litify_pm__Expense__c.Amount_Recovered_Client__c
            , litify_pm__Expense__c.Written_Off_Amount__c
            , litify_pm__Expense__c.Expense_Written_Off__c
            , litify_pm__Expense__c.Fully_Recovered__c
            , litify_pm__Expense__c.Amount_Remaining_Settlement__c
            , litify_pm__Expense__c.Amount_Remaining_Client__c
            , litify_pm__Expense__c.Amount_Remaining_to_Recover__c
            , litify_pm__Expense__c.Amount_Recovered__c
        };
    }

    /**
     *  Primary selector for litify_pm__Expense__c SObject
     *
     *  @param idSet the Set of record ids to query
     *  @return list of litify_pm__Expense__c records
     */
    public List<litify_pm__Expense__c> selectById(Set<Id> idSet)
    {
        return (List<litify_pm__Expense__c>) selectSObjectsById(idSet);
    }

    public List<litify_pm__Expense__c> selectByMatterId(Set<Id> matterIds)
    {
        fflib_QueryFactory query = newQueryFactory();
        //where conditions:
        query.setCondition('litify_pm__Matter__c in :matterIds');

        return (List<litify_pm__Expense__c>) Database.query(query.toSOQL());
    }

    public List<litify_pm__Expense__c> selectByMatterId_ResetAllocation(Set<Id> matterIds)
    {
        fflib_QueryFactory query = newQueryFactory();

        //where conditions:
        query.setCondition('litify_pm__Matter__c in :matterIds');

        query.addOrdering(litify_pm__Expense__c.CostType__c, fflib_QueryFactory.SortOrder.ASCENDING);
        query.addOrdering(litify_pm__Expense__c.litify_pm__Date__c, fflib_QueryFactory.SortOrder.ASCENDING);

        return (List<litify_pm__Expense__c>) Database.query(query.toSOQL());
    }

    public List<litify_pm__Expense__c> selectByMatterId_Allocation(Set<Id> matterIds)
    {
        fflib_QueryFactory query = newQueryFactory();

        //additional fields:
        query.selectField('litify_pm__ExpenseType2__r.Name');

        //where conditions:
        query.setCondition('litify_pm__Matter__c in :matterIds AND Fully_Recovered__c = false');

        query.addOrdering(litify_pm__Expense__c.CostType__c, fflib_QueryFactory.SortOrder.ASCENDING);
        query.addOrdering(litify_pm__Expense__c.litify_pm__Date__c, fflib_QueryFactory.SortOrder.ASCENDING);

        return (List<litify_pm__Expense__c>) Database.query(query.toSOQL());
    }
}