public class mmcommon_UserInfo
{
    private User user_m = null;

    public mmcommon_UserInfo()
    {
        this(UserInfo.getUserId());
    }

    public mmcommon_UserInfo(Id userId)
    {
        List<User> userList = mmcommon_UsersSelector.newInstance().selectByIdWithProfileRolePermissionSets(new Set<Id> {userId});

        if (!userList.isEmpty())
        {
            user_m = userList.get(0);
        }
    }

    private Boolean processedRoleAndStuff = false;
    private UserRole userRole_m = null;
    private Profile profile_m = null;
    private List<PermissionSet> permissionSetList = new List<PermissionSet>();
    private List<String> skillList = new List<String>();

    public Profile getProfile()
    {
        getRoleProfileAndPermissionsets();
        return profile_m;
    }

    public UserRole getUserRole()
    {
        getRoleProfileAndPermissionsets();
        return userRole_m;
    }

    public List<PermissionSet> getPermissionSetList()
    {
        getRoleProfileAndPermissionsets();
        return permissionSetList;
    }

    // -------------- USAGE EXPLANATION #1 --------------
    // The following methods assume an OR relationship when comparing a user's
    // traits to the expected list.  If a user's trait matches any one of the
    // expect values then True is returned.

    // For example ...
    // Assume the user has the traits 'one', 'two', and 'three'.
    // For the expected list 'three', 'four', and 'five', the result is True.
    // For the expected list 'four', 'five', and 'six', the result is False.
    // For the expected list 'one', 'two', and 'three', the result is True.
    // For the expected list 'eleven', the result is False.
    // For the expected list 'two', the result is True.

    public Boolean hasProfileName(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        getRoleProfileAndPermissionsets();

        nameList = forceToLowerCase( nameList.clone() );

        if (profile_m != null)
        {
            return new Set<String>(nameList).contains( profile_m.Name.toLowerCase() );
        }

        return false;
    }

    public Boolean hasUserRoleName(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        getRoleProfileAndPermissionsets();

        nameList = forceToLowerCase(nameList.clone());

        if (userRole_m != null)
        {
            return new Set<String>(nameList).contains(userRole_m.Name.toLowerCase());
        }

        return false;
    }

    public Boolean hasPermissionSetName(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        nameList = forceToLowerCase(nameList.clone());

        getRoleProfileAndPermissionsets();

        if (permissionSetList != null)
        {
            for (PermissionSet ps : permissionSetList)
            {
                for (String psName : nameList)
                {
                    if (ps.Name.equalsIgnoreCase(psName))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public Boolean hasSkillName(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        nameList = forceToLowerCase(nameList.clone());

        getRoleProfileAndPermissionsets();

        if (skillList != null)
        {
            for (String userSkill : skillList)
            {
                for (String skillExpected : nameList)
                {
                    if (userSkill.equalsIgnoreCase(skillExpected))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    // -------------- End of usage explanation #1. --------------

    // -------------- USAGE EXPLANATION #2 --------------
    // Like Explanation #1, these methods assume an OR relationship.
    // The following methods look for the name-list items in the respective
    // user trait.  If "admin" is one of the name-list items, it would match
    // "System Administrator" or "Case Administrator" or "Badminton player".
    // BE VERY CAREFUL IMPLEMENTING THESE METHODS.

    public Boolean hasProfileNameLike(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        getRoleProfileAndPermissionsets();

        if (profile_m != null)
        {
            nameList = forceToLowerCase( nameList.clone() );

            for (String name : nameList)
            {
                if (stringLeftContainsRight(profile_m.Name, name, true))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public Boolean hasUserRoleNameLike(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        getRoleProfileAndPermissionsets();

        if (userRole_m != null)
        {
            nameList = forceToLowerCase(nameList.clone());

            for (String name : nameList)
            {
                if (stringLeftContainsRight(userRole_m.Name, name, true))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public Boolean hasPermissionSetNameLike(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        getRoleProfileAndPermissionsets();

        if (permissionSetList != null)
        {
            for (PermissionSet ps : permissionSetList)
            {
                nameList = forceToLowerCase(nameList.clone());

                for (String psName : nameList)
                {
                    if (stringLeftContainsRight(ps.Name, psName, true))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public Boolean hasSkillNameLike(List<String> nameList)
    {
        if ( nameList == null || nameList.isEmpty() )
        {
            return false;
        }

        getRoleProfileAndPermissionsets();

        if (skillList != null)
        {
            for (String userSkill : skillList)
            {
                nameList = forceToLowerCase(nameList.clone());

                for (String skillExpected : nameList)
                {
                    if (stringLeftContainsRight(userSkill, skillExpected, true))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    // -------------- End of usage explanation #2. --------------

    private void getRoleProfileAndPermissionsets()
    {
        if (user_m != null && !processedRoleAndStuff)
        {
            profile_m = user_m.Profile;
            userRole_m = user_m.UserRole;

            permissionSetList = new List<PermissionSet>();

            for (PermissionSetAssignment psa : user_m.PermissionSetAssignments )
            {
                permissionSetList.add( psa.PermissionSet );
            }

            if (String.isNotBlank(user_m.Skill__c))
            {
                for (String s : user_m.Skill__c.split(';'))
                {
                    skillList.add(s);
                }
            }

            processedRoleAndStuff = true;
        }
    }

    private List<String> forceToLowerCase(List<String> stringList)
    {
        return mmlib_Utils.forceToCase(stringList, mmlib_Utils.TextCase.Lower);
    }

    private Boolean stringLeftContainsRight(String left, String right, Boolean caseInsensitiveCompare)
    {
        if (String.isBlank(left))
        {
            return false;
        }

        if (caseInsensitiveCompare)
        {
            return left.containsIgnoreCase(right);
        }

        return left.contains(right);
    }
}