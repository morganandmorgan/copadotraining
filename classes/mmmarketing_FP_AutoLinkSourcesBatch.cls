/**
 *  mmmarketing_FP_AutoLinkSourcesBatch
 */
public class mmmarketing_FP_AutoLinkSourcesBatch
    implements Database.Batchable<SObject>
{
    public mmmarketing_FP_AutoLinkSourcesBatch () { }

    private Set<String> sourceValueSet;
    private integer batchSize = 200;

    public mmmarketing_FP_AutoLinkSourcesBatch(Set<String> sourceValueSet)
    {
        this.sourceValueSet = sourceValueSet;
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return mmmarketing_FinancialPeriodsSelector.newInstance().selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue( sourceValueSet );
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            mmmarketing_TrackingInformations trackingInformations = new mmmarketing_TrackingInformations( (list<Marketing_Tracking_Info__c>)scope );

            trackingInformations.autoLinkToMarketingInformationIfRequiredInQueueableMode();
        }
        catch (Exception e)
        {
            System.debug('Error executing batch ' + e);
        }
    }

    public void finish(Database.BatchableContext context)
    {

    }

    public mmmarketing_FP_AutoLinkSourcesBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public void execute()
    {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticFPLinkingEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticFPLinkingEnabled__c )
        {
            Database.executeBatch( this, batchSize );
        }
    }
}