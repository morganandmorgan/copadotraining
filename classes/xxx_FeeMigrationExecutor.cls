/**
 *  script used to manage the fee based expense migration from x_CPExpenseStaging__c to Deposit__c records related to litify_pm__Matter__c records
 *
 *  @usage String query = '<<query goes here>>';
 *         new mmlib_GenericBatch( xxx_FeeMigrationExecutor.class, query).setBatchSizeTo(100).execute();
 */
public class xxx_FeeMigrationExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    private static final id morganAndMorganDisabilitiesAssociatesRecordId;
    private static final id cpDataMigrationUserId;

    private static map<String, x_CPTrancodeToLitifyExpTypeMap__c> expenseTypeMap = new map<String, x_CPTrancodeToLitifyExpTypeMap__c>();

    private map<String, SObject> usersByEmailMap = new map<String, SObject>();

    private map<String, SObject> mattersByLitifyLoadIdMap = new map<String, SObject>();

    static
    {
        cpDataMigrationUserId = [select id, name, username from user where username like 'cpdatamigration@forthepeople.com%' limit 1].id;

        list<x_CPTrancodeToLitifyExpTypeMap__c> allTrancodeToExpTypeMappings = [select Id, Name, OfficeName__c, trancode_sk__c, FeeDepositType__c
                                                                                     , ExternalIdCode__c, ExpenseType__c, Expense_Type_Description__c
                                                                                  from x_CPTrancodeToLitifyExpTypeMap__c
                                                                                 order by OfficeName__c, trancode_sk__c];

        // organize this list of mappings by a key of "OfficeName__c + ';' + trancode_sk__c"
        for (x_CPTrancodeToLitifyExpTypeMap__c trancodeToExpTypeMap : allTrancodeToExpTypeMappings )
        {
            expenseTypeMap.put( trancodeToExpTypeMap.OfficeName__c.toLowercase() + ';' + trancodeToExpTypeMap.trancode_sk__c.toLowercase(), trancodeToExpTypeMap);
        }
    }

    public void run( list<SObject> scope )
    {
        x_CPExpenseStaging__c workingRecord = null;
        ExpensePairing aPairing = null;

        list<Deposit__c> depositRecordsToInsert = new list<Deposit__c>();
        list<Deposit__c> depositRecordsToUpdate = new list<Deposit__c>();
        map<id, x_CPExpenseStaging__c> stagingRecordsToUpdateIdMap = new map<id, x_CPExpenseStaging__c>();
        list<ExpensePairing> thePairings = new list<ExpensePairing>();
        set<id> existingDepositIdSet = new set<id>();
        Set<String> matterLitifyLoadIdSet = new Set<String>();
        Set<String> stagingExpenseCreateUserEmailSet = new Set<String>();
        map<id, Deposit__c> litifyExpenseByIdMap = new map<id, Deposit__c>();

        // organize the scope records into the workingRecordsByRelatedMatterAndTransMap
        for ( SObject record : scope )
        {
//system.debug( 'mark B1' );
            workingRecord = (x_CPExpenseStaging__c)record;

            matterLitifyLoadIdSet.add( workingRecord.RelatedMatterLitifyLoadId__c );

            if ( string.isNotBlank( workingRecord.create_user_email__c ) )
            {
                stagingExpenseCreateUserEmailSet.add( workingRecord.create_user_email__c );
            }

            // determine if there are Deposit__c records already setup for these staging records
            if ( workingRecord.RelatedDeposit__c != null )
            {
                existingDepositIdSet.add( workingRecord.RelatedDeposit__c );
            }
        }
        findMattersByLitifyLoadIds( matterLitifyLoadIdSet );

        findUsersByStagingExpenseCreateUserEmail( stagingExpenseCreateUserEmailSet );

        if ( ! existingDepositIdSet.isEmpty() )
        {
            litifyExpenseByIdMap.putAll( mmffa_DepositsSelector.newInstance().selectById( existingDepositIdSet ) );
        }

        for ( SObject record : scope )
        {
            workingRecord = (x_CPExpenseStaging__c)record;
//system.debug( 'mark D1');
//system.debug( 'working x_CPExpenseStaging__c record id = ' + workingRecord.id);
            aPairing = new ExpensePairing().setStagingRecord( workingRecord )
                    .setDepositRecord( litifyExpenseByIdMap.get( workingRecord.RelatedDeposit__c ) )
                    .setMattersMap( this.mattersByLitifyLoadIdMap )
                    .setUsersByEmailMap( this.usersByEmailMap )
                    .processStagingRecord();

            if ( aPairing.isValidPairing() )
            {
                if ( aPairing.depositRecord.id == null )
                {
                    depositRecordsToInsert.add( aPairing.depositRecord );
                }
                else
                {
                    depositRecordsToUpdate.add( aPairing.depositRecord );
                }
            }

            thePairings.add( aPairing );
        }
//system.debug( 'mark E1');
        // make saves
        database.insert( depositRecordsToInsert, false );

        database.update( depositRecordsToUpdate, false );
//system.debug( 'mark F1');
        // link all expense records to staging records via aPairing.linkDepositRecordToStagingRecord() method
        for (ExpensePairing pairing : thePairings)
        {
            //system.debug( 'pairing.getStagingRecord().id == ' + pairing.getStagingRecord().id);
            pairing.linkDepositRecordToStagingRecord();

            stagingRecordsToUpdateIdMap.put( pairing.getStagingRecord().id, pairing.getStagingRecord() );
        }

        // save staging records
        list<Database.SaveResult> srList = database.update( stagingRecordsToUpdateIdMap.values(), false );
        //system.debug( srList );
    }

    private void findMattersByLitifyLoadIds(Set<String> matterLitifyLoadIdSet)
    {
        list<litify_pm__Matter__c> matters = [select id, name, referencenumber__c, CP_Case_Number__c, litify_load_id__c
                                                from litify_pm__Matter__c
                                               where litify_load_id__c = :matterLitifyLoadIdSet];

        this.mattersByLitifyLoadIdMap = generateSObjectMapByUniqueField( matters, litify_pm__Matter__c.litify_load_id__c );
        system.debug( this.mattersByLitifyLoadIdMap.keyset() );
    }

    private void findUsersByStagingExpenseCreateUserEmail( Set<String> stagingExpenseCreateUserEmailSet )
    {
        if ( ! stagingExpenseCreateUserEmailSet.isEmpty() )
        {
            String workingQuery = 'select id, name, username, email, FederationIdentifier, IsActive from User';

            boolean isFirstCriteriaElement = true;

            for ( String stagingExpenseCreateUserEmail : stagingExpenseCreateUserEmailSet )
            {
                if ( stagingExpenseCreateUserEmail != null )
                {
                    if ( isFirstCriteriaElement )
                    {
                        workingQuery += ' where';
                        isFirstCriteriaElement = false;
                    }
                    else
                    {
                        workingQuery += ' or';
                    }

                    workingQuery += ' (' + User.Username + ' like \'' + stagingExpenseCreateUserEmail + '%\''
                                +' or ' + User.Email + ' like \'' + stagingExpenseCreateUserEmail + '%\''
                                +' )';
                }
            }

            this.usersByEmailMap = generateSObjectMapByUniqueField( Database.query( workingQuery ), User.Email );
            system.debug( this.usersByEmailMap.keyset() );
        }
        else
        {
            this.usersByEmailMap = new map<String, SObject>();
        }
    }

    private map<String, SObject> generateSObjectMapByUniqueField( final List<SObject> sObjects
                                                                , final Schema.SObjectField stringFieldToBeKey )
    {
        map<String, SObject> outputMap = new map<String, SObject>();

        String stringValue = null;

        if (sObjects != null && ! sObjects.isEmpty())
        {
            for (SObject sobj : sObjects)
            {
                stringValue = (String)sobj.get(stringFieldToBeKey);

                if ( stringValue.containsIgnoreCase('=forthepeople.com@example.com') )
                {
                    stringValue = stringValue.substringBefore('=forthepeople.com@example.com') + '@forthepeople.com';
                }

                if ( ! outputMap.containsKey( stringValue.toLowercase() ) )
                {
                    outputMap.put( stringValue.toLowercase(), sobj );
                }
            }
        }

        return outputMap;
    }


    public class ExpensePairing
    {
        private Deposit__c depositRecord;
        private x_CPExpenseStaging__c stagingRecord;
        private boolean isPairingValid = true;
        private map<String, SObject> mattersByLitifyLoadIdMap;
        private map<String, SObject> usersByEmailMap;

        public ExpensePairing()
        {
            system.debug( 'new ExpensePairing created ');
        }

        public ExpensePairing setDepositRecord( Deposit__c depositRecord )
        {
            this.depositRecord = depositRecord;

            return this;
        }

        public ExpensePairing setStagingRecord( x_CPExpenseStaging__c stagingRecord )
        {
            this.stagingRecord = stagingRecord;

            return this;
        }

        public ExpensePairing setMattersMap( map<String, SObject> mattersByLitifyLoadIdMap )
        {
            this.mattersByLitifyLoadIdMap = mattersByLitifyLoadIdMap;

            return this;
        }

        public ExpensePairing setUsersByEmailMap( map<String, SObject> usersByEmailMap )
        {
            this.usersByEmailMap = usersByEmailMap;

            return this;
        }

        public Deposit__c getDepositRecord()
        {
            return this.depositRecord;
        }

        public x_CPExpenseStaging__c getStagingRecord()
        {
            return this.stagingRecord;
        }

        public ExpensePairing processStagingRecord()
        {
            system.debug( 'EP1' );
            if ( this.stagingRecord.RelatedDeposit__c != null
                && this.depositRecord != null )
            {
//system.debug( 'EP1A' );
                updateDepositRecordDataFromStagingRecord();
            }
            else
            {
//system.debug( 'EP1B' );
                generateDepositRecordFromStagingRecord();
            }

            return this;
        }

        public boolean isValidPairing()
        {
            if ( this.depositRecord.Matter__c == null )
            {
                recordValidationFailure('MATTER_NOT_FOUND', 'Related Matter record was not found');
            }

            if ( this.depositRecord.Deposit_Type__c == null )
            {
                recordValidationFailure('DEPOSIT_TYPE_NOT_FOUND', 'Deposit Type was not found');
            }

            return this.isPairingValid;
        }

        public void linkDepositRecordToStagingRecord()
        {
            if ( this.isPairingValid )
            {
                this.stagingRecord.RelatedDeposit__c = this.depositRecord.id;
                this.stagingRecord.MigrationStatus__c = 'Successful';
                this.stagingRecord.MigratedTime__c = datetime.now();
            }
        }

        private void recordValidationFailure(String statusCode, String reason)
        {
            this.stagingRecord.MigrationStatus__c = 'Failed = ' + statusCode;
            this.stagingRecord.ProcessingAnomalyComments__c = (string.isBlank(this.stagingRecord.ProcessingAnomalyComments__c) ? '' : this.stagingRecord.ProcessingAnomalyComments__c ) + reason + '\n';
            this.isPairingValid = false;
        }

        private void updateDepositRecordDataFromStagingRecord()
        {
            this.depositRecord.Amount__c = this.stagingRecord.amount__c;
            this.depositRecord.Bank_Account_to_Deposit_In__c = null;
            this.depositRecord.Check_Date__c = date.valueOf( this.stagingRecord.create_date__c );//-- date check was written -- not critical
            this.depositRecord.Check_Number__c = null; //-- probably null
            this.depositRecord.Deposit_Created__c = true;
            this.depositRecord.Deposit_Description__c = 'Migrated from CP'; //probably null or "Migrated from CP"
            this.depositRecord.Deposit_Needed__c = false;
            this.depositRecord.Journal_Line_Item__c = null;
            this.depositRecord.Litify_Load_Id__c = this.stagingRecord.SfdcLoadId__c;

            if ( expenseTypeMap.containsKey( this.stagingRecord.OfficeName__c.toLowercase() + ';' + this.stagingRecord.trancode_sk__c.toLowercase() ) )
            {
                this.depositRecord.Deposit_Type__c = expenseTypeMap.get( this.stagingRecord.OfficeName__c.toLowercase() + ';' + this.stagingRecord.trancode_sk__c.toLowercase() ).FeeDepositType__c;
            }
            else
            {
                this.depositRecord.Deposit_Type__c = null;
            }

            if ( String.isNotBlank( this.stagingRecord.create_user_email__c )
                && this.usersByEmailMap.containsKey( this.stagingRecord.create_user_email__c.toLowercase() ) )
            {
                this.depositRecord.OwnerId = this.usersByEmailMap.get( this.stagingRecord.create_user_email__c.toLowercase() ).id;
            }
            // handle special CP users CPYTRK and CNV_PJW differently
            else
            {
                //system.debug( 'EP-updateDepositRecordDataFromStagingRecord-4');
                this.depositRecord.OwnerId = cpDataMigrationUserId;
            }
        }

        private void generateDepositRecordFromStagingRecord()
        {
            this.depositRecord = new Deposit__c();

            if ( this.mattersByLitifyLoadIdMap.containsKey( stagingRecord.RelatedMatterLitifyLoadId__c.toLowercase() ) )
            {
                this.depositRecord.Matter__c = this.mattersByLitifyLoadIdMap.get( stagingRecord.RelatedMatterLitifyLoadId__c.toLowercase() ).id;
            }

            updateDepositRecordDataFromStagingRecord();
        }
    }
}