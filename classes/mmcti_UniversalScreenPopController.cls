public class mmcti_UniversalScreenPopController {

    public PageReference redir() {
        CTI_Screen_Pop_Redirect_Setting__mdt setting = mmcti_UniversalScreenPopService.getScreenPopSetting();
        if (setting == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to determine CTI Screen Pop setting'));
            return null;
        }
        
        PageReference pr = new PageReference(setting.Redirect_URL__c);
        for (String key : ApexPages.currentPage().getParameters().keySet()) {
            pr.getParameters().put(key, ApexPages.currentPage().getParameters().get(key));
        }
        for (String key : ApexPages.currentPage().getHeaders().keySet()) {
            pr.getHeaders().put(key, ApexPages.currentPage().getheaders().get(key));
        }
        pr.setRedirect(true);
        return pr;
    }

    public PageReference outboundHandler()
    {
        String recordId = Apexpages.currentPage().getParameters().get('id');

        if(String.isEmpty(recordId) || !mmlib_Utils.isValidSalesforceId(recordId)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Message: A valid Salesforce record id is required.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Usage: /apex/CTIScreenPopOutbound?id=<SalesforceRecordId>'));
            return null;
        }

        return new PageReference('/' + recordId);
    }
}