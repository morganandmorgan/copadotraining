public class mmlitifylrn_CredentialsSelector
    extends mmlib_SObjectSelector
    implements mmlitifylrn_ICredentialsSelector
{
    public static mmlitifylrn_ICredentialsSelector newInstance()
    {
        return (mmlitifylrn_ICredentialsSelector) mm_Application.Selector.newInstance(mmlitifylrn_ThirdPartyCredential__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return mmlitifylrn_ThirdPartyCredential__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            mmlitifylrn_ThirdPartyCredential__c.AccessToken__c,
            mmlitifylrn_ThirdPartyCredential__c.FirmExternalID__c,
            mmlitifylrn_ThirdPartyCredential__c.RefreshToken__c,
            mmlitifylrn_ThirdPartyCredential__c.Username__c,
            mmlitifylrn_ThirdPartyCredential__c.Last_Sync__c
        };
    }

    public List<mmlitifylrn_ThirdPartyCredential__c> selectById( Set<id> idSet )
    {
        return (List<mmlitifylrn_ThirdPartyCredential__c>) selectSObjectsById(idSet);
    }

    public List<mmlitifylrn_ThirdPartyCredential__c> selectByName( set<string> nameSet )
    {
        return Database.query( newQueryFactory().setCondition( mmlitifylrn_ThirdPartyCredential__c.Name + ' in :nameSet' ).toSOQL() );
    }

    public List<mmlitifylrn_ThirdPartyCredential__c> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }
}