/**
 * mmintake_IntakesSelectorTest
 * @description Test for IntakeTrigger Selector class.
 * @author Jeff Watson
 * @date 5/7/2019
 */
@IsTest
private class mmintake_IntakesSelectorTest {

    @TestSetup
    private static void testSetup() {
        Account attorneyAccount = TestUtil.createAccount('Attorney Account');
        insert attorneyAccount;

        Contact attorney = TestUtil.createContact(attorneyAccount.Id);
        insert attorney;

        Account personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        Intake__c intake = TestUtil.createIntake(personAccount);
        intake.Client__c = personAccount.Id;
        intake.Generating_Attorney__c = attorney.Id;
        intake.UniqueSubmissionKey__c = 'uniquekey';
        intake.Cisco_Call_ID__c = 'callID';
        insert intake;
    }

    @IsTest
    private static void selectByIdTest() {
        List<Intake__c> intakes = [SELECT Id FROM Intake__c];
        Set<Id> intakeIds = new Set<Id>{
                intakes[0].Id
        };

        Test.startTest();
        mmintake_IntakesSelector.newInstance().selectById(intakeIds);
        Test.stopTest();
    }

    @IsTest
    private static void selectBySubmissionIdTest() {
        Test.startTest();
        mmintake_IntakesSelector.newInstance().selectBySubmissionId(new Set<String>());
        Test.stopTest();
    }

    @IsTest
    private static void testMostRecentMesoReferal() {
        Test.startTest();
        mmintake_IntakesSelector.newInstance().selectMostRecentMesoReferral();
        Test.stopTest();
    }

    @IsTest
    private static void selectByIdWithCallerClientInjuredParty() {
        Set<Id> intakeIds = new Set<Id>{
                [SELECT Id FROM Intake__c LIMIT 1].Id
        };

        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByIdWithCallerClientAndInjuredParty(intakeIds);
        Test.stopTest();

        System.assert(intakes != null);
        System.assertEquals(1, intakes.size());
    }

    @IsTest
    private static void selectByIdWithCallerClientInjuredPartyAndAdditionalFields() {
        Set<Id> intakeIds = new Set<Id>{
                [SELECT Id FROM Intake__c LIMIT 1].Id
        };
        List<Schema.FieldSet> fieldSets = new List<Schema.FieldSet>();
        Schema.FieldSet relatedEventFieldSet;

        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByIdWithCallerClientInjuredPartyAndAdditionalFields(intakeIds, fieldSets, relatedEventFieldSet);
        Test.stopTest();

        System.assert(intakes != null);
        System.assertEquals(1, intakes.size());
    }

    @IsTest
    private static void selectByClientId() {
        Set<Id> clientIds = new Set<Id>{
                [SELECT Id FROM Account LIMIT 1].Id
        };

        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByClientId(clientIds);
        Test.stopTest();

        System.assert(intakes != null);
    }

    @IsTest
    private static void selectWithFieldsetByClientId() {
        Set<Id> clientIds = new Set<Id>{
                [SELECT Id FROM Account LIMIT 1].Id
        };
        Schema.FieldSet intakeFieldSet = SObjectType.Intake__c.fieldSets.Contact_Search_Modal;

        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectWithFieldsetByClientId(intakeFieldSet, clientIds);
        Test.stopTest();

        System.assert(intakes != null);
    }

    @IsTest
    private static void selectByGeneratingAttorneyAndCreatedDateRange() {
        Set<Id> attorneyIds = new Set<Id>{
                [SELECT Id FROM Contact LIMIT 1].Id
        };
        Date startDate = Date.today().addDays(-1);
        Date endDate = Date.today().addDays(1);

        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByGeneratingAttorneyAndCreatedDateRange(attorneyIds, startDate, endDate);
        Test.stopTest();

        System.assert(intakes != null);
        System.assertEquals(1, intakes.size());
    }

    @IsTest
    private static void selectByUniqueSubmissionKey() {
        Set<String> uniqueSubmissionKeys = new Set<String>{
                'uniquekey'
        };

        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByUniqueSubmissionKey(uniqueSubmissionKeys);
        Test.stopTest();

        System.assert(intakes != null);
        System.assertEquals(1, intakes.size());
    }

    @IsTest
    private static void selectByCiscoCallId() {
        Test.startTest();
        List<Intake__c> intakes = mmintake_IntakesSelector.newInstance().selectByCiscoCallId('callID');
        Test.stopTest();

        System.assert(intakes != null);
        System.assertEquals(1, intakes.size());
    }

} //class