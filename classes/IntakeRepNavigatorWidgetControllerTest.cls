@isTest
private class IntakeRepNavigatorWidgetControllerTest {
  static IntakeRepNavigatorWidgetQueue__c testQueue;
  static Intake__c intake_from_NY;
  static Account acct;
  static {
    testQueue = new IntakeRepNavigatorWidgetQueue__c(Name='Test Queue', Query__c = '[{"column": "Caller_First_Name__c", "operator": "<>", "argument": "BAD"}, {"column":"CreatedDate", "operator": ">=", "argument": "Today"}, {"column":"litigation__c", "operator": "=", "argument": "MASS TORT"}, {"column": "Client__r.AccountNumber", "operator": "<>", "argument": "1"}]', Sorting__c = '[{"column": "Last_Correct_Contact__c", "order": "DESC"}]');
    insert testQueue;

    acct = new Account(FirstName = 'Test', LastName = 'Account');
    insert acct;

    intake_from_NY = new Intake__c(Caller_First_Name__c='Test Intake from NY', Client__c = acct.id, Phone__c = '212-111-1111', Last_Correct_Contact__c = DateTime.now().addHours(-2), litigation__c = 'MASS TORT');
    insert intake_from_NY;

    PageReference PageRef = Page.IntakeRepNavigatorWidget;
    Test.setCurrentPage(PageRef);
  }

	@isTest static void OnLoad_ContainsListOfQueues() {
		IntakeRepNavigatorWidgetController controller = new IntakeRepNavigatorWidgetController();
    controller.OnLoad();
    System.assertEquals(1, controller.QueueList.size());
    System.assertEquals(testQueue.Id, controller.QueueList[0].getValue());
    System.assertEquals(testQueue.Name, controller.QueueList[0].getLabel());
	}

  @isTest static void OnLoad_IfNoCookie_SelectedQueueIsBlank() {
    IntakeRepNavigatorWidgetController controller = new IntakeRepNavigatorWidgetController();
    controller.OnLoad();

    System.assertEquals(null, controller.SelectedQueueId);
  }
	
	@isTest static void OnLoad_SelectsQueueBasedOnCookie() {
		Cookie cook = new Cookie('IntakeRepNavigatorWidget_SelectedQueueId', testQueue.Id, null, -1, false);
    ApexPages.currentPage().setCookies(new Cookie[]{cook});

    IntakeRepNavigatorWidgetController controller = new IntakeRepNavigatorWidgetController();
    controller.OnLoad();

    System.assertEquals(testQueue.Id, controller.SelectedQueueId);
	}

  @isTest static void goToNext_EarliestCallTimeIsAfterCurrentTimeInTimezone_IntakeIsSkipped() {
    testQueue.EarliestCallTime__c = Integer.valueOf(DateTime.now().addHours(1).format('H', 'EST'));
    testQueue.LatestCallTime__c = Integer.valueOf(DateTime.now().addHours(2).format('H', 'EST'));
    update testQueue;

    IntakeRepNavigatorWidgetController controller = new IntakeRepNavigatorWidgetController();
    controller.SelectedQueueId = testQueue.Id;

    controller.GoToNext();
    System.assertEquals(null, controller.NextId);
  }


  @isTest static void goToNext_LatestCallTimeIsBeforeCurrentTimeInTimezone_IntakeIsSkipped() {
    testQueue.EarliestCallTime__c = Integer.valueOf(DateTime.now().addHours(-2).format('H', 'EST'));
    testQueue.LatestCallTime__c = Integer.valueOf(DateTime.now().addHours(-1).format('H', 'EST'));
    update testQueue;

    IntakeRepNavigatorWidgetController controller = new IntakeRepNavigatorWidgetController();
    controller.SelectedQueueId = testQueue.Id;

    controller.GoToNext();
    System.assertEquals(null, controller.NextId);
  }

  //@isTest static void SortingTest_1() {
  //  IntakeRepNavigatorWidgetQueue__c q = new IntakeRepNavigatorWidgetQueue__c(Name='Q', Query__c = '[{"column":"status__c","operator":"=","argument":"Lead"},{"column":"litigation__c","operator":"=","argument":"Mass Tort"},{"column":"case_type__c","operator":"<>","argument":"Porter Ranch Gas Leak"},{"column":"createddate","operator":">=","argument":"LAST_n_DAYS:30"}]', Sorting__c = '[{"column":"generating_attorney__c","order":"DESC NULLS LAST"},{"column":"createddate","order":"DESC NULLS LAST"}]', EarliestCallTime__c = Integer.valueOf(DateTime.now().addHours(-2).format('H', 'EST')), LatestCallTime__c = Integer.valueOf(DateTime.now().addHours(2).format('H', 'EST')));
  //  insert q;

  //  acct = new Account(FirstName = 'Test', LastName = 'Account2');
  //  insert acct;

  //  Intake__c withAttorneyCreatedFirst = new Intake__c(Status__c = 'Lead', Litigation__c = 'Mass Tort', Case_Type__c = 'Energy Drink', Caller_First_Name__c='Test Intake 1', Client__c = acct.id, Phone__c = '212-111-1112', generating_attorney__c = 'Abe Lincoln', CreatedDate = DateTime.now().addHours(-1));
  //  insert withAttorneyCreatedFirst;
  //  Intake__c nullAtyCreatedLast = new Intake__c(Status__c = 'Lead', Litigation__c = 'Mass Tort', Case_Type__c = 'Energy Drink', Caller_First_Name__c='Test Intake 1', Client__c = acct.id, Phone__c = '212-111-1113', generating_attorney__c = NULL, CreatedDate = DateTime.now().addHours(1));
  //  insert nullAtyCreatedLast;

  //  IntakeRepNavigatorWidgetController controller = new IntakeRepNavigatorWidgetController();
  //  controller.SelectedQueueId = q.Id;

  //  controller.GoToNext();
  //  System.assertEquals(withAttorneyCreatedFirst.Id, controller.NextId);
  //}
	
}