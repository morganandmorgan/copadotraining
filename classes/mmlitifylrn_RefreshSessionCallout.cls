/**
 *  @usage mmlitifylrn_RefreshSessionCallout.Response resp = (mmlitifylrn_RefreshSessionCallout.Response) new mmlitifylrn_RefreshSessionCallout()
 *                                   .setRefreshToken( firmRefreshToken )
 *                                   .debug()
 *                                   .execute()
 *                                   .getResponse();
 *
 *  @usage system.debug( resp.getAccessToken() );
 *  @usage system.debug( resp.getRefreshToken() );
 */
public class mmlitifylrn_RefreshSessionCallout
    extends mmlitifylrn_BasePOSTV1Callout
{
    private mmlitifylrn_RefreshSessionCallout.Response resp = new mmlitifylrn_RefreshSessionCallout.Response();
    private string refreshToken = null;

    public override map<string, string> getParamMap()
    {
        return new map<string, string>();
    }

    public override string getPathSuffix()
    {
        return '/refresh_session';
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        mmlitifylrn_RefreshSessionCallout.RequestBody requestBody = new mmlitifylrn_RefreshSessionCallout.RequestBody();

        requestBody.refresh_token = this.refreshToken;

        addPostRequestBody( requestBody );

        httpResponse httpResp = super.executeCallout();

        if ( this.isDebugOn )
        {
            system.debug( httpResp.getStatusCode() );
            system.debug( httpResp.getStatus() );
        }

        if ( httpResp.getStatusCode() == 200 )
        {
            resp = (mmlitifylrn_RefreshSessionCallout.Response) json.deserialize( httpResp.getBody(), mmlitifylrn_RefreshSessionCallout.Response.class );

            if ( this.isDebugOn )
            {
                resp.setDebugOn();
            }

            resp.setHTTPResponse( httpResp );

        }
        else
        {
            throw new mmlitifylrn_Exceptions.CalloutException( httpResp );
        }


        return this;
    }

    public class RequestBody
    {
        public string refresh_token;
    }

    public class Response
        implements CalloutResponse
    {
        private httpResponse httpResp = null;
        private map<string, string> httpResponseHeaderMap = new map<string, string>();
        private boolean isDebugOn = false;

        private void setHTTPResponse( httpResponse httpResp )
        {
            if ( this.isDebugOn == null )
            {
                this.isDebugOn = false;
            }

            if ( httpResp != null
                && ! httpResp.getHeaderKeys().isEmpty()
                )
            {
                this.httpResp = httpResp;

                if ( httpResponseHeaderMap == null )
                {
                    httpResponseHeaderMap  = new map<string, string>();
                }

                for ( String headerKey : httpResp.getHeaderKeys() )
                {
                    if ( this.isDebugOn )
                    {
                        system.debug( 'headerkey = ' + headerKey);
                        system.debug( httpResp.getHeader( headerKey ) );
                    }

                    httpResponseHeaderMap.put( headerKey, httpResp.getHeader( headerKey ) );
                }
            }
        }

        private void setDebugOn()
        {
            this.isDebugOn = true;
        }

        public string getAccessToken()
        {
            return this.httpResponseHeaderMap != null && this.httpResponseHeaderMap.containsKey( mmlitifylrn_Constants.LRN_API_ACCESS_TOKEN_KEY ) ? httpResponseHeaderMap.get( mmlitifylrn_Constants.LRN_API_ACCESS_TOKEN_KEY ) : null;
        }

        public string getRefreshToken()
        {
            return this.httpResponseHeaderMap != null && httpResponseHeaderMap.containsKey( mmlitifylrn_Constants.LRN_API_REFRESH_TOKEN_KEY ) ? httpResponseHeaderMap.get( mmlitifylrn_Constants.LRN_API_REFRESH_TOKEN_KEY ) : null;
        }

        public integer getTotalNumberOfRecordsFound()
        {
            return -1; //string.isblank(this.total_entries) ? 0 : integer.valueOf( this.total_entries );
        }
    }

    private void validateCallout()
    {
        if ( string.isBlank( this.refreshToken ) )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Litify Referral Network refresh token has not been specified.  Please use the setRefreshToken(string) method before calling the execute() method.');
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public mmlitifylrn_RefreshSessionCallout setRefreshToken( string refreshToken )
    {
        this.refreshToken = refreshToken;

        return this;
    }
}