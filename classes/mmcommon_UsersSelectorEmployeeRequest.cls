public class mmcommon_UsersSelectorEmployeeRequest implements mmcommon_IUsersSelectorEmployeeRequest {
    public String firstName = null;
	public String lastName = null;
    public String department = null;
    public Set<String> tags = new Set<String>();
    public String extension = null;
    public String phoneNumber = null;
    public String location = null;

    private mmcommon_UsersSelectorEmployeeRequest() {
    }

    public static mmcommon_IUsersSelectorEmployeeRequest newInstance() {
        return new mmcommon_UsersSelectorEmployeeRequest();
    }

    public mmcommon_IUsersSelectorEmployeeRequest setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }
    public mmcommon_IUsersSelectorEmployeeRequest setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
    public mmcommon_IUsersSelectorEmployeeRequest setDepartment(String department) {
        this.department = department;
        return this;
    }
    public mmcommon_IUsersSelectorEmployeeRequest setTags(Set<String> tags) {
        this.tags = tags;
        return this;
    }
    public mmcommon_IUsersSelectorEmployeeRequest setExtension(String extension) {
        this.extension = extension;
        return this;
    }
    public mmcommon_IUsersSelectorEmployeeRequest setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
    public mmcommon_IUsersSelectorEmployeeRequest setLocation(String location) {
        this.location = location;
        return this;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getDepartment() {
        return department;
    }
    public Set<String> getTags() {
        return tags;
    }
    public String getExtension() {
        return extension;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public String getLocation() {
        return location;
    }
}