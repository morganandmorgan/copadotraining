/**
 *  mmcommon_BusinessAccountsServiceImpl
 */
public with sharing class mmcommon_BusinessAccountsServiceImpl
    implements mmcommon_IBusinessAccountsService
{
    public list<Account> findTreatmentFacilitiesCloseToLocation( final Location originatingLocation, final String treatmentCenterType )
    {
        system.debug('originatingLocation == ' + originatingLocation);
        list<Account> outputList = new list<Account>();

        if ( originatingLocation != null
            && originatingLocation.getLatitude() != null
            && originatingLocation.getLongitude() != null
            )
        {
            outputList.addAll( mmcommon_AccountsSelector.newInstance().selectTreatmenCentersCloseToLocation( originatingLocation, 100, treatmentCenterType ) );
        }

        return outputList;
    }
}