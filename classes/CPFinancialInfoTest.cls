@isTest 
public class CPFinancialInfoTest
{
    class MockHttpRequests implements HttpCalloutMock
    {
        public HttpResponse respond(HttpRequest request)
        {
            String endpoint = request.getEndpoint();

            if (endpoint == 'callout:CPDataProvider/oauth/token')
            {
                System.assertEquals('POST', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'text/plain');
                response.setBody('valid-token');
                response.setStatusCode(200);

                return response;
            }
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/fininfo?office=ORL&matter=9828224')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/x-www-form-urlencoded');

                StaticResource resource = [select Id, Body from StaticResource where Name = 'CPFinancialInfo' limit 1];
                response.setBody(resource.Body.toString());
                response.setStatusCode(200);

                return response;
            }
            else if (endpoint == 'https://cp-dataprovider.azurewebsites.net/api/fininfo?office=BAD&matter=BAD')
            {
                System.assertEquals('Bearer valid-token', request.getHeader('Authorization'));
                System.assertEquals('GET', request.getMethod());

                HttpResponse response = new HttpResponse();
                response.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                response.setBody('{"caseExpenseData":null,"caseTrustData":null,"caseTrustSummary":null}');
                response.setStatusCode(200);

                return response;
            }
            else
            {
                System.assert(false, 'Unexpected Endpoint \'' + endpoint + '\'');
                return null;
            }
        }
    }



    @isTest
    static void testGetToken()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());
        System.assertEquals('valid-token', CPFinancialInfo.GetAccessToken());
    }

    @isTest
    static void testValidMatterId()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9828224', litify_pm__Client__c = acct.Id, CP_Office__c = 'ORL');
        insert matter;
        
        Test.startTest();
        CPFinancialInfo.CPCaseFinInfo finInfo = CPFinancialInfo.GetMatterFinancialInfo(matter.Id);

        System.assertNotEquals(null, finInfo);
        System.assertNotEquals(null, finInfo.CaseExpenseData);
        System.assertNotEquals(null, finInfo.CaseExpenseData.CaseExpenses);
        System.assertEquals('1/16/2020 12:00:00 AM', finInfo.CaseExpenseData.CaseExpenses[2].EffDate);
        System.assertEquals('PRN', finInfo.CaseExpenseData.CaseExpenses[2].CostCode);
        System.assertEquals('[Black & White Printing]', finInfo.CaseExpenseData.CaseExpenses[2].Payee);
        System.assertEquals('Black & White Printing ', finInfo.CaseExpenseData.CaseExpenses[2].Comment);
        System.assertEquals(1.25, finInfo.CaseExpenseData.CaseExpenses[2].Amount);
        System.assertEquals(2, finInfo.CaseExpenseData.CaseExpenses[2].Index);
        System.assertEquals(9.5, finInfo.CaseExpenseData.TotalCosts);
        System.assertEquals(-9.5, finInfo.CaseExpenseData.TotalDeposits);
        System.assertEquals(0, finInfo.CaseExpenseData.TotalPayables);
        System.assertEquals(0, finInfo.CaseExpenseData.Balance);

        System.assertNotEquals(null, finInfo.CaseTrustData);
        System.assertNotEquals(null, finInfo.CaseTrustData.CaseTrusts);
        System.assertEquals('685527', finInfo.CaseTrustData.CaseTrusts[4].CheckNo);
        System.assertEquals('adj 2.29.2020 4.csv', finInfo.CaseTrustData.CaseTrusts[4].Invoice);
        System.assertEquals('Orl - Trust (Active)', finInfo.CaseTrustData.CaseTrusts[4].Bank);
        System.assertEquals(-50, finInfo.CaseTrustData.TotalTrust);

        System.assertNotEquals(null, finInfo.CaseTrustSummary);
        System.assertEquals(9.5, finInfo.CaseTrustSummary.ExpensesToDate);
        System.assertEquals(0, finInfo.CaseTrustSummary.OutstandingExpenses);
        System.assertEquals('', finInfo.CaseTrustSummary.FeesBilled);
        System.assertEquals('', finInfo.CaseTrustSummary.FeesReceived);
        System.assertEquals(0, finInfo.CaseTrustSummary.Balance);
        System.assertEquals(-50, finInfo.CaseTrustSummary.TrustBalance);

        Test.stopTest();
    }

    @isTest 
    static void testInvalidMatterId()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Test.startTest();
        System.assert(CPFinancialInfo.GetMatterFinancialInfo('BAD') == null);
        Test.stopTest();
    }

    @isTest
    static void testBadParameters()
    {
        Test.setMock(HttpCalloutMock.class, new MockHttpRequests());

        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = 'BAD', litify_pm__Client__c = acct.Id, CP_Office__c = 'BAD');
        insert matter;
        
        Test.startTest();
        CPFinancialInfo.CPCaseFinInfo finInfo = CPFinancialInfo.GetMatterFinancialInfo(matter.Id);

        System.assertEquals(null, finInfo.CaseExpenseData);
        System.assertEquals(null, finInfo.CaseTrustData);
        System.assertEquals(null, finInfo.CaseTrustSummary);
        Test.stopTest();
    }
}