/**
 *  mmctm_CTMAccountMappingSelector
 */
public class mmctm_CTMAccountMappingSelector
    extends mmlib_SObjectSelector
    implements mmctm_ICTMAccountMappingSelector
{
    public static mmctm_ICTMAccountMappingSelector newInstance()
    {
        return (mmctm_ICTMAccountMappingSelector) mm_Application.Selector.newInstance(CallTrackingMetricsAccountMapping__mdt.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return CallTrackingMetricsAccountMapping__mdt.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            CallTrackingMetricsAccountMapping__mdt.CTM_Auth_Named_Credential__c
            , CallTrackingMetricsAccountMapping__mdt.CTM_Account_Id__c
            , CallTrackingMetricsAccountMapping__mdt.Domain_Value__c
        };
    }

    public List<CallTrackingMetricsAccountMapping__mdt> selectById(Set<Id> idSet)
    {
        return (List<CallTrackingMetricsAccountMapping__mdt>) selectSObjectsById(idSet);
    }

    public List<CallTrackingMetricsAccountMapping__mdt> selectAll()
    {
        return Database.query( newQueryFactory().toSOQL() );
    }
}