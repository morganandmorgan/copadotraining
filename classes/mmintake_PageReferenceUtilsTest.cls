/**
 *  mmintake_PageReferenceUtilsTest
 */
@isTest
private class mmintake_PageReferenceUtilsTest
{
    @isTest
    private static void testGetIntakeFlow()
    {
        Intake__c testIntake = new Intake__c();

        testIntake.id = fflib_IDGenerator.generate( Intake__c.SObjectType );
        testIntake.Client__c = fflib_IDGenerator.generate( Account.SObjectType );
        testIntake.Case_Type__c = 'General Injury';
        testIntake.Litigation__c = 'Personal Injury';

        test.startTest();

        mmintake_PageReferenceUtils.getIntakeFlow( testIntake );

        test.stopTest();
    }

    @isTest
    private static void testGetConfirmCallerID()
    {
        test.startTest();

        mmintake_PageReferenceUtils.getConfirmCallerID( fflib_IDGenerator.generate( Intake__c.SObjectType ), datetime.now() );

        test.stopTest();
    }
}