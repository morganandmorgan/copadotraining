public class mmtemp_AccountsDeploymentHelper
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public mmtemp_AccountsDeploymentHelper()
    {
        // Nothing.
    }

    public void run(List<SObject> scope)
    {
        List<Account> accountList = (List<Account>) scope;

        for (Account acct : accountList)
        {
            acct.SocialSecurityNumber_Last4__c = ''; 
            if (String.isNotBlank(acct.Social_Security_Number__c) && 4 <= acct.Social_Security_Number__c.length()) 
            { 
                acct.SocialSecurityNumber_Last4__c =  
                    acct.Social_Security_Number__c.substring( 
                        acct.Social_Security_Number__c.length() - 4, 
                        acct.Social_Security_Number__c.length() 
                    ); 
            }
        }

        update accountList;
    }
}