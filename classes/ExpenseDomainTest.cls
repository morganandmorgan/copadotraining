/**
 * ExpenseDomainTest
 * @description Test for Expense Domain class.
 * @author Jeff Watson
 * @date 1/19/2019
 */
@isTest
public with sharing class ExpenseDomainTest {

    @testSetup
    static void setup() {
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        c2g__codaCompany__c company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__CustomerSettlementDiscount__c = testGLA.Id;
        update company;
    } //setup

    @isTest
    public static void ExpenseDomain_CodeCoverage() {

        litify_pm__Expense_Type__c softExpenseType = new litify_pm__Expense_Type__c(Name = 'Soft Cost Recovered', CostType__c = 'SoftCost', ExternalID__c = 'SOFTCOSTRECOVERED');
        insert softExpenseType;

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;


        litify_pm__Expense__c expense = TestUtil.createExpense(matter);
        expense.litify_pm__ExpenseType2__c = softExpenseType.Id;
        insert expense;

        expense.litify_pm__Amount__c = 5;
        update expense;

        expense.auto_create_PIN__c = true;
        update expense;

        delete expense;
    } //ExpenseDomain_CodeCoverage

} //class