public class mmgmap_GoogleGeoCodingGetCallout
    extends mmlib_BaseGETCallout
{
    // https://maps.googleapis.com/maps/api/geocode/json?
    // address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&
    // key=YOUR_API_KEY

    // ========== Construction ==================
    public static mmgmap_GoogleGeoCodingGetCallout newInstance()
    {
        return newInstance(null);
    }

    public static mmgmap_GoogleGeoCodingGetCallout newInstance(Map<String, String> paramMap)
    {
        mmgmap_GoogleGeoCodingGetCallout result = new mmgmap_GoogleGeoCodingGetCallout();
        if (paramMap != null) result.setParamMap(paramMap);
        return result;
    }

    private mmgmap_GoogleGeoCodingGetCallout()
    {
        // Hide from consumer.
    }

    // ========== Extension Methods ==================
    public override mmlib_BaseCallout execute()
    {
        String responseBody = null;

        mmlib_KeyValueParameterCalloutAuth auth = new mmlib_KeyValueParameterCalloutAuth().setParamKey('key').setParamValue(mmgmap_GoogleGeoCodingGetCallout.getAccessKey());

        system.debug('auth == '+ auth);

        super.setAuthorization( auth );

        if (!isTest()) {
            HttpResponse httpResp = super.executeCallout();
            responseBody = httpResp.getBody();
        }
        else {
            responseBody = mock_ResponseJson;
        }

        if (isDebugOn)
        {
            System.debug('responseBody:\n' + responseBody);
        }

        calloutResponse = mmgmap_GoogleGeoCodingResponse.parse(responseBody);

        return this;
    }

    public override String getHost()
    {
        return 'maps.googleapis.com';
    }

    private Map<String, String> paramMap = new Map<String, String>();

    public override Map<String, String> getParamMap()
    {
        return this.paramMap;
    }

    public void setParamMap(Map<String, String> paramMap)
    {
        this.paramMap = paramMap;
    }

    public override String getPath()
    {
        return '/maps/api/geocode/json';
    }

    private mmgmap_GoogleGeoCodingResponse calloutResponse = null;
    public override mmlib_BaseCallout.CalloutResponse getResponse()
    {
        return (mmlib_BaseCallout.CalloutResponse) calloutResponse;
    }

    // ========== Feature Custom Settings ==================
    private static Boolean settingsRetrieved = false;
    private static String accessKey_m = '';
    private static Boolean isEnabled_m = false;
    private static Boolean isEnabledInSandbox_m = false;

    private static String getAccessKey()
    {
        getSettings();
        return accessKey_m;
    }

    public static Boolean getIsEnabled()
    {
        getSettings();
        return isEnabled_m;
    }

    public static Boolean getIsEnabledInSandbox()
    {
        getSettings();
        return isEnabledInSandbox_m;
    }

    public static Boolean getIsEnabledInThisOrg()
    {
        return
            mmgmap_GoogleGeoCodingGetCallout.getIsEnabled() &&
            (!mmlib_Utils.org.isSandbox || mmgmap_GoogleGeoCodingGetCallout.getIsEnabledInSandbox());
    }

    private static void getSettings()
    {
        if ( ! settingsRetrieved)
        {

            Google_Maps_Integration__c settings = Google_Maps_Integration__c.getInstance();

            if (settings != null) {
                isEnabled_m = settings.Address_Geocoding_Enabled__c;
                isEnabledInSandbox_m = settings.Address_Geocoding_Enabled_In_Sandbox__c;
                accessKey_m = settings.Geocoding_Access_Key__c;
            }

            settingsRetrieved = true;
        }
    }

    // ========== Testing Context ==================
    private Boolean isTest()
    {
        return !String.isEmpty(mock_ResponseJson);
    }

    @TestVisible
    private String mock_ResponseJson = null;
}