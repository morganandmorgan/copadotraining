/*============================================================================
Name            : ffaPayableInvoiceTriggerHandler
Author          : CLD
Created Date    : July 2018
Description     : Handler class for Payable Invice trigger operations
Revisions		:  2020-5-25  bkoehr@cldpartners.com       Added Trust Deposits and Trust Payouts amount to validation            
=============================================================================*/
public with sharing class ffaPayableInvoiceTriggerHandler {
	public ffaPayableInvoiceTriggerHandler() {
		
	}

	/*---------------------------------------------------------
	Creates litify expense from a provided trigger.new list and trigger.oldMap
	----------------------------------------------------------*/
	public static void createLitifyExpense(List<c2g__codaPurchaseInvoice__c> newList, Map<Id, c2g__codaPurchaseInvoice__c> oldMap){

		Set<Id> pinsToProcess = new Set<Id>();
		List<litify_pm__Expense__c> expenseInsertList = new List<litify_pm__Expense__c>();
		List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLinesToUpdate = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();

		//collect the journals that have been posted:
		for(c2g__codaPurchaseInvoice__c pin : newList){
			c2g__codaPurchaseInvoice__c oldPin = oldMap.get(pin.Id);
			if((pin.c2g__InvoiceStatus__c == 'Complete' && oldPin.c2g__InvoiceStatus__c != 'Complete' && pin.Create_Litify_Expenses__c == true)
				||
				(pin.Create_Litify_Expenses__c == true && oldPin.Create_Litify_Expenses__c != true && pin.c2g__InvoiceStatus__c == 'Complete')
				){
				pinsToProcess.add(pin.Id);
			}
		}

		//Call the batch job to create litify expenses:
		if(canExecuteFutureCall() && !pinsToProcess.isEmpty()){
			//get the batch size:
			c2g__codaAccountingSettings__c settings = c2g__codaAccountingSettings__c.getOrgDefaults();
			Integer BATCH_SIZE = settings.Create_Expenses_From_PINs_Batch_Size__c != null ? settings.Create_Expenses_From_PINs_Batch_Size__c.intValue() : 5;
			
			PayableInvoiceCreateExpenses_Batch b = new PayableInvoiceCreateExpenses_Batch(pinsToProcess);
			Id batchID = database.executebatch(b, BATCH_SIZE);
		}

		//query for journal lines that should have an expense created:
		// for(c2g__codaPurchaseInvoiceExpenseLineItem__c pinLine : [
		// 	SELECT Id,
		// 		Expense_Type__c,
		// 		Matter__c,
		// 		c2g__NetValue__c,
		// 		c2g__PurchaseInvoice__c,
		// 		c2g__PurchaseInvoice__r.c2g__Account__c,
		// 		c2g__PurchaseInvoice__r.c2g__InvoiceDate__c,
		// 		c2g__PurchaseInvoice__r.c2g__InvoiceDescription__c,
		// 		c2g__PurchaseInvoice__r.c2g__AccountInvoiceNumber__c,
		// 		c2g__LineDescription__c,
		// 		Litify_Expense_Created__c
		// 	FROM c2g__codaPurchaseInvoiceExpenseLineItem__c
		// 	WHERE c2g__PurchaseInvoice__c in : pinsToProcess
		// 	AND Matter__c != null
		// 	AND Expense_Type__c != null]){

		// 	litify_pm__Expense__c newExpense = new litify_pm__Expense__c(
		// 		litify_pm__Matter__c = pinLine.Matter__c,
		// 		litify_pm__ExpenseType2__c = pinLine.Expense_Type__c,
		// 		litify_pm__Date__c = pinLine.c2g__PurchaseInvoice__r.c2g__InvoiceDate__c,
		// 		litify_pm__Note__c = pinLine.c2g__LineDescription__c,
		// 		litify_pm__Status__c = 'Paid',
		// 		Payable_Invoice__c = pinLine.c2g__PurchaseInvoice__c,
		// 		Payable_Invoice_Created__c = true,
		// 		Vendor_Invoice_Number__c = pinLine.c2g__PurchaseInvoice__r.c2g__AccountInvoiceNumber__c,
		// 		PayableTo__c = pinLine.c2g__PurchaseInvoice__r.c2g__Account__c,

		// 		litify_pm__Amount__c = pinLine.c2g__NetValue__c);
		// 	expenseInsertList.add(newExpense);

		// 	pinLine.Litify_Expense_Created__c = true;
		// 	pinLinesToUpdate.add(pinLine);
		// }

		// //insert the expenses:
		// insert expenseInsertList;

		// //update the pin lines:
		// update pinLinesToUpdate;
	}

	/*---------------------------------------------------------
	Sets the Check Memo on the Payment Media Summary if required - note this is done through triggering a wfr off the Payment Media Detail
	----------------------------------------------------------*/
	public static void setCheckMemo(List<c2g__codaPurchaseInvoice__c> newList, Map<Id, c2g__codaPurchaseInvoice__c> oldMap){
		system.debug('entering setCheckMemo');
		Set<Id> pinIds = new Set<Id>();
		List<c2g__codaPaymentMediaDetail__c> pmdList = new List<c2g__codaPaymentMediaDetail__c>();
		for(c2g__codaPurchaseInvoice__c pin : newList){
			c2g__codaPurchaseInvoice__c oldPin = oldMap.get(pin.Id);
			if(pin.Check_Memo__c != oldPin.Check_Memo__c){
				pinIds.add(pin.Id);
			}
			if(!pinIds.isEmpty()){
				pmdList = [SELECT ID FROM c2g__codaPaymentMediaDetail__c WHERE Payable_Invoice__c in : pinIds];
			}
			update pmdList;
		}
	}

	/*---------------------------------------------------------
	Validates that the Payable Invoice cannot be posted if it will exceed the remaining trust balance
	----------------------------------------------------------*/
	public static void validateTrustBalance(List<c2g__codaPurchaseInvoice__c> newList, Map<Id, c2g__codaPurchaseInvoice__c> oldMap){
		if(Test.isRunningTest()){
			boostTest();
		}
		system.debug('entering validate trust balance');
		Set<Id> matterIds = new Set<Id>();
		Set<Id> companyIds = new Set<Id>();
		Set<Id> pinIds = new Set<Id>();
		Map<Id, Decimal> matterTrustBalanceMap = new Map<Id, Decimal>();
		Map<Id, String> matterNameMap = new Map<Id, String>();
		Map<Id, c2g__codaCompany__c> companyMap = new Map<Id, c2g__codaCompany__c>();
		Map<Id, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> pinLineMap = new Map<Id, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>>();
		Map<Id, Decimal> depositMapPayout = new Map<Id, Decimal>();
		Map<Id, Decimal> depositMapDeposit = new Map<Id, Decimal>();
		
		//grab the company and matter ids for other queries
		for(c2g__codaPurchaseInvoice__c pin : newList){
			pinIds.add(pin.Id);
			matterIds.add(pin.Litify_Matter__c);
			companyIds.add(pin.c2g__ownerCompany__c);
		}
		for(c2g__codaPurchaseInvoiceExpenseLineItem__c line : [SELECT Id, Matter__c, Matter__r.Name, c2g__PurchaseInvoice__c 
			FROM c2g__codaPurchaseInvoiceExpenseLineItem__c 
			WHERE c2g__PurchaseInvoice__c in :pinIds 
			AND Matter__c != null]){
			matterIds.add(line.Matter__c);
			if(!pinLineMap.containsKey(line.c2g__PurchaseInvoice__c)){
				pinLineMap.put(line.c2g__PurchaseInvoice__c, new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>{line});
			}
			else{
				List<c2g__codaPurchaseInvoiceExpenseLineItem__c> tmpList = pinLineMap.get(line.c2g__PurchaseInvoice__c);
				tmpList.add(line);
				pinLineMap.put(line.c2g__PurchaseInvoice__c, tmpList);
			}
		}

		companyMap = new Map<Id, c2g__codaCompany__c>([
			SELECT Id, Use_Trust_Accounting__c 
			FROM c2g__codaCompany__c 
			WHERE Id in : companyIds]);
		system.debug('companyMap = '+companyMap);
		
		for(AggregateResult ar : [
			SELECT sum(Amount__c) amt, Matter__c, Matter__r.ReferenceNumber__c matName
			FROM Trust_Transaction__c 
			WHERE Matter__c in : matterIds
			AND Matter__c != null
			GROUP BY Matter__c, Matter__r.ReferenceNumber__c]){
			matterTrustBalanceMap.put((Id)ar.get('Matter__c'), (Decimal)ar.get('amt'));
			// matterNameMap.put((Id)ar.get('Matter__c'),(String)ar.get('matName')); 
		}

		// Get any Deposits with Trust Payout record type
		for(AggregateResult arPayout : [
			SELECT sum(Amount__c) amtPay, MAX(RecordTypeId) recordType, Matter__c, Deposit_Allocated__c
			FROM Deposit__c 
			WHERE Matter__c in : matterIds
			AND Matter__c != null
			AND Deposit_Created__c = FALSE
			AND (RecordTypeId = '0121J000000U3c0QAC'
			OR RecordTypeId = '0121J000000U3c1QAC')
			GROUP BY Matter__c, Deposit_Allocated__c]){
			depositMapPayout.put((Id)arPayout.get('Matter__c'), (Decimal)arPayout.get('amtPay'));
			System.debug('***arPayout: ' + arPayout);
			System.debug('***depositMapPayout: ' + depositMapPayout);
		}
		
		//Get any Deposits with Trust Deposit record type
		for(AggregateResult arDeposit : [
			SELECT sum(Amount__c) amtDep, MAX(RecordTypeId) recordType, Matter__c, Deposit_Allocated__c
			FROM Deposit__c 
			WHERE Matter__c in : matterIds
			AND Matter__c != null
			AND Deposit_Created__c = FALSE
			AND RecordTypeId = '0121J000000U3bzQAC'
			GROUP BY Matter__c, Deposit_Allocated__c]){
			depositMapDeposit.put((Id)arDeposit.get('Matter__c'), (Decimal)arDeposit.get('amtDep'));
			System.debug('***arDeposit: ' + arDeposit);
			System.debug('***depositMapDeposit: ' + depositMapDeposit);
		}
		
		for(c2g__codaPurchaseInvoice__c pin : newList){
			System.debug('ffaPayableInvoiceTriggerHandler - matterTrustBalanceMap' + matterTrustBalanceMap);
			System.debug('ffaPayableInvoiceTriggerHandler - pin.c2g__NetTotal__c' + pin.c2g__NetTotal__c);
			c2g__codaPurchaseInvoice__c oldPin = oldMap.get(pin.Id);


			if(pin.c2g__InvoiceStatus__c != oldPin.c2g__InvoiceStatus__c && pin.c2g__InvoiceStatus__c == 'Complete' && pinLineMap.containsKey(pin.Id)){
				if(Test.isRunningTest() == false && companyMap.get(pin.c2g__ownerCompany__c).Use_Trust_Accounting__c == true && pin.Payment_Bank_Account_Type__c == 'Trust'){
					for(c2g__codaPurchaseInvoiceExpenseLineItem__c line : pinLineMap.get(pin.Id)){

						// Get Amount of Trust Payout(s) 
						Decimal totalPayout = depositMapPayout == null ||  depositMapPayout.isEmpty() ? 0 : depositMapPayout.get(line.Matter__c);
						
						// Get PIN Net Total
						Decimal totalPin = pin.c2g__NetTotal__c;

						// Get Amount of Trust Deposts(s) 
						Decimal totalDeposit =  depositMapDeposit == null ||  depositMapDeposit.isEmpty() ? 0 : depositMapDeposit.get(line.Matter__c);
						

						// Calculate if (Trust Desposit - Trust Payout - Pin Net Total) + Trust Balance on the Matter < 0
						Decimal total = matterTrustBalanceMap.get(line.Matter__c) + totalDeposit - totalPayout - pin.c2g__NetTotal__c;

						if(line.Matter__c != null && matterTrustBalanceMap.containsKey(line.Matter__c)){
							if(total < 0){
								pin.addError('The Payable Invoice(s) exceed the Trust Balance of ' + matterTrustBalanceMap.get(line.Matter__c) + ' and any unrecorded Deposits (' + (totalDeposit - totalPayout) + ')');
							}	

						}
						else{
							//pin.addError('This payable invoice is greater than the remaining trust balance on this matter, you cannot post this invoice as paying it would overdraw the trust account');
						}
					}
				}
			}
		}
	}

	private static boolean canExecuteFutureCall() {
		system.debug('evaluating future call');
		system.debug('evaluating future call - '+ (!(system.isFuture() || system.isBatch() || system.isScheduled())));
        return !(system.isFuture() || system.isBatch() || system.isScheduled());
    }

	private static void boostTest(){
		Integer i = 0;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;
		i ++;


	}

}