@isTest
public without sharing class MatterFileSync_Job_Test {

    private static final Integer EXPECTED_MATTER_RECORDS = 3;

    @isTest
    private static void runBoundSync_first() {
        Integer resumePosition = 0;
        List<litify_pm__Matter__c> matter = insertTestMatter(resumePosition);
        List<DateTime> bounds = generateTestBounds(matter[resumePosition]);

        Test.startTest();
        Database.executeBatch(new MatterFileSync_Job(bounds));
        Test.stopTest();

        System.assertNotEquals(null, MatterFileSync_Job.testMatterIds);
        System.assertEquals(EXPECTED_MATTER_RECORDS, MatterFileSync_Job.testMatterIds.size());
        System.assert(MatterFileSync_Job.testMatterIds.contains(matter[resumePosition].Id));
    }

    @isTest
    private static void runBoundSync_resume() {
        Integer resumePosition = 2;
        List<litify_pm__Matter__c> matter = insertTestMatter(resumePosition);
        System.assert(matter[resumePosition - 1].CreatedDate < matter[resumePosition].CreatedDate);
        List<DateTime> bounds = generateTestBounds(matter[resumePosition]);

        Test.startTest();
        Database.executeBatch(new MatterFileSync_Job(bounds));
        Test.stopTest();

        System.assertNotEquals(null, MatterFileSync_Job.testMatterIds);
        System.assertEquals(EXPECTED_MATTER_RECORDS, MatterFileSync_Job.testMatterIds.size());
        System.assert(MatterFileSync_Job.testMatterIds.contains(matter[resumePosition].Id));
    }

    @isTest
    private static void runIdSync() {
        List<litify_pm__Matter__c> matter = insertTestMatter(0);

        Set<Id> matterIds = new Set<Id>();
        for (litify_pm__Matter__c m : matter) {
            matterIds.add(m.Id);
        }

        Test.startTest();
        Database.executeBatch(new MatterFileSync_Job(matterIds));
        Test.stopTest();

        System.assertNotEquals(null, MatterFileSync_Job.testMatterIds);
        System.assertEquals(matterIds.size(), MatterFileSync_Job.testMatterIds.size());
        for (Id matterId : matterIds) {
            System.assert(MatterFileSync_Job.testMatterIds.contains(matterId));
        }
    }

    private static List<litify_pm__Matter__c> insertTestMatter(Integer resumePosition) {
        Account acct = TestUtil.createAccount('Testy Testy');
        insert acct;

        Set<Id> matterIds = new Set<Id>();
        litify_pm__Matter__c matter;
        for (Integer i = 0; i < resumePosition + EXPECTED_MATTER_RECORDS; i++) {
            matter = TestUtil.createMatter(acct);
            insert matter;
            matterIds.add(matter.Id);

            // Wait 1 seond just to give different DatTimes for our records
            Long startTime = DateTime.now().getTime();
            Long millisecondsToWait = 1000;
            while (DateTime.now().getTime() - startTime < millisecondsToWait) {}
        }

        List<litify_pm__Matter__c> allMatter = [
            SELECT Id, CreatedDate
            FROM litify_pm__Matter__c
            WHERE Id = :matterIds
            ORDER BY CreatedDate ASC
        ];

        System.assert(allMatter[resumePosition].CreatedDate < allMatter[resumePosition + 1].CreatedDate);
        
        return allMatter;
    }

    private static List<DateTime> generateTestBounds(litify_pm__Matter__c matter) {
        return new List<DateTime> { 
            matter.CreatedDate,
            matter.CreatedDate.addDays(1) 
        };
    }
}