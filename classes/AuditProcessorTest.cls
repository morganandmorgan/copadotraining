/**
 * AuditProcessorTest
 * @description Units tests for AuditProcesser and other Audit classes
 * @author Matt Terrill
 * @date 10/10/2019
 *
    Coverage for:
        AuditFlagDomain
        AuditFlagScheduled
        AuditFlagSelector
        AuditHistoryBatch
        AuditProcessor
        AuditReportCardDomain
        AuditReportCardSelector
        AuditReportCardTrigger
        AuditRuleDomain
        AuditRuleSelector
        AuditRuleTrigger
        AuditScheduledFlagDomain
        AuditScheduledFlagSelector
        ErrorLogDomain
        ErrorLogSelector
*/
@isTest
public class AuditProcessorTest {

    @testSetup
    public static void setup() {

        Account a = new Account();
        //required
        a.FirstName = 'firstTest';
        a.LastName = 'lastTest';
        a.provider_Insurance_Segmentation__c = 'AOB Provider;PIP Insurance';
        insert a;

        //create a matter
        litify_pm__Matter__c m = new litify_pm__Matter__c();
        m.litify_pm__Client__c = a.id;
        m.litify_pm__Status__c = 'New Case';
        m.litify_pm__incident_date__c = Date.today().addDays(-2);
        insert m;

        //create a matter rule
        AuditEvalEngine.Rule rule = new AuditEvalEngine.Rule();
        rule.auditRule = new Audit_Rule__c();
        rule.auditRule.object__c = 'litify_pm__Matter__c';
        rule.auditRule.rule_type__c = 'Real-Time';
        rule.auditRule.isActive__c = true;
        AuditEvalEngine.RuleExpression e = new AuditEvalEngine.RuleExpression();
        e.argument1 = '{!litify_pm__Status__c}';
        e.operator = '=';
        e.argument2 = 'New Case';
        rule.expressions = new List<AuditEvalEngine.RuleExpression>();
        rule.expressions.add(e);

        //use the domain to save it
        AuditRuleDomain ruleDomain = new AuditRuleDomain();
        ruleDomain.upsertRule(rule);

        //make a Scheduled version of it
        rule = new AuditEvalEngine.Rule();
        rule.auditRule = new Audit_Rule__c();
        rule.auditRule.object__c = 'litify_pm__Matter__c';
        rule.auditRule.rule_type__c = 'Scheduled';
        rule.auditRule.custom_scheduled_date_field__c = 'litify_pm__incident_date__c';
        rule.auditRule.days__c = 0;
        rule.auditRule.isActive__c = true;
        e = new AuditEvalEngine.RuleExpression();
        e.argument1 = '{!litify_pm__Status__c}';
        e.operator = '=';
        e.argument2 = 'New Case';
        rule.expressions = new List<AuditEvalEngine.RuleExpression>();
        rule.expressions.add(e);

        //save it
        ruleDomain.upsertRule(rule);

    } //setup


    @isTest
    public static void triggerHandlerTest() {

        litify_pm__Matter__c obj = [SELECT id, litify_pm__Status__c FROM litify_pm__Matter__c LIMIT 1];

        litify_pm__Matter__c old = obj.clone(true,true,true,true); //true dat
        old.litify_pm__Status__c = 'Investigation';

        List<SObject> objList = new List<SObject>();
        objList.add(obj);

        Map<Id,SObject> oldMap = new Map<Id,SObject>();
        oldMap.put(old.id,old);

        //reset this so it doesn't get suppressed
        AuditProcessor.suppressById.remove(obj.id);
        
        AuditProcessor.triggerHandler(objList, oldMap);

        //now let's get the scheduled flag we just created to void/reschedule
        old = obj.clone(true,true,true,true); //true dat
        old.litify_pm__incident_date__c = Date.today();

        oldMap = new Map<Id,SObject>();
        oldMap.put(old.id,old);

        //reset this so it doesn't get suppressed
        AuditProcessor.suppressById.remove(obj.id);
        AuditProcessor.triggerHandler(objList, oldMap);

    } //triggerHandlerTest


    @isTest
    public static void auditFlagScheduledTest() {

        litify_pm__Matter__c m = [SELECT id, litify_pm__Status__c FROM litify_pm__Matter__c LIMIT 1];

        Audit_Rule__c ar = [SELECT id FROM Audit_Rule__c WHERE rule_type__c = 'Scheduled'];

        Audit_Scheduled_Flag__c schedFlag = new Audit_Scheduled_Flag__c();
        schedFlag.audit_rule__c = ar.id;
        schedFlag.litify_pm_Matter__c = m.id;
        schedFlag.status__c = 'Pending';
        schedFlag.scheduled_date__c = DateTime.now();
        insert schedFlag;

        //create an orphaned one to get voided
        schedFlag = new Audit_Scheduled_Flag__c();
        schedFlag.audit_rule__c = ar.id;
        schedFlag.status__c = 'Pending';
        schedFlag.scheduled_date__c = DateTime.now();
        insert schedFlag;

        AuditFlagScheduled afs = new AuditFlagScheduled();

        afs.execute(null);

        //we need a flag on a rule to get this extra coverage on AuditRuleDomain
        AuditRuleDomain ruleDomain = new AuditRuleDomain();
        ruleDomain.ruleHasFlags( new List<Audit_Rule__c>{ar});

        //trigger and handler test
        ar.name = 'new name';
        update ar;

    } //auditFlagScheduledTest;


    @isTest
    public static void extraCoverage() {

        AuditReportCardSelector arcs = new AuditReportCardSelector();
        arcs.addSObjectFields( new List<Schema.SObjectField> {Audit_Report_Card__c.createdDate});
        arcs.selectCurrentForUser(UserInfo.getUserId());
        arcs.selectForUsers(new Set<Id>{UserInfo.getUserId()});

        //create an ophaned flag
        Audit_Flag__c flag = new Audit_Flag__c();
        flag.user__c = UserInfo.getUserId();
        insert flag;

        //create a report card that will parent it
        Audit_Report_Card__c reportCard = new Audit_Report_Card__c();
        reportCard.user__c = UserInfo.getUserId();
        reportCard.start_date__c = Date.today().addDays(-30);
        reportCard.end_Date__c = Date.today().addDays(30);
        insert reportCard;

        //create an overlapping report card
        reportCard = new Audit_Report_Card__c();
        reportCard.user__c = UserInfo.getUserId();
        reportCard.start_date__c = Date.today().addDays(-40);
        reportCard.end_Date__c = Date.today().addDays(-20);

        //trap the error this should create
        try {
            insert reportCard;
            System.assertEquals('Error','Not Generated!');
        } catch (Exception e) {
            System.assertEquals('Error','Error');
        }
        AuditReportCardDomain reportCardDomain = new AuditReportCardDomain();

        AuditRuleSelector ars = new AuditRuleSelector();
        ars.addSObjectFields( new List<Schema.SObjectField> {Audit_Rule__c.createdDate});

        AuditScheduledFlagSelector asfs = new AuditScheduledFlagSelector();
        asfs.selectNeedsReEvaluation();
        asfs.addSObjectFields( new List<Schema.SObjectField> {Audit_Scheduled_Flag__c.createdDate});

        try {
            Integer x = 1/0;
        } catch (Exception e) {
            ErrorLogDomain errorLog = new ErrorLogDomain();
            errorlog.logError(e, 'AuditProcessorTest');
        }

        AuditFlagSelector afs = new AuditFlagSelector();
        afs.addSObjectFields( new List<Schema.SObjectField> {Audit_Flag__c.createdDate});
        afs.selectById( new Set<Id>() );
        afs.selectByRuleId( new Set<Id>(), null );

        ErrorLogSelector els = new ErrorLogSelector();
        els.addSObjectFields( new List<Schema.SObjectField> {Error_Log__c.createdDate});
        els.selectById( new Set<Id>() );
        els.selectRecent(DateTime.now().addHours(-1));

    } //extraCoverage


    @isTest
    public static void processHistoryTest() {

        litify_pm__Matter__c obj = [SELECT id, litify_pm__Status__c FROM litify_pm__Matter__c LIMIT 1];

        litify_pm__Matter__history history = new litify_pm__Matter__history();
        history.parentId = obj.id;
        history.field = 'litify_pm__Status__c';

        AuditProcessor.testHistory = new List<litify_pm__Matter__history>{history};

        AuditProcessor.processHistory('', DateTime.now(), DateTime.now(), 'litify_pm__Matter__c', new Set<Id>{obj.id});

    } //processHistoryTest


    @isTest
    public static void auditHistoryBatchTest() {

        AuditHistoryBatch ahb = new AuditHistoryBatch('', 0, '', 0);

        ahb = new AuditHistoryBatch('', DateTime.now(), DateTime.now(), 'litify_pm__Matter__c' , 5);

        ahb.execute(null);

        ahb.execute(null, new List<SObject>());

    } //auditHistoryBatchTest


} //class