public with sharing class CaseSpecificFieldsetRendererExtension {

    private Id intakeId;
    public Intake__c intake { get; private set; }

    private Id questionnaireId;
    public MM_Questionnaire__c questionnaire;

    public String customFieldSetName { get; private set; }
    public List<Schema.FieldSetMember> customFields { get; private set; }

    public CaseSpecificFieldsetRendererExtension(ApexPages.StandardController controller) {
        //determine what standard controller got us here, by the id type
        Id paramId = (Id)controller.getId();
        if ( paramId.getSObjectType().getDescribe().getName() == 'Intake__c') {
          intakeId = controller.getId();
        }
        if ( paramId.getSObjectType().getDescribe().getName() == 'MM_Questionnaire__c') {
          questionnaireId = controller.getId();
          questionnaire = [SELECT id, intake__c FROM MM_Questionnaire__c WHERE id = :questionnaireId];
          intakeId = questionnaire.intake__c;
        }
        if ( paramId.getSObjectType().getDescribe().getName() == 'Task') {
          Id taskId = controller.getId();
          Task theTask = [SELECT id, whatId FROM Task WHERE id = :taskId];

          List<litify_pm__Matter__c> matter = [SELECT id, intake__c FROM litify_pm__Matter__c WHERE id = :theTask.whatId];
          //it is possible the task was not on a matter
          if (matter.size() != 0) {
            intakeId = matter[0].intake__c;
          }
        }

        initializeIntake(intakeId);

        if (intake != null) {
          //determine what page we are on
          String strUrl = ApexPages.currentPage().getUrl();

          if(strUrl.containsIgnoreCase('questionnaireGeneralIntake')) {
            initializeFieldSet('General_Intake_Fields_for_Display');
          } else {
            initializeFieldSet('');
          }
        }
    } //constructor

    private void initializeIntake(Id id) {
        Map<String, Schema.SObjectField> fields = SObjectType.Intake__c.fields.getMap();

        List<String> queryColumns = new List<String>();
        for (String fieldName : fields.keySet()) {
          queryColumns.add(fields.get(fieldName).getDescribe().getName());
        }

        String query = 'SELECT ' + String.join(queryColumns, ',') + ' FROM Intake__c WHERE Id = :id LIMIT 1';

        List<Intake__c> intakes = (List<Intake__c>)Database.query(query);
        //intake = (Intake__c) Database.query(query);

        if (intakes.size() != 0) {
          intake = intakes[0];
        }
    } //intializeIntake

    private void initializeFieldSet(String reqFieldSetName) {

        String fieldsetName;

        if (reqFieldSetName == '') {
          String withSpaces = 'TD_' + intake.Litigation__c + '_' + intake.Case_Type__c;
          fieldsetName = withSpaces.replaceAll('[^a-zA-Z0-9]+', '_');
        } else {
          fieldsetName = reqFieldSetName;
        }

        //Add 40 character limit due to field set API name length limits
        fieldSetName = fieldSetName.left(40);

        // HACK: these seem to always be lowercase but is that a guarantee?
        fieldsetName = fieldsetName.toLowerCase();

        Map<String, Schema.FieldSet> fieldSets = SObjectType.Intake__c.fieldSets.getMap();

        System.debug('Fieldsets found: ' + fieldSets.size());
        for(String name : fieldSets.keySet()) {
          System.debug('Field set: ' + name);
        }

        Schema.FieldSet fieldSet = fieldSets.get(fieldsetName);

        customFieldSetName = fieldsetName;

        if (fieldSet == null) {
          System.debug('No fieldset found with name ' + fieldsetName);
          return;
        }

        System.debug('Found fieldset with name ' + fieldsetName);
        customFields = fieldSet.getFields();
    }
}