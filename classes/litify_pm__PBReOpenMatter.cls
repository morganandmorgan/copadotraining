/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBReOpenMatter {
    global PBReOpenMatter() {

    }
    @InvocableMethod(label='Re-Open' description='Re-Opens the given Matter')
    global static void reOpenMatters(List<litify_pm.PBReOpenMatter.ProcessBuilderReOpenMatterWrapper> reOpenMatterItems) {

    }
global class ProcessBuilderReOpenMatterWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderReOpenMatterWrapper() {

    }
}
}
