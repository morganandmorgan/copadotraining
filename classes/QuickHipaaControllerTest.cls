@isTest
public class QuickHipaaControllerTest {

    @isTest
    public static void mainTest() {

        Account account = TestUtil.createAccount('Unit Test Account');
        account.shippingStreet = '101 Main Street';
        account.shippingCity = 'Orlando';
        account.shippingState = 'FL';
        account.shippingPostalCode = '32800';

        insert account;
        
        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;

        QuickHipaaController.getMatterData(matter.id);

        QuickHipaaController.MatterData matterData = new QuickHipaaController.MatterData();
        matterData.name = account.name;
        matterData.street = account.shippingStreet;
        matterData.city = account.shippingCity;
        matterData.state = account.shippingState;
        matterData.postalCode = account.shippingPostalCode;

        litify_docs__Template__c template = new litify_docs__Template__c();
        template.name = 'FL Quick Hipaa';
        template.litify_docs__Merged_File_Name__c = 'unitTest.docx';
        template.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        insert template;

        QuickHipaaController.sendMail( Json.serialize(matterData));

    } //mainTest

} //class