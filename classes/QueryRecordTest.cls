@isTest
public class QueryRecordTest
{
    @isTest
    public static void TestQuery()
    {
        Test.startTest();

        Account acct = new Account(Name = 'Test User');
        insert acct;

        litify_pm__Matter__c matter = new litify_pm__Matter__c(ReferenceNumber__c = '9828224', litify_pm__Client__c = acct.Id);
        insert matter;

        List<litify_pm__Matter__c> results = QueryRecord.Query('982');

        System.assertEquals(results.size(), 1);
        System.assertEquals(results[0].Id, matter.Id);

        Test.stopTest();
    }

    @isTest
    public static void TestFakeQuery()
    {
        Test.startTest();

        litify_pm__Matter__c[] matters = QueryRecord.QueryFake('123');

        System.assertEquals(matters[0].Id, 'a1n1J000002wo7WQAQ');

        Test.stopTest();
    }
}