/**
 *  mmlib_IQueueableAction
 */
public interface mmlib_IQueueableAction
{
    mmlib_IQueueableAction setActionToRunInQueue( boolean isActionToRunInQueue );

    mmlib_IQueueableAction setNextQueueableActionInChain( mmlib_IQueueableAction nextAction );

    mmlib_IQueueableAction getNextQueuableAction();
}