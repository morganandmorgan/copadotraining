@isTest
private class UserProfileUtilTest {
  static {
    insert new UserRole(Name = 'Administrator', DeveloperName = 'Administrator');
  }
  @isTest static void getProfileIDByName() {
    Test.startTest();
    Id i = UserProfileUtil.getProfileIDByName('Standard User');
    Test.stopTest();

    System.assertNotEquals(null, i);
  }
  
  @isTest static void getRoleIDByDevName() {
    Test.startTest();
    Id i = UserProfileUtil.getRoleIDByDevName('Administrator');
    Test.stopTest();

    System.assertNotEquals(null, i);
  }


  @isTest static void validProfile() {
    Id i = UserProfileUtil.getProfileIDByName('Standard User');

    Test.startTest();
    Boolean b = UserProfileUtil.validProfile(i);
    Test.stopTest();

    System.assert(b);
  }

  @isTest static void validRole() {
    Id i = UserProfileUtil.getRoleIDByDevName('Administrator');

    Test.startTest();
    Boolean b = UserProfileUtil.validRole(i);
    Test.stopTest();

    System.assert(b);
  }
	
}