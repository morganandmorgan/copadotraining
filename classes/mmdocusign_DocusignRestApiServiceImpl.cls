public with sharing class mmdocusign_DocusignRestApiServiceImpl
	implements mmdocusign_IDocusignRestApiService
{
	public static mmdocusign_CreateEnvelopeResponseData createEnvelope(Intake__c record)
	{
    		mmdocusign_CreateEnvelopeCallout callout = new mmdocusign_CreateEnvelopeCallout(record);
    		callout.execute();

    		return new mmdocusign_CreateEnvelopeResponseData((mmdocusign_CreateEnvelopeResponse)callout.getResponse());
	}

	public static Task createTasksForEnvelope(Intake__c intake, String envelopeId, String clientUserId)
	{
		if (intake == null)
		{
			throw new mmdocusign_IntegrationExceptions.ParameterException('An Intake is required to create a ceremony-signing task.');
		}

		if (String.isBlank(envelopeId))
		{
			throw new mmdocusign_IntegrationExceptions.ParameterException('An envelope ID is required to create a ceremony-signing task.');
		}

		String landingPageUrlTmpl = mmdocusign_ConfigurationSettings.getSigningCeremonyLandingPageTemplate();

		if (isTest())
		{
			landingPageUrlTmpl = mock_landingPageUrlTempl;
		}

		String paramStringTempl = '?env={0}&email={1}&name={2}&cuid={3}';

		String landingPageUrl =
				landingPageUrlTmpl +
				String.format(
					paramStringTempl,
					new List<String>
					{
						envelopeId,
						EncodingUtil.urlEncode(intake.Client_Email_txt__c, 'UTF-8'),
						EncodingUtil.urlEncode(intake.Client_First_Last_Name__c, 'UTF-8'),
						EncodingUtil.urlEncode(clientUserId, 'UTF-8')
					}
				);

		test_landingPageUrl = landingPageUrl;

		if (!isTest())
		{
			landingPageUrl = mmlib_TinyUrl.getTinyUrl(landingPageUrl);
		}
		else
		{
			landingPageUrl = mock_shortenedLandingPageUrl;
		}

		System.debug('<ojs> shortened landingPageUrl:\n' + landingPageUrl);

		Task newTask = new Task();
		newTask.ActivityDate = Date.today();
		newTask.OwnerId = UserInfo.getUserId();
		newTask.PC_Signup_Url__c = landingPageUrl;
		newTask.Priority = 'Normal';
		newTask.Status = 'Completed';
		newTask.Subject = 'Created Docusign PC signup envelope';
		newTask.Type = 'Other';
		newTask.WhatId = Intake.Id;

		if (!isTest())
		{
			insert newTask;
		}

		return newTask;
	}

	public static String getRecipientLinkToEnvelope(String envelopeId, String clientEmail, String clientName, String clientUserId)
	{
    		mmdocusign_GetEnvelopeLinkCallout callout =
    			new mmdocusign_GetEnvelopeLinkCallout(
    				envelopeId,
    				clientEmail,
    				clientName,
					clientUserId);
    		callout.execute();
    		mmdocusign_EnvelopeLinkCalloutResponse resp = (mmdocusign_EnvelopeLinkCalloutResponse) callout.getResponse();
    		return resp.getEnvelopeLink();
	}

	// =========== Testing Construct ===========================================================

	@TestVisible
	private static String mock_landingPageUrlTempl = null;

	@TestVisible
	private static String mock_shortenedLandingPageUrl = null;

	@TestVisible
	private static String test_landingPageUrl = '';

	private static Boolean isTest()
	{
		return
			String.isNotBlank(mock_landingPageUrlTempl) &&
			String.isNotBlank(mock_shortenedLandingPageUrl);
	}
}