public with sharing class DateHelpers {
  public enum DateResolution { YEAR, MONTH, DAY }
  public enum DateTimeResolution { YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, MILLISECOND }
  public class InvalidDateRangeException extends Exception {}
  public class InvalidDateResolutionException extends Exception {}
  public class InvalidDateTimeResolutionException extends Exception {}

  public static Boolean Overlap(Date test, Date fromDate, Date toDate) {
    if (fromDate > toDate) {
      throw new InvalidDateRangeException('fromDate cannot be after toDate');
    }

    return test < fromDate || test > toDate;
  }

  public static Boolean Abut(Date d1, Date d2) {
    return Abut(d1, d2, DateResolution.DAY);
  }
  public static Boolean Abut(Date d1, Date d2, DateResolution resolution) {
    if (resolution == DateResolution.YEAR) {
      return Math.abs(d1.year() - d2.year()) == 1;
    } else if (resolution == DateResolution.MONTH) {
      d1 = d1.toStartOfMonth();
      d2 = d2.toStartOfMonth();
      return d1.addMonths(1) == d2 || d2.addMonths(1) == d1;
    } else if (resolution == DateResolution.DAY) {
      return d1.addDays(1) == d2 || d2.addDays(1) == d1;
    }

    throw new InvalidDateResolutionException();
  }

  public static Boolean Overlap(DateTime test, DateTime fromDate, DateTime toDate) {
    if (fromDate > toDate) {
      throw new InvalidDateRangeException('fromDate cannot be after toDate');
    }

    return fromDate < test && test < toDate;
  }

  public static Boolean Abut(DateTime dt1, DateTime dt2, DateTimeResolution resolution) {
    Date d1 = Date.newInstance(dt1.year(), dt1.month(), dt1.day());
    Date d2 = Date.newInstance(dt2.year(), dt2.month(), dt2.day());

    if (resolution == DateTimeResolution.YEAR) {
      return Abut(d1, d2, DateResolution.YEAR);
    } else if (resolution == DateTimeResolution.MONTH) {
      return Abut(d1, d2, DateResolution.MONTH);
    } else if (resolution == DateTimeResolution.DAY) {
      return Abut(d1, d2, DateResolution.DAY);
    } else if (resolution == DateTimeResolution.HOUR) {
      dt1 = DateTime.newInstance(dt1.date(), Time.newInstance(dt1.hour(), 0, 0, 0));
      dt2 = DateTime.newInstance(dt2.date(), Time.newInstance(dt2.hour(), 0, 0, 0));
      return dt1.addHours(1) == dt2 || dt2.addHours(1) == dt1;
    } else if (resolution == DateTimeResolution.MINUTE) {
      dt1 = DateTime.newInstance(dt1.date(), Time.newInstance(dt1.hour(), dt1.minute(), 0, 0));
      dt2 = DateTime.newInstance(dt2.date(), Time.newInstance(dt2.hour(), dt2.minute(), 0, 0));
      return dt1.addMinutes(1) == dt2 || dt2.addMinutes(1) == dt1;
    } else if (resolution == DateTimeResolution.SECOND) {
      dt1 = DateTime.newInstance(dt1.date(), Time.newInstance(dt1.hour(), dt1.minute(), dt1.second(), 0));
      dt2 = DateTime.newInstance(dt2.date(), Time.newInstance(dt2.hour(), dt2.minute(), dt2.second(), 0));
      return dt1.addSeconds(1) == dt2 || dt2.addSeconds(1) == dt1;
    } else if (resolution == DateTimeResolution.MILLISECOND) {
      dt1 = DateTime.newInstance(dt1.date(), Time.newInstance(dt1.hour(), dt1.minute(), dt1.second(), dt1.millisecond()));
      dt2 = DateTime.newInstance(dt2.date(), Time.newInstance(dt2.hour(), dt2.minute(), dt2.second(), dt2.millisecond()));

      return Math.abs(dt1.getTime() - dt2.getTime()) == 1;
    }

    throw new InvalidDateTimeResolutionException();
  }
}