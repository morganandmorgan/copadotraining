public interface mmlib_IAtomicDmlWorkUnitGettable
    extends fflib_SObjectUnitOfWork.IDML
{
    List<mmlib_AtomicDmlWorkUnit> getWorkUnitList();
}