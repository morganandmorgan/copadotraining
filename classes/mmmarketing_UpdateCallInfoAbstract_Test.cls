/**
 * mmmarketing_UpdateCallInfoAbstract_Test
 * @description Test for Update Call Info Abstract class.
 * @author Jeff Watson
 * @date 2/19/2019
 */
@IsTest
public with sharing class mmmarketing_UpdateCallInfoAbstract_Test {

    private static UpdateCallInfo updateCallInfo;
    private static Intake__c intake;
    private static Account personAccount;
    private static Marketing_Tracking_Info__c marketingTrackingInfo;

    static {
        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        marketingTrackingInfo = new Marketing_Tracking_Info__c();
        marketingTrackingInfo.Intake__c = intake.Id;
        marketingTrackingInfo.Tracking_Event_Timestamp__c = Datetime.now();
        marketingTrackingInfo.Calling_Phone_Number__c = '5555555555';
        insert marketingTrackingInfo;

        updateCallInfo = new UpdateCallInfo();
    }

    public class UpdateCallInfo extends mmmarketing_UpdateCallInfoAbstract {
        public override void performUpdateIfMatch(mmmarketing_IUpdateCallInfoLogic updateLogic, Marketing_Tracking_Info__c record, mmlib_BaseCallout.CalloutResponse listCallsCalloutResponse, String identifierThatFoundMatch) {}
    }

    @IsTest
    private static void getAvailableAuthorizationsCount() {
        Integer count = updateCallInfo.getAvailableAuthorizationsCount();
        System.assertEquals(10, count);
    }

    @IsTest
    private static void isValid() {
        Marketing_Tracking_Info__c marketingTrackingInfo1 = new Marketing_Tracking_Info__c();
        marketingTrackingInfo1.Intake__c = intake.Id;
        insert marketingTrackingInfo1;
        String result = null;

        result = updateCallInfo.isValid(marketingTrackingInfo1);
        System.assertEquals('Tracking_Event_Timestamp__c is null', result);

        marketingTrackingInfo1.Tracking_Event_Timestamp__c = Datetime.now();
        result = updateCallInfo.isValid(marketingTrackingInfo1);
        System.assertEquals('Calling_Phone_Number__c is blank', result);

        marketingTrackingInfo1.Calling_Phone_Number__c = '5555';
        result = updateCallInfo.isValid(marketingTrackingInfo1);
        System.assertEquals('Calling_Phone_Number__c is the internal extension.  Possible transfer.', result);

        marketingTrackingInfo1.Calling_Phone_Number__c = '5555555555';
        result = updateCallInfo.isValid(marketingTrackingInfo1);
        System.assertEquals('Basic validation is OK.\n', result);
    }

    @IsTest
    private static void debug() {
        mmmarketing_UpdateCallInfoAbstract updateCallInfoAbstract = updateCallInfo.debug();
        System.assert(updateCallInfoAbstract != null);
    }

    @IsTest
    private static void getUow() {
        mmlib_ISObjectUnitOfWork uow = updateCallInfo.getUow();
        System.assert(uow != null);
    }

    @IsTest
    private static void disableCommitWork() {
        mmmarketing_UpdateCallInfoAbstract updateCallInfoAbstract = updateCallInfo.disableCommitWork();
        System.assert(updateCallInfoAbstract != null);
    }

    @IsTest
    private static void run() {
        updateCallInfo.run(new List<Marketing_Tracking_Info__c> {marketingTrackingInfo});
    }
}