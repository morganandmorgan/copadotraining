/**
 *  mmmarketing_MTINotLinkedCriteria
 */
public class mmmarketing_MTINotLinkedCriteria
    implements mmlib_ICriteria
{
    private list<Marketing_Tracking_Info__c> records = new list<Marketing_Tracking_Info__c>();

    public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
    {
        if (records != null
            && Marketing_Tracking_Info__c.SObjectType == records.getSobjectType()
            )
        {
            this.records.addAll( (list<Marketing_Tracking_Info__c>) records);
        }

        return this;
    }

    public list<SObject> run()
    {
        list<Marketing_Tracking_Info__c> qualifiedRecords = new list<Marketing_Tracking_Info__c>();

        for (Marketing_Tracking_Info__c record : this.records)
        {
            if (record.Marketing_Campaign__c == null
                || record.Marketing_Source__c == null
                || record.Marketing_Financial_Period__c == null
                || ( record.Marketing_Exp_Variation__c == null
                    && String.isNotBlank(record.Experiment_Value__c)
                    && String.isNotBlank(record.Experiment_Variation_Value__c)
                    )
                )
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}