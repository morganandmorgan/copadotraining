/**
 *  mmmarketing_ITrackInfoRecordCallRequest
 */
public interface mmmarketing_ITrackInfoRecordCallRequest
{
    mmmarketing_ITrackInfoRecordCallRequest setCallingNumber( String callingNumber );
    mmmarketing_ITrackInfoRecordCallRequest setCallStartTime( Datetime callStartTime );
    mmmarketing_ITrackInfoRecordCallRequest setRelatedIntakeId( Id relatedIntakeId );

    String getCallingNumber();
    Datetime getCallStartTime();
    Id getRelatedIntakeId();
}