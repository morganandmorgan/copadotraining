public with sharing class mmdocusign_CreateEnvelopeExtension
{
	private ApexPages.PageReference page = null;
	private Intake__c record = null;
	private Account clientAccount = null;

	public mmdocusign_CreateEnvelopeExtension(ApexPages.StandardController cont)
	{
		record = mmintake_IntakesSelector.newInstance().selectById(new Set<Id> {cont.getId()}).get(0);
		clientAccount = mmcommon_AccountsSelector.newInstance().selectById(new Set<Id> {record.Client__c}).get(0);

		page = ApexPages.currentPage();
	}

	// Facilitates testability.
	public mmdocusign_CreateEnvelopeExtension(Intake__c intake)
	{
		record = intake;
		clientAccount = mmcommon_AccountsSelector.newInstance().selectById(new Set<Id> {record.Client__c}).get(0);
		System.debug('<ojs> clientAccount:\n' + clientAccount);
		page = ApexPages.currentPage();
	}

	private String displayText_m = 'Contacting Docusign to create Envelope.';

	public String getDisplayText()
	{
		return displayText_m;
	}

	public ApexPages.PageReference execute()
	{
		if (!'Morgan & Morgan'.equalsIgnoreCase(record.Handling_Firm__c))
		{
			setDisplayTextToException(
				new mmdocusign_IntegrationExceptions.ParameterException(
					'Only Intakes being handled by Morgan & Morgan can be sent for electronic signature.'));
			return null;
		}

		if (clientAccount == null || String.isBlank(clientAccount.PersonMobilePhone))
		{
			setDisplayTextToException(
				new mmdocusign_IntegrationExceptions.ParameterException(
					'The Intake\'s client must have a mobile phone to electronic signatures via SMS.'));
			return null;
		}

		mmdocusign_CreateEnvelopeResponseData envelopeData = null;
		
		try
		{
			envelopeData = mmdocusign_DocusignRestApiService.createEnvelope(record);
		}
		catch (Exception exc)
		{
			setDisplayTextToException(exc);
			return null;
		}

		System.debug('<ojs> envelopeData:\n' + envelopeData);

		if(envelopeData != null)
		{
			
			try
			{
				Task newTask = 
					mmdocusign_DocusignRestApiService.createTasksForEnvelope(
						record, 
						envelopeData.getEnvelopeId(), 
						envelopeData.getClientUserId());
				System.debug('<ojs> newTask:\n' + newTask);
			}
			catch (Exception exc)
			{
				setDisplayTextToException(exc);
				return page;
			}
		}
		else
		{
			setDisplayTextToException(
				new mmdocusign_IntegrationExceptions.CalloutException(
					'A Docusign envelope was not generated.'));
			return page;
		}

		return navigateToRecordDetail();
	}

	public ApexPages.PageReference navigateToRecordDetail()
	{
		return new ApexPages.PageReference('/' + record.Id);
	}

	private void setDisplayTextToException(Exception exc)
	{
		System.debug('<ojs> exc in setDisplayTextToException:\n' + exc);
		displayText_m =
			exc.getMessage() + '\n' +
			exc.getStackTraceString();
	}
}