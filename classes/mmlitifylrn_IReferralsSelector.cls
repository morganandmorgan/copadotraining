public interface mmlitifylrn_IReferralsSelector extends mmlib_ISObjectSelector
{
    List<litify_pm__Referral__c> selectById( Set<id> idSet );

    List<litify_pm__Referral__c> selectWithFirmAndCaseTypeAndTransactionsByExternalId( Set<Double> externalIdSet );

    List<litify_pm__Referral__c> selectWithFirmAndCaseTypeById( Set<id> idSet );

    String selectWithFirmByExternalIdQuery( );
}