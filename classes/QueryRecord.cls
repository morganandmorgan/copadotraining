public with sharing class QueryRecord
{
    @AuraEnabled(cacheable=true)
    public static litify_pm__Matter__c[] Query(String term)
    { 
        term = '%' + term + '%';
        return [select Id, ReferenceNumber__c, litify_pm__Display_Name__c from litify_pm__Matter__c where ReferenceNumber__c like: term or litify_pm__Display_Name__c like: term limit 30000];
    }

    @AuraEnabled(cacheable=true)
    public static litify_pm__Matter__c[] QueryFake(String term)
    {
        // This method returns data for query 988 on prod
        StaticResource resource = [select Id, Body from StaticResource where Name = 'QueryRecordJson' limit 1];
        return (litify_pm__Matter__c[])JSON.deserialize(resource.Body.toString(), litify_pm__Matter__c[].class);
    }
}