global class SpringMigrationStart {
    WebService static void StartMigration(string sfType, string[] attributes, boolean allObjects) {
        string csvString = '' ;
        SpringCMEos__EOS_Type__c eosObjectType = [Select SpringCMEos__Folder_Name__c, SpringCMEos__Path__c, SpringCMEos__Variables__c, SpringCMEos__Folder_Name_Format__c, SpringCMEos__Path_Format__c, Name from SpringCMEos__EOS_Type__c where Name = : sfType ];
        
        string query = '';
        
        Set<string> selections = new Set<String>();
        
        string [] variables = null;
        // Build Query
        if (eosObjectType.SpringCMEos__Variables__c != null && eosObjectType.SpringCMEos__Variables__c != '') {
            variables = ((string)eosObjectType.SpringCMEos__Variables__c).split(',');
            selections.addAll(variables);
            }
        else
            query = 'Select Id,(Select Id from Attachments)  from ' +  String.escapeSingleQuotes(sfType);
        
        if (attributes != null && attributes.size() != 0)
            selections.addAll(attributes);
        
        query = 'Select Id,';
        
        for (string s : selections) {
            query += s+',';
            }
        query += ' (Select Id from Attachments)  from ' +  String.escapeSingleQuotes(sfType);
        
        System.debug(query);
        System.debug(attributes);
        SpringMigrator sm = new SpringMigrator(query, sfType, variables, eosObjectType, attributes, allObjects);
        
        Id batchProcessId = Database.executeBatch(sm);
        System.debug('Returned batch process ID: ' + batchProcessId);
        }
    
    WebService static void Start(string sfType, string[] attributes) {
        StartMigration(sfType, attributes, false);
        }
    }