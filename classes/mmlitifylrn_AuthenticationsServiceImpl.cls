public class mmlitifylrn_AuthenticationsServiceImpl
    implements mmlitifylrn_IAuthenticationsService
{
    public void authenticateUser(string lrnUsername, string lrnPassword, id lrnFirmId)
    {
        if ( lrnFirmId == null
            || String.isBlank( lrnUsername )
            || String.isBlank( lrnPassword )
            )
        {
            String message = '';

            if ( String.isBlank( lrnUsername ) )
            {
                message +=  'Username must be provided.';
            }

            if ( String.isBlank( lrnPassword ) )
            {
                message += String.isNotBlank( message ) ? '\n' : '';
                message +=  'Password must be provided.';
            }

            if ( lrnFirmId == null )
            {
                message += String.isNotBlank( message ) ? '\n' : '';
                message += litify_pm__Firm__c.SObjectType.getDescribe().Label + ' must be provided.';
            }

            throw new mmlitifylrn_Exceptions.AuthenticationsServiceException( message );
        }

        //find the firm
        list<litify_pm__Firm__c> firms = mmlitifylrn_FirmsSelector.newInstance().selectById( new Set<id>{ lrnFirmId } );

        if ( firms.isempty() )
        {
            throw new mmlitifylrn_Exceptions.AuthenticationsServiceException( 'Could not find ' + litify_pm__Firm__c.SObjectType + ' record with id of \'' + lrnFirmId + '\'.');
        }

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        try
        {
            litify_pm__Firm__c firm = firms[0];

            mmlitifylrn_ICredentials credentials = mmlitifylrn_Credentials.newInstance( mmlitifylrn_CredentialsSelector.newInstance().selectByName( new set<string>{ firm.name } ) );

            credentials.authenticateForFirm( firm, lrnUsername, lrnPassword, uow );

            uow.commitWork();
        }
        catch (mmlitifylrn_Exceptions.CalloutException ce)
        {
            throw new mmlitifylrn_Exceptions.AuthenticationsServiceException( ce.getCalloutErrorMessage() );
        }
        catch (Exception e)
        {
            Exception exceptionToBundle = e;
            system.debug( e );
            if ( e.getCause() != null )
            {
                exceptionToBundle = e.getCause();
            }

            throw new mmlitifylrn_Exceptions.AuthenticationsServiceException( exceptionToBundle );
        }
    }
}