public interface mmwiz_IQuestionAndAnswerSelector extends mmlib_ISObjectSelector
{
    List<QuestionAndAnswer__c> selectBySessionGuid(Set<String> sessionGuidSet);
}