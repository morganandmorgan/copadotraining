/**
 *  mmlib_IAction
 */
public interface mmlib_IAction
{
    mmlib_IAction setRecordsToActOn( List<SObject> recordsToActOn );
    Boolean run();
}