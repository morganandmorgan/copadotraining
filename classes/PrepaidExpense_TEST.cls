/*============================================================================
Name            : PrepaidExpense_TEST
Author          : CLD
Created Date    : July 2018
Description     : Test class for Prepaid Expense Batch processing
=============================================================================*/
@isTest
public with sharing class PrepaidExpense_TEST {

    public static litify_pm__Matter__c matter;
    public static c2g__codaBankAccount__c testBankAccount;
    public static Set<Id> expenseIds;
    public static c2g__codaCompany__c company;

    static void setupData()
    {
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        update company;
        company = [SELECT Id, Name, OwnerId FROM c2g__codaCompany__c WHERE Id = :company.Id];
        testBankAccount = ffaTestUtilities.createBankAccount(company, null,testGLA.Id);

        /*FFA_Custom_Setting__c ffaSetting = new FFA_Custom_Setting__c(
            Fees_GLA_Account__c = testGLA.Id);
        insert ffaSetting;*/

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        
        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        litify_pm__Expense_Type__c expenseType1 = new litify_pm__Expense_Type__c();
        expenseType1.CostType__c = 'SoftCost';
        expenseType1.Name = 'Telephone';
        expenseType1.ExternalID__c = 'TELEPHONE';
        expenseType1.General_Ledger_Account__c = testGLA.Id;
        expenseType1.Prepaid_Expense_Type__c = true;
        expenseType1.Prepaid_Expense_Reverse_GLA__c = testGLA.Id;
        INSERT expenseType1;

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Internet';
        expenseType2.ExternalID__c = 'INTERNET1';
        expenseType2.General_Ledger_Account__c = testGLA.Id;
        expenseType1.Prepaid_Expense_Type__c = true;
        expenseType2.Prepaid_Expense_Reverse_GLA__c = testGLA.Id;

        INSERT expenseType2;

        litify_pm__Expense__c testExpense1 = new litify_pm__Expense__c();

        testExpense1.litify_pm__Matter__c = matter.Id;
        testExpense1.litify_pm__Amount__c = 100.0;
        testExpense1.litify_pm__Date__c = date.today();
        testExpense1.litify_pm__ExpenseType2__c = expenseType1.Id;
        INSERT testExpense1;

        litify_pm__Expense__c testExpense2 = new litify_pm__Expense__c();

        testExpense2.litify_pm__Matter__c = matter.Id;
        testExpense2.litify_pm__Amount__c = 10.0;
        testExpense2.litify_pm__Date__c = date.today();
        testExpense2.litify_pm__ExpenseType2__c = expenseType2.Id;

        INSERT testExpense2;    
        expenseIds = new Set<Id>{testExpense1.Id, testExpense2.Id};
    }

    /*--------------------------------------------------------------------
        START TEST METHODS
    --------------------------------------------------------------------*/
    @isTest static void testBatchJob(){

        setupData();

        Integer BATCH_SIZE = 2;
	    FFAPrepaidExpense_Job b = new FFAPrepaidExpense_Job(Date.today(), company.Id, true);
	    Id batchID = database.executebatch(b, BATCH_SIZE);

    }

}