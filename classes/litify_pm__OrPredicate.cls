/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OrPredicate implements litify_pm.IPredicate {
    global OrPredicate() {

    }
    global litify_pm.OrPredicate Append(litify_pm.IPredicate p) {
        return null;
    }
    global static litify_pm.OrPredicate NewInstance() {
        return null;
    }
    global static litify_pm.OrPredicate NewInstance(litify_pm.IPredicate p1, litify_pm.IPredicate p2) {
        return null;
    }
    global static litify_pm.OrPredicate NewInstance(litify_pm.IPredicate p1, litify_pm.IPredicate p2, litify_pm.IPredicate p3) {
        return null;
    }
    global static litify_pm.OrPredicate NewInstance(litify_pm.IPredicate p1, litify_pm.IPredicate p2, litify_pm.IPredicate p3, litify_pm.IPredicate p4) {
        return null;
    }
    global static litify_pm.OrPredicate NewInstance(litify_pm.IPredicate p1, litify_pm.IPredicate p2, litify_pm.IPredicate p3, litify_pm.IPredicate p4, litify_pm.IPredicate p5) {
        return null;
    }
    global static litify_pm.OrPredicate NewInstance(List<litify_pm.IPredicate> ps) {
        return null;
    }
    global override String toString() {
        return null;
    }
}
