public class DocuSignStatusObtainUserInfo_TrigAct
    extends TriggerAction
{

    public DocuSignStatusObtainUserInfo_TrigAct()
    {
        super();
    }

    public override Boolean shouldRun()
    {
        return isBefore() && isInsert();
    }

    public override void doAction()
    {
        List<dsfs__DocuSign_Status__c> docuSignStatuses = (List<dsfs__DocuSign_Status__c>) triggerList;

        Map<String, List<dsfs__DocuSign_Status__c>> statusesBySenderName = new Map<String, List<dsfs__DocuSign_Status__c>>();

        for (dsfs__DocuSign_Status__c docuSignStatus : docuSignStatuses)
        {
            String senderName = docuSignStatus.dsfs__Sender__c.toLowerCase();

            if (String.isNotBlank(senderName))
            {
                List<dsfs__DocuSign_Status__c> statuses = statusesBySenderName.get(senderName);

                if (statuses == null)
                {
                    statuses = new List<dsfs__DocuSign_Status__c>();
                    statusesBySenderName.put(senderName, statuses);
                }

                statuses.add(docuSignStatus);
            }
        }

        if ( ! statusesBySenderName.isEmpty())
        {
            List<User> users = [
                SELECT Name,
                    Profile.Name,
                    UserRole.Name
                FROM
                    User
                WHERE
                    Name IN :statusesBySenderName.keySet()
            ];

            for (User u : users)
            {
                if (statusesBySenderName.containsKey(u.Name.toLowerCase()))
                {
                    for (dsfs__DocuSign_Status__c docuSignStatus : statusesBySenderName.get(u.Name.toLowerCase()))
                    {
                        docuSignStatus.Sender_Profile__c = u.Profile.Name;
                        docuSignStatus.Sender_Role__c = u.UserRole.Name;
                    }
                }
            }
        }
    }
}