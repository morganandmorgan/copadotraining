@istest
public class MTI_SchedulableTest {

    static testmethod void test() {
        Account acc = TestUtil.createPersonAccount('name', 'Account');
        insert acc;

        Intake__c intk = TestUtil.createIntake();
        intk.Client__c = acc.Id;
        insert intk;

        Marketing_Tracking_Info__c mti = TestUtil.createMarketingTrackingInfo(intk);
        insert mti;

        Test.startTest();
        String cronExp = '0 0 0 ? * * *';
        String jobId = System.schedule('testBasicScheduledApex', cronExp, new MTI_Schedulable());
        Test.stopTest();
    }

}