/**
 *  mmmarketing_SourcesSelector
 */
public with sharing class mmmarketing_SourcesSelector
    extends mmlib_SObjectSelector
    implements mmmarketing_ISourcesSelector
{
    public static mmmarketing_ISourcesSelector newInstance()
    {
        return (mmmarketing_ISourcesSelector) mm_Application.Selector.newInstance(Marketing_Source__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Marketing_Source__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Marketing_Source__c.utm_source__c,
                Marketing_Source__c.Description__c,
                Marketing_Source__c.Default_Financial_Period__c
            };
    }

    public List<Marketing_Source__c> selectById(Set<Id> idSet)
    {
        return (List<Marketing_Source__c>) selectSObjectsById(idSet);
    }

    public list<Marketing_Source__c> selectByName( Set<String> nameSet )
    {
        return Database.query( newQueryFactory().setCondition('Name in :nameSet').toSOQL() );
    }

    public List<Marketing_Source__c> selectByUtm( Set<String> utmSet )
    {
        return Database.query( newQueryFactory().setCondition(Marketing_Source__c.utm_source__c + ' in :utmSet').toSOQL() );
    }

}