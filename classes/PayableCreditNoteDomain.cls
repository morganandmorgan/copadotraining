/*============================================================================
Name            : PayableCreditNoteDomain
Author          : CLD
Created Date    : Aug 2019
Description     : Domain class for Payable Credit Note
=============================================================================*/
public class PayableCreditNoteDomain extends fflib_SObjectDomain {

    private List<c2g__codaPurchaseCreditNote__c> payableCreditNotes;
    private PayableCreditNoteService pcnService;

    // Ctors
    public PayableCreditNoteDomain() {
        super();
        this.payableCreditNotes = new List<c2g__codaPurchaseCreditNote__c>();
        this.pcnService = new PayableCreditNoteService();
    }

    public PayableCreditNoteDomain(List<c2g__codaPurchaseCreditNote__c> payableCreditNotes) {
        super(payableCreditNotes);
        this.payableCreditNotes = (List<c2g__codaPurchaseCreditNote__c>) records;
        this.pcnService = new PayableCreditNoteService();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new PayableCreditNoteDomain(records);
        }
    }

    // Trigger Handlers
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {

        Map<Id, c2g__codaPurchaseCreditNote__c> oldTriggerMap = (Map<Id, c2g__codaPurchaseCreditNote__c>) existingRecords;

        //validate that the current company matches the matter company.
        pcnService.validateAmount(payableCreditNotes);
    }
   
    // Trigger Handlers
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        
        pcnService.voidExpenses(payableCreditNotes);
    }
}