global class mmdice_RecordInfo
{
    public mmdice_RecordInfo()
    {
        this.url = '/apex/CTIScreenPopOutbound?id=' + this.intake_id;
        this.number_outbound_dials = 0;
        this.preview_required = false;
        this.compliance_required = false;
    }

    public mmdice_RecordInfo(Intake__c intakeSObj)
    {
        this.intake_id = intakeSObj.Id;
        if(intakeSObj.Client__r != null)
        {
            this.account_id = intakeSObj.Client__r.Id;
            this.contact_id = intakeSObj.Client__r.Contact_ID__c;
            this.first_name = intakeSObj.Client__r.FirstName;
            this.last_name = intakeSObj.Client__r.LastName;
            this.city = intakeSObj.Client__r.BillingCity;
            this.state = intakeSObj.Client__r.BillingState;
            this.zip_code = intakeSObj.Client__r.BillingPostalCode;
            this.phone_1 = intakeSObj.Client__r.Phone;
            this.phone_2 = intakeSObj.Client__r.PersonMobilePhone;
            this.phone_3 = intakeSObj.Client__r.Work_Phone__c;
            this.phone_4 = '';
            this.preview_required = (intakeSObj.Client__r.SMS_Text_Automated_Communication_OptOut__c != null) ? intakeSObj.Client__r.SMS_Text_Automated_Communication_OptOut__c : false;
            this.compliance_required = false;
        }
        this.litigation = intakeSObj.Litigation__c;
        this.url = '/apex/CTIScreenPopOutbound?id=' + this.intake_id;
        this.number_outbound_dials = (intakeSObj.Number_Outbound_Dials_Intake__c != null) ? Integer.valueOf(intakeSObj.Number_Outbound_Dials_Intake__c) : 0;
    }

    public String id
    {
        get { return this.intake_id + '-' + this.number_outbound_dials; }
    }

    public String intake_id { get; set; }

    public String account_id { get; set; }

    public String contact_id { get; set; }

    public String litigation { get; set; }

    public String first_name { get; set; }

    public String last_name { get; set; }

    public String city { get; set; }

    public String state { get; set; }

    public String zip_code { get; set; }

    public String phone_1 { get; set; }

    public String phone_2 { get; set; }

    public String phone_3 { get; set; }

    public String phone_4 { get; set; }

    public String url { get; set; }

    public Integer number_outbound_dials { get; set; }

    public Boolean preview_required { get; set; }

    public Boolean compliance_required { get; set; }
}