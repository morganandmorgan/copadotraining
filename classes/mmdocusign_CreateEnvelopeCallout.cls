public class mmdocusign_CreateEnvelopeCallout
	extends mmlib_BasePOSTCallout
{
	private Intake__c intake;

	private String intakeId = '';
	private String clientEmail = '';
	private String clientName = '';
	private String envelopeTemplateId = '';

	public mmdocusign_CreateEnvelopeCallout(Intake__c intake)
	{
		intakeId = intake.Id;
		clientEmail = intake.Client_Email_txt__c;
		clientName = intake.Client_First_Last_Name__c;
		envelopeTemplateId = intake.DocusignTemplateID__c;
	}

	public mmdocusign_CreateEnvelopeCallout(String intakeId, String envelopeTemplateId, String clientEmail, String clientName)
	{
		this.intakeId = intakeId;
		this.clientEmail = clientEmail;
		this.clientName = clientName;
		this.envelopeTemplateId = envelopeTemplateId;
	}

	// ---------- mmlib_BasePOSTCallout Implementation --------------
	public override mmlib_BaseCallout execute()
	{
		if
		(
			mmdocusign_ConfigurationSettings.getIsEnabled() == false ||
			(mmlib_Utils.org.IsSandbox && mmdocusign_ConfigurationSettings.getIsEnabledInSandbox() == false)
		)
		{
			throw new mmdocusign_IntegrationExceptions.CalloutException('This feature is disabled via settings.  Change the settings to activate.');
		}

		setAuthorization(new mmdocusign_RestApiCalloutAuthorization());

		getHeaderMap().put('Content-Type', 'application/json');

		String clientUserId = mmlib_Utils.generateGuid();

		String requestBody =
		'{' +
		'	"customFields": {' +
		'		"textCustomFields": [' +
		'			{' +
		'				"name": "DSFSSourceObjectId",' +
		'				"show": "false",' +
		'				"value": "' + intakeId + '"' +
		'			},' +
		'			{' +
		'				"name": "SFUserId",' +
		'				"show": "false",' +
		'				"value": "' + UserInfo.getUserId() + '"' +
		'			},' +
		'			{' +
		'				"name": "CreatedByRestApi",' +
		'				"show": "false",' +
		'				"value": "true"' +
		'			}' +
		'		]},' +
		'	"status": "sent",' +
		'	"templateId": "' + envelopeTemplateId + '",' +
		'	"templateRoles": [{' +
		'			"email": "' + clientEmail + '",' +
		'			"roleName": "Signer 1",' +
		'			"name": "' + clientName + '",' +
		'			"clientUserId": "' + clientUserId + '"' +
		'	}]' +
		'}';
		test_requestBody = requestBody;

		System.debug('<ojs> requestBody:\n' + requestBody);

		addPostRequestBodyLine( requestBody );

		HttpResponse httpResp = super.executeCallout();

        System.debug('<ojs> httpResp:\n' + httpResp);

		String responseBody = '';

        if (httpResp.getStatusCode() == 200 || httpResp.getStatusCode() == 201)
        {
            responseBody = httpResp.getBody();
        }
        else
        {
            mmdocusign_IntegrationExceptions.CalloutException exc = new mmdocusign_IntegrationExceptions.CalloutException();
            exc.setMessage(
                'Callout returned a failing status code.\n' +
                httpResp.getBody()
            );
            throw exc;
        }

        if (isDebugOn)
        {
            System.debug('responseBody:\n' + responseBody);
        }

        calloutResponse = new mmdocusign_CreateEnvelopeResponse(responseBody, clientUserId);

        return this;
	}

	protected override String getHost()
	{
		return mmdocusign_ConfigurationSettings.getHost();
	}

	protected override Map<String, String> getParamMap()
	{
		return new Map<String, String>();
	}

	protected override String getPath()
	{
		return '/restapi/v2/accounts/' + mmdocusign_ConfigurationSettings.getAccountId() + '/envelopes';
	}

	private mmdocusign_CreateEnvelopeResponse calloutResponse = null;

	public override CalloutResponse getResponse()
	{
		return (mmlib_BaseCallout.CalloutResponse) calloutResponse;
	}

	// ============== Test Context ====================================================

	@TestVisible
	private static Boolean runAsTest = false;

	@TestVisible
	private static String test_requestBody = '';
}