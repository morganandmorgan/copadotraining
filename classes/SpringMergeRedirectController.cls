public class SpringMergeRedirectController  {
    
    
    public final litify_pm__Matter__c matter;
    public SpringMergeRedirectController(ApexPages.StandardController stdController) {
       this.matter =  [select id,Name,ReferenceNumber__c from litify_pm__Matter__c where Id = :stdController.getRecord().Id limit 1];
        Spring_CM_Matter_Forms__mdt config = [select Multi_Merge_URL__c from Spring_CM_Matter_Forms__mdt limit 1];
		this.url = config.Multi_Merge_URL__c +'&matterId=' + string.valueOf(this.matter.Id) + '&matterName=' + this.matter.Name + '&userId=' + UserInfo.getUserId()+ '&sessionId=' + UserInfo.getSessionId() + '&host=' + ApexPages.currentPage().getHeaders().get('Host') + '&matterRef=' + this.matter.ReferenceNumber__c;
      
			
    }

    
    public PageReference redirect(){
        
            PageReference pageRef = new PageReference(this.url);
            return pageRef;
        
    }
    public string url {get;set;}
       
        	
      
    
}