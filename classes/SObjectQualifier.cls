public with sharing class SObjectQualifier
{
    public class InvalidConditionException extends Exception {}

    /*
    * SUPPORTING CLASSES
    */
    private class QueryContainer
    {
        List<Query> container {get; set;}
    }

    public class Query
    {
        public string column { get; set; }
        public string operator { get; set; }
        public string argument { get; set; }
        public string formula { get; set; }
    }

    public static List<Query> parseQuery(String strJson)
    {
        if (strJson == null || strJson.length() < 2)
        {
            return new List<Query>();
        }

        QueryContainer queryItemsContainer = (QueryContainer) JSON.deserialize('{"container":' + strJson + '}', QueryContainer.class);

        return queryItemsContainer.container;
    }

    private Map<String, Schema.SoapType> sObjectFields;

    private Schema.sObjectType sObjectType;

    public SObjectQualifier( Schema.sObjectType objectType )
    {
        sObjectType = objectType;
        populateSObjectFields();
    }

    public Boolean criteriaMatches(String qry, SObject objectUnderTest)
    {
        List<Query> queries = parseQuery(qry);

        return criteriaMatches(queries, objectUnderTest);
    }

    public Boolean criteriaMatches(List<Query> queries, SObject objectUnderTest)
    {
        if (queries == null || queries.size() == 0)
        {
            return false;
        }

        for (Query q : queries)
        {
            Object fieldValue = objectUnderTest.get(q.column);

            if ( ! queryMatches(q, fieldValue))
            {
                return false;
            }
        }

        return true;
    }

    public sObject getFullObjectById(Id sObjectId)
    {
        // Get info on the underlying object
        Schema.SObjectType token = sObjectId.getSobjectType();
        Schema.DescribeSObjectResult description = token.getDescribe();
        String typeName = description.getName();
        Map<String, Schema.SObjectField> fields = description.fields.getMap();

        String queryColumns = '';
        for (String fieldName : fields.keySet())
        {
            if (queryColumns != '')
            {
                queryColumns += ', ';
            }
            queryColumns += fields.get(fieldName).getDescribe().getName();
        }

        String query = 'SELECT ' + queryColumns + ' FROM ' + typeName + ' WHERE Id = :sObjectId LIMIT 1';

        SObject returnObject = Database.Query(query);

        return returnObject;
    }

    private Boolean containsOnly (String val, List<String> arguments)
    {
        for (String v : val.split(';'))
        {
            boolean contains = false;

            for (String a : arguments)
            {
                if (a == v.trim())
                {
                    contains = true;
                    break;
                }
            }

            if ( ! contains )
            {
                return false;
            }
        }

        return true;
    }

    private Boolean fieldMatches(Boolean value, Query q)
    {
        if (q.operator == '=')
        {
            return value == Boolean.valueOf(q.argument);
        }

        if (q.operator == '<>')
        {
            return value != Boolean.valueOf(q.argument);
        }

        throw new InvalidConditionException(q.operator + ' is not a valid operator for Boolean');
    }

    private Boolean fieldMatches(Date value, Query q)
    {
        DateTime v = DateTime.newInstance(value, Time.newInstance(0,0,0,0));

        return fieldMatches(v, q);
    }

    private Boolean fieldMatches(DateTime value, Query q)
    {
        DateTime dv;

        if ( String.isBlank(q.formula) )
        {
            dv = DateTime.newInstance(Date.parse(q.argument), Time.newInstance(0,0,0,0));
        }
        else
        {
            dv = DateTime.now();

            if (q.formula == 'days_ago')
            {
                dv = dv.addDays(-1 * Integer.valueOf(q.argument));
            }
            else if (q.formula =='months_ago')
            {
                dv = dv.addMonths(-1 * Integer.valueOf(q.argument));
            }
            else if (q.formula == 'years_ago')
            {
                dv = dv.addYears(-1 * Integer.valueOf(q.argument));
            }
        }

        if (q.operator == '>')
        {
            return value.getTime() > dv.getTime();
        }

        if (q.operator == '<')
        {
            return value.getTime() < dv.getTime();
        }

        if (q.operator == '>=')
        {
            return value.getTime() >= dv.getTime();
        }

        if (q.operator == '<=')
        {
            return value.getTime() <= dv.getTime();
        }

        if (q.operator == '=')
        {
            return value.getTime() == dv.getTime();
        }

        if (q.operator == '<>')
        {
            return value.getTime() != dv.getTime();
        }

        throw new InvalidConditionException(q.operator + ' is not a valid operator for Date/DateTime');
    }

    private Boolean fieldMatches(Double value, Query q)
    {
        if (q.operator == '<')
        {
            return value < Double.valueOf(q.argument);
        }

        if (q.operator == '<=')
        {
            return value <= Double.valueOf(q.argument);
        }

        if (q.operator == '=')
        {
            return value == Double.valueOf(q.argument);
        }

        if (q.operator == '<>')
        {
            return value != Double.valueOf(q.argument);
        }

        if (q.operator == '>')
        {
            return value > Double.valueOf(q.argument);
        }

        if (q.operator == '>=')
        {
            return value >= Double.valueOf(q.argument);
        }

        throw new InvalidConditionException(q.operator + ' is not a valid condition for Number');
    }

    private Boolean fieldMatches(Integer val, Query q)
    {
        Double v = Double.valueOf(val);

        return fieldMatches(v, q);
    }

    private Boolean fieldMatches(String val, Query q)
    {
        val = val.toLowerCase();

        List<String> arguments = q.argument.split('\n');

        System.debug('split arguments = ' + arguments);

        if (q.operator == 'contains_only')
        {
            return ContainsOnly(val, arguments);
        }

        Boolean match = false;

        for (String arg : arguments)
        {
            System.debug('checking value ' + arg);

            if (q.operator == '=')
            {
                match = (val == arg);
            }
            else if (q.operator == '<>')
            {
                match = (val != arg);
            }
            else if (q.operator == 'contains')
            {
                System.debug(arg + ' contains ' + val + '?');
                match = val.contains(arg);
                System.debug(match);
            }
            else if (q.operator == 'does_not_contain')
            {
                System.debug(arg + ' does not contain ' + val + '?');
                match = !val.contains(arg);
                System.debug(match);
            }
            else
            {
                throw new InvalidConditionException(q.operator + ' is not a valid condition for Text');
            }

            if (match)
            {
                return match;
            }
        }

        return false;
    }

    private void populateSObjectFields()
    {
        Schema.DescribeSObjectResult objDescription = sObjectType.getDescribe();

        sObjectFields = new Map<String, Schema.SoapType>();

        for (Schema.SobjectField f : objDescription.fields.getMap().values())
        {
            Schema.DescribeFieldResult dfr = f.getDescribe();

            sObjectFields.put(dfr.getName().toLowerCase(), dfr.getSoapType());
        }
    }

    private Boolean queryMatches(Query q, Object value)
    {
        if (value == null)
        {
            return false;
        }

        Schema.SoapType st = sObjectFields.get(q.column.toLowerCase());

        q.argument = q.argument.trim().toLowerCase();

        if (st == Schema.SoapType.Boolean)
        {
            return fieldMatches((Boolean)value, q);
        }

        if (st == Schema.SoapType.Date)
        {
            return fieldMatches((Date)value, q);
        }

        if (st == Schema.SoapType.DateTime)
        {
            return fieldMatches((DateTime)value, q);
        }

        if (st == Schema.SoapType.Double)
        {
            return fieldMatches((Double)value, q);
        }

        if (st == Schema.SoapType.Integer)
        {
            return fieldMatches((Integer)value, q);
        }

        if (st == Schema.SoapType.String)
        {
            return fieldMatches((String)value, q);
        }

        return false;
    }
}