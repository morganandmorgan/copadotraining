/*============================================================================
Name            : PayableCreditNoteService
Author          : CLD
Created Date    : Aug 2019
Description     : Service class for Payable Credit Note
=============================================================================*/
public with sharing class PayableCreditNoteService {
    public PayableCreditNoteService() {
    }
    /*-----------------------------------------------------------------------------
    Method to void all expenses associated with these Payable Invoices.
    -----------------------------------------------------------------------------*/
    public void voidExpenses(List<c2g__codaPurchaseCreditNote__c> creditNotes)
    {
        List<Id> pinIds = new List<Id>();
        FOR (c2g__codaPurchaseCreditNote__c creditNote : creditNotes)
        {
            if (creditNote.c2g__CreditNoteStatus__c == 'Complete')
                pinIds.add(creditNote.c2g__PurchaseInvoice__c);
        }

        List<litify_pm__Expense__c> expenseUpdateList = new List<litify_pm__Expense__c>();
        
        for(litify_pm__Expense__c exp : [
			SELECT Id,
				Voided__c,
				litify_pm__Amount__c
			FROM litify_pm__Expense__c
			WHERE Payable_Invoice__c in :pinIds
		]){
			exp.Voided__c = true;
			exp.litify_pm__Amount__c = 0;
			expenseUpdateList.add(exp);
		}

        UPDATE expenseUpdateList;

    }
    /*-----------------------------------------------------------------------------
    Method to validate the total amount of the PCN does not exceed the collection of payable invoices related
    -----------------------------------------------------------------------------*/
    public void validateAmount(List<c2g__codaPurchaseCreditNote__c> newTrigger) {
        Map<Id, Decimal> pinAmountMap = new Map<Id, Decimal>();
        Map<Id, Decimal> otherCreditAmountMap = new Map<Id, Decimal>();
        Set<Id> pinIds = new Set<Id>();
        Set<Id> currentPCNs = new Set<Id>();
        

        for(c2g__codaPurchaseCreditNote__c pcn : newTrigger){
            pinIds.add(pcn.c2g__PurchaseInvoice__c);
            currentPCNs.add(pcn.Id);
        }

        if(!pinIds.isEmpty()){
            for(c2g__codaPurchaseCreditNote__c existingPcn : [
                SELECT Id, c2g__PurchaseInvoice__c, c2g__CreditNoteTotal__c
                FROM c2g__codaPurchaseCreditNote__c
                WHERE Id not in : currentPCNs
                AND c2g__PurchaseInvoice__c in : pinIds
                AND c2g__CreditNoteTotal__c != null
            ]){
                String key = existingPcn.c2g__PurchaseInvoice__c;
                if(otherCreditAmountMap.containsKey(key)){
                    otherCreditAmountMap.put(key, otherCreditAmountMap.get(key) + existingPcn.c2g__CreditNoteTotal__c);
                }
                else{
                    otherCreditAmountMap.put(key, existingPcn.c2g__CreditNoteTotal__c);
                }
            }

            for(c2g__codaPurchaseInvoice__c pin : [
                SELECT Id, c2g__NetTotal__c
                FROM c2g__codaPurchaseInvoice__c
                WHERE Id in : pinIds
                AND c2g__NetTotal__c != null
            ]){
                String key = pin.Id;
                if(pinAmountMap.containsKey(key)){
                    pinAmountMap.put(key, pinAmountMap.get(key) + pin.c2g__NetTotal__c);
                }
                else{
                    pinAmountMap.put(key, pin.c2g__NetTotal__c);
                }
            }

            //validate the amount of the pcn + existing pcn is not greater than the related pin
            for(c2g__codaPurchaseCreditNote__c pcn : newTrigger){
                if(pcn.c2g__PurchaseInvoice__c != null && pcn.c2g__CreditNoteTotal__c != null){
                    String key = pcn.c2g__PurchaseInvoice__c;
                    Decimal otherPCNs = otherCreditAmountMap.containsKey(key) ? otherCreditAmountMap.get(key) : 0;
                    Decimal pcnTotal = otherPCNs +pcn.c2g__CreditNoteTotal__c;
                    Decimal pinTotal =  pinAmountMap.containsKey(key) ? pinAmountMap.get(key) : null;

                    if(pinTotal != null && pinTotal < pcnTotal){
                        pcn.addError('The total value of the Payable Credit Notes exceeds the total value of the related PIN, please check for other existing Payable Credit Notes.');
                    }
                }
            }
        }
    }
}