/**
 *  mmcommon_AccountsRelevancyOrganizer
 */
public class mmcommon_AccountsRelevancyOrganizer
{
    private list<Account> records = new list<Account>();
    private mmcommon_IAccountsSelectorPersonRequest request = null;
    private integer minimumNumberOfCriteriaMatched = 1;

    private boolean isHighPrecision = false;

    private Integer lowValueWeight = 1;
    private Integer highValueWeight = 7;
    private Integer qualifyingWeight = lowValueWeight;


    public mmcommon_AccountsRelevancyOrganizer setRecordsToSort( list<Account> records )
    {
        if ( records != null )
        {
            this.records.addAll( records );
        }

        return this;
    }

    public mmcommon_AccountsRelevancyOrganizer setRequestCriteria( mmcommon_IAccountsSelectorPersonRequest request )
    {
        this.request = request;

        this.minimumNumberOfCriteriaMatched = 1;

        integer numberOfCriteriaUsed = 0;

        if ( request.getBirthDate() != null )
        {
            numberOfCriteriaUsed++;
        }

        if ( String.isNotBlank( request.getFirstName() ) )
        {
            numberOfCriteriaUsed++;
        }

        if ( String.isNotBlank( request.getLastName() ) )
        {
            numberOfCriteriaUsed++;
        }

        if ( String.isNotBlank( request.getEmail() ) )
        {
            numberOfCriteriaUsed++;
        }

        if ( String.isNotBlank( request.getPhoneNumber() ) )
        {
            numberOfCriteriaUsed++;
        }

        // there should be at least 1 criteria matched.  if there are 4 or more parmeters used, then
        //  the number of matches should increase accordingly.
        this.minimumNumberOfCriteriaMatched = numberOfCriteriaUsed - 1 < 1 ? 1 : numberOfCriteriaUsed - 1;

        return this;
    }

    public mmcommon_AccountsRelevancyOrganizer setHighPrecisionFlag(Boolean value)
    {
        if (value)
        {
            qualifyingWeight = highValueWeight;
            isHighPrecision = true;
        }
        else
        {
            qualifyingWeight = lowValueWeight;
        }

        return this;
    }

    private boolean isAppropriateToAddRecordToResults( DerivedWeightingResponse dwr )
    {
        boolean result = dwr.recordWeightedRank >= qualifyingWeight;

        if ( isHighPrecision )
        {
            result = result
                        && dwr.isFirstNameExactMatch
                        && dwr.isLastNameExactMatch
                        && dwr.isWeightDerivedFromBothNamesAndAnotherFieldMatch();
        }

        return result;
    }

    public List<Account> filterAndSort()
    {
        List<Account> sortedRecords = new List<Account>();

        if (request == null)
        {
            throw new UsageException('Please provide a request criteria before executing filterAndSort.');
        }

        if ( ! records.isEmpty())
        {
            map<integer, list<Account>> accountsSortedByRankMap = new map<integer, list<Account>>();

            DerivedWeightingResponse dwr = null;

            for ( Account record : this.records )
            {
                //recordWeightedRank = deriveWeightedRank( record );
                dwr = deriveWeightedRank( record );

                system.debug( dwr.recordWeightedRank );

                system.debug('recordWeightedRank for ' + record.name + ' (' + record.id + ') is ' + dwr.recordWeightedRank
                                    + ' : FN = ' + dwr.isFirstNameExactMatch
                                    + ' : LN = ' + dwr.isLastNameExactMatch
                                    + ' : Both Types = ' + dwr.isWeightDerivedFromBothNamesAndAnotherFieldMatch()
                                    );

                if ( isAppropriateToAddRecordToResults( dwr ) )
                {
                    if ( ! accountsSortedByRankMap.containsKey( dwr.recordWeightedRank ) )
                    {
                        accountsSortedByRankMap.put( dwr.recordWeightedRank, new list<Account>());
                    }

                    accountsSortedByRankMap.get( dwr.recordWeightedRank ).add( record );
                }
            }

            list<Integer> ranksSortedList = new List<Integer>(accountsSortedByRankMap.keyset());
            ranksSortedList.sort();

            integer maxResultSetToReturn = 200;
            integer potentialResultSetSize = 0;

            for ( Integer i = (ranksSortedList.size() -1 ); i >= 0; i-- )
            {
                // system.debug( '>>>> ' + (sortedRecords.size() < maxResultSetToReturn));

                potentialResultSetSize = sortedRecords.size() + accountsSortedByRankMap.get( ranksSortedList[i] ).size();

                if ( potentialResultSetSize > maxResultSetToReturn )
                {
                    list<Account> currentRankList = accountsSortedByRankMap.get( ranksSortedList[i] );

                    system.debug( 'potentialResultSetSize = ' + potentialResultSetSize);
                    system.debug( 'maxResultSetToReturn = ' + maxResultSetToReturn);

                    for ( Integer j = 0; j < (maxResultSetToReturn - sortedRecords.size() + 1); j++ )
                    {
                        sortedRecords.add( currentRankList[j] );
                    }

                    // we add no more records and break out of this outer for loop
                    break;
                }
                else
                {
                    sortedRecords.addAll( accountsSortedByRankMap.get( ranksSortedList[i] ) );
                }
            }
        }

        return sortedRecords;
    }

    @TestVisible
    private DerivedWeightingResponse deriveWeightedRank( Account record )
    {
        DerivedWeightingResponse output = new DerivedWeightingResponse();

        if (record == null)
        {
            return output;
        }

        String workingStringToTest = null;

        integer numberOfCriteriaMatched = 0;

        boolean isCriteriaMatchedInSomeWay = false;

        if ( String.isNotBlank( this.request.getLastName() )
            && String.isNotBlank( record.lastName ) )
        {
            workingStringToTest = mmlib_Utils.clean( this.request.getLastName() );

            if (record.lastName.equalsIgnoreCase( workingStringToTest ))
            {
                output.recordWeightedRank += 3;
                isCriteriaMatchedInSomeWay = true;
                output.isLastNameExactMatch = true;
            }

            if (record.lastName.startsWithIgnoreCase( workingStringToTest ))
            {
                output.recordWeightedRank += 2;
                isCriteriaMatchedInSomeWay = true;
            }

            if (isCriteriaMatchedInSomeWay)
            {
                numberOfCriteriaMatched++;
                output.isSomeNameMatchMade = true;
            }
        }

        isCriteriaMatchedInSomeWay = false;

        if ( String.isNotBlank( this.request.getFirstName() )
            && String.isNotBlank( record.firstName ) )
        {
            workingStringToTest = mmlib_Utils.clean( this.request.getFirstName() );

            if (record.firstName.equalsIgnoreCase( workingStringToTest ))
            {
                output.recordWeightedRank += 1;
                isCriteriaMatchedInSomeWay = true;
                output.isFirstNameExactMatch = true;
            }

            if (record.firstName.startsWithIgnoreCase( workingStringToTest ))
            {
                output.recordWeightedRank += 1;
                isCriteriaMatchedInSomeWay = true;
            }

            if ( workingStringToTest.length() > 3
                && record.firstName.startsWithIgnoreCase( workingStringToTest.left(3) ))
            {
                output.recordWeightedRank += 1;
                isCriteriaMatchedInSomeWay = true;
            }

            if (isCriteriaMatchedInSomeWay)
            {
                numberOfCriteriaMatched++;
                output.isSomeNameMatchMade = true;
            }
        }

        isCriteriaMatchedInSomeWay = false;

        if ( String.isNotBlank( this.request.getEmail() )
            && String.isNotBlank( record.PersonEmail ) )
        {
            if (record.PersonEmail.equalsIgnoreCase( mmlib_Utils.clean( this.request.getEmail() ) ))
            {
                output.recordWeightedRank += 10;
                numberOfCriteriaMatched += 2;
                output.isSomeNonNameMatchMade = true;
            }

        }

        if ( String.isNotBlank( this.request.getSSN_Last4() )
            && String.isNotBlank( record.SocialSecurityNumber_Last4__c ) )
        {
            if (record.SocialSecurityNumber_Last4__c.equalsIgnoreCase( mmlib_Utils.clean( this.request.getSSN_Last4() ) ))
            {
                output.recordWeightedRank += 10;
                numberOfCriteriaMatched += 2;
                output.isSomeNonNameMatchMade = true;
            }

        }

        if ( String.isNotBlank( this.request.getPhoneNumber() )
            && ( String.isNotBlank( record.Phone_Unformatted_Formula__c )
                || String.isNotBlank( record.MobilePhone_Unformatted_Formula__pc )
                )
            )
        {
            if (record.Phone_Unformatted_Formula__c != null
                && record.Phone_Unformatted_Formula__c.equalsIgnoreCase( mmlib_Utils.stripPhoneNumber(this.request.getPhoneNumber()) ))
            {
                output.recordWeightedRank += 9;
                numberOfCriteriaMatched += 2;
                output.isSomeNonNameMatchMade = true;
            }
            else if (record.MobilePhone_Unformatted_Formula__pc != null
                    && record.MobilePhone_Unformatted_Formula__pc.equalsIgnoreCase( mmlib_Utils.stripPhoneNumber(this.request.getPhoneNumber()) ))
            {
                output.recordWeightedRank += 9;
                numberOfCriteriaMatched += 2;
                output.isSomeNonNameMatchMade = true;
            }
        }

        if ( this.request.getBirthDate() != null && record.Date_of_Birth_mm__c != null )
        {
            if (this.request.getBirthDate().isSameDay(record.Date_of_Birth_mm__c))
            {
                output.recordWeightedRank += 5;
                numberOfCriteriaMatched += 2;
                output.isSomeNonNameMatchMade = true;
            }
        }

        return output;
    }

    /*
     *  inner class used to relay relveant information about the records weighting.
     */
    public class DerivedWeightingResponse
    {
        public integer recordWeightedRank = 0;
        public boolean isLastNameExactMatch = false;
        public boolean isFirstNameExactMatch = false;
        public boolean isSomeNameMatchMade = false;
        public boolean isSomeNonNameMatchMade = false;

        public boolean isWeightDerivedFromBothNamesAndAnotherFieldMatch()
        {
             return isSomeNameMatchMade && isSomeNonNameMatchMade;
        }
    }

    public class UsageException
        extends Exception
    {
        // No additional code.
    }
}