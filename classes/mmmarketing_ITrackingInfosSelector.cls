/**
 *  mmmarketing_ITrackingInfosSelector
 */
public interface mmmarketing_ITrackingInfosSelector extends mmlib_ISObjectSelector
{
    List<Marketing_Tracking_Info__c> selectById(Set<Id> idSet);
    List<Marketing_Tracking_Info__c> selectByIntake(Set<Id> intakeIdSet);
    List<Marketing_Tracking_Info__c> selectByIdWithMarketingTrackingDetails(Set<Id> idSet);
    List<Marketing_Tracking_Info__c> selectCallOnlyById(Set<Id> idSet);
    List<Marketing_Tracking_Info__c> selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecently();
    List<Marketing_Tracking_Info__c> selectCallOnlyWhereCTMUpdateNotMadeAndCreatedRecentlyForShallowResolution();
    List<Marketing_Tracking_Info__c> selectWhereMarketingCampaignNotLinkedByCampaignValue( Set<String> campaignValueSet );
    List<Marketing_Tracking_Info__c> selectWhereMarketingSourceNotLinkedBySourceValue( Set<String> sourceValueSet );
    Database.QueryLocator selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue( Set<String> sourceValueSet );
    Database.QueryLocator selectQueryLocatorWhereMarketingCampaignNotLinkedByCampaignValue( Set<String> campaignValueSet );
    Database.QueryLocator selectQueryLocatorWhereMarketingFinancialPeriodNotLinkedAndEventRecent();
    Database.QueryLocator selectQueryLocatorWhereMarketingExperienceVariationNotLinkedAndEventRecent();
}