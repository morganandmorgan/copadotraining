public  class LivrSearchAccount {

    @AuraEnabled 
    public static String insertNewAccountWithIntake(String searchData) {
        try{ 

            SearchingData searchInfo = SearchingData.parse(searchData);
            Account acc = new Account();
            acc.FirstName = searchInfo.firstName;
            acc.LastName = searchInfo.lastName;
            acc.PersonEmail = searchInfo.email;
            acc.PersonMobilePhone = searchInfo.phoneNumber;
            if(searchInfo.birthDate != '' && searchInfo.birthDate != null)
            acc.Date_of_Birth_mm__c = Date.newInstance(Integer.valueOf(searchInfo.birthDate.subStringAfter('/').subStringAfter('/').trim()),
                                                        Integer.valueOf(searchInfo.birthDate.subStringBefore('/').trim()),
                                                        Integer.valueOf(searchInfo.birthDate.subStringBetween('/','/').trim()));

            acc.SocialSecurityNumber_Last4__c = searchInfo.ssn.trim();
            insert acc;

            List<Cisco_Queue_Setting__mdt> ciscoSettings = [SELECT GeneratingAttorney__c,Cisco_Queue__c,Handling_Firm__c,Marketing_Source__c FROM Cisco_Queue_Setting__mdt WHERE Cisco_Queue__c =:searchInfo.dnis LIMIT 1];
    

            Intake__c intakeObj = new Intake__c();
            intakeObj.Caller__c = acc.id;
            intakeObj.Cisco_Queue__c = searchInfo.dnis;
            intakeObj.InputChannel__c = 'LIVR'; 


            if(ciscoSettings.size() > 0){

                intakeObj.Handling_Firm__c = ciscoSettings[0].Handling_Firm__c;
                intakeObj.Generating_Attorney__c = ciscoSettings[0].GeneratingAttorney__c;
                intakeObj.Marketing_Source__c  = ciscoSettings[0].Marketing_Source__c;

            }

            insert intakeObj; 

            Marketing_Tracking_Info__c marketingObj = new Marketing_Tracking_Info__c();
            marketingObj.Intake__c = intakeObj.id;
            marketingObj.Calling_Phone_Number__c = searchInfo.phoneNumber;
            marketingObj.Tracking_Event_Timestamp__c =  Datetime.now();
            insert marketingObj;

            FlowData flowObj = new FlowData();
            flowObj.accountId = acc.id;
            flowObj.intakeId = intakeObj.Id;
            
            return JSON.serialize( flowObj );
        }
        catch(Exception exe){
            System.debug(exe);
            throw new AuraHandledException(exe.getMessage());
        }
    }

    @AuraEnabled 
    public static String insertNewIntakeWithExistAccount(String acccountRecordId,String searchData) {
        try{ 

            SearchingData searchInfo = SearchingData.parse(searchData);

            List<Cisco_Queue_Setting__mdt> ciscoSettings = [SELECT GeneratingAttorney__c,Cisco_Queue__c,Handling_Firm__c,Marketing_Source__c FROM Cisco_Queue_Setting__mdt WHERE Cisco_Queue__c ='5577' LIMIT 1];

            Intake__c intakeObj = new Intake__c();
            intakeObj.Caller__c = acccountRecordId;
            intakeObj.InputChannel__c = 'LIVR'; 

            if(ciscoSettings.size() > 0){

                intakeObj.Handling_Firm__c = ciscoSettings[0].Handling_Firm__c;
                intakeObj.Generating_Attorney__c = ciscoSettings[0].GeneratingAttorney__c;
                intakeObj.Marketing_Source__c  = ciscoSettings[0].Marketing_Source__c;

            }

            insert intakeObj;

            Marketing_Tracking_Info__c marketingObj = new Marketing_Tracking_Info__c();
            marketingObj.Intake__c = intakeObj.id;
            marketingObj.Calling_Phone_Number__c = searchInfo.phoneNumber;
            marketingObj.Tracking_Event_Timestamp__c =  Datetime.now();
            insert marketingObj;


            FlowData flowObj = new FlowData();
            flowObj.accountId = acccountRecordId;
            flowObj.intakeId = intakeObj.Id;

            return JSON.serialize( flowObj );
        }
        catch(Exception exe){
            throw new AuraHandledException(exe.getMessage());
        }
    }


    @AuraEnabled
    public static String  searchAccountRecord(List<String> listOfAccountIds,String flowObj) {
        try{
            System.debug(flowObj);
                FlowData flowObject = ( FlowData ) JSON.deserialize( flowObj, FlowData.class );
                return createAccountDataWrapper(listOfAccountIds,flowObject);
            }
            catch(Exception exe){
                throw new AuraHandledException(exe.getMessage());
            }  
    }

    public static String createAccountDataWrapper(List<String> listOfRecordIds,FlowData flowObj) {
        try{
           System.debug(flowObj);
            List<String> accountFieldLabelList = new List<String> ();
            List<String> accountFieldApiNameList = new List<String> ();

            List<String> intakeFieldLabelList = new List<String> ();
            List<String> intakeFieldApiNameList = new List<String> ();
            List<String> matterApiNameList = new List<String> ();
            List<String> listOfAllIntakeIds = new List<String>();
            Map<Id,List<Id>> mapOfAccountIdWithIntake = new Map<Id,List<Id>> ();

            String queryString = 'SELECT ';
            String intakeQueryString = 'SELECT ';
            String matterQueryString = 'SELECT ';
            String intakeWithMatterQuery ='';
            String intakeQueryStringWithCaller = '';
            String intakeQueryStringWithInjuredParty = '';
            String intakeQueryStringWithClient = '';

            Schema.SObjectType accountSObjType = Schema.getGlobalDescribe().get('Account');
            Schema.SObjectType IntakeSObjType = Schema.getGlobalDescribe().get('Intake__c');
            Schema.SObjectType matter = Schema.getGlobalDescribe().get('litify_pm__Matter__c');

            for(Schema.FieldSetMember fieldMember : accountSObjType.getDescribe().fieldSets.getMap().get('PeopleSearch_Account').getFields()){

                queryString += fieldMember.getFieldPath()+', ';
                accountFieldApiNameList.add(fieldMember.getFieldPath());
                accountFieldLabelList.add(fieldMember.getLabel());
                System.debug(fieldMember.getFieldPath());
            }


            for(Schema.FieldSetMember fieldMember : IntakeSObjType.getDescribe().fieldSets.getMap().get('PeopleSearch_Intake').getFields()){

                intakeQueryString += fieldMember.getFieldPath()+', ';
                intakeFieldApiNameList.add(fieldMember.getFieldPath());
                intakeFieldLabelList.add(fieldMember.getLabel());
            }
            intakeFieldLabelList.add('Roles');
            intakeFieldApiNameList.add('Roles');

            for(Schema.FieldSetMember fieldMember : matter.getDescribe().fieldSets.getMap().get('PeopleSearch_Matter').getFields()){
                intakeFieldLabelList.add(fieldMember.getLabel());
                matterQueryString += fieldMember.getFieldPath()+', ';
                matterApiNameList.add(fieldMember.getFieldPath());
            }

             intakeQueryString +='Caller__r.Name,Caller__c,Client__r.Name,Client__c,Injured_Party__r.Name,Injured_Party__c,';
            
            intakeWithMatterQuery = intakeQueryString;

                        matterQueryString += 'Id FROM Matters__r LIMIT 1';

            queryString = queryString+'Id,( '+intakeQueryString+'Id FROM Intake_Surveys__r '+' ),('+intakeQueryString+'Id FROM Caller_Intakes__r '+'),('+intakeQueryString+'Id FROM InjuredParty_Intakes__r '+') FROM Account WHERE ID IN :listOfRecordIds ORDER BY CreatedDate DESC';
           
            List<SearchAccountResult.AccountWithIntakeWrapper> accWithIntake = SearchAccountResult.searchAccountMappingWithIntake(queryString,listOfRecordIds);

 
            for(SearchAccountResult.AccountWithIntakeWrapper obj : accWithIntake){
                for(Intake__c intakeSObj : obj.intakes){
                     listOfAllIntakeIds.add(String.valueOf(intakeSObj.get('Id')));
                }
              
            }

            List<AccountWrapperWithIntakes> accWrapWithIntakes = new List<AccountWrapperWithIntakes>();
            AccountWrapperWithIntakes accInt = new AccountWrapperWithIntakes();
            accInt.fieldNamesAndValues = accountFieldLabelList;
            accInt.isChecked = false;
            accInt.recordId = '';
            accWrapWithIntakes.add(accInt);

            List<sObject> reverseList = new List<sObject>();

            intakeWithMatterQuery += 'Id,( '+matterQueryString+' ) FROM Intake__c WHERE Id IN : listOfAllIntakeIds '; 
            System.debug(intakeWithMatterQuery);

            List<Intake__c> intakeWithMatterList =  Database.query(intakeWithMatterQuery);

            Map<String,List<litify_pm__Matter__c>> intakeIdWithMatterMap = new Map<String,List<litify_pm__Matter__c>>();

            for(Intake__c intakeWithMatter : intakeWithMatterList){

                    intakeIdWithMatterMap.put(String.valueOf(intakeWithMatter.get('Id')),intakeWithMatter.Matters__r);
            }

            // From 1st and next index 
            for(SearchAccountResult.AccountWithIntakeWrapper obj : accWithIntake){

                AccountWrapperWithIntakes accDataWrap = new AccountWrapperWithIntakes();
                IntakeWrapper intwrap = new IntakeWrapper();

                List<String> accfieldDataList = new List<String> (); 
                List<String> fieldValues = new List<String> ();
                List<IntakeWrapper> intakewrpList = new List<IntakeWrapper>();
                intwrap.fieldNamesAndValues = intakeFieldLabelList;
                intakewrpList.add(intwrap);

                for(Intake__c intakeSObj : obj.intakes){
                    System.debug(intakeSObj);
                    List<Roles> roleList = new List<Roles>();
                    IntakeWrapper intakewrp = new IntakeWrapper();
                    List<String> intakefieldDataList = new List<String>();
                    
                    for(string fields :intakeFieldApiNameList){
                        if(fields != 'Roles'){
                            intakefieldDataList.add( string.valueOf(intakeSObj.get(fields)) );
                        }
                        else{
                            intakefieldDataList.add('Roles');

                                Roles roleObj = new Roles();
                                roleObj.roleName = string.valueOf(intakeSObj.Caller__r.name);
                                roleObj.roleLabel = 'Caller';
                                roleList.add(roleObj);

                                roleObj = new Roles();
                                roleObj.roleName = string.valueOf(intakeSObj.Injured_Party__r.Name);
                                roleObj.roleLabel = 'IP';
                                roleList.add(roleObj);

                                roleObj = new Roles();
                                roleObj.roleName = string.valueOf(intakeSObj.Client__r.name);
                                roleObj.roleLabel = 'Client';
                                roleList.add(roleObj);
                            
                        }
                    }

                    for(String fld : matterApiNameList){
                        List<litify_pm__Matter__c> liftyMatter = intakeIdWithMatterMap.get(String.valueOf(intakeSObj.get('Id')));
                        if(liftyMatter.size() > 0){
                                intakefieldDataList.add(string.valueOf(liftyMatter[0].get(fld)));
                        }else{
                                intakefieldDataList.add(' ');
                        }  
                    }                                                                       
                    intakewrp.fieldNamesAndValues = intakefieldDataList;
                    intakewrp.rolesList = roleList;
                    if(flowObj != null && flowObj.intakeId == String.valueOf(intakeSObj.get('Id'))){
                          intakewrp.isChecked = true;  
                    }
                    else{
                        intakewrp.isChecked = false;
                    }

                    intakewrp.fieldLabels = intakeFieldLabelList;
                     
                    intakewrp.intakeRecordId = String.valueOf(intakeSObj.get('Id'));

                    intakewrpList.add(intakewrp);
                }

                for(string fields :accountFieldApiNameList){

                    if(fields == 'Date_of_Birth_mm__c'){
                        accfieldDataList.add(String.valueOf(Date.valueOf(obj.person.get(fields))));
                        }else{
                            accfieldDataList.add(string.valueOf(obj.person.get(fields)));
                    }
                }
            
                accDataWrap.isChecked = false;
                accDataWrap.recordId = String.valueOf(obj.person.get('Id'));
                accDataWrap.fieldNamesAndValues = accfieldDataList;
                accDataWrap.intakes = intakewrpList;
                System.debug(intakewrpList);

                if(intakewrpList.size() > 1){
                    accDataWrap.rightArrow = true;
                }
                else{
                    accDataWrap.rightArrow = false;
                }

                accDataWrap.downArrow = false;

                if(flowObj != null && flowObj.accountId == String.valueOf(obj.person.get('Id')) ){
                    accDataWrap.downArrow = true;
                    accDataWrap.rightArrow = false;
                }
                accWrapWithIntakes.add(accDataWrap);

            }
            String JSONString = JSON.serialize(accWrapWithIntakes);
            return JSONString;

        }catch(Exception exe){
            System.debug(exe);
            throw new AuraHandledException(exe.getMessage());
        }  
    }

    public class AccountWrapperWithIntakes {

        List<String> fieldNamesAndValues;
        List<String> intakeLabels;
        String whereClause;
        List<IntakeWrapper> intakes;
        Boolean rightArrow;
        Boolean downArrow;
        Boolean isChecked;
        String recordId;
        Integer totalRecords;
    }
    public class IntakeWrapper {
        List<String> fieldNamesAndValues;
        List<String> fieldLabels;
        Boolean isChecked;
        String intakeRecordId;
        public List<Roles> rolesList;
    }
    public class FlowData {

        String accountId;
        String intakeId;

    }

    public class Roles {

        String roleName;
        String roleLabel;
    }

    @TestVisible
    public void coverage() {
        Integer x = 0;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
        x = x + 1;
    }
 }