/**
 * ContactSelectorTest
 * @description Test for Contact Selector class.
 * @author Matt Terrill
 * @date 8/16/2019
 */
@IsTest
public with sharing class ContactSelectorTest {

    @TestSetup
    private static void setup() {
        Account account = TestUtil.createAccount('Test Account', 'Business_Account');
        insert account;

        Contact testContact = TestUtil.createContact(account.Id);
        insert testContact;
    }

    @IsTest
    private static void ctor() {
        ContactSelector contactSelector = new ContactSelector();
        System.assert(contactSelector != null);
    }

    @IsTest
    private static void addSObjectFields() {
        ContactSelector contactSelector = new ContactSelector();

        //get the default fields
        List<Schema.SObjectField> before = contactSelector.getSObjectFieldList().clone();

        //add one, and obscure one
        List<Schema.SObjectField> newFields = new List<Schema.SObjectField>{
                Contact.SystemModStamp
        };
        contactSelector.addSObjectFields(newFields);

        //get the list again
        List<Schema.SObjectField> after = contactSelector.getSObjectFieldList();

        //make sure the list has grown by one
        System.assertEquals(before.size() + 1, after.size());
    }

    @IsTest
    private static void selectByIds() {
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        ContactSelector contactSelector = new ContactSelector();
        List<Contact> contacts = contactSelector.selectById(new Set<Id>{
                testContact.Id
        });
        System.assert(contacts != null);
        System.assertEquals(1, contacts.size());
    }
}