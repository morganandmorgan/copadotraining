public class renderDetailController {

    public String sObjectName {get;set;}

    @TestVisible private String contentString;

    public String getPrintableView() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        sObjectName = id.getSObjectType().getDescribe().getLabel();
        String html = getContentString(id);
        // system.debug(html);
        html = html.substringAfter('</head>').substringBefore('</html>');
        String title = html.substringAfter('<h1>').substringBefore('</h1>');
        html = html.substringBefore('<h1>') + '<h1>' + sObjectName + ' - ' + title + html.substringAfter(title);
        return html;
    }

    private String getContentString(Id id) {
        PageReference pr = new PageReference('/' + id + '/p');
        contentString = Test.isRunningTest() ? contentString : pr.getContent().toString();
        return contentString;
    }
}