public with sharing class CreateIntakeFromTaskTriggerHandler {
  Map<String, String> hubspotFields = new Map<String,String>();
  Id IntakeRecordType;
    public CreateIntakeFromTaskTriggerHandler() {
    // initialize hubspot field map
    for(Hubspot_Field_Mapping__c entry : [SELECT hubspot_field_label__c, intake_field_api_name__c FROM Hubspot_Field_Mapping__c]) {
        hubspotFields.put(entry.hubspot_field_label__c, entry.intake_field_api_name__c);
    }

    IntakeRecordType = RecordTypeUtil.getRecordTypeIDByDevName('Intake__c', 'PlaceHolder');
    }

  private Map<String, Schema.SoapType> allFieldTypesBacking;
  private Map<String, Schema.SoapType> allFieldTypes {
    get {
      if (allFieldTypesBacking == null) {
        Schema.DescribeSObjectResult objDescription = Intake__c.sObjectType.getDescribe();
        allFieldTypesBacking = new Map<String, Schema.SoapType>();
        for (Schema.SobjectField f : objDescription.fields.getMap().Values()) {
          Schema.DescribeFieldResult dfr = f.getDescribe();
          allFieldTypesBacking.put(dfr.getName().toLowerCase(), dfr.getSoapType());
        }
      }
      return allFieldTypesBacking;
    }
  }

  public void Execute(List<sObject> ts) {
    List<Intake__c> newIntakes = new List<Intake__c>();
    List<Task> tasksToUpdate = new List<Task>();

    for (SObject so : ts) {
      Task t = (Task)so;

      if (t.Intake_Created_From_Task__c) {
        // base recursion case
        continue;
      }

      if (!t.Subject.startsWithIgnoreCase('Submitted Form')) {
        // not a hubspot entry, super HACK, can we uses user instead?
        continue;
      }
      if (t.AccountId == null) {
        continue;
      }

      Intake__c i = new Intake__c(RecordTypeId = IntakeRecordType, Client__c = t.AccountId, Caller__c = t.AccountId
                                  , Injured_Party__c = t.AccountId, Marketing_Source__c = t.Account.PersonLeadSource
                                  , Marketing_Sub_Source__c = t.Account.PersonLeadSubSource__c, Status__c = 'Lead'
                                 ); // , CreatedByWebform__c = true
      newIntakes.add(i);

      AppendCustomFieldMapping(t, i);

      // if this is an update we can't change the task so we use a new one with the same id
      tasksToUpdate.add(new Task(Id = t.Id, Intake_Created_From_Task__c = true));
    }

    insert newIntakes;
    update tasksToUpdate;
  }

  private void AppendCustomFieldMapping(Task t, Intake__c i) {
    Map<String,String> customFields = getTaskToIntakeMap(t);
    for (String fieldName : customFields.keySet()) {
      String fieldValue = customFields.get(fieldName);
      // HACK copied from previous trigger-based code, not only does this look inverted to me but I also don't like the magic string
      Schema.SoapType typ = allFieldTypes.get(fieldName.toLowerCase());
      Object objValue = ConvertValToType(typ, fieldValue);
      i.put(fieldName, objValue);
    }
  }

  private Map<String,String> getTaskToIntakeMap(Task t) {
    Map<String,String> result = new Map<String,String>();

    List<String> taskDescriptionSplit = t.Description.split('\n');
    system.debug(taskDescriptionSplit);

    for (String line : taskDescriptionSplit) {
      String field = line.substringBefore(': ');
      if(hubspotFields.containsKey(field)) {
        String val = line.substringAfter(': ');
        result.put(hubspotFields.get(field), val);
      }
    }

    system.debug('result: '+result);
    return result;
  }

  private Object ConvertValToType(Schema.SoapType st, String fieldValue) {
    if (st == Schema.SoapType.Boolean) {
      fieldValue = fieldValue.toLowerCase();
      if (fieldValue == 'true' || fieldValue == 't' || fieldValue == 'y' || fieldValue == '1') {
        return true;
      } else {
        return false;
      }
    }
    if (st == Schema.SoapType.Date) {
      return Date.valueOf(fieldValue);
    }
    if (st == Schema.SoapType.DateTime) {
      return DateTime.valueOf(fieldValue);
    }
    if (st == Schema.SoapType.Double) {
      return Double.valueOf(fieldValue);
    }
    if (st == Schema.SoapType.Integer) {
      return Integer.valueOf(fieldValue);
    }

    return fieldValue;
  }
}