/**
 * IntakeDomainTest
 * @description Test for Intake Domain class.
 * @author Jeff Watson
 * @date 8/29/2019
 */
@IsTest
public with sharing class IntakeDomainTest {

    @IsTest
    private static void intakeTrigger() {

        Flow_Case_Type__c ct = new Flow_Case_Type__c();
        ct.name = 'Automobile Accident';
        ct.catastrophic__c = true;
        insert ct;

        Flow_Injury_Type__c it = new Flow_Injury_Type__c();
        it.name = 'Amputation Other';
        it.catastrophic__c = false;
        it.ultra_catastrophic__c = true;
        insert it;

        Flow_Treatment_Type__c treatment = new Flow_Treatment_Type__c();
        treatment.name = 'Major/Lifesaving surgery performed';
        treatment.catastrophic__c = true;
        insert treatment;

        Intake__c intake = TestUtil.createIntake();
        insert intake;

        intake.Staff_Assigned_Date_Time__c = DateTime.now();

        intake.case_type__c = 'Automobile Accident';
        intake.type_of_injury__c = 'Amputation/Loss of Limb';
        intake.treatment_type__c = 'Major/Lifesaving surgery performed';
        intake.venue__c = 'FL - Orlando';
        intake.handling_firm__c = 'Morgan & Morgan';
        update intake;

        delete intake;

    } //intakeTrigger

    @IsTest
    private static void ctor() {
        Test.startTest();
        IntakeDomain intakeDomain = new IntakeDomain();
        System.assert(intakeDomain != null);
        Test.stopTest();
    }


    @IsTest
    private static void methodCoverage() {
        Account c = TestUtil.createPersonAccount('test','caller');
        insert c;

        Account ip = TestUtil.createPersonAccount('injured','party');
        insert ip;

        Account oa = TestUtil.createAccount('office account');
        oa.type = 'MM Office';
        insert oa;

        Contact attn = TestUtil.createAssignableAttorney();
        insert attn;

	    insert new Docusign_Template_Manager__c(Name='englishTest'
            ,Case_Type__c = 'Automobile Accident'
            //,Class_Action_Type__c
            //,Contract_Venue__c = 'FL - Orlando'
            ,County__c = 'Orange'
            ,Docusign_ID__c = 'english1'
            ,Docusign_Template_ID_Invst__c = 'english'
            ,Handling_Firm__c = 'Morgan & Morgan'
            ,Language__c = 'English'
            ,Marketing_Source__c = 'Bus'
            //,State_Abbreviation__c = 'FL'
            ,Venue__c = 'FL - Orlando'
            //,Where_did_the_injury_occur__c
        );

        //this will require 'scoring'
	    insert new Docusign_Template_Manager__c(Name='spanishTest'
            ,Case_Type__c = 'Automobile Accident'
            //,Class_Action_Type__c
            //,Contract_Venue__c = 'FL - Orlando'
            ,County__c = 'Orange'
            ,Docusign_ID__c = 'spanish1'
            ,Docusign_Template_ID_Invst__c = 'spanish'
            ,Handling_Firm__c = 'Morgan & Morgan'
            ,Language__c = 'Spanish'
            ,Marketing_Source__c = 'Bus'
            //,State_Abbreviation__c = 'FL'
            ,Venue__c = 'FL - Orlando'
            //,Where_did_the_injury_occur__c
        );

        Intake__c intake = TestUtil.createIntake();

        intake.caller_is_injured_party__c = 'No';
        intake.caller__c = c.id;
        intake.injured_party_relationship_to_caller__c = 'Spouse';
        intake.Injured_Party__c = ip.id;

        insert intake;

        IntakeDomain iDomain = new IntakeDomain(new List<Intake__c>{intake});

        iDomain.validateForInitialIntakeCall();

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {Intake__c.getSObjectType()});
        Map<Id, List<Task>> intakeIdToTasksMap = new Map<Id, List<Task>>();
        intakeIdToTasksMap.put(intake.id, new List<Task>{TestUtil.createTask()});
        iDomain.updateOutboundDialCounts(intakeIdToTasksMap, uow);

        intake.retainer_received__c = DateTime.now();
        intake.handling_firm__c = 'Morgan & Morgan';
        intake.assigned_office__c = oa.id;
        intake.state_of_incident__c = 'FL';
        intake.venue__c = 'FL - Orlando';
        intake.contract_venue__c = 'FL - Orlando';
        intake.case_type__c = 'Automobile Accident';
        intake.assigned_attorney__c = attn.id;
        intake.marketing_source__c = 'Bus';
        update intake;

        Boolean suppressTriggeredSend = IntakeDomain.suppressTriggeredSend();

    } //methodCoverage


    @IsTest
    static void assignTest()
    {
        List<Id> userIdList = new List<Id>();
        for (Integer i = 0; i < 6; i++)
        {
            userIdList.add(fflib_IDGenerator.generate(User.SObjectType));
        }

        Intake__c intake = new Intake__c();

        Approval_Assignment__c approvalAssignment = new Approval_Assignment__c();
        approvalAssignment.Approving_Attorney_1__c = userIdList.get(0);
        approvalAssignment.Approving_Attorney_2__c = userIdList.get(1);
        approvalAssignment.Approving_Attorney_3__c = userIdList.get(2);
        approvalAssignment.Approving_Attorney_4__c = userIdList.get(3);
        approvalAssignment.Approving_Attorney_5__c = userIdList.get(4);
        approvalAssignment.Approving_Attorney_6__c = userIdList.get(5);

        new IntakeDomain().assign(intake, approvalAssignment);

        System.assertEquals(intake.Approving_Attorney_1__c, approvalAssignment.Approving_Attorney_1__c);
        System.assertEquals(intake.Approving_Attorney_2__c, approvalAssignment.Approving_Attorney_2__c);
        System.assertEquals(intake.Approving_Attorney_3__c, approvalAssignment.Approving_Attorney_3__c);
        System.assertEquals(intake.Approving_Attorney_4__c, approvalAssignment.Approving_Attorney_4__c);
        System.assertEquals(intake.Approving_Attorney_5__c, approvalAssignment.Approving_Attorney_5__c);
        System.assertEquals(intake.Approving_Attorney_6__c, approvalAssignment.Approving_Attorney_6__c);
    }

    @IsTest
    static void findBestMatchTest_OneMatch()
    {
        List<String> stringList = new List<String>();
        stringList.add('Bickering');
        stringList.add('Over there');
        stringList.add('No');
        stringList.add('ThisHereUn');
        stringList.add('Sassafrass');
        stringList.add('StateOfConfusion');

        Intake__c intake = new Intake__c();
        intake.Type_of_Dispute__c = stringList.get(0);
        intake.Where_did_the_injury_occur__c = stringList.get(1);
        intake.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        intake.What_type_of_Class_Action_case_is_it__c = stringList.get(3);
        intake.County_of_incident__c = stringList.get(4);
        intake.State_of_incident__c = stringList.get(5);

        List<Approval_Assignment__c> aaList = new List<Approval_Assignment__c>();

        Approval_Assignment__c assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = 'Arguing';
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = null;
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = 'Yes';
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        // *******************
        // ****** Match ******
        // *******************
        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = null;
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = null;
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = null;
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        Approval_Assignment__c aa = new IntakeDomain().findBestMatch(intake, aaList);

        System.assertEquals(aaList.get(2).Id, aa.Id);
    }

    @IsTest
    static void findBestMatchTest_EarlierInList()
    {
        List<String> stringList = new List<String>();
        stringList.add('Bickering');
        stringList.add('Over there');
        stringList.add('No');
        stringList.add('ThisHereUn');
        stringList.add('Sassafrass');
        stringList.add('StateOfConfusion');

        Intake__c intake = new Intake__c();
        intake.Type_of_Dispute__c = stringList.get(0);
        intake.Where_did_the_injury_occur__c = stringList.get(1);
        intake.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        intake.What_type_of_Class_Action_case_is_it__c = stringList.get(3);
        intake.County_of_incident__c = stringList.get(4);
        intake.State_of_incident__c = stringList.get(5);

        List<Approval_Assignment__c> aaList = new List<Approval_Assignment__c>();

        Approval_Assignment__c assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = 'Arguing';
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = null;
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        // *******************
        // ****** Match ******
        // *******************
        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = 'Yes';
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        // *******************
        // ****** Match ******
        // *******************
        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = null;
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = null;
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = null;
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        Approval_Assignment__c aa = new IntakeDomain().findBestMatch(intake, aaList);

        System.assertEquals(aaList.get(1).Id, aa.Id);
    }

    @IsTest
    static void findBestMatchTest_NoMatch()
    {
        List<String> stringList = new List<String>();
        stringList.add('Bickering');
        stringList.add('Over there');
        stringList.add('No');
        stringList.add('ThisHereUn');
        stringList.add('Sassafrass');
        stringList.add('StateOfConfusion');

        Intake__c intake = new Intake__c();
        intake.Type_of_Dispute__c = stringList.get(0);
        intake.Where_did_the_injury_occur__c = stringList.get(1);
        intake.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        intake.What_type_of_Class_Action_case_is_it__c = stringList.get(3);
        intake.County_of_incident__c = stringList.get(4);
        intake.State_of_incident__c = stringList.get(5);

        List<Approval_Assignment__c> aaList = new List<Approval_Assignment__c>();

        Approval_Assignment__c assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = 'Arguing';
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = null;
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = 'backyard';
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = 'grape';
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(0);
        assignmentRecord.ClassActionType__c = 'Some other type';
        assignmentRecord.County__c = stringList.get(0);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        Approval_Assignment__c aa = new IntakeDomain().findBestMatch(intake, aaList);

        System.assert(aa == null, 'A match should not have occurred.');
    }

    @IsTest
    static void run_SameAsOneMatch()
    {

        List<String> stringList = new List<String>();
        stringList.add('Bickering');
        stringList.add('Over there');
        stringList.add('No');
        stringList.add('ThisHereUn');
        stringList.add('Sassafrass');
        stringList.add('StateOfConfusion');

        Intake__c intake = new Intake__c();
        intake.Case_Type__c = 'Litigation';
        intake.Venue__c = 'Florida';
        intake.Handling_Firm__c = 'M&M';
        intake.Type_of_Dispute__c = stringList.get(0);
        intake.Where_did_the_injury_occur__c = stringList.get(1);
        intake.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        intake.What_type_of_Class_Action_case_is_it__c = stringList.get(3);
        intake.County_of_incident__c = stringList.get(4);
        intake.State_of_incident__c = stringList.get(5);

        List<Approval_Assignment__c> aaList = new List<Approval_Assignment__c>();

        Approval_Assignment__c assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Case_Type__c = intake.Case_Type__c;
        assignmentRecord.Venue__c = intake.Venue__c;
        assignmentRecord.Handling_Firm__c = intake.Handling_Firm__c;
        assignmentRecord.Type_of_Dispute__c = 'Arguing';
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = null;
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Case_Type__c = intake.Case_Type__c;
        assignmentRecord.Venue__c = intake.Venue__c;
        assignmentRecord.Handling_Firm__c = intake.Handling_Firm__c;
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = 'Yes';
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        // *******************
        // ****** Match ******
        // *******************
        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Case_Type__c = intake.Case_Type__c;
        assignmentRecord.Venue__c = intake.Venue__c;
        assignmentRecord.Handling_Firm__c = intake.Handling_Firm__c;
        assignmentRecord.Type_of_Dispute__c = stringList.get(0);
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = stringList.get(2);
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = stringList.get(4);
        assignmentRecord.Approving_Attorney_1__c = fflib_IDGenerator.generate(User.SObjectType);
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Case_Type__c = intake.Case_Type__c;
        assignmentRecord.Venue__c = intake.Venue__c;
        assignmentRecord.Handling_Firm__c = intake.Handling_Firm__c;
        assignmentRecord.Type_of_Dispute__c = null;
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = null;
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = null;
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        assignmentRecord = new Approval_Assignment__c();
        assignmentRecord.Id = fflib_IDGenerator.generate(Approval_Assignment__c.SObjectType);
        assignmentRecord.Case_Type__c = intake.Case_Type__c;
        assignmentRecord.Venue__c = 'Georgia';
        assignmentRecord.Handling_Firm__c = intake.Handling_Firm__c;
        assignmentRecord.Type_of_Dispute__c = null;
        assignmentRecord.Where_did_the_injury_occur__c = stringList.get(1);
        assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c = null;
        assignmentRecord.ClassActionType__c = stringList.get(3);
        assignmentRecord.County__c = null;
        assignmentRecord.State_of_incident__c = stringList.get(5);
        aaList.add(assignmentRecord);

        IntakeDomain.mock_ApprovalAssignmentList = aaList;

        new IntakeDomain(new List<Intake__c> {intake}).setApprovingAttorneys(null);

        System.assertEquals(aaList.get(2).Approving_Attorney_1__c, intake.Approving_Attorney_1__c);
    }

    @IsTest
    private static void testCoverage() {
        //IntakeDomain.coverage();
        LeadAutoConvert_TrigAct.coverage();
        //mmintake_IntakesSelector.coverage();
        //mmintake_IntakesRestApi.coverage();
        //mmintake_IntakesRestApi.coverage2();
        mmintake_IntakesServiceExceptions.coverage();
        mmintake_TreatmentCenterSelExtension.coverage();
        IntakeService.coverage();
        IntakeSolService.coverage();
    }
}