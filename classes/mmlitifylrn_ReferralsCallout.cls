/**
 *  Callout for the Litify Referral Network "referrals" API
 *
 *  @see http://litify-swagger.bitballoon.com/#!/referrals/createReferral
 *
 *  @usage mmlitifylrn_ReferralsCallout.Response resp = (mmlitifylrn_ReferralsCallout.Response) new mmlitifylrn_ReferralsCallout()
 *                                   .setReferral( litify_pm__Referral__c )
 *                                   .setHandlingOrganization( litify_pm__Firm__c )
 *                                   .setSuggestedReferralAgreement( mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement )
 *                                   .setAuthorization( mmlitifylrn_CalloutAuthorization )
 *                                   .debug()
 *                                   .execute()
 *                                   .getResponse();
 *
 *  @usage system.debug( resp.getFooBar() );
 */
public class mmlitifylrn_ReferralsCallout
    extends mmlitifylrn_BasePOSTV1Callout
{
    private mmlitifylrn_ReferralsCallout.Response resp = new mmlitifylrn_ReferralsCallout.Response();

    private litify_pm__Referral__c referral = null;

    private litify_pm__Firm__c handlingFirm = null;

    private mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement suggestedReferralAgreement = null;

    public override map<string, string> getParamMap()
    {
        return new map<string, string>();
    }

    public override string getPathSuffix()
    {
        return '/referrals';
    }

    public mmlitifylrn_LRNModels.ReferralBasicData getRequestBody()
    {
        return new mmlitifylrn_LRNModels.ReferralBasicData( this.referral, this.handlingFirm, this.suggestedReferralAgreement );
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        addPostRequestBody( getRequestBody() );

        system.debug( 'RequestBody______________________________________________________');
        system.debug( JSON.serializePretty( getRequestBody(), true ));
        system.debug( 'End of RequestBody______________________________________________________');

        httpResponse httpResp = super.executeCallout();

        if ( this.isDebugOn )
        {
            system.debug( httpResp.getStatusCode() );
            system.debug( httpResp.getStatus() );
        }

        string jsonResponseBody = httpResp.getBody();

        if ( this.isDebugOn )
        {
            system.debug( json.deserializeUntyped( jsonResponseBody ) );
        }

        if ( httpResp.getStatusCode() >= 200
            && httpResp.getStatusCode() < 300)
        {
            resp = (mmlitifylrn_ReferralsCallout.Response) json.deserialize( '{"referral":' + httpResp.getBody() + '}', mmlitifylrn_ReferralsCallout.Response.class );
        }
        else
        {
            throw new mmlitifylrn_Exceptions.CalloutException( httpResp );
        }

        return this;
    }

    public class Response
        implements mmlib_BaseCallout.CalloutResponse
    {
        public mmlitifylrn_LRNModels.ReferralBasicData referral;

        public integer getTotalNumberOfRecordsFound()
        {
            return referral == null ? 0 : 1;
        }
    }

    private void validateCallout()
    {
        if ( this.referral == null )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The referral record has not been specified.  Please use the setReferral(litify_pm__Referral__c) method before calling the execute() method.');
        }

        if ( this.handlingFirm == null )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The handling organization record has not been specified.  Please use the setHandlingOrganization(litify_pm__Firm__c) method before calling the execute() method.');
        }

        if ( this.suggestedReferralAgreement == null )
        {
            throw new mmlitifylrn_Exceptions.ParameterException('The Litify Referral Network Suggested Referral Agreement has not been specified.  Please use the setSuggestedReferralAgreement(mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement) method before calling the execute() method.');
        }
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public mmlitifylrn_ReferralsCallout setHandlingOrganization( litify_pm__Firm__c handlingOrganization )
    {
        this.handlingFirm = handlingOrganization;

        return this;
    }

    public mmlitifylrn_ReferralsCallout setReferral( litify_pm__Referral__c referral )
    {
        this.referral = referral;

        return this;
    }

    public mmlitifylrn_ReferralsCallout setSuggestedReferralAgreement( mmlitifylrn_LRNModels.FirmToFirmRelationshipReferralAgreement suggestedReferralAgreement )
    {
        this.suggestedReferralAgreement = suggestedReferralAgreement;

        return this;
    }

}