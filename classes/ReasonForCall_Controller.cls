public class ReasonForCall_Controller {
    @AuraEnabled 
    public static String returnRecords( String questionModel ) {
        try {
            QuestionAnswerClass dataToReturn = new QuestionAnswerClass();
            List<QuestionMetaData> questionsData = new List<QuestionMetaData>();
            Wizard__mdt questionnaireModelWizard = [SELECT Id, SerializedData__c FROM Wizard__mdt WHERE DeveloperName = :questionModel LIMIT 1];
            mmwiz_SerializedData questionnaireModelWizardData = ( mmwiz_SerializedData ) JSON.deserialize( questionnaireModelWizard.SerializedData__c, mmwiz_SerializedData.class );
            dataToReturn.questionnaireTitle = questionnaireModelWizardData.title;
            List<String> pageModelNames = questionnaireModelWizardData.childTokens;

            List<Wizard__mdt> pageModelWizardList = [SELECT Id, SerializedData__c FROM Wizard__mdt WHERE DeveloperName IN :pageModelNames];
            List<String> questionModelList = new List<String>();
            mmwiz_SerializedData pageModelWizardData;

            for( Wizard__mdt pageWizard : pageModelWizardList ) {
                pageModelWizardData = ( mmwiz_SerializedData ) JSON.deserialize( pageWizard.SerializedData__c, mmwiz_SerializedData.class );
                questionModelList.addAll( pageModelWizardData.childTokens );
            }

            List<Wizard__mdt> questionModelWizardList = [SELECT Id, SerializedData__c, DisplayText__c FROM Wizard__mdt WHERE DeveloperName IN :questionModelList];
            mmwiz_SerializedData questionModelWizardData;
            List<String> answerModelList = new List<String>();
            for( Wizard__mdt questionWizard : questionModelWizardList ) {
                dataToReturn.questionTitle = questionWizard.DisplayText__c;
                questionModelWizardData = ( mmwiz_SerializedData ) JSON.deserialize( questionWizard.SerializedData__c, mmwiz_SerializedData.class );
                answerModelList.addAll( questionModelWizardData.childTokens );
            }

            List<Wizard__mdt> answerModelWizardList = [SELECT Id,MasterLabel,DeveloperName, DisplayText__c, SerializedData__c FROM Wizard__mdt WHERE DeveloperName IN :answerModelList];
            for( Wizard__mdt records : answerModelWizardList ){
                QuestionMetaData question = new QuestionMetaData();
                question.label = records.MasterLabel;
                question.apiName = records.DeveloperName;
                question.title = dataToReturn.questionTitle;
                question.displayText = records.DisplayText__c;
                question.IdOfRecord = records.Id;

                if(records.SerializedData__c != '' && records.SerializedData__c  != null){
              
                    question.serializeContent = SerailizeData.parse( records.SerializedData__c);
                }
                questionsData.add(question);
            }

            //Return string of wrapper
            return JSON.serialize( questionsData );
        }
        catch( Exception ex ) {
            throw new AuraHandledException( ex.getMessage() );
        }
    }

    @AuraEnabled 
    public static QuestionMetaData responseActions(String actionToken) {
        try{ 
            System.debug(actionToken);
            QuestionMetaData quetionsdata = new QuestionMetaData();
            List<Wizard__mdt> wizardData = [ SELECT MasterLabel,DisplayText__c  FROM Wizard__mdt WHERE IsActive__c = true AND DeveloperName =:actionToken LIMIT 1];
            
            String displayText = '';
            if(wizardData.size() > 0){
                quetionsdata.idOfRecord = wizardData[0].Id;
                quetionsdata.displayText = wizardData[0].DisplayText__c;
            }

            System.debug(quetionsdata);

            //return JSON.serialize( displayText );
            return quetionsdata;
        }
        catch(Exception exe){
            throw new AuraHandledException(exe.getMessage());
        }
    }

    public class mmwiz_SerializedData {
        public List<String> childTokens;
        public List<String> actionTokens;
        public List<String> tags;
        public String title;
        public String questionType;
    }

    public class QuestionAnswerClass {
        public String questionnaireTitle;
        public String questionTitle;
        public List<mmwiz_SerializedData> answersList;
    }
    
    public class QuestionMetaData {
        @AuraEnabled 
        public String IdOfRecord;

        String label;
        String title;
        String apiName;

        @AuraEnabled 
        public String displayText;
        public SerailizeData serializeContent;
    }
}