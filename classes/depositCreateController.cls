public with sharing class depositCreateController {

    public depositCreateController() {

    }

    public class DepositWrapper {
		@AuraEnabled public String key {get; set;}
		@AuraEnabled public String depositRecordType {get; set;}
		@AuraEnabled public String matterId {get; set;}
		@AuraEnabled public Decimal depositAmount {get; set;}
        @AuraEnabled public String checkNumber {get; set;}
        @AuraEnabled public Decimal hardCostLimit {get; set;}
        @AuraEnabled public Decimal softCostLimit {get; set;}
		@AuraEnabled public Decimal allocatedFee {get; set;}
        @AuraEnabled public Decimal unallocatedCost {get; set;}

		public DepositWrapper () {
			this.key = '';
			this.depositRecordType = '';
			this.matterId = '';
			this.depositAmount = 0.00;
			this.checkNumber = '';
			this.hardCostLimit = null;
			this.softCostLimit = null;
			this.allocatedFee = null;
			this.unallocatedCost = null;
		}
	}

	public class SaveResult {
		@AuraEnabled public Boolean hasValidationError {get; set;}
		@AuraEnabled public String validationMessage {get; set;}
		@AuraEnabled public Boolean isSuccess {get; set;}
		@AuraEnabled public String sucessMessage {get; set;}
		
		public SaveResult () {
			this.hasValidationError = false;
			this.validationMessage = '';
			this.isSuccess = false;
			this.sucessMessage = '';
		}
	}

    @AuraEnabled(cacheable=true)
	public static List<DepositWrapper> addDepositRow(List<DepositWrapper> depositList, Integer numberToAdd, Integer keyValue) {
        System.debug('CALLING addDepositRow');
		System.debug('addDepositRow - numberToAdd'+ numberToAdd);
		System.debug('addDepositRow - depositList'+ depositList);

        for(Integer i = 0; i < numberToAdd; i++){
			DepositWrapper dWrap = new DepositWrapper();
			dWrap.key = String.valueOf(keyValue);
            depositList.add(dWrap);
        }
		return depositList;
	}

	@AuraEnabled(cacheable=true)
    public static List<RecordType> getRecordTypeOptions(Id bankAccountId){
		c2g__codaBankAccount__c ba = [SELECT Id, Bank_Account_Type__c FROM c2g__codaBankAccount__c where Id = :bankAccountId];
		Set<String> excludeRTSet = new Set<String>();

		//Should not have option to select any deposit type of Trust when starting from operating or cost account, and when starting from trust account, should not have option of operating
		if(ba.Bank_Account_Type__c == 'Operating' || ba.Bank_Account_Type__c == 'Cost'){
			excludeRTSet.add('Trust Deposit');
			excludeRTSet.add('Trust Payout - Costs');
			excludeRTSet.add('Trust Payout - Fees');
		}
		if(ba.Bank_Account_Type__c == 'Trust'){
			excludeRTSet.add('Operating');
		}
        return [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Deposit__c' AND Name !='Transfer' AND Name not in :excludeRTSet];
    }

	@AuraEnabled(cacheable=true)
    public static Map<String,Object> getBankAccountDefaults(String bankAccountId){
		Map<String,Object> results = new Map<String,Object>();
		
		system.debug('bankAccountId = '+bankAccountId);
		if(bankAccountId != '' && bankAccountId !=null){
			for(c2g__codaBankAccount__c ba :  [
				SELECT Id, 
					Bank_Account_Type__c,
					c2g__OwnerCompany__c,
					c2g__OwnerCompany__r.Operating_Cash_Bank_Account__c,
					c2g__OwnerCompany__r.Cost_Bank_Account__c,
					c2g__OwnerCompany__r.Trust_Bank_Account__c
				FROM c2g__codaBankAccount__c
				WHERE Id = :bankAccountId]){

				//check for current company
				List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
				if(companies == null || companies.size() == 0) {
					results.put('Error', 'You must select a current company to continue');
				}
				else if(companies[0].Id != ba.c2g__OwnerCompany__c || companies.size()>1){
					results.put('Error', 'You must select a single current company and that company must match the bank account company');
				}
				else if(companies[0].Id == ba.c2g__OwnerCompany__c && companies.size() == 1){
					results.put('Error', '');
				}

				//add the current bank account
				results.put(ba.Bank_Account_Type__c, String.valueOf(ba.Id));

				//add the other bank accounts
				if(!results.containsKey('Operating')){
					String defaultIdString = ba.c2g__OwnerCompany__r.Operating_Cash_Bank_Account__c != null ? ba.c2g__OwnerCompany__r.Operating_Cash_Bank_Account__c : null;
					results.put('Operating', defaultIdString);
				}
				if(!results.containsKey('Cost')){
					String defaultIdString = ba.c2g__OwnerCompany__r.Cost_Bank_Account__c != null ? String.valueOf(ba.c2g__OwnerCompany__r.Cost_Bank_Account__c) : null;
					results.put('Cost', defaultIdString);
				}
				if(!results.containsKey('Trust')){
					String defaultIdString = ba.c2g__OwnerCompany__r.Trust_Bank_Account__c != null ? String.valueOf(ba.c2g__OwnerCompany__r.Trust_Bank_Account__c) : null;
					results.put('Trust', defaultIdString);
				}
			}
		}
	
		return results;
    }

	@AuraEnabled(cacheable=true)
    public static Map<String,Object> getMatterDetails(String matterId){
		Map<String,Object> result = new Map<String,Object>();
		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
		
		if(matterId != '' && matterId !=null){
			for(litify_pm__Matter__c matter :  [
				SELECT Id, 
					Name,
					litify_pm__Client__c, 
					Assigned_Office_Location__r.c2g__codaDimension1__c,
					litify_pm__Case_Type__r.Dimension_2__c,
					litify_pm__Case_Type__r.Dimension_3__c,
					AssignedToMMBusiness__r.FFA_Company__c
				FROM litify_pm__Matter__c
				WHERE Id = :matterId]){
				result.put('client', matter.litify_pm__Client__c);
				result.put('dim1', matter.Assigned_Office_Location__r.c2g__codaDimension1__c);
				result.put('dim2', matter.litify_pm__Case_Type__r.Dimension_2__c);
				result.put('dim3', matter.litify_pm__Case_Type__r.Dimension_3__c);

				if(companies == null){
					result.put('Error', 'You must select a current company to continue');
				}
				else if(companies[0].Id != matter.AssignedToMMBusiness__r.FFA_Company__c || companies.size()>1){
					result.put('Error', 'The matter you selected does not belong to your current company. - '+ matter.Name);
				}
				else if(companies[0].Id == matter.AssignedToMMBusiness__r.FFA_Company__c && companies.size() == 1){
					result.put('Error', '');
				}
			}
		}
		return result;
    }

	@AuraEnabled(cacheable=true)
    public static Map<String,Object> getBankAccountDetails(String bankAcctId){
		Map<String,Object> result = new Map<String,Object>();
		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
		
		if(bankAcctId != '' && bankAcctId !=null){
			for(c2g__codaBankAccount__c bankAcct :  [
				SELECT Id, 
					Name,
					c2g__ownerCompany__c
				FROM c2g__codaBankAccount__c
				WHERE Id = :bankAcctId]){

				if(companies == null){
					result.put('Error', 'You must select a current company to continue');
				}
				else if(companies[0].Id != bankAcct.c2g__ownerCompany__c || companies.size()>1){
					result.put('Error', 'The bank account you selected does not belong to your current company. - '+ bankAcct.Name);
				}
				else if(companies[0].Id == bankAcct.c2g__ownerCompany__c && companies.size() == 1){
					result.put('Error', '');
				}
			}
		}
		return result;
    }

	@AuraEnabled
    public static SaveResult saveDeposits(Map<String, Object> headerFields, List<DepositWrapper> depositWrapperList, String selectedRecordType){
		SaveResult sr = new SaveResult();

		Boolean validationError = false;
		Boolean saveComplete = false;
		String saveMessage = 'Sucess';
		String valMessage = '';

		//first validate the save:
		String expTotalMsg = checkEligibleExpenseTotal(depositWrapperList, selectedRecordType);
		if(expTotalMsg != 'IsValid'){
			validationError = true;
			valMessage += expTotalMsg + '\n';
		}
		String finStatusMsf = checkMatterFinancialStatus(depositWrapperList, selectedRecordType);
		if(finStatusMsf != 'IsValid'){
			validationError = true;
			valMessage += finStatusMsf + '\n';
		}
		//return if there is an error
		if(validationError == true){
			sr.hasValidationError = validationError;
			sr.validationMessage = valMessage;
			sr.isSuccess = false;
			sr.sucessMessage = '';
			return sr;
		}

		List<Deposit__c> depositInsertList = new List<Deposit__c>();
		system.debug('headerFields = '+ headerFields);
		system.debug('depositWrapperList = '+ depositWrapperList);

		Id trustRecordTypeId = Schema.SObjectType.Deposit__c.getRecordTypeInfosByName().get('Trust Deposit').getRecordTypeId();

		String dateString = headerFields.containsKey('Check_Date__c') ? (String)headerFields.get('Check_Date__c') : null;
		List<String> dateStringResult = dateString.split('-');
		Date depositDate = Date.newInstance(Integer.valueOf(dateStringResult[0]), Integer.valueOf(dateStringResult[1]), Integer.valueOf(dateStringResult[2]));

		for(DepositWrapper dw : depositWrapperList){

			if(dw.matterId != null && dw.matterId != ''){
				Deposit__c newD = new Deposit__c(
					RecordTypeId = selectedRecordType,
					Matter__c = dw.matterId,
					Amount__c = dw.depositAmount,
					Hard_Cost_Limit__c = dw.hardCostLimit,
					Soft_Cost_Limit__c = dw.softCostLimit,
					Allocated_Fee__c = dw.allocatedFee,
					Unallocated_Costs__c = dw.unallocatedCost,
					Check_Number__c = dw.checkNumber,
					Check_Date__c = depositDate,
					Deposit_Description__c = headerFields.containsKey('Deposit_Description__c') ? (String)headerFields.get('Deposit_Description__c') : null,
					Deposit_Allocated__c = trustRecordTypeId == selectedRecordType ? true : false,
					Operating_Cash_Account__c = headerFields.containsKey('Operating_Cash_Account__c') && headerFields.get('Operating_Cash_Account__c') != '' ? (Id)headerFields.get('Operating_Cash_Account__c') : null,
					Trust_Account__c = headerFields.containsKey('Trust_Account__c') && headerFields.get('Trust_Account__c') != '' ? (Id)headerFields.get('Trust_Account__c') : null,
					Cost_Account__c = headerFields.containsKey('Cost_Account__c') && headerFields.get('Cost_Account__c') != '' ? (Id)headerFields.get('Cost_Account__c') : null
				);
				depositInsertList.add(newD);
			}
		}
		system.debug('depositInsertList = '+ depositInsertList);
		Try{
			insert depositInsertList;
			Set<Id> depositIds = new Set<Id>();
			for(Deposit__c d : depositInsertList){
				depositIds.add(d.Id);
			}

			Id companyId = null;
			List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
			if(companies != null && !companies.isEmpty()){
				companyId = companies.get(0).id;
			}
			ffaDepositAllocationBatchSchedule allocationJob = new ffaDepositAllocationBatchSchedule(depositIds, companyId);
			Id batchId = Database.executeBatch(allocationJob, 1);
		
			sr.hasValidationError = false;
			sr.validationMessage = '';
			sr.isSuccess = true;
			sr.sucessMessage = 'Sucess';
			return sr;
		}
		Catch(Exception e){
			System.debug('depositCreateController - error' + e.getMessage());
            throw new AuraHandledException('That didn\'n work:'+'\n'+e.getMessage());
		}
    }

	@AuraEnabled()
	public static String checkEligibleExpenseTotal(List<DepositWrapper> depositList, String selectedRecordType) {

		Id costsRTID = Schema.SObjectType.Deposit__c.getRecordTypeInfosByName().get('Trust Payout - Costs').getRecordTypeId();
		String returnString = 'IsValid';
        Set<String> matterIds = new Set<String>();
		Set<String> offendingMatterIds = new Set<String>();
		String offendingMatterRefString = '';

		Map<String, Decimal> outsandingExpenseMap = new Map<String, Decimal>();
		Map<String, Decimal> matterDepositAmountMap = new Map<String, Decimal>();

		for(DepositWrapper dw : depositList){
			if(costsRTID == selectedRecordType){
				matterIds.add(dw.matterId);
				if(!matterDepositAmountMap.containsKey(dw.matterId)){
					matterDepositAmountMap.put(dw.matterId, dw.depositAmount);
				}
				else{
					matterDepositAmountMap.put(dw.matterId, matterDepositAmountMap.get(dw.matterId) + dw.depositAmount);
				}
			}			
		}
		for(AggregateResult ar : [SELECT litify_pm__Matter__c, sum(Amount_Remaining_to_Recover__c) outstanding
			FROM litify_pm__Expense__c 
			WHERE litify_pm__Matter__c in :matterIds
			GROUP BY litify_pm__Matter__c]){
			outsandingExpenseMap.put((String)ar.get('litify_pm__Matter__c'), (Decimal)ar.get('outstanding'));
		}
		//check the total of the matter against the outstanding expenses
		system.debug('depositCreateController - checkEligibleExpenseTotal - outsandingExpenseMap = '+ outsandingExpenseMap);
		system.debug('depositCreateController - checkEligibleExpenseTotal - matterDepositAmountMap = '+ matterDepositAmountMap);
		for(String matId : matterDepositAmountMap.keyset()){
			if(outsandingExpenseMap.containsKey(matId) && outsandingExpenseMap.get(matId) < matterDepositAmountMap.get(matId)){
				offendingMatterIds.add(matId);
			}
			else{
			}
		}

		if(!offendingMatterIds.isEmpty() && costsRTID == selectedRecordType){
			for(litify_pm__Matter__c mat : [SELECT ReferenceNumber__c, Name FROM litify_pm__Matter__c WHERE Id in : offendingMatterIds]){
				offendingMatterRefString += offendingMatterRefString == '' ? mat.Name :  ', '+mat.Name;
			}
			returnString = 'The deposit totals exceed the total expenses remaining to recover for the following matters: ' + offendingMatterRefString;
			return returnString;
		}
		else{
			return returnString;
		}
	}

	@AuraEnabled()
	public static String checkMatterFinancialStatus(List<DepositWrapper> depositList, String selectedRecordType) {
		String returnString = 'IsValid';
        Set<String> matterIds = new Set<String>();
		String offendingMatterRefString = '';
		for(DepositWrapper dw : depositList){
			matterIds.add(dw.matterId);
		}
		if(!matterIds.isEmpty()){
			for(litify_pm__Matter__c mat : [SELECT ReferenceNumber__c, Name 
				FROM litify_pm__Matter__c 
				WHERE Id in : matterIds 
				AND Finance_Status__c = 'Closed']){
				offendingMatterRefString += offendingMatterRefString == '' ? mat.Name :  ', '+mat.Name;
			}
		}
		if(offendingMatterRefString != ''){
			returnString = 'The following matters have a finance status of closed: ' + offendingMatterRefString;
			return returnString;
		}
		else{
			return returnString;
		}
	}
}