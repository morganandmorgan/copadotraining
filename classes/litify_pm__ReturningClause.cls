/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ReturningClause {
    global static litify_pm.ReturningClause NewInstance(Schema.SObjectType returningType) {
        return null;
    }
    global static litify_pm.ReturningClause NewInstance(String returningType) {
        return null;
    }
    global litify_pm.ReturningClause OrderBy(litify_pm.OrderBy o) {
        return null;
    }
    global litify_pm.ReturningClause Predicate(litify_pm.IPredicate p) {
        return null;
    }
    global litify_pm.ReturningClause SelectField(String f) {
        return null;
    }
    global litify_pm.ReturningClause SelectFields(List<String> f) {
        return null;
    }
    global litify_pm.ReturningClause SetLimit(Integer l) {
        return null;
    }
    global litify_pm.ReturningClause SetOffset(Integer o) {
        return null;
    }
    global override String toString() {
        return null;
    }
}
