public interface mmmatter_IExpensesSelector
    extends mmlib_ISObjectSelector
{
    List<litify_pm__Expense__c> selectById(Set<Id> idSet);
}