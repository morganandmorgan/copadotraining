public class Update_Blank_FolderController {

    

    private final litify_pm__Matter__c opp;
    
    public Update_Blank_FolderController(litify_pm__Matter__c testOpp){
        opp= testOpp;    
    }
    public Update_Blank_FolderController(ApexPages.StandardController stdController) {
        
        this.opp = (litify_pm__Matter__c)stdController.getRecord();

    }

public String buildWorkflowXML(String objecttype, string sfId, string recordType) {
    if (objecttype == null || objecttype == '') return null;
    System.debug('Type: '+objecttype);
        
        SpringCMEos.SpringCMUtilities.EOSObject eosObject = SpringCMEos.SpringCMUtilities.createEOSObject(sfId, objecttype);
    string xml = '';
    xml += '<object>';
    xml += '<id>' +  eosObject.getsfId() + '</id>';
    xml += '<type>Salesforce.' + eosObject.getsfType() + '</type>';
    If(!Test.isRunningTest()){
      xml += '<foldername>' + eosObject.getfoldername().escapeXml() + '</foldername>';
      xml += '<path>' +  eosObject.getPath().escapeXml() + '</path>';
    }
        xml += '<recordType>' + recordType + '</recordType>';
    xml += '</object>';
    return xml;
  }
    
    public string StartWorkflow{get{
       
        
       
           
            String xml;
            
             //if (!Test.isRunningTest()){
                xml     = buildWorkflowXML('litify_pm__Matter__c', opp.Id, '');
             //}else{
             //    xml = '<test>';
             //    xml += '</test>';
            // }
            SpringCMService svc = new SpringCMService();
            SpringCMWorkflow wf = new SpringCMWorkflow('Folder Creation',xml);
            
            system.debug(xml);
            SpringCMWorkflow wfs = new SpringCMWorkflow('test','test');
            if (!Test.isRunningTest()){
                wfs= svc.startWorkflow(wf);
            }
          system.debug(wfs.Href);
            
            return 'Matter Sub-Folders are created successfully';
            
            
    }
        
    }
    
    
    
    
    
}