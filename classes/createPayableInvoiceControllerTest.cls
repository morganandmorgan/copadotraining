@isTest
public class createPayableInvoiceControllerTest {
    public static litify_pm__Matter__c matter;
    public static Account client;
    public static Account mmAccountOffice;
    public static c2g__codaAccountingSettings__c accountingSettings;
    public static List<c2g__codaBankAccount__c> bankAccountList;
    public static List<createPayableInvoiceController.PayableInvoiceLineWrapper> createList;
    public static c2g__codaDimension1__c testDimension1;
    public static c2g__codaDimension2__c testDimension2;
    public static c2g__codaDimension3__c testDimension3;
    public static c2g__codaCompany__c company;
    public static c2g__codaGeneralLedgerAccount__c testGLA;
    static void setupData(){
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        accountingSettings = new c2g__codaAccountingSettings__c();
        insert accountingSettings;

        testDimension1 = ffaTestUtilities.createTestDimension1();
        testDimension2 = ffaTestUtilities.createTestDimension2();
        testDimension3 = ffaTestUtilities.createTestDimension3();

        testGLA = ffaTestUtilities.create_IS_GLA();
        company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__CustomerSettlementDiscount__c = testGLA.Id;
        update company;
        bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        c2g__codaBankAccount__c costBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Cost');
        bankAccountList.add(costBankAccount);
        // c2g__codaBankAccount__c trustBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Trust');
        // bankAccountList.add(trustBankAccount);
        insert bankAccountList;

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');
		Id mmOfficeAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Office_Locations');

        List<Account> testAccounts = new List<Account>();

        client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        client.Approved_Vendor__c = true;
        testAccounts.add(client);

        litify_pm__Case_Type__c caseType = TestDataFactory.createCaseType('Test case type');
        caseType.Dimension_2__c = testDimension2.Id;
        caseType.Dimension_3__c = testDimension3.Id;

        InSERT caseType;

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        mmAccount.c2g__codaDimension1__c = testDimension1.Id;
        testAccounts.add(mmAccount);

        mmAccountOffice = new Account();
        mmAccountOffice.Name = 'MM COMPANY OFFICE';
        mmAccountOffice.litify_pm__First_Name__c = 'TEST';
        mmAccountOffice.litify_pm__Last_Name__c = 'CONTACT';
        mmAccountOffice.litify_pm__Email__c = 'test@testcontact.com';
        mmAccountOffice.c2g__CODATaxCalculationMethod__c = 'Gross';
		mmAccountOffice.Type = 'MM Office';
        mmAccountOffice.RecordTypeId = mmOfficeAccount;
        mmAccountOffice.c2g__codaDimension1__c = testDimension1.Id;
        testAccounts.add(mmAccountOffice);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        
        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';
        matter.litify_pm__Case_Type__c = caseType.Id;
        matter.Assigned_Office_Location__c = mmAccountOffice.Id;

        INSERT matter;
        
    }

    /*--------------------------------------------------------------------
        START TEST METHODS
    --------------------------------------------------------------------*/
    @isTest static void testDepositCreateController(){

        setupData();
        //createPayableInvoiceController testController = new createPayableInvoiceController();
        createPayableInvoiceController.PayableInvoiceLineWrapper payableInvoiceLineWrapper = new createPayableInvoiceController.PayableInvoiceLineWrapper();
        createList = new List<createPayableInvoiceController.PayableInvoiceLineWrapper>();

        //test add row
        createList = createPayableInvoiceController.addPinRow(createList, 1, 0);

        // Retrieve matter details
        Map<String,Object> matterDetails = createPayableInvoiceController.getMatterDetails(matter.Id);

        createList[0].dim1Id = (String)matterDetails.get('dim1');
        createList[0].dim2Id =  (String)matterDetails.get('dim2');
        createList[0].dim3Id =  (String)matterDetails.get('dim3');
        createList[0].lineAmount = 100;
        createList[0].matterId = matter.Id;
        createList[0].glaId = testGLA.Id;

        // Check current company
        Map<String,Object> companyDetails = createPayableInvoiceController.checkCurrentCompany(bankAccountList[0].Id);
        // Get Default Expense Type
        createPayableInvoiceController.getAccountDefaultExpenseTypeId(client.Id);
        // Get Account Verified Info.
        createPayableInvoiceController.getAccountVerifiedInfo(client.Id);

        String dueDate = Date.today().year() + '-' + Date.today().month() + 
            '-' + Date.today().day();
        Map<String, Object> headerFields = new Map<String, Object>();
        // headerFields.put('Litify_Matter__c', matter.Id);
        headerFields.put('c2g__DueDate__c', dueDate);
        headerFields.put('c2g__InvoiceDate__c', dueDate);

        headerFields.put('c2g__Account__c', client.Id);
        headerFields.put('c2g__AccountInvoiceNumber__c', '1231241');
        headerFields.put('c2g__InvoiceDescription__c', 'TEST TEST');
        headerFields.put('Create_Litify_Expenses__c', TRUE);
        createPayableInvoiceController.savePins(headerFields, createList, bankAccountList[0].Id, false);
    }
}