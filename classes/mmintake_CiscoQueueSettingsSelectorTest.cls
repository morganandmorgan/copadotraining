@isTest
private class mmintake_CiscoQueueSettingsSelectorTest
{
	@isTest(seeAllData=true)
	static void selectAll()
	{
		mmintake_CiscoQueueSettingsSelector.newInstance().selectAll();
	}

	@isTest(seeAllData=true)
	static void selectByDnis()
	{
		List<Cisco_Queue_Setting__mdt> cqsList = mmintake_CiscoQueueSettingsSelector.newInstance().selectByDnis(new Set<String> {'5577', '5580'});
        System.assertEquals(2, cqsList.size());
	}
}