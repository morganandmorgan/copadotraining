global class mmintake_NewMinimalIntakeController
{
    private String phoneNumber = '';
    private String handlingFirm = '';
    private String marketingSource = '';
    private String optOut = '';

    public mmintake_NewMinimalIntakeController()
    {
        phoneNumber = ApexPages.currentPage().getParameters().get('ANI');

        String dnis = ApexPages.currentPage().getParameters().get('DNIS');
        List<Cisco_Queue_Setting__mdt> cqsList = mmintake_CiscoQueueSettingsSelector.newInstance().selectByDnis(new Set<String> {dnis});

        if (!cqsList.isEmpty())
        {
            handlingFirm = cqsList.get(0).Handling_Firm__c;
            marketingSource = cqsList.get(0).Marketing_Source__c;
        }

        if (isTest())
        {
            handlingFirm = mock_cqs.handlingFirm;
            marketingSource = mock_cqs.marketingSource;
        }
    }

    public String getPhone()
    {
        return phoneNumber;
    }

    public String getHandlingFirm()
    {
        return handlingFirm;
    }

    public String getMarketingSource()
    {
        return marketingSource;
    }

    public String getOptOut()
    {
        return optOut;
    }

    // ============ Static Functionality =============================
    private static Map<String, Schema.sObjectField> webIdToSObjectFieldMap = new Map<String, Schema.sObjectField>();

    static
    {
        webIdToSObjectFieldMap.put( 'combobox-litigation-select', Intake__c.Litigation__c );
        webIdToSObjectFieldMap.put( 'combobox-handling-firm-select', Intake__c.Handling_Firm__c );
        webIdToSObjectFieldMap.put( 'combobox-marketing-source-select', Intake__c.Marketing_Source__c );
        webIdToSObjectFieldMap.put( 'combobox-opt-out-select', Account.SMS_Text_Automated_Communication_OptOut__c );
        webIdToSObjectFieldMap.put( 'combobox-language-select', Account.Language__c );
        webIdToSObjectFieldMap.put( 'textarea-case-details', Intake__c.Lead_Details__c );
        webIdToSObjectFieldMap.put( 'textarea-generating-source-details', Intake__c.Additional_Marketing_Source_Information__c );
        webIdToSObjectFieldMap.put( 'text-input-client-email-address', Account.PersonEmail );
        webIdToSObjectFieldMap.put( 'text-input-client-first-name', Account.FirstName );
        webIdToSObjectFieldMap.put( 'text-input-client-last-name', Account.LastName );
        webIdToSObjectFieldMap.put( 'text-input-client-home-phone', Account.Phone);
        webIdToSObjectFieldMap.put( 'text-input-client-mobile-phone', Account.PersonMobilePhone);
    }

    @RemoteAction
    global static Map<String, String> litigationTypes()
    {
        return createSObjectFieldMap(Intake__c.Litigation__c);
    }

    @RemoteAction
    global static Map<String, String> languages()
    {
        return createSObjectFieldMap(Account.Language__c);
    }

    @RemoteAction
    global static Map<String, String> handlingFirms()
    {
        return createSObjectFieldMap(Intake__c.Handling_Firm__c);
    }

    @RemoteAction
    global static Map<String, String> marketingSources()
    {
        return createSObjectFieldMap(Intake__c.Marketing_Source__c);
    }

    @RemoteAction
    global static Map<String, String> optOut()
    {
        Map<String, String> optOutMap = new Map<String, String>();
        optOutMap.put('false', 'Yes');
        optOutMap.put('true', 'No');

        return optOutMap;
    }

    private static Map<String, String> createSObjectFieldMap(SObjectField field)
    {
        Map<String, String> output = new Map<String, String>();

        for ( PicklistEntry pe : field.getDescribe().getPicklistValues() )
        {
            if ( pe.isActive() )
            {
                output.put( pe.getValue(), pe.getLabel() );
            }
        }

        return output;
    }

    @RemoteAction
    global static IntakeDetails saveIntake(Map<String, String> fieldParameterMap)
    {
        IntakeDetails output = new IntakeDetails();

        system.debug( fieldParameterMap );

        if ( fieldParameterMap != null && ! fieldParameterMap.isEmpty() )
        {
            Map<Schema.sObjectField, String> serviceInputMap = new Map<Schema.sObjectField, String>();

            for ( String mapKey : fieldParameterMap.keyset() )
            {
                System.debug('JLW:::' + mapKey + ' = ' + fieldParameterMap.get( mapKey ));
                if ( webIdToSObjectFieldMap.containsKey( mapKey.toLowerCase() ) )
                {
                    if ( String.isNotBlank(fieldParameterMap.get( mapKey ))  )
                    {
                        serviceInputMap.put( webIdToSObjectFieldMap.get( mapKey.toLowerCase() ), fieldParameterMap.get( mapKey ));
                    }
                }
            }
            try
            {
                if (mock_exception != null)
                {
                    throw mock_exception;
                }

                serviceInputMap.put(Intake__c.InputChannel__c, 'MinimalIntake');

                Intake__c newIntake = processSignUpRequest( serviceInputMap );

                output.intakeName = newIntake.name;
                output.intakeId = newIntake.id;
            }
            catch ( mmattyprtl_IntakeSignUpServiceException isuse )
            {
                if ( isuse.getCause() instanceOf System.DMLException )
                {
                    System.DMLException dmle = (System.DMLException)isuse.getCause();

                    if ( StatusCode.REQUIRED_FIELD_MISSING == dmle.getDmlType(0) )
                    {
                        throw new UXException( 'The following fields are required: ' + String.join( dmle.getDmlFieldNames(0), ', '));
                    }
                    else
                    {
                        throw new UXException( 'There was a problem saving the refereral.  Please try again and if the issue persists, contact IT Support.' );
                    }
                }
                else
                {
                    throw new UXException( isuse.getCause() == null ? isuse.getMessage() : isuse.getCause().getMessage() );
                }
            }
        }

        return output;
    }

    private static Intake__c processSignUpRequest( Map<Schema.sObjectField, String> fieldParameterMap)
    {
        Intake__c intakeRequest = null;

        if ( fieldParameterMap != null
            && ! fieldParameterMap.isEmpty()
            )
        {

            mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

            Account client = null;

            if (!isTest())
            {
                client = new mmcommon_PersonAccountsFactory( uow ).with( fieldParameterMap ).generate();
            }
            else
            {
                client = mock_client;
            }

            mmintake_IntakesFactory intakesFactory = new mmintake_IntakesFactory( uow );

            String caseDetailsText = '';

            fieldParameterMap.put( Intake__c.Lead_Details__c, caseDetailsText + ( fieldParameterMap.containsKey( Intake__c.Lead_Details__c )
                                                                    ? '\n\n' + fieldParameterMap.get( Intake__c.Lead_Details__c )
                                                                    : null ) );

            if (!isTest())
            {
                intakeRequest = intakesFactory.with( fieldParameterMap )
                                            .setClient( client )
                                            .setClientAsCaller()
                                            .setClientAsInjuredParty()
                                            .generate();
            }
            else
            {
                intakeRequest = mock_intake;
            }

            if (!isTest())
            {
                mmmarketing_TrackingInfoFactory.getInstance(uow)
                    .generateNewCall(
                        intakeRequest,
                        client.Phone,
                        Datetime.now()
                );
            }

            try
            {
                if (!isTest())
                {
                    uow.commitWork();
                    intakeRequest = mmintake_IntakesSelector.newInstance().selectById( new set<id>{ intakeRequest.id } )[0];
                }
            }
            catch ( System.DMLException dmle )
            {
                throw new mmattyprtl_IntakeSignUpServiceException( dmle );
            }
        }

        return intakeRequest;
    }

    global class IntakeDetails
    {
        public String intakeName;
        public String intakeId;
    }

    public class UXException extends Exception { }

    // =============== Test Context ==========================================

    @TestVisible
    private static MockCiscoQueueSetting mock_cqs = null;

    @TestVisible
    private static Account mock_client = null;

    @TestVisible
    private static Intake__c mock_intake = null;

    @TestVisible
    private static mmattyprtl_IntakeSignUpServiceException mock_exception = null;

    private static Boolean isTest()
    {
        return mock_cqs != null || mock_client != null || mock_intake != null || mock_exception != null;
    }

    @TestVisible
    private class MockCiscoQueueSetting
    {
        public String handlingFirm = '';
        public String marketingSource = '';
    }
}