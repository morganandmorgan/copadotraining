/*==================================================================================
// Name         BankFileHandler
// Description  Contains all the necesary logic for creating and saving the bank payment .csv file
// Revisions    2018-May-07  bkrynitsky@cldpartners.com     Initial version
==================================================================================*/
public without sharing class BankFileHandler
{
    //DOC GENERATION TEMPLATE VARIABLES
    Set<Doc_Generation_Line__c> docGenerationLineSet;
    public map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> fileHeaderLineMap;
    public map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> fileDetailLineMap;
    public map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> fileFooterLineMap;
    public List<c2g__codaPaymentMediaSummary__c> paymentSummaryList;
    public List<c2g__codaCashEntry__c> cashEntryList;
    public c2g__codaPayment__c payment;
    public id paymentId;
    public id companyId;
    public Date filterStartDate;
    public Date filterEndDate;
    public id templateId;
    public string fileString;
    public Doc_Generation_Template__c template;
    public string delimiter;
    public boolean hasError;
    public boolean isCSV;
    String bankFileName;

    /*----------------------------------------------------------------------------------------
    // Name         BankFileHandler
    // Description  Constructor
    ----------------------------------------------------------------------------------------*/
    public BankFileHandler(Id pmtId, Id ceCompanyId, Date startDate, Date endDate, Id bankAccountId, Id tmpId)
    {
        docGenerationLineSet = new Set<Doc_Generation_Line__c>();
        fileHeaderLineMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>();
        fileDetailLineMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>();
        fileFooterLineMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>();
        paymentId = pmtId;
        filterStartDate = startDate;
        filterEndDate = endDate;
        companyId = ceCompanyId;
        templateId = tmpId;
        hasError = false;
        template = getDocGenerationTemplate(templateId);
        paymentSummaryList = Test.isRunningTest() == false && pmtId != null ? getPaymentMediaSummaryList(paymentId) : new List<c2g__codaPaymentMediaSummary__c>();
        cashEntryList = companyId != null && Test.isRunningTest() == false ? getCashEntries(companyId, filterStartDate, filterEndDate, bankAccountId) : new List<c2g__codaCashEntry__c>();
        payment = paymentId != null ? getPayment(paymentId) : null;
        populateDocLineMaps(templateId);
        fileString = '';
        delimiter = template.Delimiter__c;
        isCSV = template.File_Type__c == 'Delimited' && template.Delimiter__c == ',' ? true : false;
        String namePrefix = template.File_Prefix__c != null ? template.File_Prefix__c : '';
        bankFileName = namePrefix + template.Name + String.valueOf(DateTime.now()) + template.File_Extension__c; 
    }

    /*----------------------------------------------------------------------------------------
    // Name         renderBankFile
    // Description  Generates a text file attachment using the file string and attaches it to the payment obj
    ----------------------------------------------------------------------------------------*/
    public Boolean renderBankFile()
    {
        integer counter = 1;

        system.debug('cashEntryList = ' + cashEntryList);
        system.debug('companyId = ' + companyId);

        /****************
        IMPORTANT NOTE:
        There is an inherent assumption about the level of repeating lines based on the root object of the Doc Generation Line object.
        If root object = 'c2g__codaPayment__c' => 1 line is created for each Payment Batch
        If root object = 'c2g__codaPaymentMediaSummary__c' => 1 line is created for each Payment to an Account (ie the total payment to a vendor)
        If root object = 'c2g__codaPaymentMediaDetail__c' => 1 line is created for each Document included in the Payment to the Account.
        *****************/

        //Build the File Header Lines:
        for(Doc_Generation_Line__c dgl : fileHeaderLineMap.keySet()){
            List<Doc_Generation_Detail__c> detailList = fileHeaderLineMap.get(dgl);
            if(dgl.Root_Object__c == 'c2g__codaPayment__c'){
                fileString += buildLine(payment,dgl.Root_Object__c, true, detailList);
            }
        }


        // PAYMENTS
        // Loop through payment media summaries creating the necessary Detail Items: 
        for (c2g__codaPaymentMediaSummary__c pms : paymentSummaryList){

            for(Doc_Generation_Line__c dgl : fileDetailLineMap.keySet()){

                List<Doc_Generation_Detail__c> detailList = fileDetailLineMap.get(dgl);
                
                if(dgl.Root_Object__c == 'c2g__codaPaymentMediaSummary__c'){
                    fileString += buildLine(pms,dgl.Root_Object__c,true,detailList);
                }

                //check for payment media detail lines to create
                if(dgl.Root_Object__c == 'c2g__codaPaymentMediaDetail__c'){

                    for(c2g__codaPaymentMediaDetail__c pmd : pms.c2g__PaymentMediaDetails__r){
                        fileString += buildLine(pmd,dgl.Root_Object__c,true,detailList);
                    }
                }
            }
        }

        // CASH ENTRIES
        // Loop through payment media summaries creating the necessary Detail Items: 
        for (c2g__codaCashEntry__c ce : cashEntryList){

            for(Doc_Generation_Line__c dgl : fileDetailLineMap.keySet()){

                List<Doc_Generation_Detail__c> detailList = fileDetailLineMap.get(dgl);
                
                if(dgl.Root_Object__c == 'c2g__codaCashEntry__c'){
                    fileString += buildLine(ce,dgl.Root_Object__c,true,detailList);
                }
            }
        }

        //Build the File Footer Lines:
        for(Doc_Generation_Line__c dgl : fileFooterLineMap.keySet()){

            //only append a new line if this is not the last document line:
            Boolean appendLine = dgl.Order__c == docGenerationLineSet.size() ? false : true;

            List<Doc_Generation_Detail__c> detailList = fileFooterLineMap.get(dgl);
            if(dgl.Root_Object__c == 'c2g__codaPayment__c'){
                fileString += buildLine(payment,dgl.Root_Object__c, appendLine, detailList);
            }
        }
        
        //increment the line counter        
        counter ++;
        
        // Save the file onto the payment object
        if(fileString != '' && fileString != null){
            if(paymentId != null){
            attachFileToRecord(bankFileName, fileString, paymentId);
            }
            else if(companyId != null){
                attachFileToRecord(bankFileName, fileString, companyId);
            }
        }
        else{
            hasError = true;
        }
        
        system.debug('hasError = ' + hasError);
        return hasError;
    }

    /*----------------------------------------------------------------------------------------
    // Name         buildHeaderLine
    // Description  Builds the header record line
    ----------------------------------------------------------------------------------------*/
    public string buildLine(SObject sobj, String sobjectType, Boolean appendNewLine, List<Doc_Generation_Detail__c> detailList){

        string rowString = '';
        integer counter = 1;
        

        for (Doc_Generation_Detail__c dgd: detailList){
            system.debug('NOW BUILDING LINE = ' + dgd.Name);

            // Generation Detail Output String - to be appended to the rowString for return
            string detailString = ''; 

            // Case 1 - SObject was provided and Field Path is not null, and Header Root Object is not null - use value from SObject
            if (sobj != null && dgd.Field_Path__c != null){
                string[] parentObjectSelectList = dgd.Field_Path__c.split('\\.');
                //system.debug('parentObjectSelectList = ' + parentObjectSelectList);
                SObject tempObj = sobj;

                //Handle parent SObject references. 
                String fieldValue = '';
                try {
                    //String displayType = String.ValueOf(objectFields.get(parentObjectSelectList[parentObjectSelectList.size()-1].tolowercase()).getDescribe().getType());
                    String displayType = '';
                    
                    
                    for (Integer i=0; i <= parentObjectSelectList.size() -1; i++){
                        //system.debug('parentObjectSelectList[i] = ' + parentObjectSelectList[i]);

                        //If it's the last item in the collection then it's a field:
                        Boolean isField = i == parentObjectSelectList.size() -1 ? true : false;

                        if(isField == false){
                            //get the object type:
                            tempObj = tempObj.getSObject(parentObjectSelectList[i]);
                            //system.debug('tempObj = ' + tempObj);    
                        }

                        if(isField == true){
                            //get the data type:
                            String lowerCaseLabel = parentObjectSelectList[i].toLowercase();
                            //system.debug('lowerCaseLabel = ' + lowerCaseLabel);
                            map<string, Schema.SObjectField> objectFields = tempObj.getSObjectType().getDescribe().fields.getMap();
                            displayType = String.ValueOf(objectFields.get(lowerCaseLabel).getDescribe().getType());
                            //system.debug('displayType = ' + displayType);
                        }
                    }
                    
                /*Start formatting of values*/

                    //Convert Numbers into special formatting
                    if (template.Number_Format__c == 'No Decimal' && displayType == 'DOUBLE'){
                        Double rawDouble = Double.valueOf(tempObj.get(parentObjectSelectList[parentObjectSelectList.size()-1]));
                        fieldValue = formatDoubleNoDecimal(rawDouble);
                        
                    }
                    //Convert Dates into Strings using the appropriate methods
                    else if (displayType == 'DATE'){
                        Date rawDate = Date.valueOf(tempObj.get(parentObjectSelectList[parentObjectSelectList.size()-1]));
                        fieldValue = template.Date_Format__c == 'YYYYMMDD' ? formatDateYYYYMMDD(rawDate) : template.Date_Format__c == 'MMDDYY' ? formatDateMMDDYY(rawDate) : rawDate.format();
                        
                    }
                    //else take the default string values
                    else{
                        fieldValue = String.valueOf(tempObj.get(parentObjectSelectList[parentObjectSelectList.size()-1]));
                        
                    }

                    //strip out alpha characters if appropriate
                    fieldValue = dgd.Strip_Alpha_Characters__c == true ? stripAlphaCharacters(fieldValue) : fieldValue;
                    

                /*End formatting of values*/

                }
                catch (Exception e){
                    system.debug('BankFileHandler.buildHeaderLine(): ERROR = ' + e.getMessage() +  'STACK TRACE = '+ e.getStackTraceString());
                    hasError = true;
                }
                system.debug('fieldValue = ' + fieldValue);

                detailString += generateDocGenerationDetailValue(dgd, fieldValue); 

            }

            // Case 2 - SObject was not provided and Mapping Literal is not null - use Mapping Literal value
            else{
                // If the literal is blank or null, pass a blank string to the detail value generation method 
                if (dgd.Mapping_Literal__c == null || String.isBlank(dgd.Mapping_Literal__c)){
                    detailString += generateDocGenerationDetailValue(dgd,''); 
                }
                // If the literal is an expected generic date format expression, generate today's date in the specified format
                else if (dgd.Mapping_Literal__c.equalsIgnoreCase('Date.today()')){
                    Date rawDate = Date.today();
                    detailString += template.Date_Format__c == 'YYYYMMDD' ? formatDateYYYYMMDD(rawDate) : template.Date_Format__c == 'MMDDYY' ? formatDateMMDDYY(rawDate) : rawDate.format();
                }
                // If the literal is an expected generic date format expression, generate today's date in the specified format
                else if (dgd.Mapping_Literal__c.equalsIgnoreCase('DateTime.now()')){
                    DateTime rawDateTime = DateTime.now();
                    detailString += formatDateTimeHHMM(rawDateTime);
                }
                else {
                    detailString += generateDocGenerationDetailValue(dgd,dgd.Mapping_Literal__c);  
                }
                if(dgd.Mapping_Literal__c != null){
                    system.debug('BankFileHandler.buildHeaderLine(): Mapping Literal =' +dgd.Mapping_Literal__c);
                }
            }

            // Add the file separators:
            if (template.File_Type__c == 'Delimited' && counter < detailList.size()){
                detailString = addSeparator(detailString, delimiter);   
            }
            
            rowString += detailString;
            counter++;
        }
        if (appendNewLine){
            rowString += '\r\n'; 
        } 

        return rowString;
    }
   
    /*----------------------------------------------------------------------------------------
    // Name         attachFileToRecord
    // Description  Attaches the generated file to the payment object
    ----------------------------------------------------------------------------------------*/
    public void attachFileToRecord(String fileName, String base64FileBody, Id objectToLinkToId) {
        // Create a new Content Version object
        ContentVersion cv = new ContentVersion();
        cv.VersionData = Blob.valueOf(base64FileBody);
        cv.Title = fileName;
        cv.PathOnClient = filename;
        insert cv;

        // Create the content link to the object
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId
                                 FROM ContentVersion
                                WHERE Id =: cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = objectToLinkToId;
        cdl.ShareType = 'V';
        insert cdl;
    }

    /*----------------------------------------------------------------------------------------
    // Name         addSeparator
    // Description  Returns the string with the separator appended to the end
    ----------------------------------------------------------------------------------------*/
    public string addSeparator(string inputString, string separator)
    {
        return inputString + separator;
    }

    /*----------------------------------------------------------------------------------------
    // Name         formatDateYYYY-MM-DD
    // Description  Takes a Date variable and converts the to YYYY-MM-DD format with zeros
    ----------------------------------------------------------------------------------------*/
    public string formatDateYYYYMMDD(Date rawDate)
    {
        String sMonth = String.valueof(rawDate.month()).length() == 1 ? '0' + String.valueof(rawDate.month()) : String.valueof(rawDate.month());
        String sDay = String.valueof(rawDate.day()).length() == 1 ? '0' + String.valueof(rawDate.day()) : String.valueof(rawDate.day());
        String sYear = String.valueof(rawDate.year());
        String formattedDate = String.valueof(rawDate.year())+''+sMonth+''+sDay;
        return formattedDate;
    }

    /*----------------------------------------------------------------------------------------
    // Name         formatDateYYYY-MM-DD
    // Description  Takes a Date variable and converts the to YYYY-MM-DD format with zeros
    ----------------------------------------------------------------------------------------*/
    public string formatDateMMDDYY(Date rawDate)
    {
        String sMonth = String.valueof(rawDate.month()).length() == 1 ? '0' + String.valueof(rawDate.month()) : String.valueof(rawDate.month());
        String sDay = String.valueof(rawDate.day()).length() == 1 ? '0' + String.valueof(rawDate.day()) : String.valueof(rawDate.day());
        String sYear = String.valueof(rawDate.year()).right(2);
        String formattedDate = sMonth+sDay+sYear;
        return formattedDate;
    }

    /*----------------------------------------------------------------------------------------
    // Name         formatDateTimeHHMM
    // Description  Takes a DateTime variable and converts the to HHMM format with zeros
    ----------------------------------------------------------------------------------------*/
    public string formatDateTimeHHMM(DateTime rawDateTime)
    {
        String sHour = String.valueof(rawDateTime.hour()).length() == 1 ? '0' + String.valueof(rawDateTime.hour()) : String.valueof(rawDateTime.hour());
        String sMinute = String.valueof(rawDateTime.minute()).length() == 1 ? '0' + String.valueof(rawDateTime.minute()) : String.valueof(rawDateTime.minute());
        String formattedDateTime = sHour+sMinute;
        return formattedDateTime;
    }

    /*----------------------------------------------------------------------------------------
    // Name         formatDoubleNoDecimal
    // Description  Takes a double and returns a string value with the decimal stripped
    ----------------------------------------------------------------------------------------*/
    public string formatDoubleNoDecimal(Double rawDouble)
    {
        String roundedDecimalString = String.valueOf(Decimal.valueOf(rawDouble).setScale(2));
        String cents = roundedDecimalString.right(2);
        String dollars = roundedDecimalString.length() >3 ? roundedDecimalString.left(roundedDecimalString.length()-3) : '';
        String formattedDouble = dollars + cents;
        return formattedDouble;
    }

    /*----------------------------------------------------------------------------------------
    // Name         stripAlphaCharacters
    // Description  Takes a double and returns a string value with the decimal stripped
    ----------------------------------------------------------------------------------------*/
    public string stripAlphaCharacters(String rawString)
    {
        if(rawString == null || rawString == ''){
            return rawString;
        }
        String alpha = '';
        String numeric = '';
        List<String> chars = rawString.split('|');
        for (String s : chars){
            alpha += s.isAlpha() ? s : '';     
            numeric += s.isAlpha() == false ? s : '';
        }
        return numeric;
    }
    

    /*----------------------------------------------------------------------------------------
    // Name         generateDocGenerationDetailValue
    // Description  Returns any necessary escaped strings + any substrings to match the length
    ----------------------------------------------------------------------------------------*/
    public string generateDocGenerationDetailValue (Doc_Generation_Detail__c dgd, string inputString)
    {
        system.debug('BankFileHandler.generateDocGenerationDetailValue(): inputString = ' + inputString);

        string returnString = inputString != null ? inputString : '';
        integer valueLength = Integer.valueOf(dgd.Length__c); 

        //trim the length if value is greater
        if (returnString.length() > valueLength){
            returnString.substring(0, valueLength-1);
        }

        //fixed length operations
        if (template.File_Type__c == 'Fixed Length'){
            
            //add padding as appropriate
            if(returnString.length() < valueLength){

                //left padding and filling:
                returnString = dgd.Text_Filler__c != null && dgd.Text_Justification__c != 'Right' ? returnString.rightPad(valueLength, dgd.Text_Filler__c) : dgd.Text_Filler__c == null && dgd.Text_Justification__c != 'Right' ? returnString.rightPad(valueLength) : returnString;

                //right padding and filling:
                returnString = dgd.Text_Filler__c != null && dgd.Text_Justification__c == 'Right' ? returnString.leftPad(valueLength, dgd.Text_Filler__c) : dgd.Text_Filler__c == null && dgd.Text_Justification__c == 'Right' ? returnString.leftPad(valueLength) : returnString;
            }
        }
        
        //escape any commas in a csv file
        if(isCSV == true && returnString.containsAny(',')){
            returnString = '"'+returnString+'"';
        }
        
        return returnString; 
    }


    

    /*----------------------------------------------------------------------------------------
    // Name         getDocGenerationTemplate
    // Description  Returns all Doc Generation Template
    ----------------------------------------------------------------------------------------*/
    public Doc_Generation_Template__c getDocGenerationTemplate(Id docGenerationTemplateId)
    {
        Doc_Generation_Template__c template = [
            SELECT Id, 
               Name, 
               File_Type__c,
               File_Prefix__c,
               Delimiter__c,
               Date_Format__c,
               Number_Format__c,
               File_Extension__c
            FROM Doc_Generation_Template__c
            WHERE Id = :docGenerationTemplateId];
        return template; 
    }

    /*----------------------------------------------------------------------------------------
    // Name         populateDocLineMaps
    // Description  Returns all Doc Generation Header and Detail info for a given Doc Generation Template Id
    ----------------------------------------------------------------------------------------*/
    public void populateDocLineMaps(Id docGenerationTemplateId){
        map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>> returnMap = new map<Doc_Generation_Line__c,list<Doc_Generation_Detail__c>>(); 

        for (Doc_Generation_Line__c dgl: [SELECT Id, 
                                                   Name, 
                                                   Order__c, 
                                                   Root_Object__c,
                                                   File_Record_Type__c,
                                                   (SELECT Id, 
                                                           Name, 
                                                           Length__c, 
                                                           Field_Path__c, 
                                                           Mapping_Literal__c, 
                                                           Order__c,
                                                           Text_Filler__c,
                                                           Text_Justification__c,
                                                           Strip_Alpha_Characters__c
                                                      FROM Doc_Generation_Details__r
                                                  ORDER BY Order__c ASC)
                                              FROM Doc_Generation_Line__c
                                             WHERE Doc_Generation_Template__c = :docGenerationTemplateId
                                          ORDER BY Order__c ASC])
        {
            docGenerationLineSet.add(dgl);

            if(dgl.File_Record_Type__c == 'File Header'){
                fileHeaderLineMap.put(dgl, dgl.Doc_Generation_Details__r);     
            }
            if(dgl.File_Record_Type__c == 'File Detail'){
                fileDetailLineMap.put(dgl, dgl.Doc_Generation_Details__r);     
            }
            if(dgl.File_Record_Type__c == 'File Footer'){
                fileFooterLineMap.put(dgl, dgl.Doc_Generation_Details__r);     
            }
            
        }

    }

    /*----------------------------------------------------------------------------------------
    // Name         getPaymentMediaSummaryList
    // Description  Returns all the Payment Media Summaries and the list of the related Payment Media Details
    ----------------------------------------------------------------------------------------*/
    public List<c2g__codaPaymentMediaSummary__c> getPaymentMediaSummaryList(Id filterId)
    {
        List<c2g__codaPaymentMediaSummary__c> pmsList = [
            SELECT c2g__Account__c,
                c2g__PaymentReference__c,
                c2g__PaymentMediaControl__r.c2g__Payment__r.c2g__BankAccount__r.c2g__AccountNumber__c,
                c2g__PaymentMediaControl__r.c2g__PaymentDate__c,
                Positive_Payment_Value__c,
                Payment_Account_Name__c,
              (SELECT Id,
                      Positive_Payment_Value__c
                FROM c2g__PaymentMediaDetails__r)
         FROM c2g__codaPaymentMediaSummary__c
        WHERE c2g__PaymentMediaControl__r.c2g__Payment__c = :filterId];
    
        return pmsList; 
    }

    /*----------------------------------------------------------------------------------------
    // Name         getCashEntries
    // Description  Returns all the Cash Entries for a given company, start date, end date
    ----------------------------------------------------------------------------------------*/
    public List<c2g__codaCashEntry__c> getCashEntries(Id companyId, Date startDate, Date endDate, Id bankAccountId)
    {
        List<c2g__codaCashEntry__c> ceList = [
            SELECT c2g__Account__c,
                c2g__Reference__c,
                c2g__PaymentType__c,
                Pos_Pay_File_Idicator__c,
                c2g__Date__c,
                c2g__NetValue__c,
                Payment_Account_Name__c,
                c2g__BankAccount__r.c2g__AccountNumber__c,
                Check_Number__c
         FROM c2g__codaCashEntry__c
        WHERE c2g__OwnerCompany__c = :companyId
        AND c2g__PaymentNumber__r.c2g__PaymentMediaTypes__c = 'Check'
        AND c2g__BankAccount__c = :bankAccountId
        AND c2g__Date__c <= :endDate
        AND c2g__Date__c >= :startDate
        AND c2g__PaymentNumber__c != null
        AND Check_Number__c != null];
    
        return ceList; 
    }

    /*----------------------------------------------------------------------------------------
    // Name         getPayment
    // Description  Returns the Payment Media Control 
    ----------------------------------------------------------------------------------------*/
    public c2g__codaPayment__c getPayment(Id filterId)
    {
        c2g__codaPayment__c returnObj = 
            [SELECT Id,
                    Name,
                    c2g__PaymentDate__c,
                    c2g__PaymentMediaTypes__c,
                    c2g__BankAccount__r.c2g__AccountNumber__c
              FROM c2g__codaPayment__c
             WHERE Id = :filterId];
        
        return returnObj; 
    }

}