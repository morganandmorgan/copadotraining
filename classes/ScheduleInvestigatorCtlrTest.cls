@isTest
private class ScheduleInvestigatorCtlrTest {
    static String AZCUniqueCityRTName = 'Unique_City';
    static String AZCUniqueCountyRTName = 'Unique_County';
    static String InvestigatorProfileName = 'M&M CCC';
    static String Language1 = 'English';
    static String Language2 = 'Spanish';
    static String Skill1 = 'Nation Law';
    static String Skill2 = 'Morgan & Morgan';
    static String TerritoryName = 'TestTerritory';
    static String TerritoryName2 = 'TestTerritory2';
    static String TestCounty = 'Orange';
    static String IntakeCaseType = 'RealEstate';
    static String IntakeTypeOfInjury = 'Test Injury Type';

    static Incident__c TheIncident;
    static List<User> InvestigatorsInTerritory;
    static List<User> InvestigatorsOutOfTerritory;

    @testSetup
    static void SetupData() {
        ScheduleInvestigatorSettings__c schedulerSettings = ScheduleInvestigatorSettings__c.getInstance();
        schedulerSettings.Delayed_Signup_Reason_Time__c = 32;
        Database.upsert(schedulerSettings);

        List<SObject> toInsert = new List<SObject>();

        List<Flow_State_Code__c> stateCodes = new List<Flow_State_Code__c>();
        for (Integer i = 0; i < 50; ++i) {
            stateCodes.add(new Flow_State_Code__c(
                    Name = 'State ' + i,
                    State_Abbreviation__c = 'S' + i
                ));
        }
        toInsert.addAll((List<SObject>) stateCodes);

        // Create flow case and injury types to ensure intakes are catastrophic
        Flow_Case_Type__c caseTypeSetting = new Flow_Case_Type__c(
                Name = IntakeCaseType,
                Catastrophic__c = true
            );
        toInsert.add(caseTypeSetting);

        Flow_Injury_Type__c injuryTypeSettings = new Flow_Injury_Type__c(
                Name = IntakeTypeOfInjury,
                Catastrophic__c = true
            );
        toInsert.add(injuryTypeSettings);

        List<Address_by_Zip_Code__c> testAzc = new List<Address_by_Zip_Code__c>{
            new Address_by_Zip_Code__c( City__c = TestUtil.DefaultPersonAccountBillingCity,
                                        Flow_Label__c = TestUtil.DefaultPersonAccountBillingCity,
                                        County__c = ScheduleInvestigatorCtlrTest.TestCounty,
                                        State__c = TestUtil.DefaultPersonAccountBillingState,
                                        State_Code__c = TestUtil.DefaultPersonAccountBillingState.left(Address_by_Zip_Code__c.State_Code__c.getDescribe().getLength()),
                                        RecordTypeId = RecordTypeUtil.getRecordTypeIDByDevName('Address_by_Zip_Code__c', ScheduleInvestigatorCtlrTest.AZCUniqueCityRTName),
                                        Territory__c = TerritoryName),
            new Address_by_Zip_Code__c( City__c = TestUtil.DefaultPersonAccountBillingCity,
                                        Flow_Label__c = TestUtil.DefaultPersonAccountBillingCity,
                                        County__c = ScheduleInvestigatorCtlrTest.TestCounty,
                                        State__c = TestUtil.DefaultPersonAccountBillingState,
                                        State_Code__c = TestUtil.DefaultPersonAccountBillingState.left(Address_by_Zip_Code__c.State_Code__c.getDescribe().getLength()),
                                        RecordTypeId = RecordTypeUtil.getRecordTypeIDByDevName('Address_by_Zip_Code__c', ScheduleInvestigatorCtlrTest.AZCUniqueCountyRTName),
                                        Territory__c = TerritoryName)};
        toInsert.addAll((List<SObject>) testAzc);

        List<Account> testAccounts = new List<Account>{ TestUtil.createPersonAccount('Client', 'One'),
                                                        TestUtil.createPersonAccount('Injured', 'Party'),
                                                        TestUtil.createPersonAccount('The', 'Caller') };
        toInsert.addAll((List<SObject>) testAccounts);

        Incident__c testIncident = new Incident__c();
        toInsert.add(testIncident);

        Database.insert(toInsert);
        toInsert.clear();

        Id investigatorProfileId = [SELECT Id FROM Profile WHERE Name = :InvestigatorProfileName LIMIT 1].Id;
        List<User> investigators = new List<User>{ TestUtil.createUser(investigatorProfileId), TestUtil.createUser(investigatorProfileId), TestUtil.createUser(investigatorProfileId) };
        investigators[0].Territory__c = ScheduleInvestigatorCtlrTest.TerritoryName;
        investigators[0].Language__c = ScheduleInvestigatorCtlrTest.Language1;
        investigators[0].Skill__c = ScheduleInvestigatorCtlrTest.Skill1;
        investigators[1].Territory__c = ScheduleInvestigatorCtlrTest.TerritoryName;
        investigators[1].Language__c = ScheduleInvestigatorCtlrTest.Language1 + ';' + ScheduleInvestigatorCtlrTest.Language2;
        investigators[1].Skill__c = ScheduleInvestigatorCtlrTest.Skill1 + ';' + ScheduleInvestigatorCtlrTest.Skill2;
        investigators[2].Territory__c = ScheduleInvestigatorCtlrTest.TerritoryName2; // Used to test location filters in controller
        investigators[2].Language__c = ScheduleInvestigatorCtlrTest.Language1;
        investigators[2].Skill__c = ScheduleInvestigatorCtlrTest.Skill1;
        System.runAs(new User(Id = UserInfo.getUserId())){
            Database.insert(investigators);
        }

        List<Intake__c> testIntakes = new List<Intake__c>{ ScheduleInvestigatorCtlrTest.CreateTestIntake(testIncident.Id, testAccounts[2].Id, testAccounts[0].Id, testAccounts[1].Id),
                                                            ScheduleInvestigatorCtlrTest.CreateTestIntake(testIncident.Id, testAccounts[2].Id, testAccounts[2].Id, testAccounts[2].Id) };
        toInsert.addAll((List<SObject>) testIntakes);

        List<Event> homeEvents = new List<Event>();
        Date today = Date.today();
        Date yesterday = today.addDays(-1);
        Date tomorrow = today.addDays(1);
        DateTime yesterdayHomeEventStart = DateTime.newInstance(yesterday.year(), yesterday.month(), yesterday.day(), 17, 0, 0);
        DateTime yesterdayHomeEventEnd = DateTime.newInstance(today.year(), today.month(), today.day(), 0, 0, 0);
        DateTime todayHomeEventMorningStart = DateTime.newInstance(today.year(), today.month(), today.day(), 0, 0, 0);
        DateTime todayHomeEventMorningEnd = DateTime.newInstance(today.year(), today.month(), today.day(), 8, 0, 0);
        DateTime todayHomeEventEveningStart = DateTime.newInstance(today.year(), today.month(), today.day(), 17, 0, 0);
        DateTime todayHomeEventEveningEnd = DateTime.newInstance(tomorrow.year(), tomorrow.month(), tomorrow.day(), 0, 0, 0);
        DateTime tomorrowHomeEventStart = DateTime.newInstance(tomorrow.year(), tomorrow.month(), tomorrow.day(), 0, 0, 0);
        DateTime tomorrowHomeEventEnd = DateTime.newInstance(tomorrow.year(), tomorrow.month(), tomorrow.day(), 8, 0, 0);
        for(User investigator : investigators){
            homeEvents.add(ScheduleInvestigatorCtlrTest.CreateHomeEvent(investigator.Id, yesterdayHomeEventStart, yesterdayHomeEventEnd));
            homeEvents.add(ScheduleInvestigatorCtlrTest.CreateHomeEvent(investigator.Id, todayHomeEventMorningStart, todayHomeEventMorningEnd));
            homeEvents.add(ScheduleInvestigatorCtlrTest.CreateHomeEvent(investigator.Id, todayHomeEventEveningStart, todayHomeEventEveningEnd));
            homeEvents.add(ScheduleInvestigatorCtlrTest.CreateHomeEvent(investigator.Id, tomorrowHomeEventStart, tomorrowHomeEventEnd));
        }

        toInsert.addAll((List<SObject>) homeEvents);

        Database.insert(toInsert);
        toInsert.clear();
    }

    // Common data queries
    static void loadCommonData(){
        ScheduleInvestigatorCtlrTest.TheIncident = [SELECT Id FROM Incident__c LIMIT 1];
        ScheduleInvestigatorCtlrTest.InvestigatorsInTerritory = [SELECT Id FROM User WHERE Territory__c = :ScheduleInvestigatorCtlrTest.TerritoryName];
        ScheduleInvestigatorCtlrTest.InvestigatorsOutOfTerritory = [SELECT Id FROM User WHERE Territory__c = :ScheduleInvestigatorCtlrTest.TerritoryName2];
    }

    private static void selectAnInvestigator(ScheduleInvestigatorCtlr ctlr) {
        List<Id> idList = new List<Id>(ctlr.territoryUsersAvailableSlots.keyset());
        Id selectedInvestigatorId = idList.get(0);
system.debug( selectedInvestigatorId );
        List<ScheduleInvestigatorService.AvailableSlot> investigatorAvailability = ctlr.territoryUsersAvailableSlots.get(selectedInvestigatorId);
system.debug( investigatorAvailability );
        ScheduleInvestigatorService.AvailableSlot avail = investigatorAvailability.get(0);
        ctlr.selectedSlot = selectedInvestigatorId + '-' + avail.timeFrameHour + '.' + avail.timeFrameMinute;
        system.debug( 'ctlr.selectedSlot == '+ctlr.selectedSlot);
    }

    private static Event getScheduledInvestigationEvent(Id incidentId) {
        IncidentInvestigationEvent__c incidentInvestigationEvent = [SELECT Id FROM IncidentInvestigationEvent__c WHERE Incident__c = :incidentId];
        return [SELECT Id FROM Event WHERE WhatId = :incidentInvestigationEvent.Id AND Type = :ScheduleInvestigatorService.EVENT_TYPE];
    }

//  testMethod static void FilterInvestigatorsChecksLanguage(){
//      /*
//          Verifies:
//          1) If language override is not enabled and no language matches are found an error is provided
//          2) If language override is enabled and no language matches are found then no error is provided
//      */
//
//      ScheduleInvestigatorCtlrTest.loadCommonData();
//      Test.startTest();
//
//      ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
//      ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
//
//      controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
//      controller.scheduledDate = Date.today().format();
//      controller.isLanguagesEnabled = false;
//      controller.isLocationEnabled = true;
//      controller.isSkillEnabled = true;
//      controller.selectedLanguage = new List<String>{ ScheduleInvestigatorCtlrTest.Language2 };
//      controller.filterInvestigators();
//      System.assert(!controller.userNotifications.isEmpty());
//      for (Id userId : controller.userNotifications.keySet()) {
//          User u = controller.territoryUsersAvailableSlots.get(userId).get(0).investigator;
//          ScheduleInvestigatorCtlr.Notification notice = controller.userNotifications.get(userId);
//          if (u.Language__c.contains(ScheduleInvestigatorCtlrTest.Language2)) {
//              System.assert(!notice.isNotificationEnabled, 'Controller should not have provided error notifications.');
//          }
//          else {
//              System.assert(notice.isNotificationEnabled, 'Controller should have provided error notifications.');
//          }
//      }
//
//      controller.isLanguagesEnabled = true;
//      controller.filterInvestigators();
//      System.assert(!controller.userNotifications.isEmpty());
//      for (ScheduleInvestigatorCtlr.Notification notice : controller.userNotifications.values()) {
//          System.assert(!notice.isNotificationEnabled, 'Controller should not have provided error notifications.');
//      }
//
//      Test.stopTest();
//  }
//
//    @isTest
//    private static void FilterInvestigatorsChecksSkill() {
//        ScheduleInvestigatorCtlrTest.loadCommonData();
//        Test.startTest();
//
//        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
//        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
//
//        controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
//        controller.scheduledDate = Date.today().format();
//        controller.isLanguagesEnabled = true;
//        controller.isLocationEnabled = true;
//        controller.isSkillEnabled = false;
//        controller.selectedSkill = new List<String>{ ScheduleInvestigatorCtlrTest.Skill2 };
//        controller.filterInvestigators();
//        System.assert(!controller.userNotifications.isEmpty());
//        for (Id userId : controller.userNotifications.keySet()) {
//            User u = controller.territoryUsersAvailableSlots.get(userId).get(0).investigator;
//            ScheduleInvestigatorCtlr.Notification notice = controller.userNotifications.get(userId);
//            if (u.Skill__c.contains(ScheduleInvestigatorCtlrTest.Skill2)) {
//                System.assert(!notice.isNotificationEnabled, 'Controller should not have provided error notifications.');
//            }
//            else {
//                System.assert(notice.isNotificationEnabled, 'Controller should have provided error notifications.');
//            }
//        }
//
//        controller.isSkillEnabled = true;
//        controller.filterInvestigators();
//        System.assert(!controller.userNotifications.isEmpty());
//        for (ScheduleInvestigatorCtlr.Notification notice : controller.userNotifications.values()) {
//            System.assert(!notice.isNotificationEnabled, 'Controller should not have provided error notifications.');
//        }
//
//        Test.stopTest();
//    }
//
//    testMethod static void FilterInvestigatorsChecksLocation(){
//        /*
//            Verifies:
//            1) If location override is not enabled and no investigators match the venue then an error is provided
//            2) If location override is enabled and no investigators match the venue then no error is provided
//        */
//
//        ScheduleInvestigatorCtlrTest.loadCommonData();
//        Test.startTest();
//
//        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
//        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
//
//        controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
//        controller.scheduledDate = Date.today().format();
//        controller.isLanguagesEnabled = true;
//        controller.isLocationEnabled = false;
//        controller.isSkillEnabled = true;
//        controller.selectedLanguage = new List<String>{ ScheduleInvestigatorCtlrTest.Language1 };
//        controller.filterInvestigators();
//        System.assert(!controller.userNotifications.isEmpty());
//        for (Id userId : controller.userNotifications.keySet()) {
//            User u = controller.territoryUsersAvailableSlots.get(userId).get(0).investigator;
//            ScheduleInvestigatorCtlr.Notification notice = controller.userNotifications.get(userId);
//            if (u.Territory__c == TerritoryName) {
//                System.assert(!notice.isNotificationEnabled, 'Controller should not have provided error notifications.');
//            }
//            else {
//                System.assert(notice.isNotificationEnabled, 'Controller should have provided error notifications.');
//            }
//        }
//
//        controller.isLocationEnabled = true;
//        controller.filterInvestigators();
//        System.assert(!controller.userNotifications.isEmpty());
//        for (ScheduleInvestigatorCtlr.Notification notice : controller.userNotifications.values()) {
//            System.assert(!notice.isNotificationEnabled, 'Controller should not have provided error notifications.');
//        }
//
//        Test.stopTest();
//    }
//
//  testMethod static void InvestigationScheduledForSingleClient(){
//      // Verify the investigation event when signing up a single client
//
//      ScheduleInvestigatorCtlrTest.loadCommonData();
//      Integer numAttendees = 1;
//      Test.startTest();
//
//      ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
//      ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
//
//      controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
//      controller.scheduledDate = Date.today().format();
//      system.debug( controller.scheduledDate );
//      controller.isLanguagesEnabled = true;
//      controller.isLocationEnabled = false;
//      controller.isSkillEnabled = true;
//      controller.selectedLanguage = new List<String>{ ScheduleInvestigatorCtlrTest.Language1 };
//      controller.filterInvestigators();
//
//      controller.incidentInstance.attendees.get(0).selected = true;
//      selectAnInvestigator(controller);
//
//      PageReference pageRef = controller.saveEvent();
//
//      Test.stopTest();
//
//      System.assertNotEquals(null, pageRef, ApexPages.getMessages());
//      System.assertEquals('/' + ScheduleInvestigatorCtlrTest.TheIncident.Id, pageRef.getUrl(), ApexPages.getMessages());
//
//      Event newEvent = getScheduledInvestigationEvent(ScheduleInvestigatorCtlrTest.TheIncident.Id);
//
//      List<EventRelation> attendees = [SELECT Id, RelationId FROM EventRelation WHERE EventId = :newEvent.Id AND IsWhat = FALSE];
//      System.debug('attendees: ' + attendees);
//      System.assertEquals(numAttendees, attendees.size(), 'Controller did not save event as expected.');
//  }
//
//  testMethod static void InvestigationScheduledForMultipleClient(){
//      // Verify the investigation event when signing up multiple clients
//      // Primarily verifies all clients are related to the event
//
//      ScheduleInvestigatorCtlrTest.loadCommonData();
//      Integer numAttendees = 0;
//      Test.startTest();
//
//      ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
//      ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
//
//      controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
//      controller.scheduledDate = Date.today().format();
//      controller.isLanguagesEnabled = true;
//      controller.isLocationEnabled = false;
//      controller.isSkillEnabled = true;
//      controller.selectedLanguage = new List<String>{ ScheduleInvestigatorCtlrTest.Language1 };
//      controller.filterInvestigators();
//
//      for(ScheduleInvestigatorCtlr.Attendee eachAttendee : controller.incidentInstance.attendees){
//          eachAttendee.selected = true;
//          numAttendees++;
//      }
//
//      selectAnInvestigator(controller);
//
//      PageReference pageRef = controller.saveEvent();
//
//      Test.stopTest();
//
//      System.assertNotEquals(null, pageRef, ApexPages.getMessages());
//      System.assertEquals('/' + ScheduleInvestigatorCtlrTest.TheIncident.Id, pageRef.getUrl(), ApexPages.getMessages());
//
//      Event newEvent = getScheduledInvestigationEvent(ScheduleInvestigatorCtlrTest.TheIncident.Id);
//
//      List<EventRelation> attendees = [SELECT Id, RelationId FROM EventRelation WHERE EventId =: newEvent.Id AND IsWhat = FALSE];
//      System.debug('attendees: ' + attendees);
//      System.assertEquals(numAttendees, attendees.size(), 'Controller did not save event as expected.');
//  }

    @isTest
    private static void cancelActionSelected() {
        ScheduleInvestigatorCtlrTest.loadCommonData();
        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
        PageReference pr = controller.cancelSchedule();

        Test.stopTest();

        System.assertNotEquals(null, pr);
        System.assertEquals('/' + ScheduleInvestigatorCtlrTest.TheIncident.Id, pr.getUrl());
    }

    @isTest
    private static void cancelActionSelected_IntakeSourcePage() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        Intake__c selectedIntake = [
            SELECT Id
            FROM Intake__c
            WHERE Incident__c = :ScheduleInvestigatorCtlrTest.TheIncident.Id
            LIMIT 1
        ];

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ApexPages.currentPage().getParameters().put('srcId', selectedIntake.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
        PageReference pr = controller.cancelSchedule();

        Test.stopTest();

        System.assertNotEquals(null, pr);
        System.assertEquals('/' + selectedIntake.Id, pr.getUrl());
    }

    @isTest
    private static void verifyCatastrophicIdentifier() {
        ScheduleInvestigatorCtlrTest.loadCommonData();
        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();
        String result = controller.getCaseTypeSelectOptionCatastrophicIdentifier();

        Test.stopTest();

        System.assertEquals(ScheduleInvestigatorCtlr.CASETYPE_SELECTOPTION_CATASTROPHIC_FLAG, result);
    }

    @isTest
    private static void verifyStateSelectOptions() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        List<Flow_State_Code__c> expectedStateCodes = Flow_State_Code__c.getAll().values();
        expectedStateCodes.sort();

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        List<SelectOption> result = controller.getStateSelectOptions();

        Test.stopTest();

        System.assertEquals(expectedStateCodes.size(), result.size());
        for (Integer i = 0; i < expectedStateCodes.size(); ++i) {
            SelectOption resultOption = result.get(i);
            Flow_State_Code__c expectedStateCode = expectedStateCodes.get(i);

            System.assertEquals(expectedStateCode.Name, resultOption.getLabel());
            System.assertEquals(expectedStateCode.State_Abbreviation__c, resultOption.getValue());
        }
    }

    @isTest
    private static void verifyLanguageSelectOptions() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        List<Schema.PicklistEntry> expectedValues = User.Language__c.getDescribe().getPicklistValues();

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        List<SelectOption> result = controller.getLanguageChoices();

        Test.stopTest();

        System.assertEquals(expectedValues.size(), result.size());
        for (Integer i = 0; i < expectedValues.size(); ++i) {
            SelectOption resultOption = result.get(i);
            Schema.PicklistEntry expectedValue = expectedValues.get(i);

            System.assertEquals(expectedValue.getLabel(), resultOption.getLabel());
            System.assertEquals(expectedValue.getValue(), resultOption.getValue());
        }
    }

    @isTest
    private static void verifySkillSelectOptions() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        List<Schema.PicklistEntry> expectedValues = User.Skill__c.getDescribe().getPicklistValues();

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        List<SelectOption> result = controller.skillChoices;

        Test.stopTest();

        System.assertEquals(expectedValues.size(), result.size());
        for (Integer i = 0; i < expectedValues.size(); ++i) {
            SelectOption resultOption = result.get(i);
            Schema.PicklistEntry expectedValue = expectedValues.get(i);

            System.assertEquals(expectedValue.getLabel(), resultOption.getLabel());
            System.assertEquals(expectedValue.getValue(), resultOption.getValue());
        }
    }

    @isTest
    private static void verifyReasonsForDelayedSignupSelectOptions() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        List<Schema.PicklistEntry> expectedValues = Event.Reason_for_Delayed_Signup__c.getDescribe().getPicklistValues();

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        List<SelectOption> result = controller.getReasonsForDelayedSignupList();

        Test.stopTest();

        System.assertEquals(expectedValues.size() + 1, result.size());
        System.assertEquals('--None--', result.get(0).getLabel());
        System.assertEquals('', result.get(0).getValue());
        for (Integer i = 0; i < expectedValues.size(); ++i) {
            SelectOption resultOption = result.get(i + 1);
            Schema.PicklistEntry expectedValue = expectedValues.get(i);

            System.assertEquals(expectedValue.getLabel(), resultOption.getLabel());
            System.assertEquals(expectedValue.getValue(), resultOption.getValue());
        }
    }

    @isTest
    private static void getCityOptions_AddressesFound() {
        String stateCode = TestUtil.DefaultPersonAccountBillingState;
        List<Address_by_Zip_Code__c> expectedAddresses = [
                SELECT City__c, Flow_Label__c
                FROM Address_by_Zip_Code__c
                WHERE RecordType.DeveloperName = 'Unique_City'
                    AND State_Code__c = :stateCode
                    AND City__c != null
                    AND Flow_Label__c != null
                ORDER BY Flow_Label__c ASC
        ];

        Test.startTest();
        List<ScheduleInvestigatorCtlr.SchedulerSelectOption> result = ScheduleInvestigatorCtlr.getCityOptions(stateCode);
        Test.stopTest();

        System.assertEquals(expectedAddresses.size() + 1, result.size());

        System.assertEquals('', result.get(0).value);
        System.assertEquals('--None--', result.get(0).label);

        for (Integer i = 0; i < expectedAddresses.size(); ++i) {
            Address_by_Zip_Code__c address = expectedAddresses.get(i);
            ScheduleInvestigatorCtlr.SchedulerSelectOption opt = result.get(i + 1);

            System.assertEquals(address.City__c, opt.value);
            System.assertEquals(address.Flow_Label__c, opt.label);
        }
    }

    @isTest
    private static void getCityOptions_NoAddressesFound() {
        String stateCode = 'INVALID STATE CODE';

        Test.startTest();
        List<ScheduleInvestigatorCtlr.SchedulerSelectOption> result = ScheduleInvestigatorCtlr.getCityOptions(stateCode);
        Test.stopTest();

        System.assertEquals(1, result.size());

        System.assertEquals('', result.get(0).value);
        System.assertEquals('--None--', result.get(0).label);
    }

    @isTest
    private static void validateCatastrophicResponseSpeed_Success() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        ScheduleInvestigatorSettings__c schedulerSettings = ScheduleInvestigatorSettings__c.getInstance();
        schedulerSettings.Delayed_Signup_Reason_Time__c = 32;
        Database.upsert(schedulerSettings);

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
        controller.scheduledDate = Date.today().addDays(1).format();
        controller.isLanguagesEnabled = true;
        controller.isLocationEnabled = false;
        controller.isSkillEnabled = true;
        controller.selectedLanguage = new List<String>{ ScheduleInvestigatorCtlrTest.Language1 };
        controller.filterInvestigators();
        List<Id> idList = new List<Id>(controller.territoryUsersAvailableSlots.keyset());
        controller.selectedSlot = idList.get(0) + '-0';
        controller.incidentInstance.attendees.get(0).selected = true;

        System.assert([SELECT Catastrophic__c FROM Intake__c WHERE Id = :controller.incidentInstance.attendees.get(0).intakes.get(0).intakeId].Catastrophic__c, 'Selected intake is not catastrophic');

        controller.reasonForDelayedSignup = null;

        PageReference pageRef = controller.saveEvent();

        Test.stopTest();

        System.assertNotEquals(null, pageRef);
        System.assertEquals('/' + ScheduleInvestigatorCtlrTest.TheIncident.Id, pageRef.getUrl());

        System.assert(!ApexPages.hasMessages());
    }

    @isTest
    private static void validateCatastrophicResponseSpeed_Failure() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        ScheduleInvestigatorSettings__c schedulerSettings = ScheduleInvestigatorSettings__c.getInstance();
        schedulerSettings.Delayed_Signup_Reason_Time__c = 32;
        Database.upsert(schedulerSettings);

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        controller.selectedLocation = controller.investigationLocationOptions.get(1).getValue();
        controller.scheduledDate = Date.today().addDays(3).format();
        controller.isLanguagesEnabled = true;
        controller.isLocationEnabled = false;
        controller.isSkillEnabled = true;
        controller.selectedLanguage = new List<String>{ ScheduleInvestigatorCtlrTest.Language1 };
        controller.filterInvestigators();
        List<Id> idList = new List<Id>(controller.territoryUsersAvailableSlots.keyset());
        controller.selectedSlot = idList.get(0) + '-0';
        controller.incidentInstance.attendees.get(0).selected = true;

        System.assert([SELECT Catastrophic__c FROM Intake__c WHERE Id = :controller.incidentInstance.attendees.get(0).intakes.get(0).intakeId].Catastrophic__c, 'Selected intake is not catastrophic');

        controller.reasonForDelayedSignup = null;

        PageReference pageRef = controller.saveEvent();

        Test.stopTest();

        System.assertEquals(null, pageRef);

        System.assertEquals(1, ApexPages.getMessages().size());
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages().get(0).getSeverity());
        System.assertEquals('No Reason For Delayed Signup Selected. You must select at least one reason for delayed signup to schedule an investigation', ApexPages.getMessages().get(0).getSummary());
    }

    @isTest
    private static void languageSelectionWasDefaulted() {
        List<String> expectedDefaultLanguages = new List<String>{ 'English' };

        ScheduleInvestigatorCtlrTest.loadCommonData();

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        Test.stopTest();

        System.assertEquals(expectedDefaultLanguages, controller.selectedLanguage);
    }

    @isTest
    private static void addressOptionsAreNotDuplicated() {
        ScheduleInvestigatorCtlrTest.loadCommonData();

        // This will result in a seperate Attendee wrapper record being created for this client
        Intake__c intakeToDuplicate = [SELECT Id, Incident__c, Client__c, Injured_Party__c, Caller__c FROM Intake__c WHERE Incident__c = :ScheduleInvestigatorCtlrTest.TheIncident.Id LIMIT 1];
        Database.insert(ScheduleInvestigatorCtlrTest.CreateTestIntake(intakeToDuplicate.Incident__c, intakeToDuplicate.Caller__c, intakeToDuplicate.Client__c, intakeToDuplicate.Injured_Party__c));

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', ScheduleInvestigatorCtlrTest.TheIncident.Id);
        ScheduleInvestigatorCtlr controller = new ScheduleInvestigatorCtlr();

        Test.stopTest();

        // expecting 3 wrappers, 2 unique clients
        System.assertEquals(3, controller.incidentInstance.attendees.size());

        Set<Id> uniqueClients = new Set<Id>();
        for (ScheduleInvestigatorCtlr.Attendee attendee : controller.incidentInstance.attendees) {
            uniqueClients.add(attendee.clientId);
        }
        System.assertEquals(2, uniqueClients.size());

        System.assertNotEquals(0, controller.investigationLocationOptions.size());

        Set<String> uniqueOptionValues = new Set<String>();
        for (SelectOption opt : controller.investigationLocationOptions) {
            System.assert(uniqueOptionValues.add(opt.getValue()));
        }
    }

    private static Event CreateHomeEvent(Id ownerId, DateTime startTime, DateTime endTime){
        return new Event(
            Subject = 'Home',
            StartDateTime = startTime,
            EndDateTime = endTime,
            IsAllDayEvent = false,
            OwnerId = ownerId);
    }

    private static Intake__c CreateTestIntake(Id incidentId, Id callerId, Id clientId, Id injuredPartyId){
        return new Intake__c(
            Caller__c = callerId,
            Client__c = clientId,
            Incident__c = incidentId,
            Injured_Party__c = injuredPartyId,
            Case_Type__c = IntakeCaseType,
            What_was_the_date_of_the_incident__c = Date.today(),
            Caller_First_Name__c = 'Test',
            Caller_Last_Name__c = 'User',
            Representative_Reason__c = 'Test Reason',
            Catastrophic__c = true,
            Type_of_Injury__c = IntakeTypeOfInjury);
    }
}