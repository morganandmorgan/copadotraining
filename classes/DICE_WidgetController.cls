public with sharing class DICE_WidgetController {
  public List<SelectOption> QueueList { get; set; }
  public String Message { get; set; }
  public String SelectedQueueId { get; set; }
  public String NextId { get; set; }
  public Boolean IsAdmin { get; set; }
  private static string CookieName = 'IntakeRepNavigatorWidget_SelectedQueueId';

  public PageReference OnLoad() {
    Schema.DescribeSObjectResult objDescription = DICE_Queue__c.sObjectType.getDescribe();
    Map<String, Boolean> fieldMap = new Map<String, Boolean>();
    for (Schema.SobjectField f : objDescription.fields.getMap().Values()) {
      Schema.DescribeFieldResult dfr = f.getDescribe();
      fieldMap.put(dfr.getName().toLowerCase(), dfr.isUpdateable());
    }

    isAdmin = fieldMap.get('query__c') && fieldMap.get('sorting__c') && fieldMap.get('name') && fieldMap.get('earliestcalltime__c') && fieldMap.get('latestcalltime__c');

    List<DICE_Queue__c> queues = [SELECT Id, Name FROM DICE_Queue__c];

    QueueList = new List<SelectOption>();
    for (DICE_Queue__c q : queues) {
      QueueList.add(new SelectOption(q.Id, q.Name));
    }

    // Set SelectedQueueId
    Cookie cookie = ApexPages.currentPage().getCookies().get(CookieName);
    if (cookie != null) {
        SelectedQueueId = cookie.getValue();      
    }

    return null;
  }

  public PageReference goToNext() {
    Message = '';

    if (String.isEmpty(SelectedQueueId)) {
      Message = 'No list selected';
      return null;
    }
    // write cookie
    Cookie cook = new Cookie(CookieName, SelectedQueueId, null, -1, false);
    ApexPages.currentPage().setCookies(new Cookie[]{cook});
    DiceService service = new DiceService();
    Intake__c intake = service.GetNextIntake((ID)SelectedQueueId);
    System.debug('intake found: ' + intake);
      
    if (intake == null) {
      Message = 'This list is complete!';
      return null;
    }

    Message = 'Please wait...';

    NextId = intake.Client__c;

    return null;
  }
}