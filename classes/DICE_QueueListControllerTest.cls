@isTest
private class DICE_QueueListControllerTest {
  static DICE_Queue__c testQueue;
  static {
    testQueue = new DICE_Queue__c(Name='Test Queue');
    insert testQueue;

    PageReference PageRef = Page.DICE_QueueList;
    Test.setCurrentPage(PageRef);
  }

	@isTest static void SelectsNameAndIdFromQueues() {
		DICE_QueueListController controller = new DICE_QueueListController();
    System.assertEquals(1, controller.Queues.size());
    System.assertEquals(testQueue.id, controller.Queues[0].Id);
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}