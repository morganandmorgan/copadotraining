/**
 *  mmmarketing_IFinancialPeriodsSelector
 */
public interface mmmarketing_IFinancialPeriodsSelector extends mmlib_ISObjectSelector
{
    List<Marketing_Financial_Period__c> selectById( Set<Id> idSet );
    List<Marketing_Financial_Period__c> selectWhereCampaignNotLinked();
    List<Marketing_Financial_Period__c> selectOneWhereCampaignNotLinked();
    List<Marketing_Financial_Period__c> selectWhereSourceNotLinked();
    List<Marketing_Financial_Period__c> selectOneWhereSourceNotLinked();
    List<Marketing_Financial_Period__c> selectWhereMarketingSourceNotLinkedBySourceValue( Set<String> sourceValueSet );
    Database.QueryLocator selectQueryLocatorWhereMarketingCampaignNotLinked();
    Database.QueryLocator selectQueryLocatorWhereMarketingSourceNotLinkedBySourceValue( Set<String> sourceValueSet );
    List<Marketing_Financial_Period__c> selectByCompositeParam( mmmarketing_FinancialPeriodsSelectParam params );

}