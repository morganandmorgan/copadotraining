/**
 *  mmtwilio_ListCallsCallout
 *
 *  @usage mmtwilio_ListCallsCallout callout = new mmtwilio_ListCallsCallout();
 *  @usage callout.setTwilioAccountID( 'AC223a3cdfb8b2afda02486bcfd9357e7f' ).setAuthorization( (new mmtwilio_APIAccessNamedCredAuthorization()).setIdentifier( 'Twilio_Master' ) );
 *  @usage callout.setQueryStartDate( date.newinstance(2017, 5, 6) );
 *  @usage callout.setQueryEndDate( date.newinstance(2017, 5, 6) );
 *  @usage callout.setCallerNumber('5132031926');
 *  @usage callout.execute();
 *  @usage system.debug( callout.getResponse() ) ;
 *
 */
public class mmtwilio_ListCallsCallout
    extends mmtwilio_BaseGETV20170401Callout
{
    private mmtwilio_ListCallsCallout.Response resp = new mmtwilio_ListCallsCallout.Response();
    private string twilioAccountId = null;
    private map<string, string> paramMap = new map<string, string>();

    public mmtwilio_ListCallsCallout()
    {
        paramMap.put('Status', 'completed');
    }

    public override map<string, string> getParamMap()
    {
        return this.paramMap;
    }

    public override string getPathSuffix()
    {
        return '/Accounts/' + twilioAccountId + '/Calls.json';
    }

    public override mmlib_BaseCallout execute()
    {
        validateCallout();

        httpResponse httpResp = super.executeCallout();

        string jsonResponseBody = httpResp.getBody();

        if ( this.isDebugOn )
        {
            system.debug( json.serializePretty( json.deserializeUntyped( jsonResponseBody ) ) );
        }

        resp = (mmtwilio_ListCallsCallout.Response) json.deserialize( jsonResponseBody, mmtwilio_ListCallsCallout.Response.class );

        return this;
    }

    public class Response
        implements CalloutResponse
    {
        public list<ResponseCall> calls = new list<ResponseCall>();

        public String first_page_uri;
        public String endx;
        public String previous_page_uri;
        public String uri;
        public String page_size;
        public String start;
        public String next_page_uri;
        public String page;

        public integer getTotalNumberOfRecordsFound()
        {
            return this.calls.size();
        }

        public list<mmtwilio_ListCallsCallout.ResponseCall> getTwilioCalls()
        {
            return this.calls;
        }
    }

    public class ResponseCall
    {
        private Datetime startTimeAsDT = null;

        public String sid;
        public String date_created;
        public String date_updated;
        public String parent_call_sid;
        public String account_sid;
        public String to;
        public String to_formatted;
        public String fromx;
        public String from_formatted;
        public String phone_number_sid;
        public String status;
        public String start_time;
        public String end_time;
        public String duration;
        public String price;
        public String price_unit;
        public String direction;
        public String forwarded_from;

        public Datetime getCalledAtDatetime()
        {
            if ( this.startTimeAsDT == null
                && string.isNotBlank(this.start_time) )
            {
                try
                {
                    // Twilio's start_time is formated in the following pattern "EEE, dd MMM yyyy HH:mm:ss Z"  i.e. "Sat, 06 May 2017 13:46:13 +0000"
                    this.startTimeAsDT = mmlib_Utils.formatDatetimeFromLongFormatString( this.start_time );
                }
                catch (Exception e)
                {
                    system.debug(logginglevel.warn, 'Unable to convert \''+this.start_time+'\' to a datetime value.  Exception: ' + e.getMessage());
                }
            }

            return this.startTimeAsDT;
        }
    }

    private void validateCallout()
    {
    }

    public override CalloutResponse getResponse()
    {
        return this.resp;
    }

    public mmtwilio_ListCallsCallout setTwilioAccountID( String twilioAccountId )
    {
        this.twilioAccountId = twilioAccountId;
        return this;
    }

    public mmtwilio_ListCallsCallout setQueryStartDate( Date startDate )
    {
        paramMap.put('StartTime', string.valueOf( startDate ));
        return this;
    }

    public mmtwilio_ListCallsCallout setQueryEndDate( Date endDate )
    {
        paramMap.put('EndTime', string.valueOf( endDate ));
        return this;
    }

    public mmtwilio_ListCallsCallout setCallStatus( String status )
    {
        paramMap.put('Status', string.valueOf( status ));
        return this;
    }

    public mmtwilio_ListCallsCallout setCallerNumber( String callerNumber )
    {
        // the number needs to be a full phone number including the "+1"
        system.debug( callerNumber );

        if ( string.isNotBlank(callerNumber)
            && callerNumber.length() > 8
            )
        {
            String workingNumber = mmlib_Utils.stripPhoneNumber( callerNumber );

            system.debug( workingNumber );

            if ( workingNumber.length() == 10 )
            {
                // the number does not have the country code of "+1"
                workingNumber = '+1' + workingNumber;
            }
            else if ( ! workingNumber.startsWith('+') )
            {
                workingNumber = '+' + workingNumber;
            }

            paramMap.put('From', workingNumber);
        }

        return this;
    }

    public class ParameterException extends Exception {  }
}