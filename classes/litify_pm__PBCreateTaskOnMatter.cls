/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBCreateTaskOnMatter {
    global PBCreateTaskOnMatter() {

    }
    @InvocableMethod(label='Create Task On Matter' description='Creates new tasks on the given matters.')
    global static void createMatterFieldTasks(List<litify_pm__Matter__c> matters) {

    }
}
