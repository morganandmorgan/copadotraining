public with sharing class PaymentProcessingStatusDataAccess {

    public PaymentProcessingStatusDataAccess() {
    }

    public List<Payment_Processing_Batch_Status__c> getPaymentProcessTrackingRecords(Id paymentCollectionId){
        //Data Access method
        //Payment-level tracking records.
        List<Payment_Processing_Batch_Status__c> trackingRecords=null;
        
        trackingRecords= [
            
            select 
            Id, 
            Name, 
            Payment_Collection__c, 
            Payment_Collection__r.Payment_Method__c, 
            Payment_Collection__r.Name, 

            Payment__c, 
            Payment__r.c2g__Status__c, 
            Payment__r.Name,
            
            Are_Proposal_Lines_Added__c, 
            Is_Media_Data_Created__c, 
            Are_Check_Numbers_Updated__c, 
            Is_Posted_and_Matched__c, 
            
            Post_Job_Id__c, 
            
            Has_Post_and_Match_Error__c, 
            
            Has_Error__c, 
            Last_Error_Message__c, 
            Message__c, 

            Update_Check_Numbers_Job_Id__c,
            Has_Update_Check_Numbers_Error__c,
            Update_Check_Numbers_Last_Error_Message__c,
            
            Ordinal__c
            
            from Payment_Processing_Batch_Status__c 
            where 
            Payment_Collection__c = :paymentCollectionId
            
            order by Ordinal__c, Payment__c             
            
        ];
        
            // Are_Proposal_Lines_Added__c = true  
            // and Is_Media_Data_Created__c = true 
            // and 
            // (
            //     Are_Check_Numbers_Updated__c = true 
            //     or Payment_Collection__r.Payment_Method__c != 'Check'

            // )
            // and Is_Posted_and_Matched__c != true

        return trackingRecords;
        
    }

public List<Payment_Process_Add_To_Proposal_Status__c> getAddToProposalTrackingRecords(Id paymentCollectionId){
        //Data Access method
        //Payment-level tracking records.
        List<Payment_Process_Add_To_Proposal_Status__c> trackingRecords=null;
        
        trackingRecords= [
            
select   
Id  
,Name   
,Batch_Id__c   
,Message__c   
,Last_Error_Message__c   
,Has_Error__c   
  
,Payment_Collection__c   
,Payment_Collection__r.Name   
,Payment__c   
,Payment__r.c2g__Status__c   
,Payment__r.Name   
,Transaction_Line_Item__c   
,Transaction_Line_Item__r.Name   
  
,Is_Added_To_Proposal__c   
,Payment_Log__c   

from Payment_Process_Add_To_Proposal_Status__c   
where   
Is_Added_To_Proposal__c != true   
and Payment__r.c2g__Status__c in ('New', 'Proposed')   
and Payment_Collection__c = :paymentCollectionId          
            
        ];

        return trackingRecords;
        
    }    
    
    //-----------------------
        
    
    public List<Payment_Processing_Batch_Status__c> getPendingPostAndMatchTrackingRecords(Id paymentCollectionId){
        //Data Access method
        //Payment-level tracking records.
        List<Payment_Processing_Batch_Status__c> trackingRecords=null;
        
        trackingRecords= [
            
            select 
            Id, 
            Name, 
            Payment_Collection__c, 
            Payment_Collection__r.Payment_Method__c, 

            Payment__c, 
            Payment__r.c2g__Status__c, 
            
            Are_Proposal_Lines_Added__c, 
            Is_Media_Data_Created__c, 
            Are_Check_Numbers_Updated__c, 
            Is_Posted_and_Matched__c, 
            
            Post_Job_Id__c, 
            
            Has_Post_and_Match_Error__c, 
            
            Has_Error__c, 
            Last_Error_Message__c, 
            Message__c, 

            Update_Check_Numbers_Job_Id__c,
            Has_Update_Check_Numbers_Error__c,
            Update_Check_Numbers_Last_Error_Message__c,
            
            Ordinal__c
            
            from Payment_Processing_Batch_Status__c 
            where 
            Are_Proposal_Lines_Added__c = true  
            and Is_Media_Data_Created__c = true 
            and 
            (
                Are_Check_Numbers_Updated__c = true 
                or Payment_Collection__r.Payment_Method__c != 'Check'

            )
            and Is_Posted_and_Matched__c != true
             
            and Payment_Collection__c = :paymentCollectionId
            
            order by Ordinal__c, Payment__c     
            
        ];
        
        return trackingRecords;
        
    }

    public List<Payment_Processing_Batch_Status__c> getAllPaymentTrackingRecords(Id paymentId){
        //Data Access method
        //Payment-level tracking records.
        List<Payment_Processing_Batch_Status__c> trackingRecords=null;
        
        trackingRecords= [
            
            select 
            Id, 
            Name, 
            Payment_Collection__c, 
            Payment_Collection__r.Payment_Method__c, 

            Payment__c, 
            Payment__r.c2g__Status__c, 
            Payment__r.Name, 
            
            Are_Proposal_Lines_Added__c, 
            Is_Media_Data_Created__c, 
            Are_Check_Numbers_Updated__c, 
            Is_Posted_and_Matched__c, 
            
            Post_Job_Id__c, 
            
            Has_Post_and_Match_Error__c, 
            Post_and_Match_Last_Error_Message__c,
            
            Has_Error__c, 
            Last_Error_Message__c, 
            Message__c, 

            Update_Check_Numbers_Job_Id__c,
            Has_Update_Check_Numbers_Error__c,
            Update_Check_Numbers_Last_Error_Message__c,
                                    
            Ordinal__c
            
            from Payment_Processing_Batch_Status__c 

            where 
            Payment__c = :paymentId
            
            order by Ordinal__c, Payment__c        
            
        ];
        
        return trackingRecords;
        
    }    

    public List<Payment_Processing_Batch_Status__c> getAllPaymentCollectionTrackingRecords(Id paymentCollectionId){
        //Data Access method
        //Payment-level tracking records.
        List<Payment_Processing_Batch_Status__c> trackingRecords=null;
        
        trackingRecords= [
            
            select 
            Id, 
            Name, 
            Payment_Collection__c, 
            Payment_Collection__r.Payment_Method__c, 

            Payment__c, 
            Payment__r.c2g__Status__c,
            Payment__r.Name, 
            
            Are_Proposal_Lines_Added__c, 
            Is_Media_Data_Created__c, 
            Are_Check_Numbers_Updated__c, 
            Is_Posted_and_Matched__c, 
            
            Post_Job_Id__c, 
            
            Has_Post_and_Match_Error__c, 
            Post_and_Match_Last_Error_Message__c, 

            Has_Error__c, 
            Last_Error_Message__c, 
            Message__c, 

            Update_Check_Numbers_Job_Id__c,
            Has_Update_Check_Numbers_Error__c,
            Update_Check_Numbers_Last_Error_Message__c,
                                    
            Ordinal__c
            
            from Payment_Processing_Batch_Status__c 

            where 
            Payment_Collection__c = :paymentCollectionId
            
            order by Ordinal__c, Payment__c 
            
        ];
        
        return trackingRecords;
        
    }    

    public List<Payment_Processing_Batch_Status__c> getPendingPaymentPostAndMatchTrackingRecords(Id paymentId){
        //Data Access method
        List<Payment_Processing_Batch_Status__c> trackingRecords=null;
        
        trackingRecords= [
            
            select 
            Id, 
            Name, 
            Payment_Collection__c, 
            Payment__c, 
            Payment__r.c2g__Status__c, 
            
            Are_Proposal_Lines_Added__c, 
            Is_Media_Data_Created__c, 
            Are_Check_Numbers_Updated__c, 
            Is_Posted_and_Matched__c, 
            
            Post_Job_Id__c, 
            
            Has_Post_and_Match_Error__c, 
            
            Has_Error__c, 
            Last_Error_Message__c, 
            Message__c, 
            
            Ordinal__c
            
            from Payment_Processing_Batch_Status__c 
            where 
            Are_Proposal_Lines_Added__c = true  
            and Is_Media_Data_Created__c = true 
            and 
            (
                Are_Check_Numbers_Updated__c = true 
                or Payment_Collection__r.Payment_Method__c != 'Check' 
            )
            and Is_Posted_and_Matched__c != true
             
            and Payment__c = :paymentId
            
            order by Ordinal__c, Payment__c         
            
        ];
        
        return trackingRecords;
        
    }    

    public Id getPaymentCollectionId(Id paymentId){
        //Data Access method
        Id paymentCollectionId=null;

        c2g__codaPayment__c payment = null;

        List<c2g__codaPayment__c> payments = null;
        
        payments = [
            
            select 
            Payment_Collection__c 
            from  c2g__codaPayment__c 
            where 
            Payment_Collection__c != null      
            and Id = :paymentId     
            
        ];

        if((payments!=null)&&(payments.size()>0)){
            payment=payments[0];
        }

        if(payment!=null){
            paymentCollectionId=payment.Payment_Collection__c;
        }
        
        return paymentCollectionId;
        
    }      
    
    public List<AsyncApexJob> getPostAndMatchApexJobs(List<Id> apexJobIds){
        //Data Access method
        
        List<AsyncApexJob> apexBatchJobs=null;
        
        apexBatchJobs= [    
            
            select 
            Id, 
            ApexClassId, 
            ApexClass.Name, 
            CreatedDate, 
            CompletedDate, 
            CreatedById, 
            Status, 
            ExtendedStatus, 
            NumberOfErrors, 
            JobType, 
            
            JobItemsProcessed, 
            
            LastProcessed, 
            LastProcessedOffset, 
            MethodName, 
            
            ParentJobId, 
            
            TotalJobItems
            from AsyncApexJob 
            where 
            JobType != 'BatchApexWorker'   
            and Id in :apexJobIds
            
        ];
        
        return apexBatchJobs;
        
    }    
    
    public List<AsyncApexJob> getRunningPostAndMatchApexJobs(List<Id> apexJobIds){
        //Data Access method
        
        List<AsyncApexJob> apexBatchJobs=null;
        
        apexBatchJobs= [    
            
            select 
            Id, 
            ApexClassId, 
            ApexClass.Name, 
            CreatedDate, 
            CompletedDate, 
            CreatedById, 
            Status, 
            ExtendedStatus, 
            NumberOfErrors, 
            JobType, 
            
            JobItemsProcessed, 
            
            LastProcessed, 
            LastProcessedOffset, 
            MethodName, 
            
            ParentJobId, 
            
            TotalJobItems
            from AsyncApexJob 
            where 
            JobType != 'BatchApexWorker'   
            and Status not in (
            'Completed'
            ,'Failed'
            ,'Aborted'
            )
            and Id in :apexJobIds
            
        ];
        
        return apexBatchJobs;
        
    }      
    
    public List<AsyncApexJob> getApexJobs(List<Id> apexJobIds){
        //Data Access method
        
        List<AsyncApexJob> apexBatchJobs=null;
        
        apexBatchJobs= [    
            
            select 
            Id, 
            ApexClassId, 
            ApexClass.Name, 
            CreatedDate, 
            CompletedDate, 
            CreatedById, 
            Status, 
            ExtendedStatus, 
            NumberOfErrors, 
            JobType, 
            
            JobItemsProcessed, 
            
            LastProcessed, 
            LastProcessedOffset, 
            MethodName, 
            
            ParentJobId, 
            
            TotalJobItems
            from AsyncApexJob 
            where 
            JobType != 'BatchApexWorker'   
            and Id in :apexJobIds
            
        ];
        
        return apexBatchJobs;
        
    }    

}