global class ffaInterestService 
{
    @AuraEnabled
	public static void calculateInterest(String matterId, String settlementDateString)
    {
        Map<String, Decimal> interestRateMap = new Map<String, Decimal>();


        Date settlementDate = Date.valueOf(settlementDateString);
        System.debug('ffaInterestService.calculateInterest - matterId : ' + matterId);
        System.debug('ffaInterestService.calculateInterest - settlementDate : ' + settlementDate);

        for(Interest_Setting__mdt intSetting : [
            SELECT Interest_Rate__c, Company_Name__c
            FROM Interest_Setting__mdt]){
            interestRateMap.put(intSetting.Company_Name__c, intSetting.Interest_Rate__c);
        }

        litify_pm__Expense_Type__c interestType = null;
        Try{
            interestType = [Select Id FROM litify_pm__Expense_Type__c WHERE Name = 'Interest Expense'];
        }
        Catch (Exception e){
            System.debug('ERROR: No Interest Expense found in the Expense Type table!');
        }

        if (interestType != null){
            List<litify_pm__Expense__c> expenseList = [
                SELECT Id, 
                    litify_pm__Date__c, 
                    litify_pm__Matter__r.AssignedToMMBusiness__r.FFA_Company__r.Name,
                    Amount_Remaining_to_Recover__c,
                    Interest_Calculated__c,
                    Last_Interest_Calculation_Date__c
                FROM litify_pm__Expense__c 
                WHERE litify_pm__ExpenseType2__r.CostType__c = 'HardCost'
                AND litify_pm__Matter__r.AssignedToMMBusiness__r.FFA_Company__r.Name != null
                AND Amount_Remaining_to_Recover__c != null
                AND Amount_Remaining_to_Recover__c != 0
                AND litify_pm__ExpenseType2__r.Exclude_from_Interest_Calc__c = false
                AND litify_pm__Matter__c = :matterId];

            litify_pm__Expense__c interestExpense = new litify_pm__Expense__c(
                litify_pm__ExpenseType2__c = interestType.Id,
                litify_pm__Matter__c = matterId,
                litify_pm__Date__c = Date.today());

            Decimal totalAmountOfInterest = 0.0;
            for (litify_pm__Expense__c expense : expenseList){
                //get the appropriate interst rate:
                String interestKey = expense.litify_pm__Matter__r.AssignedToMMBusiness__r.FFA_Company__r.Name;
                Decimal annaulInterstRate = interestRateMap.containsKey(interestKey) ? interestRateMap.get(interestKey) : interestRateMap.containsKey('Default') ? interestRateMap.get('Default') : 0;
                Decimal monthlyInterestRate = (annaulInterstRate / 100) / 12;

                System.debug('ffaInterestService.calculateInterest - annaulInterstRate : ' + annaulInterstRate);
                System.debug('ffaInterestService.calculateInterest - monthlyInterestRate : ' + monthlyInterestRate);

                // Calculate the amount for this item.
                // First, let's see how many months between the dates on the matter.

                Integer monthsBetween = expense.litify_pm__Date__c.monthsBetween(settlementDate) == 0 ? 0 : expense.litify_pm__Date__c.monthsBetween(settlementDate) + 1;
                if (expense.Last_Interest_Calculation_Date__c != null)
                    monthsBetween = expense.Last_Interest_Calculation_Date__c.monthsBetween(settlementDate);

                System.debug('ffaInterestService.calculateInterest - expense.litify_pm__Date__c : ' + expense.litify_pm__Date__c);
                System.debug('ffaInterestService.calculateInterest - monthsBetween : ' + monthsBetween);

                Decimal interestAmount = ((expense.Amount_Remaining_to_Recover__c * monthlyInterestRate) * monthsBetween).setScale(2);

                expense.Interest_Calculated__c = (expense.Interest_Calculated__c == null || expense.Interest_Calculated__c == 0 ?  interestAmount : expense.Interest_Calculated__c + interestAmount);
                expense.Last_Interest_Calculation_Date__c = settlementDate;
                totalAmountOfInterest += interestAmount;

                System.debug('ffaInterestService.calculateInterest - interestAmount : ' + interestAmount);
                System.debug('ffaInterestService.calculateInterest - totalAmountOfInterest : ' + totalAmountOfInterest);
            }

            if (expenseList.size() > 0 && totalAmountOfInterest > 0){
                interestExpense.litify_pm__Amount__c = totalAmountOfInterest;

                INSERT interestExpense;
                UPDATE expenseList;

                // Todo: Review with Brian.  Does this look ok to you?  It passes a set of matter Ids to the ExpenseService.
                //if(!Test.isRunningTest()){
                //    ExpenseSummaryDomain.createExpenseSummaries(new Set<Id> {matterId});
                //}
            }
        }
    }
}