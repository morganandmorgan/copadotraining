public interface mmmarketing_ICampaigns extends fflib_ISObjectDomain
{
    void autoLinkMarketingTrackingInformationToCampaignIfRequired();
    void autoLinkMarketingFinancialPeriodsToCampaignIfRequired();
}