public interface mmcommon_IExclusionsListsSelector
{
    List<Exclusions_List__mdt> selectByAppAndTargetObject(String app, String sObj);
}