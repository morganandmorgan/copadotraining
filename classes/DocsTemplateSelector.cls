/**
 * DocsTemplateSelector
 * @description Selector for litify_docs__Template__c SObject.
 * @author Matt Terrill
 * @date 1/9/2019
 */
public without sharing class DocsTemplateSelector extends fflib_SObjectSelector {

    private List<Schema.SObjectField> sObjectFields;

    public DocsTemplateSelector() {
        sObjectFields = new List<Schema.SObjectField> {
                litify_docs__Template__c.id,
                litify_docs__Template__c.name,
                litify_docs__Template__c.litify_docs__Category__c,
                litify_docs__Template__c.litify_docs__Has_File__c,
                litify_docs__Template__c.litify_docs__Invocable_Template__c,
                litify_docs__Template__c.litify_docs__Merged_File_Name__c,
                litify_docs__Template__c.litify_docs__Starting_Object__c,
                litify_docs__Template__c.litify_docs__Subcategory__c                   
        };
    } //constructor


    // fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return litify_docs__Template__c.SObjectType;
    } //getSObjectType


    public void addSObjectFields(List<Schema.SObjectField> sObjectFields) {
        if (sObjectFields != null && !sObjectFields.isEmpty()) {
            for (Schema.SObjectField field : sObjectFields) {
                if (!this.sObjectFields.contains(field)) {
                    this.sObjectFields.add(field);
                }
            }
        }
    } //addSObjectFields


    public List<Schema.SObjectField> getSObjectFieldList() {
        return this.sObjectFields;
    } //getSObjectFieldList


    public List<litify_docs__Template__c> selectById(Set<Id> idSet) {
        //we are building this by hand so it gets run in this class, which is 'without sharing' and not fflib_SObjectSelector, 'with sharing'
        fflib_QueryFactory query = newQueryFactory();

        query.setCondition('id in :idSet');

        return Database.query(query.toSOQL());
    } //selectById   

} //class