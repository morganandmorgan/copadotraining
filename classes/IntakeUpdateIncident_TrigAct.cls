public class IntakeUpdateIncident_TrigAct extends TriggerAction {

    private final Set<Id> incidentsToUpdate = new Set<Id>();

	public IntakeUpdateIncident_TrigAct() {
		super();
	}

    public override Boolean shouldRun() {
        incidentsToUpdate.clear();
        if (isAfter() && (isInsert() || isDelete())) {
            Map<Id, Intake__c> intakeMap = (Map<Id, Intake__c>) triggerMap;
            for (Intake__c intake : intakeMap.values()) {
                if (intake.Incident__c != null) {
                    incidentsToUpdate.add(intake.Incident__c);
                }
            }
        }
        else if (isAfter() && isUpdate()) {
            Map<Id, Intake__c> intakeNewMap = (Map<Id, Intake__c>) triggerMap;
            Map<Id, Intake__c> intakeOldMap = (Map<Id, Intake__c>) triggerOldMap;
            for (Intake__c intake : intakeNewMap.values()) {
                Intake__c oldIntake = intakeOldMap.get(intake.Id);
                if (intake.Incident__c != oldIntake.Incident__c) {
                    if (intake.Incident__c != null) {
                        incidentsToUpdate.add(intake.Incident__c);
                    }
                    if (oldIntake.Incident__c != null) {
                        incidentsToUpdate.add(oldIntake.Incident__c);
                    }
                }
                else if (intake.Incident__c != null && intake.Client__c != oldIntake.Client__c) {
                    incidentsToUpdate.add(intake.Incident__c);
                }
            }
        }
        return !incidentsToUpdate.isEmpty();
    }

    public override void doAction() {
        IncidentService.updateIncidentLabels(incidentsToUpdate);
    }
}