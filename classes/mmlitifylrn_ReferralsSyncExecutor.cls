/**
 *  script used to manage the cost based expense migration from x_CPExpenseStaging__c to litify_pm__Expense__c records related to litify_pm__Matter__c records
 *  TODO: UPDATE THIS COMMENT
 *
 *  @usage String query = '<<query goes here>>';
 *         new mmlib_GenericBatch( mmlitifylrn_ReferralsSyncExecutor.class, query).setBatchSizeTo(100).execute();
 */
public with sharing class mmlitifylrn_ReferralsSyncExecutor
        implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public mmlitifylrn_ReferralsSyncExecutor() { }

    public void run(List<litify_pm__Referral__c> scope)
    {
        System.debug('TPR:::Executor called with ' + scope.size() + ' referrals.');
        Set<Double> externalIds = new Set<Double>();
        for(litify_pm__Referral__c referral : scope)
        {
            externalIds.add(referral.litify_pm__ExternalId__c);
        }

        List<litify_pm__Referral__c> referrals = mmlitifylrn_ReferralsSelector.newInstance().selectWithFirmAndCaseTypeAndTransactionsByExternalId(externalIds);

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        litify_pm__Firm__c firm = new litify_pm__Firm__c(Name = referrals[0].litify_pm__Originating_Firm__r.Name);
        Set<String> firmSet = new Set<String>();
        firmSet.add(firm.Name);
        mmlitifylrn_ICredentials currentCreds = mmlitifylrn_Credentials.newInstance(mmlitifylrn_CredentialsSelector.newInstance().selectByName(firmSet));
        mmlitifylrn_CalloutAuthorization auth = new mmlitifylrn_CalloutAuthorization().setCrendentials(currentCreds).setCurrentFirmMakingCallout(firm);
        mmlitifylrn_ReferralIntakesGetCallout callout = (mmlitifylrn_ReferralIntakesGetCallout) new mmlitifylrn_ReferralIntakesGetCallout().setAuthorization(auth);

        for (litify_pm__Referral__c referral : referrals)
        {
            System.debug('Processing referral ' + referral.Id + ' for ' + referral.litify_pm__Client_First_Name__c + ' ' + referral.litify_pm__Client_Last_Name__c);
            mmlitifylrn_ReferralIntakesGetCallout.Response resp = (mmlitifylrn_ReferralIntakesGetCallout.Response) callout
                    .setLRNExternalId(String.valueOf(referral.litify_pm__ExternalId__c))
                    .execute()
                    .getResponse();
            System.debug('TPR:::Called Litify with external id of ' + String.valueOf(referral.litify_pm__ExternalId__c));
            resp.referral.addToSObject(referral);

            // Update the existing transaction(s)
            //System.debug('    Found ' + resp.referral.referrals.size() + ' transaction(s) on the returned referral.');
            for (mmlitifylrn_LRNModels.ReferralBasicData trans : resp.referral.referrals)
            {
                litify_pm__Referral_Transaction__c existingTrans = getReferralTransactionByExternalId(referrals, Decimal.valueOf(trans.id));
                if(existingTrans != null)
                {
                    System.debug('        Updating existing transaction for ' +  trans.id);
                    trans.addToSObject(existingTrans);
                    uow.registerDirty(existingTrans);
                }
                else
                {
                    System.debug('        Creating a new transaction for ' + trans.id);
                    litify_pm__Referral_Transaction__c newTrans = trans.getTransactionSObject();
                    uow.register(newTrans);
                    uow.registerRelationship(newTrans, litify_pm__Referral_Transaction__c.litify_pm__Referral__c, referral);
                }
            }
        }

        uow.register(referrals);
        uow.commitWork();
    }

    private litify_pm__Referral_Transaction__c getReferralTransactionByExternalId(List<litify_pm__Referral__c> referrals, Decimal litifyTransactionId)
    {
        litify_pm__Referral_Transaction__c existingTransaction = null;
        for (litify_pm__Referral__c referral : referrals)
        {
            for (litify_pm__Referral_Transaction__c referralTransaction : referral.litify_pm__Referral_Transactions__r)
            {
                if (referralTransaction.litify_pm__ExternalId__c == litifyTransactionId)
                {
                    return referralTransaction;
                }
            }
        }

        return null;
    }
}