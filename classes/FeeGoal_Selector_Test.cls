/**
 * FeeGoal_Selector_Test
 * @description Test from Fee Goal Selector class.
 * @author Jeff Watson
 * @date 2/13/2019
 */
@IsTest
public with sharing class FeeGoal_Selector_Test {

    private static Fee_Goal__c feeGoal;

    static {
        feeGoal = TestUtil.createFeeGoal();
        insert feeGoal;
    }

    @IsTest
    private static void ctor() {
        FeeGoal_Selector feeGoalSelector = new FeeGoal_Selector();
        System.assert(feeGoalSelector != null);
    }

    @IsTest
    private static void selectByIds() {
        FeeGoal_Selector feeGoalSelector = new FeeGoal_Selector();
        List<Fee_Goal__c> feeGoals = feeGoalSelector.selectById(new Set<Id> {feeGoal.Id});
        System.assert(feeGoals != null);
        System.assertEquals(1, feeGoals.size());
    }
}