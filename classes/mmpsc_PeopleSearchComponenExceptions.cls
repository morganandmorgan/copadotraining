public with sharing class mmpsc_PeopleSearchComponenExceptions
{
	private mmpsc_PeopleSearchComponenExceptions() { }

    public class SettingsException extends Exception { }
}