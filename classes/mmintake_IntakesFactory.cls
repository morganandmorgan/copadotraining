/**
 *  mmintake_IntakesFactory
 */
public class mmintake_IntakesFactory
{
    private Intake__c newIntake = null;

    private Map<String, String> hubSpotFieldsMap = new Map<String,String>();
    //private Map<String, Schema.SoapType> allIntakeFieldTypes = new Map<String, Schema.SoapType>();
    private Map<String, Schema.SobjectField> allIntakeFieldTypes = Intake__c.sObjectType.getDescribe().fields.getMap();
    private fflib_ISObjectUnitOfWork uow = null;
    private mmmarketing_TrackingInfoFactory.ITrackingInfoFactory marketingTrackingInfoFactory = null;
    private Account client = null;
    private boolean isClientTheCaller = false;
    private boolean isClientTheInjuredParty = false;
    private Account injuredParty = null;
    private Account caller = null;
    private map<Schema.sObjectField, string> fieldParameterMap = new map<Schema.sObjectField, string>();


    public mmintake_IntakesFactory()
    {
    }

    public mmintake_IntakesFactory( fflib_ISObjectUnitOfWork uow )
    {
        this();

        this.uow = uow;
    }

    private void primeHubspotFieldsMap()
    {
        if (hubSpotFieldsMap.isEmpty())
        {
            for (Hubspot_Field_Mapping__c entry : Hubspot_Field_Mapping__c.getAll().values())
            {
                hubSpotFieldsMap.put(entry.hubspot_field_label__c, entry.intake_field_api_name__c);
            }
        }
    }

    private mmmarketing_TrackingInfoFactory.ITrackingInfoFactory getMarketingTrackingInfoFactoryInstance()
    {
        if (marketingTrackingInfoFactory == null)
        {
            this.marketingTrackingInfoFactory = mmmarketing_TrackingInfoFactory.getInstance( uow );
        }
        return this.marketingTrackingInfoFactory;
    }

    /*
     *  Any custom mapping fields from HubSpot to the Intake object are dealt with here.
     *
     *  Also, if there are any marketing tracking information, it will be sorted and dispatched
     *  from this point over to the Marketing Tracking Info Factory
     *
     *
     */
    private void processCustomFieldDataFromHubSpot( Intake__c newIntake, Map<String, String> hubSpotInfo )
    {
        primeHubspotFieldsMap();

        Schema.DescribeFieldResult dfr = null;

        for (String fieldName : hubSpotInfo.keySet())
        {
            String fieldValue = hubSpotInfo.get(fieldName);

            if (hubSpotFieldsMap.containskey( fieldName )
               && allIntakeFieldTypes.containsKey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) )
            {
                dfr = allIntakeFieldTypes.get(hubSpotFieldsMap.get( fieldName ).toLowerCase()).getDescribe();

                Schema.SoapType typ = dfr.getSoapType();

                // Ensure that String field values do not exceed the available space.
                if (String.isNotBlank(fieldValue)
                    && typ != null
                    && Schema.SoapType.String == typ
                    && fieldValue.length() > dfr.getLength()
                    )
                {
                    fieldValue = fieldValue.left( dfr.getLength() );
                }

                Object objValue = mmlib_Utils.convertValToType(typ, fieldValue);

                newIntake.put( hubSpotFieldsMap.get( fieldName ), objValue);
            }
        }
    }

    // TODO: Eventually refactor this to make use of the new functions available in the class
    public map<id, Intake__c> generateNewFromHubSpotInfo(list<GenerateNewFromHubSpotInfoRequest> requests)
    {
        map<id, Intake__c> intakesByRelatedTaskIdMap = new map<id, Intake__c>();

        set<id> accountIdSet = new set<id>();

        id intakePlaceHolderRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Intake__c.SobjectType, 'PlaceHolder');

        for ( GenerateNewFromHubSpotInfoRequest request : requests)
        {
            accountIdSet.add( request.accountId );
        }

        // query for all of the account records
        map<id, Account> relatedAccountMap = new map<id, Account>( mmcommon_AccountsSelector.newInstance().selectById( accountIdSet ) );

        // if the tasks pass all of the criteria
        // take action of creating an Intake from Tasks

        Intake__c newIntake = null;

        for ( GenerateNewFromHubSpotInfoRequest request : requests)
        {
            newIntake = new Intake__c();

            newIntake.RecordTypeId = intakePlaceHolderRecordTypeId;
            newIntake.Client__c = request.accountId;
            newIntake.Caller__c = request.accountId;
            newIntake.Injured_Party__c = request.accountId;
            newIntake.Marketing_Source__c = relatedAccountMap.get( request.accountId ).PersonLeadSource;
            newIntake.Marketing_Sub_Source__c = relatedAccountMap.get( request.accountId ).PersonLeadSubSource__c;
            newIntake.Status__c = 'Lead';
            //newIntake.CreatedByWebform__c = true;

            processCustomFieldDataFromHubSpot(newIntake, request.hubSpotFieldInfo);

            getMarketingTrackingInfoFactoryInstance().generateNewFromHubSpotInfo(newIntake, request.hubSpotFieldInfo);

            if (this.uow != null)
            {
                this.uow.registerNew( newIntake );
            }

            intakesByRelatedTaskIdMap.put( request.relatedTaskId, newIntake );
        }

        return intakesByRelatedTaskIdMap;
    }

    public mmintake_IntakesFactory with( map<Schema.sObjectField, string> fieldParameterMap )
    {
        if ( fieldParameterMap != null )
        {
            this.fieldParameterMap.putAll( fieldParameterMap );
        }

        return this;
    }

    public mmintake_IntakesFactory setClient( Account client )
    {
        this.client = client;

        return this;
    }

    public mmintake_IntakesFactory setClientAsCaller()
    {
        this.isClientTheCaller = true;

        return this;
    }

    public mmintake_IntakesFactory setCaller( Account caller )
    {
        this.caller = caller;

        return this;
    }

    public mmintake_IntakesFactory setInjuredParty( Account injuredParty )
    {
        this.injuredParty = injuredParty;

        return this;
    }

    public mmintake_IntakesFactory setClientAsInjuredParty()
    {
        this.isClientTheInjuredParty = true;

        return this;
    }

    public Intake__c generate()
    {
        if (this.uow == null )
        {
            throw new FactoryException('mmintake_IntakesFactory.uow was not populated.');
        }

        if ( this.client == null )
        {
            throw new FactoryException('The client is required for mmintake_IntakesFactory.  If was found to be null.');
        }

        this.newIntake = new Intake__c();

        for ( Schema.SObjectField intakeField : fieldParameterMap.keyset() )
        {
            // try to put the Schema.SObjectField on to the Intake__c object
            try
            {
                this.newIntake.put( intakeField, mmlib_Utils.convertValToDisplayType( intakeField.getDescribe().getType(), fieldParameterMap.get( intakeField ) ) );
            }
            catch ( System.SObjectException soe )
            {
                // if this occurs, then there is another SObject type's field in the list
                //   which is fine.  We simply will not use it here and we will do nothing
                //   with the exception.
            }
        }

        if ( this.isClientTheCaller )
        {
            uow.registerRelationship( this.newIntake, Intake__c.Caller__c, this.client );
        }
        else if ( this.caller != null )
        {
            uow.registerRelationship( this.newIntake, Intake__c.Caller__c, this.caller );
        }

        if ( this.isClientTheInjuredParty )
        {
            uow.registerRelationship( this.newIntake, Intake__c.Injured_Party__c, this.client );
            this.newIntake.Caller_is_Injured_Party__c = 'Yes';
        }
        else if ( this.injuredParty != null )
        {
            uow.registerRelationship( this.newIntake, Intake__c.Injured_Party__c, this.injuredParty );
        }

        if ( this.newIntake.inputChannel__c == null )
        {
            throw new FactoryException( Intake__c.InputChannel__c + ' Field is required.' );
        }

        uow.registerRelationship( this.newIntake, Intake__c.Client__c, this.client );

        uow.registerNew( this.newIntake );

        return this.newIntake;
    }

    public class GenerateNewFromHubSpotInfoRequest
    {
        public id accountId { get; set; }
        public map<string, string> hubSpotFieldInfo { get; set; } { hubSpotFieldInfo = new map<string, string>(); }
        public id relatedTaskId { get; set; }
    }

    public class FactoryException extends Exception { }
}