/**
 * SolServiceTest
 * @description Tests for SOL Service implementation.
 * @author Jeff Watson
 * @date 9/26/2018
 */
@IsTest
public with sharing class SolServiceTest {

    private static Account account;
    private static litify_pm__Matter__c matter;

    private static String caseTypeName = 'TEST';

    static {
        account = TestUtil.createAccount('Unit Test Account');
        insert account;

        litify_pm__Case_Type__c ct = TestUtil.createCaseType(caseTypeName);
        insert ct;

        matter = TestUtil.createMatter(account);
        matter.litify_pm__Case_Type__c = ct.Id;
        insert matter;

        
        List<SOL_Manager__c> solManagerSettings = new List<SOL_Manager__c>();
        solManagerSettings.add(new SOL_Manager__c(
                Name = 'SOL A',
                Apply_to_All_States__c = false,
                Case_Type__c = caseTypeName,
                Death__c = false,
                DOLT_SOL__c = 2,
                SOL__c = 1,
                State__c = 'FL',
                Type_of_Maritime_and_Admiralty_Case__c = 'Cruise Passenger',
                Where_Did_The_Injury_Occur__c = 'Standard Workers Compensation Claim'
            ));
        solManagerSettings.add(new SOL_Manager__c(
                Name = 'SOL B',
                Apply_to_All_States__c = false,
                Case_Type__c = caseTypeName,
                Death__c = true,
                DOLT_SOL__c = 3,
                SOL__c = 4,
                State__c = 'CA',
                Type_of_Maritime_and_Admiralty_Case__c = 'Cruise Passenger',
                Where_Did_The_Injury_Occur__c = 'Standard Workers Compensation Claim'
            ));
        insert solManagerSettings;
    }

    public static testMethod void SolService_Ctor() {

        // Arrange + Act
        Test.startTest();
            SolService solService = new SolService();
        Test.stopTest();

        // Assert
        System.assert(solService != null);
    }

    public static testMethod void SolService_GetBestMatch() {

        // Arrange
        SolService solService = new SolService();
        List<litify_pm__Matter__c> matterList = new List<litify_pm__Matter__c>{matter};

        // Act
        Test.startTest();
            // TODO: Uncomment after static matter is created during initialization
            Map<Id,SolService.BestMatch> bestMatch = SolService.getBestMatch(matterList);
            SolService.BestMatch bestMatch2 = SolService.getBestMatch(matter);
        Test.stopTest();

        // Assert
        //System.assert(bestMatch != null);
    }

    public static testMethod void SolService_GetBestMatch_NullParameter() {

        // Arrange + Act
        Test.startTest();
            litify_pm__Matter__c nullMatter;
            List<litify_pm__Matter__c> matterList = new List<litify_pm__Matter__c>{nullMatter};
            //Map<Id,SolService.BestMatch> bestMatchMap = new SolService().getBestMatch(matterList);
            SolService.BestMatch bestMatch = new SolService().getBestMatch(nullMatter);
        Test.stopTest();

        // Assert
        //System.assert(bestMatch == null);
    }
}