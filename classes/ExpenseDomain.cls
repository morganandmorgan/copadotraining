/**
 * ExpenseDomain
 * @description Expense Domain for litify_pm__Expense__c.
 * @author Jeff Watson
 * @date 1/19/2019
 */

public with sharing class ExpenseDomain extends fflib_SObjectDomain {

    // Ctor
    public ExpenseDomain(List<litify_pm__Expense__c> records) {
        super(records);
    }

    // IConstructable
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new ExpenseDomain(records);
        }
    }

    // Trigger Handlers
    public override void onAfterInsert() {

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {litify_pm__Expense__c.getSObjectType(), Expense_Summary__c.getSObjectType()});

        Set<Id> matterIds = new Set<Id>();
        Set<Id> pinExpIdSet = new Set<Id>();
        Map<Id,Set<Id>> expenseTypesForMatterIds = new Map<Id,Set<Id>>();

        for (SObject expense : Records) {

            Id matterId = ((litify_pm__Expense__c)expense).litify_pm__Matter__c;

            //add to the matter id set
            matterIds.add(matterId);

            if (!expenseTypesForMatterIds.containsKey(matterId)) {
                expenseTypesForMatterIds.put(matterId, new Set<Id>());
            }
            expenseTypesForMatterIds.get(matterId).add( ((litify_pm__Expense__c)expense).litify_pm__expenseType2__c);

            //add to the pin expense id set if it meets the criteria
            if(doPinCreation_Insert((litify_pm__Expense__c)expense)){
                pinExpIdSet.add(((litify_pm__Expense__c)expense).Id);
            }
        }

        //do the matter rollup calculation
        if(!matterIds.isEmpty()){
            if(canExecuteFutureCall()){
                Matter_Service.calculateExpenseTotals_Future(matterIds);
            }
            else{
                Matter_Service ms = new Matter_Service();
                ms.calculateExpenseTotals(matterIds);
            }
        }

        //do any auto creation of PINs
        if(!pinExpIdSet.isEmpty()){
            passToPINController(pinExpIdSet);
        }

        ExpenseSummaryDomain.createExpenseSummaries(expenseTypesForMatterIds, uow);
        uow.commitWork();
    } //onAfterInsert

    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {litify_pm__Expense__c.getSObjectType(), Expense_Summary__c.getSObjectType()});

        //this is a map of sets of expenseType2's for each matter, that will need new summaries
        Map<Id,Set<Id>> expenseTypesForMatterIds = new Map<Id,Set<Id>>();
        Set<Id> expenseRollupMatterIds = new Set<Id>();
        Set<Id> pinExpIdSet = new Set<Id>();

        for (SObject expense : Records) {

            Id matterId = ((litify_pm__Expense__c)expense).litify_pm__Matter__c;

            // evaluate for expense summaries
            if (this.computeExpenseSummaries((litify_pm__Expense__c)expense, (litify_pm__Expense__c)existingRecords.get(expense.Id))) {
                if (!expenseTypesForMatterIds.containsKey(matterId)) {
                    expenseTypesForMatterIds.put(matterId, new Set<Id>());
                }
                expenseTypesForMatterIds.get(matterId).add( ((litify_pm__Expense__c)expense).litify_pm__expenseType2__c);

                Id exMatterId = ((litify_pm__Expense__c)existingRecords.get(expense.Id)).litify_pm__Matter__c;
                if (matterId != exMatterId) {
                    if (!expenseTypesForMatterIds.containsKey(exMatterId)) {
                        expenseTypesForMatterIds.put(exMatterId, new Set<Id>());
                    }
                    expenseTypesForMatterIds.get(exMatterId).add( ((litify_pm__Expense__c)expense).litify_pm__expenseType2__c);
                } //if the matter changed
            } //if we need to recalc expense summaries

            // evaluate for expense rollups on the matter
            if(this.computeMatterRollup((litify_pm__Expense__c)expense, (litify_pm__Expense__c)existingRecords.get(expense.Id))) {
                expenseRollupMatterIds.add(((litify_pm__Expense__c)expense).litify_pm__Matter__c);
            }

            // add to the pin expense id set if it meets the criteria
            if(doPinCreation_Update((litify_pm__Expense__c)expense, (litify_pm__Expense__c)existingRecords.get(expense.Id))){
                pinExpIdSet.add(((litify_pm__Expense__c)expense).Id);
            }
        }

        // do the matter rollup calculation
        if(!expenseRollupMatterIds.isEmpty()){
            if(canExecuteFutureCall()){
                Matter_Service.calculateExpenseTotals_Future(expenseRollupMatterIds);
            }
            else{
                Matter_Service ms = new Matter_Service();
                ms.calculateExpenseTotals(expenseRollupMatterIds);
            }
        }

        //do any auto creation of PINs
        if(!pinExpIdSet.isEmpty()){
            passToPINController(pinExpIdSet);
        }

        ExpenseSummaryDomain.createExpenseSummaries(expenseTypesForMatterIds, uow);
        uow.commitWork();
    } //onAfterUpdate

    public override void onAfterDelete() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {litify_pm__Expense__c.getSObjectType(), Expense_Summary__c.getSObjectType()});

        Map<Id,Set<Id>> expenseTypesForMatterIds = new Map<Id,Set<Id>>();
        for (SObject expense : Records) {

            Id matterId = ((litify_pm__Expense__c)expense).litify_pm__Matter__c;

            if (!expenseTypesForMatterIds.containsKey(matterId)) {
                expenseTypesForMatterIds.put(matterId, new Set<Id>());
            }
            expenseTypesForMatterIds.get(matterId).add( ((litify_pm__Expense__c)expense).litify_pm__expenseType2__c);
    }
        ExpenseSummaryDomain.createExpenseSummaries(expenseTypesForMatterIds, uow);
        uow.commitWork();
    } //onAfterDelete

    // Methods
    private Boolean computeExpenseSummaries(litify_pm__Expense__c updatedExpense, litify_pm__Expense__c originalExpense) {

        if (updatedExpense == null || originalExpense == null) return false;

        if (updatedExpense.litify_pm__Amount__c != originalExpense.litify_pm__Amount__c) return true;
        if (updatedExpense.Written_Off_Amount__c != originalExpense.Written_Off_Amount__c) return true;
        if (updatedExpense.Amount_Remaining_to_Recover__c != originalExpense.Amount_Remaining_to_Recover__c) return true;
        if (updatedExpense.litify_pm__ExpenseType2__c != originalExpense.litify_pm__ExpenseType2__c) return true;
        if (updatedExpense.Amount_Recovered__c != originalExpense.Amount_Recovered__c) return true;

        if (updatedExpense.litify_pm__Matter__c != originalExpense.litify_pm__Matter__c) return true;

        return false;
    }

    private Boolean computeMatterRollup(litify_pm__Expense__c updatedExpense, litify_pm__Expense__c originalExpense) {

        if (updatedExpense == null || originalExpense == null) return false;

        if (updatedExpense.litify_pm__Amount__c != originalExpense.litify_pm__Amount__c) return true;
        if (updatedExpense.litify_pm__ExpenseType2__c != originalExpense.litify_pm__ExpenseType2__c) return true;
        if (updatedExpense.Amount_Recovered__c != originalExpense.Amount_Recovered__c) return true;
        if (updatedExpense.Written_Off_Amount__c != originalExpense.Written_Off_Amount__c) return true;
        if (updatedExpense.Amount_Remaining_to_Recover__c != originalExpense.Amount_Remaining_to_Recover__c) return true;

        return false;
    }

    private Boolean doPinCreation_Insert (litify_pm__Expense__c updatedExpense){
        return updatedExpense.Auto_Create_PIN__c == true ? true : false;
    }
    private Boolean doPinCreation_Update (litify_pm__Expense__c updatedExpense, litify_pm__Expense__c originalExpense){
        return updatedExpense.Auto_Create_PIN__c == true && originalExpense.Auto_Create_PIN__c == false && updatedExpense.Payable_Invoice_Created__c == false ? true : false;
    }
    private boolean canExecuteFutureCall() {
        return !(system.isFuture() || system.isBatch() || system.isScheduled());
    }

    private void passToPINController(Set<Id> expenseIds){
        processExpensesController expController = new processExpensesController();
        processExpensesController.PayableInvoiceRequest pinRequest = new processExpensesController.PayableInvoiceRequest();
        pinRequest.pinDate = Date.today();
        pinRequest.autoPostInvoice = true;
        pinRequest.autoCreatePayment = false;
        pinRequest.selectedBankAccountName = '';
        pinRequest.selectedPaymentMedia = '';

        processExpensesController.ActionResult result = processExpensesController.createPINfromExpenses(expenseIds, pinRequest);
    }

} //class