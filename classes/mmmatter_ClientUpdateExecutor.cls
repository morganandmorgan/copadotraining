/**
 * simple script to manage updating the matterRecord.litify_pm__Client__c info
 *
 *  @usage  String query = 'select Id, CP_Case_Number__c, Intake__r.Name, Intake__r.Client__c, Intake__c, litify_pm__Client__c, Name, Lawsuit__c, Litify_Load_Id__c'
 *                         + ', ReferenceNumber__c, litify_pm__Status__c, RecordTypeId, CreatedDate, CreatedById, OwnerId'
 *                         + 'from litify_pm__Matter__c'
 *                         + 'where litify_pm__Status__c = \'Open\' and litify_pm__Client__c = \'0011J00001EAdjaQAD\' and Intake__c != null';
 *          new mmlib_GenericBatch( mmmatter_ClientUpdateExecutor.class, query).setBatchSizeTo(15).execute();
 */
 public class mmmatter_ClientUpdateExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        litify_pm__Matter__c matterRecord = null;

        integer countOfEdgeCases = 0;
        integer countOfRecordsCorrected = 0;

        boolean isReferenceNumberFound = false;

        string referenceNumberFromIntake = null;

        string referenceNumberFromCPCaseNumber = null;

        list<litify_pm__Matter__c> matterRecordsToUpdate = new list<litify_pm__Matter__c>();

        for ( SObject record : scope )
        {
            matterRecord = (litify_pm__Matter__c)record;

            isReferenceNumberFound = false; // reset the boolean flag

            if ( matterRecord.CP_Case_Number__c != null
                    && matterRecord.Intake__c != null
                    && ! matterRecord.CP_Case_Number__c.containsIgnoreCase('test')
                    && ! matterRecord.CP_Case_Number__c.containsIgnoreCase('dup'))
            {
                referenceNumberFromIntake = matterRecord.Intake__r.Name.replace('INT-','');

                for ( String chunk : matterRecord.CP_Case_Number__c.splitByCharacterType() )
                {
                    if ( chunk != null
                        && chunk.isNumeric() )
                    {
                        referenceNumberFromCPCaseNumber = chunk.trim().leftPad(7,'0');
                        isReferenceNumberFound = true;
                        break;
                    }
                }

                if ( referenceNumberFromIntake.equalsIgnoreCase(referenceNumberFromCPCaseNumber) )
                {
                    // update this record using the Intake's Client record.
                    matterRecord.litify_pm__Client__c = matterRecord.Intake__r.Client__c;
                    matterRecord.IsMatterClientFieldAdjusted__c = true;

                    matterRecordsToUpdate.add( matterRecord );
                }
            }
        }

        database.update(scope, false);
    }
}