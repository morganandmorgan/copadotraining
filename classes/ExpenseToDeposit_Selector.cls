/*============================================================================/
* ExpenseToDeposit_Selector
* @description Selector for Litify Deposits
* @author Brian Krynitsky
* @date 2/26/2019
=============================================================================*/
public class ExpenseToDeposit_Selector extends fflib_SObjectSelector {
	
	public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Expense_to_Deposit__c.Id,
            Expense_to_Deposit__c.Deposit__c,
            Expense_to_Deposit__c.Expense__c,
            Expense_to_Deposit__c.Amount_Allocated__c
        };
    }

    // Constructor
    public ExpenseToDeposit_Selector() {
    }

    //required implemenation from fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return Expense_to_Deposit__c.SObjectType;
    }

    // Methods
    public List<Expense_to_Deposit__c> selectByDepositId(Set<Id> depositIds) {
        fflib_QueryFactory query = newQueryFactory();
        
        //where conditions:
        query.setCondition('Deposit__c in :depositIds');

        return (List<Expense_to_Deposit__c>) Database.query(query.toSOQL());
    }
}