/**
 *  WARNING: This Apex Class is reference in a hard-coded fashion by the AMC CTI Connector.
 *  @see the AmcOpenCti.js file - https://github.com/Morgan-and-Morgan/amc-connector/blob/master/src/js/AmcOpenCti.js
 *  @see details related to the AMC integraiton - https://morgan.atlassian.net/wiki/display/IN/CTI+Related
 *  This class should not be changed or renamed without addressing the above dependency.
 */
global class IntakeFlowController {

    private static String CCC_RECORDTYPE_NAME = 'CCC';

    WebService static String getAccount(ID accountId) {
        Account act = [SELECT Id, Name FROM Account WHERE ID = :accountId];
        return JSON.serialize(act);
    }

    WebService static String getAccountPageInfo(ID accountId) {
        Account act = [SELECT Id, Name, PersonContactId FROM Account WHERE ID = :accountId];

        Map<String, String> j = new Map<String, String>();
        j.put('url', URL.getSalesforceBaseUrl().toExternalForm() + '/' + act.Id);
        j.put('objectId', act.PersonContactId);
        j.put('objectName', act.Name);
        j.put('object', 'Contact');
        j.put('displayName', 'Account');
        j.put('accountId', act.Id);
        j.put('personAccount', 'true');

        return JSON.serialize(j);
    }
    WebService static String getIntakePageInfo(ID intakeId) {
        Intake__c intake = [SELECT Id, Name FROM Intake__c WHERE ID = :intakeId];

        Map<String, String> j = new Map<String, String>();
        j.put('url', URL.getSalesforceBaseUrl().toExternalForm() + '/' + intake.Id);
        j.put('objectId', intake.Id);
        j.put('objectName', intake.Name);
        j.put('object', 'Intake__c');
        j.put('displayName', 'Intake');

        return JSON.serialize(j);
    }

    public IntakeFlowController() {
        System.debug('CREATING INTAKE FLOW CONTROLLER');
        IsIncoming = false;
        InitializeCommentsTask();
    }

    @RemoteAction
    global static String BuildCityStateJSArray(String selectedState) {
        System.debug('BuildCityStateJSArray state: ' + selectedState);
        List<Address_by_Zip_Code__c> CityStates = [SELECT City__c, State__c, State_Code__c FROM Address_by_Zip_Code__c WHERE State__c = :selectedState OR State_Code__c = :selectedState ORDER BY City__c];

        System.debug('BuildCityStateJSArray returned size: ' + CityStates.size());

        List<String> strs = new List<String>();
        for (Address_by_Zip_Code__c cs : CityStates) {
          strs.add('{"city": "' + cs.City__c + '","state": "' + cs.State__c + '","state_code":"' + cs.State_Code__c + '"}');
        }

        System.debug('BuildCityStateJSArray strs: ' + strs.size());
        return '[' + String.join(strs, ',') + ']';
    }

    public PageReference RefreshIntake() {
        List<Intake__c> intakes = [SELECT Name, Caller__c, Caller__r.Name, Injured_Party__r.Name, Client__r.Name, Case_Type__c, Venue__c, Catastrophic__c, Lead_Details__c, Caller_is_Injured_Party__c, Injured_party_relationship_to_Caller__c, Injured_Party__c, RecordType.DeveloperName FROM Intake__c WHERE ID = :IntakeId];
        if (intakes.size() > 0) {
            intakeBackingStore =  intakes[0];
        } else {
            System.debug('Could not find intake with ID ' + IntakeId);
        }

        return redirect(intakeBackingStore);
    }

    private static PageReference redirect(Intake__c intake) {
        PageReference pr = null;
        if (intake != null && intakeIsMissingRequiredFlowFields(intake)) {
            pr = new PageReference('/apex/ContactSearch').setRedirect(true);
            pr.getParameters().put('intakeId', intake.Id);
        }
        return pr;
    }

    private static Boolean intakeIsMissingRequiredFlowFields(Intake__c intake) {
        return intake.RecordType.DeveloperName == CCC_RECORDTYPE_NAME
            && (intake.Caller__c == null
                || String.isBlank(intake.Caller_is_Injured_Party__c)
                || (intake.Caller_is_Injured_Party__c == 'No' && String.isBlank(intake.Injured_party_relationship_to_Caller__c))
                || intake.Injured_Party__c == null);
    }

    public Flow.Interview.Contact_Information_Capture FlowInstance { get; set; }
    public PageReference FinishLocation {
        get {
            String url = '/';
            if (IntakeId != null) {
                url += IntakeId;
            }
            PageReference page = new PageReference(url);
            page.setRedirect(true);
            return page;
        }
    }
    public ID IntakeId {
        get {
            ID theIntakeId;
            if (flowInstance != null) {
                theIntakeId = (ID)flowInstance.getVariableValue('IntakeID');
            }

            if (theIntakeId == null) {
                theIntakeId = ApexPages.currentPage().getParameters().get('IntakeID');
            }

            return theIntakeId;
        }
    }
    @TestVisible public ID AccountID {
        get {
            if (flowInstance != null) {
                return (ID)flowInstance.getVariableValue('PersonAccountID');
            }
            return AccountID;
        }
        private set;
    }
    private ID ContactID {
        get {
            List<Contact> contacts = [SELECT ID FROM Contact WHERE AccountID = :AccountID LIMIT 1];
            if (!contacts.isEmpty()) {
                return contacts.get(0).ID;
            } else {
                return null;
            }
        }
    }

    private Intake__c intakeBackingStore;
    public Intake__c Intake {
        get {
            return intakeBackingStore;
        }
    }
    public String IsCatastrophic {
        get {
            if (intakeBackingStore != null && intakeBackingStore.Catastrophic__c) {
                return 'yes';
            } else {
                return 'no';
            }
        }
    }

    public string Comments { get; set; }
    public Boolean IsIncoming { get; set; }
    private Task CommentTask { get; set; }
    public void SaveComment() {
        System.Debug('In Save Comment with comments: ' + Comments);

        if (IntakeID != null && ContactID != null) {
            UpdateCommentTask();
        }
    }

    private void UpdateCommentTask() {
        CommentTask.WhoId = ContactID;
        CommentTask.WhatId = IntakeID;
        CommentTask.Description = Comments;

        if (IsIncoming) {
            CommentTask.CallType = 'Inbound';
            CommentTask.CallDisposition = 'Correct Contact';
            upsert CommentTask;
            return;
        }

        if (CommentTask.ID == null && (CommentTask.Description == null || CommentTask.Description == '')) {
            return;
        }

        if (CommentTask.CallType != null && CommentTask.CallType != '') {
            CommentTask.CallType = null;
        }

        upsert CommentTask;
    }

    private void InitializeCommentsTask() {
        CommentTask = new Task();
        CommentTask.Subject = 'Intake Comment';
        CommentTask.Type = 'Comment';
        CommentTask.Status = 'Completed';
        CommentTask.ActivityDate = Date.today();
    }
}