@isTest
public class mmlitifylrn_ReferralsServiceTest  {

    // START SETUP & HELPERS
    public static final Datetime UPDATED_SINCE = Datetime.now().addDays(-100);

    @TestSetup 
    static void Setup() 
    {
        String firstName = 'George';
        String lastName = 'Jones';
        String referralRecordTypeName = 'Third Party Outgoing Referral';
        String caseTypeName = 'General Injury';
        String firmName = 'bencrump.com';

        mmlitifylrn_ThirdPartyConfig__c thirdPartyConfig = new mmlitifylrn_ThirdPartyConfig__c();
        thirdPartyConfig.IsIntegrationDebugEnabled__c = false;
        thirdPartyConfig.IsIntegrationsEnabledInSandboxEnv__c = true;
        thirdPartyConfig.IsLRNIntegrationEnabled__c = true;
        thirdPartyConfig.LRNAPIDomainName__c = 'http://referral-api-demo.litify.com';
        insert thirdPartyConfig;

        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c();
        caseType.Name = caseTypeName;
        insert caseType;

        litify_pm__Firm__c firm = new litify_pm__Firm__c();
        firm.Name = firmName;        
        firm.litify_pm__ExternalId__c = 150;
        firm.litify_pm__Is_Available__c = true;
        insert firm;

        litify_pm__Referral__c referral = new litify_pm__Referral__c();
        referral.RecordTypeId = getRecordTypeIdByName(referralRecordTypeName);
        referral.litify_pm__ExternalId__c = 877;
        referral.litify_pm__Case_Type__c = caseType.Id;
        referral.litify_pm__Client_First_Name__c = firstName;
        referral.litify_pm__Client_Last_Name__c = lastName;
        referral.litify_pm__Case_State__c = 'GA Georgia';
        referral.litify_pm__Client_phone__c = '111-222-3333';
        referral.litify_pm__Handling_Firm__c = firm.Id;
        insert referral;     

        mmlitifylrn_ThirdPartyCredential__c litifyCreds = new mmlitifylrn_ThirdPartyCredential__c();
        litifyCreds.Name = 'bencrump.com';
        litifyCreds.FirmExternalID__c = 150;
        litifyCreds.Username__c = 'bencrump-test-integration@ce-v.com';
        litifyCreds.AccessToken__c = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3JlZmVycmFsLWRlbW8ubGl0aWZ5LmNvbSIsImV4cCI6MTUxMjc3NTkzMywiaWF0IjoxNTEyMTcxMTMzLCJ1c2VyX2lkIjoyMzEsInNlc3Npb25faWRlbnRpZmllciI6ImRiNTBlOWJlM2ZmMDNkMzM0NDZhZjYyMGU1YTkwMGU0In0.avRyHu5fkmzoEMQcGdj75Ni';
        litifyCreds.RefreshToken__c = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3JlZmVycmFsLWRlbW8ubGl0aWZ5LmNvbSIsImV4cCI6MTUxMzQ2NzEzMywiaWF0IjoxNTEyMTcxMTMzLCJyZWZyZXNoX3Rva2VuIjp0cnVlLCJ1c2VyX2lkIjoyMzEsInNlc3Npb25faWRlbnRpZmllciI6ImRiNTBlOWJlM2ZmMDNkMzM0NDZhZjYyMGU1YTkwMGU0In0.bnnADHWlsgmK_28qwyScHZ28nNqyV9XaY2bEoqE1cKs';
        insert litifyCreds;
	}

    private static String getRecordTypeIdByName(String recordTypeName)
    {

        List<RecordType> recordTypes = [select Id, Name from RecordType where Name = :recordTypeName];

        if(recordTypes.size() > 0)
        {
            return recordTypes[0].Id;
        }

        return null;
    }

    private static List<litify_pm__Referral__c> getReferrals()
    {
        String fieldsToInclude = '';

        Map<String, Schema.SObjectField> fieldMap = litify_pm__Referral__c.sObjectType.getDescribe().fields.getMap();
        for (String fieldName : fieldMap.keySet())
        {
            fieldsToInclude += fieldName + ',';
        }
        fieldsToInclude = fieldsToInclude.substring(0, fieldsToInclude.length() - 1);
        String queryStr = 'select ' + fieldsToInclude + ' from litify_pm__Referral__c';

        return Database.query(queryStr);
    }

    private static String getSyncResponseAsJson()
    {
        return ('{"referral_intakes":[{"klass":"ReferralIntake","id":877,"reference_number":"877","case_type":{"klass":"CaseType","id":24,"name":"General Injury"},"descriptions":null,"description":null,"referral_intake_status":"active","extra_info":null,"external_ref_number":null,"incident_date":null,"referral_percentage_fee":20.0,"case_location":{"address1":null,"address2":"","postal_code":"","city":null,"state":"TN"},"client_first_name":"Elvis","client_last_name":"Presley","client_phone":"222-333-4444","client_email":"","client_location":{"address1":"","address2":"","postal_code":"","city":null,"state":"TN"},"active_referral":{"case_status":"sent_to_attorney","referral_sub_status":null,"handling_organization":{"klass":"Organization","id":47,"name":"Morgan & Morgan P.A."}},"created_at":"2017-11-20T20:57:23.115Z","updated_at":"2017-11-20T20:58:24.335Z","last_handling_organization_name":"Morgan & Morgan P.A.","last_active_referral_status":"sent_to_attorney"},{"klass":"ReferralIntake","id":723,"reference_number":"723","case_type":{"klass":"CaseType","id":13,"name":"Criminal"},"descriptions":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","description":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","referral_intake_status":"new","extra_info":null,"external_ref_number":null,"incident_date":"2017-09-24T00:00:00.000Z","referral_percentage_fee":0.4,"case_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":"VA"},"client_first_name":"Charlie","client_last_name":"Brown","client_phone":"(804) 555-1212","client_email":"charlie@peanuts.com","client_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":null},"created_at":"2017-09-28T18:19:44.622Z","updated_at":"2017-09-28T18:19:44.724Z"},{"klass":"ReferralIntake","id":722,"reference_number":"722","case_type":{"klass":"CaseType","id":13,"name":"Criminal"},"descriptions":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","description":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","referral_intake_status":"new","extra_info":null,"external_ref_number":null,"incident_date":"2017-09-24T00:00:00.000Z","referral_percentage_fee":0.4,"case_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":"VA"},"client_first_name":"Charlie","client_last_name":"Brown","client_phone":"(804) 555-1212","client_email":"charlie@peanuts.com","client_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":null},"created_at":"2017-09-28T17:51:42.852Z","updated_at":"2017-09-28T17:51:42.951Z"},{"klass":"ReferralIntake","id":721,"reference_number":"721","case_type":{"klass":"CaseType","id":13,"name":"Criminal"},"descriptions":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","description":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","referral_intake_status":"new","extra_info":null,"external_ref_number":null,"incident_date":"2017-09-24T00:00:00.000Z","referral_percentage_fee":0.4,"case_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":"VA"},"client_first_name":"Charlie","client_last_name":"Brown","client_phone":"(804) 555-1212","client_email":"charlie@peanuts.com","client_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":null},"created_at":"2017-09-28T17:12:46.291Z","updated_at":"2017-09-28T17:12:46.328Z"},{"klass":"ReferralIntake","id":688,"reference_number":"688","case_type":{"klass":"CaseType","id":13,"name":"Criminal"},"descriptions":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","description":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","referral_intake_status":"new","extra_info":null,"external_ref_number":null,"incident_date":"2017-09-24T00:00:00.000Z","referral_percentage_fee":0.4,"case_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":"VA"},"client_first_name":"Charlie","client_last_name":"Brown","client_phone":"(804) 555-1212","client_email":"charlie@peanuts.com","client_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":null},"created_at":"2017-09-27T14:47:25.580Z","updated_at":"2017-09-27T14:47:25.613Z"},{"klass":"ReferralIntake","id":687,"reference_number":"687","case_type":{"klass":"CaseType","id":13,"name":"Criminal"},"descriptions":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","description":"Description of Incident:\nnull\n\nInjuries:\nnull\n\nDescription of Injuries:\nnull\n\nTreatment:\nnull\n\nDescription of Treatment:\nnull\nnull","referral_intake_status":"new","extra_info":null,"external_ref_number":null,"incident_date":"2017-09-24T00:00:00.000Z","referral_percentage_fee":0.4,"case_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":"VA"},"client_first_name":"Charlie","client_last_name":"Brown","client_phone":"(804) 555-1212","client_email":"charlie@peanuts.com","client_location":{"address1":null,"address2":null,"postal_code":null,"city":null,"state":null},"created_at":"2017-09-27T02:00:27.656Z","updated_at":"2017-09-27T02:00:27.686Z"},{"klass":"ReferralIntake","id":599,"reference_number":"599","case_type":{"klass":"CaseType","id":13,"name":"Criminal"},"descriptions":"lorem ipsum ...","description":"lorem ipsum ...","referral_intake_status":"new","extra_info":"lorem ipsum ...","external_ref_number":"LY0000001","incident_date":null,"referral_percentage_fee":2.34,"case_location":{"address1":"74134 3rd Av.","address2":"31st floor","postal_code":"10014","city":"Los Angeles","state":"CA"},"client_first_name":"Thomas","client_last_name":"Lawson","client_phone":"347 856 9874","client_email":"tlawson@gmail.com","client_location":{"address1":"74133 3rd Av.","address2":"31st floor","postal_code":"10014","city":"Los Angeles","state":"CA"},"created_at":"2017-09-25T16:59:53.869Z","updated_at":"2017-09-25T16:59:53.952Z"},{"klass":"ReferralIntake","id":597,"reference_number":"597","case_type":{"klass":"CaseType","id":11,"name":"Construction"},"descriptions":"hammer fell on his head","description":"hammer fell on his head","referral_intake_status":"pending","extra_info":null,"external_ref_number":null,"incident_date":"2017-09-22T00:00:00.000Z","referral_percentage_fee":20.0,"case_location":{"address1":"5403 Monument Ave","address2":"","postal_code":"","city":null,"state":"VA"},"client_first_name":"Charlie","client_last_name":"Brown","client_phone":"(804) 555-1212","client_email":"charlie@peanuts.com","client_location":{"address1":"5403 Monument Ave","address2":"","postal_code":"23226","city":"Richmond","state":"VA"},"created_at":"2017-09-23T21:36:56.240Z","updated_at":"2017-10-04T02:04:05.685Z","last_handling_organization_name":"Morgan & Morgan P.A.","last_active_referral_status":"expired"}],"total_items":8,"total_pages":1,"current_page":1}').replaceAll('\n','').replaceAll('\r','');
    }

    public class MockHttpSyncResponseGenerator implements HttpCalloutMock
    {
        public HTTPResponse respond(HTTPRequest req)
        {       
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(getSyncResponseAsJson());
            res.setStatusCode(200);
            return res;
        }
    }
    // END SETUP & HELPERS

    // START ctor 
    static testmethod void mmlitifylrn_ReferralsService_Ctor_Test()
    {
        // Arrange + Act
        Test.startTest();
        mmlitifylrn_ReferralsService actual = new mmlitifylrn_ReferralsService();
        Test.stopTest();

        // Assert
        System.assert(actual != null);
    }
    // END ctor

    // START syncFromLitify()
    static testmethod void mmilitifylrn_ReferralsService_SyncFromLitify_Test()
    {
        // Arrange
        Test.setMock(HttpCalloutMock.class, new MockHttpSyncResponseGenerator());

        // Act + Assert
        Test.startTest();
        mmlitifylrn_ReferralsService.syncFromLitify();
        Test.stopTest();
    }
    // END syncFromLitify()

    // START referToLitify()
    static testMethod void mmlitifylrn_ReferralsService_ReferToLitify_Test()
    {
        // Arrange
        Set<Id> referralIds = new Set<Id>();
        referralIds.add([SELECT Id FROM litify_pm__Referral__c WHERE litify_pm__ExternalId__c = 877 LIMIT 1].Id);

        // Act
        Test.startTest();
        mmlitifylrn_ReferralsService.referToLitify(referralIds);
        Test.stopTest();
    }
    // END referToLitify()
}