@isTest
private class Test_TaskCreationController {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Intake', 'Person');
        toInsert.add(client);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        intakes.add(new Intake__c(
                Client__c = client.Id,
                RecordTypeId = SObjectType.Intake__c.getRecordTypeInfosByName().get('CCC').getRecordTypeId()
            ));
        intakes.add(new Intake__c(
                Client__c = client.Id,
                RecordTypeId = SObjectType.Intake__c.getRecordTypeInfosByName().get('Mass Tort').getRecordTypeId()
            ));
        toInsert.addAll((List<SObject>) intakes);

        Database.insert(intakes);
        toInsert.clear();
    }

    private static Intake__c getIntake(String recordTypeName) {
        return [
            SELECT
                Id,
                Case_Type__c
            FROM
                Intake__c
            WHERE
                RecordType.Name = :recordTypeName
        ];
    }

    @isTest
    private static void testCancel() {
        Intake__c intake = getIntake('CCC');

        ApexPages.currentPage().getParameters().put('Id', intake.Id);
        TaskCreationController controller = new TaskCreationController();

        Test.startTest();
        PageReference pr = controller.cancel();
        Test.stopTest();

        System.assertNotEquals(null, pr);
        System.assertEquals('/' + intake.Id, pr.getUrl());
    }

    @isTest
    private static void createMailoutTask_Failure() {
        Intake__c intake = getIntake('CCC');
        if (intake.Case_Type__c != null) {
            intake.Case_Type__c = null;
            Database.update(intake);
        }

        ApexPages.currentPage().getParameters().put('Id', intake.Id);
        ApexPages.currentPage().getParameters().put('taskSrc', 'mailout');
        TaskCreationController controller = new TaskCreationController();

        Test.startTest();
        PageReference pr = controller.createTask();
        Test.stopTest();

        System.assertEquals(null, pr);

        System.assertEquals(0, [SELECT COUNT() FROM Task]);
    }

    @isTest
    private static void createMailoutTask_CccIntake() {
        Intake__c intake = getIntake('CCC');
        if (intake.Case_Type__c == null) {
            intake.Case_Type__c = 'Test Subject';
            Database.update(intake);
        }

        ApexPages.currentPage().getParameters().put('Id', intake.Id);
        ApexPages.currentPage().getParameters().put('taskSrc', 'mailout');
        TaskCreationController controller = new TaskCreationController();

        Test.startTest();
        PageReference pr = controller.createTask();
        Test.stopTest();

//        System.assertNotEquals(null, pr);
//        System.assertEquals('/' + intake.Id, pr.getUrl());

//        Task newTask = [SELECT WhatId, Subject, Type, Mail_Out_Status__c, ActivityDate FROM Task];
//        System.assertEquals(intake.Id, newTask.WhatId);
//        System.assertEquals('Test Subject', newTask.Subject);
//        System.assertEquals('Mail Out', newTask.Type);
//        System.assertEquals('Pending', newTask.Mail_Out_Status__c);
//        System.assertEquals(Date.today()+1, newTask.ActivityDate);

//        Intake__c updatedIntake = [SELECT Status__c, Status_Detail__c FROM Intake__c WHERE Id = :newTask.WhatId];
//        System.assertEquals('Sign Up In Progress', updatedIntake.Status__c);
//        System.assertEquals('Mailout Sent', updatedIntake.Status_Detail__c);
    }

    @isTest
    private static void createMailoutTask_MassTortIntake() {
        Intake__c intake = getIntake('Mass Tort');
        if (intake.Case_Type__c == null) {
            intake.Case_Type__c = 'Test Subject';
            Database.update(intake);
        }

        ApexPages.currentPage().getParameters().put('Id', intake.Id);
        ApexPages.currentPage().getParameters().put('taskSrc', 'mailout');
        TaskCreationController controller = new TaskCreationController();

        Test.startTest();
        PageReference pr = controller.createTask();
        Test.stopTest();

        System.assertNotEquals(null, pr);
        System.assertEquals('/' + intake.Id, pr.getUrl());

        Task newTask = [SELECT WhatId, Subject, Type, Mail_Out_Status__c, ActivityDate FROM Task];
        System.assertEquals(intake.Id, newTask.WhatId);
        System.assertEquals('Test Subject - null', newTask.Subject);
        System.assertEquals('Mail Out', newTask.Type);
        System.assertEquals('Pending', newTask.Mail_Out_Status__c);
        System.assertEquals(Date.today()+1, newTask.ActivityDate);
    }

    @isTest
    private static void createCallbackTask_Failure() {
//        Intake__c intake = getIntake('CCC');
//
//        ApexPages.currentPage().getParameters().put('Id', intake.Id);
//        ApexPages.currentPage().getParameters().put('taskSrc', 'callback');
//        TaskCreationController controller = new TaskCreationController();
//
//        controller.taskRcd.Subject = null;
//
//        Test.startTest();
//        PageReference pr = controller.createTask();
//        Test.stopTest();
//
//        System.assertEquals(null, pr);
//
//        System.assertEquals(0, [SELECT COUNT() FROM Task]);
    }

    @isTest
    private static void createCallbackTask_CccIntake() {
        Intake__c intake = getIntake('CCC');

        ApexPages.currentPage().getParameters().put('Id', intake.Id);
        ApexPages.currentPage().getParameters().put('taskSrc', 'callback');
        TaskCreationController controller = new TaskCreationController();

        controller.taskRcd.ActivityDate = Date.today();

        Test.startTest();
        PageReference pr = controller.createTask();
        Test.stopTest();

        System.assertNotEquals(null, pr);
        System.assertEquals('/' + intake.Id, pr.getUrl());

        Task newTask = [SELECT WhatId, Status, Subject, Type, ActivityDate FROM Task];
        System.assertEquals(intake.Id, newTask.WhatId);
        System.assertEquals('Callback Request', newTask.Subject);
        System.assertEquals('Callback', newTask.Type);
        System.assertEquals('Not Started', newTask.Status);
        System.assertEquals(Date.today(), newTask.ActivityDate);

        Intake__c updatedIntake = [SELECT Status__c, Status_Detail__c FROM Intake__c WHERE Id = :newTask.WhatId];
        System.assertNotEquals('Sign Up In Progress', updatedIntake.Status__c);
        System.assertNotEquals('Mailout Sent', updatedIntake.Status_Detail__c);
    }

    @isTest
    private static void createMCallbackTask_MassTortIntake() {
        Intake__c intake = getIntake('Mass Tort');

        ApexPages.currentPage().getParameters().put('Id', intake.Id);
        ApexPages.currentPage().getParameters().put('taskSrc', 'callback');
        TaskCreationController controller = new TaskCreationController();

        controller.taskRcd.ActivityDate = Date.today();

        Test.startTest();
        PageReference pr = controller.createTask();
        Test.stopTest();

        System.assertNotEquals(null, pr);
        System.assertEquals('/' + intake.Id, pr.getUrl());

        Task newTask = [SELECT WhatId, Status, Subject, Type, ActivityDate FROM Task];
        System.assertEquals(intake.Id, newTask.WhatId);
        System.assertEquals('Callback Request', newTask.Subject);
        System.assertEquals('Callback', newTask.Type);
        System.assertEquals('Not Started', newTask.Status);
        System.assertEquals(Date.today(), newTask.ActivityDate);
    }
}