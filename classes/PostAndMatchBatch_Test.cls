@isTest
private without sharing class PostAndMatchBatch_Test {
    //NOTE:  Original name was too long.
	private static c2g__codaCompany__c company=null;
    private static c2g__codaBankAccount__c operatingBankAccount=null;

    //Cache rules everything around me:  
	private static Payment_Collection__c paymentCollection = null; 
    private static c2g__codaPayment__c payment = null; 
    private static Payment_Processing_Batch_Status__c trackingRecord = null; 
    
	static void setupTestData(){
        //TODO:  Reconsider this approach:  Maybe don't cache records in static state.
        //TODO:  Consider rolling setup methods into test data factory.
        System.debug('Creating setup test fixture data if needed...');
    	setupPaymentCollectionData();
        
        if(payment==null){
            System.debug('Creating payment, etc...');
            
            paymentCollection=createAndInsertPaymentCollection();
            payment=createAndInsertPayment(null);      
            trackingRecord=createPaymentProcessTrackingRecord(paymentCollection.Id, payment.Id);
        }

        TestDataFactory_FFA.createCheckRange();

	}
    
	static void setupPaymentCollectionData(){

        if(company==null){

            System.debug('Creating payment collection prerequisite data...');
            /*--------------------------------------------------------------------
            FFA data setup
            --------------------------------------------------------------------*/
            company = TestDataFactory_FFA.company;

            operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
            operatingBankAccount.Bank_Account_Type__c = 'Operating';
            update operatingBankAccount;

            // Map<c2g__codaPurchaseInvoice__c, List<c2g__codaPurchaseInvoiceExpenseLineItem__c>> payableInvoices_ExpenseMap = TestDataFactory_FFA.createPayableInvoices_Expense(2);
            // TestDataFactory_FFA.postPayableInvoices(payableInvoices_ExpenseMap.keySet());

        }

        System.assert(company!=null, 'Company is null.');
        System.assert(operatingBankAccount!=null, 'operatingBankAccount is null.');
        System.assert(operatingBankAccount.Bank_Account_Type__c!=null, 'operatingBankAccount.Bank_Account_Type__c is null.');

	}
    
    static Payment_Collection__c createAndInsertPaymentCollection(){

        System.debug('Creating payment collection...');

        Payment_Collection__c record = null;

        System.assert(company!=null, 'Company is null.');
        System.assert(operatingBankAccount!=null, 'operatingBankAccount is null.');

		record = new Payment_Collection__c(
			Company__c = company.Id,
			Bank_Account__c = operatingBankAccount.Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		insert record;
        return record;

    }      
    
    static c2g__codaPayment__c createAndInsertPayment(Id paymentCollectionId){
        
        c2g__codaPayment__c record = null;

        System.debug('Creating payment object...');

        {

            //Note:  Looks like cnt param doesn't do anything
            record=TestDataFactory_FFA.createPayment(1, false);

            if(paymentCollectionId==null){
                paymentCollectionId=paymentCollection.Id;
            }

            record.Payment_Collection__c = paymentCollectionId;

            System.assert((record.Payment_Collection__c!=null), 'Payment.Payment_Collection__c is null.');
            
            {
                Database.SaveResult result = null;

                System.debug('Inserting payment object...');               
                
                result=Database.insert(record);

                System.assert(result.isSuccess(), 'Payment insert failed.');

            }

            System.assert(record!=null, 'Payment is null.');
            System.assert(record.Id!=null, 'Payment Id is null.');

        }

        return record;

    } 
    
    static Payment_Processing_Batch_Status__c createPaymentProcessTrackingRecord(Id paymentCollectionId, Id paymentId){
        Payment_Processing_Batch_Status__c tracker=null;
        {
            tracker=new Payment_Processing_Batch_Status__c();
            tracker.Payment_Collection__c = paymentCollectionId;
            tracker.Payment__c = paymentId;
            insert tracker;
        }

        PaymentProcessingStatusDataAccess data=null;
        data=new PaymentProcessingStatusDataAccess();
        List<Payment_Processing_Batch_Status__c> records=null;
        //Get the full data - all the fields we need:  
        records=data.getAllPaymentCollectionTrackingRecords(paymentCollection.Id);
        //records=data.getAllPaymentTrackingRecords(payment.Id);
        //getAllPaymentCollectionTrackingRecords
        if((records!=null)&&(records.size()>0)){
            tracker=records[0];
        }else{
            System.assert(false, 'Tracking records no retrieved using data access utility.');
        }

        System.assert((tracker!=null), 'Tracking record is null.');
        System.assert((tracker.Payment__r.Name!=null), 'Tracker payment name is null.');
        
        return tracker;
    }     

    @isTest 
    static void testCreatePaymentProcessingLaunchPostAndMatchBatch0(){
        PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;

        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);

        batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(paymentId);
        batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(paymentId, null);
        System.assert(batchProcessor!=null);
    } 
    

    
    @isTest 
    static void test_start0(){
        PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;

        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        
        batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(paymentId, null);
        System.assert(batchProcessor!=null);

        Iterable<Id> result = null;
        result=batchProcessor.start(null);

        System.assert(result!=null);
        
    }

    @isTest 
    static void test_execute0(){
        PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;

        Id paymentId = null;
        paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        
        batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(paymentId, null);
        System.assert(batchProcessor!=null);

        Iterable<Id> result = null;
        result=batchProcessor.start(null);

        System.assert(result!=null);

        List<Id> scope = null;
        scope = (List<Id>) result;
        
        batchProcessor.execute((Database.BatchableContext) null, scope);
    }   

    // @isTest 
    // static void test_finish0(){

    //     //This may choke on a fake payment Id, since the finish method chains a batch.
    //     PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;

    //     Id paymentId = null;
    //     paymentId = TestDataFactory_FFA.getFakeRecordId(c2g__codaPayment__c.sObjectType);
        
    //     batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(paymentId, null);
    //     System.assert(batchProcessor!=null);

    //     Test.startTest();
    //     batchProcessor.finish(null);
    //     Test.stopTest();
        
    // }      


    @isTest 
    static void test_execute1(){

        setupTestData();

        PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;
        
        batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(payment.Id, trackingRecord);
        System.assert(batchProcessor!=null);

        Iterable<Id> result = null;
        result=batchProcessor.start(null);

        System.assert(result!=null);
       
        List<Id> scope = null;
        scope = (List<Id>) result;
        
        batchProcessor.execute((Database.BatchableContext) null, scope);
    }   

    // @isTest 
    // static void test_finish1(){

    //     setupTestData();
    //     PaymentProcessingLaunchPostAndMatchBatch batchProcessor = null;  
        
    //     batchProcessor = new PaymentProcessingLaunchPostAndMatchBatch(payment.Id, trackingRecord);
    //     System.assert(batchProcessor!=null);

    //     Test.startTest();
    //     batchProcessor.finish(null);
    //     Test.stopTest();

        
    // }             
    
}