public interface mmlib_ISObjectDomain 
{
    void processDomainLogicInjections(String domainProcessToken );
    void processDomainLogicInjections(String domainProcessToken, mmlib_ISObjectUnitOfWork uow );
}