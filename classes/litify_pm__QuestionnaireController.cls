/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class QuestionnaireController {
    global QuestionnaireController() {

    }
    @AuraEnabled
    global static Map<String,List<SObject>> fetchRecords(List<String> type) {
        return null;
    }
    @AuraEnabled
    global static String getAllSobjects() {
        return null;
    }
    @AuraEnabled
    global static Map<String,List<Map<String,List<String>>>> getFieldValues(List<String> objects) {
        return null;
    }
    @AuraEnabled
    global static Map<String,SObject> updateQuestionnaire(litify_pm__Questionnaire__c questionnaire) {
        return null;
    }
    @AuraEnabled
    global static Map<String,SObject> upsertQuestion(litify_pm__Question__c question) {
        return null;
    }
}
