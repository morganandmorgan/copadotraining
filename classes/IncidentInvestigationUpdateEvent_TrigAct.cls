public class IncidentInvestigationUpdateEvent_TrigAct extends TriggerAction {

  private final Map<Id, IncidentInvestigationEvent__c> recordsToProcessForUpdate = new Map<Id, IncidentInvestigationEvent__c>();

  public IncidentInvestigationUpdateEvent_TrigAct() {
    super();
  }

  public override Boolean shouldRun() {
    recordsToProcessForUpdate.clear();
    if (isAfter() && isUpdate()) {
      Map<Id, IncidentInvestigationEvent__c> newMap = (Map<Id, IncidentInvestigationEvent__c>) this.triggerMap;
      Map<Id, IncidentInvestigationEvent__c> oldMap = (Map<Id, IncidentInvestigationEvent__c>) this.triggerOldMap;

      for (IncidentInvestigationEvent__c newRecord : newMap.values()) {
        IncidentInvestigationEvent__c oldRecord = oldMap.get(newRecord.Id);

        if (newRecord.Contracts_to_be_Signed__c != oldRecord.Contracts_to_be_Signed__c) {
          recordsToProcessForUpdate.put(newRecord.Id, newRecord);
        }
      }
    }
    return !recordsToProcessForUpdate.isEmpty();
  }

  public override void doAction() {
    if (!recordsToProcessForUpdate.isEmpty()) {
      List<Event> toUpdate = new List<Event>();

      for (Event e : [
        SELECT
          WhatId,
          Contracts_to_be_Signed__c
        FROM
          Event
        WHERE 
          WhatId IN :recordsToProcessForUpdate.keySet()
      ]) {
        IncidentInvestigationEvent__c parent = recordsToProcessForUpdate.get(e.WhatId);
        if (e.Contracts_to_be_Signed__c != parent.Contracts_to_be_Signed__c) {
          e.Contracts_to_be_Signed__c = parent.Contracts_to_be_Signed__c;
          toUpdate.add(e);
        }
      }

      Database.update(toUpdate);
    }
  }
}