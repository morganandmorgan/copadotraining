@isTest
private class IntakeSetSignupMethodTest {

  private static final String STATUS_SUIP = 'Sign Up In Progress';
  private static final String STATUS_UNDER_REVIEW = 'Under Review';
  private static final String STATUS_DETAIL_MAILOUT = 'Mailout';

  @TestSetup
  private static void setup() {
    List<SObject> toInsert = new List<SObject>();

    Account client = TestUtil.createPersonAccount('Test', 'Person');
    toInsert.add(client);

    Database.insert(toInsert);
    toInsert.clear();
  }

  private static Intake__c createIntake(Id clientId) {
    Intake__c intake = new Intake__c(
        Client__c = clientId
      );
    return intake;
  }

  private static Account getClient() {
    return [
      SELECT Id
      FROM Account
      WHERE IsPersonAccount = true
    ];
  }

  private static Intake__c getIntake(Id intakeId) {
    return [
      SELECT Id,
        Status__c,
        Status_Detail__c,
        Sign_Up_Method__c
      FROM Intake__c
      WHERE Id = :intakeId
    ];
  }

  @isTest
  private static void captureStatusDetail_SignUpInProgress() {
    Account client = getClient();
    Intake__c intake = createIntake(client.Id);

    intake.Status__c = STATUS_SUIP;
    intake.Status_Detail__c = STATUS_DETAIL_MAILOUT;

    Test.startTest();
    Database.insert(intake);
    Test.stopTest();

    Intake__c requeriedIntake = getIntake(intake.Id);

    System.assertEquals(STATUS_DETAIL_MAILOUT, requeriedIntake.Status_Detail__c);
    System.assertEquals(STATUS_DETAIL_MAILOUT, requeriedIntake.Sign_Up_Method__c);
  }

  @isTest
  private static void captureStatusDetail_NotSignUpInProgress() {
    Account client = getClient();
    Intake__c intake = createIntake(client.Id);

    intake.Status__c = STATUS_UNDER_REVIEW;
    intake.Status_Detail__c = STATUS_DETAIL_MAILOUT;

    Test.startTest();
    Database.insert(intake);
    Test.stopTest();

    Intake__c requeriedIntake = getIntake(intake.Id);

    System.assertEquals(STATUS_DETAIL_MAILOUT, requeriedIntake.Status_Detail__c);
    System.assertEquals(null, requeriedIntake.Sign_Up_Method__c);
  }

  @isTest
  private static void statusDetailChanged_SignUpInProgress() {
    Account client = getClient();
    Intake__c intake = createIntake(client.Id);
    intake.Status__c = STATUS_UNDER_REVIEW;
    intake.Status_Detail__c = null;
    Database.insert(intake);

    intake = getIntake(intake.Id);
    intake.Status__c = STATUS_SUIP;
    intake.Status_Detail__c = STATUS_DETAIL_MAILOUT;

    Test.startTest();
    Database.update(intake);
    Test.stopTest();

    Intake__c requeriedIntake = getIntake(intake.Id);

    System.assertEquals(STATUS_DETAIL_MAILOUT, requeriedIntake.Status_Detail__c);
    System.assertEquals(STATUS_DETAIL_MAILOUT, requeriedIntake.Sign_Up_Method__c);
  }

  @isTest
  private static void statusDetailChanged_NotSignUpInProgress() {
    Account client = getClient();
    Intake__c intake = createIntake(client.Id);
    intake.Status__c = STATUS_UNDER_REVIEW;
    intake.Status_Detail__c = null;
    Database.insert(intake);

    intake = getIntake(intake.Id);
    intake.Status_Detail__c = STATUS_DETAIL_MAILOUT;

    Test.startTest();
    Database.update(intake);
    Test.stopTest();

    Intake__c requeriedIntake = getIntake(intake.Id);

    System.assertEquals(STATUS_DETAIL_MAILOUT, requeriedIntake.Status_Detail__c);
    System.assertEquals(null, requeriedIntake.Sign_Up_Method__c);
  }
}