public class mmffa_Deposits extends fflib_SObjectDomain{

    private Map<Id,Deposit__c> existingRecordsIdMap = new Map<Id,Deposit__c>();
    private List<Deposit__c> newDeposits;
    private Deposit_Service depositService;

    // Ctors
    public mmffa_Deposits() {
        super();
        this.newDeposits = new List<Deposit__c>();
        this.depositService = new Deposit_Service();
    }

    public mmffa_Deposits(List<Deposit__c> newDeposits) {
        super(newDeposits);
        this.newDeposits = (List<Deposit__c>) records;
        this.depositService = new Deposit_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new mmffa_Deposits(records);
        }
    }

    // Trigger Handlers
    public override void onBeforeInsert() {
        depositService.setDefaultFields(newDeposits);
        depositService.handleBankAccountOverrides(newDeposits);  
        depositService.validateTrustBalance(newDeposits);  
    }
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        depositService.setDefaultFields(newDeposits);
        depositService.handleBankAccountOverrides(newDeposits);
    }

    public override void onAfterInsert(){
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords){
    }
}