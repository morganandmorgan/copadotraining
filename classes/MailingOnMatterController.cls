public without sharing class MailingOnMatterController {

    public class DocData {
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String name {get;set;}
        @AuraEnabled
        public String category {get;set;}
        @AuraEnabled
        public String subCategory {get;set;}
        @AuraEnabled
        public String docTo {get;set;}
        @AuraEnabled
        public String docFrom {get;set;}
        @AuraEnabled
        public String createdDate {get;set;}
        @AuraEnabled
        public String createdBy {get;set;}
        @AuraEnabled
        public String lastModDate {get;set;}
        @AuraEnabled
        public String lastModBy {get;set;}
        @AuraEnabled
        public String filePath {get;set;}
    } //DocData

    public class RoleData {
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String name {get;set;}
        @AuraEnabled
        public String shippingStreet {get;set;}
        @AuraEnabled
        public String shippingCity {get;set;}
        @AuraEnabled
        public String shippingState {get;set;}
        @AuraEnabled
        public String shippingPostalCode {get;set;}
        @AuraEnabled
        public String role {get;set;}
        @AuraEnabled
        public String attn {get;set;}
        @AuraEnabled
        public Boolean returnEnvelope {get;set;}
    } //RoleData

    public class MailType {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String value {get;set;}
    } //MailType


    private static List<DocData> getLitifyDocs(Id matterId) {

        List<DocData> docDatas = new List<DocData>();

        for (litify_docs__file_info__c fileInfo : [SELECT id, name, litify_docs__Document_Category__c, litify_docs__Document_Subcategory__c, litify_docs__To__c, litify_docs__From__c, createdDate, createdBy.name, lastModifiedDate, lastModifiedBy.name FROM litify_docs__file_info__c WHERE litify_docs__Related_To__c = :matterId ORDER BY createdDate DESC]) {

            //for Litify, also include .doc and .docx
            if ( fileInfo.name != null && (fileInfo.name.containsIgnoreCase('.pdf') || fileInfo.name.containsIgnoreCase('.doc')) ) {
                DocData docData = new DocData();
                docData.id = fileInfo.id;

                docData.name = fileInfo.name;
                docData.category = fileInfo.litify_docs__Document_Category__c;
                docData.subCategory = fileInfo.litify_docs__Document_Subcategory__c;
                docData.docTo = fileInfo.litify_docs__To__c;
                docData.docFrom = fileInfo.litify_docs__From__c;
                docData.createdDate = fileInfo.createdDate.format('MM/dd/yyyy');
                docData.createdBy = fileInfo.createdBy.name;
                docData.lastModDate = fileINfo.lastModifiedDate.format('MM/dd/yyyy');
                docData.lastModBy = fileInfo.lastModifiedBy.name;

                docDatas.add(docData);
            } //if .pdf

        } //for file_info
        
        return docDatas;

    } //getLitifyDocs


    private static List<DocData> getCpDocs(Id matterId) {
        String mailData = CPDataProvider.GetMailData(matterId);
        
        Map<String,List<Map<String,String>>> mailDataMap = (Map<String,List<Map<String,String>>>)Json.deserialize(mailData, Map<String,List<Map<String,String>>>.class);
        
        //List<Map<String,String>> parties = mailDataMap.get('parties');
        List<Map<String,String>> documents = mailDataMap.get('documents');
        
        List<DocData> docDatas = new List<DocData>();

        //{"documentId":"Tampa-11970467","docNo":1,"dateSent":"5/24/2018 8:23:55 AM","from":"CCC","to":"FILE","re":"CCC Intake",
        //"docType":"","subType":"","filePath":"\\\\TAM-CP1\\CPWin\\History\\SALESFORCE\\5863C.01"},

        for (Map<String,String> doc : documents) {

            if ( doc.get('re') != null && doc.get('re').containsIgnoreCase('.pdf') ) {
                DocData docData = new DocData();
                docData.id = doc.get('documentId');

                docData.name = doc.get('re');
                docData.category = doc.get('docType');
                docData.subCategory = doc.get('subType');
                docData.docTo = doc.get('to');
                docData.docFrom = doc.get('from');

                String createdDate = '';
                if (doc.get('dateSent') != null) createdDate = doc.get('dateSent').split(' ')[0];
                docData.createdDate = createdDate;
                docData.createdBy = doc.get('createUser');
                
                String lastModDate = '';
                if (doc.get('maintDate') != null) lastModDate = doc.get('maintDate').split(' ')[0];
                docData.lastModDate = lastModDate;
                docData.lastModBy = doc.get('maintUser');

                docData.filePath = doc.get('filePath');

                docDatas.add(docData);
            } //if .pdf

        } //for file_info
        
        return docDatas;

    } //getCpDocs


    @AuraEnabled(cacheable=true)
    public static List<DocData> getDocData(Id matterId) {
        litify_pm__Matter__c matter = [SELECT id, CP_Active_Case_Indicator__c FROM litify_pm__Matter__c WHERE id = :matterId];

        if (matter.CP_Active_Case_Indicator__c == 'Y') {
            return getCpDocs(matterId);
        } else {
            return getLitifyDocs(matterId);
        }
    } //getDocData


    private static List<RoleData> getLitifyRoles(Id matterId) {
        List<RoleData> roleDatas = new List<RoleData>();
        for (litify_pm__Role__c role : [SELECT id, name, name__c, litify_pm__Matter__c, litify_pm__Party__r.shippingStreet, litify_pm__Party__r.shippingCity, litify_pm__Party__r.shippingState, litify_pm__Party__r.shippingPostalCode, litify_pm__Role__c FROM litify_pm__Role__c WHERE litify_pm__Matter__c = :matterId]) {
            RoleData roleData = new RoleData();
            roleData.id = role.id;
            roleData.name = role.name__c;
            roleData.shippingStreet = role.litify_pm__Party__r.shippingStreet;
            roleData.shippingCity = role.litify_pm__Party__r.shippingCity;
            roleData.shippingState = role.litify_pm__Party__r.shippingState;
            roleData.shippingPostalCode = role.litify_pm__Party__r.shippingPostalCode;
            roleData.role = role.litify_pm__Role__c;

            roleData.attn = '';
            roleData.returnEnvelope = false;

            roleDatas.add(roleData);
        }

        return roleDatas;
    } //getLitifyRoles


    private static List<RoleData> getCpParties(Id matterId) {
        String mailData = CPDataProvider.GetMailData(matterId);
        
        Map<String,List<Map<String,String>>> mailDataMap = (Map<String,List<Map<String,String>>>)Json.deserialize(mailData, Map<String,List<Map<String,String>>>.class);
        
        List<Map<String,String>> parties = mailDataMap.get('parties');
        //List<Map<String,String>> documents = mailDataMap.get('documents');
        
        //{"name":"Raymond Bleday","address":"410 W. 19th Street","city":"Panama City","state":"FL","zip":"32405","role":"Doctor"},

        List<RoleData> roleDatas = new List<RoleData>();
        Integer index = 1;
        for (Map<String,String> party : parties) {
            RoleData roleData = new RoleData();
            roleData.id = 'CP' + String.valueOf(index); //we need unique id's for the UI
            index++;
            roleData.name = party.get('name');
            roleData.shippingStreet = party.get('address');
            roleData.shippingCity = party.get('city');
            roleData.shippingState = party.get('state');
            roleData.shippingPostalCode = party.get('zip');
            roleData.role = party.get('role');

            roleData.attn = '';
            roleData.returnEnvelope = false;

            roleDatas.add(roleData);
        }

        return roleDatas;

    } //getCpParties


    @AuraEnabled(cacheable=true)
    public static List<RoleData> getRoleData(Id matterId) {
        litify_pm__Matter__c matter = [SELECT id, CP_Active_Case_Indicator__c FROM litify_pm__Matter__c WHERE id = :matterId];

        if (matter.CP_Active_Case_Indicator__c == 'Y') {
            return getCpParties(matterId);
        } else {
            return getLitifyRoles(matterId);
        }
    } //getRoleData


    @AuraEnabled(cacheable=true)
    public static List<MailType> getMailTypeOptions() {
        Schema.DescribeFieldResult fieldResult = Mailing__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        
        List<MailType> mailTypeOptions = new List<MailType>();
        for (Schema.PicklistEntry entry : ple ) {
        
            MailType option = New MailType();
            option.label = entry.getLabel();
            option.value = entry.getValue();
        
            mailTypeOptions.add(option);
        }
        
        return mailTypeOptions;
                
    } //getMailTypeOptions


    public static String createDocumentsJson(Boolean cpCase, List<DocData> docDatas) {

        List<Map<String,String>> documents = new List<Map<String,String>>();

        for (DocData docData : docDatas) {
            Map<String,String> docMap = new Map<String,String>();
            if (cpCase) {
                docMap.put('filePath',docData.filePath);
            } else {
                docMap.put('id',docData.id);
            } //if cpCase
            documents.add(docMap);
        }

        return Json.serialize(documents);
    } //createDocumentsJson


    @AuraEnabled(cacheable=false)
    public static List<String> sendMail(String matterId, String jsDocuments, String jsRoles, String mailType) {

        //String body = 'jsDocuments\n' + jsDocuments + '\njsRoles\n' + jsRoles;
        //sendEmail(body);

        List<DocData> docDatas = (List<DocData>)Json.deserialize(jsDocuments, List<DocData>.class);
        List<RoleData> roleDatas = (List<RoleData>)Json.deserialize(jsRoles, List<RoleData>.class);

        litify_pm__Matter__c matter = [SELECT id, CP_Active_Case_Indicator__c FROM litify_pm__Matter__c WHERE id = :matterId];

        Boolean cpCase = (matter.CP_Active_Case_Indicator__c == 'Y');

        List<Mailing__c> mailings = new List<Mailing__c>();
        for (RoleData roleData : roleDatas) {

            Mailing__c m = new Mailing__c();

            m.documents__c = createDocumentsJson(cpCase, docDatas);

            //m.mailing_Job_ID__c
            m.matter__c = matterId;

            m.recipient_Name__c = roleData.name;
            m.attn__c = roleData.attn;
            m.recipient_Street__c = roleData.shippingStreet;
            m.recipient_City__c = roleData.shippingCity;
            m.recipient_State__c = roleData.shippingState;
            m.recipient_Postal_Code__c = roleData.shippingPostalCode;
            m.type__c = mailType;
            if (roleData.returnEnvelope != null) {
                m.return_envelope__c = Boolean.valueOf(roleData.returnEnvelope);
            }

            m.status__c = 'Pending';

            mailings.add(m);

        }

        insert mailings;

        List<String> results = new List<String>();
        results.add( 'complete' );

        return results;

    } //sendMail



    /* for debugging
    public static void sendEmail(String body) {

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses( new String[] {'mterrill@forthepeople.com'});

        mail.setSubject('MailingOnMatter.cls');

        mail.setPlainTextBody(body);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    } //sendEmail */



} //MailingOnMatterController