/**
 *  Apex Unit Tests related to the mmlitifylrn_CredentialsInputController
 */
@isTest
public class mmlitifylrn_CredentialsInputControlTest
{
    // Mocks needed:
    //  mmlitifylrn_FirmsSelector
    //  mmlitifylrn_AuthenticationsService
    //      throwing of mmlitifylrn_Exceptions.AuthenticationsServiceException
    //      throwing of Exception
    //
    // Methods to test
    //  performAuthentication
    //  credentials attribute
    //  constructor
    private static fflib_ApexMocks mocks = new fflib_ApexMocks();
    private static mmlitifylrn_IFirmsSelector mockFirmsSelector = (mmlitifylrn_IFirmsSelector) mocks.mock(mmlitifylrn_IFirmsSelector.class);
    private static mmlitifylrn_IAuthenticationsService mockAuthenticationsService = (mmlitifylrn_IAuthenticationsService) mocks.mock(mmlitifylrn_IAuthenticationsService.class);

    static
    {
        mocks.startStubbing();
        mocks.when(mockFirmsSelector.sObjectType()).thenReturn(litify_pm__Firm__c.SObjectType);
        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockFirmsSelector);
        mm_Application.Service.setMock(mmlitifylrn_IAuthenticationsService.class, mockAuthenticationsService);
    }

    @isTest
    private static void whenCredentialsAreAccessed()
    {
        Id credential01Id = fflib_IDGenerator.generate( mmlitifylrn_ThirdPartyCredential__c.SObjectType );
    }
/*
    @isTest
    private static void whenIdNotProvidedItUsesCurrentUser()
    {

        List<Contact> reports = makeContactsWithSkillRatings(new Map<Contact, Integer>());

        mocks.startStubbing();
        mocks.when(mockSelector.getCurrentUserResourceId()).thenReturn(currentResourceId);
        mocks.when(mockService.getAllReportingResources(currentResourceId)).thenReturn(reports);
        mocks.stopStubbing();

        ApexPages.StandardController stdController = new ApexPages.StandardController(new Contact());
        psaext_DirectReportsControllerExtension controller = new psaext_DirectReportsControllerExtension(stdController);



        ((psaext_IDirectReportsService)mocks.verify(mockService)).getAllReportingResources(currentResourceId);
    }

*/
}