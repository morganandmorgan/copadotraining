@isTest
private class IntakeGeneratingAttorneyTest
{

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account generatingAttorneyAccount = TestUtil.createAccount('Morgan & Morgan P.A.');
        toInsert.add(generatingAttorneyAccount);

        Database.insert(toInsert);
        toInsert.clear();

        Contact generatingAttorneyA = new Contact(
                LastName = 'Attorney A',
                Title = 'Attorney',
                Email = 'generatingAttorneyA@Email.net'
            );
        toInsert.add(generatingAttorneyA);
        Contact generatingAttorneyB = new Contact(
                LastName = 'Attorney B',
                Title = 'Attorney',
                Email = 'generatingAttorneyB@Email.net'
            );
        toInsert.add(generatingAttorneyB);

        Database.insert(toInsert);
        toInsert.clear();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static Map<String, Cisco_Queue_Setting__mdt> getCiscoQueueSettings()
    {
        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap =
            new Map<String, Cisco_Queue_Setting__mdt>();

        for (Cisco_Queue_Setting__mdt cqs : mmintake_CiscoQueueSettingsSelector.newInstance().selectAll())
        {
            ciscoQueueSettingsMap.put(cqs.DeveloperName, cqs);
        }

        return ciscoQueueSettingsMap;
    }

    private static Account getClient() {
        return [SELECT Id FROM Account WHERE IsPersonAccount = true];
    }

    private static Incident__c getIncident() {
        return [SELECT Id FROM Incident__c];
    }

    private static final Integer INTAKES_TO_DUPLICATE = 10;

    private static List<Intake__c> buildIntakes(Account client, Incident__c incident, Cisco_Queue_Setting__mdt toMatch) {

        List<Intake__c> intakes = new List<Intake__c>();
        Date today = Date.today();
        for (Integer i = 0; i < INTAKES_TO_DUPLICATE; ++i) {
            intakes.add(new Intake__c(
                    Client__c = client.Id,
                    Incident__c = incident.Id,
                    Cisco_Queue__c = toMatch.Cisco_Queue__c,
                    Marketing_Source__c = toMatch.Marketing_Source__c,
                    Venue__c = toMatch.Venue__c,
                    County_of_Incident__c = toMatch.County__c,
                    Generating_Attorney__c = null
                ));
        }
        return intakes;
    }

    @isTest
    private static void testInsert_SuccessfulMatch() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Test.startTest();
        Database.insert(toInsert);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assert(queueInfoA.GeneratingAttorney__c.equalsIgnoreCase(requeriedIntake.Generating_Attorney__r.Email));
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assert(queueInfoB.GeneratingAttorney__c.equalsIgnoreCase(requeriedIntake.Generating_Attorney__r.Email));
        }
    }

    @isTest
    private static void testInsert_MismatchQueue() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesA) {
            intake.Cisco_Queue__c = '0000';
        }

        for (Intake__c intake : intakesB) {
            intake.Cisco_Queue__c = '0000';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Test.startTest();
        Database.insert(toInsert);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testInsert_MismatchMarketingSource() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesB) {
            intake.Marketing_Source__c = 'Not a valid marketing source';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Test.startTest();
        Database.insert(toInsert);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assert(queueInfoA.GeneratingAttorney__c.equalsIgnoreCase(requeriedIntake.Generating_Attorney__r.Email));
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testInsert_MismatchVenue() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesA) {
            intake.Venue__c += ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.Venue__c += ' MISMATCH';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Test.startTest();
        Database.insert(toInsert);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testInsert_MismatchCounty() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesA) {
            intake.County_of_incident__c += ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.County_of_incident__c += ' MISMATCH';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Test.startTest();
        Database.insert(toInsert);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testUpdate_SuccessfulMatch() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Database.insert(toInsert);

        Test.startTest();
        Database.update([SELECT Id FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assert(queueInfoA.GeneratingAttorney__c.equalsIgnoreCase(requeriedIntake.Generating_Attorney__r.Email));
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assert(queueInfoB.GeneratingAttorney__c.equalsIgnoreCase(requeriedIntake.Generating_Attorney__r.Email));
        }
    }

    @isTest
    private static void testUpdate_MismatchQueue() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesA) {
            intake.Cisco_Queue__c = '0000';
        }

        for (Intake__c intake : intakesB) {
            intake.Cisco_Queue__c = '0000';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Database.insert(toInsert);

        Test.startTest();
        Database.update([SELECT Id FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testUpdate_MismatchMarketingSource() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesB) {
            intake.Marketing_Source__c = 'Not a valid marketing source';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Database.insert(toInsert);

        Test.startTest();
        Database.update([SELECT Id FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assert(queueInfoA.GeneratingAttorney__c.equalsIgnoreCase(requeriedIntake.Generating_Attorney__r.Email));
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testUpdate_MismatchVenue() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesA) {
            intake.Venue__c += ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.Venue__c += ' MISMATCH';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Database.insert(toInsert);

        Test.startTest();
        Database.update([SELECT Id FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }

    @isTest
    private static void testUpdate_MismatchCounty() {
        Account client = getClient();
        Incident__c incident = getIncident();

        Map<String, Cisco_Queue_Setting__mdt> ciscoQueueSettingsMap = getCiscoQueueSettings();

        Cisco_Queue_Setting__mdt queueInfoA = ciscoQueueSettingsMap.get('Unit_Test_Queue_A');
        Cisco_Queue_Setting__mdt queueInfoB = ciscoQueueSettingsMap.get('Unit_Test_Queue_B');

        List<Intake__c> intakesA = buildIntakes(client, incident, queueInfoA);
        List<Intake__c> intakesB = buildIntakes(client, incident, queueInfoB);

        for (Intake__c intake : intakesA) {
            intake.County_of_incident__c += ' MISMATCH';
        }

        for (Intake__c intake : intakesB) {
            intake.County_of_incident__c += ' MISMATCH';
        }

        List<Intake__c> toInsert = new List<Intake__c>();
        toInsert.addAll(intakesA);
        toInsert.addAll(intakesB);

        Database.insert(toInsert);

        Test.startTest();
        Database.update([SELECT Id FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);
        Test.stopTest();

        Map<Id, Intake__c> requeriedIntakes = new Map<Id, Intake__c>([SELECT Id, Generating_Attorney__r.Email FROM Intake__c WHERE Id IN :intakesA OR Id IN :intakesB]);

        for (Intake__c intake : intakesA) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }

        for (Intake__c intake : intakesB) {
            Intake__c requeriedIntake = requeriedIntakes.get(intake.Id);
            System.assertEquals(null, requeriedIntake.Generating_Attorney__r.Email);
        }
    }
}