/**
 * TransactionLineItem_Domain_Test
 * @description Test for Transaction Line Domain
 * @author CLD Partners
 * @date 2/20/2019
 */
@IsTest
public with sharing class TransactionLineItem_Domain_Test {

    @IsTest
    private static void ctor() {
        TransactionLineItem_Domain tliSelector = new TransactionLineItem_Domain();
        System.assert(tliSelector != null);
    }

    @IsTest
    private static void testTrigger() {
    	Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.journals;
    }
}