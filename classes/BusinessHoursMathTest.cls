@isTest
private class BusinessHoursMathTest {

    private static BusinessHours getDefaultBusinessHours() {
        return [
            SELECT 
                MondayEndTime, 
                MondayStartTime, 
                TuesdayEndTime, 
                TuesdayStartTime, 
                WednesdayEndTime, 
                WednesdayStartTime, 
                ThursdayEndTime, 
                ThursdayStartTime, 
                FridayEndTime, 
                FridayStartTime, 
                SaturdayEndTime, 
                SaturdayStartTime, 
                SundayEndTime, 
                SundayStartTime 
            FROM BusinessHours 
            WHERE IsDefault=true
        ];
    }

    private static BusinessHours createNonDefaultBusinessHours() {
        BusinessHours newBusinessHours = new BusinessHours(
                MondayEndTime = Time.newInstance(17, 0, 0, 0), 
                MondayStartTime = Time.newInstance(8, 0, 0, 0), 
                TuesdayEndTime = Time.newInstance(16, 0, 0, 0), 
                TuesdayStartTime = Time.newInstance(7, 0, 0, 0), 
                WednesdayEndTime = Time.newInstance(15, 0, 0, 0), 
                WednesdayStartTime = Time.newInstance(6, 0, 0, 0), 
                ThursdayEndTime = Time.newInstance(14, 0, 0, 0), 
                ThursdayStartTime = Time.newInstance(5, 0, 0, 0), 
                FridayEndTime = Time.newInstance(18, 0, 0, 0), 
                FridayStartTime = Time.newInstance(9, 0, 0, 0), 
                SaturdayEndTime = Time.newInstance(14, 0, 0, 0), 
                SaturdayStartTime = Time.newInstance(11, 0, 0, 0), 
                SundayEndTime = Time.newInstance(16, 0, 0, 0), 
                SundayStartTime = Time.newInstance(14, 0, 0, 0),
                IsDefault = false
            );
        return newBusinessHours;
    }

    private static List<Holiday> createNonRecurringHolidays() {
        List<Holiday> holidays = new List<Holiday>();
        holidays.add(new Holiday(
                ActivityDate = Date.today(),
                IsAllDay = true,
                IsRecurrence = false,
                Name = 'Sample Holiday 1'
            ));
        holidays.add(new Holiday(
                ActivityDate = Date.today().addDays(1),
                IsAllDay = true,
                IsRecurrence = false,
                Name = 'Sample Holiday 2'
            ));
        return holidays;
    }

    private static List<Holiday> createRecurringHolidays() {
        List<Holiday> holidays = new List<Holiday>();
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample January Holiday',
                RecurrenceMonthOfYear = 'January',
                RecurrenceDayOfMonth = 1
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample February Holiday',
                RecurrenceMonthOfYear = 'February',
                RecurrenceDayOfMonth = 2
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample March Holiday',
                RecurrenceMonthOfYear = 'March',
                RecurrenceDayOfMonth = 3
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample April Holiday',
                RecurrenceMonthOfYear = 'April',
                RecurrenceDayOfMonth = 4
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample May Holiday',
                RecurrenceMonthOfYear = 'May',
                RecurrenceDayOfMonth = 5
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample June Holiday',
                RecurrenceMonthOfYear = 'June',
                RecurrenceDayOfMonth = 6
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample July Holiday',
                RecurrenceMonthOfYear = 'July',
                RecurrenceDayOfMonth = 7
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample August Holiday',
                RecurrenceMonthOfYear = 'August',
                RecurrenceDayOfMonth = 8
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample September Holiday',
                RecurrenceMonthOfYear = 'September',
                RecurrenceDayOfMonth = 9
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample October Holiday',
                RecurrenceMonthOfYear = 'October',
                RecurrenceDayOfMonth = 10
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample November Holiday',
                RecurrenceMonthOfYear = 'November',
                RecurrenceDayOfMonth = 11
            ));
        holidays.add(new Holiday(
                IsAllDay = true,
                IsRecurrence = true,
                RecurrenceType = 'RecursYearly',
                Name = 'Sample December Holiday',
                RecurrenceMonthOfYear = 'December',
                RecurrenceDayOfMonth = 12
            ));
        return holidays;
    }

    private static void assertBusinessDays(List<BusinessHoursMath.Day> businessDays, BusinessHours hoursRecord) {
        System.assertEquals(7, businessDays.size());

        System.assertEquals(hoursRecord.SundayStartTime != null, businessDays.get(0).isBusinessDay);
        System.assertEquals(hoursRecord.SundayStartTime, businessDays.get(0).startTime);
        System.assertEquals(hoursRecord.SundayEndTime, businessDays.get(0).endTime);

        System.assertEquals(hoursRecord.MondayStartTime != null, businessDays.get(1).isBusinessDay);
        System.assertEquals(hoursRecord.MondayStartTime, businessDays.get(1).startTime);
        System.assertEquals(hoursRecord.MondayEndTime, businessDays.get(1).endTime);

        System.assertEquals(hoursRecord.TuesdayStartTime != null, businessDays.get(2).isBusinessDay);
        System.assertEquals(hoursRecord.TuesdayStartTime, businessDays.get(2).startTime);
        System.assertEquals(hoursRecord.TuesdayEndTime, businessDays.get(2).endTime);

        System.assertEquals(hoursRecord.WednesdayStartTime != null, businessDays.get(3).isBusinessDay);
        System.assertEquals(hoursRecord.WednesdayStartTime, businessDays.get(3).startTime);
        System.assertEquals(hoursRecord.WednesdayEndTime, businessDays.get(3).endTime);

        System.assertEquals(hoursRecord.ThursdayStartTime != null, businessDays.get(4).isBusinessDay);
        System.assertEquals(hoursRecord.ThursdayStartTime, businessDays.get(4).startTime);
        System.assertEquals(hoursRecord.ThursdayEndTime, businessDays.get(4).endTime);

        System.assertEquals(hoursRecord.FridayStartTime != null, businessDays.get(5).isBusinessDay);
        System.assertEquals(hoursRecord.FridayStartTime, businessDays.get(5).startTime);
        System.assertEquals(hoursRecord.FridayEndTime, businessDays.get(5).endTime);

        System.assertEquals(hoursRecord.SaturdayStartTime != null, businessDays.get(6).isBusinessDay);
        System.assertEquals(hoursRecord.SaturdayStartTime, businessDays.get(6).startTime);
        System.assertEquals(hoursRecord.SaturdayEndTime, businessDays.get(6).endTime);
    }
	
    @isTest
    private static void configureBusinessWeekUsesDefaultBusinessHours() {
        BusinessHours hoursRecord = getDefaultBusinessHours();

        BusinessHoursMath mathInstance = new BusinessHoursMath();

        Test.startTest();
        BusinessHoursMath returnValue = mathInstance.configureBusinessWeek();
        Test.stopTest();

        System.assertEquals(hoursRecord.Id, mathInstance.businessHours.Id);
        System.assert(mathInstance === returnValue);

        assertBusinessDays(mathInstance.businessDays, hoursRecord);
    }

    @isTest
    private static void setBusinessHours() {
        BusinessHours newBusinessHours = createNonDefaultBusinessHours();

        BusinessHoursMath mathInstance = new BusinessHoursMath();

        Test.startTest();
        BusinessHoursMath returnValue = mathInstance.setBusinessHours(newBusinessHours);
        Test.stopTest();

        System.assert(newBusinessHours === mathInstance.businessHours);
        System.assert(mathInstance === returnValue);

        assertBusinessDays(mathInstance.businessDays, newBusinessHours);
    }

    @isTest
    private static void setHolidays() {
        List<Holiday> holidays = createNonRecurringHolidays();

        BusinessHoursMath mathInstance = new BusinessHoursMath();

        Test.startTest();
        BusinessHoursMath returnValue = mathInstance.setHolidays(holidays);
        Test.stopTest();

        System.assert(holidays === mathInstance.holidays);
        System.assert(mathInstance === returnValue);
    }

    @isTest
    private static void isBusinessDay_NonRecurringHoliday_True() {
        BusinessHours newBusinessHours = createNonDefaultBusinessHours();
        List<Holiday> holidays = createNonRecurringHolidays();

        BusinessHoursMath mathInstance = new BusinessHoursMath();
        mathInstance.setBusinessHours(newBusinessHours).setHolidays(holidays);

        Date selectedDate = holidays.get(0).ActivityDate;

        Test.startTest();
        Boolean returnValue = mathInstance.isBusinessDay(selectedDate);
        Test.stopTest();

        System.assert(!returnValue);
    }

    @isTest
    private static void isBusinessDay_NoMatchingHoliday_True() {
        BusinessHours newBusinessHours = createNonDefaultBusinessHours();
        List<Holiday> holidays = createNonRecurringHolidays();

        BusinessHoursMath mathInstance = new BusinessHoursMath();
        mathInstance.setBusinessHours(newBusinessHours).setHolidays(holidays);

        Date selectedDate = holidays.get(0).ActivityDate.addDays(-1);

        Test.startTest();
        Boolean returnValue = mathInstance.isBusinessDay(selectedDate);
        Test.stopTest();

        System.assert(returnValue);
    }

    @isTest
    private static void isBusinessDay_IsRecurringHolidays() {
        BusinessHours newBusinessHours = createNonDefaultBusinessHours();
        List<Holiday> holidays = createRecurringHolidays();

        BusinessHoursMath mathInstance = new BusinessHoursMath();
        mathInstance.setBusinessHours(newBusinessHours).setHolidays(holidays);

        Test.startTest();
        for (Integer i = 1; i <= holidays.size(); ++i) {
            Date selectedDate = Date.newInstance(Date.today().year(), i, i);

            Boolean returnValue = mathInstance.isBusinessDay(selectedDate);

            System.assert(!returnValue);
        }
        Test.stopTest();
    }

    @isTest
    private static void getEndOfBusinessDay() {
        BusinessHours newBusinessHours = createNonDefaultBusinessHours();
        List<Holiday> holidays = new List<Holiday>();

        BusinessHoursMath mathInstance = new BusinessHoursMath();
        mathInstance.setBusinessHours(newBusinessHours).setHolidays(holidays);

        Datetime selectedDate = Datetime.newInstance(2016, 8, 1, 0, 0, 0); // Monday

        Test.startTest();
        Datetime returnValue = mathInstance.getEndOfBusinessDay(selectedDate);
        Test.stopTest();

        System.assertEquals(selectedDate.date(), returnValue.date());
        System.assertEquals(mathInstance.businessHours.MondayEndTime, returnValue.time());
    }

    @isTest
    private static void getEndOfBusinessDayFromDate() {
        BusinessHours newBusinessHours = createNonDefaultBusinessHours();
        List<Holiday> holidays = new List<Holiday>();

        BusinessHoursMath mathInstance = new BusinessHoursMath();
        mathInstance.setBusinessHours(newBusinessHours).setHolidays(holidays);

        Date selectedDate = Date.newInstance(2016, 8, 1); // Monday
        Integer numberOfDays = 3;

        Test.startTest();
        Datetime returnValue = mathInstance.getEndOfBusinessDayFromDate(selectedDate, numberOfDays);
        Test.stopTest();

        System.assertEquals(selectedDate.addDays(numberOfDays), returnValue.date());
        System.assertEquals(mathInstance.businessHours.ThursdayEndTime, returnValue.time());
    }
}