/**
 * SyncMatterTeamMembersToLawsuitQueuableTe
 * @description 
 * @author Jeff Watson
 * @date 1/12/2019
 */
@IsTest
public with sharing class SyncMatterTeamMembersToLawsuitQueuableTe {

    private static List<litify_pm__Matter_Team_Member__c> matterTeamMembers = new List<litify_pm__Matter_Team_Member__c>();

    public static testMethod void SyncMatterTeamMembersToLawsuitQueuable_Test() {

        SyncMatterTeamMembersToLawsuitQueuable syncMatterTeamMembersToLawsuitQueuable = new SyncMatterTeamMembersToLawsuitQueuable(matterTeamMembers);
        system.enqueueJob(syncMatterTeamMembersToLawsuitQueuable);
    }
}