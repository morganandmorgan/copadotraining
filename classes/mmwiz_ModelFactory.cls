public class mmwiz_ModelFactory
{
    public static mmwiz_AbstractBaseModel generateFromWizard( Wizard__mdt wizardRecord )
    {
        mmwiz_AbstractBaseModel outputModel = null;

        if ( wizardRecord != null )
        {
            try
            {
                outputModel = (mmwiz_AbstractBaseModel) Type.forName( wizardRecord.Type__c ).newInstance();

                outputModel.initialize( wizardRecord );
            }
            catch ( Exception e )
            {

            }
        }

        return outputModel;
    }

    public static mmwiz_AnswerModel generateAnswerModel( String token, String displayText )
    {
        mmwiz_AnswerModel answerModel = new mmwiz_AnswerModel();

        answerModel.setDisplayText(displayText);
        answerModel.setToken(token);

        return answerModel;
    }

    public static mmwiz_AnswerModel generateAnswerModel( String displayText )
    {
        return generateAnswerModel( mmlib_Utils.generateGuid() , displayText);
    }

    public static mmwiz_QuestionModel generateQuestionModel( String token, String displayText )
    {
        mmwiz_QuestionModel questionModel = new mmwiz_QuestionModel();

        questionModel.setDisplayText( displayText );
        questionModel.setToken( token );

        return questionModel;
    }

    public static mmwiz_PageModel generatePageModel( String token, String displayText )
    {
        mmwiz_PageModel pageModel = new mmwiz_PageModel();

        pageModel.setDisplayText( displayText );
        pageModel.setToken( token );

        return pageModel;
    }

    public static mmwiz_QuestionnaireModel generateQuestionnaireModel( String token, String displayText )
    {
        mmwiz_QuestionnaireModel questionnaireModel = new mmwiz_QuestionnaireModel();

        questionnaireModel.setDisplayText( displayText );
        questionnaireModel.setToken( token );

        return questionnaireModel;
    }

    public static mmwiz_ActionDisplayTextModel generateActionModelDisplayedTextType( String token, String displayText )
    {
        mmwiz_ActionDisplayTextModel actionModel = new mmwiz_ActionDisplayTextModel();

        actionModel.setDisplayText( displayText );
        actionModel.setToken( token );

        return actionModel;
    }

    public static mmwiz_ActionDisplayLinkModel generateActionModelDisplayedTextType( String token, String displayText, String hrefString )
    {
        mmwiz_ActionDisplayLinkModel actionModel = new mmwiz_ActionDisplayLinkModel();

        actionModel.setDisplayText( displayText );
        actionModel.setToken( token );
        actionModel.setHref( hrefString );

        return actionModel;
    }
}