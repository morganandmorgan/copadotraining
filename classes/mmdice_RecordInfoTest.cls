@isTest
public with sharing class mmdice_RecordInfoTest
{
    static testMethod void mmdice_RecordInfo_Ctor_Test()
    {
        // Arrange + Act
        Test.startTest();
        mmdice_RecordInfo record = new mmdice_RecordInfo();
        Test.stopTest();

        // Assert
        System.assert(record != null);
    }

    static testMethod void mmdice_RecordInfo_Ctor_Override_Test()
    {
        // Arrange
        Intake__c intakeSObj = new Intake__c(Id = 'a003D000001dPZAAA1');
        intakeSObj.Client__r = new Account(FirstName = 'Kenny', LastName = 'Chesney', BillingCity = 'Nashville', BillingState = 'TN', BillingPostalCode = '11111', PersonMobilePhone = '111-111-1111', Phone = '111-111-2222', Work_Phone__c = '111-111-3333');

        // Act
        Test.startTest();
        mmdice_RecordInfo result = new mmdice_RecordInfo(intakeSObj);
        Test.stopTest();

        // Assert
        System.assert(result != null);
        System.assert(result.Id != null);
        System.assertEquals(intakeSObj.Client__r.FirstName, result.first_name);
        System.assertEquals(intakeSObj.Client__r.LastName, result.last_name);
        System.assertEquals(intakeSObj.Client__r.BillingCity, result.city);
        System.assertEquals(intakeSObj.Client__r.BillingState, result.state);
        System.assertEquals(intakeSObj.Client__r.BillingPostalCode, result.zip_code);
    }
}