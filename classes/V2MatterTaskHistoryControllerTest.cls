/**
 * V2MatterTaskHistoryControllerTest
 * @description Tests for V2MatterTaskHistoryController class.
 * @author Jeff Watson
 * @date 1/23/2019
 */
@IsTest
public with sharing class V2MatterTaskHistoryControllerTest {

    private static Account acct;
    private static litify_pm__Matter__c matter;

    static {
        acct = TestUtil.createAccountBusinessMorganAndMorgan('Biz Account');
        insert acct;

        Account officeAccount = TestUtil.createAccount('MM Office');
        officeAccount.Type = 'MM Office';
        insert officeAccount;

        Account personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        litify_pm__Case_Type__c caseType = TestUtil.createCaseType();
        insert caseType;

        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;

        matter = TestUtil.createMatter(acct);
        matter.litify_pm__Case_Type__c = caseType.Id;
        matter.Assigned_Office_Location__c = officeAccount.Id;
        matter.AssignedToMMBusiness__c = acct.Id;
        insert matter;
    }
    @IsTest
    private static void getRecs_ReturnsCorrectNumberOfDocs() {

        insert TestUtil.createTask(matter.Id, 'the subject', 'Completed', 'Normal', '', System.today());

        insert new MM_Document__c(Matter__c = matter.Id, File_Type__c = 'docx', From__c = 'Bill Gates', Document_Name__c = 'TestDoc', To__c = 'Steve Jobs', Document_Type__c = 'Correspondence', Document_Subtype__c = 'Expert');

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.Subject = 'JW TEST from Anonymous Apex';
        emailMessage.HtmlBody = '<h3>BAM</h3><br/><br/><b><i>Pretty cool!</i></b>';
        emailMessage.RelatedToId = matter.Id;
        insert emailMessage;

        Test.startTest();
        V2MatterTaskHistoryController.Model m = V2MatterTaskHistoryController.getRecs(matter.Id);
        Test.stopTest();

        System.assertEquals(3, m.items.size()); /* 1 task, 1 email, 1 document */

    }

    @isTest
    private static void updateField_Documentsubject_UpdatesField() {
        MM_Document__c doc = new MM_Document__c(Matter__c = matter.Id, File_Type__c = 'docx', From__c = 'Bill Gates', Document_Name__c = 'TestDoc', To__c = 'Steve Jobs', Document_Type__c = 'Correspondence', Document_Subtype__c = 'Expert');
        insert doc;

        Test.startTest();
        V2MatterTaskHistoryController.updateField(doc.Id, 'subject', 'NEW');
        Test.stopTest();

        doc = [SELECT Document_Name__c FROM MM_Document__c WHERE ID = :doc.id];
        System.assertEquals('NEW', doc.Document_Name__c);
    }
    @isTest
    private static void updateField_DocumentdocumentType_UpdatesField() {
        MM_Document__c doc = new MM_Document__c(Matter__c = matter.Id, File_Type__c = 'docx', From__c = 'Bill Gates', Document_Name__c = 'TestDoc', To__c = 'Steve Jobs', Document_Type__c = 'Correspondence', Document_Subtype__c = 'Expert');
        insert doc;

        Test.startTest();
        V2MatterTaskHistoryController.updateField(doc.Id, 'documentType', 'NEW');
        Test.stopTest();

        doc = [SELECT Document_Type__c FROM MM_Document__c WHERE ID = :doc.id];
        System.assertEquals('NEW', doc.Document_Type__c);
    }
    @isTest
    private static void updateField_DocumentdocumentSubtype_UpdatesField() {
        MM_Document__c doc = new MM_Document__c(Matter__c = matter.Id, File_Type__c = 'docx', From__c = 'Bill Gates', Document_Name__c = 'TestDoc', To__c = 'Steve Jobs', Document_Type__c = 'Correspondence', Document_Subtype__c = 'Expert');
        insert doc;

        Test.startTest();
        V2MatterTaskHistoryController.updateField(doc.Id, 'documentSubtype', 'NEW');
        Test.stopTest();

        doc = [SELECT Document_Subtype__c FROM MM_Document__c WHERE ID = :doc.id];
        System.assertEquals('NEW', doc.Document_Subtype__c);
    }
    @isTest
    private static void updateField_documentFrom_UpdatesField() {
        MM_Document__c doc = new MM_Document__c(Matter__c = matter.Id, File_Type__c = 'docx', From__c = 'Bill Gates', Document_Name__c = 'TestDoc', To__c = 'Steve Jobs', Document_Type__c = 'Correspondence', Document_Subtype__c = 'Expert');
        insert doc;

        Test.startTest();
        V2MatterTaskHistoryController.updateField(doc.Id, 'documentFrom', 'NEW');
        Test.stopTest();

        doc = [SELECT From__c FROM MM_Document__c WHERE ID = :doc.id];
        System.assertEquals('NEW', doc.From__c);
    }
    @isTest
    private static void updateField_documentTo_UpdatesField() {
        MM_Document__c doc = new MM_Document__c(Matter__c = matter.Id, File_Type__c = 'docx', From__c = 'Bill Gates', Document_Name__c = 'TestDoc', To__c = 'Steve Jobs', Document_Type__c = 'Correspondence', Document_Subtype__c = 'Expert');
        insert doc;

        Test.startTest();
        V2MatterTaskHistoryController.updateField(doc.Id, 'documentTo', 'NEW');
        Test.stopTest();

        doc = [SELECT To__c FROM MM_Document__c WHERE ID = :doc.id];
        System.assertEquals('NEW', doc.To__c);
    }
    @isTest
    private static void updateField_EmailMessagedocumentType_UpdatesField() {
        EmailMessage eml = new EmailMessage(Subject = 'Test Email Subject', HtmlBody = '<html><body>Test Email Body</body></html>', TextBody = 'Test Email Body',
          FromName = 'Bill Gates', FromAddress = 'bill@microsoft.com', ToAddress = 'steve@apple.com', Email_Type__c = 'Type1');
        insert eml;

        System.debug('email type: ' + eml.Email_Type__c);
        Test.startTest();
        V2MatterTaskHistoryController.updateField(eml.Id, 'documentType', 'NEW');
        Test.stopTest();

        eml = [SELECT Email_Type__c FROM EmailMessage WHERE ID = :eml.id];
        System.assertEquals('NEW', eml.Email_Type__c);
    }
    @isTest
    private static void updateField_EmailMessagedocumentSubtype_UpdatesField() {
        EmailMessage eml = new EmailMessage(Subject = 'Test Email Subject', HtmlBody = '<html><body>Test Email Body</body></html>', TextBody = 'Test Email Body',
          FromName = 'Bill Gates', FromAddress = 'bill@microsoft.com', ToAddress = 'steve@apple.com', Email_Subtype2__c = 'Subtype1');
        insert eml;

        Test.startTest();
        V2MatterTaskHistoryController.updateField(eml.Id, 'documentSubtype', 'NEW');
        Test.stopTest();

        eml = [SELECT Email_Subtype2__c FROM EmailMessage WHERE ID = :eml.id];
        System.assertEquals('NEW', eml.Email_Subtype2__c);
    }
    @isTest
    private static void updateField_PassedObjectOtherThanMMDocument_Throws() {
        Task t = TestUtil.createTask(matter.Id, 'the subject', 'Completed', 'Normal', '', System.today());
        insert t;

        Test.startTest();
        try {
            V2MatterTaskHistoryController.updateField(t.Id, 'subject', 'NEW');
            System.assert(false, 'Expected NoAccessException');
        } catch (NoAccessException ex) {
            System.assert(true);
        } catch (Exception ex) {
            System.assert(false, 'Expected NoAccessException but received ' + ex);
        }
        Test.stopTest();
    }

    @isTest
    private static void getBodies_ReturnsBodiesForTasksAndEmails() {
        Task t = TestUtil.createTask(matter.Id, 'the subject', 'Completed', 'Normal', '', System.today());
        t.Description = 'Test Task Description';
        insert t;

        EmailMessage emailWithHtml = new EmailMessage(Subject = 'Test Email Subject', HtmlBody = '<html><body>Test Email Body</body></html>', TextBody = 'Test Email Body',
          FromName = 'Bill Gates', FromAddress = 'bill@microsoft.com', ToAddress = 'steve@apple.com');
        insert emailWithHtml;
        EmailMessage emailNoHtml = new EmailMessage(Subject = 'Test Email Subject', TextBody = 'Test Email Body',
          FromName = 'Bill Gates', FromAddress = 'bill@microsoft.com', ToAddress = 'steve@apple.com');
        insert emailNoHtml;

        Test.startTest();
        Map<Id, String> bodies = V2MatterTaskHistoryController.getBodies(new List<Id> {t.Id, emailWithHtml.Id, emailNoHtml.Id});
        Test.stopTest();

        System.assertEquals(3, bodies.size());
        System.assertEquals(bodies.get(t.Id), 'Test Task Description');
        System.assertEquals(bodies.get(emailWithHtml.Id), 'Test Email Body');
        System.assertEquals(bodies.get(emailNoHtml.Id), 'Test Email Body');
    }
}