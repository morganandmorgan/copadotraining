@isTest
private class EmailAlertsIIEControllerTest {
	
	@TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < 3; ++i) {
            intakes.add(new Intake__c(
                    Incident__c = incident.Id,
                    Client__c = client.Id
                ));
        }
        toInsert.addAll((List<SObject>) intakes);

        IncidentInvestigationEvent__c incidentInvestigation = new IncidentInvestigationEvent__c(
                Incident__c = incident.Id,
                EndDateTime__c = Datetime.now().addHours(1),
                StartDateTime__c = Datetime.now()
            );
        toInsert.add(incidentInvestigation);

        Database.insert(toInsert);
        toInsert.clear();

        List<IntakeInvestigationEvent__c> intakeInvestigations = new List<IntakeInvestigationEvent__c>();
        for (Intake__c intake : intakes) {
            intakeInvestigations.add(new IntakeInvestigationEvent__c(
                    IncidentInvestigation__c = incidentInvestigation.Id,
                    Intake__c = intake.Id,
                    StartDateTime__c = incidentInvestigation.StartDateTime__c,
                    EndDateTime__c = incidentInvestigation.EndDateTime__c
                ));
        }
        toInsert.addAll((List<SObject>) intakeInvestigations);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Intake__c> getIntakes() {
        return [SELECT Id, Client__c, Incident__c FROM Intake__c];
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents(List<Intake__c> intakes) {
        return [SELECT Id, Intake__c FROM IntakeInvestigationEvent__c WHERE Intake__c IN :intakes];
    }

    private static IncidentInvestigationEvent__c getIncidentInvestigationEvent() {
        return [SELECT Id FROM IncidentInvestigationEvent__c];
    }

    private static EmailAlertsIIEController buildController(Intake__c relatedIntake) {
        EmailAlertsIIEController controller = new EmailAlertsIIEController();
        controller.relatedIntakeId = relatedIntake.Id;
        return controller;
    }

    @isTest
    private static void initRelatedIntakeId() {
        List<Intake__c> intakes = getIntakes();
        
        Intake__c selectedIntake = intakes.get(0);

        Test.startTest();
        EmailAlertsIIEController controller = buildController(selectedIntake);
        Test.stopTest();

        System.assertEquals(selectedIntake.Id, controller.thisIntake.Id);
        System.assertEquals(selectedIntake.Client__c, controller.thisAccount.Id);
        System.assertEquals(null, controller.thisIntakeInvestigation);
        System.assertEquals(null, controller.thisIncident);
        System.assertEquals(selectedIntake.Id, controller.thisIntakeInvestigationEvent.Intake__c);
        System.assertEquals(null, controller.incidentInvestigationFieldSet);
        System.assertEquals(null, controller.intakeInvestigationFieldSet);
        System.assertNotEquals(null, controller.intakeFieldSet);
        System.assertEquals(null, controller.selectedInvestigation);
    }

    @isTest
    private static void getAllIncidentsForThisIntake() {
        List<Intake__c> intakes = getIntakes();
        
        Intake__c selectedIntake = intakes.get(0);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents(new List<Intake__c>{ selectedIntake });
        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEvent();

        EmailAlertsIIEController controller = buildController(selectedIntake);

        Test.startTest();
        List<IntakeInvestigationEvent__c> result = controller.getAllIncidentsForThisIntake();
        Test.stopTest();

        System.assertEquals(new Map<Id, IntakeInvestigationEvent__c>(intakeInvestigationEvents).keySet(), new Map<Id, IntakeInvestigationEvent__c>(result).keySet());
        System.assertEquals(incidentInvestigationEvent.Id, controller.selectedInvestigation);
    }

    @isTest
    private static void getAllIntakeInvestigationsForThisIncident() {
        List<Intake__c> intakes = getIntakes();
        
        Intake__c selectedIntake = intakes.get(0);

        Map<Id, Intake__c> relatedIntakes = new Map<Id, Intake__c>(intakes);
        relatedIntakes.remove(selectedIntake.Id);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents(relatedIntakes.values());
        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEvent();

        EmailAlertsIIEController controller = buildController(selectedIntake);
        controller.selectedInvestigation = incidentInvestigationEvent.Id;

        Test.startTest();
        List<IntakeInvestigationEvent__c> result = controller.getAllIntakeInvestigationsForThisIncident();
        Test.stopTest();

        System.assertEquals(new Map<Id, IntakeInvestigationEvent__c>(intakeInvestigationEvents).keySet(), new Map<Id, IntakeInvestigationEvent__c>(result).keySet());
    }
}