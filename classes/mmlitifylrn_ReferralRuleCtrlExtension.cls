public with sharing class mmlitifylrn_ReferralRuleCtrlExtension
{
    public List<SelectOption> fieldList { get; set; }

    public List<SelectOption> operatorOptions { get; set; }

    public List<SelectOption> formulaOptions { get; set; }

    public list<SelectOption> originatingFirmsList { get; private set; } { originatingFirmsList = mmlib_Utils.getSelectOptions( mmlitifylrn_FirmsSelector.newInstance().selectByDefaultAndSelectedThirdPartyReferralManaged(), true ); }

    public ReferralRule__c rule { get; set; }

    public List<mmlitifylrn_ReferralRulesMatchLogic.Query> queries { get; set; }

    public mmlitifylrn_ReferralRuleCtrlExtension(ApexPages.StandardController stdController)
    {
        ReferralRule__c stdObj = (ReferralRule__c)stdController.getRecord();

        if ( stdObj == null || ! String.isBlank(stdObj.Id) )
        {
            rule = mmlitifylrn_ReferralRulesSelector.newInstance().selectById( new set<id>{ stdObj.Id } )[0];

            queries = mmlitifylrn_ReferralRulesMatchLogic.parseQuery( rule.Query__c );
        }
        else
        {
            rule = new ReferralRule__c();

            queries = new List<mmlitifylrn_ReferralRulesMatchLogic.Query>();
        }

        fieldList = mmlib_UIHelper.getFieldList(Intake__c.sObjectType);

        operatorOptions = mmlib_UIHelper.getOperatorOptions();

        formulaOptions = mmlib_UIHelper.getFormulaOptions();

        while( queries.size() < 10 )
        {
            queries.add(new mmlitifylrn_ReferralRulesMatchLogic.Query());
        }
    }
}