/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Damage_PaymentTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Damage_PaymentTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Damage_Payment__c());
    }
}