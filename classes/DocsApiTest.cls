@isTest
public class DocsApiTest {

    @isTest
    public static void mainTest() {
        
        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;
        
        Intake__c intake = TestUtil.createIntake();
        insert intake;        

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        matter.intake__c = intake.id;
        //i can't insert these, I get errors, so we'll inject this

        ContentVersion version = new ContentVersion();
        version.PathOnClient = 'test';
        version.Title = 'TestDocument';
        version.VersionData = Blob.valueOf('test');
        version.IsMajorVersion = true;
        insert version;

        ContentDocument document = [
            SELECT Id, Title, FileType
            FROM ContentDocument 
            WHERE Id IN (
                SELECT ContentDocumentId 
                FROM ContentVersion 
                WHERE Id =: version.Id
            )
            LIMIT 1
        ];

        ContentDocumentLink link = new ContentDocumentLink (
            LinkedEntityId = intake.Id,
            ContentDocumentId = document.id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        insert link;
        
        DocsApi.DocumentAttachment attachment = new DocsApi.DocumentAttachment(
            document.Id, document.Title, intake.Id, document.FileType, version.VersionData);
        
        DocsApi.testMock(new List<litify_pm__Matter__c>{matter}, attachment.uniqueName, document.Id);
        
        Test.startTest();
        DocsApi.migrateIntakeDocuments(null);
        Test.stopTest();

        Set<Id> fileInfoIds = DocsApi.testFileInfoIds;
        System.assertEquals(1, fileInfoIds.size());
        System.assert(fileInfoIds.contains(document.Id));
    } //mainTest
   
    
    @isTest
    public static void multiMergeTest() {
        
        DocsApi.MergeData md = new DocsApi.mergeData();
        //md.relatedToId = ;
        md.relatedToApiName = 'litify_pm__Matter__c';
        md.category = 'Multi-Merge';
        md.subcategory = 'Role';
        md.fileType = DocsApi.defaultFileType;
        md.name = 'test.docx';
        //md.templateId = ;
        
        md.tags = new Map<String,Object>();
		md.tags.put('test_tag_123','data');

        DocsApi.multiMerge(new List<DocsApi.MergeData>{md});
        
    } //multiMergeTest

    
} //class