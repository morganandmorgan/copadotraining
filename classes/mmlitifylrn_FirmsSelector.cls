public class mmlitifylrn_FirmsSelector
    extends mmlib_SObjectSelector
    implements mmlitifylrn_IFirmsSelector
{
    public static mmlitifylrn_IFirmsSelector newInstance()
    {
        return (mmlitifylrn_IFirmsSelector) mm_Application.Selector.newInstance(litify_pm__Firm__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Firm__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField>
        {
            litify_pm__Firm__c.litify_pm__City__c,
            litify_pm__Firm__c.litify_pm__Country__c,
            litify_pm__Firm__c.litify_pm__ExternalId__c,
            litify_pm__Firm__c.litify_pm__Is_Available__c,
            litify_pm__Firm__c.litify_pm__Litify_com_ID__c,
            litify_pm__Firm__c.litify_pm__Location__c,
            litify_pm__Firm__c.litify_pm__Primary_Contact_Email__c,
            litify_pm__Firm__c.litify_pm__Primary_Contact_First_Name__c,
            litify_pm__Firm__c.litify_pm__Primary_Contact_Last_Name__c,
            litify_pm__Firm__c.litify_pm__Primary_Contact_Phone__c,
            litify_pm__Firm__c.litify_pm__State__c,
            litify_pm__Firm__c.mmlitifylrn_IsOutgoingReferralsManaged__c,
            litify_pm__Firm__c.mmlitifylrn_OutgoingReferralsManagedMap__c
        };
    }

    public List<litify_pm__Firm__c> selectById( Set<id> idSet )
    {
        return (List<litify_pm__Firm__c>) selectSObjectsById(idSet);
    }

    public List<litify_pm__Firm__c> selectBySelectedThirdPartyReferralManaged()
    {
        // setup the QueryFactory
        fflib_QueryFactory qf = newQueryFactory();

        // reset the default ordering
        qf.getOrderings().clear();

        return Database.query( qf.setCondition( litify_pm__Firm__c.mmlitifylrn_IsOutgoingReferralsManaged__c + ' = TRUE' )
                                 .addOrdering( litify_pm__Firm__c.Name, fflib_QueryFactory.SortOrder.ASCENDING, true )
                                 .toSOQL() );
    }

    public List<litify_pm__Firm__c> selectByDefaultAndSelectedThirdPartyReferralManaged()
    {
        string conditionClause = litify_pm__Firm__c.mmlitifylrn_IsOutgoingReferralsManaged__c + ' = TRUE';

        // get the default organization id from Litify
        try
        {
            conditionClause += ' or ' + litify_pm__Firm__c.litify_pm__ExternalId__c + ' = ' + litify_pm__Public_Setup__c.getInstance().litify_pm__litify_organization_id__c;
        }
        catch ( Exception e )
        {
            // if there is a problem getting this ID, then just
        }

        // setup the QueryFactory
        fflib_QueryFactory qf = newQueryFactory();

        // reset the default ordering
        qf.getOrderings().clear();

        return Database.query( qf.setCondition( conditionClause )
                                 .addOrdering( litify_pm__Firm__c.Name, fflib_QueryFactory.SortOrder.ASCENDING, true )
                                 .toSOQL() );
    }

    public List<litify_pm__Firm__c> selectAll()
    {
        return Database.query(newQueryFactory().toSOQL());
    }

    public List<litify_pm__Firm__c> selectByExternalId( Set<Double> externalIdSet )
    {
        return Database.query( newQueryFactory().setCondition(litify_pm__Firm__c.litify_pm__ExternalId__c  + ' in :externalIdSet').toSOQL() );
    }
}