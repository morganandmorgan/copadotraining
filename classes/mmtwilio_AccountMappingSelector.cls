public class mmtwilio_AccountMappingSelector extends mmlib_SObjectSelector implements mmtwilio_IAccountMappingSelector {

    public static mmtwilio_IAccountMappingSelector newInstance() {
        return (mmtwilio_IAccountMappingSelector) mm_Application.Selector.newInstance(Twilio_Account_Mapping__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType() {
        return Twilio_Account_Mapping__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Twilio_Account_Mapping__c.Domain_Value__c,
                Twilio_Account_Mapping__c.Phone_Number__c,
                Twilio_Account_Mapping__c.Source_Value__c,
                Twilio_Account_Mapping__c.Twilio_Account_Id__c,
                Twilio_Account_Mapping__c.Twilio_Auth_Named_Credential__c
        };
    }

    public List<Twilio_Account_Mapping__c> selectById(Set<Id> idSet) {
        return (List<Twilio_Account_Mapping__c>) selectSObjectsById(idSet);
    }

    public List<Twilio_Account_Mapping__c> selectAll() {
        return Database.query(newQueryFactory().toSOQL());
    }
}