@isTest
private class mmcommon_TrainingUserUtilsTest
{
    @isTest
    static void testTheCreationOfUsersFromExplicitCommandLine()
    {
        Test.startTest();
        (new mmcommon_TrainingUserUtils()).presetUserNumberAt(1).setEmailAddressTo('adiaz@forthepeople.com').setTotalNumberOfUsersToGenerateTo(20).generateTrainingUsers().persist();
        Test.stopTest();
    }

    @isTest
    static void testTheCreationOfUsersRelyingOnCustomSettings()
    {
        TrainingUserSetupRelated__c customSettingRecord = new TrainingUserSetupRelated__c();

        customSettingRecord.SetupOwnerId = mmlib_Utils.org.id;

        insert customSettingRecord;

        Test.startTest();

        system.debug( TrainingUserSetupRelated__c.getInstance() );

        (new mmcommon_TrainingUserUtils()).generateTrainingUsers().persist();
        Test.stopTest();
    }
}