/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBTurnDown {
    global PBTurnDown() {

    }
    @InvocableMethod(label='Turn Down' description='Turn Down the given intake')
    global static void turnDown(List<litify_pm.PBTurnDown.ProcessBuilderTurnDownWrapper> turnDownItems) {

    }
global class ProcessBuilderTurnDownWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    @InvocableVariable( required=false)
    global String turn_down_reason;
    @InvocableVariable( required=false)
    global String turn_down_reason_other;
    global ProcessBuilderTurnDownWrapper() {

    }
}
}
