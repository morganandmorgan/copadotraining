/**
 *  mmmarketing_CreateSourcesForMFPAction
 */
public with sharing class mmmarketing_CreateSourcesForMFPAction
    extends mmlib_AbstractAction
{
    public override Boolean runInProcess()
        {
        CampaignTrackingRelated__c customSetting = CampaignTrackingRelated__c.getInstance();

        //If the custom setting is not initialized yet or it is initialized and IsAutomaticSourceSetupEnabled__c == true, then proceed.
        if ( customSetting == null || customSetting.IsAutomaticSourceSetupEnabled__c )
        {
            // of the records submitted,
            mmmarketing_SourceFactory factory = new mmmarketing_SourceFactory( this.uow );


            Set<String> sourceValuesSet = new Set<String>();

            // loop through the records
            for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)this.records)
            {
                //system.debug('~~~~~~ record.Marketing_Source__c : '+record.Marketing_Source__c);
                //system.debug('~~~~~~ record.Source_Value__c : '+record.Source_Value__c);

                // collect all of the Source_Value__c values
                if (record.Source_Value__c != null)
                {
                    sourceValuesSet.add( record.Source_Value__c );
                }
            }

            //system.debug('~~~~~~ sourceValuesSet : '+ sourceValuesSet);

            // Only proceed if there are values in the sourceValuesSet
            if ( ! sourceValuesSet.isEmpty() )
            {
                // With that set of source values, query the source table
                List<Marketing_Source__c> currentlyAvailableSourceRecordList = mmmarketing_SourcesSelector.newInstance().selectByUtm( sourceValuesSet );

                // Sort source records in to a set
                Set<String> currentlyAvailableSourceNameList = new Set<String>();
                Set<String> sourceValuesUtilizedSet = new Set<String>();

                for (Marketing_Source__c currentlyAvailableSource : currentlyAvailableSourceRecordList)
                {
                    if ( String.isNotBlank( currentlyAvailableSource.name ) )
                    {
                        currentlyAvailableSourceNameList.add( currentlyAvailableSource.name );
                    }
                }

                //system.debug('~~~~~~ currentlyAvailableSourceNameList : '+ currentlyAvailableSourceNameList);
                for (Marketing_Financial_Period__c record : (list<Marketing_Financial_Period__c>)this.records)
                {
                    //system.debug('~~~~~~ record.Source_Value__c : '+record.Source_Value__c);
                    if ( ! currentlyAvailableSourceNameList.contains(record.Source_Value__c)
                        && ! sourceValuesUtilizedSet.contains( record.Source_Value__c ))
                    {
                        //system.debug('~~~~~~ it is being added.');
                        factory.generateNewFromSourceValue( record.Source_Value__c );

                        // Add the source value to the sourceValuesUtilizedSet so we don't create duplicates
                        sourceValuesUtilizedSet.add( record.Source_Value__c );
                    }
                }
            }
        }

        return true;
    }
}