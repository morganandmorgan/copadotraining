@IsTest
public with sharing class PermissionSetSelectorTest {

    public static testMethod void PermissionSetSelector_SelectPermissionSetNames() {

        // Arrange
        String permissionSetName = 'Allow_Ability_to_Delete_Tasks';
        Integer expectedCount = 1;

        // Act
        Test.startTest();
        List<PermissionSet> permissionSets = PermissionSetSelector.selectPermissionSetByNames(new Set<String> {permissionSetName});
        Test.stopTest();

        // Assert
        System.assert(permissionSets != null);
        System.assertEquals(expectedCount, permissionSets.size());
    }
}