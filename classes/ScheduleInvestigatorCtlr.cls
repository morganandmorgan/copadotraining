public with sharing class ScheduleInvestigatorCtlr {

  @TestVisible private static final String CASETYPE_SELECTOPTION_CATASTROPHIC_FLAG = ':catastrophic';

  private static final String KEY_ADD_NEW_LOCATION = 'Add New Location';

  private static final List<SelectOption> SELECTOPTIONS_STATE = new List<SelectOption>();
  private static final List<SelectOption> SELECTOPTIONS_DELAYREASON = new List<SelectOption>{ new SelectOption('', '--None--') };
  private static final List<SelectOption> SELECTOPTIONS_LANGUAGE = new List<SelectOption>();

  static {
    List<Flow_State_Code__c> stateSettings = Flow_State_Code__c.getAll().values();
    stateSettings.sort();
    for (Flow_State_Code__c stateSetting : stateSettings) {
      if (String.isNotBlank(stateSetting.State_Abbreviation__c)) {
        SELECTOPTIONS_STATE.add(new SelectOption(stateSetting.State_Abbreviation__c, stateSetting.Name));
      }
    }

    for (Schema.PicklistEntry eachValue : Event.Reason_for_Delayed_Signup__c.getDescribe().getPicklistValues()) {
      SELECTOPTIONS_DELAYREASON.add(new SelectOption(eachValue.getValue(), eachValue.getLabel()));
    }

    for (Schema.PicklistEntry eachValue : User.Language__c.getDescribe().getPicklistValues()) {
      SELECTOPTIONS_LANGUAGE.add(new SelectOption(eachValue.getValue(), eachValue.getLabel()));
    }
  }

  public String selectedLocation { get; set; }
  public String selectedDocuments { get; set; }
  public String additionalNotes { get; set; }
  public String scheduledDate { get; set; }
  public Integer timeRequired { get; set; }
  public String selectedSlot { get; set; }
  public String reasonForDelayedSignup { get; set; }
  public Boolean isLocationEnabled { get; set; }
  public Boolean isLanguagesEnabled { get; set; }
  public Boolean isSkillEnabled { get; set; }
  public Boolean isTimeEnabled { get; set; }
  public List<String> selectedLanguage { get; set; }
  public List<String> selectedSkill { get; set; }

  public Incident incidentInstance { get; private set; }
  public List<SelectOption> investigationLocationOptions { get; private set; }
  public List<SelectOption> additionalDocumentOptions { get; private set; }
  public List<SelectOption> skillChoices { get; private set; }
  public List<String> caseTypes { get; private set; }
  public Map<Id, List<ScheduleInvestigatorService.AvailableSlot>> territoryUsersAvailableSlots { get; private set; }
  public Map<Id, Notification> userNotifications { get; private set; }
  public Map<String, InvestigationAddress> investigationLocationMap { get; private set; }

  private final String incidentId;
  private final String sourceRecordId;
  
  public ScheduleInvestigatorCtlr() {
    this.incidentId = ApexPages.currentPage().getParameters().get('id');
    this.sourceRecordId = ApexPages.currentPage().getParameters().get('srcId');

    this.selectedDocuments = null;
    this.additionalNotes = null;
    this.scheduledDate = Date.today().format();
    this.timeRequired = 1;
    this.selectedSlot = '';
    this.reasonForDelayedSignup = '';
    this.isLocationEnabled = true;
    this.isLanguagesEnabled = true;
    this.isSkillEnabled = true;
    this.isTimeEnabled = true;
    this.selectedLanguage = new List<String>();
    this.selectedSkill = new List<String>();

    this.territoryUsersAvailableSlots = new Map<Id, List<ScheduleInvestigatorService.AvailableSlot>>();
    this.userNotifications = new Map<Id, Notification>();

    // build Intake__c data
    this.incidentInstance = createIncidentWrapper(this.incidentId);

    // obtain deduped list of all intake case types
    this.caseTypes = new List<String>();
    this.caseTypes.addAll(getAllIntakeCaseTypes(this.incidentInstance.attendees));

    populateClientAddresses();

    // obtain all available additional document names
    this.additionalDocumentOptions = new List<SelectOption>();
    for (String documentName : getAvailableAdditionalDocumentNames(this.incidentInstance.attendees)) {
      this.additionalDocumentOptions.add(new SelectOption(documentName, documentName));
    }

    this.skillChoices = new List<SelectOption>();
    List<String> availableSkills = getAvailableSkills(this.incidentInstance.attendees);
    if (availableSkills.isEmpty()) {
      for (Schema.PicklistEntry eachValue : User.Skill__c.getDescribe().getPicklistValues()) {
        this.skillChoices.add(new SelectOption(eachValue.getValue(), eachValue.getLabel()));
      }
    }
    else {
      for (String skill : availableSkills) {
        this.skillChoices.add(new SelectOption(skill, skill));
      }
      this.selectedSkill.addAll(availableSkills);
    }

    // default the language selection
    for (SelectOption languageOption : SELECTOPTIONS_LANGUAGE) {
      if (languageOption.getValue() == 'English') {
        this.selectedLanguage.add(languageOption.getValue());
        break;
      }
    }

    filterInvestigators();
  }

  private void populateClientAddresses() {

    this.investigationLocationMap = new Map<String, InvestigationAddress>();
    this.investigationLocationOptions = new List<SelectOption>();

    for (Attendee childAttendee : this.incidentInstance.attendees) {
      for (InvestigationAddress currentAddress : childAttendee.investigationAddresses) {
        if (!this.investigationLocationMap.containsKey(currentAddress.getAddressKey())) {
          this.investigationLocationMap.put(currentAddress.getAddressKey(), currentAddress);
          this.investigationLocationOptions.add(new SelectOption(currentAddress.getAddressKey(), currentAddress.investigationLocationName));
        }
      }
    }

    // add entries for manual location
    this.investigationLocationMap.put(KEY_ADD_NEW_LOCATION, incidentInstance.manualAddress);
    this.investigationLocationOptions.add(new SelectOption(KEY_ADD_NEW_LOCATION, KEY_ADD_NEW_LOCATION));

    this.selectedLocation = investigationLocationOptions.get(0).getValue();
  }

  public String getCaseTypeSelectOptionCatastrophicIdentifier() {
    return CASETYPE_SELECTOPTION_CATASTROPHIC_FLAG;
  }

  public List<SelectOption> getStateSelectOptions() {
    return SELECTOPTIONS_STATE;
  }

  public List<SelectOption> getReasonsForDelayedSignupList() {
    return SELECTOPTIONS_DELAYREASON;
  }

  public List<SelectOption> getLanguageChoices() {
    return SELECTOPTIONS_LANGUAGE;
  }

  private Incident createIncidentWrapper(String incidentId) {

    Incident__c incidentRecord = [SELECT Id, Name,
      (SELECT Id, Name, Caller_First_Name__c, Caller_Last_Name__c, Can_IP_sign__c, Catastrophic__c, Case_Type__c,  Client__c,  Client__r.Name,Client__r.PersonContactId, Client__r.BillingStreet, Client__r.BillingCity, Client__r.BillingState,  Client__r.BillingPostalCode, Client__r.PersonHomePhone, Client__r.PersonMobilePhone, Client__r.Language__c, Handling_Firm__c, Injured_Party__c, Injured_Party__r.Name, Primary_Intake__c, Representative_Reason__c, Status__c, Status_Detail__c, What_was_the_date_of_the_incident__c FROM Intakes__r WHERE Case_Type__c != null ORDER BY Client__r.Name ASC, Injured_Party__r.Name ASC NULLS FIRST) FROM Incident__c WHERE Id = :incidentId];

    List<Intake__c> childIntakeRecordList = incidentRecord.Intakes__r;

    Incident incidentWrapper = new Incident();
    incidentWrapper.incidentName = incidentRecord.Name;

    Map<AttendeeKey, List<Intake__c>> clientToChildIntakeRecords = new Map<AttendeeKey, List<Intake__c>>();
    for (Intake__c childIntakeRecord : childIntakeRecordList) {
      AttendeeKey key = new AttendeeKey(childIntakeRecord.Client__c, childIntakeRecord.Injured_Party__c, childIntakeRecord.Status__c, childIntakeRecord.Status_Detail__c);
      List<Intake__c> childIntakeRecords = clientToChildIntakeRecords.get(key);
      if (childIntakeRecords == null) {
        childIntakeRecords = new List<Intake__c>();
        clientToChildIntakeRecords.put(key, childIntakeRecords);
      }
      childIntakeRecords.add(childIntakeRecord);

      if (String.isNotBlank(this.sourceRecordId) && childIntakeRecord.Id == Id.valueOf(this.sourceRecordId)) {
        incidentWrapper.primaryIntakeClientName = childIntakeRecord.Client__r.Name;
      }
    }

    for (AttendeeKey key : clientToChildIntakeRecords.keySet()) {
      List<Intake__c> childIntakeRecords = clientToChildIntakeRecords.get(key);
      Map<String, List<Intake__c>> caseTypeToChildIntakeRecords = new Map<String, List<Intake__c>>();
      for (Intake__c childIntakeRecord : childIntakeRecords) {
        String caseTypeWithFlag = childIntakeRecord.Catastrophic__c == true ? childIntakeRecord.Case_Type__c + CASETYPE_SELECTOPTION_CATASTROPHIC_FLAG : childIntakeRecord.Case_Type__c;
        
        List<Intake__c> intakeRecords = caseTypeToChildIntakeRecords.get(caseTypeWithFlag);
        if (intakeRecords == null) {
          intakeRecords = new List<Intake__c>();
          caseTypeToChildIntakeRecords.put(caseTypeWithFlag, intakeRecords);
        }
        intakeRecords.add(childIntakeRecord);
      }

      List<Attendee> resultAttendeeWrappers = createAttendeeWrappers(caseTypeToChildIntakeRecords.values());
      incidentWrapper.attendees.addAll(resultAttendeeWrappers);
    }

    return incidentWrapper;
  }

  private static List<Attendee> createAttendeeWrappers(List<List<Intake__c>> intakeRecordsGroupedByCaseType) {
    List<Attendee> resultAttendeeWrappers = new List<Attendee>();
    createAttendeeWrappers(intakeRecordsGroupedByCaseType, 0, new List<Intake__c>(), resultAttendeeWrappers);
    return resultAttendeeWrappers;
  }

  private static void createAttendeeWrappers(List<List<Intake__c>> intakeRecordsGroupedByCaseType, Integer caseTypeIndex, List<Intake__c> selectedCaseTypes, List<Attendee> resultList) {
    if (caseTypeIndex >= intakeRecordsGroupedByCaseType.size()) {
      if (!selectedCaseTypes.isEmpty()) {
        Attendee attendeeWrapper = new Attendee(selectedCaseTypes.get(0));
        for (Integer i = 1; i < selectedCaseTypes.size(); ++i) {
          Intake__c selectedCaseType = selectedCaseTypes.get(i);
          attendeeWrapper.addIntake(selectedCaseType);
        }
        resultList.add(attendeeWrapper);
      }
      return;
    }

    List<Intake__c> intakeRecords = intakeRecordsGroupedByCaseType.get(caseTypeIndex);
    for (Intake__c intakeRecord : intakeRecords) {
      List<Intake__c> selectedCaseTypesClone = selectedCaseTypes.clone();
      selectedCaseTypesClone.add(intakeRecord);

      createAttendeeWrappers(intakeRecordsGroupedByCaseType, caseTypeIndex + 1, selectedCaseTypesClone, resultList);
    }
  }

  private List<String> getAllIntakeCaseTypes(List<Attendee> attendees) {
    Set<String> allIntakeCaseTypes = new Set<String>();
    for (Attendee obj : attendees) {
      allIntakeCaseTypes.addAll(obj.getAllCaseTypes());
    }

    List<String> sortedCaseTypes = new List<String>(allIntakeCaseTypes);
    sortedCaseTypes.sort();
    return sortedCaseTypes;
  }

  private List<String> getAvailableAdditionalDocumentNames(List<Attendee> attendees) {
    Set<String> allIntakeRepReasons = new Set<String>();
    for (Attendee obj : attendees) {
      allIntakeRepReasons.add(nullSafeToLowerCase(obj.reasonForRepresentative));
    }

    List<String> availableDocumentNames = new List<String>();
    List<AdditionalDocumentSettings__c> documentsList = AdditionalDocumentSettings__c.getAll().values();
    for (AdditionalDocumentSettings__c eachDocument : documentsList) {
      if (allIntakeRepReasons.contains(nullSafeToLowerCase(eachDocument.Case_Type__c))) {
        availableDocumentNames.add(eachDocument.Name);
      }
    }

    availableDocumentNames.sort();

    return availableDocumentNames;
  }

  private List<String> getAvailableSkills(List<Attendee> attendees) {
    Set<String> allSkills = new Set<String>();
    for (Attendee attendeeWrapper : attendees) {
      for (Intake intakeWrapper : attendeeWrapper.intakes) {
        allSkills.add(nullSafeToLowerCase(intakeWrapper.handlingFirm));
      }
    }

    List<String> availableSkills = new List<String>();
    for (Schema.PicklistEntry eachValue : User.Skill__c.getDescribe().getPicklistValues()) {
      if (allSkills.contains(nullSafeToLowerCase(eachValue.getValue()))) {
        availableSkills.add(eachValue.getValue());
      }
    }

    availableSkills.sort();

    return availableSkills;
  }
   
  public PageReference filterInvestigators() {
    this.territoryUsersAvailableSlots.clear();
    this.userNotifications.clear();

    InvestigationAddress selectedAddress = investigationLocationMap.get(this.selectedLocation);

    List<Address_by_Zip_Code__c> addresses = [SELECT County__c, State__c, Territory__c, Catastrophic_Territory__c FROM Address_by_Zip_Code__c WHERE RecordType.DeveloperName = 'Unique_City' AND (State_Code__c = :selectedAddress.investigationLocationState OR State__c = :selectedAddress.investigationLocationState) AND City__c  = :selectedAddress.investigationLocationCity ORDER BY CreatedDate ASC, Id ASC LIMIT 1];
    Address_by_Zip_Code__c addressByZipcode = addresses.isEmpty() ? null : addresses.get(0);

    if (addressByZipcode == null || addressByZipcode.Territory__c == null) {
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not find an investigator territory for the provided address.'));
      return null;
    }

    if (addressByZipcode.Catastrophic_Territory__c && !this.incidentInstance.attendees.get(0).intakes.get(0).isCatastrophic) {
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The investigation you are attempting to schedule is in an area in which we dont usually send investigators. We only send investigators to this area if the case is Catastrophic. If the case qualifies for Sign Up please attempt to E-Sign or send a Mailout. If you believe this is an error please contact a supervisor.'));
      return null;
    }

    List<String> selectedTerritories = SplitAndTrim(addressByZipcode.Territory__c, ';');
    Date eventDate = Date.parse(scheduledDate);
    List<String> selectedLanguagesList = getSelectedLanguagesAsList();
    List<String> selectedSkillsList = getSelectedSkillsAsList();


    Map<Id, List<ScheduleInvestigatorService.AvailableSlot>> userAvailabilitySlots = ScheduleInvestigatorService.GetInvestigatorAvailability(eventDate, timeRequired, (isTimeEnabled != true), (isLocationEnabled ? selectedTerritories : null), (isLanguagesEnabled ? selectedLanguagesList : null), (isSkillEnabled ? selectedSkillsList : null));

    for (Id uid : userAvailabilitySlots.keySet()) {
      List<ScheduleInvestigatorService.AvailableSlot> singleInvestigatorSlot = userAvailabilitySlots.get(uid);
      if (singleInvestigatorSlot == null || singleInvestigatorSlot.size() == 0) {
        continue;
      }
      
      User u = singleInvestigatorSlot[0].investigator;

      Notification notification = new Notification();
      notification.isNotificationEnabled = false;

      if (this.isLocationEnabled != true && !StringContainsAny(u.Territory__c, selectedTerritories)) {
        notification.isNotificationEnabled = true;
        notification.notificationMsg = 'Doesn\'t meet the following requirements';
        notification.locationNotification = '-location';
      }

      if (this.isLanguagesEnabled != true && userLanguageIsNotSelected(u, selectedLanguagesList)) {
        notification.isNotificationEnabled = true;
        notification.notificationMsg = 'Doesn\'t meet the following requirements';
        notification.languageNotification = '-language';
      }

      if (this.isSkillEnabled != true && userSkillIsNotSelected(u, selectedSkillsList)) {
        notification.isNotificationEnabled = true;
        notification.notificationMsg = 'Doesn\'t meet the following requirements';
        notification.skillNotification = '-skill';
      }

      this.userNotifications.put(uid, notification);
    }

    this.territoryUsersAvailableSlots.putAll(userAvailabilitySlots);
    return null;
  }

  private List<String> getSelectedLanguagesAsList() {
    return this.selectedLanguage == null ? new List<String>() : this.selectedLanguage.clone();
  }

  private List<String> getSelectedSkillsAsList() {
    return this.selectedSkill == null ? new List<String>() : this.selectedSkill.clone();
  }

  private ScheduleInvestigatorService.AvailableSlot getAvailableSlotByUserTime(User u, Integer hour, Integer minute) {
    List<ScheduleInvestigatorService.AvailableSlot> slots = this.territoryUsersAvailableSlots.get(u.id);

    for (ScheduleInvestigatorService.AvailableSlot avail : slots) {
      if (avail.timeFrameHour == hour && avail.timeFrameMinute == minute) {
        return avail;
      }
    }
    return null; 
  }

  private User getUserById(String id) {
    for (Id uid : this.territoryUsersAvailableSlots.keySet()) {
      if (uid == id) {
        return this.territoryUsersAvailableSlots.get(uid)[0].investigator;
      }
    }
    return null;
  }
  
  public PageReference saveEvent() {
    Date eventDate =  Date.parse(scheduledDate);

    // TODO: break error checking into own method(s)?
    List<String> selectedSlotList;
    User selectedUser;
    ScheduleInvestigatorService.AvailableSlot slotSelected;
    
    if (String.isBlank(this.selectedSlot)) {
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select a slot to schedule an investigation.'));
      System.debug('issue parsing selectedSlot: ' + this.selectedSlot);
      return null;
    } else {
      selectedSlotList = this.selectedSlot.split('-');
      if (selectedSlotList.size() == 2) {
        selectedUser = getUserById(selectedSlotList.get(0));
        String strHour = selectedSlotList.get(1);
        String strMinute = '0';
        if (strHour.contains('.')) {
          List<String> selectedTimeLst = strHour.split('\\.');
          strHour = selectedTimeLst.get(0);
          strMinute = selectedTimeLst.get(1);
        }

        slotSelected = getAvailableSlotByUserTime(selectedUser, Integer.valueOf(strHour), Integer.valueOf(strMinute));

        if (slotSelected == null) {
          ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred. Please contact support and reference error E-SLOT-102'));
          System.debug('this slot was null ' + this.selectedSlot);
          return null;
        }

      } else {
        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred. Please contact support and reference error E-SLOT-101'));
        System.debug('issue parsing selectedSlot: ' + this.selectedSlot);
      }
    }

    Integer startHour = slotSelected.timeFrameHour;
    Integer startMinute = slotSelected.timeFrameMinute;

    Set<String> attendees = getAttendeeContactIds();
    if (attendees.isEmpty()) {
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Attendees Selected. You must select at least one attendee to schedule an investigation'));
      System.debug('No attendees selected!');
    }

    validateReasonForDelayedSignup(eventDate, startHour, startMinute);

    if (ApexPages.getMessages().size() > 0) {
      return null;
    }

    // IF we got here there are no errors
    

    Set<Id> intakeIds = new Set<Id>();
    for (Attendee selectedAttendee : this.incidentInstance.getSelectedAttendees()) {
      for (Intake selectedIntake : selectedAttendee.getSelectedIntakes()) {
        intakeIds.add(selectedIntake.intakeId);
      }
    }

    Time selectedStartTime = Time.newInstance(startHour,startMinute,0,0);
    try {
      Event evt = ScheduleInvestigatorService.scheduleIncidentMeeting(this.incidentId,
            new List<Id>(intakeIds),
            selectedUser,
            attendees,
            eventDate,
            selectedStartTime,
            this.timeRequired,
            additionalNotes,
            investigationLocationMap.get(this.selectedLocation).getLocation(),
            this.selectedDocuments,
            this.reasonForDelayedSignup,
            (this.isTimeEnabled != true)
          );
    } catch (ScheduleInvestigatorService.ConflictingAppointmentException ex) {
      DateTime selectedDateTime = DateTime.newInstance(eventDate, selectedStartTime);
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Agent is no longer available at the selected date & time. Please select another date/time or another agent.'));
      
      // refresh investigator slots
      filterInvestigators();

      return null;
    }

    return getDetailPageReference();
  }

  private void validateReasonForDelayedSignup(Date eventDate, Integer startHour, Integer startMinute) {
    if (String.isBlank(this.reasonForDelayedSignup) && intakeSelectionsIncludeCatastrophicCaseTypes()) {
      Datetime investigationStartTime = Datetime.newInstance(eventDate.year(), eventDate.month(), eventDate.day(), startHour, startMinute, 0);

      Long hourDifference = ((investigationStartTime.getTime() - Datetime.now().getTime()) / Decimal.valueOf(3600000)).round(System.RoundingMode.CEILING);

      Decimal reasonForDelayedSignupTime = ScheduleInvestigatorSettings__c.getInstance().Delayed_Signup_Reason_Time__c;

      if (reasonForDelayedSignupTime == null || hourDifference > reasonForDelayedSignupTime) {
        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Reason For Delayed Signup Selected. You must select at least one reason for delayed signup to schedule an investigation'));
        System.debug('No reason for delayed signup selected!');
      }
    }
  }

  private Set<String> getAttendeeContactIds() {
    Set<String> attendeeContactIds = new Set<String>();
    for (Attendee selectedAttendee : this.incidentInstance.getSelectedAttendees()) {
      attendeeContactIds.add(selectedAttendee.personContactId);
    }
    return attendeeContactIds;
  }

  private Boolean intakeSelectionsIncludeCatastrophicCaseTypes() {
    for (Attendee selectedAttendee : this.incidentInstance.getSelectedAttendees()) {
      for (Intake selectedIntake : selectedAttendee.getSelectedIntakes()) {
        if (selectedIntake.isCatastrophic) {
          return true;
        }
      }
    }
    return false;
  }

  public PageReference cancelSchedule() {
    return getDetailPageReference();
  }

  private static Boolean userLanguageIsNotSelected(User u, List<String> selectedLanguagesList) {
    return !selectedLanguagesList.isEmpty() && !multiPicklistFieldContainsAll(u.Language__c, selectedLanguagesList);
  }

  private static Boolean userSkillIsNotSelected(User u, List<String> selectedSkillsList) {
    return !selectedSkillsList.isEmpty() && !multiPicklistFieldContainsAll(u.Skill__c, selectedSkillsList);
  }

  private static Boolean multiPicklistFieldContainsAll(String multiPicklistFieldValue, List<String> toCompare) {
    Set<String> toCompareSet = new Set<String>();
    for (String s : toCompare) {
      toCompareSet.add(nullSafeToLowerCase(s));
    }

    Set<String> fieldValues = new Set<String>();
    if (String.isNotBlank(multiPicklistFieldValue)) {
      String s = nullSafeToLowerCase(multiPicklistFieldValue);
      fieldValues.addAll(s.split(';'));
    }

    return fieldValues.containsAll(toCompareSet);
  }

  private PageReference getDetailPageReference() {
    PageReference sourceDetailPage = new PageReference('/' + (String.isBlank(this.sourceRecordId) ? this.incidentId : this.sourceRecordId));
    return sourceDetailPage;
  }

  private static String nullSafeToLowerCase(String s) {
    return s == null ? s : s.toLowerCase();
  }

  @RemoteAction
  public static List<SchedulerSelectOption> getCityOptions(String stateCode) {
    List<SchedulerSelectOption> cityOptions = new List<SchedulerSelectOption>();
    SchedulerSelectOption noneOption = new SchedulerSelectOption();
    noneOption.value = '';
    noneOption.label = '--None--';
    cityOptions.add(noneOption);
    if (String.isNotBlank(stateCode)) {
      for (Address_by_Zip_Code__c address : [
        SELECT City__c, Flow_Label__c
        FROM Address_by_Zip_Code__c
        WHERE RecordType.DeveloperName = 'Unique_City'
          AND State_Code__c = :stateCode
          AND City__c != null
          AND Flow_Label__c != null
        ORDER BY Flow_Label__c ASC
      ]) {
        SchedulerSelectOption cityOption = new SchedulerSelectOption();
        cityOption.value = address.City__c;
        cityOption.label = address.Flow_Label__c;
        cityOptions.add(cityOption);
      }
    }
    return cityOptions;
  }

  public class SchedulerSelectOption {
    public String value { get; set; }
    public String label { get; set; }
  }

  public class Incident {
    public String primaryIntakeClientName { get; private set; }
    public String incidentName { get; private set; }
    public List<Attendee> attendees { get; private set; }
    public InvestigationAddress manualAddress { get; private set; }

    public Incident() {
      this.attendees = new List<Attendee>();
      this.manualAddress = new InvestigationAddress();
    }

    private List<Attendee> getSelectedAttendees() {
      List<Attendee> selectedAttendees = new List<Attendee>();
      for (Attendee attendeeWrapper : attendees) {
        if (attendeeWrapper.selected == true) {
          selectedAttendees.add(attendeeWrapper);
        }
      }
      return selectedAttendees;
    }
  }

  public class Attendee {
    public Boolean selected { get; set; }
    public List<String> selectedCaseTypes { get; set; }

    public String clientName { get; private set; }
    public String attendeeRepresentative { get; private set; }
    public String reasonForRepresentative { get; private set; }
    public String injuredPartyName { get; private set; }
    public String status { get; private set; }
    public String statusDetail { get; private set; }
    public List<InvestigationAddress> investigationAddresses { get; private set; }
    public List<SelectOption> availableCaseTypeOptions { get; private set; }

    @TestVisible private final List<Intake> intakes = new List<Intake>();
    private final Map<String, Intake> caseTypeToIntake = new Map<String, Intake>();

    @TestVisible private String clientId;
    private Id personContactId;

    public Attendee() {}

    private Attendee(Intake__c intakeRecord) {
      this.clientId = intakeRecord.Client__c;
      this.personContactId = intakeRecord.Client__r.PersonContactId;
      this.clientName = intakeRecord.Client__r.Name;

      this.investigationAddresses = new List<InvestigationAddress>{
        new InvestigationAddress('Home', intakeRecord.Client__r)
      };

      this.attendeeRepresentative = concatenateCallerName(intakeRecord);
      this.reasonForRepresentative = intakeRecord.Representative_Reason__c;
      this.injuredPartyName = concatenateInjuredPartyName(intakeRecord);
      this.status = intakeRecord.Status__c;
      this.statusDetail = intakeRecord.Status_Detail__c;

      this.availableCaseTypeOptions = new List<SelectOption>();
      this.selected = false;
      this.selectedCaseTypes = new List<String>();

      addIntake(intakeRecord);
    }

    private String concatenateInjuredPartyName(Intake__c intakeRecord) {
      String result = '';
      if (intakeRecord.Client__c != intakeRecord.Injured_Party__c && String.isNotBlank(intakeRecord.Injured_Party__r.Name)) {
        result = intakeRecord.Injured_Party__r.Name;
        if (String.isNotBlank(intakeRecord.Can_IP_sign__c) && intakeRecord.Can_IP_sign__c != 'Yes') {
          result = result + intakeRecord.Can_IP_sign__c.removeStartIgnoreCase('No');
        }
      }
      return result;
    }

    private String concatenateCallerName(Intake__c intakeRecord) {
      String result = '';
      if (String.isNotEmpty(intakeRecord.Caller_First_Name__c) && String.isNotEmpty(intakeRecord.Caller_Last_Name__c)) {
        result = intakeRecord.Caller_First_Name__c + ' ' + intakeRecord.Caller_Last_Name__c;
      }
      return result;
    }

    private void addIntake(Intake__c intakeRecord) {
      Intake intakeWrapper = new Intake(intakeRecord);
      String caseTypeValue = intakeWrapper.isCatastrophic == true ? intakeWrapper.caseType + CASETYPE_SELECTOPTION_CATASTROPHIC_FLAG : intakeWrapper.caseType;
      if (!caseTypeToIntake.containsKey(caseTypeValue)) {
        selectedCaseTypes.add(caseTypeValue);
        availableCaseTypeOptions.add(new SelectOption(caseTypeValue, getCaseTypeOptionLabel(intakeRecord)));
        intakes.add(intakeWrapper);
        caseTypeToIntake.put(caseTypeValue, intakeWrapper);
      }
    }

    private String getCaseTypeOptionLabel(Intake__c intakeRecord) {
      return intakeRecord.Case_Type__c + ' - ' + intakeRecord.Name.removeStart('INT-');
    }

    private List<String> getAllCaseTypes() {
      Set<String> uniqueTypes = new Set<String>();
      for (String selectedType : selectedCaseTypes) {
        uniqueTypes.add(selectedType.remove(CASETYPE_SELECTOPTION_CATASTROPHIC_FLAG));
      }
      List<String> sortedTypes = new List<String>(uniqueTypes);
      sortedTypes.sort();
      return sortedTypes;
    }

    private List<Intake> getSelectedIntakes() {
      List<Intake> selectedIntakes = new List<Intake>();
      for (String selectedType : selectedCaseTypes) {
        Intake intakeWrapper = caseTypeToIntake.get(selectedType);
        if (intakeWrapper != null) {
          selectedIntakes.add(intakeWrapper);
        }
      }
      return selectedIntakes;
    }
  }

  public class InvestigationAddress {
    public String investigationLocationName { get; set; }
    public String investigationLocationStreet1 { get; set; }
    public String investigationLocationStreet2 { get; set; }
    public String investigationLocationCity { get; set; }
    public String investigationLocationState { get; set; }
    public String investigationLocationZipcode { get; set; }

    private String clientId;

    public InvestigationAddress() {
      this.clientId = '';
    }

    private InvestigationAddress(String locationName, Account client) {
      this.clientId = client.Id;
      this.investigationLocationName = client.Name + ' ' + locationName;
      this.investigationLocationStreet1 = client.BillingStreet;
      this.investigationLocationStreet2 = '';
      this.investigationLocationCity = client.BillingCity;
      this.investigationLocationState = client.BillingState;
      this.investigationLocationZipcode = client.BillingPostalCode;
    }

    public String getAddressKey() {
      return clientId + '_' + investigationLocationName;
    }

    public String getLocation() {
      List<String> addressValues = new List<String>();
      if (String.isNotBlank(this.investigationLocationStreet1)) {
        addressValues.add(this.investigationLocationStreet1);
      }
      if (String.isNotBlank(this.investigationLocationStreet2)) {
        addressValues.add(this.investigationLocationStreet2);
      }
      if (String.isNotBlank(this.investigationLocationCity)) {
        addressValues.add(this.investigationLocationCity);
      }
      if (String.isNotBlank(this.investigationLocationState)) {
        addressValues.add(this.investigationLocationState);
      }
      if (String.isNotBlank(this.investigationLocationZipcode)) {
        addressValues.add(this.investigationLocationZipcode);
      }

      return String.join(addressValues, ' ');
    }
  }

  private List<String> SplitAndTrim(String str, String c) {
    List<String> split = str.split(c);
    List<String> trimmed = new List<String>();
    for (String s : split) {
      trimmed.add(s.trim());
    }
    return trimmed;
  }

  private Boolean StringContainsAny(String str, List<String> lst) {
    for (String str2 : lst) {
      if (str.Contains(str2)) {
        return true;
      }
    }
    return false;
  }
  
  private class Intake {
    @TestVisible private Id intakeId;
    private String caseType;
    private Boolean isCatastrophic;
    private String handlingFirm;

    private Intake(Intake__c intakeRecord) {
      this.intakeId = intakeRecord.Id;
      this.caseType = intakeRecord.Case_Type__c;
      this.isCatastrophic = intakeRecord.Catastrophic__c;
      this.handlingFirm = intakeRecord.Handling_Firm__c;
    }
  }
  
  public class Notification {
    public Boolean isNotificationEnabled { get; private set; }
    public String notificationMsg { get; private set; }
    public String locationNotification { get; private set; }
    public String languageNotification { get; private set; }
    public String skillNotification { get; private set; }
  }

  private class AttendeeKey {
    private final String CLIENT_ID;
    private final String IP_ID;
    private final String STATUS;
    private final String STATUS_DETAIL;
    private final Integer HASHCODE;

    private AttendeeKey(String clientId, String injuredPartyId, String status, String statusDetail) {
      this.CLIENT_ID = clientId;
      this.IP_ID = (clientId == injuredPartyId) ? null : injuredPartyId;
      this.STATUS = status;
      this.STATUS_DETAIL = statusDetail;

      Integer hash = 17;
      hash = hash * 23 + nullSafeHashCode(this.CLIENT_ID);
      hash = hash * 23 + nullSafeHashCode(this.IP_ID);
      hash = hash * 23 + nullSafeHashCode(this.STATUS);
      hash = hash * 23 + nullSafeHashCode(this.STATUS_DETAIL);
      this.HASHCODE = hash;
    }

    private Integer nullSafeHashCode(String s) {
      return s == null ? 0 : s.hashCode();
    }

    public Integer hashCode() {
      return HASHCODE;
    }

    public Boolean equals(Object obj) {
      if (obj instanceof AttendeeKey) {
        AttendeeKey o = (AttendeeKey) obj;
        return CLIENT_ID == o.CLIENT_ID
            && IP_ID == o.IP_ID
            && STATUS == o.STATUS
            && STATUS_DETAIL == o.STATUS_DETAIL;
      }
      return false;
    }
  }
}