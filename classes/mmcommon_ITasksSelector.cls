public interface mmcommon_ITasksSelector extends mmlib_ISObjectSelector {
    List<Task> selectMyTodayOpenTasks();
    List<Task> selectFutureOpenTasks(Integer numDays);
    List<Task> selectMyOverdueTasks();
    List<Task> selectCompletedTasksByRelatedId(Id relatedId);
    List<Task> selectOpenTasksByRelatedIdSet(Set<Id> relatedIdSet);
}