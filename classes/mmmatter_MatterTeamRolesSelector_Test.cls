/**
 * mmmatter_MatterTeamRolesSelector_Test
 * @description Test for Matter Team Roles Selector class.
 * @author Jeff Watson
 * @date 3/28/2019
 */
@IsTest
public with sharing class mmmatter_MatterTeamRolesSelector_Test {

    private static litify_pm__Matter_Team_Role__c matterTeamRole;

    static {
        matterTeamRole = TestUtil.createMatterTeamMemberRole();
        insert matterTeamRole;
    }

    @IsTest
    private static void ctor() {
        mmmatter_MatterTeamRolesSelector matterTeamRolesSelector = new mmmatter_MatterTeamRolesSelector();
        System.assert(matterTeamRolesSelector != null);
    }

    @IsTest
    private static void selectAll() {
        mmmatter_MatterTeamRolesSelector matterTeamRolesSelector = new mmmatter_MatterTeamRolesSelector();
        List<litify_pm__Matter_Team_Role__c> matterTeamRoles = matterTeamRolesSelector.selectAll();
        System.assert(matterTeamRoles != null);
        System.assertEquals(1, matterTeamRoles.size());
    }

    @IsTest
    private static void selectByIds() {
        mmmatter_MatterTeamRolesSelector matterTeamRolesSelector = new mmmatter_MatterTeamRolesSelector();
        List<litify_pm__Matter_Team_Role__c> matterTeamRoles = matterTeamRolesSelector.selectById(new Set<Id> {matterTeamRole.Id});
        System.assert(matterTeamRoles != null);
        System.assertEquals(1, matterTeamRoles.size());
    }

    @IsTest
    private static void selectByName() {
        mmmatter_MatterTeamRolesSelector matterTeamRolesSelector = new mmmatter_MatterTeamRolesSelector();
        List<litify_pm__Matter_Team_Role__c> matterTeamRoles = matterTeamRolesSelector.selectByName(new Set<String> {'Principle Attorney'});
        System.assert(matterTeamRoles != null);
        System.assertEquals('Principle Attorney', matterTeamRoles[0].Name);
    }
}