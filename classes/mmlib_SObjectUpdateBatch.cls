/**
 */
public class mmlib_SObjectUpdateBatch implements Database.Batchable<SObject>
{
    private final String query;
    private final SObjectType sobjectType;
    private final Map<Schema.SObjectField, Object> fieldVsUpdateValueMap;
    private boolean isUpdatingAllOrNothing = false;
    private integer batchSize = 200;

    private mmlib_SObjectUpdateBatch()
    {

    }

    public mmlib_SObjectUpdateBatch(SObjectType sobjType, String query, Map<Schema.SObjectField, Object> fieldsAndValues)
    {
        this.query = query;
        this.sobjectType = sobjType;
        this.fieldVsUpdateValueMap = fieldsAndValues;
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        return Database.getQueryLocator( this.query );
    }

    public void execute(Database.BatchableContext context, List<SObject> scope)
    {
        try
        {
            Schema.SObjectField otherField = null;

             for(SObject s : scope)
             {
                 for(Schema.SObjectField fieldKey : this.fieldVsUpdateValueMap.keySet())
                 {
                     if ( this.fieldVsUpdateValueMap.get(fieldKey) instanceOf Schema.SObjectField )
                     {
                         otherField = (Schema.SObjectField)this.fieldVsUpdateValueMap.get(fieldKey);

                        s.put(fieldKey, s.get(otherField));
                     }
                     else
                     {
                        s.put(fieldKey, this.fieldVsUpdateValueMap.get(fieldKey));
                     }
                 }
             }

            database.update( scope, this.isUpdatingAllOrNothing );
        }
        catch (Exception e)
        {
            System.debug('Error executing SObject : ' + this.sobjectType + ' update batch ' + e);
        }
    }

    public void finish(Database.BatchableContext context)
    {

    }

    public mmlib_SObjectUpdateBatch setUpdateModeToAllOrNothing()
    {
        this.isUpdatingAllOrNothing = true;

        return this;
    }

    public mmlib_SObjectUpdateBatch setBatchSizeTo( final integer newBatchSize )
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public void execute()
    {
        Database.executeBatch( this, batchSize );
    }
}