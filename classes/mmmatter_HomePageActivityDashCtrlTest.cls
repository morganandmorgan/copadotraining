@isTest
private class mmmatter_HomePageActivityDashCtrlTest {
    @isTest
    private static void testTasks() {
        Account acc = TestUtil.createAccount('test acct');
        insert acc;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;

        litify_pm__Matter__c lm1 = TestUtil.createMatter(acc, matterPlan);
        litify_pm__Matter__c lm2 = TestUtil.createMatter(acc, matterPlan);
        insert new list<litify_pm__Matter__c> {lm1, lm2};

		insert TestUtil.createTask(lm1.Id, 'the subject', System.today());
        Event e = TestUtil.createEvent(UserInfo.getUserId(), System.now(), System.now().addMinutes(60));
        e.WhatId = lm1.Id;
        insert e;

        Test.startTest();
        mmmatter_HomePageActivityDashController.getSettings();
        mmmatter_HomePageActivityDashController.getTodaysTasks();
        mmmatter_HomePageActivityDashController.getThisWeekTasks();
        mmmatter_HomePageActivityDashController.getOverdueTasks();
        mmmatter_HomePageActivityDashController.getMattersWithoutTasks();
        Test.stopTest();
    }
}