public class IntakeSetApprovingAttorneys_TrigAct extends TriggerAction {

    private final List<Intake__c> intakesToAssignAttorneys = new List<Intake__c>();

    public IntakeSetApprovingAttorneys_TrigAct() {
        super();
    }

    public override Boolean shouldRun() {
        intakesToAssignAttorneys.clear();

        List<Intake__c> intakeNewList = getNewListForContext();

        if (isUpdate() && shouldBypassUpdateCriteria()) {
            intakesToAssignAttorneys.addAll(intakeNewList);
        }
        else {
            Map<Id, Intake__c> intakeOldMap = (Map<Id, Intake__c>) triggerOldMap;

            for (Intake__c intake : intakeNewList) {
                if (String.isNotBlank(intake.Case_Type__c) 
                    && String.isNotBlank(intake.Venue__c)
                    && String.isNotBlank(intake.Handling_Firm__c)) {

                    if (intakeOldMap != null) {
                        Intake__c oldIntake = intakeOldMap.get(intake.Id);
                        if (intake.Case_Type__c == oldIntake.Case_Type__c
                            && intake.Venue__c == oldIntake.Venue__c
                            && intake.Handling_Firm__c == oldIntake.Handling_Firm__c
                            && intake.Type_of_Dispute__c == oldIntake.Type_of_Dispute__c
                            && intake.Where_did_the_injury_occur__c == oldIntake.Where_did_the_injury_occur__c
                            && intake.Did_the_accident_involve_a_truck_or_bus__c == oldIntake.Did_the_accident_involve_a_truck_or_bus__c) {

                            continue;
                        }
                    }

                    intakesToAssignAttorneys.add(intake);
                }
            }
        }

        return !intakesToAssignAttorneys.isEmpty();
    }

    private static Boolean shouldBypassUpdateCriteria() {
        List<Approval_Assignments_Trigger_Setting__mdt> mdtSetting = [SELECT Id FROM Approval_Assignments_Trigger_Setting__mdt WHERE DeveloperName = 'Default' AND Bypass_Update_Criteria__c = true];
        return !mdtSetting.isEmpty();
    }

    public override void doAction() {
        Set<String> caseTypes = new Set<String>();
        Set<String> venues = new Set<String>();
        Set<String> handlingFirms = new Set<String>();

        Set<String> disputeTypes = new Set<String>{ null };
        Set<String> occurenceLocations = new Set<String>{ null };
        Set<String> truckBusInvolvement = new Set<String>{ null };

        Map<ApprovalAssignmentKey, List<Intake__c>> intakesByAssignmentKey = new Map<ApprovalAssignmentKey, List<Intake__c>>();

        for (Intake__c intake : intakesToAssignAttorneys) {
            ApprovalAssignmentKey key = new ApprovalAssignmentKey(intake.Case_Type__c, intake.Venue__c, intake.Handling_Firm__c);

            List<Intake__c> mappedIntakes = intakesByAssignmentKey.get(key);
            if (mappedIntakes == null) {
                mappedIntakes = new List<Intake__c>();
                intakesByAssignmentKey.put(key, mappedIntakes);

                caseTypes.add(key.caseType);
                venues.add(key.venue);
                handlingFirms.add(key.handlingFirm);
            }

            disputeTypes.add(intake.Type_of_Dispute__c);
            occurenceLocations.add(intake.Where_did_the_injury_occur__c);
            truckBusInvolvement.add(intake.Did_the_accident_involve_a_truck_or_bus__c);

            mappedIntakes.add(intake);
        }

        Map<ApprovalAssignmentKey, List<Approval_Assignment__c>> approvalAssignments = new Map<ApprovalAssignmentKey, List<Approval_Assignment__c>>();
        for (Approval_Assignment__c approvalAssignment : [
            SELECT
                Case_Type__c,
                Venue__c,
                Handling_Firm__c,
                Type_of_Dispute__c,
                Where_did_the_injury_occur__c,
                Did_the_accident_involve_a_truck_or_bus__c,
                Approving_Attorney_1__c,
                Approving_Attorney_2__c,
                Approving_Attorney_3__c,
                Approving_Attorney_4__c,
                Approving_Attorney_5__c,
                Approving_Attorney_6__c,
                New_Case_Email__c
            FROM
                Approval_Assignment__c
            WHERE
                Case_Type__c IN :caseTypes
                AND Venue__c IN :venues
                AND Handling_Firm__c IN :handlingFirms
                AND Type_of_Dispute__c IN :disputeTypes
                AND Where_did_the_injury_occur__c IN :occurenceLocations
                AND Did_the_accident_involve_a_truck_or_bus__c IN :truckBusInvolvement
            ORDER BY
                CreatedDate ASC
        ]) {
            ApprovalAssignmentKey key = new ApprovalAssignmentKey(approvalAssignment.Case_Type__c, approvalAssignment.Venue__c, approvalAssignment.Handling_Firm__c);
            List<Approval_Assignment__c> assignments = approvalAssignments.get(key);
            if (assignments == null) {
                assignments = new List<Approval_Assignment__c>();
                approvalAssignments.put(key, assignments);
            }
            assignments.add(approvalAssignment);
        }

        for (ApprovalAssignmentKey key : intakesByAssignmentKey.keySet()) {
            List<Approval_Assignment__c> assignmentRecords = approvalAssignments.get(key);
            if (assignmentRecords != null) {
                for (Intake__c intake : intakesByAssignmentKey.get(key)) {
                    Approval_Assignment__c matchedAssignment = findBestMatch(intake, assignmentRecords);
                    copyAssignmentFields(intake, matchedAssignment);
                }
            }
        }
    }

    private static Approval_Assignment__c findBestMatch(Intake__c intake, List<Approval_Assignment__c> assignmentRecords) {
        Integer matchedAssignmentNullFieldCount = null;
        Approval_Assignment__c matchedAssignment = null;
        for (Approval_Assignment__c assignmentRecord : assignmentRecords) {
            if ((assignmentRecord.Type_of_Dispute__c == null || intake.Type_of_Dispute__c == assignmentRecord.Type_of_Dispute__c)
                && (assignmentRecord.Where_did_the_injury_occur__c == null || intake.Where_did_the_injury_occur__c == assignmentRecord.Where_did_the_injury_occur__c)
                && (assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c == null || intake.Did_the_accident_involve_a_truck_or_bus__c == assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c)) {

                Integer nullFieldCount = assignmentRecord.Type_of_Dispute__c == null ? 1 : 0;
                nullFieldCount += assignmentRecord.Where_did_the_injury_occur__c == null ? 1 : 0;
                nullFieldCount += assignmentRecord.Did_the_accident_involve_a_truck_or_bus__c == null ? 1 : 0;

                if (matchedAssignment == null || nullFieldCount < matchedAssignmentNullFieldCount) {
                    matchedAssignment = assignmentRecord;
                    matchedAssignmentNullFieldCount = nullFieldCount;
                }
            }
        }
        return matchedAssignment;
    }

    private static void copyAssignmentFields(Intake__c intake, Approval_Assignment__c approvalAssignment) {
        if (intake != null && approvalAssignment != null) {
            intake.Approving_Attorney_1__c = approvalAssignment.Approving_Attorney_1__c;
            intake.Approving_Attorney_2__c = approvalAssignment.Approving_Attorney_2__c;
            intake.Approving_Attorney_3__c = approvalAssignment.Approving_Attorney_3__c;
            intake.Approving_Attorney_4__c = approvalAssignment.Approving_Attorney_4__c;
            intake.Approving_Attorney_5__c = approvalAssignment.Approving_Attorney_5__c;
            intake.Approving_Attorney_6__c = approvalAssignment.Approving_Attorney_6__c;
            intake.New_Case_Email__c = approvalAssignment.New_Case_Email__c;
        }
    }

    private List<Intake__c> getNewListForContext() {
        if (isBefore() && isInsert()) {
            return (List<Intake__c>) triggerList;
        }
        else if (isBefore() && isUpdate()) {
            return (List<Intake__c>) triggerMap.values();
        }
        return new List<Intake__c>();
    }

    private class ApprovalAssignmentKey {

        private final String caseType;
        private final String venue;
        private final String handlingFirm;
        private final Integer hashCode;

        private ApprovalAssignmentKey(String caseType, String venue, String handlingFirm) {
            this.caseType = caseType == null ? null : caseType.toLowerCase();
            this.venue = venue == null ? null : venue.toLowerCase();
            this.handlingFirm = handlingFirm == null ? null : handlingFirm.toLowerCase();

            Integer hash = 17;
            hash = hash * 23 + nullSafeHashCode(this.caseType);
            hash = hash * 23 + nullSafeHashCode(this.venue);
            hash = hash * 23 + nullSafeHashCode(this.handlingFirm);
            this.hashCode = hash;
        }

        private Integer nullSafeHashCode(String s) {
            return s == null ? 0 : s.hashCode();
        }

        public Integer hashCode() {
            return hashCode;
        }

        public Boolean equals(Object obj) {
            if (obj instanceof ApprovalAssignmentKey) {
                ApprovalAssignmentKey o = (ApprovalAssignmentKey) obj;
                return caseType == o.caseType
                    && venue == o.venue
                    && handlingFirm == o.handlingFirm;
            }
            return false;
        }
    }
}