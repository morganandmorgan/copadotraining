/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBSetTaskDueDate {
    global PBSetTaskDueDate() {

    }
    @InvocableMethod(label='Set Task Due Date' description='Manually sets the tasks' due dates to the dates given.')
    global static void setTaskDueDate(List<litify_pm.PBSetTaskDueDate.PBSetTaskDueDateWrapper> input) {

    }
global class PBSetTaskDueDateWrapper {
    @InvocableVariable( required=false)
    global Date dueDate;
    @InvocableVariable( required=false)
    global Task inputTask;
    global PBSetTaskDueDateWrapper() {

    }
}
}
