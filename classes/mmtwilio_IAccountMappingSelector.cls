/**
 *  mmtwilio_IAccountMappingSelector
 */
public interface mmtwilio_IAccountMappingSelector extends mmlib_ISObjectSelector
{
    List<Twilio_Account_Mapping__c> selectById(Set<Id> idSet);
    List<Twilio_Account_Mapping__c> selectAll();
}