/**
 * FeeGoalAllocation_Selector_Test
 * @description Test for Fee Goal Allocation Selector class.
 * @author Jeff Watson
 * @date 2/13/2019
 */
@IsTest
public with sharing class FeeGoalAllocation_Selector_Test {

    private static Fee_Goal__c feeGoal;
    private static Settlement__c settlement;
    private static Fee_Goal_Allocation__c feeGoalAllocation;

    static {
        feeGoal = TestUtil.createFeeGoal();
        insert feeGoal;

        settlement = TestUtil.createSettlement();
        insert settlement;

        feeGoalAllocation = TestUtil.createFeeGoalAllocation(feeGoal, settlement);
        insert feeGoalAllocation;
    }

    @IsTest
    private static void ctor() {
        FeeGoalAllocation_Selector feeGoalAllocationSelector = new FeeGoalAllocation_Selector();
        System.assert(feeGoalAllocationSelector != null);
    }

    @IsTest
    private static void selectByIds() {
        FeeGoalAllocation_Selector feeGoalAllocationSelector = new FeeGoalAllocation_Selector();
        List<Fee_Goal_Allocation__c> feeGoalAllocations = feeGoalAllocationSelector.selectById(new Set<Id> {feeGoalAllocation.Id});
        System.assert(feeGoalAllocations != null);
        System.assertEquals(1, feeGoalAllocations.size());
    }

    @IsTest
    private static void selectByFeeGoalIds() {
        FeeGoalAllocation_Selector feeGoalAllocationSelector = new FeeGoalAllocation_Selector();
        List<Fee_Goal_Allocation__c> feeGoalAllocations = feeGoalAllocationSelector.selectByFeeGoalIds(new Set<Id> {feeGoal.Id});
        System.assert(feeGoalAllocations != null);
        System.assertEquals(1, feeGoalAllocations.size());
    }
}