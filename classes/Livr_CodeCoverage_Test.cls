/**
 * Livr_CodeCoverage_Test
 * @description 
 * @author Jeff Watson
 * @date 7/17/2019
 */
// TODO: This class should be removed after 10K provides actual unit tests
@IsTest
public with sharing class Livr_CodeCoverage_Test {

    @IsTest
    private static void coverage() {
        new SearchAccountResult().coverage();
        new SearchingData().coverage();
    }
}