@isTest
public with sharing class DocsMergeRolesControllerTest {

    @isTest
    public static void mainTest() {

        //create a test template
        litify_docs__Template__c template = new litify_docs__Template__c();
        template.litify_docs__Merged_File_Name__c = 'unitTest.docx';
        template.litify_docs__Starting_Object__c = 'litify_pm__Matter__c';
        template.multi_merge__c = true;
        insert template;

        List<DocsMergeRolesController.LwcOption> templates = DocsMergeRolesController.getTemplates();

        Account account = TestUtil.createAccount('Unit Test Account');
        insert account;
        
        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;

        litify_pm__Role__c role = TestUtil.createRole(matter, account);
        role.litify_pm__Matter__c = matter.id;
        insert role;

        litify_pm__Matter_Team_Role__c matterTeamRole = TestUtil.createMatterTeamMemberRole();
        insert matterTeamRole;

        User u = [SELECT id FROM User where id = :UserInfo.getUserId()];

        litify_pm__Matter_Team_Member__c teamMember = TestUtil.createMatterTeamMember(matter, u, matterTeamRole);
		insert teamMember;        

        List<litify_pm__Role__c> roles = DocsMergeRolesController.getRoles(matter.id);

        List<DocsMergeRolesController.LwcOption> teamMemeber = DocsMergeRolesController.getTeamMembers(matter.id);

        Map<String,Id> sourceIds = new Map<String,Id>();
        Set<Id> roleIds = new Set<Id>();

        List<String> mergeRusults = DocsmergeRolesController.doMerge(Json.serialize(sourceIds), Json.serialize(roleIds));

    } //mainTest

} //class