@isTest
private class CQCPageExtensionTest {

    @TestSetup
    private static void setup() {
        Case_Qualify_Criteria__c caseQualifyCriteria = new Case_Qualify_Criteria__c(
                Criteria__c = ''
            );
        Database.insert(caseQualifyCriteria);
    }

    private static Case_Qualify_Criteria__c getCaseQualifyCriteria() {
        return [SELECT Id, Criteria__c FROM Case_Qualify_Criteria__c];
    }
	
	@isTest
    private static void populateOperatorOptions() {
        Case_Qualify_Criteria__c caseQualifyCriteria = getCaseQualifyCriteria();

        CQCPageExtension controller = new CQCPageExtension(new ApexPages.StandardController(caseQualifyCriteria));

        List<SelectOption> result = controller.OperatorOptions;

        System.assert(result.size() > 1);

        System.assertEquals('', result.get(0).getValue());
        System.assertEquals('', result.get(0).getLabel());

        for (Integer i = 1; i < result.size(); ++i) {
            System.assertNotEquals('', result.get(i).getValue());
            System.assertNotEquals('', result.get(i).getLabel());
        }
    }

    @isTest
    private static void populateFormulaOptions() {
        Case_Qualify_Criteria__c caseQualifyCriteria = getCaseQualifyCriteria();

        CQCPageExtension controller = new CQCPageExtension(new ApexPages.StandardController(caseQualifyCriteria));

        List<SelectOption> result = controller.FormulaOptions;

        System.assert(result.size() > 1);

        System.assertEquals('', result.get(0).getValue());
        System.assertEquals('', result.get(0).getLabel());

        for (Integer i = 1; i < result.size(); ++i) {
            System.assertNotEquals('', result.get(i).getValue());
            System.assertNotEquals('', result.get(i).getLabel());
        }
    }

    @isTest
    private static void populateIntakeColumns() {
        Case_Qualify_Criteria__c caseQualifyCriteria = getCaseQualifyCriteria();

        CQCPageExtension controller = new CQCPageExtension(new ApexPages.StandardController(caseQualifyCriteria));

        List<SelectOption> result = controller.IntakeColumns;

        System.assert(result.size() > 1);

        System.assertEquals('', result.get(0).getValue());
        System.assertEquals('', result.get(0).getLabel());

        for (Integer i = 1; i < result.size(); ++i) {
            System.assertNotEquals('', result.get(i).getValue());
            System.assertNotEquals('', result.get(i).getLabel());
        }
    }
}