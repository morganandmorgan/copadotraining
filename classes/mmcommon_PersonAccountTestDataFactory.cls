public class mmcommon_PersonAccountTestDataFactory
    extends mmlib_AbstractTestDataFactory
{
    public enum PERSON_ACCOUNT_TYPES
    {
        PERSON_ACCOUNT
    }

    private PERSON_ACCOUNT_TYPES individualTypeRequested;

    private Schema.sObjectType getSObjectType()
    {
        return Account.SObjectType;
    }

    private Account getPersonAccount()
    {
        return (Account)this.record;
    }

    private void configurationDuringGeneration()
    {
        getPersonAccount().firstName = 'Foo';
        getPersonAccount().lastName = 'Bar';

        if ( individualTypeRequested == null )
        {
            //default to PERSON_ACCOUNT
            setRecordType( PERSON_ACCOUNT_TYPES.PERSON_ACCOUNT.name() );
        }
        else
        {
            setRecordType( individualTypeRequested.name() );
        }
    }

    public mmcommon_PersonAccountTestDataFactory typePersonAccount()
    {
        configAcceptanceChecker('typePersonAccount()');

        this.individualTypeRequested = PERSON_ACCOUNT_TYPES.PERSON_ACCOUNT;

        return this;
    }
}