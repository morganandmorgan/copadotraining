public without sharing class DocsMergeRolesController {

    public class LwcOption {
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String value {get; set;}

        public LwcOption(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<LwcOption> getTemplates() {
        List<LwcOption> templates = new List<LwcOption>();

        for (litify_docs__Template__c t : [SELECT id, name FROM litify_docs__Template__c WHERE Multi_Merge__c = true]) {
            LwcOption option = new LwcOption(t.name,t.id);
            templates.add(option);
        }

        return templates;
    } //getTemplates


    @AuraEnabled(cacheable=true)
    public static List<litify_pm__Role__c> getRoles(Id matterId) {
        return [SELECT id, name, name__c, litify_pm__role__c, litify_pm__subtype__c, litify_pm__additional_trait__c, litify_pm__Matter__c FROM litify_pm__Role__c WHERE litify_pm__Matter__c = :matterId];
    } //getRoles


    @AuraEnabled(cacheable=true)
    public static List<LwcOption> getTeamMembers(Id matterId) {
        List<LwcOption> teamMembers = new List<LwcOption>();

        for (litify_pm__Matter_Team_Member__c tm : [SELECT id, name, litify_pm__Role__r.name FROM litify_pm__Matter_Team_Member__c WHERE 	litify_pm__Matter__c = :matterId]) {
            LwcOption option = new LwcOption(tm.litify_pm__Role__r.name + ' - ' + tm.name, tm.id);
            teamMembers.add(option);
        }

        return teamMembers;
    } //getTeamMembers


    private static String getLightiningSessionId() {
        String content;
        if (!Test.isRunningTest()) { //getContent doesn't work in tests
            content = Page.SessionIdForLightning.getContent().toString();
        } else {
            content = 'Start_Of_Session_Id123mockSessionId456End_Of_Session_Id';
        }
        String sessionId = content.substringBetween('Start_Of_Session_Id', 'End_Of_Session_Id');
        return sessionId;
    } //getLightningSessionId


    @AuraEnabled(cacheable=false)
    public static List<String> doMerge(String sourceIdMap, String roleIdSet) {

        String result;

        try {
            Map<String,Id> sourceIds = (Map<String,Id>)Json.deserialize(sourceIdMap, Map<String,Id>.class);
            Set<Id> roleIds = (Set<Id>)Json.deserialize(roleIdSet, Set<Id>.class);

            //result = String.valueOf( roleIds );

            DocsHelper dh = new DocsHelper();
            dh.sessionId = getLightiningSessionId();
            dh.multiMergeOnRoles(sourceIds, roleIds);

            result = 'Merge Complete';

        } catch (Exception e) {
            result = e.getMessage() + ' - ' + e.getStackTraceString();
        }

        List<String> results = new List<String>();
        results.add( result );

        return results;

    } //doMerge

} //class