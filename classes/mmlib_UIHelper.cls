public class mmlib_UIHelper
{
    public static List<SelectOption> getOperatorOptions()
    {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('', ''));
        options.add(new SelectOption('<', 'Less/Earlier Than'));
        options.add(new SelectOption('<=', 'Less/Earlier Than or Equal'));
        options.add(new SelectOption('=', 'Equals'));
        options.add(new SelectOption('<>', 'Does Not Equal'));
        options.add(new SelectOption('>=', 'Greater/Later Than or Equal'));
        options.add(new SelectOption('>', 'Greater/Later Than'));
        options.add(new SelectOption('contains', 'Contains'));
        options.add(new SelectOption('does_not_contain', 'Does Not Contain'));
        options.add(new SelectOption('contains_only', 'Contains Only'));

        return options;
    }

    public static List<SelectOption> getFormulaOptions()
    {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('', ''));
        options.add(new SelectOption('hours_ago', 'Hours Ago'));
        options.add(new SelectOption('days_ago', 'Days Ago'));
        options.add(new SelectOption('months_ago', 'Months Ago'));
        options.add(new SelectOption('years_ago', 'Years Ago'));

        return options;
    }

    public static List<SelectOption> getFieldList(Schema.sObjectType objectType)
    {
        Schema.DescribeSObjectResult description = objectType.getDescribe();

        Map<String, Schema.SObjectField> fields = description.fields.getMap();

        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('', ''));

        for (String fieldName : fields.keySet())
        {
            options.add(new SelectOption(fieldName, '[' + description.getName() + '].' + fields.get(fieldName).getDescribe().getLabel()));
        }

        return options;
    }
}