@isTest
private class mmcommon_AccountsTest {

    @isTest
    static void mainTest() {

        Account pa = TestUtil.createPersonAccount('Unit','Test');

        insert pa;

        pa.personEmail = 'test@forthepeople.com';
        pa.phone = '(407) 123-4567';
        pa.billingStreet = '321 Not Main St.';

        update pa;

        List<Account> accounts = new List<Account>{pa};
        mmcommon_Accounts mca = new mmcommon_Accounts(accounts);

        mca.validate();

        delete pa;

    } //mainTest

    @isTest
    static void SynchronizeSSNs() {
        String last4 = '4488';

        Account acct = new Account();
        acct.Social_Security_Number__c = '111-22-' + last4;

        mmcommon_Accounts ad = new mmcommon_Accounts(new List<Account> {acct});
        ad.synchronizeSSNFields();

        System.assertEquals(acct.Social_Security_Number__c, acct.SocialSecurityNumberSearch__c);
        System.assertEquals(last4, ad.getRecords().get(0).SocialSecurityNumber_Last4__c);
    } //SyncronizeSSNs

    @isTest
    static void SynchronizeSSNs_NoSSN() {
        Account acct = new Account();
        acct.Social_Security_Number__c = '';

        mmcommon_Accounts ad = new mmcommon_Accounts(new List<Account> {acct});
        ad.synchronizeSSNFields();

        System.assert(String.isBlank(acct.SocialSecurityNumberSearch__c));
        System.assert(String.isBlank(ad.getRecords().get(0).SocialSecurityNumber_Last4__c));
    } //SynchronizeSSNs_NoSSN

    @isTest
    static void SynchronizeSSNs_TooShort() {
        Account acct = new Account();
        acct.Social_Security_Number__c = '123';

        mmcommon_Accounts ad = new mmcommon_Accounts(new List<Account> {acct});
        ad.synchronizeSSNFields();

        System.assertEquals(acct.Social_Security_Number__c, acct.SocialSecurityNumberSearch__c);
        System.assert(String.isBlank(ad.getRecords().get(0).SocialSecurityNumber_Last4__c));
    } //SyncronizeSSNs_TooShort

    @isTest
    static void SynchronizeSSNs_RemoveSSN() {
        Account acct = new Account();
        acct.Social_Security_Number__c = '123';

        mmcommon_Accounts ad = new mmcommon_Accounts(new List<Account> {acct});
        ad.synchronizeSSNFields();

        acct.Social_Security_Number__c = null;

        ad = new mmcommon_Accounts(new List<Account> {acct});
        ad.synchronizeSSNFields();

        System.assert(String.isBlank(acct.SocialSecurityNumberSearch__c));
        System.assert(String.isBlank(ad.getRecords().get(0).SocialSecurityNumber_Last4__c));
    } //SyncronizeSSNs_RemoveSSN

} //class