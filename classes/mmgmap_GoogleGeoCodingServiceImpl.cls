public with sharing class mmgmap_GoogleGeoCodingServiceImpl
	implements mmgmap_IGeoCodingService
{
	// https://maps.googleapis.com/maps/api/geocode/json?
	// address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&
	// key=YOUR_API_KEY

	// ========== Implementation Methods ==================
	public Location getLatitudeLongitudeForAddress(mmgmap_Address addr)
	{
		Location loc = null;

		if (mmgmap_GoogleGeoCodingGetCallout.getIsEnabledInThisOrg() || isTest())
		{

			if (addr == null) {
				throw
					new mmgmap_GoogleGeoCodingExceptions.ParameterException(
						mmgmap_GoogleGeoCodingExceptions.ADDRESS_PARAMETER_NULL_MESSAGE);
			}

			if ( ! addr.isValidAddress() )
			{
				throw
					new mmgmap_GoogleGeoCodingExceptions.ParameterException(
						mmgmap_GoogleGeoCodingExceptions.INSUFFICIENT_DATA_MESSAGE);
			}

			String addressString = '';
			if (!String.isEmpty(addr.getStreet())) addressString += addr.getStreet() + ' ';
			if (!String.isEmpty(addr.getCity())) addressString += addr.getCity() + ' ';
			if (!String.isEmpty(addr.getState())) addressString += addr.getState() + ' ';
			if (!String.isEmpty(addr.getPostalCode())) addressString += addr.getPostalCode() + ' ';
			if (!String.isEmpty(addr.getCountry())) addressString += addr.getCountry();
			addressString = addressString.trim();
			addressString = addressString.replace('  ', ' ');

			Map<String, String> paramMap = new Map<String, String>();
			paramMap.put('address', EncodingUtil.urlEncode(addressString, 'UTF-8'));
system.debug('getLatitudeLongitudeForAddress area mark 3 - paramMap : '+ paramMap );
			mmgmap_GoogleGeoCodingGetCallout callout = mmgmap_GoogleGeoCodingGetCallout.newInstance(paramMap);
			if (isTest()) {
				callout = mock_callout;
			}

			try {
				callout.execute();
			}
			catch (Exception e) {
				throw new mmgmap_GoogleGeoCodingExceptions.CalloutExecutionException(e.getMessage(), e);
			}

			mmgmap_GoogleGeoCodingResponse calloutResponse = (mmgmap_GoogleGeoCodingResponse) callout.getResponse();

			if ('OK'.equalsIgnoreCase(calloutResponse.status)) {
				loc =
					Location.newInstance(
						Decimal.valueOf(calloutResponse.results.get(0).geometry.location.lat),
						Decimal.valueOf(calloutResponse.results.get(0).geometry.location.lng)
					);
			}
			else {
				throw new mmgmap_GoogleGeoCodingExceptions.ParameterException(calloutResponse.error_message);
			}
		}
		else {
			throw
				new mmgmap_GoogleGeoCodingExceptions.ParameterException(
					mmgmap_GoogleGeoCodingExceptions.DISABLED_SERVICE_MESSAGE);
		}

		return loc;
	}

	// ========== Testing Context ==================
	private Boolean isTest()
	{
		return mock_callout != null;
	}

	@TestVisible
	private mmgmap_GoogleGeoCodingGetCallout mock_callout = null;

}