public class SpringCMImportTaskStatus {
    public String Message { get; set; }
    public String Status { get; set; }
    public String Href { get; set; }
}