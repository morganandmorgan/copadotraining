@isTest
private class mmcommon_EventsSelectorTest {
    @isTest
    private static void selectMyTodayAndFutureEvents() {
        insert new List<Event>{
            TestUtil.createEvent(UserInfo.getUserId(), Datetime.now(), Datetime.now().addMinutes(60)),
            TestUtil.createEvent(UserInfo.getUserId(), Datetime.now().addDays(1), Datetime.now().addDays(2))
        };

        Test.startTest();
        System.assert(mmcommon_EventsSelector.newInstance().selectMyTodayStartEvents().size() == 1);
        System.assert(mmcommon_EventsSelector.newInstance().selectFutureOpenTasks(null).size() == 1);
        Test.stopTest();
    }

  @isTest
  private static void selectExistingEvents() {
    List<Event> eventsToQuery = new List<Event>{
      TestUtil.createAllDayEvent(UserInfo.getUserId(), Date.today(), Date.today()),
      TestUtil.createEvent(UserInfo.getUserId(), Datetime.now(), Datetime.now().addDays(1))
    };
    Database.insert(eventsToQuery);

    List<Event> otherEvents = new List<Event>{
      TestUtil.createAllDayEvent(UserInfo.getUserId(), Date.today(), Date.today()),
      TestUtil.createEvent(UserInfo.getUserId(), Datetime.now(), Datetime.now().addDays(1))
    };
    Database.insert(otherEvents);

    Set<Id> queryIds = new Map<Id, Event>(eventsToQuery).keySet();

    Test.startTest();
    List<Event> result = mmcommon_EventsSelector.newInstance().selectById(queryIds);
    Test.stopTest();

    Map<Id, Event> resultMap = new Map<Id, Event>(result);
    System.assertEquals(queryIds, resultMap.keySet());
  }

  @isTest
  private static void selectEventsByUserAndDate_AllDayEvents() {
    User user1 = TestUtil.createUser();
    User user2 = TestUtil.createUser();
    Database.insert(new List<User>{ user1, user2 });
    Set<Id> userIds = new Set<Id>{ user1.Id, user2.Id };

    Event user1day1 = TestUtil.createAllDayEvent(user1.Id, Date.newInstance(2016, 10, 1), Date.newInstance(2016, 10, 1));
    Event user1day2 = TestUtil.createAllDayEvent(user1.Id, Date.newInstance(2016, 10, 2), Date.newInstance(2016, 10, 2));
    Event user1day3 = TestUtil.createAllDayEvent(user1.Id, Date.newInstance(2016, 10, 3), Date.newInstance(2016, 10, 3));

    Event user2day1 = TestUtil.createAllDayEvent(user2.Id, Date.newInstance(2016, 10, 1), Date.newInstance(2016, 10, 1));
    Event user2day2 = TestUtil.createAllDayEvent(user2.Id, Date.newInstance(2016, 10, 2), Date.newInstance(2016, 10, 2));
    Event user2day3 = TestUtil.createAllDayEvent(user2.Id, Date.newInstance(2016, 10, 3), Date.newInstance(2016, 10, 3));

    Database.insert(new List<Event>{
        user1day1,
        user1day2,
        user1day3,
        user2day1,
        user2day2,
        user2day3
      });

    Set<Date> queryDates = new Set<Date>{
      Date.newInstance(2016, 10, 1),
      Date.newInstance(2016, 10, 3)
    };

    Test.startTest();
    List<Event> result = mmcommon_EventsSelector.newInstance().selectByUserIdAndDate(userIds, queryDates);
    Test.stopTest();

    Set<Id> expectedEventIds = new Set<Id>{
      user1day1.Id,
      user1day3.Id,
      user2day1.Id,
      user2day3.Id
    };

    Map<Id, Event> resultMap = new Map<Id, Event>(result);

    System.assertEquals(expectedEventIds, resultMap.keySet());
  }

  @isTest
  private static void selectEventsByUserAndDate_RegularEvents() {
    User user1 = TestUtil.createUser();
    User user2 = TestUtil.createUser();
    Database.insert(new List<User>{ user1, user2 });
    Set<Id> userIds = new Set<Id>{ user1.Id, user2.Id };

    Event user1day1 = TestUtil.createEvent(user1.Id, Datetime.newInstance(2016, 10, 1, 0, 0, 0), Datetime.newInstance(2016, 10, 2, 0, 0, 0));
    Event user1day2 = TestUtil.createEvent(user1.Id, Datetime.newInstance(2016, 10, 2, 6, 0, 0), Datetime.newInstance(2016, 10, 2, 16, 0, 0));
    Event user1day3 = TestUtil.createEvent(user1.Id, Datetime.newInstance(2016, 10, 3, 0, 0, 0), Datetime.newInstance(2016, 10, 3, 6, 0, 0));

    Event user2day1 = TestUtil.createEvent(user2.Id, Datetime.newInstance(2016, 10, 1, 12, 0, 0), Datetime.newInstance(2016, 10, 2, 12, 0, 0));
    Event user2day2 = TestUtil.createEvent(user1.Id, Datetime.newInstance(2016, 10, 2, 6, 0, 0), Datetime.newInstance(2016, 10, 2, 16, 0, 0));
    Event user2day3 = TestUtil.createEvent(user2.Id, Datetime.newInstance(2016, 10, 3, 0, 0, 0), Datetime.newInstance(2016, 10, 3, 6, 0, 0));

    Database.insert(new List<Event>{
        user1day1,
        user1day2,
        user1day3,
        user2day1,
        user2day2,
        user2day3
      });

    Set<Date> queryDates = new Set<Date>{
      Date.newInstance(2016, 10, 1),
      Date.newInstance(2016, 10, 2)
    };

    Test.startTest();
    List<Event> result = mmcommon_EventsSelector.newInstance().selectByUserIdAndDate(userIds, queryDates);
    Test.stopTest();

    Set<Id> expectedEventIds = new Set<Id>{
      user1day1.Id,
      user1day2.Id,
      user2day1.Id,
      user2day2.Id
    };

    Map<Id, Event> resultMap = new Map<Id, Event>(result);

    System.assertEquals(expectedEventIds, resultMap.keySet());
  }
}