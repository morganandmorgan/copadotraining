/**
 * SlackSlashCommandsRestTest
 * @description Unit test for SlackSlashCommandsRest
 * @author Matt Terrill
 * @date 8/22/2019
 */
@isTest
public with sharing class SlackSlashCommandsRestTest {

    @isTest
    private static void salesforceLimits() {

        Test.setMock(HttpCalloutMock.class, new GovernorLimitsApiTestMock());

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
         
        req.addParameter('token', 'ulp3onrYByb0CSa6QqTxzP9s');
        req.httpMethod = 'POST'; 
        RestContext.request = req;
        RestContext.response = res;
        SlackSlashCommandsRest.salesforceLimits();

    } //salesforceLimits

} //class