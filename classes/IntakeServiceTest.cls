/**
 * IntakeServiceTest
 * @description Test for Intake Service class.
 * @author Jeff Watson
 * @date 9/5/2019
 */
@IsTest
public with sharing class IntakeServiceTest {

    @IsTest
    private static void ctor() {
        Test.startTest();
        IntakeService intakeService = new IntakeService();
        System.assert(intakeService != null);
        Test.stopTest();
    }

    @IsTest
    static void significantEmailMatches()
    {
        String firstName = 'Jon';
        String lastName = 'Snow';
        String email = 'jon.snow@winterfell.com';

        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmcommon_IPersonAccountsService mockPersonAccountsService = (mmcommon_PersonAccountsServiceImpl) mocks.mock(mmcommon_PersonAccountsServiceImpl.class);

        mocks.startStubbing();
        mocks.when(
                mockPersonAccountsService.searchAccountsUsingHighPrecision(firstName, lastName, email, null, null))
                .thenReturn(new List<Account> {acct});
        mocks.stopStubbing();

        mm_Application.Service.setMock(mmcommon_IPersonAccountsService.class, mockPersonAccountsService);

        Map<SObjectField, String> fieldValueMap = new Map<SObjectField, String>();
        fieldValueMap.put(Account.FirstName, firstName);
        fieldValueMap.put(Account.LastName, lastName);
        fieldValueMap.put(Account.PersonEmail, email);

        Account matchingAccount = IntakeService.getExistingAccount(fieldValueMap);

        System.assertEquals(null, matchingAccount);
    }

    @IsTest
    static void significantPhoneMatches()
    {
        String firstName = 'Jon';
        String lastName = 'Snow';
        String phoneNum = '8885553400';

        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmcommon_IPersonAccountsService mockPersonAccountsService = (mmcommon_PersonAccountsServiceImpl) mocks.mock(mmcommon_PersonAccountsServiceImpl.class);

        mocks.startStubbing();
        mocks.when(
                mockPersonAccountsService.searchAccountsUsingHighPrecision(firstName, lastName, null, phoneNum, null))
                .thenReturn(new List<Account> {acct});
        mocks.stopStubbing();

        mm_Application.Service.setMock(mmcommon_IPersonAccountsService.class, mockPersonAccountsService);

        Map<SObjectField, String> fieldValueMap = new Map<SObjectField, String>();
        fieldValueMap.put(Account.FirstName, firstName);
        fieldValueMap.put(Account.LastName, lastName);
        fieldValueMap.put(Account.Phone, phoneNum);

        Account matchingAccount = IntakeService.getExistingAccount(fieldValueMap);

        System.assertEquals(null, matchingAccount);
    }

    @IsTest
    static void blankEmailAndPhoneDoesNotMatch()
    {
        String firstName = 'Jon';
        String lastName = 'Snow';

        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmcommon_IPersonAccountsService mockPersonAccountsService = (mmcommon_PersonAccountsServiceImpl) mocks.mock(mmcommon_PersonAccountsServiceImpl.class);

        mocks.startStubbing();
        mocks.when(
                mockPersonAccountsService.searchAccountsUsingHighPrecision(firstName, lastName, null, null, null))
                .thenReturn(new List<Account> {acct});
        mocks.stopStubbing();

        mm_Application.Service.setMock(mmcommon_IPersonAccountsService.class, mockPersonAccountsService);

        Map<SObjectField, String> fieldValueMap = new Map<SObjectField, String>();
        fieldValueMap.put(Account.FirstName, firstName);
        fieldValueMap.put(Account.LastName, lastName);

        Account matchingAccount = IntakeService.getExistingAccount(fieldValueMap);

        System.assertEquals(null, matchingAccount);
    }
}