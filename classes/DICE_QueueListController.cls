public with sharing class DICE_QueueListController {
  public List<DICE_Queue__c> Queues { get; set; }
	public DICE_QueueListController() {
		Queues = [SELECT Id, Name FROM DICE_Queue__c];
	}
}