@isTest
public class mmattyprtl_IntakeSignUpServiceImplTest {

	@isTest
    public static void maintTest() {

		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Contact att;
		System.runAs ( thisUser ) {        
        	String emailTemplate = 'CaseSignUpConfirmation_UserVariant';
            
            insert new Attorneys_Portal__c(Case_Sign_Up_Confirmation_Email_Template__c = emailTemplate);
            
            EmailTemplate et = new EmailTemplate();
            et.name = emailTemplate;
            et.developerName = emailTemplate;
            et.templateType= 'Text';
            et.folderId = UserInfo.getUserId();
            
            insert et;
            
            att = TestUtil.createAssignableAttorney();
            att.email = 'attorney@forthepeople.com';
            insert att;
            
        }


        Attorneys_Portal__c custSettings = Attorneys_Portal__c.getInstance();
        
		Map<Schema.SObjectField, String> fieldParameterMap = new Map<Schema.SObjectField, String>();
		fieldParameterMap.put(Account.LastName, 'Test');
        fieldParameterMap.put(Intake__c.Generating_Attorney__c, att.id);
       
        mmattyprtl_Enums.SCREENING_OPTIONS screeningOption = mmattyprtl_Enums.SCREENING_OPTIONS.CALL_AND_SCREEN;
        
        Set<String> additionalEmailAddressesToNotifySet = new Set<String>();
            
        Test.startTest();
		mmattyprtl_IntakeSignUpServiceImpl svc = new mmattyprtl_IntakeSignUpServiceImpl();   
        
        Intake__c i = svc.processSignUpRequest(fieldParameterMap, screeningOption, additionalEmailAddressesToNotifySet);

        
        screeningOption = mmattyprtl_Enums.SCREENING_OPTIONS.CALL_AND_SIGN_UP;

        i = svc.processSignUpRequest(fieldParameterMap, screeningOption, additionalEmailAddressesToNotifySet);

        
        screeningOption = mmattyprtl_Enums.SCREENING_OPTIONS.DO_NOT_CALL;

        i = svc.processSignUpRequest(fieldParameterMap, screeningOption, additionalEmailAddressesToNotifySet);
        Test.stopTest();

        System.assertNotEquals(null, mmattyprtl_IntakeSignUpServiceImpl.ORG_WIDE_EMAIL_ADDRESS);
        System.assertNotEquals(null, mmattyprtl_IntakeSignUpServiceImpl.ORG_WIDE_EMAIL_ADDRESS_NAME);

    } //mainTest
    
} //class