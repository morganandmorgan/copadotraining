@isTest
private class Matter_Service_Test {
	
	public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;

	static void setupData()
    {
    	/*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaBankAccount__c operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
        update operatingBankAccount;
       
		/*--------------------------------------------------------------------
        LITIFY Data Setup
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = TestDataFactory_FFA.company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        INSERT expenseType;

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Internet';
        expenseType2.ExternalID__c = 'INTERNET1';
        INSERT expenseType2;

        litify_pm__Expense_Type__c expenseType3 = new litify_pm__Expense_Type__c();
        expenseType3.CostType__c = 'HardCost';
        expenseType3.Name = 'Write Offs (Hard Costs)';
        expenseType3.ExternalID__c = 'WOFF1';
        INSERT expenseType3;

        litify_pm__Expense_Type__c expenseType4 = new litify_pm__Expense_Type__c();
        expenseType4.CostType__c = 'SoftCost';
        expenseType4.Name = 'Write Offs (Soft Costs)';
        expenseType4.ExternalID__c = 'WOFF2';
        INSERT expenseType4;

        List<litify_pm__Expense__c> expList = new List<litify_pm__Expense__c>();
        
        litify_pm__Expense__c testExpense = new litify_pm__Expense__c();
        testExpense.litify_pm__Matter__c = matter.Id;
        testExpense.litify_pm__Amount__c = 100.0;
        testExpense.litify_pm__Date__c = date.today();
        testExpense.litify_pm__ExpenseType2__c = expenseType.Id;
        expList.add(testExpense);

        litify_pm__Expense__c testExpense2 = new litify_pm__Expense__c();
        testExpense2.litify_pm__Matter__c = matter.Id;
        testExpense2.litify_pm__Amount__c = 10.0;
        testExpense2.litify_pm__Date__c = date.today();
        testExpense2.litify_pm__ExpenseType2__c = expenseType2.Id;
        expList.add(testExpense2);

        litify_pm__Expense__c testExpense3 = new litify_pm__Expense__c();
        testExpense3.litify_pm__Matter__c = matter.Id;
        testExpense3.litify_pm__Amount__c = -75.0;
        testExpense3.litify_pm__Date__c = date.today();
        testExpense3.litify_pm__ExpenseType2__c = expenseType.Id;
        expList.add(testExpense3);

        litify_pm__Expense__c testExpense4 = new litify_pm__Expense__c();
        testExpense4.litify_pm__Matter__c = matter.Id;
        testExpense4.litify_pm__Amount__c = -25.0;
        testExpense4.litify_pm__Date__c = date.today();
        testExpense4.litify_pm__ExpenseType2__c = expenseType3.Id;
        expList.add(testExpense4);

        litify_pm__Expense__c testExpense5 = new litify_pm__Expense__c();
        testExpense5.litify_pm__Matter__c = matter.Id;
        testExpense5.litify_pm__Amount__c = -5.0;
        testExpense5.litify_pm__Date__c = date.today();
        testExpense5.litify_pm__ExpenseType2__c = expenseType2.Id;
        expList.add(testExpense5);

        litify_pm__Expense__c testExpense6 = new litify_pm__Expense__c();
        testExpense6.litify_pm__Matter__c = matter.Id;
        testExpense6.litify_pm__Amount__c = -5.0;
        testExpense6.litify_pm__Date__c = date.today();
        testExpense6.litify_pm__ExpenseType2__c = expenseType4.Id;
        expList.add(testExpense6);

        insert expList;

        testDeposit = new Deposit__c();
        testDeposit.RecordTypeId = Schema.SObjectType.Deposit__c.RecordTypeInfosByName.get('Operating').RecordTypeId;
        testDeposit.Matter__c = matter.Id;
        testDeposit.Amount__c = 10.0;
        testDeposit.Check_Date__c = date.today();
        testDeposit.Source__c = 'Client';
        testDeposit.Operating_Cash_Account__c = operatingBankAccount.Id;
        INSERT testDeposit;

        // create new Trust Transaction
        Trust_Transaction__c tt1 = new Trust_Transaction__c();
        tt1.Date__c = date.today();
        tt1.Matter__c = matter.id;
        tt1.Amount__c = 100;
        INSERT tt1;

		Trust_Transaction__c tt2 = new Trust_Transaction__c();
        tt2.Date__c = date.today();
        tt2.Matter__c = matter.id;
        tt2.Amount__c = 100;
        INSERT tt2;
    }

    //================= TEST METHODS ====================

	@isTest static void test_resetDepositAllocations() {
		setupData();

		Set<Id> matterIds = new Set<Id>{matter.Id};
		Matter_Service ms = new Matter_Service();
		ms.resetDepositAllocations(matterIds);
	}

	@isTest static void test_executeAllocationReset() {
		setupData();
		Matter_Service.executeAllocationReset(matter.Id);
	}

	@isTest static void test_autoAllocateAllItems() {
		setupData();

		Set<Id> matterIds = new Set<Id>{matter.Id};
		Matter_Service ms = new Matter_Service();
		ms.autoAllocateAllItems(matterIds);
	}

	@isTest static void test_executeAutoAllocation() {
		setupData();

		Matter_Service.executeAutoAllocation(matter.Id);
	}

	@isTest static void test_allocateAllDeposits() {
		setupData();

		Set<Id> matterIds = new Set<Id>{matter.Id};
		Matter_Service ms = new Matter_Service();
		MatterAllocationService mas = new MatterAllocationService();
        MatterAllocationService.allocateItems (matterIds);
	}

	@isTest static void test_allocateNegativeExpenses() {
		setupData();

		Set<Id> matterIds = new Set<Id>{matter.Id};
		Matter_Service ms = new Matter_Service();
        MatterAllocationService mas = new MatterAllocationService();
        MatterAllocationService.allocateItems (matterIds);
	}	
    
}