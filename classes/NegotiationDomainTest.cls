@IsTest
public class NegotiationDomainTest {

    @IsTest
    private static void testConstructors() {
        Test.startTest();
        NegotiationDomain domain = new NegotiationDomain();
        System.assert(domain != null);
        domain = new NegotiationDomain(new List<litify_pm__Negotiation__c>());
        System.assert(domain != null);
        Test.stopTest();
    } //testConstructors

    @IsTest
    private static void testTriggers() {

        Account a = TestUtil.createPersonAccount('Test','Account');
        insert a;

        litify_pm__Matter__c m = TestUtil.createMatter(a);
        m.litify_pm__Principal_Attorney__c = UserInfo.getUserId();
        insert m;

        litify_pm__Negotiation__c negotiation = new litify_pm__Negotiation__c();

        negotiation.litify_pm__Matter__c = m.id;
        negotiation.submit_for_approval__c = false;
        negotiation.approving_attorney__c = UserInfo.getUserId();
        negotiation.name = 'Test Negotiation';
        negotiation.litify_pm__Date__c = Date.today();
        negotiation.insurance_Company__c = 'AAA Insurance Company';
        
        insert negotiation;

        litify_pm__Negotiation__c negotiation2 = new litify_pm__Negotiation__c();

        negotiation2.litify_pm__Matter__c = m.id;
        negotiation2.submit_for_approval__c = false;
        negotiation2.approving_attorney__c = UserInfo.getUserId();
        negotiation2.name = 'Test Negotiation';
        negotiation2.litify_pm__Date__c = Date.today().addMonths(1);
        negotiation2.insurance_Company__c = 'AAA Insurance Company';
        
        insert negotiation2;
        
        negotiation.name = 'Changed Name';
        negotiation.litify_pm__Date__c = Date.today().addMonths(3);

        update negotiation;

        
        RecordType demandRecordType = [SELECT id, name, sObjectType FROM RecordType WHERE sObjectType = 'litify_pm__Negotiation__c' AND name = 'Offer'];
        
        litify_pm__Negotiation__c offerNeg = new litify_pm__Negotiation__c();

		offerNeg.recordTypeId = demandRecordType.id;                                       
                                       
        offerNeg.litify_pm__Matter__c = m.id;
        offerNeg.submit_for_approval__c = false;
        offerNeg.approving_attorney__c = UserInfo.getUserId();
        offerNeg.name = 'Test Negotiation';
        offerNeg.litify_pm__Date__c = Date.today().addMonths(1);
        offerNeg.insurance_Company__c = 'AAA Insurance Company';
        
        insert offerNeg;
        
        offerNeg.litify_pm__Date__c = Date.today().addMonths(2);
        update offerNeg;
        
        
        delete offerNeg;
        
    }

} //class