@isTest
private class mmcommon_CreateHolidayForUsersActionTest
{
    private static void assertHolidaysForUsers(List<User> users, List<Date> orderedDates)
    {
        for (User u : users)
        {
            system.debug( 'u == ' + u );

            List<Event> userEvents = mmcommon_EventsSelector.newInstance().selectByUserOrderByDate(new Set<Id>{ u.Id });
            system.debug( 'userEvents == ' + userEvents.size() );
            System.assertEquals( orderedDates.size(), userEvents.size() );
            for (Integer i = 0; i < orderedDates.size(); ++i)
            {
                Date d = orderedDates.get(i);
                Event userEvent = userEvents.get(i);

                system.debug( 'd == ' + d );
                system.debug( 'userEvent == ' + userEvent );

                System.assertEquals(d, userEvent.ActivityDate);
                System.assertEquals(true, userEvent.IsAllDayEvent);
                System.assertEquals(mmcommon_Events.EVENT_SUBJECT_HOLIDAY, userEvent.Subject);
            }
        }
    }

    @isTest
    private static void createHolidaysForUsers()
    {
        User user1 = TestUtil.createUser();
        User user2 = TestUtil.createUser();
        List<User> users = new List<User>{ user1, user2 };
        Database.insert(users);

        Map<Id, User> userMap = new Map<Id, User>(users);

        System.assertEquals(0, mmcommon_EventsSelector.newInstance().selectByUserOrderByDate(userMap.keySet()).size());

        Date fixedDate = Date.newInstance(2000, 12, 25);
        Date today = Date.today();

        Set<Date> dates = new Set<Date>{ fixedDate, today };

        mmcommon_CreateHolidayForUsersAction.futureHolidays.addAll( dates );

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        mmcommon_CreateHolidayForUsersAction action = new mmcommon_CreateHolidayForUsersAction();
        action.setUnitOfWork( uow );
        action.setRecordsToActOn( users );

        Test.startTest();

        action.run();

        uow.commitWork();

        Test.stopTest();

        assertHolidaysForUsers(users, new List<Date>{ fixedDate, today });
    }

    @isTest
    private static void skipExistingHolidaysForUsers()
    {
        User user1 = TestUtil.createUser();
        User user2 = TestUtil.createUser();
        List<User> users = new List<User>{ user1, user2 };
        Database.insert(users);

        Map<Id, User> userMap = new Map<Id, User>(users);

        //System.assertEquals(0, mmcommon_EventsSelector.newInstance().selectByUserOrderByDate(userMap.keySet()).size());

        Date fixedDate = Date.newInstance(2000, 12, 25);
        Date today = Date.today();

        Set<Date> dates = new Set<Date>{ fixedDate, today };

        mmcommon_CreateHolidayForUsersAction.futureHolidays.addAll( dates );

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        mmcommon_CreateHolidayForUsersAction action = new mmcommon_CreateHolidayForUsersAction();
        action.setUnitOfWork( uow );
        action.setRecordsToActOn( users );

        // create pre-existing event for a single user
        Event mockEvent1 = new Event();
        mockEvent1.ownerId = user1.id;
        mockEvent1.ActivityDate = fixedDate;
        mockEvent1.IsAllDayEvent = true;
        mockEvent1.Subject = mmcommon_Events.EVENT_SUBJECT_HOLIDAY;

        Event mockEvent2 = new Event();
        mockEvent2.ownerId = user1.id;
        mockEvent2.ActivityDate = today;
        mockEvent2.IsAllDayEvent = true;
        mockEvent2.Subject = mmcommon_Events.EVENT_SUBJECT_HOLIDAY;

        Event mockEvent3 = new Event();
        mockEvent3.ownerId = user1.id;
        mockEvent3.ActivityDate = fixedDate;
        mockEvent3.IsAllDayEvent = true;
        mockEvent3.Subject = mmcommon_Events.EVENT_SUBJECT_HOLIDAY;

        Event mockEvent4 = new Event();
        mockEvent4.ownerId = user1.id;
        mockEvent4.ActivityDate = today;
        mockEvent4.IsAllDayEvent = true;
        mockEvent4.Subject = mmcommon_Events.EVENT_SUBJECT_HOLIDAY;

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmcommon_IEventsSelector mockEventsSelector = (mmcommon_IEventsSelector) mocks.mock(mmcommon_IEventsSelector.class);

        mocks.startStubbing();

        mocks.when(mockEventsSelector.sObjectType()).thenReturn(Event.SObjectType);
        mocks.when(mockEventsSelector.selectByUserIdAndDate(new Set<Id>{ user1.Id, user2.Id }, dates )).thenReturn(new List<Event>{ mockEvent1 });
        mocks.when(mockEventsSelector.selectByUserOrderByDate(new Set<Id>{ user1.Id } )).thenReturn(new List<Event>{ mockEvent1, mockEvent2 });
        mocks.when(mockEventsSelector.selectByUserOrderByDate(new Set<Id>{ user2.Id } )).thenReturn(new List<Event>{ mockEvent3, mockEvent4 });

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockEventsSelector);


        //action.run(new List<User>{ user1 }, new Set<Date>{ fixedDate });

        Test.startTest();

        //action.run(users, dates);
        action.run();

        uow.commitWork();

        Test.stopTest();

        assertHolidaysForUsers(users, new List<Date>{ fixedDate, today });
    }
}