/**
 *  Domain action to create holiday entries on their calendars
 */
public class mmcommon_CreateHolidayForUsersAction
    extends mmlib_AbstractAction
{
    @TestVisible
    private static Set<Date> futureHolidays = new Set<Date>();

    private static Set<Date> getFutureHolidayDates()
    {
        system.debug( 'futureHolidays == ' + futureHolidays);
        if (futureHolidays.isEmpty())
        {
            for (Holiday h : mmcommon_HolidaysSelector.newInstance().selectAllFuture())
            {
                futureHolidays.add( h.ActivityDate );
            }
        }
        system.debug( 'futureHolidays == ' + futureHolidays);
        return futureHolidays;
    }

    public override Boolean runInProcess()
    {
        Set<Date> datesToCreate = getFutureHolidayDates();

        if ( ! datesToCreate.isEmpty() )
        {
            system.debug( 'foobar 1' );
            List<Event> existingUserEvents = mmcommon_EventsSelector.newInstance().selectByUserIdAndDate( (new Map<Id, SObject>( this.records )).keySet().clone(), datesToCreate.clone());
system.debug( 'existingUserEvents == ' + existingUserEvents );
            Map<Id, List<Event>> existingUserEventsMap = mapEventsByOwner(existingUserEvents);
system.debug( 'existingUserEventsMap.size() == ' + existingUserEventsMap.size() );
            Event newEvent = null;

            for (SObject record : this.records)
            {
system.debug( 'record == ' + record );
                List<Event> existingEvents = existingUserEventsMap.get(record.Id);

                for (Date d : datesToCreate)
                {
                    system.debug( 'foobar 2' );
                    if (existingEvents != null && alreadyHasHolidayEvent(d, existingEvents))
                    {
                        system.debug( 'foobar 3' );
                        continue;
                    }
                    system.debug( 'foobar 4' );

                    newEvent = mmcommon_Events.createHolidayEvent( record.Id, d );
                    system.debug( 'newEvent == ' + newEvent );

                    if ( this.uow != null )
                    {
                        system.debug( 'foobar 5' );
                        this.uow.registerNew( newEvent );
                    }
                }
            }
        }

        return true;
    }

    private static Map<Id, List<Event>> mapEventsByOwner(List<Event> events)
    {
        Map<Id, List<Event>> result = new Map<Id, List<Event>>();
        for (Event e : events)
        {
            Id mapKey = e.OwnerId;
            List<Event> mapValue = result.get(mapKey);
            if (mapValue == null)
            {
                mapValue = new List<Event>();
                result.put(mapKey, mapValue);
            }
            mapValue.add(e);
        }
        return result;
    }

    private static Boolean alreadyHasHolidayEvent(Date holidayDate, List<Event> existingEvents)
    {
        for (Event existingEvent : existingEvents)
        {
            if ( eventIsHoliday(existingEvent) && eventIncludesDate(holidayDate, existingEvent) )
            {
                return true;
            }
        }

        return false;
    }

    private static Boolean eventIsHoliday(Event e)
    {
        return e.Subject == mmcommon_Events.EVENT_SUBJECT_HOLIDAY
                            && e.IsAllDayEvent == true;
    }

    private static Boolean eventIncludesDate(Date d, Event e)
    {
        Datetime dtStart = Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0));

        Datetime dtEnd = dtStart.addDays(1);

        return (  e.IsAllDayEvent == true
                 && e.ActivityDate <= d
                 && e.ActivityDate >= d
               )
               || ( e.IsAllDayEvent != true
                  && ( ( e.StartDateTime >= dtStart
                       && e.StartDateTime < dtEnd
                       )
                     || ( e.EndDateTime > dtStart
                         && e.EndDateTime <= dtEnd
                         )
                     || ( e.StartDateTime <= dtStart
                         && e.EndDateTime >= dtEnd
                         )
                     )
                  );
    }
}