public with sharing class RelatedIntakeInvestigationsExt {
  public class InvestigationWrapper {
    public Datetime startDateTime { get; private set; }
    public String investigationStatus { get; private set; }
    public Intake__c intake { get; private set; }
    public IncidentInvestigationEvent__c incie { get; private set; }
    public Event Event { get; private set; }

    private InvestigationWrapper(IntakeInvestigationEvent__c intie, Event ev) {
      this.startDateTime = intie.StartDateTime__c;
      this.investigationStatus = intie.Investigation_Status__c;
      this.intake = intie.Intake__r;
      this.incie = intie.IncidentInvestigation__r;
      this.Event = ev;
    }

    public Boolean getEventLinkIsActive() {
      return investigationStatus != INVESTIGATION_STATUS_CANCELED && investigationStatus != INVESTIGATION_STATUS_RESCHEDULED;
    }
  }

  public List<InvestigationWrapper> IntakeInvestigations { get; private set; }
  public List<InvestigationWrapper> otherIntakeInvestigations { get; private set; }
  public Boolean HasEvents { get {
      return IntakeInvestigations.size() > 0;
    }}
  
  private final ApexPages.StandardController stdController;

  @TestVisible private static final String INVESTIGATION_STATUS_CANCELED = 'Canceled';
  @TestVisible private static final String INVESTIGATION_STATUS_RESCHEDULED = 'Rescheduled';


  public RelatedIntakeInvestigationsExt(ApexPages.StandardController stdControllerParam) {
    stdController = stdControllerParam;
    if (!Test.isRunningTest()) {
      stdController.addFields(new List<String>{ 'Client__c' });
    }

    Intake__c thisIntake = (Intake__c) stdController.getRecord();

    List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents(thisIntake);

    Set<Id> incidentInvestigationEventIds = new Set<Id>();
    for (IntakeInvestigationEvent__c intakeInvestigationEvent : intakeInvestigationEvents) {
      if (intakeInvestigationEvent.IncidentInvestigation__c != null) {
        incidentInvestigationEventIds.add(intakeInvestigationEvent.IncidentInvestigation__c);
      }
    }
    List<IntakeInvestigationEvent__c> otherIntakeInvestigationEvents = getOtherIntakeInvestigationEvents(thisIntake, incidentInvestigationEventIds);
    
    List<Event> events = getEvents(intakeInvestigationEvents);

    IntakeInvestigations = buildInvestigationWrappers(intakeInvestigationEvents);
    otherIntakeInvestigations = buildInvestigationWrappers(otherIntakeInvestigationEvents);
  }

  private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents(Intake__c selectedIntake) {
    List<IntakeInvestigationEvent__c> intakeInvestigationEvents = [
      SELECT Id, 
        IncidentInvestigation__c, 
        IncidentInvestigation__r.Canceled_Rescheduled_By__c, 
        StartDateTime__c, 
        EndDateTime__c, 
        OwnerId, 
        Owner.Name, 
        Location__c, 
        Investigation_Status__c, 
        Intake__r.Id, 
        Intake__r.Name, 
        Intake__r.Case_Type__c, 
        Intake__r.Incident__c 
      FROM IntakeInvestigationEvent__c 
      WHERE Intake__r.Client__c = :selectedIntake.Client__c
        AND Intake__c = :selectedIntake.Id
      ORDER BY 
        StartDateTime__c ASC,
        Name ASC
    ];
    return intakeInvestigationEvents;
  }

  private static List<IntakeInvestigationEvent__c> getOtherIntakeInvestigationEvents(Intake__c selectedIntake, Set<Id> incidentInvestigationsToExclude) {
    List<IntakeInvestigationEvent__c> intakeInvestigationEvents = [
      SELECT Id, 
        IncidentInvestigation__c,
        IncidentInvestigation__r.Canceled_Rescheduled_By__c, 
        StartDateTime__c, 
        EndDateTime__c, 
        OwnerId, 
        Owner.Name, 
        Location__c, 
        Investigation_Status__c, 
        Intake__r.Id, 
        Intake__r.Name, 
        Intake__r.Case_Type__c, 
        Intake__r.Incident__c 
      FROM IntakeInvestigationEvent__c 
      WHERE Intake__r.Client__c = :selectedIntake.Client__c
        AND Intake__c != :selectedIntake.Id
        AND Investigation_Status__c NOT IN (:INVESTIGATION_STATUS_CANCELED, :INVESTIGATION_STATUS_RESCHEDULED)
        AND StartDateTime__c > :Datetime.now()
        AND IncidentInvestigation__c NOT IN :incidentInvestigationsToExclude
      ORDER BY 
        StartDateTime__c ASC,
        Name ASC
    ];
    return intakeInvestigationEvents;
  }

  private static List<InvestigationWrapper> buildInvestigationWrappers(List<IntakeInvestigationEvent__c> intakeInvestigationEvents) {
    List<Event> events = getEvents(intakeInvestigationEvents);

    List<InvestigationWrapper> result = new List<InvestigationWrapper>();

    for(IntakeInvestigationEvent__c intie : intakeInvestigationEvents) {
      Event associatedEvent = null;
      for (Event ev : events) {
        if (ev.WhatId == intie.IncidentInvestigation__c) {
          associatedEvent = ev;
          break;
        }
      }

      InvestigationWrapper wrapper = new InvestigationWrapper(intie, associatedEvent);
      result.add(wrapper);
    }
    return result;
  }

  private static List<Event> getEvents(List<IntakeInvestigationEvent__c> intakeInvestigationEvents) {
    Set<Id> intieIDs = new Set<Id>();
    for (IntakeInvestigationEvent__c ev : intakeInvestigationEvents) {
      intieIDs.add(ev.IncidentInvestigation__c);
    }
    
    List<Event> events = [SELECT Id, WhatId, WhoId, Who.Name, OwnerId, Owner.Name FROM Event WHERE WhatId IN :intieIDs AND Type = :ScheduleInvestigatorService.EVENT_TYPE];
    return events;
  }
}