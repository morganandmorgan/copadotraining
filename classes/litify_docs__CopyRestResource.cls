/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/copy')
global class CopyRestResource {
    global CopyRestResource() {

    }
    @HttpPost
    global static void doPost() {

    }
}
