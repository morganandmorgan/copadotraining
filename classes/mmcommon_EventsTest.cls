@isTest
private class mmcommon_EventsTest {

  @isTest
  private static void createHolidayEvent() {
    Id ownerId = TestUtil.getFakeId(User.SObjectType);
    Date eventDate = Date.today();

    Test.startTest();
    Event result = mmcommon_Events.createHolidayEvent(ownerId, eventDate);
    Test.stopTest();

    System.assertEquals(mmcommon_Events.EVENT_SUBJECT_HOLIDAY, result.Subject);
    System.assertEquals(ownerId, result.OwnerId);
    System.assertEquals(eventDate, result.ActivityDate);
  }
}