@isTest
private class Test_MatterInfoRequestsDataController {
    
    @testSetup static void setupRecords() {
        RecordType matterRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'litify_pm__Matter__c' AND DeveloperName = 'Personal_Injury'];
        RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account' LIMIT 1];
        
        List<Account> acc = new List<account>{new Account(LastName = 'TestAcc 1', RecordTypeId = acctRcdType.Id)};
        Insert acc;

        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c(Name = 'TestCaseType 1');
        insert caseType;

        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c(Name = 'TestMatterPlan 1');
        matterPlan.litify_pm__Case_Type__c = caseType.Id;
        insert matterPlan;

        User testUser = createUser();
        insert testUser;

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = matterRcdType.Id;
        matter.ReferenceNumber__c = '999999056';
        matter.litify_pm__Client__c = acc[0].Id;
        matter.litify_pm__Principal_Attorney__c = testUser.Id;
        matter.litify_pm__Open_Date__c = Date.today();
        matter.litify_pm__Case_Type__c = caseType.Id;
        insert matter;

        /*Information_Requests__c infoObj = new Information_Requests__c(Request__c = 'Request',Related_Matter__c = matter.Id );
        insert infoObj;*/
        litify_pm__Request__c infoObj = new litify_pm__Request__c(Request__c = '911 Tapes', litify_pm__Matter__c = matter.Id);
        insert infoObj;
    }

    @isTest static void test_component_controller() {
        // Saving an Information_Requests__c record
        Test.startTest();
        litify_pm__Matter__c matterObj = [SELECT Id FROM litify_pm__Matter__c][0];      
        /*Information_Requests__c infoObj = [SELECT Id, Request__c FROM Information_Requests__c WHERE Related_Matter__c =: matterObj.Id LIMIT 1];*/
        litify_pm__Request__c infoObj = [SELECT Id, Request__c FROM litify_pm__Request__c WHERE litify_pm__Matter__c =: matterObj.Id LIMIT 1];

        
        infoObj.Request__c = 'Police Report';
        String infoId = MatterInformationRequestsDataController.saveRecord(infoObj);
        System.assertEquals('Police Report', infoObj.Request__c);
        System.assertEquals(infoObj.Id, infoId);
        
        // Deleting an Information_Requests__c record
        MatterInformationRequestsDataController.deleteRecord(infoObj);   
        try { MatterInformationRequestsDataController.deleteRecord(infoObj); }
        catch (Exception dmx) {}    
        try { MatterInformationRequestsDataController.saveRecord(infoObj); }
        catch (Exception dmx) {}
        Test.stopTest();
    }

    public static User createUser() {
        Integer userNum = 1;
        return new User(
            IsActive = true,
            FirstName = 'FirstName ' + userNum,
            LastName = 'LastName ' + userNum,
            //Username = 'apex.temp.test.user@sample.com.' + userNum,
            Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@xyz.com',
            Email = 'apex.temp.test.user@sample.com.' + userNum,
            Alias = 'alib' + userNum,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            Territory__c = null
        );
    }       
}