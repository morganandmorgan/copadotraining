public class AccountUpdateIncident_TrigAct extends TriggerAction {

    private final Set<Id> personsRequiringIncidentUpdates = new Set<Id>();

    public AccountUpdateIncident_TrigAct() {
        super();
    }

    public override Boolean shouldRun() {
        personsRequiringIncidentUpdates.clear();
        if (isBefore() && isDelete()) {
            Map<Id, Account> accountMap = (Map<Id, Account>) triggerMap;
            for (Account acc : accountMap.values()) {
                if (acc.IsPersonAccount) {
                    personsRequiringIncidentUpdates.add(acc.Id);
                }
            }
        }
        else if (isAfter() && isUpdate()) {
            Map<Id, Account> accountNewMap = (Map<Id, Account>) triggerMap;
            Map<Id, Account> accountOldMap = (Map<Id, Account>) triggerOldMap;
            for (Account acc : accountNewMap.values()) {
                Account oldAccount = accountOldMap.get(acc.Id);
                if (isPersonNameChanged(acc, oldAccount)) {
                    personsRequiringIncidentUpdates.add(acc.Id);
                }
            }
        }
        return !personsRequiringIncidentUpdates.isEmpty();
    }

    private static Boolean isPersonNameChanged(Account newAccount, Account oldAccount) {
        return newAccount.IsPersonAccount
            && (newAccount.FirstName != oldAccount.FirstName
                || newAccount.LastName != oldAccount.LastName);
    }

    public override void doAction() {
        if (isBefore() && isDelete()) {
            IncidentService.updateIncidentLabelsForAccountDeletion(personsRequiringIncidentUpdates);
        }
        else if (isAfter() && isUpdate()) {
            IncidentService.updateIncidentLabelsForAccountChange(personsRequiringIncidentUpdates);
        }
    }
}