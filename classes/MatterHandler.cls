public class MatterHandler {

    //Method called from MatterTrigger on after Insert
    public static void onAfterInsert(List <litify_pm__Matter__c> matterList) {
        Map<String, String> fieldsToMap = getFieldsToMapFromCMT();
        Map<String, Set<String>> fieldToMapValues = new Map<String, Set<String>>();
        Map<String, Map<String, String>> matterFieldValues = new Map<String, Map<String, String>>();
        if (!fieldsToMap.isEmpty()) {
            for (litify_pm__Matter__c matter : matterList) {
                fetchCMT(matter, fieldToMapValues, matterFieldValues, fieldsToMap);
            }
            //Fetching Criteria List From CMT
            List<Matter_Information_Request_Setting__mdt> mappingList = getCriteriaListFromCMT(fieldToMapValues, false);
            //Inserting Infos
            if (!mappingList.isEmpty()) {
                createInfoRecords(mappingList, matterFieldValues, matterList, false);
            }
        }

        System.debug(LoggingLevel.WARN, 'Gov Limits at MatterHandler - onAfterInsert: \n' + FFAUtilities.checkGovLimits());
    }

    public static void onAfterUpdate(List <litify_pm__Matter__c> matterList, Map<Id,litify_pm__Matter__c> oldMatterMap) {
        Map<String, String> fieldsToMap = getFieldsToMapFromCMT();
        Map<String, Set<String>> fieldToMapValues = new Map<String, Set<String>>();
        Map<String, Map<String, String>> matterFieldValues = new Map<String, Map<String, String>>();
        List<litify_pm__Matter__c> acceptedMatterList = new List<litify_pm__Matter__c>();
        for (litify_pm__Matter__c matter : matterList){
            for (String matterField : fieldsToMap.keySet() ){
                if (matter.get(matterField) != oldMatterMap.get(matter.id).get(matterField)){
                    acceptedMatterList.add(matter);
                    break;
                }
            }
        }
        if (!fieldsToMap.isEmpty() && !acceptedMatterList.isEmpty()) {
            for (litify_pm__Matter__c matter : matterList) {
                fetchCMT(matter, fieldToMapValues, matterFieldValues, fieldsToMap);
            }
            //Fetching Criteria List From CMT
            List<Matter_Information_Request_Setting__mdt> mappingList = getCriteriaListFromCMT(fieldToMapValues, true);
            //Inserting Infos
            if (!mappingList.isEmpty()) {
                createInfoRecords(mappingList, matterFieldValues, matterList, true);
            }
        }

        System.debug(LoggingLevel.WARN, 'Gov Limits at MatterHandler - onAfterUpdate: \n' + FFAUtilities.checkGovLimits());
    }

    //Fetch dynamic mapping from CMT
    public static Map<String, String> getFieldsToMapFromCMT() {
        List<Matter_Information_Request_Mapping__mdt> fieldMappingList = [SELECT MasterLabel, QualifiedApiName, 
                                                                                CMT_Field__c, Matter_Field__c 
                                                                                FROM Matter_Information_Request_Mapping__mdt];
        Map<String, String> fieldsToMap = new Map <String, String>();
        for (Matter_Information_Request_Mapping__mdt requestMappingObj: fieldMappingList) {
            fieldsToMap.put(requestMappingObj.Matter_Field__c, requestMappingObj.CMT_Field__c);
        }
        return fieldsToMap;
    }

    //Get final value mapping
    public static void fetchCMT(litify_pm__Matter__c matter, Map<String, Set<String>> fieldToMapValues, 
                                    Map<String, Map<String, String>> matterFieldValues, 
                                    Map<String, String> fieldsToMap) {
        for (String fieldName: fieldsToMap.keyset()) {
            if (matter.get(fieldName) != null) {
                if (fieldToMapValues.get(fieldsToMap.get(fieldName)) == null) {

                    fieldToMapValues.put(fieldsToMap.get(fieldName), new Set<String>{
                        String.valueOf(matter.get(fieldname))
                    });                    
                } else {                    
                    Set<String> fieldValues = fieldToMapValues.get(fieldsToMap.get(fieldName));
                    fieldValues.add(String.valueOf(matter.get(fieldName)));
                    fieldToMapValues.put(fieldsToMap.get(fieldName), fieldValues);                    
                }
                if (matterFieldValues.get(matter.Id) == null) {
                    matterFieldValues.put(matter.Id, new Map<String, String>{
                        fieldname + ',' + fieldsToMap.get(fieldName) => String.valueOf(matter.get(fieldname))
                    });
                } else {
                    Map<String, String> mapValues = matterFieldValues.get(matter.Id);
                    mapValues.put(fieldname + ',' + fieldsToMap.get(fieldName), String.valueOf(matter.get(fieldname)));
                    matterFieldValues.put(matter.Id, mapValues);
                }
            }
        }
    }

    //Making dynamic query for CMT 
    public static List<Matter_Information_Request_Setting__mdt> getCriteriaListFromCMT(Map<String, Set<String>> fieldToMapValues, Boolean isUpdateTrigger) {
        List<Matter_Information_Request_Setting__mdt> mappingList = new List<Matter_Information_Request_Setting__mdt>();
        if (!fieldToMapValues.isEmpty()) {
            String queryBeforeFrom = 'SELECT Id, MasterLabel,Trigger_on_edit__c, Document_Link__c, ';
            String queryAfterFrom = 'FROM Matter_Information_Request_Setting__mdt WHERE ';
            if (isUpdateTrigger){
                queryAfterFrom = queryAfterFrom + 'Trigger_on_edit__c = true AND ';
            }
            for (String fieldToMapApiName: fieldToMapValues.keySet()) {
                queryBeforeFrom = queryBeforeFrom + fieldToMapApiName + ', ';
                String fieldValuesAsString = JSON.serialize(fieldToMapValues.get(fieldToMapApiName));
                String fieldType = String.valueOf(Schema.SObjectType.Matter_Information_Request_Setting__mdt.fields.getMap().get(fieldToMapApiName.toLowerCase()).getDescribe().getType());
                fieldType = fieldType.toLowerCase();
                if (fieldType == 'date') {
                    fieldValuesAsString = fieldValuesAsString.replace('[', '(').replace(']', ')').replaceAll('"', '').replaceAll('00:00:00','');
                }
                else if (fieldType == 'percent' || fieldType == 'integer' ||
                    fieldType == 'double' || fieldType == 'currency' ||
                    fieldType == 'datetime' || fieldType == 'time' || 
                    fieldType == 'boolean') {
                    fieldValuesAsString = fieldValuesAsString.replace('[', '(').replace(']', ')').replaceAll('"', '');
                } else {
                    fieldValuesAsString = fieldValuesAsString.replace('[', '(').replace(']', ')').replaceAll('"', '\'');
                }
                queryAfterFrom = queryAfterFrom + fieldToMapApiName + ' IN ' + fieldValuesAsString + ' AND ';
            }
            queryBeforeFrom = queryBeforeFrom.removeEndIgnoreCase(', ');
            queryAfterFrom = queryAfterFrom.removeEndIgnoreCase('AND ');
            String query = queryBeforeFrom + ' ' + queryAfterFrom;
            try {
                mappingList = Database.query(query);
            } 
            catch (Exception e) {
                System.debug(e.getMessage());
            }
        }
        return mappingList;
    }

    //Creating information request records
    public static void createInfoRecords(List<Matter_Information_Request_Setting__mdt> mappingList, 
                                            Map<String, Map<String, String>> matterFieldValues, 
                                            List<litify_pm__Matter__c> matterList, Boolean isUpdateTrigger) {
        List<litify_pm__Request__c> infoReqestList = new List<litify_pm__Request__c>();
        Map<Id, litify_pm__Matter__c> matterInfoRequestMap;
        Set<String> matterIdSet = new Set<String>();
        System.debug('Update : ' + isUpdateTrigger);
        if (isUpdateTrigger){
            for (litify_pm__Matter__c matter : matterList){
                matterIdSet.add(matter.id);
            }
            if (!matterIdSet.isEmpty()){
                try{
                     matterInfoRequestMap = new Map<Id, litify_pm__Matter__c>([Select id, (select litify_pm__Matter__c,Request__c,litify_pm__Facility__c,litify_pm__Facility_Name__c,
                                                litify_pm__Date_Requested__c,litify_pm__Date_Received__c,litify_pm__Comments__c,Status__c,
                                                litify_pm__Document__c FROM litify_pm__Requests__r)  From litify_pm__Matter__c WHERE id IN: matterIdSet]);
                }
                catch(Exception e){
                    System.debug(e.getMessage());
                }
            }
        }
         
        if (!mappingList.isEmpty()) {
            for (litify_pm__Matter__c matter: matterList) {
                Set<String> existedInfoRequestSet = new Set<String>();
                if (isUpdateTrigger && !matterInfoRequestMap.get(matter.id).litify_pm__Requests__r.isEmpty()){
                    for (litify_pm__Request__c info :  matterInfoRequestMap.get(matter.id).litify_pm__Requests__r){
                        existedInfoRequestSet.add(info.Request__c);
                    }
                }
                for (Matter_Information_Request_Setting__mdt mapperObj : mappingList) {
                    Boolean attachToMatter = true;
                    for (String fieldName: matterFieldValues.get(matter.Id).keyset()) {
                        String matterField = fieldName.substringBefore(',');
                        String cmtField = fieldName.substringafter(',');
                        if ((matter.get(matterField) != mapperObj.get(cmtField))) {
                            attachToMatter = false;
                            break;
                        }
                        if (mapperObj.get(cmtField) == NULL && matter.get(matterField) == NULL) {
                            attachToMatter = false;
                            break;
                        }
                        if (isUpdateTrigger && existedInfoRequestSet.contains(mapperObj.MasterLabel)){
                            attachToMatter = false;
                            break;
                        }
                    }
                    System.debug(attachToMatter);
                    if (attachToMatter) {
                        litify_pm__Request__c info = new litify_pm__Request__c();
                        info.Request__c = mapperObj.MasterLabel;                        
                        info.Name = mapperObj.MasterLabel;
                        info.litify_pm__Document__c = mapperObj.Document_Link__c;
                        info.litify_pm__Matter__c = matter.Id;
                        info.litify_pm__Date_Requested__c = null;
                        infoReqestList.add(info);
                    }
                }
            }
        }
        // Inserting litify_pm__Request__c Records 
        if (!infoReqestList.isEmpty()) {
            try {
                insert infoReqestList;
            } 
            catch (Exception e) {
                System.debug(e.getMessage());
            }
        }
    }


    public static List<litify_pm__Request__c> fetchRecords(litify_pm__Matter__c matter, Boolean checkNewInfo) {
        Map<String, String> fieldsToMap = getFieldsToMapFromCMT();
        List<litify_pm__Request__c> infoList = new List<litify_pm__Request__c>();
        if (!fieldsToMap.isEmpty()) {
            Map<String, Set<String>> fieldToMapValues = new Map<String, Set<String>>();
            Map<String, Map<String, String>> matterFieldValues = new Map<String, Map <String, String>>();

            fetchCMT(matter, fieldToMapValues, matterFieldValues, fieldsToMap);

            List<Matter_Information_Request_Setting__mdt> mappingList = getCriteriaListFromCMT(fieldToMapValues, false);

            infoList = fetchInfoRecords(mappingList, matter.id, checkNewInfo);
        }
        return infoList;
    }

    public static List<litify_pm__Request__c> fetchInfoRecords(List<Matter_Information_Request_Setting__mdt> mappingList, 
                                                                    String matterId, Boolean checkNewInfo) {
        List<litify_pm__Request__c> infoList = new List<litify_pm__Request__c>();
        List<String> requestList = new List<String>();
        for (Matter_Information_Request_Setting__mdt mapperObj: mappingList) {
            requestList.add(mapperObj.MasterLabel);
        }
        String query = 'Select id, litify_pm__Matter__c,Request__c,litify_pm__Facility__c,litify_pm__Facility_Name__c,' +
                        'litify_pm__Date_Requested__c,litify_pm__Date_Received__c,litify_pm__Comments__c,Status__c,' +
                        'litify_pm__Document__c FROM litify_pm__Request__c WHERE ' +
                        'litify_pm__Matter__c =: matterId';
        infoList = Database.query(query);
        if (checkNewInfo && !mappingList.isEmpty()) {
            infoList = checkAndCreateNewInfos(mappingList, infoList, new Set < String > (requestList), matterId);
        }
        if (infoList == null) {
            return new List<litify_pm__Request__c>();
        } else {
            return infoList;
        }
    }

    public static List<litify_pm__Request__c> checkAndCreateNewInfos(List<Matter_Information_Request_Setting__mdt> mappingList,
                                                                        List<litify_pm__Request__c> infoList,
                                                                        Set<String> requestSet, String matterId) {
        for (litify_pm__Request__c info: infoList) {
            if (requestSet.contains(info.Request__c)) {
                requestSet.remove(info.Request__c);
            }
        }
        if (!requestSet.isEmpty()) {
            List<litify_pm__Request__c> infoToInsert = new List<litify_pm__Request__c>();
            for (Matter_Information_Request_Setting__mdt mapperObj: mappingList) {
                if (requestSet.contains(mapperObj.MasterLabel)) {
                    litify_pm__Request__c info = new litify_pm__Request__c();
                    info.Request__c = mapperObj.MasterLabel;
                    info.Name = mapperObj.MasterLabel;
                    info.litify_pm__Document__c = mapperObj.Document_Link__c;
                    info.litify_pm__Matter__c = matterId;
                    info.litify_pm__Date_Requested__c = null;
                    info.Status__c = 'Not Started';
                    infoToInsert.add(info);
                }
            }
            if (!infoToInsert.isEmpty()) {
                insert infoToInsert;
                infoList.addAll(infoToInsert);
            }
        }
        return infoList;
    }
}