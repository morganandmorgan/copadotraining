@isTest
private class CreateIntakeFromTaskTriggerTest {
  static List<Task> tasks = new List<Task>();
  static Task t = new Task();
  static Account act;
  static CreateIntakeFromTaskTriggerHandler handler = new CreateIntakeFromTaskTriggerHandler();
	static {
    act = new Account(FirstName = 'Bill', LastName = 'Gates');
    insert act;

    t = new Task(
      Subject = 'Submitted Form "forthepeople Case Evaluation Form (Contact Us)"',
      Description = 'First Name: kari\nKeep me informed about important consumer alerts.: true\nPhone Number: 4073602255\nMarketing Source: 22BFADFD-8CC4-E211-A930-00155D5D2603\nCase Details: Well where do I start ...\nEmail: asdf@forthepeople.com.com\nWeb Property: ForThePeople-com-INTERNET\nLast Name: lonmo',
      WhatId = act.id
    );
    tasks.add(t);
  }
	@isTest static void Execute_TaskHasIntakeCreated_DoesNothing() {
    t.Intake_Created_From_Task__c = true;
    insert tasks;

    Test.startTest();
		handler.execute(tasks);
    Test.stopTest();

    List<Intake__c> ins = [SELECT Id FROM Intake__c Where Client__c = :act.Id];
    System.assertEquals(0, ins.size());
	}

  @isTest static void Execute_TaskHasNonHubspotSubject_DoesNothing() {
    t.Subject = 'Do Something';
    insert tasks;

    Test.startTest();
    handler.execute(tasks);
    Test.stopTest();

    List<Intake__c> ins = [SELECT Id FROM Intake__c Where Client__c = :act.Id];
    System.assertEquals(0, ins.size());    
  }
	
	@isTest static void Execute_CreatesIntake() {
    insert tasks;

    Test.startTest();
    handler.execute(tasks);
    Test.stopTest();

    List<Intake__c> ins = [SELECT Id FROM Intake__c Where Client__c = :act.Id];
    System.assertEquals(1, ins.size());
	}
	
}