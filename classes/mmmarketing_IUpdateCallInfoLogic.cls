public interface mmmarketing_IUpdateCallInfoLogic
{
    map<String, mmlib_NamedCredentialCalloutAuth> getAvailableAuthorizations();

    mmlib_BaseCallout prepareCalloutToService( Marketing_Tracking_Info__c record, string identifier );

    void updateMTICallRecordIfMatch( Marketing_Tracking_Info__c record
                                   , mmlib_BaseCallout.CalloutResponse theCallServiceResponse
                                   , String identifier
                                   , mmlib_ISObjectUnitOfWork uow );

    void updateMTICallRecordShallowReconciliationIfMatch ( Marketing_Tracking_Info__c record
                                   , mmlib_BaseCallout.CalloutResponse theCallServiceResponse
                                   , String identifier
                                   , mmlib_ISObjectUnitOfWork uow );
}