public class mmmarketing_Logic
{
    private static final string CAMPAIGN_TECHNICAL_KEY_DELIMITER = '-';

    /*
     * Hide the default constructor
     */
    private mmmarketing_Logic()
    {

    }

    private static string assembleMarketingCampaignTechnicalKey( final string domainValue, final string secondValue )
    {
        return (string.isNotBlank( domainValue ) && string.isNotBlank( secondValue ))
                        ? (domainValue.toLowerCase() + CAMPAIGN_TECHNICAL_KEY_DELIMITER + secondValue.toLowerCase()).left(255)
                        : null;
    }

    /**
     * Assemble the technical key for a Marketing_Campaign__c record using a Marketing_Campaign__c record as input
     *
     * @param campaign Marketing_Campaign__c
     * @returns string representing the Marketing_Campaign__c.Technical_Key_UTM__c
     */
    public static string assembleMarketingCampaignTechnicalKeyUTM( final Marketing_Campaign__c campaign )
    {
        return assembleMarketingCampaignTechnicalKey( campaign.Domain_Value__c, campaign.UTM_Campaign__c );
    }

    /**
     * Assemble the technical key for a Marketing_Campaign__c record using a Marketing_Campaign__c record as input
     *
     * @param campaign Marketing_Campaign__c
     * @returns string representing the Marketing_Campaign__c.Technical_Key_Source_Campaign_ID__c
     */
    public static string assembleMarketingCampaignTechnicalKeySourceCampaignId( final Marketing_Campaign__c campaign )
    {
        return assembleMarketingCampaignTechnicalKey( campaign.Domain_Value__c, campaign.Source_Campaign_ID__c );
    }

    /**
     * Assemble the technical keys for a Marketing_Campaign__c record using a Marketing_Campaign__c record as input
     *
     * @param trackingInfo Marketing_Tracking_Info__c
     * @returns set<string> representing the Marketing_Campaign__c's technical keys
     */
    public static set<string> assembleMarketingCampaignTechnicalKeys( final Marketing_Campaign__c campaign )
    {
        set<String> workingStringSet = new set<String>();

        workingStringSet.add( assembleMarketingCampaignTechnicalKeyUTM( campaign ) );
        workingStringSet.add( assembleMarketingCampaignTechnicalKeySourceCampaignId( campaign ) );

        //remove any nulls that may have been added
        workingStringSet.remove( null );

        return workingStringSet;
    }

    /**
     * Assemble the technical keys for a Marketing_Campaign__c record using a Marketing_Tracking_Info__c record as input
     *
     * @param trackingInfo Marketing_Tracking_Info__c
     * @returns set<string> representing the Marketing_Campaign__c's technical keys
     */
    public static set<string> assembleMarketingCampaignTechnicalKeys( final Marketing_Tracking_Info__c trackingInfo )
    {
        set<String> workingStringSet = new set<String>();

        workingStringSet.add( assembleMarketingCampaignTechnicalKey( trackingInfo.Domain_Value__c, trackingInfo.Campaign_Value__c ) );

        //remove any nulls that may have been added
        workingStringSet.remove( null );

        return workingStringSet;
    }

    /**
     * Assemble the technical keys for a Marketing_Campaign__c record using a Marketing_Financial_Period__c record as input
     *
     * @param financialPeriod Marketing_Financial_Period__c
     * @returns set<string> representing the Marketing_Campaign__c's technical keys
     */
    public static set<string> assembleMarketingCampaignTechnicalKeys( final Marketing_Financial_Period__c financialPeriod )
    {
        set<String> workingStringSet = new set<String>();

        workingStringSet.add( assembleMarketingCampaignTechnicalKey( financialPeriod.Domain_Value__c, financialPeriod.Campaign_Value__c ) );
        workingStringSet.add( assembleMarketingCampaignTechnicalKey( financialPeriod.Domain_Value__c, financialPeriod.Campaign_AdWords_ID_Value__c ) );

        //remove any nulls that may have been added
        workingStringSet.remove( null );

        return workingStringSet;
    }
}