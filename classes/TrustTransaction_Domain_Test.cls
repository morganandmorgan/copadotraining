@isTest
private class TrustTransaction_Domain_Test {
	
	public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;

	static void setupData()
    {
    	/*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaBankAccount__c operatingBankAccount = TestDataFactory_FFA.bankAccounts[0];
        operatingBankAccount.Bank_Account_Type__c = 'Operating';
        update operatingBankAccount;
       
		/*--------------------------------------------------------------------
        LITIFY Data Setup
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = TestDataFactory_FFA.company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        

        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        testDeposit = new Deposit__c();
        testDeposit.RecordTypeId = Schema.SObjectType.Deposit__c.RecordTypeInfosByName.get('Operating').RecordTypeId;
        testDeposit.Matter__c = matter.Id;
        testDeposit.Amount__c = 10.0;
        testDeposit.Check_Date__c = date.today();
        testDeposit.Source__c = 'Client';
        testDeposit.Operating_Cash_Account__c = operatingBankAccount.Id;
        INSERT testDeposit;
    }

    //================= TEST METHODS ====================
	@isTest static void test_Constructor() {
		TrustTransaction_Domain tt = new TrustTransaction_Domain();
	} 

	@isTest static void test_TrustTransactionInsertUpdate() {
		setupData();

		Trust_Transaction__c tt = new Trust_Transaction__c(
			Amount__c = 100,
			Matter__c = matter.Id
		);
		insert tt;
		update tt;
	}    
}