public abstract class mmwiz_AbstractBaseModel
{
    public static final string CHILD_TOKENS_KEY = 'childTokens';
    public static final string ACTION_TOKENS_KEY = 'actionTokens';
    public static final string BUSINESS_RULE_TOKENS_KEY = 'businessRuleTokens';

    private String description = '';

    public String getDescription()
    {
        return description;
    }

    public String setDescription(String value)
    {
        description = value;
        return value;
    }

    private String displayText = '';

    public String getDisplayText()
    {
        return displayText;
    }

    public String setDisplayText(String value)
    {
        displayText = value;
        return value;
    }

    private Boolean isActive = true;

    public Boolean getIsActive()
    {
        return isActive;
    }

    public Boolean setIsActive(Boolean value)
    {
        isActive = value;
        return isActive;
    }

    private Boolean isDeleted = false;

    public Boolean getIsDeleted()
    {
        return isDeleted;
    }

    public Boolean setIsDeleted(Boolean value)
    {
        isDeleted = value;
        return isDeleted;
    }

    private Integer sortOrder = 0;

    public Integer getSortOrder()
    {
        return sortOrder;
    }

    public Integer setSortOrder(Integer value)
    {
        sortOrder = value;
        return sortOrder;
    }

    private String type = '';

    public String getType()
    {
        return this.type;
    }

    protected void setType( system.Type value )
    {
        this.type = value.getName();
    }

    private string token = '';

    public void setToken( string token )
    {
        this.token = token;
    }

    public string getToken()
    {
        return this.token;
    }

    public virtual void prepareForSerialization()
    {
        this.childTokens = extractModelNames(this.childModels);
        this.actionTokens = extractModelNames(this.actionModels);
    }

    private List<String> extractModelNames(List<mmwiz_AbstractBaseModel> modelList)
    {
        List<String> result = new List<String>();

        if (modelList != null && ! modelList.isEmpty())
        {
            Set<String> duplicateTokenPreventionSet = new Set<String>();

            for (mmwiz_AbstractBaseModel bm : modelList)
            {
                if ( bm != null && !duplicateTokenPreventionSet.contains( bm.getToken() ) )
                {
                    duplicateTokenPreventionSet.add( bm.getToken() );
                    result.add(bm.getToken());
                }
            }
        }

        return result;
    }

    public transient map<string, Object> serializedDataMap { get; set; } { serializedDataMap = new map<string, Object>(); }

    public list<mmwiz_AbstractBaseModel> childModels { get; set; } { childModels = new list<mmwiz_AbstractBaseModel>(); }
    public list<string> childTokens = new list<String>();

    public mmwiz_AbstractBaseModel addChildModel( mmwiz_AbstractBaseModel childModel)
    {
        childModel.setSortOrder( this.childModels.size() );
        this.childModels.add( childModel );

        return this;
    }

    public list<mmwiz_AbstractBaseModel> actionModels { get; set; } { actionModels = new list<mmwiz_AbstractBaseModel>(); }
    public list<String> actionTokens = new list<string>();

    public mmwiz_AbstractBaseModel addActionModel( mmwiz_AbstractBaseModel actionModel)
    {
        actionModel.setSortOrder( this.actionModels.size() );
        this.actionModels.add( actionModel );

        return this;
    }

    private list<String> businessRuleTokens = new list<String>();

    public list<String> getBusinessRuleTokens()
    {
        return this.businessRuleTokens;
    }

    public void addBusinessRuleToken( String businessRuleToken )
    {
        if ( String.isNotBlank( businessRuleToken ) )
        {
            this.businessRuleTokens.add( businessRuleToken );
        }
    }

    public virtual void initialize(Wizard__mdt wizard)
    {
        if (wizard == null)
        {
            throw new mmwiz_Exceptions.ParameterException('Parameter \'wizard\' must consist of an instanced object.');
        }

        try
        {
            this.setType( System.Type.forName( wizard.Type__c ) );
        }
        catch (Exception e)
        {
            throw new mmwiz_Exceptions.ParameterException('Parameter \'wizard\' type value of \'' + wizard.Type__c + '\' is not a valid model type.');
        }

        this.setToken( wizard.DeveloperName );
        this.setDescription( wizard.Description__c );
        this.setDisplayText( wizard.DisplayText__c );
        this.setIsActive( wizard.IsActive__c );
        this.setIsDeleted( wizard.IsDeleted__c );

        if ( string.isNotBlank( wizard.SerializedData__c ))
        {
            try
            {
                this.serializedDataMap.putAll( (Map<String, Object>) json.deserializeUntyped( wizard.SerializedData__c ) );

            }
            catch (Exception e)
            {
                system.debug('unable to deserialize Wizard__mdt.SerializedData__c field for record "' + wizard.DeveloperName + '".');
            }
        }

        if ( this.serializedDataMap.containsKey( CHILD_TOKENS_KEY ) )
        {
            for ( Object childTokenValueAsObject : (list<Object>)this.serializedDataMap.get( CHILD_TOKENS_KEY ) )
            {
                this.childTokens.add( (String)childTokenValueAsObject );
            }
        }

        if ( this.serializedDataMap.containsKey( ACTION_TOKENS_KEY ))
        {
            for ( Object actionTokenValueAsObject : (list<Object>)this.serializedDataMap.get( ACTION_TOKENS_KEY ) )
            {
                this.actionTokens.add( (String)actionTokenValueAsObject );
            }
        }

        if ( this.serializedDataMap.containsKey( BUSINESS_RULE_TOKENS_KEY ))
        {
            for ( Object businessRuleTokenValueAsObject : (list<Object>)this.serializedDataMap.get( BUSINESS_RULE_TOKENS_KEY ) )
            {
                this.businessRuleTokens.add( (String)businessRuleTokenValueAsObject );
            }
        }
    }

    public list<mmwiz_AbstractBaseModel> findAllNodesByType( Type modelTypeSought )
    {
        // system.debug( 'findAllNodesByType start for token ' + getToken() );
        list<mmwiz_AbstractBaseModel> modelNodesFoundList = new list<mmwiz_AbstractBaseModel>();

        list<mmwiz_AbstractBaseModel> childNodesList = new list<mmwiz_AbstractBaseModel>();

        childNodesList.addAll( this.childModels );
        childNodesList.addAll( (list<mmwiz_AbstractBaseModel>)this.actionModels );

        for ( mmwiz_AbstractBaseModel childNode :  childNodesList )
        {
            //system.debug( childNode.getType() );
            //system.debug( modelTypeSought );
            //system.debug( 'findAllNodesByType modelTypeSought.equals( childNode ) ==  ' + modelTypeSought.equals( childNode.getType() ));
            //system.debug( 'findAllNodesByType modelTypeSought == childNode  ==  ' + ( modelTypeSought.getName() == childNode.getType() ));

            if ( modelTypeSought.getName() == childNode.getType()
                || modelTypeSought == mmwiz_AbstractBaseModel.class) // if the type supplied is mmwiz_AbstractBaseModel, then just add everything.
            {
                modelNodesFoundList.add( childNode );
            }

            modelNodesFoundList.addAll( childNode.findAllNodesByType( modelTypeSought ) );
        }

        return modelNodesFoundList;
    }
}