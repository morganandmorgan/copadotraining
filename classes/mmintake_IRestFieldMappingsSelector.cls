public interface mmintake_IRestFieldMappingsSelector extends mmlib_ISObjectSelector
{
    List<IntakeRestFieldMapping__mdt> selectAll();
}