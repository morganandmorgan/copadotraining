public class CommentController {

    public Comments__c comment {get;set;}
    public List<Comments__c> commentList {get;set;}
    public String retUrl{get;set;}
    public String intakeId{get;set;}
    public String quesId;
    
    public CommentController() {
    
        comment=new Comments__c();
        intakeId=ApexPages.currentPage().getParameters().get('intakeId');
        quesId=ApexPages.currentPage().getParameters().get('quesId');
        if(quesId != NULL && quesId !='') {
        
            retUrl=URL.getSalesforceBaseUrl().toExternalForm()+'/'+quesId;
            for(Questionnaire__c q:[SELECT Id,Intake__c FROM Questionnaire__c WHERE Id = :quesId AND Intake__c != NULL LIMIT 1]) {
                
                intakeId=q.Intake__c;
            }
        } else if(intakeId != NULL && intakeId !='')  {
        
            retUrl=URL.getSalesforceBaseUrl().toExternalForm()+'/'+intakeId;
        }
        if(intakeId != NULL && intakeId != '') {
            
            commentList=[SELECT Id,Comments__c,CreatedDate,CreatedBy.Name FROM Comments__c WHERE Intake__c = :intakeId ORDER BY CreatedDate DESC]; 
        }
    }
    
    public  PageReference saveComment() {
        
        PageReference pg=new PageReference(retUrl);
        comment.Intake__c=intakeId;
        if(quesId != NULL && quesId != '') {
            
            comment.Questionnaire__c=quesId;    
        }
        insert comment;
        return pg;
            
    }
    public PageReference cancelComment() {
            
        PageReference pg=new PageReference(retURL);
        return pg;
    }
}