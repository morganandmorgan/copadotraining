public with sharing class IntakeRepNavigatorWidgetController {
  public List<SelectOption> QueueList { get; set; }
  public String Message { get; set; }
  public String SelectedQueueId { get; set; }
  public String NextId { get; set; }
  public Boolean IsAdmin { get; set; }
  private static string CookieName = 'IntakeRepNavigatorWidget_SelectedQueueId';

  public PageReference OnLoad() {
    Schema.DescribeSObjectResult objDescription = IntakeRepNavigatorWidgetQueue__c.sObjectType.getDescribe();
    Map<String, Boolean> fieldMap = new Map<String, Boolean>();
    for (Schema.SobjectField f : objDescription.fields.getMap().Values()) {
      Schema.DescribeFieldResult dfr = f.getDescribe();
      fieldMap.put(dfr.getName().toLowerCase(), dfr.isUpdateable());
    }

    isAdmin = fieldMap.get('query__c') && fieldMap.get('sorting__c') && fieldMap.get('name') && fieldMap.get('earliestcalltime__c') && fieldMap.get('latestcalltime__c');

    List<IntakeRepNavigatorWidgetQueue__c> queues = [SELECT Id, Name FROM IntakeRepNavigatorWidgetQueue__c];

    QueueList = new List<SelectOption>();
    for (IntakeRepNavigatorWidgetQueue__c q : queues) {
      QueueList.add(new SelectOption(q.Id, q.Name));
    }

    // Set SelectedQueueId
    Cookie cookie = ApexPages.currentPage().getCookies().get(CookieName);
    if (cookie != null) {
        SelectedQueueId = cookie.getValue();      
    }

    return null;
  }

  public PageReference goToNext() {
    Message = '';

    // write cookie
    Cookie cook = new Cookie(CookieName, SelectedQueueId, null, -1, false);
    ApexPages.currentPage().setCookies(new Cookie[]{cook});

    List<Intake__c> intakes = QueryIntakes(SelectedQueueId);

    System.debug('intakes found: ' + intakes);
    if (intakes == null || intakes.size() == 0) {
      Message = 'This list is complete!';
      return null;
    }

    Message = 'Please wait...';
    NextId = intakes[0].Client__c;

    // Add id to access log
    IntakeRepNavigatorWidgetAccessLog__c logEntry = new IntakeRepNavigatorWidgetAccessLog__c(ItemId__c = NextId, NavigatorId__c = SelectedQueueId);
    INSERT logEntry;

    return null;
  }

  private List<Intake__c> QueryIntakes(Id queueId) {
    IntakeRepNavigatorWidgetQueue__c queue  = [SELECT Query__c, Sorting__c, EarliestCallTime__c, LatestCallTime__c FROM IntakeRepNavigatorWidgetQueue__c WHERE ID = :SelectedQueueId];

    String query = 'SELECT Id, Client__c FROM Intake__c';
    query = AppendQuery(query, queue.Query__c);
    query = AppendTimezoneSelectionToQuery(query, queue);

    List<String> idList = GetRecentlyAccessedIds(queueId);

    if (query.contains(' WHERE ')) {
      query += ' AND ';
    } else {
      query += ' WHERE ';
    }
    query += 'Client__c NOT IN :idList';

    query = AppendSorting(query, queue.Sorting__c);

    query += ' LIMIT 1';

    //Message = query;
    //return null;
    System.debug('idList: ' + idList);
    System.debug('Query: ' + query);

    List<SObject> intakes = Database.query(query);
    return intakes;
  }

  private Map<String, Schema.SoapType> intakeFieldMapBackingStore;
  private Map<String, Schema.SoapType> IntakeFieldMap {
    get {
      if (intakeFieldMapBackingStore == null) {
        Schema.DescribeSObjectResult intakeDescription = Intake__c.sObjectType.getDescribe();
        intakeFieldMapBackingStore = new Map<String, Schema.SoapType>();
        for (Schema.SobjectField f : intakeDescription.fields.getMap().Values()) {
          Schema.DescribeFieldResult dfr = f.getDescribe();
          intakeFieldMapBackingStore.put(dfr.getName().toLowerCase(), dfr.getSoapType());
        }
      }
      return IntakeFieldMapBackingStore;
    }
  }
  private Map<String, Schema.SoapType> accountFieldMapBackingStore;
  private Map<String, Schema.SoapType> AccountFieldMap {
    get {
      if (accountFieldMapBackingStore == null) {
        Schema.DescribeSObjectResult accountDescription = Account.sObjectType.getDescribe();
        accountFieldMapBackingStore = new Map<String, Schema.SoapType>();
        for (Schema.SobjectField f : accountDescription.fields.getMap().Values()) {
          Schema.DescribeFieldResult dfr = f.getDescribe();
          accountFieldMapBackingStore.put(dfr.getName().toLowerCase(), dfr.getSoapType());
        }
      }
      return accountFieldMapBackingStore;
    }
  }

  private Schema.SoapType getSoapTypeForColumn(String column) {
    if (column.StartsWith('Client__r.')) {
      String field = column.SubstringAfter('Client__r.');
      return AccountFieldMap.get(field.toLowerCase());
    } else {
      return IntakeFieldMap.get(column.toLowerCase());
    }
  }
  private String AppendQuery(String base, String queryJson) {
    if (queryJson.length() > 0) {
      List<IntakeRepNavigator_Json.Query> qry = IntakeRepNavigator_Json.ParseQuery(queryJson);

      for (IntakeRepNavigator_Json.Query qi : qry) {

        if (!base.contains(' WHERE ')) {
          base += ' WHERE ';
        } else {
          base += ' AND ';
        }

        Schema.SoapType st = getSoapTypeForColumn(qi.column);

        String[] arguments = qi.argument.split('\n');

        String singleCrit = '(';
        for (String arg : arguments) {
          if (singleCrit.length() > 1) {
            singleCrit += ' OR ';
          }

          if (st == Schema.SoapType.String) {
            singleCrit += qi.column + ' ' + qi.operator + ' \'' + qi.argument + '\'';  
          } else {
            singleCrit += qi.column + ' ' + qi.operator + ' ' + qi.argument;
          }
        }
        singleCrit += ') ';
        base += singleCrit;
      }
    }

    return base;
  }

  private String AppendTimezoneSelectionToQuery(String base, IntakeRepNavigatorWidgetQueue__c queue) {
    Map<String, String> areaCodes = new Map<String, String>{ '201' => 'EST', '202' => 'EST', '203' => 'EST', '205' => 'CST', '206' => 'PST', '207' => 'EST', '208' => 'MST', '209' => 'PST', '210' => 'CST', '212' => 'EST', '213' => 'PST', '214' => 'CST', '215' => 'EST', '216' => 'EST', '217' => 'CST', '218' => 'CST', '219' => 'CST', '224' => 'CST', '225' => 'CST', '228' => 'CST', '229' => 'EST', '231' => 'EST', '234' => 'EST', '239' => 'EST', '240' => 'EST', '248' => 'EST', '251' => 'CST', '252' => 'EST', '253' => 'PST', '254' => 'CST', '256' => 'CST', '260' => 'EST', '262' => 'CST', '267' => 'EST', '269' => 'EST', '270' => 'CST', '272' => 'EST', '276' => 'EST', '281' => 'CST', '301' => 'EST', '302' => 'EST', '303' => 'MST', '304' => 'EST', '305' => 'EST', '307' => 'MST', '308' => 'CST', '309' => 'CST', '310' => 'PST', '312' => 'CST', '313' => 'EST', '314' => 'CST', '315' => 'EST', '316' => 'CST', '317' => 'EST', '318' => 'CST', '319' => 'CST', '320' => 'CST', '321' => 'EST', '323' => 'PST', '325' => 'CST', '330' => 'EST', '331' => 'CST', '334' => 'CST', '336' => 'EST', '337' => 'CST', '339' => 'EST', '346' => 'CST', '347' => 'EST', '351' => 'EST', '352' => 'EST', '360' => 'PST', '361' => 'CST', '364' => 'CST', '385' => 'MST', '386' => 'EST', '401' => 'EST', '402' => 'CST', '404' => 'EST', '405' => 'CST', '406' => 'MST', '407' => 'EST', '408' => 'PST', '409' => 'CST', '410' => 'EST', '412' => 'EST', '413' => 'EST', '414' => 'CST', '415' => 'PST', '417' => 'CST', '419' => 'EST', '423' => 'EST', '424' => 'PST', '425' => 'PST', '430' => 'CST', '432' => 'CST', '434' => 'EST', '435' => 'MST', '440' => 'EST', '442' => 'PST', '443' => 'EST', '458' => 'PST', '469' => 'CST', '470' => 'EST', '475' => 'EST', '478' => 'EST', '479' => 'CST', '480' => 'MST', '484' => 'EST', '501' => 'CST', '502' => 'EST', '503' => 'PST', '504' => 'CST', '505' => 'MST', '507' => 'CST', '508' => 'EST', '509' => 'PST', '510' => 'PST', '512' => 'CST', '513' => 'EST', '515' => 'CST', '516' => 'EST', '517' => 'EST', '518' => 'EST', '520' => 'MST', '530' => 'PST', '531' => 'CST', '534' => 'CST', '539' => 'CST', '540' => 'EST', '541' => 'PST', '551' => 'EST', '559' => 'PST', '561' => 'EST', '562' => 'PST', '563' => 'CST', '567' => 'EST', '570' => 'EST', '571' => 'EST', '573' => 'CST', '574' => 'EST', '575' => 'MST', '580' => 'CST', '585' => 'EST', '586' => 'EST', '601' => 'CST', '602' => 'MST', '603' => 'EST', '605' => 'CST', '606' => 'EST', '607' => 'EST', '608' => 'CST', '609' => 'EST', '610' => 'EST', '612' => 'CST', '614' => 'EST', '615' => 'CST', '616' => 'EST', '617' => 'EST', '618' => 'CST', '619' => 'PST', '620' => 'CST', '623' => 'MST', '626' => 'PST', '630' => 'CST', '631' => 'EST', '636' => 'CST', '641' => 'CST', '646' => 'EST', '650' => 'PST', '651' => 'CST', '657' => 'PST', '660' => 'CST', '661' => 'PST', '662' => 'CST', '667' => 'EST', '669' => 'PST', '678' => 'EST', '681' => 'EST', '682' => 'CST', '701' => 'CST', '702' => 'PST', '703' => 'EST', '704' => 'EST', '706' => 'EST', '707' => 'PST', '708' => 'CST', '712' => 'CST', '713' => 'CST', '714' => 'PST', '715' => 'CST', '716' => 'EST', '717' => 'EST', '718' => 'EST', '719' => 'MST', '720' => 'MST', '724' => 'EST', '725' => 'PST', '727' => 'EST', '731' => 'CST', '732' => 'EST', '734' => 'EST', '737' => 'CST', '740' => 'EST', '747' => 'PST', '754' => 'EST', '757' => 'EST', '760' => 'PST', '762' => 'EST', '763' => 'CST', '765' => 'EST', '769' => 'CST', '770' => 'EST', '772' => 'EST', '773' => 'CST', '774' => 'EST', '775' => 'PST', '779' => 'CST', '781' => 'EST', '785' => 'CST', '786' => 'EST', '801' => 'MST', '802' => 'EST', '803' => 'EST', '804' => 'EST', '805' => 'PST', '806' => 'CST', '808' => 'US\\Hawaii', '809' => 'AST', '810' => 'EST', '812' => 'EST', '813' => 'EST', '814' => 'EST', '815' => 'CST', '816' => 'CST', '817' => 'CST', '818' => 'PST', '828' => 'EST', '830' => 'CST', '831' => 'PST', '832' => 'CST', '843' => 'EST', '845' => 'EST', '847' => 'CST', '848' => 'EST', '850' => 'CST', '856' => 'EST', '857' => 'EST', '858' => 'PST', '859' => 'EST', '860' => 'EST', '862' => 'EST', '863' => 'EST', '864' => 'EST', '865' => 'EST', '870' => 'CST', '872' => 'CST', '878' => 'EST', '901' => 'CST', '903' => 'CST', '904' => 'EST', '906' => 'EST', '907' => 'US\\Alaska', '908' => 'EST', '909' => 'PST', '910' => 'EST', '912' => 'EST', '913' => 'CST', '914' => 'EST', '915' => 'MST', '916' => 'PST', '917' => 'EST', '918' => 'CST', '919' => 'EST', '920' => 'CST', '925' => 'PST', '928' => 'MST', '929' => 'EST', '931' => 'CST', '936' => 'CST', '937' => 'EST', '938' => 'CST', '940' => 'CST', '941' => 'EST', '947' => 'EST', '949' => 'PST', '951' => 'PST', '952' => 'CST', '954' => 'EST', '956' => 'CST', '959' => 'EST', '970' => 'MST', '971' => 'PST', '972' => 'CST', '973' => 'EST', '978' => 'EST', '979' => 'CST', '980' => 'EST', '984' => 'EST', '985' => 'CST', '989' => 'EST' };

    DateTime moment = DateTime.now();
    Set<String> timezoneStrings = new Set<String>(areaCodes.values());
    for (String tzs : timezoneStrings) {
      TimeZone tz = TimeZone.getTimeZone(tzs);
      Integer hour = Integer.valueOf(moment.format('H', tzs));
      if (hour < queue.EarliestCallTime__c || hour > queue.LatestCallTime__c) {
        // restrict query from all area codes in time zone 
        for (String ac : areaCodes.keySet()) {
          if (areaCodes.get(ac) == tzs) {
            if (!base.contains(' WHERE ')) {
              base += ' WHERE ';
            } else {
              base += ' AND ';
            }
            base += ' (NOT Phone__c LIKE \'' + ac + '%\') ';
          }
        }
      }
    }
    return base;
  }
  private String AppendSorting(String base, String sortingJson) {
    //Message = base;
    if (sortingJson.length() > 0) {
      List<IntakeRepNavigator_Json.SortOrder> sortOrders = IntakeRepNavigator_Json.ParseSortOrder(sortingJson);
      for (IntakeRepNavigator_Json.SortOrder si : sortOrders) {
        if (!base.contains(' ORDER BY ')) {
          base += ' ORDER BY ';
        } else {
          base += ', ';
        }
        base += si.column + ' ' + si.order;
      }
    }
    return base;
  }

  private List<String> GetRecentlyAccessedIds(ID queueId) {
    // get recently accessed objects
    DateTime lastHour = DateTime.now().addHours(-1);
    List<IntakeRepNavigatorWidgetAccessLog__c> logs = [SELECT ItemId__c FROM IntakeRepNavigatorWidgetAccessLog__c WHERE NavigatorId__c = :queueId AND AccessedDate__c >= :lastHour];
    List<String> idList = new List<String>(logs.size());
    for (IntakeRepNavigatorWidgetAccessLog__c l : logs) {
      idList.add(l.ItemId__c);
    }
    return idList;
  }
}