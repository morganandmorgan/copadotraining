public interface mmdocusign_IDocusignRestApiService
{
	mmdocusign_CreateEnvelopeResponseData createEnvelope(Intake__c record);
	Task createTasksForEnvelope(Intake__c intake, String envelopeId, String clientUserId);
	String getRecipientLinkToEnvelope(String envelopeId, String clientEmail, String clientName, String clientUserId);
}