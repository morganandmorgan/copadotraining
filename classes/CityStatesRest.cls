@RestResource(urlMapping='/CityStates')
global with sharing class CityStatesRest  {

    @HttpGet
    global static String all() {
        List<Address_by_Zip_Code__c> obj = [SELECT City__c, State__c, State_Code__c FROM Address_by_Zip_Code__c];

        List<String> jsObjects = new List<String>();
        for (Address_by_Zip_Code__c cs : obj) {
            jsObjects.add('{city: "' + cs.City__c + '", state: "' + cs.State__c + '", state_code:"' + cs.State_Code__c + '"}');
        }
        return '[' + String.join(jsObjects, ',') + ']';
    }

}