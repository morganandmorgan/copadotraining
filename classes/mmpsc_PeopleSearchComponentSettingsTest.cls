@isTest
private class mmpsc_PeopleSearchComponentSettingsTest
{
    @isTest
	static void accountFieldsetTest()
	{
        setup();
        System.assertEquals(testSettings.AccountFieldset__c, mmpsc_PeopleSearchComponentSettings.getAccountFieldset());
	}

    @isTest
	static void intakeFieldsetTest()
	{
        setup();
        System.assertEquals(testSettings.IntakeFieldset__c, mmpsc_PeopleSearchComponentSettings.getIntakeFieldset());
	}

    @isTest
	static void lawsuitFieldsetTest()
	{
        setup();
        System.assertEquals(testSettings.LawsuitFieldset__c, mmpsc_PeopleSearchComponentSettings.getLawsuitFieldset());
	}

    @isTest
	static void matterFieldsetTest()
	{
        setup();
        System.assertEquals(testSettings.MatterFieldset__c, mmpsc_PeopleSearchComponentSettings.getMatterFieldset());
	}

    private static mmpsc_CustomSettings__c testSettings = null;

    private static void setup()
    {
        testSettings = new mmpsc_CustomSettings__c();
        testSettings.AccountFieldset__c = 'one';
        testSettings.IntakeFieldset__c = 'two';
        testSettings.LawsuitFieldset__c = 'three';
        testSettings.MatterFieldset__c = 'four';
        insert testSettings;
    }
}