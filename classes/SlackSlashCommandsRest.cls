/**
 * SlackSlashCommandsRest
 * @description REST services published for Slack slash commands
 * @author Matt Terrill
 * @date 8/20/2019
 */
@RestResource(urlMapping='/slackSlashCommands/*')
global with sharing class SlackSlashCommandsRest {

    @HttpPost
    global static void salesforceLimits() {

        String slackToken = 'ulp3onrYByb0CSa6QqTxzP9s'; //found in Slack slash command setup

        RestRequest req = RestContext.request;

        //get the token
        String paramToken = req.params.get('token');

        //verify it
        if (slackToken != paramToken) {
            //no? lets get out of here
            return;//
        }

        //sendEmail(emailBody);

        GovernorLimitsApi gla = new GovernorLimitsApi();

        String limits = gla.formatForSlack(false);

        Map<String,String> payload = new Map<String,String>();
        payload.put('text',limits);
        payload.put('response_type','in_channel'); //this will let everyone in the channel see the response

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(payload));

    } //salesforceLimits


    /* for debugging
    global static void sendEmail(String body) {

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses( new String[] {'mterrill@forthepeople.com'});

        mail.setSubject('SlacSlashCommandsRest.cls');

        mail.setPlainTextBody(body);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    } //sendEmail */

} //class