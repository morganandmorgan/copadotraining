/*=======================================================================================
Name            : ffaManualExpenseAllocationController
Author          : CLD
Created Date    : Aug 27,2018
Description     : Manual Expense Allocation page controller
Test Class      : ffaManualExpenseAllocationControllerTest
Revisions       :
=======================================================================================*/
public with sharing class ffaManualExpenseAllocationController 
{
    private String depositId;
    public List<ExpenseHolder> expenseHolderList{get;set;}
    public Deposit__c deposit{get;set;}
    public Decimal depositAmount {get;set;}
    public Boolean selectAllCheckbox{get;set;}

	public ffaManualExpenseAllocationController() 
    {
	   depositId = ApexPages.currentPage().getParameters().get('depositId');
       loadData();
	}

    public void loadData()
    {

        deposit = [SELECT Id,
            Name, 
            Amount__c,
            Deposit_Description__c,
            Matter__c,
            Check_Date__c,
            Deposit_Allocated__c,
            Unallocated_Costs__c,
            Allocated_Soft_Cost__c,
            Allocated_Hard_Cost__c,
            Allocated_Fee__c,
            Source__c
            FROM Deposit__c
            WHERE Id = :depositId];

        depositAmount = deposit.Unallocated_Costs__c != null ? deposit.Amount__c - deposit.Unallocated_Costs__c : 0;

        if (deposit.Deposit_Allocated__c == false){
            deposit.Allocated_Fee__c = deposit.Amount__c;
        }

        List<litify_pm__Expense__c> expenseList = new List<litify_pm__Expense__c>();

        if(deposit.Source__c == 'Client' || deposit.Source__c == 'Operating'){
            expenseList = [Select Id,
            Name,
            litify_pm__Note__c,
            litify_pm__ExpenseType2__r.Name,
            CostType__c,
            litify_pm__Date__c,
            litify_pm__Amount__c,
            Fully_Recovered__c,
            Amount_Recovered_Client__c,
            Amount_Recovered_Settlement__c,
            Amount_Remaining_Client__c,
            PayableTo__c,
            (SELECT Id,
                Deposit__c,
                Deposit__r.Name,
                Deposit__r.Amount__c,
                Deposit__r.Deposit_Description__c,
                Deposit__r.Check_Date__c,
                Expense__c,
                Amount_Allocated__c
            FROM Expense_to_Deposits__r WHERE Deposit__c = :depositId)
            FROM litify_pm__Expense__c
            WHERE litify_pm__Matter__c = :deposit.Matter__c
            AND Amount_Remaining_Client__c != 0];
        }
        if(deposit.Source__c == 'SSA' || deposit.Source__c == 'Settlement'){
            expenseList = [Select Id,
            Name,
            litify_pm__Note__c,
            litify_pm__ExpenseType2__r.Name,
            CostType__c,
            litify_pm__Date__c,
            litify_pm__Amount__c,
            Fully_Recovered__c,
            Amount_Recovered_Client__c,
            Amount_Recovered_Settlement__c,
            Amount_Remaining_Settlement__c,
            PayableTo__c,
            (SELECT Id,
                Deposit__c,
                Deposit__r.Name,
                Deposit__r.Amount__c,
                Deposit__r.Deposit_Description__c,
                Deposit__r.Check_Date__c,
                Expense__c,
                Amount_Allocated__c
            FROM Expense_to_Deposits__r WHERE Deposit__c = :depositId)
            FROM litify_pm__Expense__c
            WHERE litify_pm__Matter__c = :deposit.Matter__c
            AND Amount_Remaining_Settlement__c != 0];
        }
            

        expenseHolderList = new List<ExpenseHolder>();
        FOR (litify_pm__Expense__c expense : expenseList)
        {
            expenseHolderList.add(new ExpenseHolder(expense, deposit.Id));
        }
    }

    public PageReference selectAll()
    {
        FOR (ExpenseHolder holder : expenseHolderList)
        {
            holder.isSelected = selectAllCheckbox;
        }
        return null;
    }
    public PageReference save()
    {
        List<Expense_To_Deposit__c> toSaveETDs = new List<Expense_to_Deposit__c>();
        List<litify_pm__Expense__c> toSaveExpenses = new List<litify_pm__Expense__c>();

        Boolean isError = false;
        Boolean isSourceClient = true;

        if (deposit.Source__c == 'SSA')
            isSourceClient = false;

        Decimal softCosts = 0;
        Decimal hardCosts = 0;

        FOR (ExpenseHolder holder : expenseHolderList)
        {
            litify_pm__Expense__c expense = holder.expense;
            Expense_to_Deposit__c expToDeposit = holder.expToDeposit;
            if (holder.isSelected)
            {

                //If the expense has not been fully allocated, then create and expense / deposit and update hard / soft costs
                if(expense.Fully_Recovered__c == false){
                    if (expense.litify_pm__Amount__c < expToDeposit.Amount_Allocated__c)
                    {
                        holder.expToDeposit.Amount_Allocated__c.addError('Amount allocated is over the expense amount!');
                        isError = true;
                    }
                    toSaveETDs.add(holder.expToDeposit);

                    if (expense.CostType__c == 'HardCost')
                        hardCosts += expToDeposit.Amount_Allocated__c;
                    else if (expense.CostType__c == 'SoftCost')
                        softCosts += expToDeposit.Amount_Allocated__c;
                }

                if (isSourceClient)
                    expense.Amount_Recovered_Client__c = expToDeposit.Amount_Allocated__c;
                else
                    expense.Amount_Recovered_Settlement__c = expToDeposit.Amount_Allocated__c;

                if (expense.litify_pm__Amount__c == expToDeposit.Amount_Allocated__c)
                {
                    expense.Fully_Recovered__c = true;
                }
                toSaveExpenses.add(expense);


                    
               
            }
            else if (holder.expToDeposit.Id != null)
            {
                if (expense.CostType__c == 'HardCost')
                    hardCosts += expToDeposit.Amount_Allocated__c;
                else if (expense.CostType__c == 'SoftCost')
                    softCosts += expToDeposit.Amount_Allocated__c;
            }
        }

        if (isError)
            return null;

        deposit.Deposit_Allocated__c = true;
        deposit.Allocated_Hard_Cost__c = hardCosts;
        deposit.Allocated_Soft_Cost__c = softCosts;
        deposit.Allocated_Fee__c = deposit.Amount__c - hardCosts - softCosts;

        /*if (toSaveETDs.size() > 0)
        {*/
            INSERT toSaveETDs;
            UPDATE toSaveExpenses;
            UPDATE deposit;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 
                'Data was saved successfully.'));
            loadData();
        //o
        return null;
    }

    public PageReference back()
    {   
        return new PageReference('/' + depositId);
    }

    public class ExpenseHolder
    {
        public Boolean isSelected{get;set;}
        public litify_pm__Expense__c expense{get;set;}
        public Expense_to_Deposit__c expToDeposit{get;set;}

        public ExpenseHolder(litify_pm__Expense__c expense, String depositId)
        {
            this.expense = expense;
            if (expense.Expense_to_Deposits__r != null && 
                expense.Expense_to_Deposits__r.size() == 1)
            {
                expToDeposit = expense.Expense_to_Deposits__r.get(0);
            }
            else
            {
                expToDeposit = new Expense_to_Deposit__c();
                expToDeposit.Deposit__c = depositId;
                expToDeposit.Expense__c = expense.Id;
                expToDeposit.Amount_Allocated__c = expense.litify_pm__Amount__c;
            }

            isSelected = false;
        }
        public Decimal amountAllocated
        {
            get
            {
                if (isSelected || expToDeposit.Id != null)
                {
                    return expToDeposit.Amount_Allocated__c;
                }
                else
                {
                    return 0.00;
                }
            }
            set
            {
                expToDeposit.Amount_Allocated__c = value;
            }
        }
    }
}