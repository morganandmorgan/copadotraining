public with sharing class MultimatterInterstitialController {
  private Id intakeId;

  public MultimatterInterstitialController(ApexPages.StandardController stdControllerParam) {
    intakeId = stdControllerParam.getRecord().Id;
  }

  private Intake__c CopyFieldsFromIntake() {
    // create new intake by copying data from this intake
    Intake__c existingIntake = queryForIntake();

    Intake__c newIntake = existingIntake.clone(false, true, false, false);
    newIntake.Caller_is_Injured_Party__c = 'Yes';

    insert newIntake;

    return newIntake;
  }

  private Intake__c queryForIntake() {
    // only query for fields to be copied
    Set<String> intakeFields = new Set<String>{ 'Id' };
    for (FieldSetMember fsm : SObjectType.Intake__c.FieldSets.Multi_Matter_Clone.getFields()) {
      intakeFields.add(fsm.getFieldPath());
    }

    System.debug(intakeFields);

    SoqlUtils.SoqlQuery intakeQuery = SoqlUtils.getSelect(new List<String>(intakeFields), 'Intake__c')
                    .withCondition(SoqlUtils.getEq('Id', intakeId));
    return Database.query(intakeQuery.toString());
  }

	public PageReference PerformRedirect() {
    Intake__c newIntake = CopyFieldsFromIntake();

    PageReference pr = Page.ContactSearch;
    pr.getParameters().putAll(new Map<String, String>{
            'intakeID' => newIntake.Id
        });
    return pr;
	}
}