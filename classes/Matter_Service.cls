/*============================================================================/
* Matter_Service
* @description Service class for Litify Matter
* @author Brian Krynitsky
* @date 2/26/2019
=============================================================================*/

public with sharing class Matter_Service {
	// Ctors
	public Matter_Service() {

	}

	// Methods
	/*-----------------------------------------------------------------------------
    Method to reset deposit allocations
    -----------------------------------------------------------------------------*/
	public void resetDepositAllocations(Set<Id> matterIds) {

		//instantiate selector classes
		mmmatter_ExpensesSelector expSelector = new mmmatter_ExpensesSelector();
		Deposit_Selector depositSelector = new Deposit_Selector();
		ExpenseToDeposit_Selector expDepSelector = new ExpenseToDeposit_Selector();

		Set<ID> depositIds = new Set<ID>();
		Set<ID> depositDeleteIds = new Set<ID>();

		Map<Id, Decimal> amtRecoveredMap = new Map<Id, Decimal>();
		
		//query
		List<litify_pm__Expense__c> expenseUpdate = expSelector.selectByMatterId_ResetAllocation(matterIds);
		List<Deposit__c> depList = depositSelector.selectOperatingTrustByMatterId(matterIds);
		List<Deposit__c> depositUpdate = new List<Deposit__c>();

		//Update deposits
	    for (Deposit__c deposit : depList){
			depositIds.add(deposit.Id);
			if(deposit.Journal__c == null){
				deposit.Deposit_Allocated__c = false;
				deposit.Allocated_Soft_Cost__c = 0;
				deposit.Allocated_Hard_Cost__c = 0;
				deposit.Allocated_Fee__c = 0;
				depositDeleteIds.add(deposit.Id);
				depositUpdate.add(deposit);
			}
	    }

	    //select the join records to delete
		List<Expense_to_Deposit__c> joinDeleteList = new List<Expense_to_Deposit__c>();
	    List<Expense_to_Deposit__c> joinList = expDepSelector.selectByDepositId(depositIds);
		for(Expense_to_Deposit__c expJoin : joinList){
			if(!depositDeleteIds.contains(expJoin.Deposit__c) && expJoin.Amount_Allocated__c != null){
				if(!amtRecoveredMap.containsKey(expJoin.Expense__c)){
					amtRecoveredMap.put(expJoin.Expense__c, expJoin.Amount_Allocated__c);
				}
				else{
					amtRecoveredMap.put(expJoin.Expense__c, amtRecoveredMap.get(expJoin.Expense__c) + expJoin.Amount_Allocated__c);
				}
			}
			else if(depositDeleteIds.contains(expJoin.Deposit__c)){
				joinDeleteList.add(expJoin);
			}
		}

		//delete the negative expenses
		List<Expense_to_Expense__c> expenseJoinDeleteList = [SELECT id FROM Expense_to_Expense__c WHERE Source_Expense__r.litify_pm__Matter__c in : matterIds];
		
		//Update Expenses
		for(litify_pm__Expense__c expense : expenseUpdate){
	        expense.Amount_Recovered_Settlement__c = amtRecoveredMap.containsKey(expense.Id) ? amtRecoveredMap.get(expense.Id) : 0;
	        expense.Amount_Recovered_Client__c = 0;
	        expense.Written_Off_Amount__c = 0;
	        expense.Expense_Written_Off__c = null;
	        expense.Written_Off__c = false;
	        expense.Fully_Recovered__c = expense.Amount_Recovered_Settlement__c == expense.litify_pm__Amount__c ? true: false;
	    }
		
		//dml
		update expenseUpdate;
		update depositUpdate;
		delete joinDeleteList;
		delete expenseJoinDeleteList;
	}

	/*-----------------------------------------------------------------------------
    Method for executing resetDepositAllocations from lightning component
    -----------------------------------------------------------------------------*/
	@AuraEnabled
    public static void executeAllocationReset(Id recordId){
        Set<Id> ids = new Set<Id>();
        ids.add(recordId);

        Matter_Service matterService = new Matter_Service();
        matterService.resetDepositAllocations(ids);
    } 

	/*-----------------------------------------------------------------------------
    autoAllocateAllItems - Allocates both negative expenses and then deposits
    -----------------------------------------------------------------------------*/
	public void autoAllocateAllItems(Set<Id> matterIds) {
		
		//this will allocated all of the negative expenses and the unallocated deposits
		MatterAllocationService.allocateItems(matterIds);
		
		//first allocated the negative expenses:
		//allocateNegativeExpenses(matterIds);

		//now allocate the deposits:
		//allocateAllDeposits(matterIds);

	}

	/*-----------------------------------------------------------------------------
    Method for executing autoAllocateAllItems from lightning component
    -----------------------------------------------------------------------------*/
	@AuraEnabled
    public static void executeAutoAllocation(Id recordId){
        Set<Id> ids = new Set<Id>();
        ids.add(recordId);

        Matter_Service matterService = new Matter_Service();
        matterService.autoAllocateAllItems(ids);
    } 

	/*-----------------------------------------------------------------------------
    allocateAllDeposits
    -----------------------------------------------------------------------------*/
	/* public void allocateAllDeposits(Set<Id> matterIds) {	

		Deposit_Selector depositSelector = new Deposit_Selector();
		List<Id> depositIdList = new List<Id>();
		for(Deposit__c d : depositSelector.selectOperatingTrustByMatterId(matterIds)){
			depositIdList.add(d.Id);
		}
		ffaDepositHelper.recordDeposits(depositIdList);
	} */

	/*-----------------------------------------------------------------------------
    allocateNegativeExpenses
    -----------------------------------------------------------------------------*/
	/* public void allocateNegativeExpenses(Set<Id> matterIds) {

		Set<String> writeOffExpenseTypeNames = new Set<String>{'Write Offs (Hard Costs)','Write Offs (Soft Costs)'};
	    Map<Id, List<litify_pm__Expense__c>> expenseMap = new Map<Id, List<litify_pm__Expense__c>>();
		Map<Id, Date> maxNegativeWriteOffDateMap = new Map<Id, Date>();
	    List<litify_pm__Expense__c> expensesToUpdate = new List<litify_pm__Expense__c>();
	    List<litify_pm__Expense__c> negativeExpenseList = new List<litify_pm__Expense__c>();

	    mmmatter_ExpensesSelector expSelector = new mmmatter_ExpensesSelector();

	    for(litify_pm__Expense__c expense : expSelector.selectByMatterId_Allocation(matterIds)){
	    	
			//load up the expense map
			if(!expenseMap.containsKey(expense.litify_pm__Matter__c)){
	    		List<litify_pm__Expense__c> eList = new List<litify_pm__Expense__c>{expense};
	    		expenseMap.put(expense.litify_pm__Matter__c, eList);
	    	}
	    	else{
	    		List<litify_pm__Expense__c> eList = expenseMap.get(expense.litify_pm__Matter__c);
	    		eList.add(expense);
	    		expenseMap.put(expense.litify_pm__Matter__c, eList);
	    	}

			//load up the max expense date of any write offs
			if(writeOffExpenseTypeNames.contains(expense.litify_pm__ExpenseType2__r.Name)){
				if(!maxNegativeWriteOffDateMap.containsKey(expense.litify_pm__Matter__c)){
					maxNegativeWriteOffDateMap.put(expense.litify_pm__Matter__c, expense.litify_pm__Date__c);
				}
				else{
					Date maxExpDate = expense.litify_pm__Date__c > maxNegativeWriteOffDateMap.get(expense.litify_pm__Matter__c) ? expense.litify_pm__Date__c : maxNegativeWriteOffDateMap.get(expense.litify_pm__Matter__c);
					maxNegativeWriteOffDateMap.put(expense.litify_pm__Matter__c, maxExpDate);
				}
			}


	    }
		    
	    for(Id matterId : expenseMap.keySet()){
	    	// First loop through all of the expenses and categorize them by CostType__c:
	        Map<String, List<litify_pm__Expense__c>> costTypeToExpenseMap = new Map<String, List<litify_pm__Expense__c>>();
	        Map<String, Decimal> negativeExpenseMap = new Map<String, Decimal>();
			Map<String, Decimal> negativeExpenseMap_WriteOff = new Map<String, Decimal>();

	        //get the list of expenses from the expense map:
	    	List<litify_pm__Expense__c> eList = expenseMap.get(matterId);

			//get the max negative write off date:
			Date maxWriteOffDate = maxNegativeWriteOffDateMap.containsKey(matterId) ? maxNegativeWriteOffDateMap.get(matterId) : null;
			
			for(litify_pm__Expense__c expense : eList){
				if(expense.litify_pm__Amount__c >= 0){
	                if (costTypeToExpenseMap.get(expense.CostType__c) == null){
	                    costTypeToExpenseMap.put(expense.CostType__c, new List<litify_pm__Expense__c>{expense});
	                }
	                else{
	                    costTypeToExpenseMap.get(expense.CostType__c).add(expense);    
	                }    
	            }
	            if(expense.litify_pm__Amount__c < 0){

					//separate the negative write offs from the other negative expenses
					if(writeOffExpenseTypeNames.contains(expense.litify_pm__ExpenseType2__r.Name)){
						if (negativeExpenseMap_WriteOff.get(expense.CostType__c) == null){
	                    	negativeExpenseMap_WriteOff.put(expense.CostType__c, expense.litify_pm__Amount__c);
						}
						else{
							negativeExpenseMap_WriteOff.put(expense.CostType__c, negativeExpenseMap_WriteOff.get(expense.CostType__c)+expense.litify_pm__Amount__c);
						}
					}
					else{
						if (negativeExpenseMap.get(expense.CostType__c) == null){
	                    	negativeExpenseMap.put(expense.CostType__c, expense.litify_pm__Amount__c);
						}
						else{
							negativeExpenseMap.put(expense.CostType__c, negativeExpenseMap.get(expense.CostType__c)+expense.litify_pm__Amount__c);
						}
					}
					negativeExpenseList.add(expense);
	            }     
			}

			// First, the hard costs allocated to negative hard costs
	        if(costTypeToExpenseMap.containsKey('HardCost') && (negativeExpenseMap.containsKey('HardCost') || negativeExpenseMap_WriteOff.containsKey('HardCost'))){
	            Decimal abs_remainingAmount = negativeExpenseMap.containsKey('HardCost') ? negativeExpenseMap.get('HardCost').abs() : 0;
				Decimal abs_remainingAmount_WriteOff = negativeExpenseMap_WriteOff.containsKey('HardCost') ? negativeExpenseMap_WriteOff.get('HardCost').abs() : 0;

				//get the list of the expenses we're going to update:
				List<litify_pm__Expense__c> expList = costTypeToExpenseMap.get('HardCost');

				//first loop through and apply write off expenses to positive ones if they fall within the max date
				if(abs_remainingAmount_WriteOff != null && abs_remainingAmount_WriteOff != 0 && maxWriteOffDate != null){
					//loop through the non write off expenses
					for(litify_pm__Expense__c expense : expList){

						//only proceed if the expense is on or before the max write off date
						if(expense.litify_pm__Date__c <= maxWriteOffDate){
							//set the values to 0 if for some reason they are null:
							expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
							expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
							expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

							// Check to verify that there is enough money left to pay off the expense.
							Decimal currentExpenseAmount = expense.litify_pm__Amount__c - expense.Written_Off_Amount__c;
							Decimal abs_currentExpenseAmount = currentExpenseAmount.abs();

							if (abs_remainingAmount_WriteOff >= currentExpenseAmount){
								
								expense.Fully_Recovered__c = true;
								expense.Written_Off_Amount__c += currentExpenseAmount;
								expense.litify_pm__Status__c = 'Paid';
								
								abs_remainingAmount_WriteOff -= currentExpenseAmount;
							}
							else if (currentExpenseAmount > 0){
								expense.Written_Off_Amount__c += abs_remainingAmount_WriteOff;
								abs_remainingAmount_WriteOff = 0;
							}
							System.debug('WRITE OFF LOOP - expense.Written_Off_Amount__c: ' + expense.Written_Off_Amount__c);
						}
					}
				}

				if(abs_remainingAmount != null && abs_remainingAmount != 0){
					
					//loop through the non write off expenses
					for(litify_pm__Expense__c expense : expList){
						//set the values to 0 if for some reason they are null:
						expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
						expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
						expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

						// Check to verify that there is enough money left to pay off the expense.
						Decimal currentExpenseAmount = expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Written_Off_Amount__c;
						Decimal abs_currentExpenseAmount = currentExpenseAmount.abs();

						if (abs_remainingAmount >= currentExpenseAmount){
							
							expense.Fully_Recovered__c = true;
							expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
							expense.litify_pm__Status__c = 'Paid';
							
							abs_remainingAmount -= currentExpenseAmount;
						}
						else if (currentExpenseAmount > 0){
							expense.Amount_Recovered_Settlement__c += abs_remainingAmount;
							abs_remainingAmount = 0;
						}
						System.debug('VOID LOOP - expense.Amount_Recovered_Settlement__c: ' + expense.Amount_Recovered_Settlement__c);
					}
				}
				expensesToUpdate.addAll(expList);
	        }
	        
	        // Next (lastly), the soft costs...
	        if(costTypeToExpenseMap.containsKey('SoftCost') && (negativeExpenseMap.containsKey('SoftCost') || negativeExpenseMap_WriteOff.containsKey('SoftCost'))){
	            Decimal abs_remainingAmount = negativeExpenseMap.containsKey('SoftCost') ?  negativeExpenseMap.get('SoftCost').abs() : 0;
				Decimal abs_remainingAmount_WriteOff = negativeExpenseMap_WriteOff.containsKey('SoftCost') ? negativeExpenseMap_WriteOff.get('SoftCost').abs() : 0;

				//get the list of the expenses we're going to update:
				List<litify_pm__Expense__c> expList = costTypeToExpenseMap.get('SoftCost');

	            // System.debug('SC: ' + costTypeToExpenseMap.get('SoftCost'));
	            // System.debug('SC SIZE: ' + costTypeToExpenseMap.get('SoftCost').size());
	            // System.debug('NEGATIVE SC: ' + negativeExpenseMap.get('SoftCost'));
				// System.debug('NEGATIVE SC WRITE OFF: ' + abs_remainingAmount_WriteOff);
				// System.debug('negativeExpenseMap ' + negativeExpenseMap);
				// System.debug('negativeExpenseMap_WriteOff ' + negativeExpenseMap_WriteOff);
				// System.debug('maxWriteOffDate: ' + maxWriteOffDate.format());

	            //first loop through and apply write off expenses to positive ones if they fall within the max date
				if(abs_remainingAmount_WriteOff != null && abs_remainingAmount_WriteOff != 0 && maxWriteOffDate != null){
					//loop through the non write off expenses
					for(litify_pm__Expense__c expense : expList){

						//only proceed if the expense is on or before the max write off date
						if(expense.litify_pm__Date__c <= maxWriteOffDate){
							//set the values to 0 if for some reason they are null:
							expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
							expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
							expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

							// Check to verify that there is enough money left to pay off the expense.
							Decimal currentExpenseAmount = expense.litify_pm__Amount__c - expense.Written_Off_Amount__c;
							Decimal abs_currentExpenseAmount = currentExpenseAmount.abs();

							if (abs_remainingAmount_WriteOff >= currentExpenseAmount){
								
								expense.Fully_Recovered__c = true;
								expense.Written_Off_Amount__c += currentExpenseAmount;
								expense.litify_pm__Status__c = 'Paid';
								
								abs_remainingAmount_WriteOff -= currentExpenseAmount;
							}
							else if (currentExpenseAmount > 0){
								expense.Written_Off_Amount__c += abs_remainingAmount_WriteOff;
								abs_remainingAmount_WriteOff = 0;
							}
							System.debug('WRITE OFF LOOP - expense.Written_Off_Amount__c: ' + expense.Written_Off_Amount__c);
						}
					}
				}

				if(abs_remainingAmount != null && abs_remainingAmount != 0){
					
					//loop through the non write off expenses
					for(litify_pm__Expense__c expense : expList){
						//set the values to 0 if for some reason they are null:
						expense.Amount_Recovered_Client__c = expense.Amount_Recovered_Client__c == null ? 0 : expense.Amount_Recovered_Client__c;
						expense.Amount_Recovered_Settlement__c = expense.Amount_Recovered_Settlement__c == null ? 0 : expense.Amount_Recovered_Settlement__c;
						expense.Written_Off_Amount__c = expense.Written_Off_Amount__c == null ? 0 : expense.Written_Off_Amount__c;

						// Check to verify that there is enough money left to pay off the expense.
						Decimal currentExpenseAmount = expense.litify_pm__Amount__c - expense.Amount_Recovered_Settlement__c - expense.Written_Off_Amount__c;
						Decimal abs_currentExpenseAmount = currentExpenseAmount.abs();

						if (abs_remainingAmount >= currentExpenseAmount){
							
							expense.Fully_Recovered__c = true;
							expense.Amount_Recovered_Settlement__c += currentExpenseAmount;
							expense.litify_pm__Status__c = 'Paid';
							
							abs_remainingAmount -= currentExpenseAmount;
						}
						else if (currentExpenseAmount > 0){
							expense.Amount_Recovered_Settlement__c += abs_remainingAmount;
							abs_remainingAmount = 0;
						}
						System.debug('VOID LOOP - expense.Amount_Recovered_Settlement__c: ' + expense.Amount_Recovered_Settlement__c);
					}
				}
				expensesToUpdate.addAll(expList);
	        }
	    }

	    //now loop through the negative expenses and set the fully recovered flags and the amount recovered fields
		for(litify_pm__Expense__c exp : negativeExpenseList){
			//update a different field if the expense is a write off
			if(writeOffExpenseTypeNames.contains(exp.litify_pm__ExpenseType2__r.Name)){
				//exp.Written_Off_Amount__c = exp.litify_pm__Amount__c;	
			}
			else{
				exp.Amount_Recovered_Settlement__c = exp.litify_pm__Amount__c;
			}
	        exp.Fully_Recovered__c = true;
	        expensesToUpdate.add(exp);
	    }
	    
	    update expensesToUpdate;
	} */

	/*-----------------------------------------------------------------------------
    Calculate Expense Totals Future
    -----------------------------------------------------------------------------*/
	@future public static void calculateExpenseTotals_Future (Set<Id> matterIds) {
		Matter_Service ms = new Matter_Service();
		ms.calculateExpenseTotals(matterIds);
	}

	/*-----------------------------------------------------------------------------
    Calculate Expense Totals
    -----------------------------------------------------------------------------*/
	public void calculateExpenseTotals (Set<Id> matterIds) {

	    Map<Id, Decimal> softCosts_Total_Map     = new Map<Id, Decimal>();
	    Map<Id, Decimal> softCosts_Recovered_Map = new Map<Id, Decimal>();
	    Map<Id, Decimal> softCosts_Remaining_Map = new Map<Id, Decimal>();
	    Map<Id, Decimal> softCosts_WriteOff_Map  = new Map<Id, Decimal>();
	    Map<Id, Decimal> hardCosts_Total_Map     = new Map<Id, Decimal>();
	    Map<Id, Decimal> hardCosts_Recovered_Map = new Map<Id, Decimal>();
	    Map<Id, Decimal> hardCosts_Remaining_Map = new Map<Id, Decimal>();
	    Map<Id, Decimal> hardCosts_WriteOff_Map  = new Map<Id, Decimal>();
		Map<Id, Decimal> trustBalance_Map        = new Map<Id, Decimal>();

	    mmmatter_ExpensesSelector expSelector = new mmmatter_ExpensesSelector();
		mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();
		TrustTransaction_Selector ttSelector = new TrustTransaction_Selector();
		List<litify_pm__Matter__c> matterList = matterSelector.selectById(matterIds);

		//summarize the expenses
	    for(litify_pm__Expense__c expense : expSelector.selectByMatterId(matterIds)){
			
			String key = expense.litify_pm__Matter__c;

			//hard costs
			if(expense.CostType__c == 'HardCost'){

				//total map
				if(expense.litify_pm__Amount__c != null){
					if(!hardCosts_Total_Map.containsKey(key)){
						hardCosts_Total_Map.put(expense.litify_pm__Matter__c, expense.litify_pm__Amount__c);
					}
					else{
						Decimal tmp = hardCosts_Total_Map.get(expense.litify_pm__Matter__c) + expense.litify_pm__Amount__c;
						hardCosts_Total_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}

				//recovered map
				if(expense.Amount_Recovered__c != null){
					if(!hardCosts_Recovered_Map.containsKey(key)){
						hardCosts_Recovered_Map.put(expense.litify_pm__Matter__c, expense.Amount_Recovered__c);
					}
					else{
						Decimal tmp = hardCosts_Recovered_Map.get(expense.litify_pm__Matter__c) + expense.Amount_Recovered__c;
						hardCosts_Recovered_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}

				//remaining map
				if(expense.Amount_Remaining_to_Recover__c != null){
					if(!hardCosts_Remaining_Map.containsKey(key)){
						hardCosts_Remaining_Map.put(expense.litify_pm__Matter__c, expense.Amount_Remaining_to_Recover__c);
					}
					else{
						Decimal tmp = hardCosts_Remaining_Map.get(expense.litify_pm__Matter__c) + expense.Amount_Remaining_to_Recover__c;
						hardCosts_Remaining_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}

				//write off map
				if(expense.Written_Off_Amount__c != null){
					if(!hardCosts_WriteOff_Map.containsKey(key)){
						hardCosts_WriteOff_Map.put(expense.litify_pm__Matter__c, expense.Written_Off_Amount__c);
					}
					else{
						Decimal tmp = hardCosts_WriteOff_Map.get(expense.litify_pm__Matter__c) + expense.Written_Off_Amount__c;
						hardCosts_WriteOff_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}
			}
			//soft costs
			if(expense.CostType__c == 'SoftCost'){

				//total map
				if(expense.litify_pm__Amount__c != null){
					if(!softCosts_Total_Map.containsKey(key)){
						softCosts_Total_Map.put(expense.litify_pm__Matter__c, expense.litify_pm__Amount__c);
					}
					else{
						Decimal tmp = softCosts_Total_Map.get(expense.litify_pm__Matter__c) + expense.litify_pm__Amount__c;
						softCosts_Total_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}

				//recovered map
				if(expense.Amount_Recovered__c != null){
					if(!softCosts_Recovered_Map.containsKey(key)){
						softCosts_Recovered_Map.put(expense.litify_pm__Matter__c, expense.Amount_Recovered__c);
					}
					else{
						Decimal tmp = softCosts_Recovered_Map.get(expense.litify_pm__Matter__c) + expense.Amount_Recovered__c;
						softCosts_Recovered_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}

				//remaining map
				if(expense.Amount_Remaining_to_Recover__c != null){
					if(!softCosts_Remaining_Map.containsKey(key)){
						softCosts_Remaining_Map.put(expense.litify_pm__Matter__c, expense.Amount_Remaining_to_Recover__c);
					}
					else{
						Decimal tmp = softCosts_Remaining_Map.get(expense.litify_pm__Matter__c) + expense.Amount_Remaining_to_Recover__c;
						softCosts_Remaining_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}

				//write off map
				if(expense.Written_Off_Amount__c != null){
					if(!softCosts_WriteOff_Map.containsKey(key)){
						softCosts_WriteOff_Map.put(expense.litify_pm__Matter__c, expense.Written_Off_Amount__c);
					}
					else{
						Decimal tmp = softCosts_WriteOff_Map.get(expense.litify_pm__Matter__c) + expense.Written_Off_Amount__c;
						softCosts_WriteOff_Map.put(expense.litify_pm__Matter__c, tmp);
					}
				}
			}
	    }

		//summarize the trust transactions
		for(Trust_Transaction__c tt : ttSelector.selectByMatterId(matterIds)){
			String key = tt.Matter__c;
			//trust balance map
			if(tt.Amount__c != null){
				if(!trustBalance_Map.containsKey(key)){
					trustBalance_Map.put(tt.Matter__c, tt.Amount__c);
				}
				else{
					Decimal tmp = trustBalance_Map.get(tt.Matter__c) + tt.Amount__c;
					trustBalance_Map.put(tt.Matter__c, tmp);
				}
			}
		}

		//loop through the matters and update values accordingly   
	    for(litify_pm__Matter__c matter : matterList){
	    	Id key = matter.id;

			//first set the values to 0
			matter.Total_Soft_Costs__c           = 0;
	    	matter.Total_Soft_Costs_Recovered__c = 0;
	    	matter.Total_Soft_Costs_Remaining__c = 0;
	    	matter.Write_Offs_Soft_Costs__c      = 0;
	    	matter.Total_Hard_Costs__c           = 0;
	    	matter.Total_Hard_Costs_Recovered__c = 0;
	    	matter.Total_Hard_Costs_Remaining__c = 0;
	    	matter.Write_Offs_Hard_Costs__c      = 0;
			matter.Trust_Balance__c              = 0;

			//now set the values according to the maps
			matter.Total_Soft_Costs__c           = softCosts_Total_Map.containsKey(key) ? softCosts_Total_Map.get(key) : matter.Total_Soft_Costs__c;
	    	matter.Total_Soft_Costs_Recovered__c = softCosts_Recovered_Map.containsKey(key) ? softCosts_Recovered_Map.get(key) : matter.Total_Soft_Costs_Recovered__c;
	    	matter.Total_Soft_Costs_Remaining__c = softCosts_Remaining_Map.containsKey(key) ? softCosts_Remaining_Map.get(key) : matter.Total_Soft_Costs_Remaining__c;
	    	matter.Write_Offs_Soft_Costs__c      = softCosts_WriteOff_Map.containsKey(key) ? softCosts_WriteOff_Map.get(key) : matter.Write_Offs_Soft_Costs__c;
	    	matter.Total_Hard_Costs__c           = hardCosts_Total_Map.containsKey(key) ? hardCosts_Total_Map.get(key) : matter.Total_Hard_Costs__c;
	    	matter.Total_Hard_Costs_Recovered__c = hardCosts_Recovered_Map.containsKey(key) ? hardCosts_Recovered_Map.get(key) : matter.Total_Hard_Costs_Recovered__c;
	    	matter.Total_Hard_Costs_Remaining__c = hardCosts_Remaining_Map.containsKey(key) ? hardCosts_Remaining_Map.get(key) : matter.Total_Hard_Costs_Remaining__c;
	    	matter.Write_Offs_Hard_Costs__c      = hardCosts_WriteOff_Map.containsKey(key) ? hardCosts_WriteOff_Map.get(key) : matter.Write_Offs_Hard_Costs__c;
			matter.Trust_Balance__c              = trustBalance_Map.containsKey(key) ? trustBalance_Map.get(key) : matter.Trust_Balance__c;
	    }

		update matterList;
	}

}