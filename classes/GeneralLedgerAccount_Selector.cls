/*============================================================================/
* GeneralLedgerAccount_Selector
* @description Selector for FFA General Ledger Accounts
* @author Brian Krynitsky
* @date Apr 2019
=============================================================================*/
public class GeneralLedgerAccount_Selector extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
			c2g__codaGeneralLedgerAccount__c.Id,
			c2g__codaGeneralLedgerAccount__c.Name,
			c2g__codaGeneralLedgerAccount__c.c2g__ReportingCode__c
        };
    }

    // Constructor
    public GeneralLedgerAccount_Selector() {
    }

    //required implemenation from fflib_SObjectSelector
    public Schema.SObjectType getSObjectType() {
        return c2g__codaGeneralLedgerAccount__c.SObjectType;
    }

    // Methods
    public List<c2g__codaGeneralLedgerAccount__c> selectByReportingCodes(Set<String> reportingCodes) {
        fflib_QueryFactory query = newQueryFactory();

        //where conditions:
        query.setCondition('c2g__ReportingCode__c IN :reportingCodes');

        return (List<c2g__codaGeneralLedgerAccount__c>) Database.query(query.toSOQL());
    }
}