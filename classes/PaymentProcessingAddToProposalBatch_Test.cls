@isTest
private without sharing class PaymentProcessingAddToProposalBatch_Test {

	
    static Payment_Collection__c createAndInsertPaymentCollection(){
        //Note:  These are better candidates for reuse.
        System.debug('Creating payment collection...');

        Payment_Collection__c record = null;
     
		record = new Payment_Collection__c(
			Company__c = TestDataFactory_FFA.company.Id,
			Bank_Account__c = TestDataFactory_FFA.bankAccounts[0].Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		insert record;
        return record;

    }   
    
    @isTest 
    static void test00(){
        //method template
    }

    @isTest 
    static void testCreate0(){
        PaymentProcessingAddToProposalBatch batchProcessor = null;

        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);

        batchProcessor = new PaymentProcessingAddToProposalBatch(paymentCollectionId);
        System.assert(batchProcessor!=null);
    }    

    @isTest 
    static void testCreate1(){
        PaymentProcessingAddToProposalBatch batchProcessor = null;

        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);

        batchProcessor = new PaymentProcessingAddToProposalBatch(paymentCollectionId, true);
        System.assert(batchProcessor!=null);
    }   

    @isTest 
    static void testCreate2(){
        PaymentProcessingAddToProposalBatch batchProcessor = null;

        Id paymentCollectionId = null;
        paymentCollectionId = TestDataFactory_FFA.getFakeRecordId(Payment_Collection__c.sObjectType);

        Payment_Collection__c paymentCollection = null;
        paymentCollection=createAndInsertPaymentCollection();

        batchProcessor = new PaymentProcessingAddToProposalBatch(paymentCollection);
        System.assert(batchProcessor!=null);
    }        

    @isTest 
    static void runBatchTest1(){

        PaymentProcessingAddToProposalBatch batchProcessor = null;

        Payment_Collection__c paymentCollection = null;
        paymentCollection=TestDataFactory_FFA.createAndInsertPaymentCollection();
        System.assert(paymentCollection!=null);

        Id paymentCollectionId = null;
        paymentCollectionId = paymentCollection.Id;

        c2g__codaPayment__c payment = null;
        //This may create and post expense PINs:
        //payment= TestDataFactory_FFA.createAndInsertPayment(paymentCollection);
        //payment = TestDataFactory_FFA.createPayment(1, true, 'Check');
        payment = TestDataFactory_FFA.createAndInsertPayment();
        System.assert(payment!=null);

        Id paymentId = null;
        paymentId = payment.Id; 

        //c2g.CODAAPIException: [1]Failed to post transaction:;[2]Payable Invoice: Object validation has failed. You cannot modify this invoice.;

        //Get limit error here calling postPayableInvoices:  
        //System.LimitException: c2g:Too many SOQL queries: 101

        Test.startTest();

        try{
            TestDataFactory_FFA.postPayableInvoices();
        }catch(Exception ffae) {
            //c2g.CODAAPIException
            // Type is not visible: c2g.CODAAPIException
            String message = null;
            System.debug('PIN Posting error:  ' + message);
            message = ffae.getMessage();
            if(message.containsIgnoreCase('You cannot modify this invoice')){
                System.debug('PIN already posted?' );
            }else{
                throw ffae;
            }
        }

        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = getTestTransactionLines();

        System.assert(transactionLineItems!=null);
        System.assert((transactionLineItems.size()>0));        
        
        //TODO:  Insert tracking records for the batch to work on, launch it:  


        // Payment_Processing_Batch_Status__c paymentStatus =  null;
        // paymentStatus = TestDataFactory_FFA.createPaymentProcessTrackingRecord(Id paymentCollectionId, Id paymentId);

        PaymentProcessingHelper utility = null;
        utility = new PaymentProcessingHelper();

        //This would create the payment again, which we don't want.
        //utility.createAndSavePayments(paymentCollection, transactionLineItems);

        PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper = null;
        paymentCollectionWrapper = new PaymentProcessing.PaymentCollectionWrapper();

        paymentCollectionWrapper.payments = new List<PaymentProcessing.PaymentWrapper>();

        PaymentProcessing.PaymentWrapper paymentWrapper = null;
        paymentWrapper = new PaymentProcessing.PaymentWrapper(payment);

        //TODO:  Add trans line wrappers to the wrapper:
        paymentWrapper.transactionLineItems = transactionLineItems;

        //List<Payment_Process_Add_To_Proposal_Status__c> insertPaymentCollectionAddToProposalStatus(PaymentProcessing.PaymentCollectionWrapper paymentCollectionWrapper){

        utility.insertPaymentCollectionAddToProposalStatus(paymentCollectionWrapper);

        // batchProcessor = new PaymentProcessingAddToProposalBatch(paymentCollectionId);
        // System.assert(batchProcessor!=null);
        

        utility.createProposals(paymentCollection, transactionLineItems);

        Test.stopTest();    //block for batches to complete.


    }


    private static List<c2g__codaTransactionLineItem__c> getTestTransactionLines(){
    
        List<c2g__codaTransactionLineItem__c> transactionLineItems=null;

        transactionLineItems = 
        [

        select 
        Id
        ,Name 
        ,c2g__Account__c
        ,c2g__Account__r.Name 
        ,c2g__Account__r.Split_Invoices_on_Separate_Payments__c 

        ,c2g__Transaction__r.c2g__DocumentNumber__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.c2g__AccountInvoiceNumber__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Name
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__c 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Matter__r.Name 
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__c
        ,c2g__Transaction__r.c2g__PayableInvoice__r.Litify_Expense__r.Name 

        ,c2g__Transaction__r.c2g__PayableInvoice__r.Payment_Bank_Account_Type__c 

        ,c2g__OwnerCompany__c
        ,c2g__OwnerCompany__r.Name 
        ,c2g__DocumentOutstandingValue__c 
        ,c2g__LineType__c 

        from c2g__codaTransactionLineItem__c 

        where 
        c2g__LineType__c = 'Account'
        and c2g__Transaction__r.c2g__PayableInvoice__c != null 
        and c2g__DocumentOutstandingValue__c != 0 
        and c2g__Account__r.c2g__CODAIntercompanyAccount__c != true 
        and c2g__OwnerCompany__c = :TestDataFactory_FFA.company.Id

        and c2g__Account__r.Split_Invoices_on_Separate_Payments__c = true 

        and Id not in 
        (
            select 
            c2g__TransactionLineItem__c
            from c2g__codaPaymentLineItem__c
        )

        order by 
        c2g__Transaction__r.c2g__Account__c
        ,c2g__Transaction__r.c2g__PayableInvoice__c 

        limit 1
        ];

        return transactionLineItems;

    }  
    

    
}