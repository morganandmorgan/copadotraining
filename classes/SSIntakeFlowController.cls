public class SSIntakeFlowController {

    public SSIntakeFlowController() { }

    public Flow.Interview.SS_Contact_Capture FlowInstance { get; set; }
    public PageReference FinishLocation {
        get {
            String url = '/';
            if (IntakeId != null) {
                url += IntakeId;
            }
            PageReference page = new PageReference(url);
            page.setRedirect(true);
            return page;
        }
    }
    @TestVisible public ID IntakeId {
        get {
            if (flowInstance != null) {
                return (ID)flowInstance.getVariableValue('IntakeID');
            }
            return IntakeId;
        }
        private set;
    }
    @TestVisible public ID AccountID {
        get {
            if (flowInstance != null) {
                return (ID)flowInstance.getVariableValue('PersonAccountID');
            }
            return AccountID;
        }
        private set;
    }
    private ID ContactID {
        get {
            List<Contact> contacts = [SELECT ID FROM Contact WHERE AccountID = :AccountID LIMIT 1];
            if (!contacts.isEmpty()) {
                return contacts.get(0).ID;
            } else {
                return null;
            }
        }
    }
}