/**
 * mmmatter_MatterTeamMembers
 * @description
 * @author Jeff Watson
 * @date 1/12/2018
 */

public class mmmatter_MatterTeamMembers extends mmlib_SObjectDomain implements mmmatter_IMatterTeamMembers {

    // Ctor
    public mmmatter_MatterTeamMembers(List<litify_pm__Matter_Team_Member__c> records) {
        super(records);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new mmmatter_MatterTeamMembers(sObjectList);
        }
    }

    // Trigger Handling
    public override void onAfterInsert() {
        System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(this.records));
    }

    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(this.records, (Map<Id, litify_pm__Matter_Team_Member__c>) existingRecords));
    }

    public override void onAfterDelete() {
        System.enqueueJob(new SyncMatterTeamMembersToLawsuitQueuable(this.records));
    }
}