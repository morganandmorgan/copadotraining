public class SpringAdvancedSearch {
    
    public void SpringAdvancedSearch(string matterId)
    {
        Spring_CM_Matter_Forms__mdt config = [select Advanced_Search__c from Spring_CM_Matter_Forms__mdt limit 1];
        	string folderId = '';
        try{
        	SpringCMService scm = new SpringCMService(UserInfo.getSessionId());
        	SpringCMFolder eosFolder = scm.findOrCreateEosFolder(matterId, 'litify_pm__Matter__c');
        	System.debug(eosFolder);
			integer index = eosFolder.Folders.Href.LastIndexOf('/');
        	System.debug(eosFolder.Folders.Href);
			string [] folderIdArr = eosFolder.Folders.Href.Split('/folders/'); 
        	folderId = folderIdArr[1].Split('/folders')[0];
        	System.debug(folderId);
        }
        catch(Exception e)
        {
            //Silent Failure
        }
        	
			this.url = config.Advanced_Search__c + '&fUid=' + folderId + '&nav=false';
      
    }
    
    public SpringAdvancedSearch()
    {
        	string matterId = ApexPages.currentPage().getParameters().get('matterId');
           	SpringAdvancedSearch(matterId);
	}
    public string url {get;set;}

}