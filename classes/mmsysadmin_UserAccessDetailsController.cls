public class mmsysadmin_UserAccessDetailsController
{
    // Define some constants so we can avoid magic strings.  At the very least, by using
    // constants, we avoid mismatches due to typos.
    private static final String PERMISSIONS = 'permissions';
    private static final String CREATE = 'permissionscreate';
    private static final String READ = 'permissionsread';
    private static final String EDIT = 'permissionsedit';
    private static final String DELET = 'permissionsdelete';
    private static final String VAR = 'permissionsviewallrecords';
    private static final String MAR = 'permissionsmodifyallrecords';

    private static final string UID_PARAM = 'uid';

    //
    // Be careful about the amount of viewstate to carry.  Too much viewstate has a
    // negative impact on performance.  Plus, it's just a bad idea to carry around stuff
    // that you don't need from request to request.
    //

    // The user is pretty central to this demonstration page and is needed for almost any
    // and every request.  We don't actually hold on to a lot of user data, so this should
    // be OK to carry around the view state.
    private Id userId;

    public String userFullName { get; private set; }

    // Likewise, the list of permsets for which we're displaying information is pretty
    // important for every request, so we should hold on to that in viewstate.
    public List<mmsysadmin_UADNameLabel> permsetInfo { get; private set; }

    // A note about the various mmsysadmin_UADMapHolder variables.  These are Maps for permission set id
    // to access settings construct.  Each of these maps will have one entry with a null
    // key which represents the users total access.

    // User perm info is only used for the initial request, so don't hold on to it.  The
    // comments on the following lines identify the patterns used for variables declared
    // in subsequent blocks.
    private transient List<mmsysadmin_UADNameLabel> xUserPermLabels;  // API Names and Labels, and IDs
    private transient mmsysadmin_UADMapHolder xUserPermStatus;        // Which perms are enabled for this user
    private transient Map<Id,PermissionSet> xUserPerms; // Map from permset Id to PermissionSet Object,
                                                        // which has the userperm settings

    // Likewise, object perm info is only used for the initial request, so don't hold on to it
    private transient List<mmsysadmin_UADNameLabel> xObjectLabels;
    private transient mmsysadmin_UADMapHolder xObjectStatus;

    // Map from permset id -> object name -> ObjectPermissions object.
    private transient Map<Id,Map<String,ObjectPermissions>> xObjectPerms;

    // FLS info used for rendering is only used a single time per request and not needed
    // from request to request, so we don't need to hold on to it.
    private transient List<mmsysadmin_UADNameLabel> xFieldLabels;
    private transient mmsysadmin_UADMapHolder xFieldStatus;

    // Map from permset id -> field name -> FieldPermissions object.  This is the set of
    // FLS data relevant for the object defined by flsObjectType (see below)
    private transient Map<Id,Map<String,FieldPermissions>> xFls;

    // flsObjectType contains the name of the object for which we display FLS information.
    // the VisualForce page uses this as a way to handle actionFunctions, allowing for
    // click-based interaction via javascript
    public String flsObjectType
    {
        get
        {
            if ( null != userId && null == flsObjectType)
            {
                // Default flsObjectType to the first "visible" object.
                for (mmsysadmin_UADNameLabel nl : getObjectLabels())
                {
                    if (getObjectPermStatus().it.get(nl.apiName))
                    {
                        flsObjectType = nl.apiName;
                        break;
                    }
                }
            }
            return flsObjectType;
        }
        set
        {
            if (value != flsObjectType)
            {
                // When setting flsObjectType, ensure that all the
                // FLS stuff is cleared out so that we are forced to
                // recalculate
                xFls = null;
                xFieldLabels = null;
                xFieldStatus = null;
                flsObjectType = value;
            }
        }
    }

    // ApexClass info is only used during the initial request.  Don't need to hold on to it.
    private transient List<mmsysadmin_UADNameLabel> xClassNames;
    private transient Map<Id,Map<String,Boolean>> xApexClasses;

    // Likewise, we don't need to hold onto VisualForce page information.
    private transient List<mmsysadmin_UADNameLabel> xPageNames;
    private transient Map<Id,Map<String,Boolean>> xPages;

    /**
     *  Main Constructor
     */
    public mmsysadmin_UserAccessDetailsController()
    {
        // Expect a uid Query Parameter
        if (ApexPages.currentPage().getParameters().containsKey(UID_PARAM))
        {
            lookup(ApexPages.currentPage().getParameters().get(UID_PARAM));
        }
    }

    /**
     * Looks up the target user and loads all their permission set info (name / label)
     */
    private void lookup(Id uid)
    {
        try
        {
            list<User> userList = mmcommon_UsersSelector.newInstance().selectByIdWithProfileRolePermissionSets( new set<Id> { uid } );

            User userData = userList[0];

            userId = userData.Id;
            userFullName = userData.Name;

            mmsysadmin_UADNameLabel totalAccess = new mmsysadmin_UADNameLabel('1TOTAL_ACCESS', 'TOTAL ACCESS');

            mmsysadmin_UADNameLabel profilePermsetInfo;

            List<mmsysadmin_UADNameLabel> nls = new List<mmsysadmin_UADNameLabel>();

            for (PermissionSetAssignment psa : userData.PermissionSetAssignments)
            {
                if (psa.PermissionSet.isOwnedByProfile)
                {
                    profilePermsetInfo = new mmsysadmin_UADNameLabel( psa.PermissionSet.Id
                                                                    , psa.PermissionSet.Name
                                                                    , psa.PermissionSet.Profile.Name);
                }
                else
                {
                    nls.add( new mmsysadmin_UADNameLabel( psa.PermissionSet.Id
                                                        , psa.PermissionSet.Name
                                                        , psa.PermissionSet.Label
                                                        ));
                }
            }

            permsetInfo = new List<mmsysadmin_UADNameLabel>();
            permsetInfo.add(totalAccess);
            permsetInfo.add(profilePermsetInfo);

            if ( ! nls.isEmpty() )
            {
                nls.sort();
                permsetInfo.addAll(nls);
            }
        }
        catch (DMLException ex)
        {
            // Don't care?
        }
        catch( Exception e )
        {
            // Don't care
        }
    }

    /**
     * Use Apex describe information to determine the set of User Permissions available within
     * this organization.
     */
    public List<mmsysadmin_UADNameLabel> getUserPermLabels()
    {
        if (null == xUserPermLabels)
        {
            List<mmsysadmin_UADNameLabel> result = new List<mmsysadmin_UADNameLabel>();

            Map<String, Schema.SObjectField> fMap = Schema.SObjectType.PermissionSet.fields.getMap();

            for (String fName : fMap.keySet())
            {
                if (fName.startsWith(PERMISSIONS))
                {
                    String label = fMap.get(fName).getDescribe().getLabel();
                    result.add(new mmsysadmin_UADNameLabel(fName, label));
                }
            }
            result.sort();
            xUserPermLabels = result;
        }
        return xUserPermLabels;
    }

    /**
     * Load the permission set information.  Yes, we could slightly optimize this by loading
     * this at the same time we load the permission set label information, but separating
     * the two keeps the code cleaner.
     */
    public Map<Id,PermissionSet> getUserPerms()
    {
        if (null == xUserPerms)
        {
            Map<Id,PermissionSet> result = new Map<Id,PermissionSet>();

            // Compute total access along the way.
            PermissionSet totalAccess = new PermissionSet();

            result.put(null, totalAccess);

            for (SObject sobj : queryUserPerms(extractPsIds(permsetInfo)))
            {
                PermissionSet ps = (PermissionSet) sobj;
                result.put(ps.Id, ps);

                for (mmsysadmin_UADNameLabel nl : getUserPermLabels())
                {
                    // Total access computation
                    totalAccess.put( nl.apiName
                                   , getWithDefault(totalAccess, nl.apiName, false)
                                        || getWithDefault(sobj, nl.apiName, false)
                                   );
                }
            }
            xUserPerms = result;
        }
        return xUserPerms;
    }

    /**
     * Helper function to encapsulate default handling of Boolean fields.  This is
     * assuredly overkill, but oh well...
     */
    private Boolean getWithDefault(SObject sobj, String fld, Boolean dflt)
    {
        try
        {
            if (null == sobj.get(fld))
            {
                return false;
            }
            return (Boolean) sobj.get(fld);
        }
        catch (Exception ex)
        {
            return dflt;
        }
    }

    /**
     * Basically provides an alternate representation of total access to make our
     * grid display consistent between access settings.
     */
    public mmsysadmin_UADMapHolder getUserPermStatus()
    {
        if (null == xUserPermStatus)
        {
            PermissionSet totalAccess = getUserPerms().get(null);

            Map<String,Boolean> msb = new Map<String,Boolean>();

            for (mmsysadmin_UADNameLabel nl : getUserPermLabels())
            {
                msb.put(nl.apiName, (Boolean) totalAccess.get(nl.apiName));
            }

            xUserPermStatus = new mmsysadmin_UADMapHolder(msb);
        }
        return xUserPermStatus;
    }

    /**
     * Transforms a list of mmsysadmin_UADNameLabel objects into just a List of Id objects
     */
    private List<Id> extractPsIds(List<mmsysadmin_UADNameLabel> psInfo)
    {
        List<Id> result = new List<Id>();

        if ( psInfo != null)
        {
            for (mmsysadmin_UADNameLabel nl : psInfo)
            {
                if (null != nl.Id)
                {
                    result.add(nl.Id);
                }
            }

        }

        return result;
    }

    /**
     * Queries all the available user perms from the permission set object.  Makes
     * use of getUserPermLabels, which uses Apex Describe to get the set of available
     * user perms.  This is used to construct a dynamic SOQL query.
     */
    private List<SObject> queryUserPerms(List<Id> psIds)
    {
        String soql = 'SELECT Id';

        for (mmsysadmin_UADNameLabel nl : getUserPermLabels())
        {
            soql += ', ' + nl.apiName;
        }

        soql += ' FROM PermissionSet WHERE Id IN :psIds';

        return Database.query(soql);
    }

    /**
     * Computes the API Name and Label information for the set of business objects available
     * to the organization
     */
    public List<mmsysadmin_UADNameLabel> getObjectLabels()
    {
        if (null == xObjectLabels)
        {
            List<mmsysadmin_UADNameLabel> result = new List<mmsysadmin_UADNameLabel>();
            Map<String,SObjectType> describe = Schema.getGlobalDescribe();

            // We can't really depend on describe to give us the object we want
            // since that includes a lot of stuff that doesn't support CRUD
            // or other supporting bits of metadata.  We're really just interested
            // in business data, so we'll rely on the set of object perms from
            // the system administrator profile to tell us what those objects are.
            //
            // Note: this is likely not a viable long-term solution, but it works
            // for now.
            for (ObjectPermissions op : [SELECT SObjectType
                                           FROM ObjectPermissions
                                          WHERE Parent.Profile.Name = 'System Administrator'])
            {
                if ( describe.containsKey(op.SObjectType)
                    && describe.get(op.SObjectType) != null
                    )
                {
                    result.add(new mmsysadmin_UADNameLabel( op.SObjectType
                                        , describe.get(op.SObjectType).getDescribe().getLabel()));
                }
            }

            result.sort();

            xObjectLabels = result;
        }

        return xObjectLabels;
    }

    /**
     * List of object perm API Names, in the order we wish them displayed
     */
    public List<String> getObjectPermFieldNames()
    {
        return new List<String> { CREATE, READ, EDIT, DELET, VAR, MAR };
    }

    /**
     *
     */
    public mmsysadmin_UADMapHolder getObjectPermStatus()
    {
        if (null == xObjectStatus)
        {
            Map<String,Boolean> msb = new Map<String, Boolean>();

            for (mmsysadmin_UADNameLabel nl : getObjectLabels())
            {
                // Converts total access into our mmsysadmin_UADMapHolder data structure.
                msb.put(nl.apiName, getWithDefault(getObjectPerms().get(null).get(nl.apiName), READ, false));
            }
            xObjectStatus = new mmsysadmin_UADMapHolder(msb);
        }
        return xObjectStatus;
    }

    /**
     * Computes the set of object permissions for all permsets assigned to the user,
     * including the user's total access
     */
    public Map<Id,Map<String,ObjectPermissions>> getObjectPerms()
    {
        if (null == xObjectPerms)
        {
            Map<Id,Map<String,ObjectPermissions>> result = new Map<Id,Map<String,ObjectPermissions>>();

            // Ensure that each permset and object has an entry in our map structure
            for (mmsysadmin_UADNameLabel ps : permsetInfo)
            {
                result.put(ps.Id, new Map<String,ObjectPermissions>());

                for (mmsysadmin_UADNameLabel op : getObjectLabels())
                {
                    result.get(ps.Id).put(op.apiName, new ObjectPermissions());
                }
            }

            // Create a record for total access and also make sure that it has
            // an ObjectPermissions entry for every object.
            Map<String,ObjectPermissions> totalAccess = new Map<String,ObjectPermissions>();

            result.put(null, totalAccess);

            for (mmsysadmin_UADNameLabel op : getObjectLabels())
            {
                totalAccess.put(op.apiName, new ObjectPermissions());
            }

            // Retrieve the set of ObjectPermissions in our permission sets and use
            // that to populate our result
            for (ObjectPermissions op : [SELECT Id, SObjectType, ParentId,
                                                PermissionsCreate, PermissionsRead, PermissionsEdit, PermissionsDelete,
                                                PermissionsViewAllRecords, PermissionsModifyAllRecords
                                           FROM ObjectPermissions
                                          WHERE ParentId IN :extractPsIds(permsetInfo)])
            {
                Map<String,ObjectPermissions> opMap = result.get(op.ParentId);

                opMap.put(op.SObjectType, op);

                ObjectPermissions totalOp = totalAccess.get(op.SObjectType);

                if ( totalOp != null )
                {
                    for (String apiName : getObjectPermFieldNames())
                    {
                        totalOp.put( apiName
                                   , getWithDefault( totalOp, apiName, false )
                                     || getWithDefault( op, apiName, false )
                                   );
                    }
                }
            }

            xObjectPerms = result;
        }
        return xObjectPerms;
    }

    /**
     * Returns the set of FLS settings we're interested in, in the order we wis to display them
     */
    public List<String> getFlsPermFieldNames()
    {
        return new String[] { READ, EDIT };
    }

    /**
     * Computes the list of fields.  NOTE: there is a subtle bug in here.  Because it uses
     * describe, it will return the set of fields available to the viewing user, not the user
     * in question.  There isn't a good way around this, as far as I know.  In reality, this
     * shouldn't be much of an issue here since a user won't have FieldPermissions assigned
     * for fields they do not have access to
     */
    public List<mmsysadmin_UADNameLabel> getFieldLabels()
    {
        if (null == flsObjectType) return null;

        if (null == xFieldLabels)
        {
            List<mmsysadmin_UADNameLabel> result = new List<mmsysadmin_UADNameLabel>();

            Map<String, Schema.SObjectField> fmap = Schema.getGlobalDescribe().get(flsObjectType).getDescribe().fields.getMap();

            for (String key : fmap.keySet())
            {
                result.add(new mmsysadmin_UADNameLabel(key, fmap.get(key).getDescribe().getLabel()));
            }

            result.sort();

            xFieldLabels = result;
        }
        return xFieldLabels;
    }

    /**
     * This is the action invoked by the VisualForce actionFunction.
     */
    public void loadFls()
    {
        // Nothing to do here.  The action call here sets flsObjectType, and getFls() does the loading lazily.
    }

    /**
     * Computes the set of FLS information for display, including total access calculation
     */
    public Map<Id,Map<String,FieldPermissions>> getFls()
    {
        if (null == xFls)
        {
            Map<Id,Map<String,FieldPermissions>> result = new Map<Id,Map<String,FieldPermissions>>();

            Set<Id> psIds = new Set<Id>();

            for (mmsysadmin_UADNameLabel ps : permsetInfo)
            {
                result.put(ps.Id, new Map<String,FieldPermissions>());
                psIds.add(ps.Id);
            }

            for (FieldPermissions fp : [SELECT ParentId, SObjectType, Field, PermissionsRead, PermissionsEdit
                                          FROM FieldPermissions
                                         WHERE ParentId IN :psIds
                                           AND SObjectType = :flsObjectType])
            {
                if (!fp.Field.startsWith(fp.SObjectType + '.')) continue;

                String fName = fp.Field.substring(fp.SObjectType.length() + 1).toLowerCase();

                result.get(fp.ParentId).put(fName, fp);

                if ( ! result.get(null).containsKey(fName) )
                {
                    result.get(null).put(fName, fp.clone(false));
                }
                else
                {
                    FieldPermissions tot = result.get(null).get(fName);
                    tot.PermissionsEdit |= fp.PermissionsEdit;
                    tot.PermissionsRead |= fp.PermissionsRead;
                }
            }

            // Some fields do not support FLS, but the user is considered to have
            // certain permissions on them.  These settings are controlled by
            // something other than FLS, but for the purposes of this exercise,
            // these settings are included in the user's total access.  NOTE:
            // because we use describe, the information is returned as appropriate
            // for the viewing user, not the user under scrutiny.  As such, the
            // information may be subtly incorrect.  Caveat Emptor.
            Map<String, Schema.SObjectField> fmap = Schema.getGlobalDescribe().get(flsObjectType).getDescribe().fields.getMap();

            for (String key : fmap.keySet())
            {
                DescribeFieldResult fDesc = fmap.get(key).getDescribe();

                if ( ! fDesc.isPermissionable() )
                {
                    result.get(null).put( fDesc.getName().toLowerCase(), new FieldPermissions( PermissionsRead = fDesc.isAccessible()
                                                                                             , PermissionsEdit = fDesc.isUpdateable()
                                                                                             )
                                        );
                }
            }
            xFls = result;
        }
        return xFls;
    }

    public mmsysadmin_UADMapHolder getFieldStatus()
    {
        if (null == xFieldStatus)
        {
            Map<String,Boolean> msb = new Map<String,Boolean>();

            for (mmsysadmin_UADNameLabel nl : getFieldLabels())
            {
                for (mmsysadmin_UADNameLabel ps : permsetInfo)
                {
                    if ( ! getFls().get(ps.Id).containsKey(nl.apiName))
                    {
                        getFls().get(ps.Id).put(nl.apiName, new FieldPermissions());
                    }
                }
                msb.put(nl.apiName, getFls().get(null).get(nl.apiName).PermissionsRead);
            }
            xFieldStatus = new mmsysadmin_UADMapHolder(msb);
        }
        return xFieldStatus;
    }

    /**
     * Computes the list of classes available in the org
     */
    public List<mmsysadmin_UADNameLabel> getClassNames()
    {
        if (null == xClassNames)
        {
            List<mmsysadmin_UADNameLabel> result = new List<mmsysadmin_UADNameLabel>();

            for (ApexClass ac : [SELECT Id, Name FROM ApexClass])
            {
                result.add(new mmsysadmin_UADNameLabel(ac.Id, ac.Name, ac.Name));
            }
            result.sort();
            xClassNames = result;
        }
        return xClassNames;
    }

    /**
     * Computes which Apex Classes are available per permset
     */
    public Map<Id,Map<String,Boolean>> getApexClasses()
    {
        if (null == xApexClasses)
        {
            xApexClasses = calculateSetupEntityAccess('ApexClass', getClassNames());
        }
        return xApexClasses;
    }

    public mmsysadmin_UADMapHolder getClassStatus()
    {
        return new mmsysadmin_UADMapHolder(getApexClasses().get(null));
    }

    /**
     * Computes which Visual Force pages are present in the org
     */
    public List<mmsysadmin_UADNameLabel> getPageNames()
    {
        if (null == xPageNames)
        {
            List<mmsysadmin_UADNameLabel> result = new List<mmsysadmin_UADNameLabel>();
            for (ApexPage ap : [SELECT Id, Name FROM ApexPage]) {
                result.add(new mmsysadmin_UADNameLabel(ap.Id, ap.Name, ap.Name));
            }
            result.sort();
            xPageNames = result;
        }
        return xPageNames;
    }

    public Map<Id,Map<String,Boolean>> getPages()
    {
        if (null == xPages)
        {
           xPages = calculateSetupEntityAccess('ApexPage', getPageNames());
        }
        return xPages;
    }

    public mmsysadmin_UADMapHolder getPageStatus()
    {
        return new mmsysadmin_UADMapHolder(getPages().get(null));
    }

    /**
     * Both ApexClass and ApexPage access settings are stored as SetupEntityAccess objects.  As such,
     * they can easily be processed using common logic with some variables used to account for
     * differences
     */
    private Map<Id,Map<String,Boolean>> calculateSetupEntityAccess( String seaType, List<mmsysadmin_UADNameLabel> seaNames )
    {
        Map<Id,Map<String,Boolean>> result = new Map<Id,Map<String,Boolean>>();
        Map<Id,String> seaMap = new Map<Id,String>();

        Map<String,Boolean> total = new Map<String,Boolean>();

        result.put(null, total);

        // Queries all the SetupEntityAccess objects of the appropriate type, and adds
        // the info to the result datastructure
        Map<Id,Set<Id>> enabledByPermset = new Map<Id,Set<Id>>();

        for (SetupEntityAccess sea : [SELECT ParentId, SetupEntityId
                                        FROM SetupEntityAccess
                                       WHERE SetupEntityType = :seaType])
        {
            Set<Id> enabled = enabledByPermset.get(sea.ParentId);

            if (null == enabled)
            {
                enabled = new Set<Id>();
                enabledByPermset.put(sea.ParentId, enabled);
            }
            enabled.add(sea.SetupEntityId);
        }

        // Ensure that each permset and setup entity has a row in our result datastructure.  This
        // keeps VisualForce happy when it does Map processing.
        for (mmsysadmin_UADNameLabel ps : permsetInfo)
        {
            if (null != ps.Id)
            {
                Map<String,Boolean> seaStatus = result.get(ps.Id);

                if (null == seaStatus)
                {
                    seaStatus = new Map<String,Boolean>();
                    result.put(ps.Id, seaStatus);
                }

                for (mmsysadmin_UADNameLabel nl : seaNames)
                {
                    boolean hasSea = enabledByPermset.containsKey(ps.Id) && enabledByPermset.get(ps.Id).contains(nl.id);
                    seaStatus.put(nl.apiName, hasSea);
                    if (hasSea || !total.containsKey(nl.apiName))
                    {
                        total.put(nl.apiName, hasSea);
                    }
                }
            }
        }

        return result;
    }
}