/*============================================================================/
* Deposit_Service
* @description Service class for Deposits
* @author Brian Krynitsky
* @date Apr 2019
=============================================================================*/
public class Deposit_Service {
	public Deposit_Service() {
		
	}

	// Methods
	/*-----------------------------------------------------------------------------
    Method to set the  Transfer Deposit and Intercompany Sales Invoice
    -----------------------------------------------------------------------------*/
	public void setDefaultFields(List<Deposit__c> newTrigger) {

		mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();
		Map<Id, litify_pm__Matter__c> matter_Map = new Map<Id, litify_pm__Matter__c>();

		Set<Id> matterIds = new Set<Id>();
		for(Deposit__c dep : newTrigger){
			matterIds.add(dep.Matter__c);
		}
		
		//store the matters in a map
		List<litify_pm__Matter__c> matterList = matterSelector.selectById(matterIds);
		for(litify_pm__Matter__c m : matterList){
			matter_Map.put(m.Id, m);
		}

		for(Deposit__c dep : newTrigger){

			//set the business lookup if it's null
			dep.AssignedToMMBusiness__c = dep.AssignedToMMBusiness__c == null ? matter_Map.get(dep.Matter__c).AssignedToMMBusiness__c : dep.AssignedToMMBusiness__c;
		}
	}

	/*-----------------------------------------------------------------------------
    Method to validate that the trust balance 
    -----------------------------------------------------------------------------*/
	public void validateTrustBalance(List<Deposit__c> newTrigger) {

		mmmatter_MattersSelector matterSelector = new mmmatter_MattersSelector();
		Map<Id, litify_pm__Matter__c> matter_Map = new Map<Id, litify_pm__Matter__c>();
		Map<Id, Decimal> matterTrustBalanceMap = new Map<Id, Decimal>();
		
		Id trustPayoutCostRT = Schema.SObjectType.Deposit__c.getRecordTypeInfosByName().get('Trust Payout - Costs').getRecordTypeId();
		Id trustPayoutFeeRT = Schema.SObjectType.Deposit__c.getRecordTypeInfosByName().get('Trust Payout - Fees').getRecordTypeId();
		Set<Id> trustImpactDepositRecordTypes = new Set<Id>{trustPayoutCostRT, trustPayoutFeeRT};
		system.debug('Deposit Service - validateTrustBalance - matterTrustBalanceMap = '+matterTrustBalanceMap);

		Set<Id> matterIds = new Set<Id>();
		for(Deposit__c dep : newTrigger){
			matterIds.add(dep.Matter__c);
		}
		
		//store the matters in a map
		List<litify_pm__Matter__c> matterList = matterSelector.selectById(matterIds);
		for(litify_pm__Matter__c m : matterList){
			matter_Map.put(m.Id, m);
		}

		for(AggregateResult ar : [
			SELECT sum(Amount__c) amt, Matter__c 
			FROM Trust_Transaction__c 
			WHERE Matter__c in : matter_Map.keySet()
			GROUP BY Matter__c]){
			matterTrustBalanceMap.put((Id)ar.get('Matter__c'), (Decimal)ar.get('amt'));
		}
		system.debug('Deposit Service - validateTrustBalance - matterTrustBalanceMap = '+matterTrustBalanceMap);

		for(Deposit__c dep : newTrigger){
			if(Test.isRunningTest() == false && dep.Matter__c != null && matter_Map.get(dep.Matter__c).AssignedToMMBusiness__r.FFA_Company__r.Use_Trust_Accounting__c == true && trustImpactDepositRecordTypes.contains(dep.RecordTypeId)){
				if(matterTrustBalanceMap.containsKey(dep.Matter__c)){
					if(dep.Amount__c > matterTrustBalanceMap.get(dep.Matter__c)){
						dep.addError('This deposit is greater than the remaining trust balance on this matter('+matter_Map.get(dep.Matter__c).ReferenceNumber__c+') you cannot create this deposit as it would overdraw the trust account');
					}
				}
				else{
					dep.addError('This deposit is greater than the remaining trust balance on this matter('+matter_Map.get(dep.Matter__c).ReferenceNumber__c+') you cannot create this deposit as it would overdraw the trust account');
				}
			}
		}
	}

	/*-----------------------------------------------------------------------------
    Method to set the  Default Bank Accounts on a Deposit
    -----------------------------------------------------------------------------*/
	public void handleBankAccountOverrides(List<Deposit__c> newTrigger){
        Set<Id> matterIds = new Set<Id>();
        for(Deposit__c deposit : newTrigger){
            matterIds.add(deposit.Matter__c);
        }

        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>([SELECT Id,
                    AssignedToMMBusiness__r.FFA_Company__r.Trust_Bank_Account__c,
                    AssignedToMMBusiness__r.FFA_Company__r.Cost_Bank_Account__c,
                    AssignedToMMBusiness__r.FFA_Company__r.Operating_Cash_Bank_Account__c
             FROM litify_pm__Matter__c
             WHERE Id IN :matterIds]);
        
        //loop through trigger new and set defaults     
        for(Deposit__c deposit : newTrigger){
            litify_pm__Matter__c matter = matterMap.containsKey(deposit.Matter__c) ? matterMap.get(deposit.Matter__c) : null;

            //set the fields
            deposit.Trust_Account__c = deposit.Trust_Account__c == null && matter != null ? matter.AssignedToMMBusiness__r.FFA_Company__r.Trust_Bank_Account__c : deposit.Trust_Account__c;
            deposit.Cost_Account__c = deposit.Cost_Account__c == null && matter != null ? matter.AssignedToMMBusiness__r.FFA_Company__r.Cost_Bank_Account__c : deposit.Cost_Account__c;
            deposit.Bank_Account_to_Deposit_In__c = deposit.Bank_Account_to_Deposit_In__c == null && matter != null ? matter.AssignedToMMBusiness__r.FFA_Company__r.Trust_Bank_Account__c : deposit.Bank_Account_to_Deposit_In__c;
        }
    }
}