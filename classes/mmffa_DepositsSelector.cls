public class mmffa_DepositsSelector
    extends mmlib_SObjectSelector
    implements mmffa_IDepositsSelector
{
    public static mmffa_IDepositsSelector newInstance()
    {
        return (mmffa_IDepositsSelector) mm_Application.Selector.newInstance(Deposit__c.SObjectType);
    }

    private Schema.sObjectType getSObjectType()
    {
        return Deposit__c.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            Deposit__c.Amount__c,
            Deposit__c.Bank_Account_to_Deposit_In__c,
            Deposit__c.Bank_General_Ledger_Account__c,
            Deposit__c.Check_Date__c,
            Deposit__c.Check_Number__c,
            Deposit__c.Deposit_Created__c,
            Deposit__c.Deposit_Description__c,
            Deposit__c.Deposit_Needed__c,
            Deposit__c.Deposit_Type__c,
            Deposit__c.FFA_Company__c,
            Deposit__c.FFA_Company_Name__c,
            Deposit__c.Journal_Line_Item__c,
            Deposit__c.Matter__c
        };
    }

    /**
     *  Primary selector for Deposit__c SObject
     *
     *  @param idSet the Set of record ids to query
     *  @return list of Deposit__c records
     */
    public List<Deposit__c> selectById(Set<Id> idSet)
    {
        return (List<Deposit__c>) selectSObjectsById(idSet);
    }
}