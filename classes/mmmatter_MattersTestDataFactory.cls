public class mmmatter_MattersTestDataFactory
    extends mmlib_AbstractTestDataFactory
{
    public enum MATTER_TYPES
    {
        SOCIAL_SECURITY
    }

    private MATTER_TYPES matterTypeRequested;
    private Account client;

    private Schema.sObjectType getSObjectType()
    {
        return litify_pm__Matter__c.SObjectType;
    }

    private litify_pm__Matter__c getMatter()
    {
        return (litify_pm__Matter__c)this.record;
    }

    public mmmatter_MattersTestDataFactory setClient( Account client )
    {
        configAcceptanceChecker('setClient( Account )');

        this.client = client;

        return this;
    }

    public mmmatter_MattersTestDataFactory typeSocialSecurity()
    {
        configAcceptanceChecker('typeSocialSecurity()');

        this.matterTypeRequested = MATTER_TYPES.SOCIAL_SECURITY;

        return this;
    }

    private void configurationDuringGeneration()
    {
        getMatter().litify_pm__Status__c = 'starting_value';

        if ( matterTypeRequested == null )
        {
            //default to Social_Security
            setRecordType( MATTER_TYPES.SOCIAL_SECURITY.name() );
        }
        else
        {
            setRecordType( matterTypeRequested.name() );
        }

        if ( this.client != null
            && this.uow != null )
        {
            this.uow.registerRelationship( this.record, litify_pm__Matter__c.litify_pm__Client__c, this.client );
        }

    }
}