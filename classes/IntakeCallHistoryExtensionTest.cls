@isTest
private class IntakeCallHistoryExtensionTest {

  @TestSetup
  private static void setup() {
    List<SObject> toInsert = new List<SObject>();

    Account client = TestUtil.createPersonAccount('Test', 'Client');
    toInsert.add(client);

    Incident__c incident = new Incident__c();
    toInsert.add(incident);

    Database.insert(toInsert);
    toInsert.clear();

    Intake__c intake = new Intake__c(
      Incident__c = incident.Id,
      Client__c = client.Id
    );
    toInsert.add(intake);

    Database.insert(toInsert);
    toInsert.clear();

    List<Task> tasks = new List<Task>();
    for (String callType : new List<String>{ 'Inbound', 'Outbound', null }) {
      for (Integer i = 0; i < 3; ++i) {
        Task t = new Task(
          Subject = 'Test Subject',
          CallType = callType,
          ActivityDate = Date.today().addDays(i)
        );
        tasks.add(t);
      }
    }
  }

  private static Intake__c getIntake() {
    return [
      SELECT
        Id
      FROM
        Intake__c
    ];
  }

  private static List<Task> getCallHistoryTasks(Intake__c intake) {
    return [
      SELECT
        Id
      FROM
        Task
      WHERE
        WhatId = :intake.Id
        AND CallType != null
      ORDER BY
        ActivityDate ASC,
        CreatedDate ASC
    ];
  }

  @isTest
  private static void populateTasks() {
    Intake__c intake = getIntake();
    List<Task> expectedTasks = getCallHistoryTasks(intake);

    Test.startTest();
    IntakeCallHistoryExtension extension = new IntakeCallHistoryExtension(new ApexPages.StandardController(intake));
    Test.stopTest();

    System.assertEquals(expectedTasks.size(), extension.tasks.size());
    for (Integer i = 0; i < expectedTasks.size(); ++i) {
      System.assertEquals(expectedTasks.get(i).Id, extension.tasks.get(i).Id);
    }
  }
}