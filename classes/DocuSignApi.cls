public without sharing class DocuSignApi {

    private String accountId;
    private String username;
    private String password;
    private String integratorKey;
    private String host;
    private String defaultReturnUrl;

    public DocuSignApi() {

        Docusign_Integration__c settings = Docusign_Integration__c.getInstance();

        accountId = settings.docusignAccountId__c;
        password = settings.Password__c;
        username = settings.Username__c;
        integratorKey = settings.IntegratorKey__c;
        host = settings.host__c;
        defaultReturnUrl = settings.ReturnUrl__c;

        /*
        isEnabled = settings.Enabled__c;
        isEnabledInSandbox = settings.EnabledInSandbox__c;
        signingCeremonyLandingPageTemplate = settings.Signing_Ceremony_Landing_Page_Template__c;
        docuSignUrlFailure = settings.docuSign_URL_failure__c;
        */
    } //constr


    private String getBaseUrl() {
        return 'HTTPS://' + host + '/restapi/v2.1/accounts/' + accountId;
    } //getBaseUrl


    private String getAuthorizationJson() {
		return  '{"Username": "' + username + '", ' +
			    '"Password": "' + password + '", ' +
			    '"IntegratorKey": "' + integratorKey + '"}';
    } //getAuthorizationJson


    private String jsonInlineTemplate(Integer sequence, String clientUserId, Map<String,String> recipient, Map<String,String> tokens) {
        
        String inlineTemplate = ''
        +'    "inlineTemplates": [{'
        +'        "sequence": "2",'
        +'        "recipients": {'
        +'            "signers": [{'
        +'                "email": "' + recipient.get('email') + '",'
        +'                "name": "' + recipient.get('name') + '",'
        +'                "recipientId": "1",'
		+'  			  "clientUserId": "' + clientUserId + '",'
        +'                "roleName": "Signer 1",'
        //+'                "suppressEmails": "true",'
            
        +'                "tabs": {'
        +'                    "textTabs": [';

        String delimiter = '';
        for (String token : tokens.keySet()) {
            inlineTemplate = inlineTemplate + delimiter
            +'                        {'
            +'                            "tabLabel": "' + token + '",'
            +'                            "value": "' + tokens.get(token) + '"'
            +'                        }';
            delimiter = ',';
        } //for tokens

        inlineTemplate = inlineTemplate
        +'                    ]'
        +'                }'
        +'            }]'
        +'        },'
            
		+'        "customFields": {'
		+'			"textCustomFields": ['
		//+'				{'
		//+'					"name": "DSFSSourceObjectId",'
		//+'					"show": "false",'
		//+'					"value": "' + intakeId + '"'
		//+'				},'
		+'				{'
		+'					"name": "SFUserId",'
		+'					"show": "false",'
		+'					"value": "' + UserInfo.getUserId() + '"'
		+'				},'
		+'				{'
		+'					"name": "CreatedByRestApi",'
		+'					"show": "false",'
		+'					"value": "true"'
		+'				}'
		+'         ]}'
            
        +'    }]';

        return inlineTemplate;
        
    } //jsonInlineTemplate


    private String jsonCompositeTemplate(List<String> templateIds, String clientUserId, Map<String,String> recipient, Map<String,String> tokens) {

        String compositeTemplate = '';

        Integer sequence = 1;
        String delimiter = '';
        for (String templateId : templateIds) {

            compositeTemplate = compositeTemplate + delimiter+'{'
            +'    "serverTemplates": [{'
            +'        "sequence": "1",'
            +'        "templateId": "' + templateId + '"'
            +'    }],'
            + jsonInlineTemplate(sequence, clientUserId, recipient, tokens)
            +'}';

            sequence++;
            delimiter = ',';

        } //for templateIds
        
        return compositeTemplate;

    } //jsonCompositeTemplate


    public String createCompositeEnvelope(List<String> templateIds, String clientUserId, Map<String,String> recipient, Map<String,String> tokens) {

        String requestBody = '{'
            //+'"emailSubject": "DocuSign API - Composite Templates",'
            //+'"emailBlurb": "Composite Templates Sample 1",'
            +'"status": "sent",'
            +'"compositeTemplates": ['

            +jsonCompositeTemplate(templateIds, clientUserId, recipient, tokens)

            +']'
        +'}';

        HttpRequest req = new HttpRequest();
		req.setHeader('X-DocuSign-Authentication', getAuthorizationJson());
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setEndpoint( getBaseUrl() + '/envelopes');
        req.setMethod('POST');

        req.setBody(requestBody);
        System.debug(requestBody);

        Http dsHttp = new Http();

        Map<String,String> response = new Map<String,String>();
        if ( !Test.isRunningTest()) {
            HTTPResponse res = dsHttp.send(req);
            System.debug( res );
            System.debug( res.getBody() );
            response = (Map<String,String>)Json.deserialize(res.getBody(), map<String,String>.Class);
        } else {
            response.put('envelopeId','1234abcd');
        }
        
        //System.debug( response );

        return response.get('envelopeId');

    } //createCompositeEnvelope

    
    public String getSigningUrl(String clientUserId, Map<String,String> recipient, String envelopeId, String iFrameOrigin, String returnUrl) {
        
       	String useReturnUrl;
        if ( !String.isBlank(returnUrl)) {
            useReturnUrl = returnUrl;
        } else {
            useReturnUrl = defaultReturnUrl;
        }
        
		String requestBody = '{'
            +'"username": "' + recipient.get('name') + '",'
		    +'"email": "' + recipient.get('email') + '",'
        	+'"recipientId": "1",'
			+'"clientUserId": "' + clientUserId + '",'
			+'"authenticationMethod": "email",'
            
            +'"xFrameOptions": "allow_from",'
            +'"xFrameOptionsAllowFromUrl": "' + iFrameOrigin + '",'
            
            +'"returnUrl": "' + useReturnUrl + '"'            
        +'}';

        HttpRequest req = new HttpRequest();
		req.setHeader('X-DocuSign-Authentication', getAuthorizationJson());
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setEndpoint( getBaseUrl() + '/envelopes/' + envelopeId + '/views/recipient');
        req.setMethod('POST');

        req.setBody(requestBody);
        System.debug(requestBody);

        Http dsHttp = new Http();

        Map<String,String> response = new Map<String,String>();
        if ( !Test.isRunningTest()) {
            HTTPResponse res = dsHttp.send(req);
            System.debug( res );
            System.debug( res.getBody() );
            response = (Map<String,String>)Json.deserialize(res.getBody(), map<String,String>.Class);
        } else {
            response.put('url','https://www.forthepeople.com');
        }
        
        //System.debug( response );

        return response.get('url');

    } //getSigningUrl

} //class