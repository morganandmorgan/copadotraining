@IsTest
private class MatterTaskHistoryTest {
    @testSetup
    static void prepareTestData() {
        // create matter and all needed related stuff
        Account account = new Account(Name = 'Test Account', litify_pm__Last_Name__c = 'Test Account');
        insert account;

        // create Default Matter Task and all needed related stuff
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c(Name = 'Test Matter Plane');
        insert matterPlan;
        
        litify_pm__Matter__c matter = new litify_pm__Matter__c(
                litify_pm__Client__c = account.Id,
                litify_pm__Status__c = 'Open',
                litify_pm__Matter_Plan__c = matterPlan.Id);
        insert matter;

        litify_pm__Matter_Stage__c matterStage = new litify_pm__Matter_Stage__c(
                Name = 'Test Matter Stage',
                litify_pm__Matter_Plan__c = matterPlan.Id,
                litify_pm__Stage_Order__c = 1,
                External_ID__c = 'TEST_TEST');
        insert matterStage;

        litify_pm__Default_Matter_Task__c defaultMatterTask = new litify_pm__Default_Matter_Task__c(
                Name='Test Default Matter Task',
                litify_pm__Matter_Stage__c = matterStage.Id);
        insert defaultMatterTask;

        // create Task
        Task trackableTask = new Task(
            ActivityDate = Date.newInstance(2018, 11, 21),
            Subject = 'Trackable Task',
            OwnerId = UserInfo.getUserId(),
            WhatId = matter.Id,
            Priority = 'Normal',
            Status = 'Not Started',
            litify_pm__Default_Matter_Task__c = defaultMatterTask.Id);
        insert trackableTask;

        Task untrackableTask = new Task(
                ActivityDate = System.Today(),
                Subject = 'Untrackable Task',
                OwnerId = UserInfo.getUserId(),
                Priority = 'Normal',
                Status = 'Not Started');

        insert untrackableTask;
    }

    static testMethod void testInsert() {
        Test.startTest();

        List<TaskHistory__c> taskHistories = [SELECT Id, TaskId__c, Type__c
                                              FROM TaskHistory__c
                                              WHERE Type__c = 'Insert'];
        Set<Id> insertedTaskIds = new Set<Id>();
        for(TaskHistory__c tH : taskHistories) {
            insertedTaskIds.add( Id.valueOf(tH.TaskId__c) );
        }

        List<Task> trackabletasks = [SELECT Id, Subject
                                     FROM Task
                                     WHERE Id IN :insertedTaskIds
                                         AND Subject = 'Trackable Task'];

        System.assertEquals(1, trackableTasks.size() );

        List<Task> untrackableTasks =  [SELECT Id, Subject
                                        FROM Task
                                        WHERE Id IN :insertedTaskIds
                                            AND Subject = 'Untrackable Task'];

        System.assertEquals(0, untrackableTasks.size() );

        Test.stopTest();
    }

    static testMethod void testUpdateDueDate() {
        Test.startTest();

        Task task = [SELECT Id, ActivityDate FROM Task WHERE Subject = 'Trackable Task'];
        task.ActivityDate = Date.newInstance(2018, 11, 22);
        update task;

        System.assertEquals(1, [SELECT count()
                                FROM TaskHistory__c
                                WHERE Type__c = 'Update'
                                    AND TaskId__c = :String.valueOf(task.Id)]);

        Test.stopTest();
    }

    static testMethod void testUpdatePriority() {
        Test.startTest();

        Task task = [SELECT Id, ActivityDate FROM Task WHERE Subject = 'Trackable Task'];
        task.Priority = 'High';
        update task;

        System.assertEquals(0, [SELECT count()
                                FROM TaskHistory__c
                                WHERE Type__c = 'Update'
                                   AND TaskId__c = :String.valueOf(task.Id)]);

        Test.stopTest();
    }
}