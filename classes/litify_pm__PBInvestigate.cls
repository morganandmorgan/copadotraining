/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBInvestigate {
    global PBInvestigate() {

    }
    @InvocableMethod(label='Investigate' description='Investigates the given referral')
    global static void investigate(List<litify_pm.PBInvestigate.ProcessBuilderInvestigateWrapper> investigateItems) {

    }
global class ProcessBuilderInvestigateWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderInvestigateWrapper() {

    }
}
}
