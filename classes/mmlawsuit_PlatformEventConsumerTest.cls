@isTest
private class mmlawsuit_PlatformEventConsumerTest {
    @isTest
    static void dev_test() {
        mmmatter_Matters.StatusChangedPlatformEventData d =
                new mmmatter_Matters.StatusChangedPlatformEventData();
        d.matter_id = fflib_IDGenerator.generate(litify_pm__Matter__c.sobjecttype);
        d.new_status = 'hello world';
        System.debug(
                JSON.serialize(
                        new List<mmmatter_Matters.StatusChangedPlatformEventData>{
                                d
                        }));
    }

    @isTest
    static void MatterStatusChanged_NominalPath() {
        // ----- Starting Data
        TestData td = initializeTestData();

        // Prepare test
        List<mmmatter_Matters.StatusChangedPlatformEventData> payloadList = new List<mmmatter_Matters.StatusChangedPlatformEventData>();
        mmmatter_Matters.StatusChangedPlatformEventData payload = new mmmatter_Matters.StatusChangedPlatformEventData();
        payload.matter_id = td.lawsuitMap.values().get(0).Related_Matter__c;
        payload.new_status = 'new status';
        payloadList.add(payload);

        List<mmlib_Event__e> eventList = new List<mmlib_Event__e>();
        mmlib_Event__e evt = new mmlib_Event__e();
        evt.EventName__c = 'MatterStatusChanged';
        evt.Payload__c = JSON.serialize(payloadList);
        eventList.add(evt);
        System.debug(JSON.serialize(evt));

        // Test
        mmlawsuit_PlatformEventConsumer consumer = new mmlawsuit_PlatformEventConsumer();
        consumer.setEvents(eventList);
        consumer.execute(null);

        List<Lawsuit__c> updatedLawsuitList = td.cts.sobjList();
        System.debug(JSON.serialize(updatedLawsuitList));

        System.assert(updatedLawsuitList != null);
        System.assert(!updatedLawsuitList.isEmpty());
        System.assertEquals(payload.new_status, updatedLawsuitList.get(0).Status__c);
    }

    @isTest
    static void UserChanged_NominalPath() {
        // ----- Starting Data
        TestData td = initializeTestData();

        // ----- Prepare to Execute
        String payload = '["' + td.userMap.values().get(1).Id + '"]';

        List<mmlib_Event__e> eventList = new List<mmlib_Event__e>();
        mmlib_Event__e evt = new mmlib_Event__e();
        evt.EventName__c = 'UsersChanged';
        evt.Payload__c = payload;
        eventList.add(evt);

        // ----- Execute
        mmlawsuit_PlatformEventConsumer consumer = new mmlawsuit_PlatformEventConsumer();
        consumer.setEvents(eventList);
        consumer.execute(null);

        // ----- Assertions
        List<Lawsuit__c> updatedLawsuitList = td.cts.sobjList();
        System.debug(JSON.serialize(updatedLawsuitList));

        System.assert(updatedLawsuitList != null);
        System.assert(!updatedLawsuitList.isEmpty());
        System.assertEquals(td.lawsuitMap.values().get(0).CP_Case_Developer_Email__c, updatedLawsuitList.get(0).CP_Case_Developer_Email__c);
        System.assertEquals(td.userMap.values().get(1).Email, updatedLawsuitList.get(0).CP_Managing_Attorney_Email__c);
        System.assertEquals(td.lawsuitMap.values().get(0).CP_Handling_Attorney_Email__c, updatedLawsuitList.get(0).CP_Handling_Attorney_Email__c);
    }

    static TestData initializeTestData() {
        TestData td = new TestData();

        // Users
        td.userMap = new Map<Id, User>();
        User usr = new User();
        usr.LastName = 'One';
        usr.FirstName = 'Joe';
        usr.Email = 'joe.one@example.com';
        usr.Id = fflib_IDGenerator.generate(User.SObjectType);
        usr.IsActive = true;
        td.userMap.put(usr.Id, usr);

        usr = new User();
        usr.LastName = 'Two';
        usr.FirstName = 'Jane';
        usr.Email = 'jane.two@example.com';
        usr.Id = fflib_IDGenerator.generate(User.SObjectType);
        usr.IsActive = true;
        td.userMap.put(usr.Id, usr);

        usr = new User();
        usr.LastName = 'Three';
        usr.FirstName = 'John';
        usr.Email = 'john.three@example.com';
        usr.Id = fflib_IDGenerator.generate(User.SObjectType);
        usr.IsActive = true;
        td.userMap.put(usr.Id, usr);

        // Matters
        td.matterMap = new Map<Id, litify_pm__Matter__c>();
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);
        td.matterMap.put(matter.Id, matter);

        // Team Roles
        td.roleMap = new Map<Id, litify_pm__Matter_Team_Role__c>();
        litify_pm__Matter_Team_Role__c role = new litify_pm__Matter_Team_Role__c();
        role.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Role__c.SObjectType);
        role.Name = mmlawsuit_LawsuitsService.RelevantTeamRoleName.CASE_DEVELOPER.name().replace('_', ' ');
        td.roleMap.put(role.Id, role);

        role = new litify_pm__Matter_Team_Role__c();
        role.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Role__c.SObjectType);
        role.Name = mmlawsuit_LawsuitsService.RelevantTeamRoleName.MANAGING_ATTORNEY.name().replace('_', ' ');
        td.roleMap.put(role.Id, role);

        role = new litify_pm__Matter_Team_Role__c();
        role.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Role__c.SObjectType);
        role.Name = mmlawsuit_LawsuitsService.RelevantTeamRoleName.PRINCIPAL_ATTORNEY.name().replace('_', ' ');
        td.roleMap.put(role.Id, role);

        // Team Members
        td.teamMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        litify_pm__Matter_Team_Member__c team = new litify_pm__Matter_Team_Member__c();
        team.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        team.litify_pm__Matter__c = td.matterMap.values().get(0).Id;
        team.litify_pm__User__c = td.userMap.values().get(0).Id;
        team.litify_pm__Role__c = td.roleMap.values().get(0).Id;
        td.teamMap.put(team.Id, team);

        team = new litify_pm__Matter_Team_Member__c();
        team.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        team.litify_pm__Matter__c = td.matterMap.values().get(0).Id;
        team.litify_pm__User__c = td.userMap.values().get(1).Id;
        team.litify_pm__Role__c = td.roleMap.values().get(1).Id;
        td.teamMap.put(team.Id, team);

        team = new litify_pm__Matter_Team_Member__c();
        team.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        team.litify_pm__Matter__c = td.matterMap.values().get(0).Id;
        team.litify_pm__User__c = td.userMap.values().get(2).Id;
        team.litify_pm__Role__c = td.roleMap.values().get(2).Id;
        td.teamMap.put(team.Id, team);

        // Lawsuits
        td.lawsuitMap = new Map<Id, Lawsuit__c>();
        Lawsuit__c lawsuit = new Lawsuit__c();
        lawsuit.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);
        lawsuit.Related_Matter__c = td.matterMap.values().get(0).Id;
        lawsuit.CP_Case_Developer__c = 'anonymous-1';
        lawsuit.CP_Case_Developer_Email__c = 'a1@example.com';
        lawsuit.CP_Managing_Attorney__c = 'anonymous-2';
        lawsuit.CP_Managing_Attorney_Email__c = 'a2@example.com';
        lawsuit.CP_Handling_Attorney__c = 'anonymous-3';
        lawsuit.CP_Handling_Attorney_Email__c = 'a3@example.com';
        td.lawsuitMap.put(lawsuit.Id, lawsuit);

        // ----- Mocks
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        td.mockTeamsSelector = (mmmatter_IMatterTeamMembersSelector) mocks.mock(mmmatter_IMatterTeamMembersSelector.class);
        td.mockUsersSelector = (mmcommon_IUsersSelector) mocks.mock(mmcommon_IUsersSelector.class);
        td.mockLawsuitsSelector = (mmlawsuit_ILawsuitsSelector) mocks.mock(mmlawsuit_ILawsuitsSelector.class);
        td.mockRolesSelector = (mmmatter_IMatterTeamRolesSelector) mocks.mock(mmmatter_IMatterTeamRolesSelector.class);

        mocks.startStubbing();

        mocks.when(td.mockTeamsSelector.sObjectType()).thenReturn(litify_pm__Matter_Team_Member__c.SObjectType);
        mocks.when(td.mockTeamsSelector.selectByMatter(td.matterMap.keyset())).thenReturn(td.teamMap.values());
        for (Integer i = 0; i < td.userMap.values().size(); i++) {
            mocks.when(
                    td.mockTeamsSelector
                            .selectByUser(new Set<Id>{
                            td.userMap.values().get(i).Id
                    }))
                    .thenReturn(new List<litify_pm__Matter_Team_Member__c>{
                    td.teamMap.values().get(i)
            });
        }

        mocks.when(td.mockUsersSelector.sObjectType()).thenReturn(User.SObjectType);
        mocks.when(td.mockUsersSelector.selectById(td.userMap.keyset())).thenReturn(td.userMap.values());
        for (User u : td.userMap.values()) {
            mocks.when(td.mockUsersSelector.selectById(new Set<Id>{
                    u.Id
            })).thenReturn(new List<User>{
                    u
            });
        }

        mocks.when(td.mockLawsuitsSelector.sObjectType()).thenReturn(Lawsuit__c.SObjectType);
        mocks.when(td.mockLawsuitsSelector.selectByMatter(new Set<Id>{
                lawsuit.Related_Matter__c
        })).thenReturn(new List<Lawsuit__c>{
                lawsuit
        });

        Set<String> teamRoleNameSet = new Set<String>();
        for (Integer i = 0; i < mmlawsuit_LawsuitsService.RelevantTeamRoleName.values().size(); i++) {
            teamRoleNameSet.add(mmlawsuit_LawsuitsService.RelevantTeamRoleName.values().get(i).name().replace('_', ' '));
        }

        mocks.when(td.mockRolesSelector.sObjectType()).thenReturn(litify_pm__Matter_Team_Role__c.SObjectType);
        mocks.when(td.mockRolesSelector.selectByName(teamRoleNameSet)).thenReturn(td.roleMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(td.mockTeamsSelector);
        mm_Application.Selector.setMock(td.mockUsersSelector);
        mm_Application.Selector.setMock(td.mockLawsuitsSelector);
        mm_Application.Selector.setMock(td.mockRolesSelector);

        td.cts = new CustomTestingStub();
        td.mockUow = (mmlib_ISObjectUnitOfWork) Test.createStub(mmlib_ISObjectUnitOfWork.class, td.cts);
        mm_Application.UnitOfWork.setMock(td.mockUow);

        return td;
    }

    class TestData {
        public Map<Id, User> userMap = null;
        public Map<Id, litify_pm__Matter__c> matterMap = null;
        public Map<Id, litify_pm__Matter_Team_Role__c> roleMap = null;
        public Map<Id, litify_pm__Matter_Team_Member__c> teamMap = null;
        public Map<Id, Lawsuit__c> lawsuitMap = null;
        public mmmatter_IMatterTeamMembersSelector mockTeamsSelector = null;
        public mmcommon_IUsersSelector mockUsersSelector = null;
        public mmlawsuit_ILawsuitsSelector mockLawsuitsSelector = null;
        public mmmatter_IMatterTeamRolesSelector mockRolesSelector = null;
        public CustomTestingStub cts = null;
        public mmlib_ISObjectUnitOfWork mockUow = null;
    }

    class CustomTestingStub
            implements System.StubProvider {
        private Map<Id, SObject> sobjMap = new Map<Id, SObject>();

        private List<SObject> sobjList() {
            return sobjMap.values();
        }

        public Object handleMethodCall(
                Object stubbedObject,
                String stubbedMethodName,
                Type returnType,
                List<Type> listOfParamTypes,
                List<String> listOfParamNames,
                List<Object> listOfArgs
        ) {
            if ('registerdirty'.equalsIgnoreCase(stubbedMethodName)) {
                for (Object obj : listOfArgs) {
                    SObject sobj = (SObject) obj;
                    sobjMap.put(sobj.Id, sobj);
                }
            }
            return null;
        }
    }
}