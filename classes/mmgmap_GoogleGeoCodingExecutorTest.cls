@isTest
private class mmgmap_GoogleGeoCodingExecutorTest
{
	@isTest
	private static void NormalTest()
	{
		Decimal lat = 45.0;
		Decimal lng = -90.0;

		mockGeoCodingService mockService = new mockGeoCodingService();
		mockService.lat = lat;
		mockService.lng = lng;

		mm_Application.Service.setMock(mmgmap_IGeoCodingService.class, mockService);

		Account acct = getAccount();

		mmlib_GenericBatch.IGenericExecuteBatch exec = new mmgmap_GoogleGeoCodingExecutor();
		exec.run(new List<Account> {acct});

		acct = [select Id, Name
                     , PersonMailingStreet, BillingStreet, ShippingStreet, PersonOtherStreet
                     , PersonMailingCity, BillingCity, ShippingCity, PersonOtherCity
                     , PersonMailingState, BillingState, ShippingState, PersonOtherState
                     , PersonMailingPostalCode, BillingPostalCode, ShippingPostalCode, PersonOtherPostalCode
                     , PersonMailingCountry, BillingCountry, ShippingCountry, PersonOtherCountry
                     , PersonMailingLatitude, BillingLatitude, ShippingLatitude, PersonOtherLatitude
                     , PersonMailingLongitude, BillingLongitude, ShippingLongitude, PersonOtherLongitude
                     , PersonMailingAddress, BillingAddress, ShippingAddress, PersonOtherAddress
                  from Account
                 where Id = :acct.Id
                 limit 1];

		System.debug(acct);
		System.debug(acct.PersonMailingAddress);

		//System.assertEquals(lat, acct.PersonMailingAddress.getLatitude(), 'Latitude was not set properly.');
		//System.assertEquals(lng, acct.PersonMailingAddress.getLongitude(), 'Longitude was not set properly.');
	}

	private static Account getAccount()
	{
		Id personAccountRecordType = [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;

		Account acct = new Account();

		acct.RecordTypeID = personAccountRecordType;
		acct.FirstName = 'Test FName';
		acct.LastName = 'Test LName';
		acct.PersonMailingStreet = '1600 Amphitheatre Pkwy.';
		acct.PersonMailingCity = 'Mountain View';
		acct.PersonMailingState = 'CA';
		acct.PersonMailingPostalCode = '';
		acct.PersonEmail = 'test@forthepeople.com';
		acct.PersonHomePhone = '1234567';
		acct.PersonMobilePhone = '12345678';

		insert acct;

		return [select Id, Name
                     , PersonMailingStreet, BillingStreet, ShippingStreet, PersonOtherStreet
                     , PersonMailingCity, BillingCity, ShippingCity, PersonOtherCity
                     , PersonMailingState, BillingState, ShippingState, PersonOtherState
                     , PersonMailingPostalCode, BillingPostalCode, ShippingPostalCode, PersonOtherPostalCode
                     , PersonMailingCountry, BillingCountry, ShippingCountry, PersonOtherCountry
                     , PersonMailingLatitude, BillingLatitude, ShippingLatitude, PersonOtherLatitude
                     , PersonMailingLongitude, BillingLongitude, ShippingLongitude, PersonOtherLongitude
                     , PersonMailingAddress, BillingAddress, ShippingAddress, PersonOtherAddress
		          from Account
		         where Id = :acct.Id
		         limit 1];
	}

	private class mockGeoCodingService
		implements mmgmap_IGeoCodingService
	{
		public Decimal lat = 0.0;
		public Decimal lng = 0.0;

		public Location getLatitudeLongitudeForAddress(mmgmap_Address addr)
		{
			return Location.newInstance(lat, lng);
		}
	}
}