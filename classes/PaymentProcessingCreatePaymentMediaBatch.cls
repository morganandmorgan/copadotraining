global class PaymentProcessingCreatePaymentMediaBatch implements Database.Batchable<SObject> {
    
    global Payment_Collection__c paymentCollection {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    global Id paymentCollectionId {get; set;}	//This may only have a value the first time since this is not a stateful batch.
    
    public PaymentProcessingCreatePaymentMediaBatch(Payment_Collection__c paymentCollection){
        this.paymentCollection=paymentCollection;
        this.paymentCollectionId=paymentCollection.Id;
    }	
    
    public PaymentProcessingCreatePaymentMediaBatch(Id paymentCollectionId){
        this.paymentCollectionId=paymentCollectionId;
    }    
    
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
        String soql=null;
        soql= 'select ' + 
        'Id ' + 
        ',Name ' + 
        '' + 
        ',Message__c ' + 
        ',Last_Error_Message__c ' + 
        ',Has_Error__c ' +             
        '' + 
        ',Payment_Collection__c ' + 
        ',Payment__c ' + 
        ',Payment__r.c2g__Status__c ' + 
        '' + 
        ',Are_Proposal_Lines_Added__c ' + 
        ',Is_Media_Data_Created__c ' + 
        '' + 
        'from Payment_Processing_Batch_Status__c ' + 
        'where ' + 
        'Payment__r.c2g__Status__c = \'Proposed\' ' +
        ' and Are_Proposal_Lines_Added__c = true ' + 
        'and Is_Media_Data_Created__c = false ' + 
        'and Payment_Collection__c = \'' + paymentCollectionId + '\' ';
        
        return Database.getQueryLocator(
            soql
        );
    }
    
    global void execute(Database.BatchableContext batchContext, List<Payment_Processing_Batch_Status__c> scope){
        // process each batch of records
        System.debug('PaymentProcessingCreatePaymentMediaBatch execute() - ' + scope.size() + ' records.');
        
        for (Payment_Processing_Batch_Status__c batchStatus : scope) {
			//TODO:  c2g.PaymentsPlusService.addToProposal(pay.Id, transLineIdList);
			//TODO:  Batch these, make one call for the scope for each Payment Id.
			Id paymentId = null;
            paymentId = batchStatus.Payment__c;                

            {   //Synchronous approach:  
                //TODO:  Consider making Media Control a lookup on the status object:  
                Id paymentMediaControlId = null;
                //FATAL_ERROR c2g.CODAException: You can only create media data for a payment proposal of type "Proposed".
                paymentMediaControlId = c2g.PaymentsPlusService.createMediaData(paymentId); 
                
                //TODO:  Figure out how to store and monitor these batches:
                
                batchStatus.Is_Media_Data_Created__c = true;
                batchStatus.Message__c = 'Media data created';
                
            }            
            
            /*
            {   //Asynchronous approach:
                Id batchProcessId = null;
                batchProcessId =  c2g.PaymentsPlusService.createMediaDataAsync(paymentId);
                
                //TODO:  Figure out how to store and monitor/poll these batches, flag as done when done::
                
                //batchStatus.Is_Media_Data_Created__c = true;
                //batchStatus.Message__c = 'Media data created';
                
            }
            */

        }
        
        update scope;	//update the batch status tracking records for this chunk of scope.
        
    }
    
    global void finish(Database.BatchableContext batchContext){
        System.debug('PaymentProcessingCreatePaymentMediaBatch finished.');
        
    }
    
}