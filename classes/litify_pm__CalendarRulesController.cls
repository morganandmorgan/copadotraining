/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CalendarRulesController {
    global CalendarRulesController() {

    }
global class processFlowInput {
    @InvocableVariable( required=false)
    global litify_pm__CalendarRulesTrigger__c crTrigger;
    @InvocableVariable( required=false)
    global Event evt;
    global processFlowInput() {

    }
}
}
