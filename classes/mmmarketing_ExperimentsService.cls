/**
 *  mmmarketing_ExperimentsService
 */
public class mmmarketing_ExperimentsService
{
    private static mmmarketing_IExperimentsService service()
    {
        return (mmmarketing_IExperimentsService) mm_Application.Service.newInstance( mmmarketing_IExperimentsService.class );
    }

    public static void setupExperimentsAndVariationsForMTIs( list<Marketing_Tracking_Info__c> mtiRecords )
    {
        service().setupExperimentsAndVariationsForMTIs( mtiRecords );
    }
}