public with sharing class DICE_DiceQueueGroupController {

    //container
    public QueueItem[] queues {get; set;}
        
    //service
    private DICEService service = new DICEService();
    
    public DICE_DiceQueueGroupController(Id DiceQueueGroupId) {
        //init
        this.queues = new QueueItem[]{};
            
        //load queues for group
		this.loadDiceQueueIds(DiceQueueGroupId);
		
		//collect
        for(QueueItem i : this.queues) {
            i.count = this.service.getRecordCountByQueueId(
            		  	i.id
            		  );
        }
        
    }
    
    public DICE_DiceQueueGroupController(ApexPages.StandardController stdController) {
        this(
            stdController.getRecord().Id
        );
    }
    
    private void loadDiceQueueIds(Id DiceQueueGroupId) {
        for (DICE_Queue_Group_Junction__c j : [SELECT DICE_Queue__c, DICE_Queue__r.Name
                                               FROM DICE_Queue_Group_Junction__c
                                               WHERE DICE_Queue_Group__c = :DiceQueueGroupId
                                              ]) {
        	//collect
            this.queues.add(
                new QueueItem(
                	j.DICE_Queue__c,
                	j.DICE_Queue__r.Name,
                	0
                )
            );
        }
    }
    
    public class QueueItem {
        public QueueItem(Id id, String name, Integer count){
            this.id = id;
            this.name = name;
            this.count = count;
        }
        public Id id {get; set;}
        public String name {get; set;}
        public Integer count {get; set;}
    }
    

}