public without sharing class AuditRuleToolController {

    public String userTheme {get;set;}

    public AuditEvalEngine.Rule theRule {get;set;}
    public Boolean hasFlags {get;set;}

    public String baseField {get;set;}
    public String baseFieldRendered {get;set;}

    public String baseRelatedField {get;set;}
    
    public String relatedField {get;set;}
    public String relatedFieldRendered {get;set;}
    public String relatedObject {get;set;}

    public Integer deleteIndex {get;set;}

    private AuditEvalEngine.Rule loadRule(Id ruleId) {
        AuditRuleSelector ars = new AuditRuleSelector();
        Schema.FieldSet fieldSet = Schema.SObjectType.Audit_Rule__c.fieldSets.getMap().get('AuditRuleTool');

        //add the fields from the field set
        if (fieldSet != null) {
            List<Schema.SObjectField> sObjectFields = new List<Schema.SObjectField>();
            for(Schema.FieldSetMember f : fieldSet.getFields()) {
                sObjectFields.add( f.getSObjectField() );
            }
            ars.addSObjectFields(sObjectFields);
        }

        return ars.selectById(new Set<Id>{ruleId})[0];
    } //loadRule

    public AuditRuleToolController(ApexPages.StandardController controller) {

        userTheme = UserInfo.getUiThemeDisplayed();

        Id ruleId = controller.getRecord().id;

        if ( ruleId != null ) {
            theRule = loadRule(ruleId);
            AuditRuleDomain ruleDomain = new AuditRuleDomain();
            hasFlags = ruleDomain.ruleHasFlags(new List<Audit_Rule__c>{theRule.auditRule}).get(theRule.auditRule.id);
        } else {
            Id cloneId = System.currentPageReference().getParameters().get('cloneId');
            if (cloneId == null) {
                theRule = new AuditEvalEngine.Rule();
                theRule.auditRule = new Audit_Rule__c();
                theRule.expressions = new List<AuditEvalEngine.RuleExpression>();

                //let's add a blank expression to use on the page
                AuditEvalEngine.RuleExpression expression = new AuditEvalEngine.RuleExpression();
                theRule.expressions.add(expression);
            } else {
                theRule = loadRule(cloneId);
                theRule.auditRule = theRule.auditRule.clone(false,true,false,false);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'Audit Rule Cloned.'));
            }
            hasFlags = false;
        } //if ruleId

    } //constructor


    public List<Schema.FieldSetMember> getDynamicFields() {
        Schema.FieldSet fieldSet = Schema.SObjectType.Audit_Rule__c.fieldSets.getMap().get('AuditRuleTool');

        if (fieldSet != null) {
            return fieldSet.getFields();
        } else {
            return new List<Schema.FieldSetMember>();
        }
    } //getDynamicFields


    private List<SelectOption> sortOptions(List<SelectOption> unsortedOptions) {

        List<String> labels = new List<String>();
        Map<String,SelectOption> optionsMap = new Map<String,SelectOption>();

        for (SelectOption option : unsortedOptions) {
            String sortLabel = option.getLabel();

            Integer labelVers = 0;
            while (optionsMap.containsKey(sortLabel)) {
                labelVers++;
                sortLabel = option.getLabel() + ' [[' + String.valueOf(labelVers);
            }

            labels.add( sortLabel);
            optionsMap.put(sortLabel, option);
        }

        labels.sort();

        List<SelectOption> sortedOptions = new List<SelectOption>();

        for (String label : labels) {
            sortedOptions.add( optionsMap.get(label));
        }

        return sortedOptions;
    } //sortOptions


    public List<SelectOption> getBaseObjects() {

        List<SelectOption> baseObjects = new List<SelectOption>();
        baseObjects.add( new SelectOption('',''));

        SObjectType objType = Schema.getGlobalDescribe().get('Audit_Scheduled_Flag__c');
        Map<String,Schema.SObjectField> ofields = objType.getDescribe().fields.getMap();

        for (String fieldName : ofields.keySet()) {
            if (fieldName != 'audit_rule__c' && fieldName != 'connectionreceivedid' && fieldName != 'connectionsentid'
                    && fieldName != 'createdbyid' && fieldName != 'lastmodifiedbyid'
                    && fieldName != 'user__c' && fieldName != 'ownerId') {
                String svalue = fieldName;
                Schema.DescribeFieldResult fdesc = ofields.get(fieldName).getDescribe();
                if (fdesc.getReferenceTo().size() != 0) {
                    String objName = fdesc.getReferenceTo()[0].getDescribe().getName();
                    baseObjects.add( new SelectOption(objName, objName));
                }
            }
        } //for fields

        return sortOptions(baseObjects);

    } //getBaseObjects


    public List<SelectOption> getDateTimeFields() {
        List<SelectOption> dtFields = new List<SelectOption>();
        dtFields.add( new SelectOption('',''));

        if (theRule.auditRule.object__c != null && theRule.auditRule.object__c != '') {
            SObjectType objType = Schema.getGlobalDescribe().get(theRule.auditRule.object__c);
            Map<String,Schema.SObjectField> ofields = objType.getDescribe().fields.getMap();

            for (String fieldName : ofields.keySet()) {
                Schema.DescribeFieldResult fdesc = ofields.get(fieldName).getDescribe();

                if (fdesc.getType() == Schema.DisplayType.Date || fdesc.getType() == Schema.DisplayType.DateTime) {
                    dtFields.add( new SelectOption(fieldName, fdesc.getLabel()));
                }
            } //for fields

        } //if object

        return sortOptions(dtFields);

    } //getDateTimeFields


    public List<SelectOption> getUserFields() {
        List<SelectOption> userFields = new List<SelectOption>();
        userFields.add( new SelectOption('',''));

        if (theRule.auditRule.object__c != null && theRule.auditRule.object__c != '') {
            SObjectType objType = Schema.getGlobalDescribe().get(theRule.auditRule.object__c);
            Map<String,Schema.SObjectField> ofields = objType.getDescribe().fields.getMap();

            for (String fieldName : ofields.keySet()) {
                Schema.DescribeFieldResult fdesc = ofields.get(fieldName).getDescribe();
                //loop through any refrences
                for (Schema.sObjectType refTo: fdesc.getReferenceTo()) {
                    //if it references a user, we want it
                    if (refTo.getDescribe().getName() == 'User') {
                        userFields.add( new SelectOption(fieldName, fdesc.getLabel()));
                    }
                }
            } //for fields

        } //if object

        return sortOptions(userFields);

    } //getUserFields


    public List<SelectOption> getBaseFields() {

        List<SelectOption> baseFields = new List<SelectOption>();
        baseFields.add( new SelectOption('',''));

        if (theRule.auditRule.object__c != null && theRule.auditRule.object__c != '') {
            SObjectType objType = Schema.getGlobalDescribe().get(theRule.auditRule.object__c);
            Map<String,Schema.SObjectField> ofields = objType.getDescribe().fields.getMap();

            for (String fieldName : ofields.keySet()) {
                String svalue = fieldName;
                Schema.DescribeFieldResult fdesc = ofields.get(fieldName).getDescribe();
                if (fdesc.getRelationshipName() != null && fdesc.getReferenceTo().size() != 0) {
                    svalue = svalue + '^' + fdesc.getRelationshipName();
                    svalue = svalue + '^' + fdesc.getReferenceTo()[0];
                }
                baseFields.add( new SelectOption(svalue, fdesc.getLabel()));
            } //for fields

        } //if object

        return sortOptions(baseFields);

    } //getBaseFields


    public PageReference selectBaseField() {

        if (baseField.contains('^')) {
            List<String> baseParts = baseField.split('\\^');
            baseFieldRendered = '';
            relatedObject = baseParts[2].replace('(','').replace(')','');
            baseRelatedField = baseParts[1];
        } else {
            baseFieldRendered = '{!' + baseField + '}';
            relatedObject = null;
            baseRelatedField = null;
        }

        return null;
    } //selectBaseField


    public PageReference copyBaseFieldToNextArgument() {

        for (AuditEvalEngine.RuleExpression expression : theRule.expressions) {
            if ( expression.argument1 == null || expression.argument1 == '') {
                expression.argument1 = '{!' + baseField + '}';
                break;
            }
            if ( expression.argument2 == null || expression.argument2 == '') {
                expression.argument2 = '{!' + baseField + '}';
                break;
            }
        }

        return null;
    } //copyBaseFieldToNextArgument


    public List<SelectOption> getRelatedFields() {

        List<SelectOption> relatedFields = new List<SelectOption>();
        relatedFields.add( new SelectOption('',''));

        if (relatedObject != null && relatedObject != '') {
            SObjectType objType = Schema.getGlobalDescribe().get(relatedObject);
            Map<String,Schema.SObjectField> ofields = objType.getDescribe().fields.getMap();
            for (String fieldName : ofields.keySet()) {
                Schema.DescribeFieldResult fdesc = ofields.get(fieldName).getDescribe();
                relatedFields.add( new SelectOption(fieldName, fdesc.getLabel()));
            } //for fields
        } //if object

        return sortOptions(relatedFields);

    } //getRelatedFields


    public PageReference selectRelatedField() {
        relatedFieldRendered = '{!' + baseRelatedField + '.' + relatedField + '}';
        return null;
    } //selectBaseField


    public PageReference copyRelatedFieldToNextArgument() {

        for (AuditEvalEngine.RuleExpression expression : theRule.expressions) {
            if ( expression.argument1 == null || expression.argument1 == '') {
                expression.argument1 = relatedFieldRendered;
                break;
            }
            if ( expression.argument2 == null || expression.argument2 == '') {
                expression.argument2 = relatedFieldRendered;
                break;
            }
        }

        return null;
    } //copyBaseFieldToNextArgument


    public PageReference addExpression() {
        AuditEvalEngine.RuleExpression expression = new AuditEvalEngine.RuleExpression();
        theRule.expressions.add(expression);
        return null;
    } //addExpression

    public PageReference deleteExpression() {
        theRule.expressions.remove(deleteIndex);
        return null;
    }


    public PageReference cloneRule() {
        PageReference pr = Page.AuditRuleTool;
        pr.getParameters().put('cloneId',theRule.auditRule.id);
        pr.setRedirect(true);
        return pr;
    } //cloneRule


    public PageReference testRule() {
        AuditEvalEngine aee = new AuditEvalEngine();
        String result = aee.test(theRule);

        if (result.containsIgnoreCase('failed')) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, result));
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, result));
        }

        return null;
    } //testRule


    public PageReference saveRule() {
        //validation
        Boolean valid = true;
        if ( theRule.auditRule.rule_type__c=='Scheduled' && theRule.auditRule.days__c == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, '"Days" is required for "Scheduled" rules.'));
            valid = false;            
        }
        for (AuditEvalEngine.RuleExpression expression : theRule.expressions) {
            if ( !String.isBlank(expression.dateModifier) && !expression.argument2.isNumeric()) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'If a Date Modifier is selected, Argument 2 must be numeric.'));
                valid = false;
            }
        }
        if (valid) {
            AuditRuleDomain ard = new AuditRuleDomain();
            ard.upsertRule(theRule);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Rule saved.'));
        }
        return null;
    } //saveRule

    public PageReference scheduleJob() {
        AuditFlagScheduled afs = new AuditFlagScheduled();
        afs.execute(null);        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'Job scheduled.'));
        return null;
    } //scheduleJob

} //class