public with sharing class mmsms_ScheduledSmsSelector
	extends mmlib_SObjectSelector
	implements mmsms_IScheduledSmsSelector
{
	public static mmsms_IScheduledSmsSelector newInstance()
	{
		return new mmsms_ScheduledSmsSelector();
	}

	private mmsms_ScheduledSmsSelector() {}

	private Schema.sObjectType getSObjectType()
	{
		return tdc_tsw__Scheduled_Sms__c.SObjectType;
	}

	private List<Schema.SObjectField> getAdditionalSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				tdc_tsw__Scheduled_Sms__c.tdc_tsw__Account__c,
				tdc_tsw__Scheduled_Sms__c.tdc_tsw__Campaign__c,
				tdc_tsw__Scheduled_Sms__c.tdc_tsw__Contact__c,
				tdc_tsw__Scheduled_Sms__c.tdc_tsw__Phone_Api__c,
				tdc_tsw__Scheduled_Sms__c.tdc_tsw__SMS_Template__c
		};
	}

	public List<tdc_tsw__Scheduled_Sms__c> getRecentRecords_AllRows(Integer timePeriodInMinutes)
	{
		return
			Database.query
			(
				newQueryFactory()
					.setCondition
					(
						tdc_tsw__Scheduled_Sms__c.CreatedDate +
						' > ' +
						mmlib_Utils.formatSoqlDatetimeUTC(Datetime.now().addMinutes(-1 * timePeriodInMinutes))
					)
				.toSOQL() +
				' all rows'
			);
	}
}