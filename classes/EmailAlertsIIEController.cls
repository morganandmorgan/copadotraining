public class EmailAlertsIIEController {

    public Intake__c thisIntake {get;set;}    
    public Account thisAccount {get;set;}

    public IntakeInvestigationEvent__c thisIntakeInvestigationEvent {get;set;}
    public IncidentInvestigationEvent__c thisIncident {get;set;}
    public IntakeInvestigation__c thisIntakeInvestigation {get;set;}
    
    public String intakeFieldSet {get;set;}
    public String intakeInvestigationFieldSet {get;set;}
    public String incidentInvestigationFieldSet {get;set;}
    
    public String selectedInvestigation {get;set;}

    public Id relatedIntakeId {
        get {
            return relatedIntakeId;
        }
        set {
            relatedIntakeId = value;
            
            intakeFieldSet = getFieldSet(); 
            thisIntake = queryIntake(intakeFieldSet);
            

            Id thisAccountId = findPerson();
            thisAccount = queryPerson(thisAccountId);
            
            Id thisIncidentId = findIncident();
            thisIntakeInvestigationEvent = queryIntakeInvestigationEvent(thisIncidentId);
              
        }
    }
    
    public EmailAlertsIIEController() {

    }
    
    public List<IntakeInvestigationEvent__c> getAllIncidentsForThisIntake() {
        List<IntakeInvestigationEvent__c> incidents;
        incidents = [SELECT Name, Location__c, StartDateTime__c, EndDateTime__c, Client__c, Intake__c, Intake__r.Name, Intake__r.Client__c, Catastrophic__c, IncidentInvestigation__c FROM IntakeInvestigationEvent__c WHERE Intake__c = :relatedIntakeId];        
        for(IntakeInvestigationEvent__c i: incidents){
            selectedInvestigation = i.IncidentInvestigation__c;
        }
        return incidents;
    }
    
    public List<IntakeInvestigationEvent__c> getAllIntakeInvestigationsForThisIncident() {
        List<IntakeInvestigationEvent__c> intakeEvents;
        intakeEvents = [SELECT ID, Name, Intake__c, Intake__r.Name, Intake__r.Client__C, Client__c, Location__c, StartDateTime__c FROM IntakeInvestigationEvent__c WHERE IncidentInvestigation__c =:selectedInvestigation AND Intake__c != :relatedIntakeId];
        return intakeEvents;
    }


    public String getFieldSet() {
        Intake__c dummyIntake = [SELECT Id, Case_Type__c FROM Intake__c WHERE Id = :relatedIntakeId];
        String relatedIntakeCT = dummyIntake.Case_Type__c;
        String fieldSetResult = null;

        Map<String,Case_Type_Field_Set_Mappings__c> ctCustomSettingMap = Case_Type_Field_Set_Mappings__c.getAll();
        Map<String, String> CTMap = new Map<String, String>();
        for(Case_Type_Field_Set_Mappings__c ctcs : ctCustomSettingMap.values()) {
            CTMap.put(ctcs.Case_Type__c, ctcs.Field_Set_Name__c);
        }

        fieldSetResult = CTMap.get(relatedIntakeCT);

        if(fieldSetResult != null) {
            return fieldSetResult;
        } else {
            return 'Email_Alerts_Default';
        }
    }

    //Get Fieldset Fields in Apex dynamically when Object Name and FieldSet name is supplied at runtime
    
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields();
    }
    
    public Intake__c queryIntake(String intakeFieldSetName) {
        List<Schema.FieldSetMember> fieldSetMemberList = readFieldSet(intakeFieldSetName, 'Intake__c');

        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, client__c from Intake__c WHERE Id = \''+relatedIntakeId+'\' LIMIT 1';
        return (Intake__c) queryFirstResult(query);
    }
    
    public Id findPerson() {
        List<Account> queriedAccount = [select id from Account where id = :thisIntake.client__c];
        if (queriedAccount.size() > 0) {
            return queriedAccount[0].Id;
        } else {
            //error could not query account from intake
            return null;
        }
    }

    public Account queryPerson(Id queriedAccount) {
        List<Schema.FieldSetMember> fieldSetMemberList = readFieldSet('General_Account_Info', 'Account');
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id from Account WHERE Id = \''+queriedAccount+'\'';
        return (Account) queryFirstResult(query);
    }
    
    public Id findIncident() {
        List<IntakeInvestigationEvent__c> queriedIncident = [SELECT IncidentInvestigation__c FROM IntakeInvestigationEvent__c WHERE Intake__c = :thisIntake.Id];
        if (queriedIncident.size() > 0) {
            return queriedIncident[0].IncidentInvestigation__c;
        } else {
            return null;
        }
    }
    
    public IntakeInvestigationEvent__c queryIntakeInvestigationEvent(Id queriedIncident) {
        List<Schema.FieldSetMember> fieldSetMemberList = readFieldSet('IntakeInvestigationEvent_Field_Set', 'IntakeInvestigationEvent__c');
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'IncidentInvestigation__c FROM IntakeInvestigationEvent__c WHERE IncidentInvestigation__c = \'' + queriedIncident + '\'';
        return (IntakeInvestigationEvent__c) queryFirstResult(query);
    }

    private static SObject queryFirstResult(String queryString) {
        SObject queryResult = null;
        try {
            queryResult = Database.query(queryString).get(0);
        }
        catch (Exception e) {
            System.debug(e);
        }
        return queryResult;
    }
}