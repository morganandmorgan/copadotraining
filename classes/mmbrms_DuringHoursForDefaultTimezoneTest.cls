@isTest
private class mmbrms_DuringHoursForDefaultTimezoneTest
{
    private static Profile standardUserProfile = null;
    private static User testUser = null;

    static void testSetup()
    {
        standardUserProfile = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        testUser = new User(
            Alias = 'joe', 
            Email='jslkdfklslkdflk@ftp.com', 
            EmailEncodingKey='UTF-8', 
            LastName='dflkslkdflsk', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = standardUserProfile.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='jslkdfklslkdflk@testorg.com');
    }

    @isTest
    static void NormalOperation_MarkDuringHours()
    {
        testSetup();

        System.runAs(testUser)
        {
            mock_BusinessHoursSelector mockBHSelector = 
                new mock_BusinessHoursSelector(
                    new BusinessHours(TimeZoneSidKey = 'America/New_York')
                );

            mm_Application.Selector.setMock(mockBHSelector);

            mmbrms_DuringHoursForDefaultTimezone rule = new mmbrms_DuringHoursForDefaultTimezone();
            rule.startTime = '8:30';
            rule.endTime = '18:00';
            rule.mock_mark = DateTime.newInstance(2017, 8, 29, 10, 30, 0);

            Boolean result = rule.evaluate();

            System.assertEquals(true, result);
        }
    }

    @isTest
    static void NormalOperation_MarkNotDuringHours()
    {
        testSetup();

        System.runAs(testUser)
        {
            mock_BusinessHoursSelector mockBHSelector = 
                new mock_BusinessHoursSelector(
                    new BusinessHours(TimeZoneSidKey = 'America/New_York')
                );

            mm_Application.Selector.setMock(mockBHSelector);

            mmbrms_DuringHoursForDefaultTimezone rule = new mmbrms_DuringHoursForDefaultTimezone();
            rule.startTime = '8:30';
            rule.endTime = '18:00';
            rule.mock_mark = DateTime.newInstance(2017, 8, 29, 18, 0, 0);

            Boolean result = rule.evaluate();

            System.assertEquals(false, result);
        }
    }

    @isTest
    static void NormalOperation_OverMidnight_MarkBeforeEndTime()
    {
        testSetup();

        System.runAs(testUser)
        {
            mock_BusinessHoursSelector mockBHSelector = 
                new mock_BusinessHoursSelector(
                    new BusinessHours(TimeZoneSidKey = 'America/New_York')
                );

            mm_Application.Selector.setMock(mockBHSelector);

            mmbrms_DuringHoursForDefaultTimezone rule = new mmbrms_DuringHoursForDefaultTimezone();
            rule.startTime = '18:30';
            rule.endTime = '6:00';
            rule.mock_mark = DateTime.newInstance(2017, 8, 29, 1, 0, 0);

            Boolean result = rule.evaluate();

            System.assertEquals(true, result);
        }
    }

    @isTest
    static void NormalOperation_OverMidnight_MarkAfterStartTime()
    {
        testSetup();

        System.runAs(testUser)
        {
            mock_BusinessHoursSelector mockBHSelector = 
                new mock_BusinessHoursSelector(
                    new BusinessHours(TimeZoneSidKey = 'America/New_York')
                );

            mm_Application.Selector.setMock(mockBHSelector);

            mmbrms_DuringHoursForDefaultTimezone rule = new mmbrms_DuringHoursForDefaultTimezone();
            rule.startTime = '18:30';
            rule.endTime = '6:00';
            rule.mock_mark = DateTime.newInstance(2017, 8, 29, 16, 0, 0);

            Boolean result = rule.evaluate();

            System.assertEquals(true, result);
        }
    }

    @isTest
    static void NormalOperation_OverMidnight_MarkNotDuringHours()
    {
        testSetup();

        System.runAs(testUser)
        {
            mock_BusinessHoursSelector mockBHSelector = 
                new mock_BusinessHoursSelector(
                    new BusinessHours(TimeZoneSidKey = 'America/New_York')
                );

            mm_Application.Selector.setMock(mockBHSelector);

            mmbrms_DuringHoursForDefaultTimezone rule = new mmbrms_DuringHoursForDefaultTimezone();
            rule.startTime = '18:30';
            rule.endTime = '6:00';
            rule.mock_mark = DateTime.newInstance(2017, 8, 29, 14, 0, 0);

            Boolean result = rule.evaluate();

            System.assertEquals(false, result);
        }
    }

    public class mock_BusinessHoursSelector
        implements mmcommon_IBusinessHoursSelector
    {
        public BusinessHours record = null;
        
        public mock_BusinessHoursSelector(BusinessHours hours)
        {
            record = hours;
        }

        public List<BusinessHours> selectDefault()
        {
            return new List<BusinessHours> { record };
        }

        // ----------------------------------
        public Schema.SObjectType sObjectType()
        {
            return BusinessHours.SObjectType;
        }

        public List<SObject> selectSObjectsById(Set<Id> idSet)
        {
            return new List<BusinessHours> {record};
        }

        public String selectSObjectsByIdQuery()
        {
            return null;
        }
    }
}