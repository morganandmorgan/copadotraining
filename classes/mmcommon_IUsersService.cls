public interface mmcommon_IUsersService {
    List<User> searchUsersUsingFuzzyMatching(Map<String, String> data);
}