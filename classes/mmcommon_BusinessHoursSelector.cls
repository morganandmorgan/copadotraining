public with sharing class mmcommon_BusinessHoursSelector
        extends mmlib_SObjectSelector
        implements mmcommon_IBusinessHoursSelector {
    public static mmcommon_IBusinessHoursSelector newInstance() {
        return (mmcommon_IBusinessHoursSelector) mm_Application.Selector.newInstance(BusinessHours.SObjectType);
    }

    private Schema.sObjectType getSObjectType() {
        return BusinessHours.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList() {
        return new List<Schema.SObjectField>{
        };
    }

    public List<BusinessHours> selectDefault() {
        return
                (List<BusinessHours>)
                        Database.query(
                                newQueryFactory()
                                        .setCondition(BusinessHours.IsActive + ' = true')
                                        .setCondition(BusinessHours.IsDefault + ' = true')
                                        .toSOQL()
                        );
    }

    public BusinessHours selectBusinessHoursByName(String name) {
        List<BusinessHours> businessHours = (List<BusinessHours>) Database.query(
                newQueryFactory()
                    .setCondition(BusinessHours.Name + ' = \'' + name + '\'')
                    .toSOQL()
                );

        return (businessHours != null && businessHours.size() > 0) ? businessHours[0] : null;
    }
}