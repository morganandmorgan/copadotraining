@isTest
private class mmmatter_MatterTeamMembersTest
{
    private static Id otherRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Other');
    private static Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
    private static List<litify_pm__Matter_Team_Role__c> roleList = new List<litify_pm__Matter_Team_Role__c>();

    static void commonSetup()
    {
        roleList.add(new litify_pm__Matter_Team_Role__c(Name = 'Case Developer'));
        roleList.add(new litify_pm__Matter_Team_Role__c(Name = 'Insignificant Role'));
        roleList.add(new litify_pm__Matter_Team_Role__c(Name = 'Principle Attorney'));
        roleList.add(new litify_pm__Matter_Team_Role__c(Name = 'Case Manager'));
        insert roleList;
        //roleList = [select Id, Name from litify_pm__Matter_Team_Role__c];
    }

    public static testMethod void InsertFullyRelevantRecord()
    {
        commonSetup();

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(0).Id;

        memberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterInsert();
    }

    public static testMethod void InsertNotSocialSecurityRecord()
    {
        commonSetup();

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = otherRecordTypeId;  // <<<<<<========== Not Social Security Matter
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(0).Id;

        memberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterInsert();
    }

    public static testMethod void InsertIrrelevantRoleRecord()
    {
        commonSetup();

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(1).Id;  // <========= Irrelevant Role

        memberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterInsert();
    }

    public static testMethod void UpdateFullyRelevantRecord()
    {
        commonSetup();

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(0).Id;

        memberMap.put(member.Id, member);

        member = member.clone(true, true, true, true);
        member.Id = memberMap.values().get(0).Id;
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(2).Id;

        existingMemberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterUpdate(memberMap);
    }

    public static testMethod void UpdateToRelevantRoleRecord()
    {
        commonSetup();

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(0).Id;

        memberMap.put(member.Id, member);

        member = member.clone(true, true, true, true);
        member.Id = memberMap.values().get(0).Id;
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(1).Id;

        existingMemberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterUpdate(memberMap);
    }

    public static testMethod void UpdateFromRelevantRoleRecord()
    {
        commonSetup();

        Map<Id, User> userMap = new Map<Id, User>();
        User u = new User();
        u.Id = fflib_IDGenerator.generate(User.SObjectType);
        userMap.put(u.Id, u);

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(1).Id;
        member.litify_pm__User__c = userMap.values().get(0).Id;

        memberMap.put(member.Id, member);

        member = member.clone(true, true, true, true);
        member.Id = memberMap.values().get(0).Id;
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(0).Id;
        member.litify_pm__User__c = userMap.values().get(0).Id;

        existingMemberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterUpdate(memberMap);
       }

    public static testMethod void DeleteFromRelevantRoleRecord()
    {
        commonSetup();

        Map<Id, User> userMap = new Map<Id, User>();
        User u = new User();
        u.Id = fflib_IDGenerator.generate(User.SObjectType);
        userMap.put(u.Id, u);

        // Create Matters
        Map<Id, litify_pm__Matter__c> matterMap = new Map<Id, litify_pm__Matter__c>();

        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.Id = fflib_IDGenerator.generate(litify_pm__Matter__c.SObjectType);

        matterMap.put(matter.Id, matter);

        // Create Team Member records
        Map<Id, litify_pm__Matter_Team_Member__c> memberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();
        Map<Id, litify_pm__Matter_Team_Member__c> existingMemberMap = new Map<Id, litify_pm__Matter_Team_Member__c>();

        litify_pm__Matter_Team_Member__c member = new litify_pm__Matter_Team_Member__c();
        member.Id = fflib_IDGenerator.generate(litify_pm__Matter_Team_Member__c.SObjectType);
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(1).Id;
        member.litify_pm__User__c = userMap.values().get(0).Id;

        memberMap.put(member.Id, member);

        member = member.clone(true, true, true, true);
        member.Id = memberMap.values().get(0).Id;
        member.litify_pm__Matter__c = matter.Id;
        member.litify_pm__Role__c = roleList.get(0).Id;
        member.litify_pm__User__c = userMap.values().get(0).Id;

        existingMemberMap.put(member.Id, member);

        // Mock selectors
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        mmmatter_IMattersSelector mockMatterSelector = (mmmatter_IMattersSelector) mocks.mock(mmmatter_IMattersSelector.class);

        mocks.startStubbing();

        mocks.when(mockMatterSelector.sObjectType()).thenReturn(litify_pm__Matter__c.SObjectType);
        mocks.when(mockMatterSelector.selectById(matterMap.keyset())).thenReturn(matterMap.values());

        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockMatterSelector);
        new mmmatter_MatterTeamMembers(memberMap.values()).onAfterDelete();
    }
}