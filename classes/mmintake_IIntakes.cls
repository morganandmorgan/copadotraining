public interface mmintake_IIntakes extends mmlib_ISObjectDomain
{
    void setApprovingAttorneys();
    void setGeneratingAttorney();
    void setSolStartDates();
    void setCatastrophic();
    void setSignUpMethod();
    void validateForInitialIntakeCall();
    void updateOutboundDialCounts( Map<Id, List<Task>> intakeIdToTasksMap, fflib_ISObjectUnitOfWork uow );
}