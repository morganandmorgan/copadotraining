/**
 *  Domain criteria used to determine if the User is an investigator
 */
public class mmcommon_UserIsInvstgtrChngdCriteria
    implements mmlib_ICriteriaWithExistingRecords
{
    private list<User> records = new list<User>();
    private Map<Id, User> existingUserRecordsMap = new Map<Id, User>();

    public mmlib_ICriteria setExistingRecords( Map<Id, SObject> existingRecords )
    {
        this.existingUserRecordsMap.clear();
        this.existingUserRecordsMap.putAll( (Map<Id, User>)existingRecords );

        return this;
    }

    public mmlib_ICriteria setRecordsToEvaluate(List<SObject> records)
    {
        this.records.clear();
        this.records.addAll( (list<User>)records );

        return this;
    }

    public List<SObject> run()
    {
        list<User> qualifiedRecords = new list<User>();

        // Loop through the User records.
        for ( User record : this.records )
        {
            if ( record.id != null
                && existingUserRecordsMap.containsKey( record.id )
                && mmcommon_PersonUtils.isInvestigator( record )
                && ! mmcommon_PersonUtils.isInvestigator( existingUserRecordsMap.get( record.id) )
                )
            {
                qualifiedRecords.add( record );
            }
            else if ( mmcommon_PersonUtils.isInvestigator( record )
                    && ! existingUserRecordsMap.containsKey( record.id ) // if the record was not seen previously, then it is a new record.
                    )
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}