/**
 *  mmcommon_AccountsSelectorPersonRequest
 */
public class mmcommon_AccountsSelectorPersonRequest
    implements mmcommon_IAccountsSelectorPersonRequest
{
    private string firstName = null;
    private string lastName = null;
    private string email = null;
    private string phoneNumber = null;
    private Date birthDate = null;
    private String ssn_last4 = null;

    private mmcommon_AccountsSelectorPersonRequest()
    {

    }

    public static mmcommon_IAccountsSelectorPersonRequest newInstance()
    {
        return new mmcommon_AccountsSelectorPersonRequest();
    }

    public mmcommon_IAccountsSelectorPersonRequest setFirstName( String firstName )
    {
        this.firstName = firstName;
        return this;
    }

    public mmcommon_IAccountsSelectorPersonRequest setLastName( String lastName )
    {
        this.lastName = lastName;
        return this;
    }

    public mmcommon_IAccountsSelectorPersonRequest setEmail( String email )
    {
        this.email = email;
        return this;
    }

    public mmcommon_IAccountsSelectorPersonRequest setPhoneNumber( String phoneNumber )
    {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public mmcommon_IAccountsSelectorPersonRequest setBirthDate( Date birthDate )
    {
        this.birthDate = birthDate;
        return this;
    }

    public mmcommon_IAccountsSelectorPersonRequest setSsn_Last4( String ssn_Last4 )
    {
        this.ssn_last4 = ssn_Last4;
        return this;
    }

    public String getFirstName()
    {
        return this.firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getPhoneNumber()
    {
        return this.phoneNumber;
    }

    public Date getBirthDate()
    {
        return this.birthDate;
    }

    public String getSsn_Last4()
    {
        return ssn_last4;
    }
}