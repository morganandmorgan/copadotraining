/**
 * Matter_Domain
 * @description Domain class for Matter SObject.
 * @author Jeff Watson
 * @date 3/14/2019
 */

public with sharing class Matter_Domain extends fflib_SObjectDomain {

    private List<litify_pm__Matter__c> matters;

    // Ctors
    public Matter_Domain() {
        super();
        this.matters = new List<litify_pm__Matter__c>();
    }

    public Matter_Domain(List<litify_pm__Matter__c> matters) {
        super(matters);
        this.matters = (List<litify_pm__Matter__c>) this.Records;
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new Matter_Domain(records);
        }
    }

    // Trigger Handlers
    public override void onAfterInsert() {
        //syncIntakeDocuments();
        syncIntakeDocumentsToLitify();
        AuditProcessor.triggerHandler(records, null);        
    }
    
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        syncOfficeToIntake(existingRecords);
        AuditProcessor.triggerHandler(records, existingRecords);
    }
    
    /* Methods
    Documents are now migrated to Litify/Docrio
    private void syncIntakeDocuments() {
        // Make a call to an implementation class like SpringCM_Service.cls or something here
        // The "scope" is in this.Records
        System.debug('Matters in trigger scope? ' + this.Records.size());
        System.debug( matters );
        Set<Id> filteredIdSet = new Set<Id>();
        for( litify_pm__Matter__c matter : ( List<litify_pm__Matter__c> ) this.Records ) {
            // filter the list where Intake__c in not NULL
            if( String.isNotEmpty( matter.Intake__c ) ) {
                filteredIdSet.add( matter.Id );
            }
        }
        // make the first callout
        if( !filteredIdSet.isEmpty() ) {
            SpringCMApiManager.fetchAttachments( filteredIdSet );
        }
    }*/

    private void syncIntakeDocumentsToLitify() {
        Set<Id> matterIds = new Set<Id>();
        for ( litify_pm__Matter__c matter : ( List<litify_pm__Matter__c> ) this.Records ) {
            if (matter.send_to_Litify_Docs__c) {
                matterIds.add( matter.id );
            }
        }

        if( !matterIds.isEmpty() ) {
            DocsApi.migrateIntakeDocuments( matterIds );
        }
    } //syncIntakeDocumentsToLitify
    
    
    //after,update
    private void syncOfficeToIntake(Map<Id, SObject> existingRecords) {

        //avoidance
        Set<Id> intakeIds = new Set<Id>();
        List<litify_pm__Matter__c> syncMatters = new List<litify_pm__Matter__c>();
        Date d60DaysAgo = Date.today().addDays(-60);
        for (litify_pm__Matter__c matter : (List<litify_pm__Matter__c>)this.records) {
            litify_pm__Matter__c oldRecord = (litify_pm__Matter__c)existingRecords.get(matter.id);
            if (matter.litify_pm__Open_Date__c >= d60DaysAgo && 
                    matter.assigned_office_location__c != oldRecord.assigned_office_location__c) {
                intakeIds.add(matter.intake__c);
                syncMatters.add(matter);
            } //if field changed
        } //for matters

        if (syncMatters.size() != 0) {

            mmintake_IntakesSelector intakeSelector = new mmintake_IntakesSelector();
            Map<Id,Intake__c> intakeMap = new Map<Id,Intake__c>(intakeSelector.selectById(intakeIds));

            for (litify_pm__Matter__c matter : syncMatters) {
                litify_pm__Matter__c oldRecord = (litify_pm__Matter__c)existingRecords.get(matter.id);
                if (matter.assigned_office_location__c != oldRecord.assigned_office_location__c) {
                    intakeMap.get(matter.intake__c).assigned_office__c = matter.assigned_office_location__c;
                } //if office changed
            } //for syncMatters

            update intakeMap.values();
        } //if avoidance

    } //syncOfficeToIntake


    //support method
    private Id findContact(User matterUser, List<Contact> contacts, String title) {
        if (matterUser == null) return null;

        List<Contact> emailMatch = new List<Contact>();
        //get a list of contacts that match on email
        for (Contact cc : contacts) {
            if (matterUser.email.equalsIgnoreCase(cc.email)
                && (title == null || title == cc.title) //for the attorney match, title must be 'Attorney'
            ) {
                emailMatch.add(cc);
            }
        }

        //if we did not find any or just one...
        if (emailMatch.size() == 0) return null;
        if (emailMatch.size() == 1) return emailMatch[0].id;

        //go through the emailMatches matching on last name
        for (Contact cc : emailMatch) {
            if (matterUser.lastname.equalsIgnoreCase(cc.lastname)) return cc.id;
        }

        //if we got this far, we didn't find anything
        return null;
    } //findContact

    //called from a scheduled job, uses injected this.matters
    public void syncPersonnelToIntake() {
        List<litify_pm__Matter__c> syncMatters = new List<litify_pm__Matter__c>();
        Set<Id> intakeIds = new Set<Id>();
        Set<id> userIds = new Set<Id>();
        Date d60DaysAgo = Date.today().addDays(-60);
        for (litify_pm__Matter__c matter : this.matters) {
            if (matter.litify_pm__Open_Date__c >= d60DaysAgo) {
                intakeIds.add(matter.intake__c);
                syncMatters.add(matter);
                if (matter.litify_pm__Principal_Attorney__c != null) {
                    userIds.add(matter.litify_pm__Principal_Attorney__c);                
                } 
                if (matter.case_Manager__c != null) {
                    userIds.add(matter.case_Manager__c);
                }
            }
        }

        if (syncMatters.size() != 0) {

            Map<Id,User> userMap;
            List<Contact> contacts;
            if (userIds.size() != 0) {
                userMap = new Map<Id,User>([SELECT id, lastname, email FROM User WHERE id IN :userIds]);
                Set<String> userEmails = new Set<String>();
                for (User u : userMap.values()) {
                    userEmails.add(u.email);
                }
                contacts = [SELECT id, lastname, email, title, account.name FROM Contact WHERE email IN :userEmails AND account.name = 'Morgan & Morgan P.A.'];
            }

            mmintake_IntakesSelector intakeSelector = new mmintake_IntakesSelector();
            Map<Id,Intake__c> intakeMap = new Map<Id,Intake__c>(intakeSelector.selectById(intakeIds));

            for (litify_pm__Matter__c matter : syncMatters) {
                if (matter.litify_pm__Principal_Attorney__c != null) {
                    Id attorneyId = findContact(userMap.get(matter.litify_pm__Principal_Attorney__c), contacts, 'Attorney');
                    if (attorneyId != null) intakeMap.get(matter.intake__c).assigned_attorney__c = attorneyId;
                }
                if (matter.case_Manager__c != null) {
                    Id cmId = findContact(userMap.get(matter.case_Manager__c), contacts, null);
                    if (cmId != null) intakeMap.get(matter.intake__c).case_Manager_name__c = cmId;
                }
            } //for syncMatters

            update intakeMap.values();
        } //if avoidance

    } //syncPersonnelToIntake


} //class