@isTest
public class mmintake_EmailAlertsControllerTest {

    //also covers: mmintake_EmailAlertsInfo.cls

    @testSetup
    public static void setup() {
    } //setup

    @isTest
    public static void testController() {

        Incident__c incident = TestUtil.createIncident();
        insert incident;

        Intake__c intake = TestUtil.createIntake();
        intake.incident__c = incident.id;
        insert intake;

        IncidentInvestigationEvent__c incIe = TestUtil.createIncidentInvestigationEvent(incident);
        insert incIe;

        IntakeInvestigationEvent__c intIe = TestUtil.createIntakeInvestigationEvent(intake, incIe);
        insert intIe;

        mmintake_EmailAlertsController controller = new mmintake_EmailAlertsController();

        controller.relatedIntakeId = intake.id;
        controller.relatedIncidentInvestigationEventId = incIe.id;
        controller.relatedIntakeInvestigationEventId = intIe.id;
        controller.relatedIncidentId = incident.id;

        //extra coverage for mmintake_EmailAlertsInfo
        controller.info.getBaseUrl();
        controller.info.getDefaultFieldsetMemberNonEmpty();
        controller.info.getIntakeFieldsetMemberNonEmpty();
        controller.info.getMarketingInfoFieldsetMemberNonEmpty();

    } //testController


} //class