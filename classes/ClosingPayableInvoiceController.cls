/*============================================================================
Name            : ClosingPayableInvoiceController
Author          : CLD
Created Date    : June 2019
Description     : Controller class for creating payable invoices from a matter for closing activity.
=============================================================================*/
public class ClosingPayableInvoiceController {
    
    // ------------------------------------------------------------------------
    // Variables:
    // ------------------------------------------------------------------------
	public static Date pinDate {get;set;}
	public static Id companyId {get;set;}
    public static String currentCompanyName {get;set;}
	//public List<String> bankAccountNames {get;set;}
    
    // ------------------------------------------------------------------------
    // Constructor:
    // ------------------------------------------------------------------------
    public ClosingPayableInvoiceController(){
    }
    public ClosingPayableInvoiceController(ApexPages.StandardController stdController){

		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if(companies != null && !companies.isEmpty()){
            companyId = companies.get(0).id;
            currentCompanyName = companies.get(0).Name;
        }

        //get the related bank accounts to that company:
        // bankAccountNames = new List<String>();
        // for(c2g__codaBankAccount__c ba : [SELECT Name FROM c2g__codaBankAccount__c
        // 	WHERE c2g__OwnerCompany__c = :companyId AND Bank_Account_Type__c = 'Trust']){
        // 	bankAccountNames.add(ba.Name);
        // }
    }

    //-------------------------------------------------
    // Wrapper class for the SF exception
    //-------------------------------------------------
    public class applicationException extends Exception {}

    // ------------------------------------------------------------------------
    // Wrapper for results returned from remote actions
    // ------------------------------------------------------------------------
    public class ActionResult {
        public Object[] records {get; set;}
        public Map<String, Object> response {get; set;}

        // optional batch id when generating...
        public String batchId {get; set;}

        public ActionResult() {
            this.records = new Object[]{};
            this.response = new Map<String, Object>();
        }
    }

    //-------------------------------------------------
    // Wrapper class for the query filter payload from the page
    //-------------------------------------------------
    public class QueryRequest {
        public String matterId {get; set;}
    }

	//-------------------------------------------------
    // Wrapper class for the various records that will be used as a source for PINs
    //-------------------------------------------------
	public class ClosingTransactionWrapper {
		public String type {get; set;}
		public Boolean isSelected {get; set;}
		public Decimal amountToPay {get; set;}
		public String accountToPayName {get; set;}
		public Id accountToPayId {get; set;}
		public String overrideAccountToPayName {get; set;}
		public String sourceRecordName {get; set;}
		public Id sourceRecordId {get; set;}
		public String checkMemo {get; set;}
        public litify_pm__Damage__c damageObj {get; set;}
		public Finance_Request__c financeRequestObj {get; set;}
		public Lien_LOP__c LienLopObj {get; set;}

		public ClosingTransactionWrapper(){
            checkMemo = '';
        }
		
    }
    //-------------------------------------------------
    // Wrapper class for account search queries.
    //-------------------------------------------------
    public class AccountSearchResult {
		public String accountName {get; set;}
		public Id accountId {get; set; }
        public String address{get;set;}

		public AccountSearchResult() {}
		
    }

    //-------------------------------------------------
    // Wrapper class for the Transaction Line Item Payload from the page
    //-------------------------------------------------
    public class PayableInvoiceRequest {
        public List<ClosingTransactionWrapper> wrappersToProcess {get; set;}
		public Date pinDate {get; set;}
		public Date pinDueDate {get; set;}
        public Boolean autoPostInvoice {get;set;}
        //public String selectedBankAccountName {get;set;}
    }

    // ------------------------------------------------------------------------
    // Queries the eligible transactions and in progress payments related to the Payment Collection
    // ------------------------------------------------------------------------
    @RemoteAction
    public static ActionResult fetchEligibleTranscations(String matterId) {
        //parse the list of filter fields
    	//QueryRequest queryFilters = (QueryRequest)JSON.deserialize(jsonPayload_Filters, ClosingPayableInvoiceController.QueryRequest.class);
        ActionResult ar = queryEligibleTranscations(matterId);
        return ar;
    }

    // ------------------------------------------------------------------------
    // Queries for the active accounts to associate to this matter
    // ------------------------------------------------------------------------
    @RemoteAction
    public static ActionResult searchForAccounts(String searchTerm)
    {

        ActionResult ar = queryActiveAccounts(searchTerm);
        return ar;
    }

    // ------------------------------------------------------------------------
    // Queries for the active accounts to associate to this matter
    // ------------------------------------------------------------------------
    public static ActionResult queryActiveAccounts(String searchTerm)
    {
        AccountSearchResult[] records = new AccountSearchResult[]{};
        Map<String, Object> resultsMap = new Map<String, Object>();

        searchTerm = '%' + searchTerm + '%';
        for (Account acc: [SELECT Id, Name, BillingStreet, BillingCity, BillingState FROM Account WHERE Name LIKE :searchTerm 
        AND Vendor_Verification__c = 'Verified'
        LIMIT 10])
        {
            AccountSearchResult result = new AccountSearchResult();
            result.accountId = acc.Id;
            result.accountName = acc.Name;
            result.address = (acc.BillingStreet != null ? acc.BillingStreet + ' ' : '') + 
                (acc.BillingCity != null ? acc.BillingCity + ', ' : '') + (acc.BillingState != null ? acc.BillingState : '');
		    resultsMap.put('isSuccess', true);

            records.add(result); 
        }
        ActionResult actionResult = new ActionResult();
        actionResult.records = records;
        return actionResult;

    }
    // ------------------------------------------------------------------------
    // Queries the eligible payable transaction line items
    // ------------------------------------------------------------------------
    public static ActionResult queryEligibleTranscations(String matterId) {

        ClosingTransactionWrapper[] records = new ClosingTransactionWrapper[]{};

        //call the query of Transcaction Lines
        litify_pm__Matter__c matter = fetchMatterWithRelatedItems(matterId);
        
        //Create wrappers related to Damages
        for(litify_pm__Damage__c damage : matter.litify_pm__Damages__r){
            ClosingTransactionWrapper closingWrapper = new ClosingTransactionWrapper();
            closingWrapper.isSelected = true;
            closingWrapper.type = 'Damage - ' + damage.litify_pm__Type__c;
			closingWrapper.damageObj = damage;
			closingWrapper.sourceRecordName = damage.Name;
			closingWrapper.sourceRecordId = damage.Id;
            closingWrapper.amountToPay = damage.litify_pm__Amount_Due__c;
			closingWrapper.accountToPayId = damage.litify_pm__Provider__r.litify_pm__Party__c;
			closingWrapper.accountToPayName = damage.litify_pm__Provider__r.litify_pm__Party__r.Name;
			closingWrapper.checkMemo = damage.Account__c != null && damage.litify_pm__Invoice__c != null ? damage.Account__c +' - '+ damage.litify_pm__Invoice__c : '';
			closingWrapper.checkMemo = closingWrapper.checkMemo.length()> 80 ? closingWrapper.checkMemo.subString(0,79) : closingWrapper.checkMemo;
            
			records.add(closingWrapper);
        }

		//Create wrappers related to Finance Rquests
        for(Finance_Request__c financeRequest : matter.Finance_Requests__r){
            ClosingTransactionWrapper closingWrapper = new ClosingTransactionWrapper();
            closingWrapper.isSelected = true;
            closingWrapper.type = 'Proceeds';
			closingWrapper.financeRequestObj = financeRequest;
			closingWrapper.sourceRecordName = financeRequest.Name;
			closingWrapper.sourceRecordId = financeRequest.Id;
            closingWrapper.amountToPay = financeRequest.Settlement_Proceeds_Amount_f__c;
            closingWrapper.accountToPayId = matter.litify_pm__Client__c;
			closingWrapper.accountToPayName = matter.litify_pm__Client__r.Name;
            records.add(closingWrapper);
        }

		//Create wrappers related to Liens
        for(Lien_LOP__c lienLop : matter.Liens_LOPs__r){
            ClosingTransactionWrapper closingWrapper = new ClosingTransactionWrapper();
            closingWrapper.isSelected = true;
            closingWrapper.type = lienLop.Type__c;
			closingWrapper.LienLopObj = lienLop;
			closingWrapper.sourceRecordName = lienLop.Name;
			closingWrapper.sourceRecordId = lienLop.Id;
            closingWrapper.amountToPay = lienLop.Actual_Amount__c;
			closingWrapper.accountToPayId = lienLop.Account_Role__r.litify_pm__Party__c;
			closingWrapper.accountToPayName = lienLop.Account_Role__r.litify_pm__Party__r.Name;
	
            records.add(closingWrapper);
        }

        System.debug(LoggingLevel.WARN, '***FETCHING LINES records.size = ' + records.size());

        ActionResult actionResult = new ActionResult();
        actionResult.records = records;
        return actionResult;
    }

    // ------------------------------------------------------------------------
    // Step 1 includes creating the payment proposals / adding items to those proposals / creating payment media
    // ------------------------------------------------------------------------
    @RemoteAction
    public static Map<String, Object> createPayableInvoices(String jsonPayload_Transactions, String matterObjId) {
        ClosingTransactionWrapper[] records = new ClosingTransactionWrapper[]{};

        //parse the list of filter fields
        PayableInvoiceRequest closingPinRequest = (PayableInvoiceRequest)JSON.deserialize(jsonPayload_Transactions, PayableInvoiceRequest.class);

        //query the matter based on the Id provided
        litify_pm__Matter__c matter =fetchMatterDetails(matterObjId);

        //call the create PIN method
        return createPayableInvoices(closingPinRequest, matter);
    }

	public static Map<String, Object> createPayableInvoices(ClosingPayableInvoiceController.PayableInvoiceRequest pinRequest, litify_pm__Matter__c matter){
		Map<String, Object> resultsMap = new Map<String, Object>();

		List<litify_pm__Damage__c>          damageUpdateList     = new List<litify_pm__Damage__c>();
		List<Finance_Request__c> finRequestUpdateList = new List<Finance_Request__c>();
		List<Lien_LOP__c>        lienLopUpdateList    = new List<Lien_LOP__c>();

		Map<String,Id> currencyMap = new Map<String,Id>();
		Map<String, c2g__codaPurchaseInvoice__c> pinMap = new Map<String, c2g__codaPurchaseInvoice__c>();
		List<c2g__codaPurchaseInvoiceExpenseLineItem__c> pinLinesToInsert = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();

		List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if(companies != null && !companies.isEmpty()){
            companyId = companies.get(0).id;
            currentCompanyName = companies.get(0).Name;
        }

		//get the accounting currencies
        for(c2g__codaAccountingCurrency__c acctCurr : [SELECT Id, Name FROM c2g__codaAccountingCurrency__c WHERE c2g__OwnerCompany__c = :companyId]){
        	currencyMap.put(acctCurr.Name, acctCurr.Id);
        }
        System.debug(LoggingLevel.WARN, '*** currencyMap size = '+ currencyMap.size());

		for(ClosingTransactionWrapper wrapper : pinRequest.wrappersToProcess){

			//determine key for adding to the map - id of the source record
			String key = wrapper.damageObj != null ? String.valueOf(wrapper.damageObj.Id) : wrapper.LienLopObj != null ? String.valueOf(wrapper.LienLopObj.Id) : wrapper.financeRequestObj != null ? String.valueOf(wrapper.financeRequestObj.Id) : '';

			String invoiceNumber = wrapper.LienLopObj != null ? wrapper.LienLopObj.Name : wrapper.financeRequestObj != null ? wrapper.financeRequestObj.Name : '';
			if(wrapper.damageObj != null){
				invoiceNumber = wrapper.damageObj.litify_pm__Invoice__c != null ? wrapper.damageObj.litify_pm__Invoice__c : Datetime.now().format();
			}
			invoiceNumber = invoiceNumber.length() > 80 ? invoiceNumber.subString(0,79) : invoiceNumber;

			//add the source records to individual lists
			if(wrapper.damageObj != null){
				damageUpdateList.add(wrapper.damageObj);
			}
			if(wrapper.LienLopObj != null){
				lienLopUpdateList.add(wrapper.LienLopObj);
			}
			if(wrapper.financeRequestObj != null){
				finRequestUpdateList.add(wrapper.financeRequestObj);
			}

			Id requestedById = matter.Finance_Requests__r == null || matter.Finance_Requests__r.isEmpty() ? null : matter.Finance_Requests__r[0].CreatedbyId;

			//now create the PIN header / PIN lines:
			c2g__codaPurchaseInvoice__c newPIN = new c2g__codaPurchaseInvoice__c(
				Linking_Key__c               = key,
				c2g__OwnerCompany__c         = matter.AssignedToMMBusiness__r.FFA_Company__c,
				c2g__Dimension1__c           = matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
				c2g__Dimension2__c           = matter.litify_pm__Case_Type__r.Dimension_2__c,
				c2g__Dimension3__c           = matter.litify_pm__Case_Type__r.Dimension_3__c,
				OwnerId                      = matter.AssignedToMMBusiness__r.FFA_Company__r.OwnerId,
				Override_Account_Name__c     = wrapper.overrideAccountToPayName,
				Litify_Matter__c             = matter.Id,
				c2g__InvoiceCurrency__c      = currencyMap.containsKey('USD') ? currencyMap.get('USD') : null,
				c2g__Account__c              = wrapper.accountToPayId,
				c2g__AccountInvoiceNumber__c = invoiceNumber,
				c2g__InvoiceDescription__c   = 'Trust Payments from closing statement: '+ matter.ReferenceNumber__c,
				c2g__DeriveCurrency__c       = true,
				c2g__DerivePeriod__c         = true,
				Finance_Request__c           = wrapper.financeRequestObj != null ? wrapper.financeRequestObj.Id : null,
				Damage__c                    = wrapper.damageObj != null ? wrapper.damageObj.Id : null,
				Lien_LOP__c                  = wrapper.LienLopObj != null ? wrapper.LienLopObj.Id : null,
				Check_Memo__c                = wrapper.checkMemo,
				Payment_Bank_Account_Type__c = 'Trust',
				c2g__DeriveDueDate__c        = false,
				c2g__InvoiceDate__c          = pinRequest.pinDate,
				c2g__DueDate__c              = pinRequest.pinDueDate,
				Requested_By__c 			 = requestedById
			);
			pinMap.put(key, newPIN);
			
			c2g__codaPurchaseInvoiceExpenseLineItem__c expLine = new c2g__codaPurchaseInvoiceExpenseLineItem__c(
				Linking_Key__c               = key,
				c2g__GeneralLedgerAccount__c = matter.AssignedToMMBusiness__r.FFA_Company__r.Contra_Trust_GLA__c,
				c2g__LineDescription__c      = 'Settlement Payout for '+ wrapper.type +' on  Matter '+matter.ReferenceNumber__c,
				Matter__c                    = matter.Id,
				c2g__Dimension1__c           = matter.Assigned_Office_Location__r.c2g__codaDimension1__c,
				c2g__Dimension2__c           = matter.litify_pm__Case_Type__r.Dimension_2__c,
				c2g__Dimension3__c           = matter.litify_pm__Case_Type__r.Dimension_3__c,
				c2g__NetValue__c             = wrapper.amountToPay
			);
			pinLinesToInsert.add(expLine);
        }

		Savepoint sp = Database.setSavepoint();
        try{
			//now insert the pins and pin lines
			insert pinMap.values();
			for(c2g__codaPurchaseInvoiceExpenseLineItem__c pinLine : pinLinesToInsert){
				pinLine.c2g__PurchaseInvoice__c = pinMap.containsKey(pinLine.Linking_Key__c) ? pinMap.get(pinLine.Linking_Key__c).Id : null;
			}
			insert pinLinesToInsert;

			//update the source records;
			for(litify_pm__Damage__c d : damageUpdateList){
				String key = String.valueOf(d.Id);
				d.Payable_Invoice__c = pinMap.containsKey(key) ? pinMap.get(key).Id : null;
				d.Payable_Invoice_Created__c = true;
			}
			update damageUpdateList;

			for(Lien_LOP__c l : lienLopUpdateList){
				String key = String.valueOf(l.Id);
				l.Payable_Invoice__c = pinMap.containsKey(key) ? pinMap.get(key).Id : null;
				l.Payable_Invoice_Created__c = true;
			}
			update lienLopUpdateList;

			for(Finance_Request__c f : finRequestUpdateList){
				String key = String.valueOf(f.Id);
				f.Payable_Invoice__c = pinMap.containsKey(key) ? pinMap.get(key).Id : null;
				f.Payable_Invoice_Created__c = true;
			}
			update finRequestUpdateList;

			//Post the invoice if instructed to:
            List<Id> pinIds = new List<Id>();
            if(pinRequest.autoPostInvoice == true){
            	for(c2g__codaPurchaseInvoice__c pin : pinMap.values()){
            		pinIds.add(pin.Id);
            	}	
                List<c2g__codaPurchaseInvoice__c> resultList = FFAUtilities.postPINs(pinIds);
            }
			
			//populate the ActionResult
			String successMessage = 'Payable Invoices Created!';
			resultsMap.put('isSuccess', true);
			resultsMap.put('successMessage', successMessage);

        }catch (Exception e){
            System.debug(LoggingLevel.WARN, ' *** processExpensesController - ERROR = '+e.getMessage() + 'STACK: '+ e.getStackTraceString());
            String errorMsg = e.getMessage();
			resultsMap.put('isError', true);
            resultsMap.put('errorMsg', errorMsg);
            Database.rollback(sp);
            throw e;
        }
		return resultsMap;
	}

	public static litify_pm__Matter__c fetchMatterWithRelatedItems (String matterId){
		return [SELECT Id,
			Name,
			litify_pm__Client__c,
			Case_Manager__c,
			litify_pm__Client__r.Name,
			ReferenceNumber__c,
			AssignedToMMBusiness__r.Name,
			AssignedToMMBusiness__r.FFA_Company__c,
			AssignedToMMBusiness__r.FFA_Company__r.OwnerId,
			AssignedToMMBusiness__r.FFA_Company__r.Contra_Trust_GLA__c,
			Assigned_Office_Location__r.c2g__codaDimension1__c,
			litify_pm__Case_Type__r.Dimension_2__c,
			litify_pm__Case_Type__r.Dimension_3__c,
			(SELECT 
				Id,
				Name,
				Payable_Invoice_Created__c,
				Payable_Invoice__c,
				litify_pm__Amount_Due__c,
				litify_pm__Provider__r.litify_pm__Party__c,
				litify_pm__Provider__r.litify_pm__Party__r.Name,
				litify_pm__Type__c,
				litify_pm__Invoice__c,
				Account__c
			FROM litify_pm__Damages__r
			WHERE Payable_Invoice_Created__c = false
			AND Do_Not_Pay__c = false
			AND litify_pm__Amount_Due__c != 0),
			(SELECT 
				Id,
				Name,
				Payable_Invoice_Created__c,
				Payable_Invoice__c,
				Settlement_Proceeds_Amount_f__c,
				Proceed_Check_Issued_To__c
			FROM Finance_Requests__r
			WHERE Payable_Invoice_Created__c = false
			AND RecordType.Name = 'Closing Statement Request'),
			(SELECT 
				Id,
				Name,
				Payable_Invoice_Created__c,
				Payable_Invoice__c,
				Actual_Amount__c,
				Type__c,
				Account_Role__r.litify_pm__Party__c,
				Account_Role__r.litify_pm__Party__r.Name
			FROM Liens_LOPs__r
			WHERE Payable_Invoice_Created__c = false)
		FROM litify_pm__Matter__c
		WHERE Id = :matterId
		LIMIT 1];
	
	}

	public static litify_pm__Matter__c fetchMatterDetails (Id matterId){
		return [
		SELECT Id,
			Name,
			Case_Manager__c,
			litify_pm__Client__c,
			litify_pm__Client__r.Name,
			ReferenceNumber__c,
			AssignedToMMBusiness__r.Name,
			AssignedToMMBusiness__r.FFA_Company__c,
			AssignedToMMBusiness__r.FFA_Company__r.OwnerId,
			AssignedToMMBusiness__r.FFA_Company__r.Contra_Trust_GLA__c,
			Assigned_Office_Location__r.c2g__codaDimension1__c,
			litify_pm__Case_Type__r.Dimension_2__c,
			litify_pm__Case_Type__r.Dimension_3__c,
			(SELECT Id, CreatedBy.Id 
			FROM Finance_Requests__r 
			WHERE RecordType.Name = 'Closing Statement Request'
			ORDER BY CreatedDate desc
			LIMIT 1)
		FROM litify_pm__Matter__c
		WHERE Id = :matterId
		LIMIT 1];
	
	}

    // --------------------------------------------------------------------------
    // -- Get details from the matter and validate the user's current company
    // --------------------------------------------------------------------------
    @RemoteAction
    public static Map<String, Object> fetchMatterDetailsAndValidate (String matterObjId) {

		//fetch current company Id
        Id companyId = null;
        List<c2g__codaCompany__c> companies = FFAUtilities.getCurrentCompanies();
        if(companies != null && !companies.isEmpty()){
            companyId = companies.get(0).id;
        }

		String errorMsg = '';

        Map<String, Object> response = new Map<String, Object>();
        litify_pm__Matter__c matterObj = fetchMatterDetails(matterObjId);

		errorMsg = matterObj.AssignedToMMBusiness__r.FFA_Company__c == null ? 'The matter is not related to an MM Business' : '';
		errorMsg = companyId == null ? 'You must be logged into the company related to the matter in order to continue' : '';
		errorMsg = companies.size() > 1 ? 'You must be logged into the company related to the matter in order to continue' : '';
		errorMsg = matterObj.AssignedToMMBusiness__r.FFA_Company__c != companyId  ? 'You must be logged into the company related to the matter in order to continue' : '';

		if(errorMsg != ''){
			response.put('isError', true);
			response.put('errorMsg', errorMsg);	
		}	
		else{
			response.put('isError', false);
		}
        response.put('matterObjNumber', matterObj.Name);
        response.put('matterObjId', matterObj.Id);

        return response;
    }




}