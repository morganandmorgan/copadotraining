/**
 * Trigger_Helper
 * @description Provides helper methods for Triggers including Trigger Settings.
 * @date 5/10/2019 10K Initial
 * @date 5/14/2019 Jeff Revised and ported to Trigger Helper class and added code coverage.
 */

public with sharing class Trigger_Helper {

    public static Boolean isFirstRun = true;

    public static Boolean isDisabled(String objAPI_Name) {
        Triggers_Setting__c triggerSetting = Triggers_Setting__c.getInstance();
        Set<String> objectsAPI_Set = new Set<String>();
        if (String.isNotBlank(triggerSetting.Object_API_Name__c)) {
            for (String apiName : triggerSetting.Object_API_Name__c.split(',')) {
                objectsAPI_Set.add(apiName.trim());
            }
        }

        Boolean isTriggerDisabled = false;
        if (objectsAPI_Set.contains(objAPI_Name)) {
            isTriggerDisabled = true;
        }

        return isTriggerDisabled;
    }
}