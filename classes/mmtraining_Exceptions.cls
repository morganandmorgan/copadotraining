public class mmtraining_Exceptions
{
    private mmtraining_Exceptions() { }

    public class UserNotSpecifiedException extends Exception { }
    public class ExecutionInProductionNotAllowedException extends Exception { }
    public class UserNumberNotSpecifiedException extends Exception { }


}