public class mmcommon_Events {

  public static final String EVENT_SUBJECT_HOLIDAY = 'Firm Holiday';

  public static Event createHolidayEvent(Id ownerId, Date eventDate) {
    Event newEvent = new Event(
        ActivityDate = eventDate,
        IsAllDayEvent = true,
        IsPrivate = false,
        IsReminderSet = false,
        OwnerId = ownerId,
        Subject = EVENT_SUBJECT_HOLIDAY
      );
    return newEvent;
  }
}