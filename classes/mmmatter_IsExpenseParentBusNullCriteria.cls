/**
 *  Domain criteria used to find all litify_pm__Expense__c records where the ParentBusiness__c field is null
 */
public class mmmatter_IsExpenseParentBusNullCriteria
    implements mmlib_ICriteria
{
    private list<litify_pm__Expense__c> records = new list<litify_pm__Expense__c>();

    public mmlib_ICriteria setRecordsToEvaluate(List<SObject> records)
    {
        this.records.clear();
        this.records.addAll( (list<litify_pm__Expense__c>)records );

        return this;
    }

    public List<SObject> run()
    {
        list<litify_pm__Expense__c> qualifiedRecords = new list<litify_pm__Expense__c>();

        // Loop through the litify_pm__Expense__c records.
        for ( litify_pm__Expense__c record : this.records )
        {
            system.debug( 'record amount ' + record.litify_pm__Amount__c + ' -- record.ParentBusiness__c == ' + record.ParentBusiness__c );
            if ( record.ParentBusiness__c == null )
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}