@isTest
private without sharing class PaymentCollection_Domain_Test {

    static Payment_Collection__c createAndInsertPaymentCollection(){
        //Note:  These are better candidates for reuse.
        System.debug('Creating payment collection...');

        Payment_Collection__c record = null;
     
		record = new Payment_Collection__c(
			Company__c = TestDataFactory_FFA.company.Id,
			Bank_Account__c = TestDataFactory_FFA.bankAccounts[0].Id,
			Payment_Method__c = 'Check',
			Payment_Date__c = Date.today()
		);
		insert record;
        return record;

    }    
    
    @isTest 
    static void testCreate0(){
        PaymentCollection_Domain target = null;
        target = new PaymentCollection_Domain();
        System.assert(target!=null);        
    }
    
    @isTest 
    static void testCreate1(){
        PaymentCollection_Domain target = null;

        Payment_Collection__c paymentCollection = null;
        paymentCollection = createAndInsertPaymentCollection();

        List<Payment_Collection__c> paymentCollections=null;
        paymentCollections=new List<Payment_Collection__c>();

        paymentCollections.add(paymentCollection);

        target = new PaymentCollection_Domain(paymentCollections);
        System.assert(target!=null);        
    }

    @isTest 
    static void test_onBeforeInsert_0(){
        PaymentCollection_Domain target = null;

        Payment_Collection__c paymentCollection = null;
        paymentCollection = createAndInsertPaymentCollection();

        List<Payment_Collection__c> paymentCollections=null;
        paymentCollections=new List<Payment_Collection__c>();

        paymentCollections.add(paymentCollection);

        target = new PaymentCollection_Domain(paymentCollections);
        System.assert(target!=null);
        
        Test.startTest();

        target.onBeforeInsert();

        Test.stopTest();
    }    

    @isTest 
    static void test_Construct_0(){
        PaymentCollection_Domain target = null;

        Payment_Collection__c paymentCollection = null;
        paymentCollection = createAndInsertPaymentCollection();

        List<Payment_Collection__c> paymentCollections=null;
        paymentCollections=new List<Payment_Collection__c>();

        paymentCollections.add(paymentCollection);

        PaymentCollection_Domain.Constructor constructionZone=null;
        constructionZone=new PaymentCollection_Domain.Constructor();

        target = (PaymentCollection_Domain) constructionZone.construct(paymentCollections);
        System.assert(target!=null);
        
        Test.startTest();

        target.onBeforeInsert();

        Test.stopTest();
    }     
    
}