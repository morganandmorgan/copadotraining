/*============================================================================
Name            : ProcessExpensesController_TEST
Author          : CLD
Created Date    : July 2018
Description     : Test class for Process Expense Logic
=============================================================================*/
@isTest
public with sharing class ProcessExpensesController_TEST {

    public static litify_pm__Matter__c matter;
    public static c2g__codaBankAccount__c testBankAccount;
    public static Set<Id> expenseIds;

    static void setupData()
    {
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        c2g__codaCompany__c company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        company.c2g__CustomerSettlementDiscount__c = testGLA.Id;
        update company;
        company = [SELECT Id, Name, OwnerId FROM c2g__codaCompany__c WHERE Id = :company.Id];
        testBankAccount = ffaTestUtilities.createBankAccount(company, null,testGLA.Id);

        /*FFA_Custom_Setting__c ffaSetting = new FFA_Custom_Setting__c(
            Fees_GLA_Account__c = testGLA.Id);
        insert ffaSetting;*/

        /*--------------------------------------------------------------------
        LITIFY
        --------------------------------------------------------------------*/

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.c2g__CODAAccountTradingCurrency__c = 'USD';
        client.c2g__CODAAccountsPayableControl__c = testGLA.id;
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;

        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan;
        
        // create new Matter
        matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;

        litify_pm__Expense_Type__c expenseType1 = new litify_pm__Expense_Type__c();
        expenseType1.CostType__c = 'HardCost';
        expenseType1.Name = 'Telephone';
        expenseType1.ExternalID__c = 'TELEPHONE';
        expenseType1.General_Ledger_Account__c = testGLA.Id;
        INSERT expenseType1;

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Internet';
        expenseType2.ExternalID__c = 'INTERNET1';
        expenseType2.General_Ledger_Account__c = testGLA.Id;

        INSERT expenseType2;

        litify_pm__Expense__c testExpense1 = new litify_pm__Expense__c();

        testExpense1.litify_pm__Matter__c = matter.Id;
        testExpense1.litify_pm__Amount__c = 100.0;
        testExpense1.PayableTo__c = client.Id;
        testExpense1.litify_pm__Date__c = date.today();
        testExpense1.litify_pm__ExpenseType2__c = expenseType1.Id;
        testExpense1.ParentBusiness__c = testAccounts[1].Id;

        INSERT testExpense1;

        litify_pm__Expense__c testExpense2 = new litify_pm__Expense__c();

        testExpense2.litify_pm__Matter__c = matter.Id;
        testExpense2.litify_pm__Amount__c = 10.0;
        testExpense2.PayableTo__c = client.Id;
        testExpense2.litify_pm__Date__c = date.today();
        testExpense2.litify_pm__ExpenseType2__c = expenseType2.Id;
        testExpense2.ParentBusiness__c = testAccounts[1].Id;

        INSERT testExpense2;    
        expenseIds = new Set<Id>{testExpense1.Id, testExpense2.Id};
    }

    /*--------------------------------------------------------------------
        START TEST METHODS
    --------------------------------------------------------------------*/
    @isTest static void testProcessExpenseController(){

        setupData();

        processExpensesController ctlr = new processExpensesController();
        
        processExpensesController.ActionResult fetchAR = processExpensesController.fetchExpenseLines(null, null,null);
        processExpensesController.PayableInvoiceRequest pinRequest = new processExpensesController.PayableInvoiceRequest();
        pinRequest.pinDate = Date.today();
        pinRequest.selectedPaymentMedia = 'Electronic';
        pinRequest.selectedBankAccountName = testBankAccount.Name;
        pinRequest.autoPostInvoice = true;
        pinRequest.autoCreatePayment = true;

        expenseIds = new Set<Id>();
        for(litify_pm__Expense__c exp : [SELECT Id FROM litify_pm__Expense__c]){
        	expenseIds.add(exp.Id);
        }

        processExpensesController.ActionResult processAR = processExpensesController.createPINfromExpenses(expenseIds,pinRequest);

    }

    @isTest static void testExpenseTrigger(){

        setupData();

        processExpensesController ctlr = new processExpensesController();
        
        processExpensesController.ActionResult fetchAR = processExpensesController.fetchExpenseLines(null, null,null);
        processExpensesController.PayableInvoiceRequest pinRequest = new processExpensesController.PayableInvoiceRequest();
        pinRequest.pinDate = Date.today();
        pinRequest.selectedPaymentMedia = 'Electronic';
        pinRequest.selectedBankAccountName = testBankAccount.Name;
        pinRequest.autoPostInvoice = false;
        pinRequest.autoCreatePayment = false;

        List<litify_pm__Expense__c> expList = [SELECT Id, Auto_Create_PIN__c FROM litify_pm__Expense__c];
        for(litify_pm__Expense__c exp : expList){
            exp.Auto_Create_PIN__c = true;
            exp.Payable_Invoice_Created__c = false;
        }
        update expList;
    }

}