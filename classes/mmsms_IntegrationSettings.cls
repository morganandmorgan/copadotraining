public with sharing class mmsms_IntegrationSettings
{
	private mmsms_IntegrationSettings()
	{
		// Static only class.
	}

	private static Boolean settingsRetrieved = false;
	private static Integer timeLimitMinutes = 0;
	private static Boolean isEnabled_m = false;
	private static Boolean isEnabledInSandbox_m = false;

	public static Integer getTimeLimit()
	{
		getSettings();
		return timeLimitMinutes;
	}

	public static Boolean getIsEnabled()
	{
		getSettings();
		return isEnabled_m;
	}

	public static Boolean getIsEnabledInSandbox()
	{
		getSettings();
		return isEnabledInSandbox_m;
	}

	private static void getSettings()
	{
		if (!settingsRetrieved) {

			SMS_Integration__c settings = SMS_Integration__c.getInstance();

			if (settings != null) {
				isEnabled_m = settings.Integation_enabled__c;
				isEnabledInSandbox_m = settings.Integration_Enabled_In_Sandbox__c;
				timeLimitMinutes = Integer.valueOf(settings.Duplicate_Time_Limit__c);
			}

			settingsRetrieved = true;
		}
	}
}