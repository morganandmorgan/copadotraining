@isTest
private class IncidentInvestigationUpdateEventTest {

  @TestSetup
  private static void setup() {
    List<SObject> toInsert = new List<SObject>();

    Incident__c incident = new Incident__c();
    toInsert.add(incident);

    Account client = TestUtil.createPersonAccount('Person', 'Test');
    toInsert.add(client);

    Database.insert(toInsert);
    toInsert.clear();

    List<IncidentInvestigationEvent__c> incidentEvents = new List<IncidentInvestigationEvent__c>();
    Datetime now = Datetime.now();
    for (Integer i = 0; i < 3; ++i) {
      IncidentInvestigationEvent__c incidentEvent = new IncidentInvestigationEvent__c(
          EndDateTime__c = now.addHours(2),
          Incident__c = incident.Id,
          StartDateTime__c = now.addHours(1),
          Contracts_to_be_Signed__c = 3
        );
      incidentEvents.add(incidentEvent);
    }
    toInsert.addAll((List<SObject>) incidentEvents);

    Database.insert(toInsert);
    toInsert.clear();

    List<Event> events = new List<Event>();
    for (IncidentInvestigationEvent__c incidentEvent : incidentEvents) {
      Event e = new Event(
          EndDateTime = incidentEvent.EndDateTime__c,
          StartDateTime = incidentEvent.StartDateTime__c,
          Subject = 'Test Subject',
          WhatId = incidentEvent.Id
        );
      events.add(e);
    }
    toInsert.addAll((List<SObject>) events);

    Database.insert(toInsert);
    toInsert.clear();
  }

  private static Map<Id, IncidentInvestigationEvent__c> getIncidentEvents() {
    return new Map<Id, IncidentInvestigationEvent__c>([
      SELECT
        Id,
        Contracts_to_be_Signed__c
      FROM
        IncidentInvestigationEvent__c
    ]);
  }

  private static Map<Id, Event> getEvents(Set<Id> incidentEvents) {
    return new Map<Id, Event>([
      SELECT
        Id,
        WhatId,
        Contracts_to_be_Signed__c
      FROM
        Event
      WHERE
        WhatId IN :incidentEvents
    ]);
  }

  @isTest
  private static void testUpdate_ContractsToSignModified() {
    Map<Id, IncidentInvestigationEvent__c> incidentInvestigationEvents = getIncidentEvents();
    Map<Id, Event> investigationEvents = getEvents(incidentInvestigationEvents.keySet());

    for (IncidentInvestigationEvent__c incidentInvestigationEvent : incidentInvestigationEvents.values()) {
      ++incidentInvestigationEvent.Contracts_to_be_Signed__c;
    }

    Test.startTest();
    Database.update(incidentInvestigationEvents.values());
    Test.stopTest();

    Map<Id, IncidentInvestigationEvent__c> requeriedIncidentInvestigationEvents = getIncidentEvents();
    Map<Id, Event> requeriedInvestigationEvents = getEvents(incidentInvestigationEvents.keySet());

    for (IncidentInvestigationEvent__c incidentInvestigationEvent : incidentInvestigationEvents.values()) {
      IncidentInvestigationEvent__c requeriedIncidentInvestigationEvent = requeriedIncidentInvestigationEvents.get(incidentInvestigationEvent.Id);

      System.assertEquals(incidentInvestigationEvent.Contracts_to_be_Signed__c, requeriedIncidentInvestigationEvent.Contracts_to_be_Signed__c);
    }

    System.assertEquals(investigationEvents.keySet(), requeriedInvestigationEvents.keySet());
    for (Event requeriedInvestigationEvent : requeriedInvestigationEvents.values()) {
      IncidentInvestigationEvent__c incidentInvestigationEvent = incidentInvestigationEvents.get(requeriedInvestigationEvent.WhatId);

      System.assertEquals(incidentInvestigationEvent.Contracts_to_be_Signed__c, requeriedInvestigationEvent.Contracts_to_be_Signed__c);
    }
  }
}