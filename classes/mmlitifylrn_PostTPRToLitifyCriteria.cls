/**
 *  mmlitifylrn_PostTPRToLitifyCriteria
 */
public class mmlitifylrn_PostTPRToLitifyCriteria
    implements mmlib_ICriteria
{
    private static final id THIRD_PARTY_OUTGOING_REFERRAL_ID;

    static
    {
        THIRD_PARTY_OUTGOING_REFERRAL_ID = mmlib_RecordTypeUtils.getRecordTypeIDByDevName( litify_pm__Referral__c.SObjectType, 'mmlitifylrn_ThirdPartyOutgoingReferral');
    }

    private list<litify_pm__Referral__c> records = new list<litify_pm__Referral__c>();

    public mmlitifylrn_PostTPRToLitifyCriteria()
    {

    }

    public mmlib_ICriteria setRecordsToEvaluate( list<SObject> records )
    {
        if (records != null
            && litify_pm__Referral__c.SObjectType == records.getSobjectType()
            )
        {
            this.records.addAll( (list<litify_pm__Referral__c>) records);
        }

        return this;
    }

    public list<SObject> run()
    {
        list<litify_pm__Referral__c> qualifiedRecords = new list<litify_pm__Referral__c>();

        for (litify_pm__Referral__c record : this.records)
        {
            if ( THIRD_PARTY_OUTGOING_REFERRAL_ID == record.RecordTypeId
                && mmlitifylrn_Constants.REFERRAL_SYNCSTATUS.THIRDPARTYNEW.name().equalsIgnoreCase( record.litify_pm__sync_status__c )
                )
            {
                qualifiedRecords.add( record );
            }
        }

        return qualifiedRecords;
    }
}