/**
 * ConstantsTest
 * @description Test for Constants class.
 * @author Jeff Watson
 * @date 1/18/2019
 */
@IsTest
public with sharing class ConstantsTest {

    public static testMethod void Constants_Test() {

        System.assertEquals('limit', Constants.QUERY_LIMIT);
        System.assertEquals(100, Constants.QUERY_LIMIT_DEFAULT);
        System.assertEquals('before', Constants.QUERY_BEFORE);
        System.assertEquals(Datetime.now().addDays(1), Constants.QUERY_BEFORE_DEFAULT);
        System.assertEquals('search', Constants.QUERY_SEARCH);

        System.assertEquals(200, Constants.HTTP_STATUS_CODE_OK);
        System.assertEquals(400, Constants.HTTP_STATUS_CODE_BAD_REQUEST);
        System.assertEquals('Bad Request', Constants.HTTP_STATUS_CODE_BAD_REQUEST_MESSAGE);
        System.assertEquals(204, Constants.HTTP_STATUS_CODE_NO_CONTENT);
        System.assertEquals('No Content', Constants.HTTP_STATUS_CODE_NO_CONTENT_MESSAGE);
        System.assertEquals(500, Constants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR);
        System.assertEquals('Internal Server Error', Constants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR_MESSAGE);

        System.assertEquals('matters', Constants.PATH_ENTITY_MATTERS);
        System.assertEquals('queues', Constants.PATH_ENTITY_QUEUES);

        System.assertEquals('application/json', Constants.APPLICATION_JSON);
        System.assertEquals('UTF-8', Constants.UTF8);
    }
}