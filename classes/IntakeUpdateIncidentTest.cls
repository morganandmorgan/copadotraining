@isTest
private class IntakeUpdateIncidentTest {

    @TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        List<Incident__c> incidents = new List<Incident__c>();
        incidents.add(new Incident__c());
        incidents.add(new Incident__c());
        incidents.add(new Incident__c());
        toInsert.addAll((List<SObject>) incidents);

        List<Account> clients = new List<Account>();
        clients.add(TestUtil.createPersonAccount('Person', 'A'));
        clients.add(TestUtil.createPersonAccount('Person', 'B'));
        clients.add(TestUtil.createPersonAccount('Person', 'C'));
        toInsert.addAll((List<SObject>) clients);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Account> getClients() {
        return [
            SELECT
                Id,
                Name
            FROM
                Account
            WHERE
                IsPersonAccount = true
        ];
    }

    private static List<Incident__c> getIncidents() {
        return [
            SELECT
                Id,
                Incident_Label__c
            FROM
                Incident__c
        ];
    }

    private static List<Intake__c> getIntakes() {
        return [
            SELECT
                Name,
                Client__r.Name
            FROM
                Intake__c
            ORDER BY
                Client__r.Name ASC,
                Name ASC
        ];
    }

    private static List<Intake__c> getIntakes(Set<Id> recordIds) {
        return [
            SELECT
                Name,
                Client__r.Name
            FROM
                Intake__c
            WHERE
                Id IN :recordIds
            ORDER BY
                Client__r.Name ASC,
                Name ASC
        ];
    }

    private static Intake__c createIntake(Account client, Incident__c incident) {
        Intake__c intake = new Intake__c();
        if (client != null) {
            intake.Client__c = client.Id;
        }
        if (incident != null) {
            intake.Incident__c = incident.Id;
        }
        return intake;
    }

    private static String getExpectedIncidentLabel(List<Intake__c> intakes) {
        List<String> intakeDetails = new List<String>();
        for (Intake__c intake : intakes) {
            intakeDetails.add(intake.Client__r.Name + ' - ' + intake.Name);
        }
        return String.join(intakeDetails, ', ');
    }

    @isTest
    private static void afterInsert_IncidentLabelUpdated() {
        List<Account> clients = getClients();
        Incident__c incident = getIncidents().get(0);

        System.assertEquals(null, incident.Incident_Label__c);

        List<Intake__c> newIntakes = new List<Intake__c>();
        for (Account client : clients) {
            newIntakes.add(createIntake(client, incident));
        }

        Test.startTest();
        Database.insert(newIntakes);
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        Incident__c requeriedIncident = new Map<Id, Incident__c>(getIncidents()).get(incident.Id);

        String expectedIncidentLabel = getExpectedIncidentLabel(requeriedIntakes);

        System.assertEquals(expectedIncidentLabel, requeriedIncident.Incident_Label__c);
    }

    @isTest
    private static void afterDelete_IncidentLabelUpdated() {
        List<Account> clients = getClients();
        Incident__c incident = getIncidents().get(0);
        List<Intake__c> newIntakes = new List<Intake__c>();
        for (Account client : clients) {
            newIntakes.add(createIntake(client, incident));
        }
        Database.insert(newIntakes);

        Test.startTest();
        Intake__c toDelete = newIntakes.get(0);
        Database.delete(toDelete);
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        Incident__c requeriedIncident = new Map<Id, Incident__c>(getIncidents()).get(incident.Id);

        String expectedIncidentLabel = getExpectedIncidentLabel(requeriedIntakes);

        System.assertEquals(expectedIncidentLabel, requeriedIncident.Incident_Label__c);
    }

    @isTest
    private static void afterUpdate_RemovedIncident() {
        List<Account> clients = getClients();
        Incident__c incident = getIncidents().get(0);
        List<Intake__c> newIntakes = new List<Intake__c>();
        for (Account client : clients) {
            newIntakes.add(createIntake(client, incident));
        }
        Database.insert(newIntakes);

        Test.startTest();
        Intake__c toUpdate = newIntakes.get(0);
        toUpdate.Incident__c = null;
        Database.update(toUpdate);
        Test.stopTest();

        Set<Id> relatedIntakeIds = new Map<Id, Intake__c>(newIntakes).keySet().clone();
        relatedIntakeIds.remove(toUpdate.Id);

        List<Intake__c> requeriedIntakes = getIntakes(relatedIntakeIds);
        Incident__c requeriedIncident = new Map<Id, Incident__c>(getIncidents()).get(incident.Id);

        String expectedIncidentLabel = getExpectedIncidentLabel(requeriedIntakes);

        System.assertEquals(expectedIncidentLabel, requeriedIncident.Incident_Label__c);
    }

    @isTest
    private static void afterUpdate_AddedIncident() {
        List<Account> clients = getClients();
        Incident__c incident = getIncidents().get(0);
        List<Intake__c> newIntakes = new List<Intake__c>();
        for (Account client : clients) {
            newIntakes.add(createIntake(client, null));
        }
        Database.insert(newIntakes);

        // expected incidents to automatically be created
        for (Intake__c intake : newIntakes) {
            intake.Incident__c = null;
        }
        Database.update(newIntakes);

        Test.startTest();
        Intake__c toUpdate = newIntakes.get(0);
        toUpdate.Incident__c = incident.Id;
        Database.update(toUpdate);
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes(new Set<Id>{ toUpdate.Id });
        Incident__c requeriedIncident = new Map<Id, Incident__c>(getIncidents()).get(incident.Id);

        String expectedIncidentLabel = getExpectedIncidentLabel(requeriedIntakes);

        System.assertEquals(expectedIncidentLabel, requeriedIncident.Incident_Label__c);
    }

    @isTest
    private static void afterUpdate_TransferredIncident() {
        List<Account> clients = getClients();
        List<Incident__c> allIncidents = getIncidents();
        Incident__c incident = allIncidents.get(0);
        Incident__c newIncident = allIncidents.get(1);

        List<Intake__c> newIntakes = new List<Intake__c>();
        for (Account client : clients) {
            newIntakes.add(createIntake(client, incident));
        }
        Database.insert(newIntakes);

        for (Intake__c intake : newIntakes) {
            intake.Incident__c = newIncident.Id;
        }

        Test.startTest();
        Database.update(newIntakes);
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        Map<Id, Incident__c> requeriedIncidents = new Map<Id, Incident__c>(getIncidents());

        String expectedIncidentLabel = getExpectedIncidentLabel(requeriedIntakes);

        System.assertEquals(expectedIncidentLabel, requeriedIncidents.get(newIncident.Id).Incident_Label__c);
        System.assertEquals(null, requeriedIncidents.get(incident.Id).Incident_Label__c);
    }

    @isTest
    private static void afterUpdate_ChangedClient() {
        List<Account> clients = getClients();
        Incident__c incident = getIncidents().get(0);
        List<Intake__c> newIntakes = new List<Intake__c>();
        for (Account client : clients) {
            newIntakes.add(createIntake(client, incident));
        }
        Database.insert(newIntakes);

        Account newClient = TestUtil.createPersonAccount('Person', 'NewClient');
        newClient.External_ID__c = mmlib_Utils.generateGuid();
        Database.insert(newClient);

        for (Intake__c intake : newIntakes) {
            intake.Client__c = newClient.Id;
        }

        Test.startTest();
        Database.update(newIntakes);
        Test.stopTest();

        List<Intake__c> requeriedIntakes = getIntakes();
        Incident__c requeriedIncident = new Map<Id, Incident__c>(getIncidents()).get(incident.Id);

        String expectedIncidentLabel = getExpectedIncidentLabel(requeriedIntakes);

        System.assertEquals(expectedIncidentLabel, requeriedIncident.Incident_Label__c);
    }
}