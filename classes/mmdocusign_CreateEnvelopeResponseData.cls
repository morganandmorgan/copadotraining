public with sharing class mmdocusign_CreateEnvelopeResponseData
{
	private String clientUserId = '';
	private String envelopeId = '';

	public String getClientUserId()
	{
		return clientUserId;
	}

	public String getEnvelopeId()
	{
		return envelopeId;
	}

	public mmdocusign_CreateEnvelopeResponseData(mmdocusign_CreateEnvelopeResponse resp)
	{
		this.envelopeId = resp.getEnvelopeId();
		this.clientUserId = resp.getClientUserId();
	}

	@TestVisible
	private mmdocusign_CreateEnvelopeResponseData(String envelopeId, String clientUserId)
	{
		this.envelopeId = envelopeId;
		this.clientUserId = clientUserId;
	}
}