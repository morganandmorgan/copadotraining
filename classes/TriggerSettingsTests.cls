@IsTest
public with sharing class TriggerSettingsTests {

    private static Account account;
    private static Account personAccount;
    private static Contact contact;
    private static Incident__c incident;
    private static Intake__c intake;
    private static User user;
    private static litify_pm__Role__c role;
    private static litify_pm__Matter__c matter;
    private static Map<String, SObject> roleMap;

    static {
        account = TestUtil.createAccount('UT Account');
        insert account;

        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        contact = TestUtil.createContact(account.Id);
        insert contact;

        incident = TestUtil.createIncident();
        insert incident;

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        user = TestUtil.createUser();
        insert user;

        matter = TestUtil.createMatter(account);
        insert matter;

        role = TestUtil.createRole(matter, account);
        //insert role;
    }

    public static testMethod void triggerSettings_Trigger_CodeCoverage() {

        Account account = TestUtil.createAccount('UT Account');
        insert account;

        dsfs__DocuSign_Status__c docuSignStatus = TestUtil.createDocusignStatus();
        insert docuSignStatus;

        IncidentInvestigationEvent__c incidentInvestigationEvent = TestUtil.createIncidentInvestigationEvent(incident);
        insert incidentInvestigationEvent;
        update incidentInvestigationEvent;

        IntakeInvestigationEvent__c intakeInvestigationEvent = TestUtil.createIntakeInvestigationEvent(intake, incidentInvestigationEvent);
        insert intakeInvestigationEvent;

        Intake__c intake = TestUtil.createIntake(personAccount);
        insert intake;

        Event event = TestUtil.createEvent(user.Id, Datetime.now(), Datetime.now());
        insert event;

        litify_pm__Matter__c matter = TestUtil.createMatter(account);
        insert matter;
        update matter;

        //itify_pm__Matter_Team_Member__c matterTeamMember = TestUtil.createMatterTeamMember(matter, user, role);
        //insert matterTeamMember;

        //litify_pm__Referral__c referral =

        litify_pm__Expense__c expense = TestUtil.createExpense(matter);
        insert expense;
        update expense;

        Task task = TestUtil.createTask(contact.Id, 'Unit Test Task');
        insert task;
        update task;
    }
}