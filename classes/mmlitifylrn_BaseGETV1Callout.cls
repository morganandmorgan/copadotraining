/**
 *  mmlitifylrn_BaseGETV1Callout
 */
public abstract class mmlitifylrn_BaseGETV1Callout
    extends mmlib_BaseGETCallout
{
    mmlitifylrn_ThirdPartyConfig__c configSetting = mmlitifylrn_ThirdPartyConfig__c.getInstance();

    public mmlitifylrn_BaseGETV1Callout()
    {
        super();

        if ( configSetting != null
            && configSetting.IsIntegrationDebugEnabled__c
            )
        {
            this.isDebugOn = configSetting.IsIntegrationDebugEnabled__c;
        }

        getHeaderMap().put('Content-Type','application/json');
    }

    protected override string getHost()
    {
        if ( configSetting == null )
        {
            throw new mmlitifylrn_Exceptions.CalloutException( 'The ' + mmlitifylrn_ThirdPartyConfig__c.SObjectType + ' custom setting does not have a record populated.');
        }

        if ( string.isBlank( configSetting.LRNAPIDomainName__c ) )
        {
            throw new mmlitifylrn_Exceptions.CalloutException( 'The ' + mmlitifylrn_ThirdPartyConfig__c.LRNAPIDomainName__c + ' custom setting field is not populated.');
        }

        return configSetting.LRNAPIDomainName__c;

    return null;
    }

    protected override string getPath()
    {
        return '/api/v1' + (string.isNotBlank( getPathSuffix() ) ? getPathSuffix() : '');
    }

    protected abstract string getPathSuffix();

    protected override HttpResponse executeCallout()
    {
        HttpResponse resp = null;

        if ( configSetting != null
            && configSetting.IsLRNIntegrationEnabled__c == true
            && ( ! mmlib_Utils.org.isSandbox
                || (mmlib_Utils.org.isSandbox && configSetting.IsIntegrationsEnabledInSandboxEnv__c == true)
                )
            )
        {
            resp = super.executeCallout();

            if ( 401 == resp.getStatusCode()
                && mmlitifylrn_Constants.LRN_API_ERROR_MESSAGE_INVALID_TOKEN.equalsIgnoreCase( mmlitifylrn_Logic.processCalloutErrorMessage( resp ) )
                )
            {
                system.debug( resp );

                // this represents a session token expiration
                // grab the authorization
                mmlitifylrn_CalloutAuthorization mmlitifylrnAuth = (mmlitifylrn_CalloutAuthorization)this.auth;
                // a callout was attempted
                // the callout was not authorized
                // refresh the token and try again.
                mmlitifylrnAuth.getCredentials().refreshSessionForFirm( mmlitifylrnAuth.getFirmMakingTheCallout(), mmlitifylrnAuth.getUow() );


                system.debug( 'PRIOR TO SECOND CALLOUT ATTEMPT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

                // try the callout again with the new, refreshed session token
                resp = super.executeCallout();

                system.debug( 'AFTER TO SECOND CALLOUT ATTEMPT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            }

        }
        else
        {
            resp = new HttpResponse();

            resp.setBody('{}');
            resp.setStatus('Callouts to Litify Referral Network are currently disabled.');
            resp.setStatusCode(203);

            system.debug( resp.getStatus() );
        }

        return resp;
    }
}