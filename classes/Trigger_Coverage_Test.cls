/**
 * Trigger_Coverage_Test
 * @description Test coverage for triggers.  This is only intended to provide coverage.  Logic tests can be found
 * in each Domain class where trigger handling occurs.
 * @author Jeff Watson
 * @date 1/2/2019
 */
@IsTest
public with sharing class Trigger_Coverage_Test {

    private static Account account;
    private static Account officeAccount;
    private static Account personAccount;
    private static Contact contact;
    private static Incident__c incident;
    private static IncidentInvestigationEvent__c incidentInvestigationEvent;
    private static Intake__c intake;
    private static User user;
    private static litify_pm__Role__c role;
    private static litify_pm__Matter_Team_Role__c teamRole;
    private static litify_pm__Role__c providerRole;
    private static litify_pm__Matter__c matter;
    private static Map<String, SObject> roleMap;
    private static litify_pm__Damage__c damage;
    private static litify_pm__Firm__c firm;
    private static litify_pm__Case_Type__c caseType;
    private static Fee_Goal__c feeGoal;
    private static Settlement__c settlement;
    private static Fee_Goal_Allocation__c feeGoalAllocation;

    static {

        Trigger_Control__c triggerControl = new Trigger_Control__c(Name = 'Default Controls');
        insert triggerControl;

        account = TestUtil.createAccountBusinessMorganAndMorgan('Biz Account');
        insert account;

        officeAccount = TestUtil.createAccount('MM Office');
        officeAccount.Type = 'MM Office';
        insert officeAccount;

        personAccount = TestUtil.createPersonAccount('Unit', 'Test');
        insert personAccount;

        contact = TestUtil.createContact(account.Id);
        insert contact;

        incident = TestUtil.createIncident();
        insert incident;

        incidentInvestigationEvent = TestUtil.createIncidentInvestigationEvent(incident);

        intake = TestUtil.createIntake(personAccount);
        insert intake;

        user = TestUtil.createUser();
        insert user;

        caseType = TestUtil.createCaseType();
        insert caseType;

        matter = TestUtil.createMatter(account);
        matter.litify_pm__Case_Type__c = caseType.Id;
        matter.Assigned_Office_Location__c = officeAccount.Id;
        matter.AssignedToMMBusiness__c = account.Id;
        insert matter;

        role = TestUtil.createRole(matter, account);
        insert role;

        teamRole = new litify_pm__Matter_Team_Role__c(Name = 'Case Developer');
        insert teamRole;

        providerRole = TestUtil.createRole(matter, account);
        providerRole.litify_pm__Parent_Role__c = role.Id;
        insert providerRole;

        damage = TestUtil.createDamage(matter);
        damage.litify_pm__Provider__c = providerRole.Id;
        insert damage;

        firm = TestUtil.createFirm('bencrump.com');
        insert firm;

        insert new litify_pm__Expense_Type__c(Name = 'Soft Cost Recovered', CostType__c = 'SoftCost', ExternalID__c = 'SOFTCOSTRECOVERED');
        insert new litify_pm__Expense_Type__c(Name = 'Hard Cost Recovered',  CostType__c = 'HardCost', ExternalID__c = 'HARDCOSTRECOVERED');

        feeGoal = TestUtil.createFeeGoal();
        insert feeGoal;

        settlement = TestUtil.createSettlement();
        insert settlement;

        feeGoalAllocation = TestUtil.createFeeGoalAllocation(feeGoal, settlement);
        insert feeGoalAllocation;
    }

    @IsTest
    private static void Account_Trigger() {
        insert TestUtil.createAccount('Coverage Account');
    }

    @IsTest
    private static void DocuSignStatus_Trigger() {
        //insert new dsfs__DocuSign_Status__c();
    }

    @IsTest
    private static void Event_Trigger() {
        insert TestUtil.createEvent();
    }

    @IsTest
    private static void FeeGoalAllocation_Trigger() {
        insert TestUtil.createFeeGoalAllocation(feeGoal, settlement);
    }

    @IsTest
    private static void IncidentInvestigationEvent_Trigger() {
        insert TestUtil.createIncidentInvestigationEvent(incident);
    }

    @IsTest
    private static void Intake_Trigger() {
        insert TestUtil.createIntake(personAccount);
    }

    @IsTest
    private static void IntakeInvestigationEvent_Trigger() {
        IncidentInvestigationEvent__c investigationEvent = TestUtil.createIncidentInvestigationEvent(incident);
        insert TestUtil.createIntakeInvestigationEvent(intake, investigationEvent);
    }

    @IsTest
    private static void Lawsuit_Trigger() {
       insert TestUtil.createLawsuit();
    }

    @IsTest
    private static void LitifyExpense_Trigger() {
       insert TestUtil.createExpense(matter);
    }

    @IsTest
    private static void LitifyMatter_Trigger() {
        insert TestUtil.createMatter(account);
    }

    @IsTest
    private static void LitifyMatterTeamMember_Trigger() {
        insert TestUtil.createMatterTeamMember(matter, user, teamRole);
    }

    @IsTest
    private static void LitifyReferral_Trigger() {
        insert TestUtil.createReferral(caseType, firm);
    }

    @IsTest
    private static void MarketingCampaign_Trigger() {
        //insert new Marketing_Campaign__c();
    }

    @IsTest
    private static void MarketingFinancialPeriod_Trigger() {
        insert new Marketing_Financial_Period__c();
    }

    @IsTest
    private static void MarketingSource_Trigger() {
        insert new Marketing_Source__c();
    }

    @IsTest
    private static void MarketingTrackingInfo_Trigger() {
        //insert new Marketing_Tracking_Info__c();
    }

    @IsTest
    private static void MMDocument_Trigger() {
        insert new MM_Document__c();
    }

    @IsTest
    private static void MMLibEvent_Trigger() {
        /* The dependancy mmmatter_Matters needs to be deleted and pointed to MatterDomain, once the Matter Refactor is done */
        mmmatter_Matters.StatusChangedPlatformEventData payload = new mmmatter_Matters.StatusChangedPlatformEventData();
        payload.matter_id = matter.Id;
        payload.new_status = 'new status';

        mmlib_Event__e mmlibEvent = new mmlib_Event__e();
        mmlibEvent.EventName__c = 'MatterStatusChanged';
        mmlibEvent.Payload__c = JSON.serialize(payload);
        EventBus.publish(mmlibEvent);
    }

    @IsTest
    private static void Role_Trigger() {
        insert TestUtil.createRole(matter, account);
    }

    @IsTest
    private static void Task_Trigger() {
        //insert TestUtil.createTask();
    }

    @IsTest
    private static void TransactionLineItem_Trigger() {

    }

    @IsTest
    private static void User_Trigger() {
        insert TestUtil.createUser();
    }

    // Todo: Test logic for trigger handlers can be moved into TransactionLineItem_Domain on the handler event.
    @IsTest
    private static void TransactionLineItem_Trigger_Disable() {
        Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.journals;
        TestDataFactory_FFA.postJournals(journals.keySet());
    }

    @IsTest
    private static void TransactionLineItem_Trigger_Enable() {
        Map<c2g__codaJournal__c, List<c2g__codaJournalLineItem__c>> journals = TestDataFactory_FFA.journals;
        TestDataFactory_FFA.postJournals(journals.keySet());
    }
}