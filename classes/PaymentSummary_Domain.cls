/*============================================================================
Name            : PaymentSummary_Domain
Author          : CLD
Created Date    : May 2019
Description     : Domain class for Payment Summaries (c2g__codaPaymentAccountLineItem__c)
=============================================================================*/
public without sharing class PaymentSummary_Domain extends fflib_SObjectDomain {
	private List<c2g__codaPaymentAccountLineItem__c> pmtSummaryList;
    private PaymentSummary_Service pmtSummaryService;

	// Ctors
    public PaymentSummary_Domain() {
        super();
        this.pmtSummaryList = new List<c2g__codaPaymentAccountLineItem__c>();
        this.pmtSummaryService = new PaymentSummary_Service();
    }

    public PaymentSummary_Domain(List<c2g__codaPaymentAccountLineItem__c> pmtSummaryList) {
        super(pmtSummaryList);
        this.pmtSummaryList = (List<c2g__codaPaymentAccountLineItem__c>) records;
        this.pmtSummaryService = new PaymentSummary_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new PaymentSummary_Domain(records);
        }
    }

    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        
		Map<Id, c2g__codaPaymentAccountLineItem__c> oldTriggerMap = (Map<Id, c2g__codaPaymentAccountLineItem__c>) existingRecords;
		Set<Id> pmtSummaryIds = new Set<Id>();

		for(c2g__codaPaymentAccountLineItem__c pmtSummary : pmtSummaryList){
			if(pmtSummary.c2g__Status__c == 'Canceled' && oldTriggerMap.get(pmtSummary.Id).c2g__Status__c != 'Canceled'){
				pmtSummaryIds.add(pmtSummary.Id);
			}
		}
		
		//perform the necessary voiding functions
		if(!pmtSummaryIds.isEmpty()){
			pmtSummaryService.handlePaymentVoid(pmtSummaryIds);
		}
        
    }
}