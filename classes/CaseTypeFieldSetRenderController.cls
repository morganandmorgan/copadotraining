public class CaseTypeFieldSetRenderController {

    public Intake__c thisIntake {get;set;}
    public Event thisEvent {get;set;}
    public String eventFieldSet {get;set;}
    public String intakeFieldSet {get;set;}
    public Account thisAccount {get;set;}
    public Task thisCallComment {get;set;}
    public Id relatedEventId {
        get {
            return relatedEventId;
        }
        set {
            relatedEventId = value;

            eventFieldSet = 'Investigation_Sched_Investigator_Email_N';
            thisEvent = queryEvent(eventFieldSet);

            Id thisIntakeId = thisEvent.WhatId;
            thisIntake = queryIntake(thisIntakeId,'Investigation_Sched_Investigator_Email_N');

            Id thisAccountId = findPerson();
            thisAccount = queryPerson(thisAccountId);
            }
        }
    public Id relatedIntakeId {
        get {
            return relatedIntakeId;
        }
        set {
            relatedIntakeId = value;
            intakeFieldSet = getFieldSet();
            thisIntake = queryIntake(intakeFieldSet);

            Id thisAccountId = findPerson();
            thisAccount = queryPerson(thisAccountId);

            Id thisCallCommentId = findCommentTask();
            thisCallComment = queryTask(thisCallCommentId);
            }
        }

    public CaseTypeFieldSetRenderController() {

    }

    public String getFieldSet() {
        Intake__c dummyIntake = [SELECT Id, Case_Type__c FROM Intake__c WHERE Id = :relatedIntakeId];
        String relatedIntakeCT = dummyIntake.Case_Type__c;
        String fieldSetResult = null;

        Map<String,Case_Type_Field_Set_Mappings__c> ctCustomSettingMap = Case_Type_Field_Set_Mappings__c.getAll();
        Map<String, String> CTMap = new Map<String, String>();
        for(Case_Type_Field_Set_Mappings__c ctcs : ctCustomSettingMap.values()) {
            CTMap.put(ctcs.Case_Type__c, ctcs.Field_Set_Name__c);
        }

        fieldSetResult = CTMap.get(relatedIntakeCT);

        if(fieldSetResult != null) {
            return fieldSetResult;
        } else {
            return 'Email_UR_Labor_OtherDisc';
        }
    }

   public static List<FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields();
   }

   public Intake__c queryIntake(String intakeFieldSetName) {
        List<FieldSetMember> fieldSetMemberList = readFieldSet(intakeFieldSetName, 'Intake__c');

        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, client__c from Intake__c WHERE Id = \''+relatedIntakeId+'\' LIMIT 1';
        return (Intake__c) queryFirstResult(query);
   }

   public Intake__c queryIntake(Id intakeId, String intakeFieldSetName) {
        List<FieldSetMember> fieldSetMemberList = readFieldSet(intakeFieldSetName, 'Intake__c');

        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, client__c from Intake__c WHERE Id = \''+intakeId+'\' LIMIT 1';
        return (Intake__c) queryFirstResult(query);
   }

   public Event queryEvent(String eventFieldSetName) {
       List<FieldSetMember> fieldSetMemberList = readFieldSet(eventFieldSetName, 'Event');

       String query = 'SELECT ';
       Set<String> fieldPaths = new Set<String>{ 'Id', 'WhatId' };
       for(Schema.FieldSetMember f : fieldSetMemberList) {
            fieldPaths.add(f.getFieldPath());
       }
       query += String.join(new List<String>(fieldPaths), ', ');
       query += ' from Event WHERE Id = \''+relatedEventId+'\' LIMIT 1';
       return (Event) queryFirstResult(query);
   }

   public Id findPerson() {
        List<Account> queriedAccount = [select id from Account where id = :thisIntake.client__c];
        if (queriedAccount.size() > 0) {
            return queriedAccount[0].Id;
        } else {
            //error could not query account from intake
            return null;
        }
   }

   public Account queryPerson(Id queriedAccount) {
        List<FieldSetMember> fieldSetMemberList = readFieldSet('Email_UR_Labor_TCPA', 'Account');
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id from Account WHERE Id = \''+queriedAccount+'\'';

        return (Account) queryFirstResult(query);
   }

   public Id findCommentTask() {
        List<Task> queriedTask = [SELECT Id,WhatId FROM Task WHERE WhatId = :thisIntake.Id AND Subject = 'Intake Comment' LIMIT 1];
        if (queriedTask.size() > 0) {
            return queriedTask[0].Id;
        } else {
            return null;
        }
   }

   public Task queryTask(Id queriedTask) {
        List<FieldSetMember> fieldSetMemberList = readFieldSet('Email_UR_Labor_TCPA', 'Task');
        string query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id from Task WHERE Id = \''+queriedTask+'\'';
        return (Task) queryFirstResult(query);
   }

    private static SObject queryFirstResult(String queryString) {
        SObject queryResult = null;
        try {
            queryResult = Database.query(queryString).get(0);
        }
        catch (Exception e) {
            System.debug(e);
        }
        return queryResult;
    }
}