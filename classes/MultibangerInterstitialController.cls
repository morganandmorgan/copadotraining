public with sharing class MultibangerInterstitialController {
  private Id intakeId;

  public MultibangerInterstitialController(ApexPages.StandardController stdControllerParam) {
    intakeId = stdControllerParam.getRecord().Id;
  }

  private Intake__c CopyFieldsFromIntake() {
    // create new intake by copying data from this intake
    Intake__c existingIntake = queryForIntake();

    Intake__c newIntake = existingIntake.clone(false, true, false, false);
    newIntake.Caller_is_Injured_Party__c = 'No';
    newIntake.Can_IP_sign__c = null;
    newIntake.Who_can_legally_sign_on_behalf_of_IP__c = null;

    insert newIntake;

    return newIntake;
  }

  private Intake__c queryForIntake() {
    // only query for fields to be copied
    Set<String> intakeFields = new Set<String>{ 'Id' };
    for (FieldSetMember fsm : SObjectType.Intake__c.FieldSets.Multi_Banger_Clone.getFields()) {
      intakeFields.add(fsm.getFieldPath());
    }

    System.debug(intakeFields);

    SoqlUtils.SoqlQuery intakeQuery = SoqlUtils.getSelect(new List<String>(intakeFields), 'Intake__c')
                    .withCondition(SoqlUtils.getEq('Id', intakeId));
    return Database.query(intakeQuery.toString());
  }

  public PageReference PerformRedirect() {
    Intake__c newIntake = CopyFieldsFromIntake();

    PageReference pr = Page.ContactSearch;
    pr.getParameters().putAll(new Map<String, String>{
            'intakeID' => newIntake.Id
        });
    return pr;
  }
}