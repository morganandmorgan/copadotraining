/**
 *  simple script to generate an "injured party" role record for a matter specified
 *      assumes that if there is an existing related role record
 *
 *  @usage String query = 'select id, name, litify_pm__Client__c'
 *                           + ', ( select id, name, litify_pm__Party__c, litify_pm__Matter__c'
 *                                + ' from litify_pm__Roles__r'
 *                               + ' where litify_pm__Role__c = \'Injured Party\')'
 *                       + ' from litify_pm__Matter__c';
 *         new mmlib_GenericBatch( mmmatter_RoleInjuredPartyCreateExecutor.class, query).setBatchSizeTo(15).execute();
 */
public class mmmatter_RoleInjuredPartyCreateExecutor
    implements mmlib_GenericBatch.IGenericExecuteBatch
{
    public void run( list<SObject> scope )
    {
        list<litify_pm__Role__c> matterRecordsToCreate = new list<litify_pm__Role__c>();

        litify_pm__Matter__c matterRecord = null;

        for ( SObject record : scope )
        {
            matterRecord = (litify_pm__Matter__c)record;

            if ( matterRecord.litify_pm__Roles__r.isEmpty() )
            {
                // there are no role records with injured party specified.
                // go ahead and create one.
                litify_pm__Role__c newInjuredPartyRole = new litify_pm__Role__c();

                 newInjuredPartyRole.litify_pm__Role__c = 'Injured Party';
                 newInjuredPartyRole.litify_pm__Matter__c = matterRecord.id;
                 newInjuredPartyRole.litify_pm__Party__c = matterRecord.litify_pm__Client__c;
                 newInjuredPartyRole.litify_pm__Additional_Trait__c = 'Client;Injured Party';
                 newInjuredPartyRole.litify_pm__Comments__c = 'This role record is generated from the parent matter\'s client field.';

                 matterRecordsToCreate.add( newInjuredPartyRole );
            }
        }

        database.insert( matterRecordsToCreate, false );
    }
}