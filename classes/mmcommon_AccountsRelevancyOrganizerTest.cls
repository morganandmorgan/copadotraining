@isTest
private class mmcommon_AccountsRelevancyOrganizerTest
{
    @isTest
    static void typicalLowPrecisionScenarioTest()
    {
        List<Account> accountList = baseAccounts();

        Account targetAccount = accountList.get(1);

        mmcommon_IAccountsSelectorPersonRequest req =
            mmcommon_AccountsSelectorPersonRequest.newInstance()
                .setLastName(targetAccount.LastName)
                .setEmail(targetAccount.PersonEmail);

        List<Account> result =
            new mmcommon_AccountsRelevancyOrganizer()
                .setRecordsToSort(accountList)
                .setRequestCriteria(req)
                .setHighPrecisionFlag(false)
                .filterAndSort();

        System.assert(!result.isEmpty());
        System.assertEquals(1, result.size());
        System.assertEquals(targetAccount.Id, result.get(0).Id);
    }

    @isTest
    static void typicalHighPrecisionScenario_MissingName_Test()
    {
        List<Account> accountList = baseAccounts();

        Account targetAccount = accountList.get(1);

        mmcommon_IAccountsSelectorPersonRequest req =
            mmcommon_AccountsSelectorPersonRequest.newInstance()
                .setLastName(targetAccount.LastName)
                .setEmail(targetAccount.PersonEmail);

        List<Account> result =
            new mmcommon_AccountsRelevancyOrganizer()
                .setRecordsToSort(accountList)
                .setRequestCriteria(req)
                .setHighPrecisionFlag(true)
                .filterAndSort();

        System.assert(result.isEmpty());
    }

    @isTest
    static void typicalHighPrecisionScenario_MissingNonName_Test()
    {
        List<Account> accountList = baseAccounts();

        Account targetAccount = accountList.get(1);

        mmcommon_IAccountsSelectorPersonRequest req =
            mmcommon_AccountsSelectorPersonRequest.newInstance()
                .setFirstName(targetAccount.FirstName)
                .setLastName(targetAccount.LastName);

        List<Account> result =
            new mmcommon_AccountsRelevancyOrganizer()
                .setRecordsToSort(accountList)
                .setRequestCriteria(req)
                .setHighPrecisionFlag(true)
                .filterAndSort();

        System.assert(result.isEmpty());
    }

    @isTest
    static void typicalHighPrecisionScenarioTest()
    {
        List<Account> accountList = baseAccounts();

        Account targetAccount = accountList.get(1);

        mmcommon_IAccountsSelectorPersonRequest req =
            mmcommon_AccountsSelectorPersonRequest.newInstance()
                .setFirstName(targetAccount.FirstName)
                .setLastName(targetAccount.LastName)
                .setEmail(targetAccount.PersonEmail);

        List<Account> result =
            new mmcommon_AccountsRelevancyOrganizer()
                .setRecordsToSort(accountList)
                .setRequestCriteria(req)
                .setHighPrecisionFlag(true)
                .filterAndSort();

        System.assert(!result.isEmpty());
        System.assertEquals(1, result.size());
        System.assertEquals(targetAccount.Id, result.get(0).Id);
    }



    @isTest
    static void sameNameLowPrecisionScenarioTest()
    {
        List<Account> accountList = baseAccounts();

        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.PersonEmail = 'drac@transylvania.net';
        accountList.add(acct);

        Account targetAccount = accountList.get(1);

        mmcommon_IAccountsSelectorPersonRequest req =
            mmcommon_AccountsSelectorPersonRequest.newInstance()
                .setLastName(targetAccount.LastName)
                .setEmail(targetAccount.PersonEmail);

        List<Account> result =
            new mmcommon_AccountsRelevancyOrganizer()
                .setRecordsToSort(accountList)
                .setRequestCriteria(req)
                .setHighPrecisionFlag(false)
                .filterAndSort();

        System.assert(!result.isEmpty());
        System.assertEquals(1, result.size());
        System.assertEquals(targetAccount.Id, result.get(0).Id);
    }

    @isTest
    static void noRequestCriteriaTest()
    {
        List<Account> accountList = baseAccounts();

        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.PersonEmail = 'drac@transylvania.net';
        accountList.add(acct);

        try
        {
            List<Account> result =
                new mmcommon_AccountsRelevancyOrganizer()
                    .setRecordsToSort(accountList)
                    .filterAndSort();
            System.assert(false, 'A UsageException was expected.');
        }
        catch (mmcommon_AccountsRelevancyOrganizer.UsageException exc)
        {
            // This is expected.
        }
    }

    @isTest
    static void weightingTest_Birthdate()
    {
        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.Phone = '888-555-2900';
        acct.PersonEmail = 'drac@transylvania.com';
        acct.Date_of_Birth_mm__c = Date.newInstance(1981, 6, 2);

        mmcommon_AccountsRelevancyOrganizer organizer = new mmcommon_AccountsRelevancyOrganizer();

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setBirthDate(acct.Date_of_Birth_mm__c)
        );
        System.assertEquals(5, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setBirthDate(acct.Date_of_Birth_mm__c.addYears(5))
        );
        System.assertEquals(0, organizer.deriveWeightedRank(acct).recordWeightedRank);
    }

    @isTest
    static void weightingTest_Email()
    {
        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.Phone = '888-555-2900';
        acct.PersonEmail = 'drac@transylvania.com';

        mmcommon_AccountsRelevancyOrganizer organizer = new mmcommon_AccountsRelevancyOrganizer();

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setEmail('drac@otherspookywoods.com')
        );
        System.assertEquals(0, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setEmail('drac@transylvania.com')
        );
        System.assertEquals(10, organizer.deriveWeightedRank(acct).recordWeightedRank);
    }

    @isTest
    static void weightingTest_FirstName()
    {
        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.Phone = '888-555-2900';
        acct.PersonEmail = 'drac@transylvania.com';

        mmcommon_AccountsRelevancyOrganizer organizer = new mmcommon_AccountsRelevancyOrganizer();

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setFirstName('dra')
        );
        System.assertEquals(1, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setFirstName('drac')
        );
        System.assertEquals(2, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setFirstName('dracula')
        );
        System.assertEquals(3, organizer.deriveWeightedRank(acct).recordWeightedRank);
    }

    @isTest
    static void weightingTest_LastName()
    {
        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.Phone = '888-555-2900';
        acct.PersonEmail = 'drac@transylvania.com';

        mmcommon_AccountsRelevancyOrganizer organizer = new mmcommon_AccountsRelevancyOrganizer();

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setLastName('von')
        );
        System.assertEquals(2, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setLastName('von Vampire')
        );
        System.assertEquals(5, organizer.deriveWeightedRank(acct).recordWeightedRank);
    }

    @isTest
    static void weightingTest_Phone()
    {
        Account acct = new Account();
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.Phone = '888-555-2900';
        acct.PersonEmail = 'drac@transylvania.com';

        // The method 'SObject.recalculateFormulas()' doesn't work as I'd expect it.
        insert acct;
        acct = [select Id, Phone, PersonMobilePhone, Phone_Unformatted_Formula__c, MobilePhone_Unformatted_Formula__pc from Account where Id = :acct.Id].get(0);

        mmcommon_AccountsRelevancyOrganizer organizer = new mmcommon_AccountsRelevancyOrganizer();

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setPhoneNumber('888-555-2901')
        );
        System.assertEquals(0, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setPhoneNumber('888-555-2900')
        );
        System.assertEquals(9, organizer.deriveWeightedRank(acct).recordWeightedRank);

        acct.PersonMobilePhone = acct.Phone;
        acct.Phone = null;
        update acct;
        acct = [select Id, Phone, PersonMobilePhone, Phone_Unformatted_Formula__c, MobilePhone_Unformatted_Formula__pc from Account where Id = :acct.Id].get(0);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setPhoneNumber('888-555-2901')
        );
        System.assertEquals(0, organizer.deriveWeightedRank(acct).recordWeightedRank);

        organizer.setRequestCriteria(
            mmcommon_AccountsSelectorPersonRequest.newInstance().setPhoneNumber('888-555-2900')
        );
        System.assertEquals(9, organizer.deriveWeightedRank(acct).recordWeightedRank);
    }

    private static List<Account> baseAccounts()
    {
        List<Account> accountList = new List<Account>();

        Account acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Dracula';
        acct.LastName = 'von Vampire';
        acct.Phone = '888-555-2900';
        acct.PersonEmail = 'drac@transylvania.com';
        accountList.add(acct);

        acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Frank';
        acct.LastName = 'Enstein';
        acct.Phone = '888-555-1100';
        acct.PersonEmail = 'boltneck@fire-bad.com';
        accountList.add(acct);

        acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Murray';
        acct.LastName = 'Mummy';
        acct.Phone = '888-555-4400';
        acct.PersonEmail = 'murray@thesands.com';
        accountList.add(acct);

        acct = new Account();
        acct.Id = fflib_IDGenerator.generate(Account.SObjectType);
        acct.FirstName = 'Wayne';
        acct.LastName = 'Werewolf';
        acct.Phone = '888-555-2200';
        acct.PersonEmail = 'wayne_werewolf@fullmoon.com';
        accountList.add(acct);

        return accountList;
    }
}