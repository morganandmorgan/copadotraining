/**
 *  mmlib_ISObjectSelector
 */
public interface mmlib_ISObjectSelector extends fflib_ISObjectSelector
{
    /**
     * Returns the SelectSObjectsById(Set<Id>) query string.  Useful for Database.Querylocators
     */
    String selectSObjectsByIdQuery();
}