/**
 * AuditReportCardDomain
 * @description Domain class for Audit_Report_Card__c SObject.
 * @author Matt Terrill
 * @date 12/13/2019
 */
public without sharing class AuditReportCardDomain extends fflib_SObjectDomain {

    private List<Audit_Report_Card__c> auditReportCards;

    public AuditReportCardDomain() {
        super();
        this.auditReportCards = new List<Audit_Report_Card__c>();
    } //constructor


    public AuditReportCardDomain(List<Audit_Report_Card__c> auditReportCards) {
        super(auditReportCards);
        this.auditReportCards = auditReportCards;
    } //constructor()


    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new AuditReportCardDomain(records);
        }
    } //IConstructable


    public override void onBeforeInsert() {
        //make sure report cards for the same user do not overlap start/end dates

        //build a set of the userIds from these report cards
        Set<Id> userIds = new Set<Id>();
        for (Audit_Report_Card__c rc : auditReportCards) {
            userIds.add(rc.user__c);
        } //for report cards

        AuditReportCardSelector rcSelector = new AuditReportCardSelector();
        List<Audit_Report_Card__c> possibleOverlaps = rcSelector.selectForUsers(userIds);

        for (Audit_Report_Card__c reportCard : auditReportCards) {
            for (Audit_Report_Card__c possibleOverlap : possibleOverlaps) {
                if (possibleOverlap.user__c == reportCard.user__c && (
                    (possibleOverlap.start_date__c <= reportCard.start_date__c && possibleOverlap.end_date__c >= reportCard.start_date__c)
                    || (possibleOverlap.start_date__c <= reportCard.end_date__c && possibleOverlap.end_date__c >= reportCard.end_date__c)) ) {

                    reportCard.addError('Report Card Start and End dates cannot overlap for the same user.');
                    break;

                } //if overlap
            } //for possible overlapping report cards
        } //for trigger reportCards

    } //onBeforeInsert

    public override void onAfterInsert() {
        //parent or reparent flag to new report cards

        //build a set of the userIds from these report cards
        Set<Id> userIds = new Set<Id>();
        for (Audit_Report_Card__c rc : auditReportCards) {
            userIds.add(rc.user__c);
        } //for report cards

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {Audit_Flag__c.getSObjectType()});

        AuditFlagSelector flagSelector = new AuditFlagSelector();
        flagSelector.addSObjectFields(new List<Schema.SObjectField>{Audit_Flag__c.createdDate});
        //loop over all the flags
        for (Audit_Flag__c flag : flagSelector.selectByUserId(userIds)) {
            //loop over the report cards to find a match (there could be multiple cards per user because of date ranges)
            for (Audit_Report_Card__c reportCard : auditReportCards) {
                if (reportCard.user__c == flag.user__c && reportCard.start_date__c <= flag.createdDate && reportCard.end_date__c >= flag.createdDate) {
                    flag.audit_report_card__c = reportCard.id;
                    uow.registerDirty(flag);
                    break;
                } //if report card match
            } //for report cards
        } //for flags

        uow.commitWork();

    } //onAfterInsert

} //class