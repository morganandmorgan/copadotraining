/**
 *  mmcommon_AccountsSelector
 */
public with sharing class mmcommon_AccountsSelector
    extends mmlib_SObjectSelector
    implements mmcommon_IAccountsSelector
{
    public static mmcommon_IAccountsSelector newInstance()
    {
        return (mmcommon_IAccountsSelector) mm_Application.Selector.newInstance( Account.SObjectType );
    }

    private Schema.sObjectType getSObjectType()
    {
        return Account.SObjectType;
    }

    private List<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Account.Address_Formatted__c,
                Account.Date_of_Birth_mm__c,
                Account.Drivers_License_State__c,
                Account.Gender__c,
                Account.Language__c,
                Account.Last_Geocoding_Update__pc,
                Account.Maiden_Name__c,
                Account.Marital_Status__c,
                Account.MobilePhone_Unformatted_Formula__pc,
                Account.OfficeLocationCode__c,
                Account.Other_Names_Used__c,
                Account.PersonLeadSubSource__c,
                Account.PersonMobilePhone,
                Account.Phone_Unformatted_Formula__c,
                Account.SocialSecurityNumber_Last4__c,
                Account.Spouse_Partner_Name__c,
                Account.Work_Phone__c
            };
    }

    public List<Account> selectById(Set<Id> idSet)
    {
        return (List<Account>) selectSObjectsById(idSet);
    }

    public List<Account> selectWithFieldsetById(Schema.FieldSet fs, Set<Id> idSet)
    {
        return
            (List<Account>)
            Database.query(
                newQueryFactory()
                .selectFieldSet(fs)
                .setCondition(Account.Id + ' in :idSet')
                .toSOQL()
            );
    }

    /**
     *
    */
    public list<Account> selectTreatmenCentersCloseToLocation(Location originatingLocation, integer distanceInMiles, String treatmentCenterType)
    {
        /*
            select Id, RecordType.name, Name
                , distance( BillingAddress, geolocation( 37.454549, -77.668859 ), 'mi' ) Dist
            from Account
            where distance( BillingAddress, geolocation( 37.454549, -77.668859 ), 'mi' ) < 10
            and recordtype.developername = 'Treatment_Centers'
        */
        system.debug('originatingLocation == ' + originatingLocation);
        system.debug( 'distanceInMiles == ' + distanceInMiles);

        fflib_QueryFactory qf =
            newQueryFactory()
                .setCondition(
                    'RecordType.DeveloperName = \'Treatment_Centers\''
                    + ' and Treatment_Center_Type__c includes (\'' + treatmentCenterType + '\')'
                    + ' and distance( ' + Account.BillingAddress + ', geolocation( ' + originatingLocation.getLatitude() + ', ' + originatingLocation.getLongitude() + ' ), \'mi\' ) < ' + distanceInMiles);

        qf.getOrderings().clear();

        String soql =
            qf.toSOQL() + ' ' +
            'ORDER BY distance( ' + Account.BillingAddress + ', geolocation( ' + originatingLocation.getLatitude() + ', ' + originatingLocation.getLongitude() + ' ), \'mi\' ) ' +
            'LIMIT 3';

        System.debug('Treatment Centers close to location query:\n' + soql);

        return Database.query(soql);
    }

    public List<Account> selectByFuzzyMatching(mmcommon_IAccountsSelectorPersonRequest request)
    {
        List<String> whereClauseComponents = new List<String>();
        for (SoqlUtils.SoqlCondition cond : buildPersonAccountSearchConditions(request)) {
            whereClauseComponents.add(cond.toString());
        }

        return
            (List<Account>)
            Database.query(
                newQueryFactory()
                .setCondition(String.join(whereClauseComponents, ' AND '))
                .setLimit(20000) // mimic what is returned by the contact search page as the wide net before ranking
                .toSOQL()
            );
    }

    public List<Account> selectByOfficeLocationCode(Set<String> codeSet)
    {
        Id officeLocationRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Office_Locations');
        return 
            Database.query(
                newQueryFactory()
                    .setCondition(
                        Account.OfficeLocationCode__c + ' in :codeSet and ' +
                        Account.RecordTypeId + ' = :officeLocationRecordTypeId')
                    .toSOQL() );
    }

    /**
     * //TODO: refactor this query once the fflib_apex_commons framework is implemented
     */
    public list<Account> selectByPersonFieldRequest( mmcommon_IAccountsSelectorPersonRequest request )
    {

        // This builds the search where clause
        List<SoqlUtils.SoqlCondition> searchConditions = buildPersonAccountSearchConditions(request);

        List<Account> searchResults = new List<Account>();

        if ( ! searchConditions.isEmpty())
        {
            SoqlUtils.SoqlQuery personQuery = SoqlUtils.getSelect( buildPersonAccountSearchFields(), 'Account') // this is the Select field1, field2, field... from SObject
                    .withCondition( SoqlUtils.getAnd( searchConditions ) )
                    .withLimit( 20000 )
                    .withOrder(SoqlUtils.getOrder('LastName').ascending()
                            .thenBy('FirstName').ascending().withNullsLast());
            searchResults.addAll((List<Account>) Database.query( personQuery.toString() ));
        }

        return searchResults;
    }

    private List<SoqlUtils.SoqlCondition> buildPersonAccountSearchConditions(mmcommon_IAccountsSelectorPersonRequest request)
    {
        List<SoqlUtils.SoqlCondition> searchConditions = new List<SoqlUtils.SoqlCondition>();

        list<SoqlUtils.SoqlCondition> searchOrConditions = new list<SoqlUtils.SoqlCondition>();
        SoqlUtils.SoqlCondition searchPhoneCondition = null;

        /*
            if the phone number is not null
            and either first or last name is not null
            and everything else is null
            then nulls are not allowed
        */

        String lastName = request.getLastName();
        String firstName = request.getFirstName();

        if (String.isNotBlank(lastName) || String.isNotBlank(firstName))
        {
            if (String.isNotBlank(lastName) && String.isNotBlank(firstName))
            {
                searchOrConditions.add(
                    SoqlUtils.getAnd(
                        SoqlUtils.getLike('LastName', mmlib_Utils.clean(lastName) + '%')
                        ,SoqlUtils.getLike('FirstName', mmlib_Utils.clean(firstName) + '%')
                    )
                );
            }
            else if (String.isNotBlank(lastName))
            {
                // Last name will be an "OR" - "If it matches" criteria
                searchOrConditions.add( SoqlUtils.getLike('LastName', mmlib_Utils.clean(lastName) + '%') );
            }
            else if (String.isNotBlank(firstName))
            {
                // First name will be an "OR" - "If it matches" criteria
                searchOrConditions.add( SoqlUtils.getLike('FirstName', mmlib_Utils.clean(firstName) + '%') );
            }
        }

        String strippedPhoneNum = mmlib_Utils.stripPhoneNumber(request.getPhoneNumber());

        if (String.isNotBlank(strippedPhoneNum))
        {
            // Phone will be an "OR" - "If it matches" criteria
            searchOrConditions.add( SoqlUtils.getEq('Phone_Unformatted_Formula__c', strippedPhoneNum) );
            searchOrConditions.add( SoqlUtils.getEq('MobilePhone_Unformatted_Formula__pc', strippedPhoneNum) );
        }

        String email = request.getEmail();

        if ( String.isNotBlank(email) )
        {
            // Email will be an "OR" - "If it matches" criteria
            searchOrConditions.add( SoqlUtils.getEq('PersonEmail', mmlib_Utils.clean(email)) );
        }

        String ssnLast4 = request.getSSN_Last4();

        if ( String.isNotBlank(ssnLast4) )
        {
            searchOrConditions.add( SoqlUtils.getEq('' + Account.SocialSecurityNumber_Last4__c, mmlib_Utils.clean(ssnLast4)) );
        }

        Date birthDate = request.getBirthDate();

        if (birthDate != null)
        {
            // Birth date will be an "OR" - "If it matches" criteria
            searchOrConditions.add( SoqlUtils.getEq('Date_of_Birth_mm__c', birthDate) );
        }

        if ( ! searchOrConditions.isEmpty() )
        {
            // merge all of the "OR" conditions together and add it to the main where clause
            searchConditions.add( SoqlUtils.getOr( searchOrConditions ) );
        }

        if ( ! searchConditions.isEmpty())
        {
            // also ensure that we are dealing with only Person account record types
            searchConditions.add(SoqlUtils.getEq('RecordTypeId', RecordTypeUtil.getRecordTypeIDByDevName('Account', 'Person_Account')));
        }

        system.debug( searchConditions );

        return searchConditions;
    }

    private List<String> buildPersonAccountSearchFields()
    {
        Set<String> accountFieldsSet = new Set<String>{ 'id'
                    , 'Name'
                    , 'FirstName'
                    , 'LastName'
                    , 'PersonMobilePhone'
                    , 'Phone_Unformatted_Formula__c'
                    , 'MobilePhone_Unformatted_Formula__pc'
                    , 'PersonEmail'
                    , 'Gender__c'
                    , 'Date_of_Birth_mm__c'
        };

        for (FieldSetMember fsm : SObjectType.Account.FieldSets.Contact_Create_Screen.getFields()) {
            accountFieldsSet.add(fsm.getFieldPath());
        }

        List<String> personAccountFields = new List<String>(accountFieldsSet);

        List<String> intakeFields = buildIntakeSearchFields();

        personAccountFields.add(SoqlUtils.getRelationshipSelect(intakeFields, 'Intake_Surveys__r').toString());
        personAccountFields.add(SoqlUtils.getRelationshipSelect(intakeFields, 'Caller_Intakes__r').toString());
        personAccountFields.add(SoqlUtils.getRelationshipSelect(intakeFields, 'InjuredParty_Intakes__r').toString());

        return personAccountFields;
    }

    private List<String> buildIntakeSearchFields()
    {
        Set<String> intakeFields = new Set<String>{
            'Id',
            'Name',
            'Case_Type__c',
            'Caller__r.Name',
            'Client__c',
            'Client__r.Name',
            'Gender__c',
            'Handling_Firm__c',
            'Injured_Party__r.Name',
            'Lead_Details__c',
            'Litigation__c',
            'Status__c',
            'Status_Detail__c',
            'What_was_the_date_of_the_incident__c',
            'RecordType.Name'
        };

        for (FieldSetMember fsm : SObjectType.Intake__c.FieldSets.Contact_Create_Screen.getFields())
        {
            intakeFields.add(fsm.getFieldPath());
        }

        return new List<String>(intakeFields);
    }

    // Todo: Remove along with DateOfBirthCoverageTest.cls once real tests are created.
    @TestVisible
    private static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}