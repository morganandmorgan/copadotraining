@IsTest
global class ffaDepositHelperTest 
{
    public static litify_pm__Matter__c matter;
    public static Deposit__c testDeposit;
    public static c2g__codaCompany__c company;

    @testSetup
    static void setup()
    {
        /*--------------------------------------------------------------------
        FFA
        --------------------------------------------------------------------*/
        c2g__codaDimension1__c testDimension1 = ffaTestUtilities.createTestDimension1();
        c2g__codaDimension2__c testDimension2 = ffaTestUtilities.createTestDimension2();
        c2g__codaDimension3__c testDimension3 = ffaTestUtilities.createTestDimension3();
        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();
        company = ffaTestUtilities.createFFACompany('ApexTestCompany', true, 'USD');
        company = [SELECT Id, Name, OwnerId, Default_Fee_GLA__c, Contra_Trust_GLA__c  FROM c2g__codaCompany__c WHERE Id = :company.Id];
        company.Default_Fee_GLA__c = testGLA.Id;
        company.Contra_Trust_GLA__c = testGLA.Id;
        update company;
        List<c2g__codaBankAccount__c> bankAccountList = new List<c2g__codaBankAccount__c>();
        c2g__codaBankAccount__c operatingBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Operating');
        bankAccountList.add(operatingBankAccount);
        c2g__codaBankAccount__c costBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Cost');
        bankAccountList.add(costBankAccount);
        c2g__codaBankAccount__c trustBankAccount = ffaTestUtilities.initBankAccount(company, null,testGLA.Id, 'Trust');
        bankAccountList.add(trustBankAccount);
        insert bankAccountList;

        Id socSecRecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(litify_pm__Matter__c.SObjectType, 'Social_Security');
        Id mmBusinessAccount = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Account.SObjectType, 'Morgan_Morgan_Businesses');

        List<Account> testAccounts = new List<Account>();

        Account client = new Account();
        client.Name = 'TEST CONTACT';
        client.litify_pm__First_Name__c = 'TEST';
        client.litify_pm__Last_Name__c = 'CONTACT';
        client.litify_pm__Email__c = 'test@testcontact.com';
        testAccounts.add(client);

        Account mmAccount = new Account();
        mmAccount.Name = 'MM COMPANY';
        mmAccount.litify_pm__First_Name__c = 'TEST';
        mmAccount.litify_pm__Last_Name__c = 'CONTACT';
        mmAccount.litify_pm__Email__c = 'test@testcontact.com';
        mmAccount.FFA_Company__c = company.Id;
        mmAccount.RecordTypeId = mmBusinessAccount;
        testAccounts.add(mmAccount);

        INSERT testAccounts;
        
        // Matter Plan
        litify_pm__Matter_Plan__c matterPlan = new litify_pm__Matter_Plan__c();
        matterPlan.Name = 'apex test matter plan';
        insert matterPlan; 

        // create new Matter
        litify_pm__Matter__c matter = new litify_pm__Matter__c();
        matter.RecordTypeId = socSecRecordTypeId;
        matter.AssignedToMMBusiness__c = testAccounts[1].Id;
        matter.litify_pm__Matter_Plan__c = matterPlan.Id;
        matter.litify_pm__Client__c = testAccounts[0].Id;
        matter.litify_pm__Status__c = 'Open';

        INSERT matter;


        litify_pm__Expense_Type__c expenseType = new litify_pm__Expense_Type__c();
        expenseType.CostType__c = 'HardCost';
        expenseType.Name = 'Telephone';
        expenseType.ExternalID__c = 'TELEPHONE';
        INSERT expenseType;

        litify_pm__Expense_Type__c expenseType2 = new litify_pm__Expense_Type__c();
        expenseType2.CostType__c = 'SoftCost';
        expenseType2.Name = 'Internet';
        expenseType2.ExternalID__c = 'INTERNET1';

        INSERT expenseType2;

        litify_pm__Expense__c testExpense = new litify_pm__Expense__c();

        testExpense.litify_pm__Matter__c = matter.Id;
        testExpense.litify_pm__Amount__c = 400.0;
        testExpense.litify_pm__Date__c = date.today();
        testExpense.litify_pm__ExpenseType2__c = expenseType.Id;
        INSERT testExpense;

        litify_pm__Expense__c testExpense2 = new litify_pm__Expense__c();

        testExpense2.litify_pm__Matter__c = matter.Id;
        testExpense2.litify_pm__Amount__c = 100.0;
        testExpense2.litify_pm__Date__c = date.today();
        testExpense2.litify_pm__ExpenseType2__c = expenseType2.Id;

        INSERT testExpense2;    

        litify_pm__Expense__c testExpense3 = new litify_pm__Expense__c();

        testExpense3.litify_pm__Matter__c = matter.Id;
        testExpense3.litify_pm__Amount__c = 200.0;
        testExpense3.litify_pm__Date__c = date.today();
        testExpense3.litify_pm__ExpenseType2__c = expenseType2.Id;

        INSERT testExpense3;    

    }

    @isTest
    public static void testDepositHelperFirstHappyPath()
    {

        litify_pm__Matter__c matter = [Select Id FROM litify_pm__Matter__c LIMIT 1];

        Deposit__c deposit = new Deposit__c();
        deposit.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Operating');
        deposit.Matter__c = matter.Id;
        deposit.Amount__c = 40.00;
        deposit.Source__c = 'Settlement';
        deposit.Check_Date__c = Date.today();

        INSERT deposit; 

        Deposit__c deposit2 = new Deposit__c();
        deposit2.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Operating');
        deposit2.Matter__c = matter.Id;
        deposit2.Amount__c = 400.00;
        deposit2.Source__c = 'Client';
        deposit2.Check_Date__c = Date.today();

        INSERT deposit2; 

        Test.startTest();
        
        ffaDepositAllocationBatchSchedule batch = new ffaDepositAllocationBatchSchedule();
        Database.executeBatch(batch, 25);

        Test.stopTest();
    }

    @isTest
    public static void testDepositHelperSecondHappyPath()
    {
        litify_pm__Matter__c matter = [Select Id FROM litify_pm__Matter__c LIMIT 1];

        Deposit__c deposit = new Deposit__c();

        deposit.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Operating');
        deposit.Matter__c = matter.Id;
        deposit.Amount__c = 45.00;
        deposit.Source__c = 'Client';
        deposit.Check_Date__c = Date.today();

        INSERT deposit; 

        Deposit__c deposit2 = new Deposit__c();

        deposit2.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Operating');
        deposit2.Matter__c = matter.Id;
        deposit2.Amount__c = 745.00;
        deposit2.Source__c = 'Client';
        deposit2.Check_Date__c = Date.today();

        INSERT deposit2; 

        Set<Id> depositIds = new Set<Id>();
        depositIds.add(deposit.Id);
        depositIds.add(deposit2.Id);

        MatterAllocationService.allocateItems_Deposit(depositIds);

        deposit = [Select Id, Deposit_Allocated__c, Allocated_Hard_Cost__c, Allocated_Soft_Cost__c, Allocated_Fee__c 
            FROM Deposit__c WHERE Id = :deposit.Id];

        System.assertEquals(true, deposit.Deposit_Allocated__c, 'Deposit Allocated flag was not set to false as expected!');
        System.assertEquals(45.00, deposit.Allocated_Hard_Cost__c, 'Allocated Hard Cost was not 45.00 as expected!');
        System.assertEquals(0.00, deposit.Allocated_Soft_Cost__c, 'Allocated Soft Cost was not 0.00 as expected!');
        System.assertEquals(0.00, deposit.Allocated_Fee__c, 'Allocated Fee was not 0.00 as expected!');

    }


    @IsTest
    public static void ffaDepositAllocationScheduleTest()
    {
        litify_pm__Matter__c matter = [Select Id FROM litify_pm__Matter__c LIMIT 1];
        Deposit__c deposit = new Deposit__c();

        deposit.RecordTypeId = mmlib_RecordTypeUtils.getRecordTypeIDByDevName(Deposit__c.SObjectType, 'Operating');
        deposit.Matter__c = matter.Id;
        deposit.Amount__c = 45.00;
        deposit.Source__c = 'Client';
        deposit.Check_Date__c = Date.today();

        INSERT deposit; 

        ffaDepositAllocationBatchSchedule batch = new ffaDepositAllocationBatchSchedule();          
        String sch = '0 0 8 13 2 ?';
        System.schedule('Deposit Allocation test', sch, batch );

        Date newDate = Date.today();
        c2g__codaCompany__c  testcompany = [SELECT Id  FROM c2g__codaCompany__c LIMIT 1];
        batch = new ffaDepositAllocationBatchSchedule(newDate, testcompany.Id);  

        System.schedule('Deposit Allocation test 2', sch, batch );

        Set<Id> specificDeposits = new Set<Id>();
        for(Deposit__c d : [SELECT Id FROM Deposit__c]){
            specificDeposits.add(d.Id);
        }
        ffaDepositAllocationBatchSchedule bjob = new ffaDepositAllocationBatchSchedule(specificDeposits, testcompany.Id);
        Database.executeBatch(bjob, 50);

    }

    @isTest
    public static void testReverseDepositAllocation()
    {
        testDepositHelperSecondHappyPath();
        Deposit__c deposit = [Select Id FROM Deposit__c LIMIT 1];

        List<Id> depositIds = new List<Id>();
        depositIds.add(deposit.Id);

        ffaDepositHelper.reverseDeposits(depositIds, date.today());
    }

    @isTest
    public static void testDepositHelperExtension()
    {
        litify_pm__Matter__c matter = [Select Id FROM litify_pm__Matter__c LIMIT 1];
        ffaDepositHelperExtension.executeDepositHelper(matter.Id);

        ffaDepositHelperExtension.executeDepositReverseHelper(matter.Id, String.valueOf(Date.today()));
    }
}