public without sharing class AuditProcessor {

    //Everything is static to support the @future
    private static AuditEvalEngine evalEngine = new AuditEvalEngine();
    private static AuditFlagDomain flagDomain = new AuditFlagDomain();
    private static AuditScheduledFlagDomain scheduledFlagDomain = new AuditScheduledFlagDomain();


    private static Map<Schema.SObjectType, String> relatedFields = new Map<Schema.SObjectType, String>();
    //builds a map linking an object type to an Id to store the lookup, ex. litify_pm__Matter__c, matter_id__c
    public static String findRelatedIdField(SObject obj) {
        //Get the type of the object
        Schema.SObjectType objType = obj.getsObjectType();

        if ( !relatedFields.containsKey(objType) ) {
            Schema.SObjectType otype = Audit_Flag__c.sObjectType;
            Schema.DescribeSObjectResult odesc = otype.getDescribe();
            String typeName = odesc.getName();
            Map<String, Schema.SObjectField> ofields = odesc.fields.getMap();

            for (String fieldName : ofields.keySet()) {
                //look for a reference to the current object
                if (ofields.get(fieldName).getDescribe().getRelationshipName() != null) {
                    if ( ofields.get(fieldName).getDescribe().getReferenceTo()[0] == objType) {
                        relatedFields.put(objType, evalEngine.fixRelatedFieldName(fieldName));
                        return fieldName;
                    } //if type match
                } //if relationship field
            } //for fields
        } else {
            return relatedFields.get(objType);
        }

        return null;
    } //findRelatedIdField


    @testVisible
    private static Map<String,Boolean> suppressById = new Map<String,Boolean>();
    private static Boolean suppressTrigger(String id) {
        if ( !suppressById.containsKey(id) ) {
            suppressById.put(id,true);
            return false;
        }
        return suppressById.get(id);
    } //suppressTrigger

    public static void triggerHandler(List<SObject> objList, Map<Id,SObject> oldMap) {
        // Check for Global Trigger control
        if (Trigger_Helper.isDisabled('AuditProcessor')) return;

        List<SObject> objToProcess = new List<SObject>();
        //we are now suppressing by record Id, so only process the records that haven't been suppressed
        for (SObject obj : objList) {
            if (!AuditProcessor.suppressTrigger(obj.id)) objToProcess.add(obj);
        }

        //Make sure we have objects to process
        if (objToProcess.size() != 0) {
            //Don't @future if we are already there or in a batch
            if (System.isFuture() || System.isBatch()) {
                processTrigger(objToProcess,oldMap);
            } else {
                String jObjList = JSON.serialize(objToProcess);
                String jOldMap = JSON.serialize(oldMap);
                processTrigger(jObjList, jOldMap);
            }
        } //if objToProcess
    } //triggerHandler


    @future(callout=true) //the callout is for the error notification (if needed)
    public static void processTrigger(String jObjList, String jOldMap) {

        List<SObject> objList = (List<SObject>)JSON.deserialize(jObjList,List<SObject>.Class);
        Map<Id,SObject> oldMap = (Map<Id,SObject>)JSON.deserialize(jOldMap,Map<Id,SObject>.Class);

        processTrigger(objList, oldMap);

    } //processTrigger @future


    public static void processTrigger(List<SObject> objList, Map<Id,SObject> oldMap) {
        try {
            Map<Id,SObject> objects = new Map<Id,Sobject>(objList);

            List<AuditEvalEngine.EvaluationResult> results = evalEngine.evaluate(objects, oldMap);

            Map<Id,AuditEvalEngine.EvaluationResult> oldEval;
            //if this is an update, we need to make sure that the criteria was met on this change, and wasn't already (oldMap)
            if (oldMap != null) {
                //build a list of the criteria met results
                List<AuditEvalEngine.EvaluationResult> metResults = new List<AuditEvalEngine.EvaluationResult>();
                for (AuditEvalEngine.EvaluationResult result : results) {
                    if ( result.criteriaMet ) {
                        metResults.add(result);
                    }
                }
                //now we need to rerun those with the old data
                oldEval = evalEngine.evaluate(metResults,oldMap);
            }

            fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {Audit_Flag__c.getSObjectType(), Audit_Scheduled_Flag__c.getSObjectType()});

            if (results.size() != 0) {

                String relatedIdField = findRelatedIdField(results[0].obj);

                //keep a list of scheduled rules that are met (to check for duplicates below)
                List<AuditEvalEngine.EvaluationResult> metScheduled = new List<AuditEvalEngine.EvaluationResult>();

                //keep a list of scheduled rules that were not met (to void existing flags)
                List<AuditEvalEngine.EvaluationResult> notMetScheduled = new List<AuditEvalEngine.EvaluationResult>();

                for (AuditEvalEngine.EvaluationResult result : results) {

                    if ( result.criteriaMet && result.rule.auditRule.rule_type__c == 'Real-Time' ) {
                        //Is this an insert or would it have not matched the criteria before this change?
                        if (oldEval==null || !oldEval.get((Id)result.obj.get('id')).criteriaMet) {
                            flagDomain.createFromResult(result, relatedIdField, uow);
                        }
                    } //if criteria met and real-time

                    if ( result.criteriaMet && result.rule.auditRule.rule_type__c == 'Scheduled' ) {
                        //Is this an insert or would it have not matched the criteria before this change?
                        if (oldEval==null || !oldEval.get((Id)result.obj.get('id')).criteriaMet) {
                            metScheduled.add(result);
                        }
                    } //if criteria met and scheduled

                    if ( !result.criteriaMet && result.rule.auditRule.rule_type__c == 'Scheduled' ) {
                        notMetScheduled.add(result);
                    } //if criteria not met and scheduled

                } //for results

                //creat scheduled flags
                scheduledFlagDomain.createFromResults(metScheduled, relatedIdField, uow, true);

                //satisfy any existing sheduled flags where the critera is no longer met
                scheduledFlagDomain.updateStatusFromResults(notMetScheduled, relatedIdField, 'Satisfied', uow);
            } //if results

            //void and reschedule any flags if the date they are based on changed, not on inserts
            if (oldMap != null) {
                voidAndReschedule(objList, oldMap, uow);
            }

            uow.commitWork();

        } catch (Exception e) {
            //make sure this doesn't error silently
            ErrorLogDomain errorLog = new ErrorLogDomain();
            errorlog.logError(e,'AuditProcessor');
        } //try/catch
    } //processTrigger


    private static void voidAndReschedule(List<SObject> objList, Map<Id,SObject> oldMap, fflib_SObjectUnitOfWork uow) {

        //determine the type of the object we are dealing with
        String objType = objList[0].getsObjectType().getDescribe().getName();

        //get the rules for these objects
        AuditRuleSelector ars = new AuditRuleSelector();
        List<AuditEvalEngine.Rule> rules = ars.selectByActiveForObject(objType);

        //build a list of *potential* reschedules
        List<AuditEvalEngine.EvaluationResult> potentials = new List<AuditEvalEngine.EvaluationResult >();

        //and a map of rules we'll use later
        Map<Id,AuditEvalEngine.Rule> ruleMap = new Map<Id,AuditEvalEngine.Rule>();

        for (AuditEvalEngine.Rule rule : rules) {
            if (rule.auditRule.custom_scheduled_date_field__c != null && rule.auditRule.custom_scheduled_date_field__c != '') {
                for (SObject obj : objList) {
                    SObject oldObj = oldMap.get((Id)obj.get('id'));
                    //did the field change?
                    if ( obj.get(rule.auditRule.custom_scheduled_date_field__c) != oldObj.get(rule.auditRule.custom_scheduled_date_field__c)) {
                        AuditEvalEngine.EvaluationResult rs = new AuditEvalEngine.EvaluationResult ();
                        rs.obj = obj;
                        rs.rule = rule;
                        potentials.add(rs);
                    }
                }
            } //if sched date field
            ruleMap.put(rule.auditRule.id, rule);
        } //for rules

        String relatedIdField = findRelatedIdField(objList[0]);

        //find any outstanding scheduled flags
        AuditScheduledFlagSelector asfs = new AuditScheduledFlagSelector();
        List<Audit_Scheduled_Flag__c> scheduled = asfs.selectByResultsPending(potentials, relatedIdField);

        //build some maps to looks stuff up in
        Map<Id,SObject> objMap = new Map<Id,SObject>(objList);
    
        //build a final list of reschedules
        List<AuditEvalEngine.EvaluationResult> reschedules = new List<AuditEvalEngine.EvaluationResult >();
        for (Audit_Scheduled_Flag__c sflag : scheduled) {
            AuditEvalEngine.EvaluationResult rs = new AuditEvalEngine.EvaluationResult ();
            rs.obj = objMap.get((Id)sflag.get(relatedIdField));
            rs.rule = ruleMap.get(sflag.audit_rule__c);
            reschedules.add(rs);
        } //for reschedules

        //now we need to void...
        scheduledFlagDomain.updateStatusFromResults(reschedules, relatedIdField, 'Void', uow);

        //..and reschedule them
        scheduledFlagDomain.createFromResults(reschedules, relatedIdField, uow, false);

    } //voidAndReschedule


    public static String determineHistoryObject(String objType) {
        String historyObj;
        if (objType.contains('__c')) {
            historyObj = objType.replace('__c','__history');
        } else {
            historyObj = objType + '__history';
        }
        return historyObj;
    } //determineHistoryObject


    /*  Overnight jobs are run with the triggers turned off, so the audit trigger does not run
        This method will attempt to rebuild the trigger data and run the rules
    */
    public static void processHistory(String userEmail, DateTime startDT, DateTime endDT, String objType, Set<Id> objIds) {

        String historyObj = determineHistoryObject(objType);

        String historyQuery = 'SELECT parentId, oldValue, newValue, field, createdDate'
            +' FROM ' + historyObj
            +' WHERE createdDate >= :startDT AND createdDate <= :endDT'
            +' AND createdBy.email = :userEmail'
            +' AND parentId IN :objIds'
            +' ORDER BY createdDate DESC';

        List<SObject> histories;                
        if (!Test.isRunningTest()) {
            histories = Database.query(historyQuery);
        } else {
            histories = testHistory;
        }

        Map<Id,List<SObject>> historyMap = new Map<Id,List<SObject>>(); 
        //build a map of the history records
        for (SObject history: histories) {
            Id objId = (Id)history.get('parentId');
            //make sure this parentId is in the map
            if ( !historyMap.containsKey(objId) ) {
                List<SObject> historyList = new List<SObject>();
                historyMap.put(objId,historyList);
            }
            historyMap.get(objId).add(history);
        } //for history

        //get the rules for these objects
        AuditRuleSelector ars = new AuditRuleSelector();
        List<AuditEvalEngine.Rule> rules = ars.selectByActiveForObject(objType);

        AuditEvalEngine aee = new AuditEvalEngine();

        //make sure we have rules to run
        if ( rules.size() != 0) {
            //build a list of fields we need to query for these rules
            String queryFields = aee.buildFieldList(rules,objType);

            //get the fields for all the objects
            Set<Id> idSet = historyMap.keySet();
            String objQuery = 'SELECT ' + queryFields + ' FROM ' + objType + ' WHERE id IN :idSet';
            List<SObject> objects = Database.query(objQuery);

            //so we can check below if the field is needed
            List<String> fieldList = queryFields.split('\\,');

            //now build a map with the old values
            Map<Id,SObject> oldObjects = new Map<Id,SObject>();
            for (SObject obj : objects) {
                //create a copy to modify
                SObject oldObj = obj.clone(true,true,true,true);
                //set all the fields with thier old values from the history for this obj
                for (SObject history: historyMap.get(obj.id)) {
                    String fieldName = (String)history.get('field');
                    if (fieldList.contains(fieldName.toLowerCase())) { //we only need old values for the fields in the rules
                        //determine the field's type
                        Schema.SoapType fieldType = aee.getFieldType(obj,fieldName);
                        //and convert the oldValue
                        if (fieldType == Schema.SoapType.Boolean) oldObj.put(fieldName, Boolean.valueOf(history.get('oldValue')) );
                        if (fieldType == Schema.SoapType.Date) oldObj.put(fieldName, Date.valueOf(history.get('oldValue')) );
                        if (fieldType == Schema.SoapType.DateTime) oldObj.put(fieldName, DateTime.valueOf(history.get('oldValue')) );
                        if (fieldType == Schema.SoapType.Double) oldObj.put(fieldName, Double.valueOf(history.get('oldValue')) );
                        if (fieldType == Schema.SoapType.Integer) oldObj.put(fieldName, Integer.valueOf(history.get('oldValue')) );
                        if (fieldType == Schema.SoapType.String) oldObj.put(fieldName, String.valueOf(history.get('oldValue')) );
                    }
                }
                oldObjects.put(oldObj.id,oldObj);
            }

            //now run the rules just like the trigger happened
            processTrigger(objects, oldObjects);

        } //if rules

    } //processHistory

    @TestVisible //for injecting test history
    private static List<litify_pm__Matter__history> testHistory = new List<litify_pm__Matter__history>();

} //class