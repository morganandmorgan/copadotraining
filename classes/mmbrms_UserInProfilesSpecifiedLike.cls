public class mmbrms_UserInProfilesSpecifiedLike
    implements mmbrms_IRuleEvaluation
{
    public List<String> profilesSpecifiedList = new List<String>();

    public Boolean isEvaluationResponseInverted;

    public Boolean evaluate()
    {
        if (isEvaluationResponseInverted == null)
        {
            isEvaluationResponseInverted =  false;
        }

        Boolean evalResponse = new mmcommon_UserInfo( UserInfo.getUserId() ).hasProfileNameLike( profilesSpecifiedList );

        return isEvaluationResponseInverted ? ! evalResponse : evalResponse;
    }
}