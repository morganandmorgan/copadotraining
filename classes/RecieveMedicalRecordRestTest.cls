@isTest
private class RecieveMedicalRecordRestTest {
  static Account acct;
  static Lawsuit__c lawsuit;
  static RestRequest req;
  static {
    acct = new Account(FirstName = 'TestFirst', LastName = 'TestLast');
    insert acct;

    lawsuit = new Lawsuit__c(name = 'Test Lawsuit', AccountId__c = acct.id);
    insert lawsuit;

    req = new RestRequest();
    RestContext.request = req;
    RestContext.response = new RestResponse();
    Map<String, Object> jsonMap = new Map<String, Object>();
    jsonMap.put('requestId', '1');
    jsonMap.put('externalId', lawsuit.id);
    jsonMap.put('type', 'pdf');
    jsonMap.put('contents', EncodingUtil.base64Encode(Blob.valueOf('The Medical Records')));

    req.requestBody = Blob.valueOf(JSON.serialize(jsonMap));
  }
	@isTest static void Post_AddsAttachment() {
		ReceiveMedicalRecordRest.DoPost();
    Attachment att = [SELECT Name, Body FROM Attachment WHERE ParentId = :lawsuit.id LIMIT 1];

    System.assertEquals('The Medical Records', att.Body.toString());
	}

  @isTest static void BlankPostReturns404() {
    Map<String, Object> jsonMap = new Map<String, Object>();
    req.requestBody = Blob.valueOf(JSON.serialize(jsonMap));

    ReceiveMedicalRecordRest.DoPost();

    System.assertEquals(404, RestContext.response.statusCode);
  }
	
}