/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GreaterThanOrEqualsPredicate extends litify_pm.BasicPredicate {
    global static litify_pm.IPredicate NewInstance(String fieldName, Date value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, Double value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, Integer value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, Long value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, String value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, Decimal value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, Datetime value) {
        return null;
    }
    global static litify_pm.IPredicate NewInstance(String fieldName, List<String> value) {
        return null;
    }
    global override String toString() {
        return null;
    }
}
