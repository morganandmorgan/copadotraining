public with sharing class mmdocusign_ConfigurationSettings
{
	private static String accountId = '';

	public static String getAccountId()
	{
		fetchSettings();
		return accountId;
	}

	private static String host = '';

	public static String getHost()
	{
		fetchSettings();
		return host;
	}

	private static String integratorKey = '';

	public static String getIntegratorKey()
	{
		fetchSettings();
		return integratorKey;
	}

	private static Boolean isEnabled = false;

	public static Boolean getIsEnabled()
	{
		fetchSettings();
		return isEnabled;
	}

	private static Boolean isEnabledInSandbox = false;

	public static Boolean getIsEnabledInSandbox()
	{
		fetchSettings();
		return isEnabledInSandbox;
	}

	private static String password = '';

	public static String getPassword()
	{
		fetchSettings();
		return password;
	}

	private static String returnUrl = '';

	public static String getReturnUrl()
	{
		fetchSettings();
		return returnUrl;
	}

	private static String signingCeremonyLandingPageTemplate = '';

	public static String getSigningCeremonyLandingPageTemplate()
	{
		fetchSettings();
		return signingCeremonyLandingPageTemplate;
	}

	private static String username = '';

	public static String getUsername()
	{
		fetchSettings();
		return username;
	}

	private static String docuSignUrlFailure;

	public static String getDocuSignUrlFailure() {
		fetchSettings();
		return docuSignUrlFailure;
	}

	private static Boolean settingsFetched = false;

	private static void fetchSettings()
	{
		if (settingsFetched == false)
		{
			// Get the settings from their final locale.
			Docusign_Integration__c settings = Docusign_Integration__c.getInstance();

			if (settings == null)
			{
				throw new mmdocusign_IntegrationExceptions.ConfigurationException('Feature\'s custom settings were not found.');
			}

			accountId = settings.DocusignAccountId__c;
			host = settings.Host__c;
			integratorKey = settings.IntegratorKey__c;
			isEnabled = settings.Enabled__c;
			isEnabledInSandbox = settings.EnabledInSandbox__c;
			password = settings.Password__c;
			returnUrl = settings.ReturnUrl__c;
			signingCeremonyLandingPageTemplate = settings.Signing_Ceremony_Landing_Page_Template__c;
			username = settings.Username__c;
			docuSignUrlFailure = settings.docuSign_URL_failure__c;
		}

		settingsFetched = true;
	}
}