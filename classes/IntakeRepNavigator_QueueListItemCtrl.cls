public with sharing class IntakeRepNavigator_QueueListItemCtrl {
  public IntakeRepNavigatorWidgetQueue__c q { get; set; }
  public List<IntakeRepNavigator_Json.Query> Queries { get; set; }
  public List<IntakeRepNavigator_Json.SortOrder> Sortorders { get; set; }
  public List<SelectOption> IntakeColumns { get; set; }
  public List<SelectOption> OperatorOptions { get; set; }
  public List<SelectOption> SortOrderOptions { get; set; }
  public List<SelectOption> EarliestCallTimes { get; set; }
  public List<SelectOption> LatestCallTimes { get; set; }
  public IntakeRepNavigator_QueueListItemCtrl() {
    String qid = ApexPages.currentPage().getParameters().get('q');
    Queries = new List<IntakeRepNavigator_Json.Query>();
    SortOrders = new List<IntakeRepNavigator_Json.SortOrder>();

    if (qid != null && qid.length() > 0) {
      q = [SELECT Id, Name, EarliestCallTime__c, LatestCallTime__c, Query__c, Sorting__c FROM IntakeRepNavigatorWidgetQueue__c WHERE Id = :qid];
      Queries = IntakeRepNavigator_Json.ParseQuery(q.Query__c);
      SortOrders = IntakeRepNavigator_Json.ParseSortOrder(q.Sorting__c);
    } else {
      q = new IntakeRepNavigatorWidgetQueue__c();
    }

    while(Queries.size() < 10) {
      Queries.add(new IntakeRepNavigator_Json.Query());
    }
    while(SortOrders.size() < 10) {
      SortOrders.add(new IntakeRepNavigator_Json.SortOrder());
    }

    PopulateEarliestCallTimes();
    PopulateLatestCallTimes();
    PopulateIntakeColumns();
    PopulateOperatorOptions();
    PopulateSortOrderOptions();
  }

  public PageReference Save() {
    upsert q;
    return null;
  }

  public PageReference Remove() {
    delete q;
    return new PageReference('/apex/IntakeRepNavigatorQueueList');
  }

  public PageReference CloneObj() {
    IntakeRepNavigatorWidgetQueue__c queue = new IntakeRepNavigatorWidgetQueue__c(Name = q.Name + ' clone',  EarliestCallTime__c = q.EarliestCallTime__c, LatestCallTime__c = q.LatestCallTime__c, Query__c = q.Query__c, Sorting__c = q.Sorting__c);
    insert queue;
    return new PageReference('/apex/IntakeRepNavigator_QueueListItem?q=' + queue.id);
  }

  private void PopulateOperatorOptions() {
    OperatorOptions = new List<SelectOption>();
    OperatorOptions.add(new SelectOption('', ''));
    OperatorOptions.add(new SelectOption('<', 'Less Than'));
    OperatorOptions.add(new SelectOption('<=', 'Less Than or Equal'));
    OperatorOptions.add(new SelectOption('=', 'Equals'));
    OperatorOptions.add(new SelectOption('<>', 'Does Not Equal'));
    OperatorOptions.add(new SelectOption('>=', 'Greater Than or Equal'));
    OperatorOptions.add(new SelectOption('>', 'Greater Than'));
  }
  private void PopulateSortOrderOptions() {
    SortOrderOptions = new List<SelectOption>();
    SortOrderOptions.add(new SelectOption('', ''));
    SortOrderOptions.add(new SelectOption('DESC', 'descending (z-a, 100-1, today-yesterday)'));
    SortOrderOptions.add(new SelectOption('DESC NULLS LAST', 'desc nulls last (z-a, 100-1, today-yesterday)'));
    SortOrderOptions.add(new SelectOption('ASC', 'ascending (a-z, 1-100, yesterday-today)'));
    SortOrderOptions.add(new SelectOption('ASC NULLS LAST', 'asc nulls last (a-z, 1-100, yesterday-today)'));
  }
  private void PopulateIntakeColumns() {
    IntakeColumns = new List<SelectOption>();

    Schema.DescribeSObjectResult description = Intake__c.sObjectType.getDescribe();
    Map<String, Schema.SObjectField> fields = description.fields.getMap();
    for (String fieldName : fields.keySet()) {
      IntakeColumns.add(new SelectOption(fieldName, '[Intake] ' + fields.get(fieldName).getDescribe().getLabel()));

    }

    description = Account.sObjectType.getDescribe();
    fields = description.fields.getMap();
    for (String fieldName : fields.keySet()) {
      IntakeColumns.add(new SelectOption('Client__r.' + fieldName, '[Account] ' + fields.get(fieldName).getDescribe().getLabel()));
    }
    
    IntakeColumns.sort();

    IntakeColumns.add(0, new SelectOption('', ''));
  }
  private void PopulateEarliestCallTimes() {
    EarliestCallTimes = new List<SelectOption>();
    for(Integer i = 7; i <= 22; i++) {
      String label = '';
      if (i == 12) {
        label = '12pm';
      } else if (i > 12) {
        label = (i - 12) + 'pm';
      } else {
        label = i + 'am';
      }

      EarliestCallTimes.add(new SelectOption(i.format(), label));
    }
  }
  private void PopulateLatestCallTimes() {
    LatestCallTimes = EarliestCallTimes;
  }
}