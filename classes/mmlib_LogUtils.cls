public class mmlib_LogUtils {

  private mmlib_LogUtils() {}

  public static void logSaveErrors(Database.SaveResult[] saveResults) {
    for (Database.SaveResult result : saveResults) {
      if (result.isSuccess()) {
        continue;
      }
      System.debug(LoggingLevel.ERROR, 'Error occurred inserting ' + result.getId() + ' :');
      for (Database.Error err : result.getErrors()) {
        System.debug(LoggingLevel.ERROR, 'Error code: ' + err.getStatusCode() + ', Fields: ' + err.getFields() + ', Message: ' + err.getMessage());
      }
    }
  }
}