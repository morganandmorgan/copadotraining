/**
 * AuditFlagScheduled
 * @description Scheduled apex to process Audit_Scheduled_Flag__c's
 * @author Matt Terrill
 * @date 9/23/2019 - Batchable refactor 1/8/2020
 */
global without sharing class AuditFlagScheduled implements Database.Batchable<SObject>, Schedulable {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        DateTime endOfToday = DateTime.newInstance(Date.today(), Time.newInstance(23,59,59,0));
        //get the flags that need evaluation today or earlier
        return Database.getQueryLocator([SELECT id, scheduled_date__c, status__c, audit_rule__c, user__c, litify_pm_Matter__c, litify_pm_Negotiation__c
            FROM Audit_Scheduled_Flag__c
            WHERE scheduled_date__c <= :endOfToday AND status__c = 'Pending']);
        
    } //start (batch)

    global void execute(SchedulableContext sc) {
        //first lets clean up orphaned flags
        AuditScheduledFlagDomain domain = new AuditScheduledFlagDomain();
        domain.voidOrphanedFlags();
        //now re-evaluate scheduled flags in batches
        Database.executeBatch(new AuditFlagScheduled(), 5);
    } //execute scheduled

    global void execute(Database.BatchableContext bc, List<Audit_Scheduled_Flag__c> scheduledFlags) {
        //evaluate them
        AuditEvalEngine evalEngine = new AuditEvalEngine();
        List<AuditEvalEngine.EvaluationResult> results =  evalEngine.evaluate(scheduledFlags);

        AuditFlagDomain flagDomain = new AuditFlagDomain();

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {Audit_Flag__c.getSObjectType(), Audit_Scheduled_Flag__c.getSObjectType()});

        //insert any flags required, and update the scheduled flag's status
        for (AuditEvalEngine.EvaluationResult result : results) {

            //re-evaluate if it is still active...
            if ( result.rule.auditRule.isActive__c ) {
                if ( result.criteriaMet ) {
                    flagDomain.createFromResult(result, AuditProcessor.findRelatedIdField(result.obj), uow);
                    result.scheduledFlag.status__c = 'Flagged';
                } else {
                    result.scheduledFlag.status__c = 'Satisfied';
                }
            } else { //void the scheduled flag if the rule is not longer active
                result.scheduledFlag.status__c = 'Void';
            }

            uow.registerDirty(result.scheduledFlag);
        } //for results

        uow.commitWork();
    } //execute batch

    global void finish(Database.BatchableContext bc) {
    } //finish

} //class