/**
 *  mmlib_GenericIterableBatch
 */
public class mmlib_GenericIterableBatch 
    implements Database.Batchable<Object>, Database.AllowsCallouts, mmlib_IExecuteable
{
    public class GenericBatchException extends Exception { }

    public interface IGenericExecuteBatch
    {
        void run(Iterable<Object> scope);
    }

    private Type theGenericExecutorType = null;
    private Iterable<Object> theIterable = null;
    private Integer batchSize = 200;
    private Set<Id> idSet = null;
    private mmlib_IExecuteable genericBatchToRunAfterwards = null;

    private mmlib_GenericIterableBatch() { }

    public mmlib_GenericIterableBatch(Type theGenericExecutorType
                             , Iterable<Object> theIterable)
    {
        // verify that the Type if is implements IGenericExecuteBatch
        try
        {
            IGenericExecuteBatch genericExecutorRunner = (IGenericExecuteBatch)theGenericExecutorType.newInstance();
        }
        catch (exception e)
        {
            throw new GenericBatchException('theGenericExecutorType parameter Type must implement the interface \'' + mmlib_GenericIterableBatch.IGenericExecuteBatch.class.getName() + '\'');
        }

        this.theGenericExecutorType = theGenericExecutorType;

        if (theIterable == null)
        {
            throw new GenericBatchException('theIterable must be supplied.');
        }
        this.theIterable = theIterable;
    }

    public Iterable<Object> start(Database.BatchableContext context)
    {
        return this.theIterable;
    }

    public void execute(Database.BatchableContext context, Iterable<Object> scope)
    {
        try
        {
            IGenericExecuteBatch genericExecutorRunner = (IGenericExecuteBatch)theGenericExecutorType.newInstance();

            genericExecutorRunner.run(scope);
        }
        catch (Exception e)
        {
            System.debug('Error executing batch -- ' + e);
            System.debug(e.getStackTraceString());
        }
    }

    public void finish(Database.BatchableContext context)
    {
        if (this.genericBatchToRunAfterwards != null)
        {
            this.genericBatchToRunAfterwards.execute();
        }
    }

    public mmlib_GenericIterableBatch setBatchSizeTo(final integer newBatchSize)
    {
        this.batchSize = newBatchSize;

        return this;
    }

    public mmlib_GenericIterableBatch setGenericBatchToRunAfterwards(mmlib_IExecuteable genericBatchToRunAfterwards)
    {
        this.genericBatchToRunAfterwards = genericBatchToRunAfterwards;

        return this;
    }

    public void execute()
    {
        Database.executeBatch(this, batchSize);
    }
}