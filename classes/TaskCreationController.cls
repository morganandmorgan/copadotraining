public class TaskCreationController {

    private final String INTAKE_ID;
    private final String TASK_SRC;
    private final String RET_URL;

    public Task taskRcd { get; private set; }

    public TaskCreationController() {
        INTAKE_ID = ApexPages.currentPage().getParameters().get('Id');
        TASK_SRC = ApexPages.currentPage().getParameters().get('taskSrc');
        RET_URL = ApexPages.currentPage().getparameters().get('retURL');

        List<Intake__c> intakes = [SELECT RecordType.DeveloperName, Case_Type__c, Marketing_Source__c, Handling_Firm__c FROM Intake__c WHERE Id = :INTAKE_ID];
        if (!intakes.isEmpty()) {
            Intake__c intake = intakes.get(0);

            if (getIsMailout()) {
                taskRcd = createDefaultMailoutTask(intake);
            }
            else if (getIsCallback()) {
                taskRcd = createDefaultCallbackTask(intake);
            }
        }

        if (taskRcd == null) {
            taskRcd = createDefaultTask();
        }
    }

    private static Task createDefaultMailoutTask(Intake__c intake) {
        Id oAssignedToId = null;
        String subject = intake.Case_Type__c + ' - ' + intake.Marketing_Source__c;

        List<Mailout_Task_Default_Assignment__mdt> assignments = [SELECT Default_Task_Owner_Id__c FROM Mailout_Task_Default_Assignment__mdt WHERE Intake_Record_Type_API_Name__c = :intake.RecordType.DeveloperName AND Handling_Firm__c = :intake.Handling_Firm__c];
        if (!assignments.isEmpty()) {
            oAssignedToId = assignments.get(0).Default_Task_Owner_Id__c;
        }

        return new Task(WhatId = intake.Id, ActivityDate = Date.today()+1, Mail_Out_Status__c = 'Pending', Type = 'Mail Out', Subject = subject, OwnerId = oAssignedToId );
    }

    private static Task createDefaultCallbackTask(Intake__c intake) {
        return new Task(WhatId = intake.Id, Type = 'Callback', Status = 'Not Started', Subject = 'Callback Request', OwnerId = UserInfo.getUserId(), IsReminderSet = true);
    }

    private static Task createDefaultTask() {
        return new Task();
    }

    public Boolean getIsMailout() {
        return TASK_SRC == 'mailout';
    }

    public Boolean getIsCallback() {
        return TASK_SRC == 'callback';
    }

    public PageReference createTask() {
        Savepoint sp = Database.setSavepoint();
        try {
            insert taskRcd;

            updateCccIntake();
        } catch(Exception e) {
            Database.rollback(sp);

            taskRcd.Id = null;

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }

        return new PageReference(RET_URL == null ? '/'+INTAKE_ID : RET_URL).setRedirect(true);
    }

    private void updateCccIntake() {
       if (getIsMailout() && taskRcd.WhatId != null && taskRcd.WhatId.getSObjectType() == Intake__c.SObjectType) {
            List<Intake__c> intakes = [SELECT Id, Status__c, Status_Detail__c FROM Intake__c WHERE Id = :taskRcd.WhatId AND RecordType.DeveloperName = 'CCC'];
            for (Intake__c intake : intakes) {
                intake.Status__c = 'Sign Up In Progress';
                intake.Status_Detail__c = 'Mailout Sent';
            }
            update intakes;
        }
    }

    public PageReference cancel() {
        PageReference pageRef = new PageReference('/'+INTAKE_ID);
        pageRef.setRedirect(true);
        return pageRef;
    }
}