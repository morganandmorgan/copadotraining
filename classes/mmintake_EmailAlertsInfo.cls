public class mmintake_EmailAlertsInfo
{
    public String getBaseUrl() {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/';
    }

    public Intake__c intake { get; set; }
    public Account client { get; set; }
    public litify_pm__Matter__c matter { get; set; }

    public Schema.FieldSet defaultFieldsetNonEmpty { get; set; }
    public Schema.FieldSet intakeFieldsetNonEmpty { get; set; }
    public Schema.FieldSet marketingInfoFieldsetNonEmpty { get; set; }

    public List<Schema.FieldSetMember> getDefaultFieldsetMemberNonEmpty()
    {
        return this.defaultFieldsetNonEmpty == null ? new List<Schema.FieldSetMember>() : this.defaultFieldsetNonEmpty.getFields();
    }

    public List<Schema.FieldSetMember> getIntakeFieldsetMemberNonEmpty()
    {
        return this.intakeFieldsetNonEmpty == null ? new List<Schema.FieldSetMember>() : this.intakeFieldsetNonEmpty.getFields();
    }

    public List<Schema.FieldSetMember> getMarketingInfoFieldsetMemberNonEmpty()
    {
        return this.marketingInfoFieldsetNonEmpty == null ? new List<Schema.FieldSetMember>() : this.marketingInfoFieldsetNonEmpty.getFields();
    }

    public List<IntakeInvestigationEvent__c> allIncidentsForThisIntake { get; set; } { allIncidentsForThisIntake = new List<IntakeInvestigationEvent__c>(); }
    public List<IntakeInvestigationEvent__c> allIntakeInvestigationsForThisIncident { get; set; } { allIntakeInvestigationsForThisIncident = new List<IntakeInvestigationEvent__c>(); }
    public List<IntakeInvestigationEvent__c> allCanceledIncidentsForThisIntake { get; set; } { allCanceledIncidentsForThisIntake = new List<IntakeInvestigationEvent__c>(); }
    public List<Event> additionalCaseNotes { get; set; } { additionalCaseNotes = new List<Event>(); }
}