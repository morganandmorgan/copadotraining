public with sharing class SkuidQuestionnaireRedirectExt {
  ID QuestionnaireID;

	public SkuidQuestionnaireRedirectExt(ApexPages.StandardController controller) {
		QuestionnaireID = controller.getRecord().Id;
	}

  public String getIframeUrl() {
    MM_Questionnaire__c questionnaire = [SELECT Id, Intake__c FROM MM_Questionnaire__c WHERE ID = :QuestionnaireID];

    String u = '/apex/skuid__ui?page=CccQuestionnaire&intakeid=' + questionnaire.Intake__c + '&questionnaireId=' + questionnaire.id;

    return u;
 
  }
}