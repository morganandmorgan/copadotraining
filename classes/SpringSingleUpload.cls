public class SpringSingleUpload {

    public string url { get; set; }

    public SpringSingleUpload(ApexPages.StandardController stdController) {
        try {
            Spring_CM_Matter_Forms__mdt config = [select UploadUrl__c from Spring_CM_Matter_Forms__mdt limit 1];
            this.url = config.UploadUrl__c + '&sessionId=' + UserInfo.getSessionId() + '&userId=' + UserInfo.getUserId() + '&host=' + ApexPages.currentPage().getHeaders().get('Host') + '&sfId=' + stdController.getRecord().Id + '&nav=false';
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    public PageReference redirect() {
        PageReference pageRef = new PageReference(this.url);
        return pageRef;
    }
}