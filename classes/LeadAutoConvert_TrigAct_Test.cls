@isTest
private class LeadAutoConvert_TrigAct_Test {

    static {
        Lead lead = new Lead();
        lead.FirstName = 'Unit';
        lead.LastName = 'Test';
        insert lead;

        Task task = new Task();
        task.Subject = 'Submitted Form "forthepeople Case Evaluation Form"';
        task.WhoId = lead.Id;
        task.Status = 'Not Started';
        task.Priority = 'Normal';
        task.put('Description', 'First Name: TEST');
        insert task;
    }

    @IsTest
    private static void coverage() {
        LeadAutoConvert_TrigAct.coverage();
    }
}