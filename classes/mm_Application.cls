/**
 *  mm_Application
 */
public class mm_Application
{
    public class mm_ApplicationException extends Exception { }

    @TestVisible
    private static List<SObjectType> unitOfWorkFactorySObjectTypeList;

    @TestVisible
    private static Map<Type, Type> serviceFactoryTypeMap;

    @TestVisible
    private static Map<SObjectType, Type> selectorFactoryTypeMap;

    @TestVisible
    private static Map<SObjectType, Type> domainFactoryTypeMap;

    static
    {
        Map<String, Schema.SObjectType> schemaGlobalDescribe = Schema.getGlobalDescribe();

        unitOfWorkFactorySObjectTypeList = new List<SObjectType>();

        serviceFactoryTypeMap = new Map<Type, Type>();

        selectorFactoryTypeMap = new Map<SObjectType, Type>();

        domainFactoryTypeMap = new Map<SObjectType, Type>();

        // Get all of the Application Factory config records and sort through them.
        // Ensure to order by SortOrder__c before DeveloperName.  For most records, it won't matter.
        //   For UnitOfWork records, the SortOrder__c needs to take precedence.
        list<ApplicationFactory__mdt> factoryConfigRecords = [select Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName
                                                                   , Type__c, SObjectType__c, Interface__c, SortOrder__c, ClassToInject__c
                                                                from ApplicationFactory__mdt
                                                               order by Type__c, SortOrder__c, DeveloperName];

        for ( ApplicationFactory__mdt factoryConfigRecord : factoryConfigRecords )
        {
            if ( factoryConfigRecord.Type__c == 'Domain' )
            {
                // Find the SObjectType and add it to the list.
                if ( schemaGlobalDescribe.containsKey( factoryConfigRecord.SObjectType__c ) )
                {
                    domainFactoryTypeMap.put( schemaGlobalDescribe.get( factoryConfigRecord.SObjectType__c ), Type.forName( factoryConfigRecord.ClassToInject__c ) );
                }
                else
                {
                    throw new mm_ApplicationException( 'Application Factory configuration \'' + factoryConfigRecord.DeveloperName + '\' specification of SObjectType '
                                                            + factoryConfigRecord.SObjectType__c + ' was not found.  Please have the system administrator make adjustments.');
                }
            }
            else if ( factoryConfigRecord.Type__c == 'Selector' )
            {
                // Find the SObjectType and add it to the list.
                if ( schemaGlobalDescribe.containsKey( factoryConfigRecord.SObjectType__c ) )
                {
                    selectorFactoryTypeMap.put( schemaGlobalDescribe.get( factoryConfigRecord.SObjectType__c ), Type.forName( factoryConfigRecord.ClassToInject__c ) );
                }
                else
                {
                    throw new mm_ApplicationException( 'Application Factory configuration \'' + factoryConfigRecord.DeveloperName + '\' specification of SObjectType '
                                                            + factoryConfigRecord.SObjectType__c + ' was not found.  Please have the system administrator make adjustments.');
                }
            }
            else if ( factoryConfigRecord.Type__c == 'Service' )
            {
                serviceFactoryTypeMap.put( Type.forName( factoryConfigRecord.Interface__c ), Type.forName( factoryConfigRecord.ClassToInject__c ) );
            }
            else if ( factoryConfigRecord.Type__c == 'UnitOfWork' )
            {
                // Find the SObjectType and add it to the list.
                if ( schemaGlobalDescribe.containsKey( factoryConfigRecord.SObjectType__c ) )
                {
                    unitOfWorkFactorySObjectTypeList.add( schemaGlobalDescribe.get( factoryConfigRecord.SObjectType__c ) );
                }
                else
                {
                    throw new mm_ApplicationException( 'Application Factory configuration \'' + factoryConfigRecord.DeveloperName + '\' specification of SObjectType '
                                                            + factoryConfigRecord.SObjectType__c + ' was not found.  Please have the system administrator make adjustments.');
                }
            }
        }
    }

    /**
     *  UnitOfWork Factory
     *
     *  Configure and create the UnitOfWorkFactory for this Application
     */
    public static final mm_Application.UnitOfWorkFactory UnitOfWork = new mm_Application.UnitOfWorkFactory( unitOfWorkFactorySObjectTypeList );

    /**
     *  Service Factory
     *
     *  Configure and create the ServiceFactory for this Application
     */
    public static final fflib_Application.ServiceFactory Service = new fflib_Application.ServiceFactory( serviceFactoryTypeMap );

    /**
     *  Configure and create the SelectorFactory for this mm_Application
     */
    public static final fflib_Application.SelectorFactory Selector = new fflib_Application.SelectorFactory( selectorFactoryTypeMap );

    /**
     * Configure and create the DomainFactory for this  mm_Application
     */
    public static final fflib_Application.DomainFactory Domain = new fflib_Application.DomainFactory( mm_Application.Selector, domainFactoryTypeMap );

    /**
     * Class implements a Unit of Work factory
     *
     * ***NOTE*** This inner class will be replaced by the full fflib_Application class
     *              once the fflib framework is fully adopted
     *
     **/
    public class UnitOfWorkFactory
    {
        private List<SObjectType> m_objectTypes;

        private mmlib_ISObjectUnitOfWork m_mockUow;

        /**
         * Constructs a Unit Of Work factory
         *
         * @param objectTypes List of SObjectTypes in dependency order
         **/
        public UnitOfWorkFactory(List<SObjectType> objectTypes)
        {
            m_objectTypes = objectTypes.clone();
        }

        /**
         * Returns a new mmlib_ISObjectUnitOfWork configured with the
         *   SObjectType list provided in the constructor, returns a Mock implementation
         *   if set via the setMock method
         *
         * @return a mmlib_ISObjectUnitOfWork UOW
         **/
        public mmlib_ISObjectUnitOfWork newInstance()
        {
            // Mock?
            if(m_mockUow!=null)
                return m_mockUow;
            return new mmlib_SObjectUnitOfWork(m_objectTypes);
        }

        /**
         * Returns a new fflib_SObjectUnitOfWork configured with the
         *   SObjectType list provided in the constructor and the fflib_SObjectUnitOfWork.IDML
         *   passed as a parameter, returns a Mock implementation
         *   if set via the setMock method
         *
         * @param fflib_SObjectUnitOfWork.IDML the IDML preferred
         * @return a mmlib_ISObjectUnitOfWork UOW
         **/
        public mmlib_ISObjectUnitOfWork newInstance(fflib_SObjectUnitOfWork.IDML dml)
        {
            // Mock?
            if(m_mockUow!=null)
                return m_mockUow;
            if ( dml == null )
            {
                throw new mm_ApplicationException('No fflib_SObjectUnitOfWork.IDML instance provided as parameter.');
            }
            return new mmlib_SObjectUnitOfWork(m_objectTypes, dml);
        }

        /**
         * Returns a new mmlib_ISObjectUnitOfWork configured with the
         *   SObjectType list specified, returns a Mock implementation
         *   if set via the setMock method
         *
         * @remark If mock is set, the list of SObjectType in the mock could be different
         *         then the list of SObjectType specified in this method call
         **/
        public mmlib_ISObjectUnitOfWork newInstance(List<SObjectType> objectTypes)
        {
            // Mock?
            if(m_mockUow!=null)
                return m_mockUow;
            //return new fflib_SObjectUnitOfWork(m_objectTypes);
            return new mmlib_SObjectUnitOfWork(m_objectTypes);
        }

        @TestVisible
        private void setMock(mmlib_ISObjectUnitOfWork mockUow)
        {
            m_mockUow = mockUow;
        }
    }

}