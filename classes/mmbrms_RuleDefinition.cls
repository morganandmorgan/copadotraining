public with sharing class mmbrms_RuleDefinition
{
    public Boolean defaultResponse = null;

    public List<mmbrms_RuleDefinition.RuleSegment> segmentList = new List<mmbrms_RuleDefinition.RuleSegment>();

    public mmbrms_RuleDefinition()
    {
        // Code here.
    }

    public class RuleSegment
    {
        public String ruleType = null;
        public String ruleSetupJson = null;
        public Set<mmbrms_RulesEngine.RuleInputHint> ruleInputHints = null;
    }
}