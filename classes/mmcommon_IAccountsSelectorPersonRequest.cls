/**
 *  mmcommon_IAccountsSelectorPersonRequest
 */
public interface mmcommon_IAccountsSelectorPersonRequest
{
    mmcommon_IAccountsSelectorPersonRequest setFirstName( String firstName );
    mmcommon_IAccountsSelectorPersonRequest setLastName( String lastName );
    mmcommon_IAccountsSelectorPersonRequest setEmail( String email );
    mmcommon_IAccountsSelectorPersonRequest setPhoneNumber( String phoneNumber );
    mmcommon_IAccountsSelectorPersonRequest setBirthDate( Date birthDate );
    mmcommon_IAccountsSelectorPersonRequest setSsn_Last4( String ssn_Last4 );

    String getFirstName();
    String getLastName();
    String getEmail();
    String getPhoneNumber();
    Date getBirthDate();
    String getSsn_Last4();
}