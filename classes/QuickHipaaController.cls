public without sharing class QuickHipaaController {

    public class MatterData {
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String name {get;set;}
        @AuraEnabled
        public String street {get;set;}
        @AuraEnabled
        public String city {get;set;}
        @AuraEnabled
        public String state {get;set;}
        @AuraEnabled
        public String postalCode {get;set;}
    }

    @AuraEnabled(cacheable=true)
    public static MatterData getMatterData(Id matterId) {

        MatterData md = new MatterData();

        litify_pm__matter__c matter = [SELECT id, litify_pm__client__r.name, litify_pm__client__r.shippingStreet, litify_pm__client__r.shippingCity, litify_pm__client__r.shippingState, litify_pm__client__r.shippingPostalCode FROM litify_pm__matter__c WHERE id = :matterId];

        md.name = matter.litify_pm__client__r.name;
        md.street = matter.litify_pm__client__r.shippingStreet;
        md.city = matter.litify_pm__client__r.shippingCity;
        md.state = matter.litify_pm__client__r.shippingState;
        md.postalCode = matter.litify_pm__client__r.shippingPostalCode;

        return md;

    } //getMatterData


    private static litify_docs__Template__c findTemplate(String state) {
        String filter = '%' + state + ' Quick HIPAA';
        List<litify_docs__Template__c> template = [SELECT id, name FROM litify_docs__Template__c WHERE name LIKE :filter];

        if (template.size() == 0) {
            return null;
        } else {
            return template[0];
        }
    } //findTemplate


    private static String getLightiningSessionId() {
        String content;
        if (!Test.isRunningTest()) { //getContent doesn't work in tests
            content = Page.SessionIdForLightning.getContent().toString();
        } else {
            content = 'Start_Of_Session_Id123mockSessionId456End_Of_Session_Id';
        }
        String sessionId = content.substringBetween('Start_Of_Session_Id', 'End_Of_Session_Id');
        return sessionId;
    } //getLightningSessionId


    @AuraEnabled(cacheable=false)
    public static String sendMail(String jsMatterData) {

        //Deserialize the data from the UI
        MatterData matterData = (MatterData)Json.deserialize(jsMatterData, MatterData.class);
        //sendEmail( matterData.toString() );

        //figure out what template to send
        litify_docs__Template__c template = findTemplate(matterData.state);

        if (template == null) {
            return 'Mail NOT sent.  No HIPAA template for state: "' + matterData.state + '" found.';
        }

        //build the data for the merge
        Map<String,Id> sourceIds = new Map<String,Id>();
        sourceIds.put('litify_docs__Template__c', template.id);
        sourceIds.put('litify_pm__Matter__c', matterData.id);

        String filename = matterData.name + '||' + UserInfo.getName() + '.docx';

        //attempt to do the merge
        Id fileInfoId;
        try {
            DocsHelper dh = new DocsHelper();
            dh.sessionId = getLightiningSessionId();
            fileInfoId = dh.singleMerge(sourceIds, filename);
        } catch (Exception e) {
            return e.getMessage() + ' - ' + e.getStackTraceString();
        }

        //create the mailing record
        Mailing__c m = new Mailing__c();

        List<Map<String,String>> documents = new List<Map<String,String>>();
        Map<String,String> docMap = new Map<String,String>();
        docMap.put('id', fileInfoId);
        documents.add(docMap);

        m.documents__c = Json.serialize(documents);

        m.matter__c = matterData.Id;

        m.recipient_Name__c = matterData.name;
        m.recipient_Street__c = matterData.street;
        m.recipient_City__c = matterData.city;
        m.recipient_State__c = matterData.state;
        m.recipient_Postal_Code__c = matterData.postalCode;
        m.type__c = 'First Class';
        m.return_envelope__c = true;

        m.status__c = 'Pending';

        insert m;

        return 'Success';
    } //sendMail

} //class