public with sharing class mmwiz_QuestionnaireServiceImpl
    implements mmwiz_IQuestionnaireService
{
    private PageReference pageRef = null;

    public mmwiz_QuestionnaireModel findQuestionnaire( string questionnaireToken, PageReference pageRef, String sessionGuid )
    {
        this.pageRef = pageRef;

        mmwiz_QuestionnaireModel output = null;

        if ( String.isNotBlank( questionnaireToken ) )
        {
            List<mmwiz_AbstractBaseModel> baseModelList = mmwiz_Serializer.deserialize(new Set<String> { questionnaireToken });

            if ( ! baseModelList.isEmpty()
                && baseModelList.get(0) instanceOf mmwiz_QuestionnaireModel )
            {
                output = (mmwiz_QuestionnaireModel) baseModelList.get(0);

                // How do we inject the SetupModel into the Questionnaire?
                // * As a single instance at the Questionnaire level?
                // * As many instances at the Page level?
                if (String.isNotBlank(sessionGuid))
                {
                    output.addSetupModel( new mmwiz_SetupPreviousSelectionModel(sessionGuid) );
                }

                evaluateBusinessRules( output );

                updateActionDisplayLinkModelsIfNeeded( output );
            }
        }

        return output;
    }

    private void evaluateBusinessRules( mmwiz_AbstractBaseModel currentNode )
    {
        // Need to loop through all nodes
            // if the node has busRules, then process them
                // if the busRules come back "false", then remove that node
                // if the busRules come back "true" for a node, then recursively process that node's child elements
        set<String> childModelTokensToRemoveSet = new set<String>();

        list<Integer> childModelPositionsToRemove = new list<Integer>();

        list<mmwiz_AbstractBaseModel> childModelCloneList = currentNode.childModels.clone();

        integer listIndex = 0;

        boolean ruleEvalResponse = false;

        for ( mmwiz_AbstractBaseModel childModel : childModelCloneList )
        {
            system.debug( 'childModel.getBusinessRulesList() for childModel \'' + childModel.getToken() + '\' is ' + childModel.getBusinessRuleTokens() );

            if ( ! childModel.getBusinessRuleTokens().isEmpty() )
            {
                string firstRuleToken = childModel.getBusinessRuleTokens()[0];

                ruleEvalResponse = mmbrms_RulesEngine.newInstance( firstRuleToken ).evaluate();

                system.debug( 'ruleEvalResponse == ' + ruleEvalResponse );

                if ( ruleEvalResponse )
                {
                    // Check the child models of this model
                    evaluateBusinessRules( childModel );
                }
                else
                {
                    // business rules response is false.  remove this model
                    childModelTokensToRemoveSet.add( childModel.getToken() );

                    // system.debug( 'Index - ' + listIndex + ' element is ' + currentNode.childModels[ listIndex ] );

                    currentNode.childModels.remove( listIndex );

                    // Now that the childModel has been removed, the index needs to go down by 1.
                    --listIndex;
                }
            }
            else
            {
                // This model had no business rules to evaluate so it stays in the collection.  Now evaluate the child records of this model.
                evaluateBusinessRules( childModel );
            }

            ++listIndex;
        }

        system.debug( 'childModelTokensToRemoveSet == ' + childModelTokensToRemoveSet );

        if ( ! childModelTokensToRemoveSet.isEmpty() )
        {
            set<string> existingWithTokensRemovedSet = new set<string>( currentNode.childTokens );

            existingWithTokensRemovedSet.removeAll( childModelTokensToRemoveSet );

            currentNode.childTokens = new list<string>( existingWithTokensRemovedSet );
        }
    }

    private void updateActionDisplayLinkModelsIfNeeded( mmwiz_AbstractBaseModel currentNode )
    {
        // find all of the mmwiz_ActionDisplayLinkModel classes and see if the
        List<mmwiz_AbstractBaseModel> actionDisplayLinkModelList = currentNode.findAllNodesByType( mmwiz_ActionDisplayLinkModel.class );

        mmwiz_ActionDisplayLinkModel actionDisplayLink = null;

        for ( mmwiz_AbstractBaseModel actionDisplayLinkModel : actionDisplayLinkModelList )
        {
            actionDisplayLink = (mmwiz_ActionDisplayLinkModel)actionDisplayLinkModel;

            if ( actionDisplayLink.getIsOriginalURLParametersIncludedInLink() )
            {
                actionDisplayLink.addPageParametersToLink( pageRef );
            }
        }
    }

    public list<mmwiz_AbstractActionModel> processResponseActions( String questionnaireToken, String pageToken,  map<string, list<string>> questionResponseMap, PageReference pageRef, String sessionGuid )
    {
        this.pageRef = pageRef;

        mmlib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        mmwiz_QuestionnaireModel questionnaireModel = findQuestionnaire( questionnaireToken, pageRef, sessionGuid );

        map<String, QuestionAndAnswer__c> existingAnswers = new map<String, QuestionAndAnswer__c>();
        for (QuestionAndAnswer__c qa : mmwiz_QuestionAndAnswerSelector.newInstance().selectBySessionGuid(new set<string> { sessionGuid })) {
            existingAnswers.put('' + qa.QuestionnaireToken__c + '_' + qa.QuestionToken__c, qa);
        }

        list<mmwiz_AbstractActionModel> uiActionModelList = new list<mmwiz_AbstractActionModel>();

        system.debug( 'findResponseActions mark 1 : questionnaireToken == '+ questionnaireToken );
        system.debug( 'findResponseActions mark 1 : pageToken == '+ pageToken);
        system.debug( 'findResponseActions mark 1 : questionResponseMap.keyset() == ' + questionResponseMap.keyset());

        for ( mmwiz_AbstractBaseModel pageModel : questionnaireModel.childModels )
        {
            system.debug( 'findResponseActions mark 2 : pageModel.token == '+ pageModel.getToken());

            if ( pageModel.getToken().equalsIgnoreCase( pageToken ) )
            {
                system.debug( 'findResponseActions mark 3');

                list<mmwiz_AbstractBaseModel> answerModelsSelected = new list<mmwiz_AbstractBaseModel>();

                for ( mmwiz_AbstractBaseModel questionModel : pageModel.childModels )
                {
                    system.debug( 'findResponseActions mark 4 : questionModel.token == ' + questionModel.getToken() );
                    system.debug( 'findResponseActions mark 4 : contained in keyset? == ' + questionResponseMap.keyset().contains( questionModel.getToken() )  );

                    if ( questionResponseMap.keyset().contains( questionModel.getToken() ) )
                    {
                        system.debug( 'findResponseActions mark 5');

                        list<string> answerTokenToFind = questionResponseMap.get( questionModel.getToken() );

                        system.debug( 'findResponseActions mark 5 : answerTokenToFind == ' + answerTokenToFind);

                        map<string, mmwiz_AbstractBaseModel> modelByToken = new map<string, mmwiz_AbstractBaseModel>();
                        for ( mmwiz_AbstractBaseModel answerModel : questionModel.childModels )
                        {
                            modelByToken.put(answerModel.getToken().toLowerCase(), answerModel);
                        }
                        for (String s : answerTokenToFind) {
                            mmwiz_AbstractBaseModel mod = String.isEmpty(s) ? null : modelByToken.get(s.toLowerCase());
                            if (mod == null) continue;
                            answerModelsSelected.add(mod);

                            for ( mmwiz_AbstractBaseModel actionAsBaseModel : mod.actionModels )
                            {
                                mmwiz_AbstractActionModel actionModel = (mmwiz_AbstractActionModel)actionAsBaseModel;

                                system.debug( 'findResponseActions mark 6 : actionModel.token == ' + actionModel.getToken() + ' --- ' + actionModel.getIsUIBasedAction());

                                if ( actionModel.getIsUIBasedAction() )
                                {
                                    uiActionModelList.add( actionModel );
                                }
                            }
                        } // end of answerModel for loop
                    }

                    // process the question's specific actions here
                    for ( mmwiz_AbstractBaseModel actionAsBaseModel : questionModel.actionModels )
                    {
                        mmwiz_AbstractActionModel actionModel = (mmwiz_AbstractActionModel)actionAsBaseModel;

                        if ( actionModel instanceOf mmwiz_ActionGenerateRecord_WizardAnswer )
                        {
                            mmwiz_ActionGenerateRecord_WizardAnswer wizAnswerGenerateRecAction = (mmwiz_ActionGenerateRecord_WizardAnswer)actionModel;
                            
                            wizAnswerGenerateRecAction.setQuestionText( questionModel.getDisplayText() );
                            wizAnswerGenerateRecAction.setQuestionToken( questionModel.getToken() );
                            
                            wizAnswerGenerateRecAction.setSessionGuid( sessionGuid );
                            
                            // TODO - support multiple?  delimiter? text input answers?
                            List<String> selAnswers = new List<String>();
                            for (mmwiz_AbstractBaseModel selAnswer : answerModelsSelected) {
                                selAnswers.add(selAnswer.getDisplayText());
                            }
                            wizAnswerGenerateRecAction.setAnswer( String.join(selAnswers, ';') );
                            
                            wizAnswerGenerateRecAction.setQuestionnaireToken( questionnaireModel.getToken() );

                            QuestionAndAnswer__c answer = existingAnswers.get(questionnaireModel.getToken() + '_' + questionModel.getToken());
                            if (answer == null) {
                                answer = (QuestionAndAnswer__c)wizAnswerGenerateRecAction.generateRecord();
                            } else {
                                answer.Answer__c = wizAnswerGenerateRecAction.getAnswer();
                            }
                            uow.register( answer );
                        }
                    } // end of processing the question's specific actions

                } // end of questionModel for-loop

                // process the page's specific actions here

                break;
            }
        }

        try
        {
            uow.commitWork();
        }
        catch (Exception e)
        {
            system.debug( e );
            system.debug( e.getStackTraceString() );
        }

        return uiActionModelList;
    }
}