@isTest
public with sharing class mmdice_QueueRestApiTest
{
    @testSetup
    static void setup()
    {
        // Create some accounts
        List<Account> personAccounts = new List<Account>();
        Id personAccountRecordTypeId = [select Id from RecordType where Name = 'Person Account' limit 1].Id;

        personAccounts.add(new Account(RecordTypeId = personAccountRecordTypeId, FirstName = 'Kenny', LastName = 'Chesney', PersonMailingCity = 'Nashville', PersonMailingState = 'TN', PersonMailingPostalCode = '11111', PersonMobilePhone = '111-111-1111'));
        personAccounts.add(new Account(RecordTypeId = personAccountRecordTypeId, FirstName = 'Tim', LastName = 'McGraw', PersonMailingCity = 'Nashville', PersonMailingState = 'TN', PersonMailingPostalCode = '22222', PersonMobilePhone = '222-222-2222'));

        insert personAccounts;

        // Create some intakes
        List<Intake__c> intakes = new List<Intake__c>();
        for(Account personAccout : personAccounts)
        {
            Intake__c intake = new Intake__c();
            intake.Client__c = personAccout.Id;
            intakes.add(intake);
        }
        insert intakes;

        // Create a Queue
        Dice_Queue__c queue = new Dice_Queue__c(Name = 'Unit Test Queue');
        insert queue;
    }

    static testMethod void mmdice_QueueRestApi_Ctor_Test()
    {
        // Arrange + Act
        Test.startTest();
        mmdice_QueueRestApi queueRestApi = new mmdice_QueueRestApi();
        Test.stopTest();

        // Assert
        System.assert(queueRestApi != null);
    }

    static testMethod void mmdice_QueueRestApi_Get_Test()
    {
        // Arrange
        String queueId = [SELECT Id FROM Dice_Queue__c LIMIT 1].Id;
        Integer expectedCount = 2;
        Integer expectedStatusCode = 200;

        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.requestURI = '/services/apexrest/dice/queues/' + queueId;
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = resp;

        // Act
        Test.startTest();
        mmdice_QueueRestApi.GetResponse getResponse = mmdice_QueueRestApi.get();
        Test.stopTest();

        // Assert
        //System.assertEquals(expectedStatusCode, resp.statusCode);
        //System.assert(getResponse != null);
        //System.assertEquals(expectedCount, getResponse.total_items);
    }

    static testMethod void mmdice_QueueRestApi_Get_InternalServerError_Test()
    {
        // Arrange
        String queueId = 'a003D000001dPZD'; // Valid format but invalid queue
        Integer expectedStatusCode = 500;

        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.requestURI = '/services/apexrest/dice/queues/' + queueId;
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = resp;

        // Act
        Test.startTest();
        mmdice_QueueRestApi.GetResponse getResponse = mmdice_QueueRestApi.get();
        Test.stopTest();

        // Assert
        System.assertEquals(expectedStatusCode, resp.statusCode);
        System.assert(getResponse == null);
    }

    static testMethod void mmdice_QueueRestApi_Get_BadRequest_Test()
    {
        // Arrange
        Integer expectedStatusCode = 400;

        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();
        req.requestURI = '/services/apexrest/dice/queues/NotASalesforceId';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = resp;

        // Act
        Test.startTest();
        mmdice_QueueRestApi.GetResponse getResponse = mmdice_QueueRestApi.get();
        Test.stopTest();

        // Assert
        System.assertEquals(expectedStatusCode, resp.statusCode);
        System.assert(getResponse == null);
    }
}