/**
 *  mmmarketing_CampaignFactory
 */
public class mmmarketing_CampaignFactory
{
    private fflib_ISObjectUnitOfWork uow = null;

    private Marketing_Campaign__c newRecord = null;

    public mmmarketing_CampaignFactory()
    {
    }

    public mmmarketing_CampaignFactory( fflib_ISObjectUnitOfWork uow )
    {
        this();

        this.uow = uow;
    }

    public Marketing_Campaign__c generateNewFromCampaignValues( String utmCampaignValue, String sourceCampaignIDValue, String domainValue )
    {
        // initialize the instance variable
        this.newRecord = new Marketing_Campaign__c();

        this.newRecord.Campaign_Name__c = utmCampaignValue;
        this.newRecord.utm_campaign__c = utmCampaignValue.toLowerCase();
        this.newRecord.Source_Campaign_ID__c = sourceCampaignIDValue;
        this.newRecord.Domain_Value__c = domainValue;

        if (this.uow != null
            && this.newRecord != null
            )
        {
            this.uow.registerNew( this.newRecord );
        }

        return this.newRecord;
    }
}