public with sharing class ScheduledEventMergeController {

	public Intake__c thisIntake {get;set;}
	public Event thisEvent {get;set;}
	public Account thisAccount {get;set;}
	public Id relatedAccountId {get;set;}
	public Id relatedIntakeId {get;set;}
	public Id relatedEventId {
		get {
			return relatedEventId;
		}
		set {
			relatedEventId = value;
			thisEvent = queryEvent('Investigation_Sched_Investigator_Email_N');

			relatedIntakeId = thisEvent.WhatId;
			thisIntake = queryIntake('Investigation_Sched_Investigator_Email_N');
			
			this.relatedAccountId = thisIntake.Client__c;
			thisAccount = queryPerson('Investigation_Sched_Investigator_Email_N');
		}
	}

	public ScheduledEventMergeController() {

	}

	public static List<FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
         Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
         Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
         Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
         Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
         return fieldSetObj.getFields();
    }

	public Event queryEvent(String eventFieldSetName) {
		List<FieldSetMember> fieldSetMemberList = readFieldSet(eventFieldSetName, 'Event');

        String query = 'SELECT ';
        for(Schema.FieldSetMember f : fieldSetMemberList) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, WhatId from Event WHERE Id = \''+relatedEventId+'\' LIMIT 1';
        return (Event) queryFirstResult(query);
	}

	public Intake__c queryIntake(String intakeFieldSetName) {
         List<FieldSetMember> fieldSetMemberList = readFieldSet(intakeFieldSetName, 'Intake__c');

         String query = 'SELECT ';
         for(Schema.FieldSetMember f : fieldSetMemberList) {
             query += f.getFieldPath() + ', ';
         }
         query += 'Id, client__c from Intake__c WHERE Id = \''+relatedIntakeId+'\' LIMIT 1';
        return (Intake__c) queryFirstResult(query);
    }

	public Account queryPerson(String accountFieldSetName) {
		List<FieldSetMember> fieldSetMemberList = readFieldSet(accountFieldSetName, 'Account');

		String query = 'SELECT ';
		for(Schema.FieldSetMember f : fieldSetMemberList) {
			query += f.getFieldPath() + ', ';
		}
		query += 'Id from Account WHERE Id = \''+relatedAccountId+'\'';
        return (Account) queryFirstResult(query);
	}

    private static SObject queryFirstResult(String queryString) {
        SObject queryResult = null;
        try {
            queryResult = Database.query(queryString).get(0);
        }
        catch (Exception e) {
            System.debug(e);
        }
        return queryResult;
    }
}