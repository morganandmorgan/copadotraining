/**
 * mmtwilio_AccountMappingSelector_Test
 * @description Test for Twilio Account Mapping Selector class.
 * @author Jeff Watson
 * @date 2/21/2019
 */
@IsTest
public with sharing class mmtwilio_AccountMappingSelector_Test {

    private static Twilio_Account_Mapping__c twilioAccountMapping;
    private static mmtwilio_AccountMappingSelector accountMappingSelector;

    static {
        twilioAccountMapping = new Twilio_Account_Mapping__c();
        twilioAccountMapping.Domain_Value__c = 'domain.com';
        twilioAccountMapping.Phone_Number__c = '8888888888';
        twilioAccountMapping.Source_Value__c = 'Call';
        twilioAccountMapping.Twilio_Account_Id__c = 'Twilio_Master';
        twilioAccountMapping.Twilio_Auth_Named_Credential__c = 'NamedCred';
        insert twilioAccountMapping;

        accountMappingSelector = new mmtwilio_AccountMappingSelector();
    }

    @IsTest
    private static void selectById() {
        List<Twilio_Account_Mapping__c> accountMappings = accountMappingSelector.selectById(new Set<Id> {twilioAccountMapping.Id});
        System.assert(accountMappings != null);
        System.assertEquals(1, accountMappings.size());
    }

    @IsTest
    private static void selectAll() {
        List<Twilio_Account_Mapping__c> accountMappings = accountMappingSelector.selectAll();
        System.assert(accountMappings != null);
        System.assertEquals(1, accountMappings.size());
    }
}