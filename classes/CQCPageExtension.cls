public class CQCPageExtension
{
    public static final String App = 'CQC';

    private final Case_Qualify_Criteria__c cqc;
    public List<SObjectQualifier.Query> criteriaList { get; set; }
    public List<SelectOption> intakeColumns { get; set; }
    public List<SelectOption> operatorOptions { get; set; }
    public List<SelectOption> formulaOptions { get; set; }

    public CQCPageExtension(ApexPages.StandardController stdController)
    {
        this.cqc = (Case_Qualify_Criteria__c)stdController.getRecord();

        this.cqc.criteria__c = String.isBlank(this.cqc.criteria__c) ? '' : this.cqc.criteria__c.toLowerCase();

        populateIntakeColumns();

        operatorOptions = SObjectQualifierUIHelper.getOperatorOptions();

        formulaOptions = SObjectQualifierUIHelper.getFormulaOptions();

        criteriaList = SObjectQualifier.ParseQuery(cqc.Criteria__c);

        while (criteriaList.size() < 10)
        {
            criteriaList.add(new SObjectQualifier.Query());
        }
    }

    private List<String> getExclusionsForObject(String targetSObj)
    {
        List<String> exclusionFields = new List<String>();
        List<Exclusions_List__mdt> exclusionsLists = mmcommon_ExclusionsListsSelector.newInstance().selectByAppAndTargetObject(App, targetSObj);

        for(Exclusions_List__mdt exclusionsList : exclusionsLists)
        {
            if(exclusionsList.Target_Object_Fields__c != null)
            {
                for (String field : exclusionsList.Target_Object_Fields__c.split(','))
                {
                    exclusionFields.add(field.trim());
                }
            }
        }

        return exclusionFields;
    }

    private void populateIntakeColumns()
    {
        intakeColumns = new List<SelectOption>();

        Schema.DescribeSObjectResult description = Intake__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fields = description.fields.getMap();
        List<String> fieldExclusions = this.getExclusionsForObject(description.name);
        for (String fieldName : fields.keySet())
        {
            if(!fieldExclusions.contains(fieldName))
            {
                intakeColumns.add(new SelectOption(fieldName, '[Intake] ' + fields.get(fieldName).getDescribe().getLabel()));
            }
        }

        description = Account.sObjectType.getDescribe();
        fields = description.fields.getMap();
        fieldExclusions = this.getExclusionsForObject(description.name);
        for (String fieldName : fields.keySet())
        {
            if ( ! fieldName.startsWithIgnoreCase('ffr__')
                && ! fieldName.startsWithIgnoreCase('qbdialer__')
                && ! fieldName.startsWithIgnoreCase('ffaci__')
                && ! fieldName.startsWithIgnoreCase('ffbf__')
                && ! fieldName.startsWithIgnoreCase('fferpcore__')
                && ! fieldName.startsWithIgnoreCase('c2g__')
                && ! fieldName.startsWithIgnoreCase('ffbext__')
                && ! fieldName.startsWithIgnoreCase('ffxp__')
                && ! fieldName.startsWithIgnoreCase('ffc__')
                && ! fieldName.startsWithIgnoreCase('fferpcore__')
                && ! fieldName.startsWithIgnoreCase('ffcash__')
                && ! fieldName.startsWithIgnoreCase('ffqs__')
                && ! fieldName.startsWithIgnoreCase('ffirule__')
                && ! fieldName.startsWithIgnoreCase('ffct__')
                && ! fieldExclusions.contains(fieldName)
                )
            {
                System.debug(fieldName);
                intakeColumns.add(new SelectOption('Client__r.' + fieldName, '[Account] ' + fields.get(fieldName).getDescribe().getLabel()));
            }
        }

        intakeColumns.sort();

        intakeColumns.add(0, new SelectOption('', ''));
    }

//    private void populateOperatorOptions()
//    {
//        operatorOptions = new List<SelectOption>();
//
//        operatorOptions.add(new SelectOption('', ''));
//        operatorOptions.add(new SelectOption('<', 'Less/Earlier Than'));
//        operatorOptions.add(new SelectOption('<=', 'Less/Earlier Than or Equal'));
//        operatorOptions.add(new SelectOption('=', 'Equals'));
//        operatorOptions.add(new SelectOption('<>', 'Does Not Equal'));
//        operatorOptions.add(new SelectOption('>=', 'Greater/Later Than or Equal'));
//        operatorOptions.add(new SelectOption('>', 'Greater/Later Than'));
//        operatorOptions.add(new SelectOption('contains', 'Contains'));
//        operatorOptions.add(new SelectOption('does_not_contain', 'Does Not Contain'));
//        operatorOptions.add(new SelectOption('contains_only', 'Contains Only'));
//    }
//
//    private void populateFormulaOptions()
//    {
//        formulaOptions = new List<SelectOption>();
//
//        formulaOptions.add(new SelectOption('', ''));
//        formulaOptions.add(new SelectOption('days_ago', 'Days Ago'));
//        formulaOptions.add(new SelectOption('months_ago', 'Months Ago'));
//        formulaOptions.add(new SelectOption('years_ago', 'Years Ago'));
//    }
}