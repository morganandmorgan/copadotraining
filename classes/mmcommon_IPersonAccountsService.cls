/**
 *  mmcommon_IPersonAccountsService
 */
public interface mmcommon_IPersonAccountsService
{
    List<Account> searchAccounts( String firstName, String lastName, String email, String phoneNum, Date birthDate );
    List<Account> searchAccountsUsingHighPrecision(String firstName, String lastName, String email, String phoneNum, Date birthDate);
    List<Account> searchAccountsUsingFuzzyMatching(Map<String, String> data);
    void saveRecords( list<Account> records );
    void geocodeAddresses(Set<Id> idSet);
}