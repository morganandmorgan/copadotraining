public with sharing class mmlib_TinyUrl {

    @InvocableMethod(Label='URL Shortener' Description='Returns a shortened version of a long url string.')
    public static List<String> getTinyUrl(List<String> urls) {
        List<String> shortUrls = new List<String>();
        for (String url : urls) {
            shortUrls.add(getTinyUrl(url));
        }
        return shortUrls;
    }

    public static String getTinyUrl(String longUrl) {
        String result = '';

        if (String.isBlank(longUrl)) {
            return result;
        }

        // http://tinyurl.com/api-create.php?url=http://scripting.com/

        Map<String, String> paramMap = new Map<String, String>();
        paramMap.put('url', EncodingUtil.urlEncode(longUrl, 'UTF-8'));

        HttpRequest req =
                mmlib_HttpRequestFactory.buildHttpRequest(
                        'GET',
                        'http',
                        'tinyurl.com',
                        '/api-create.php',
                        paramMap,
                        null,
                        null,
                        null
                );

        HttpResponse resp = new Http().send(req);

        if (resp.getStatusCode() == 200) {
            result = resp.getBody();
        }

        return result;
    }
}