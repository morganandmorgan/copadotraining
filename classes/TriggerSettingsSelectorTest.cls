/**
 * TriggerSettingsSelectorTest
 * @description Tests for Trigger Settings Selector implementation.
 * @author Jeff Watson
 * @date 10/26/2018
 */
@isTest
public with sharing class TriggerSettingsSelectorTest {

    public static testMethod void trac_TriggerSettingsSelector_selectDefaultSettings() {

        // Arrange + Act
        Test.startTest();
        Trigger_Settings__mdt triggerSettings = TriggerSettingsSelector.selectDefaultSettings();
        Test.stopTest();

        // Assert
        System.assert(triggerSettings != null);
        System.debug('Done');
    }
}