/**
 *  mmintake_InvestigationEventsSelector
 */
public with sharing class mmintake_InvestigationEventsSelector
    extends mmlib_SObjectSelector
    implements mmintake_IInvestigationEventsSelector
{
    public static mmintake_IInvestigationEventsSelector newInstance()
    {
        return (mmintake_IInvestigationEventsSelector) mm_Application.Selector.newInstance( IntakeInvestigationEvent__c.SObjectType );
    }

    private Schema.sObjectType getSObjectType()
    {
        return IntakeInvestigationEvent__c.SObjectType;
    }

    private list<Schema.SObjectField> getAdditionalSObjectFieldList()
    {
        return new list<Schema.SObjectField> {
            IntakeInvestigationEvent__c.Approving_Attorney_1__c,
            IntakeInvestigationEvent__c.Approving_Attorney_2__c,
            IntakeInvestigationEvent__c.Approving_Attorney_3__c,
            IntakeInvestigationEvent__c.Approving_Attorney_4__c,
            IntakeInvestigationEvent__c.Approving_Attorney_5__c,
            IntakeInvestigationEvent__c.Approving_Attorney_6__c,
            IntakeInvestigationEvent__c.Assigned_Attorney__c,
            IntakeInvestigationEvent__c.Catastrophic__c,
            IntakeInvestigationEvent__c.Client__c,
            IntakeInvestigationEvent__c.Contact__c,
            IntakeInvestigationEvent__c.Contracts_to_be_Signed__c,
            IntakeInvestigationEvent__c.EndDateTime__c,
            IntakeInvestigationEvent__c.IncidentInvestigation__c,
            IntakeInvestigationEvent__c.Intake__c,
            IntakeInvestigationEvent__c.Investigation_Status__c,
            IntakeInvestigationEvent__c.Investigator_Territory__c,
            IntakeInvestigationEvent__c.Location__c,
            IntakeInvestigationEvent__c.New_Case_Email__c,
            IntakeInvestigationEvent__c.Reason_for_Delayed_Signup__c,
            IntakeInvestigationEvent__c.StartDateTime__c,
            IntakeInvestigationEvent__c.Handling_Firm__c,
            IntakeInvestigationEvent__c.Case_Manager_Email__c,
            IntakeInvestigationEvent__c.Client_Name_Formula__c,
            IntakeInvestigationEvent__c.Intake_Number_Formula__c,
            IntakeInvestigationEvent__c.Investigation_Contact_Mobile__c
        };
    }

    public list<IntakeInvestigationEvent__c> selectById(Set<Id> idSet)
    {
        return (list<IntakeInvestigationEvent__c>) selectSObjectsById(idSet);
    }

    public list<IntakeInvestigationEvent__c> selectScheduledByIntakesWithIntakeAndNames(Set<Id> intakeIdSet)
    {
        fflib_QueryFactory qf = newQueryFactory().setCondition( IntakeInvestigationEvent__c.Intake__c + ' in :intakeIdSet and '
                                                              + IntakeInvestigationEvent__c.Investigation_Status__c + ' = \'Scheduled\'');

        qf.getOrderings().clear();
        qf.addOrdering(Intake__c.LastModifiedDate, fflib_QueryFactory.SortOrder.DESCENDING, false);

        set<string> ADDITIONAL_FIELDS_SET = new set<String>{ IntakeInvestigationEvent__c.Client__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Contact__c.getDescribe().getRelationshipName() + '.Name'

                                                           // intake's client account info
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Client__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Client__c.getDescribe().getRelationshipName() + '.' + Account.Date_of_Birth_mm__c
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Client__c.getDescribe().getRelationshipName() + '.' + Account.Marital_Status__c

                                                           // intake's injured party account info
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Injured_Party__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Injured_Party__c.getDescribe().getRelationshipName() + '.' + Account.Date_of_Birth_mm__c
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Injured_Party__c.getDescribe().getRelationshipName() + '.' + Account.Marital_Status__c

                                                           , IntakeInvestigationEvent__c.OwnerId.getDescribe().getRelationshipName() + '.Name' };

        qf.selectFields( ADDITIONAL_FIELDS_SET );

        new mmintake_IntakesSelector().addQueryFactoryParentSelect( qf, IntakeInvestigationEvent__c.Intake__c );

        return Database.query( qf.toSOQL() );
    }

    public list<IntakeInvestigationEvent__c> selectByIncidentInvestigationsNotRelatedToIntakesWithIntakeAndNames(Set<Id> incidentInvestigationIdSet, Set<Id> intakeIdSet)
    {
        fflib_QueryFactory qf = newQueryFactory().setCondition( IntakeInvestigationEvent__c.IncidentInvestigation__c + ' in :incidentInvestigationIdSet and '
                                                              + IntakeInvestigationEvent__c.Intake__c + ' not in :intakeIdSet');

        qf.getOrderings().clear();
        qf.addOrdering(Intake__c.LastModifiedDate, fflib_QueryFactory.SortOrder.DESCENDING, false);

        set<string> ADDITIONAL_FIELDS_SET = new set<String>{ IntakeInvestigationEvent__c.Client__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Contact__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Client__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.OwnerId.getDescribe().getRelationshipName() + '.Name' };

        qf.selectFields( ADDITIONAL_FIELDS_SET );

        new mmintake_IntakesSelector().addQueryFactoryParentSelect( qf, IntakeInvestigationEvent__c.Intake__c );

        return Database.query( qf.toSOQL() );
    }

    public list<IntakeInvestigationEvent__c> selectCanceledRescheduledByIntakesWithIntakeAndNames( Set<Id> intakeIdSet )
    {
        set<String> statusSet = new set<String>();

        // TODO: Need to eventually centralize these picklist value references to a "intake specific constants class" for all to use.
        //          @see InvestigationEventTriggerHandler.CANCEL_STATUS and InvestigationEventTriggerHandler.RESCHEDULE_STATUS
        statusSet.add( 'Canceled' );
        statusSet.add( 'Rescheduled' );

        fflib_QueryFactory qf = newQueryFactory().setCondition( IntakeInvestigationEvent__c.Investigation_Status__c + ' in :statusSet and '
                                                              + IntakeInvestigationEvent__c.Intake__c + ' in :intakeIdSet');

        qf.getOrderings().clear();
        qf.addOrdering(Intake__c.LastModifiedDate, fflib_QueryFactory.SortOrder.DESCENDING, false);

        set<string> ADDITIONAL_FIELDS_SET = new set<String>{ IntakeInvestigationEvent__c.Client__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Contact__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.Intake__c.getDescribe().getRelationshipName() + '.'
                                                                    + Intake__c.Client__c.getDescribe().getRelationshipName() + '.Name'
                                                           , IntakeInvestigationEvent__c.OwnerId.getDescribe().getRelationshipName() + '.Name' };

        qf.selectFields( ADDITIONAL_FIELDS_SET );

        new mmintake_IntakesSelector().addQueryFactoryParentSelect( qf, IntakeInvestigationEvent__c.Intake__c );

        return Database.query( qf.toSOQL() );
    }

    // Todo: Remove along with DateOfBirthCoverageTest.cls once real tests are created.
    @TestVisible
    private static void coverage() {
        Integer i = 0;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
        i = i + 1;
    }
}