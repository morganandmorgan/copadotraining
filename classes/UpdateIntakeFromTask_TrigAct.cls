public class UpdateIntakeFromTask_TrigAct extends TriggerAction {

    private static final String TASK_STATUS_COMPLETED = 'Completed';
    private static final String TASK_CALLTYPE_OUTBOUND = 'Outbound';

    private final List<Task> completedCalls = new List<Task>();

    public UpdateIntakeFromTask_TrigAct() {
		super();
	}

    public override Boolean shouldRun() {
        return shouldRunForCompletedCalls();
    }

    private Boolean shouldRunForCompletedCalls() {
        completedCalls.clear();

        List<Task> newList = getNewListForContext();
        Map<Id, Task> oldMap = (Map<Id, Task>) triggerOldMap;

        for (Task newTask : newList) {
            if (    ((newTask.WhatId != null && (newTask.WhatId.getSObjectType() == Account.SObjectType || newTask.WhatId.getSObjectType() == Intake__c.SObjectType)) || newTask.AccountId != null)
                    && String.isNotBlank(newTask.CallType)
                    && newTask.Status == TASK_STATUS_COMPLETED
                    && (oldMap == null || newTask.Status != oldMap.get(newTask.Id).Status)) {

                completedCalls.add(newTask);
            }
        }

        return !completedCalls.isEmpty();
    }

    public override void doAction() {
        doActionForCompletedCalls();
    }

    private void doActionForCompletedCalls() {
        Map<Id, List<Task>> accountIdToTasks = new Map<Id, List<Task>>();
        Map<Id, List<Task>> accountIdToOutboundTasks = new Map<Id, List<Task>>();
        Map<Id, List<Task>> intakeIdToTasks = new Map<Id, List<Task>>();
        Map<Id, List<Task>> intakeIdToOutboundTasks = new Map<Id, List<Task>>();
        for (Task completedCall : completedCalls) {
            Boolean isOutboundCall = completedCall.CallType == TASK_CALLTYPE_OUTBOUND;
            if (completedCall.WhatId != null && completedCall.WhatId.getSObjectType() == Account.SObjectType) {
                addTaskToMap(completedCall, completedCall.WhatId, accountIdToTasks);
                if (isOutboundCall) {
                    addTaskToMap(completedCall, completedCall.WhatId, accountIdToOutboundTasks);
                }
            }
            else if (completedCall.WhatId != null && completedCall.WhatId.getSObjectType() == Intake__c.SObjectType) {
                addTaskToMap(completedCall, completedCall.WhatId, intakeIdToTasks);
                if (isOutboundCall) {
                    addTaskToMap(completedCall, completedCall.WhatId, intakeIdToOutboundTasks);
                }
            }
            else if (completedCall.AccountId != null) {
                addTaskToMap(completedCall, completedCall.AccountId, accountIdToTasks);
                if (isOutboundCall) {
                    addTaskToMap(completedCall, completedCall.AccountId, accountIdToOutboundTasks);
                }
            }
        }

        fflib_ISObjectUnitOfWork uow = mm_Application.UnitOfWork.newInstance();

        if (!intakeIdToTasks.isEmpty()) {
            List<Intake__c> intakesToUpdate =[
                SELECT
                    Id,
                    Client__c,
                    Dials__c,
                    Last_Call_Time__c
                FROM
                    Intake__c
                WHERE
                    Id IN :intakeIdToTasks.keySet()
                FOR UPDATE
            ];

            Datetime now = Datetime.now();
            for (Intake__c intakeToUpdate : intakesToUpdate) {
                List<Task> intakeTasks = intakeIdToTasks.get(intakeToUpdate.Id);

                intakeToUpdate.Last_Call_Time__c = now;

                List<Task> outboundTasks = intakeIdToOutboundTasks.get(intakeToUpdate.Id);
                Integer totalOutboundTasks = outboundTasks == null ? 0 : outboundTasks.size();
                intakeToUpdate.Dials__c = (intakeToUpdate.Dials__c == null ? totalOutboundTasks : intakeToUpdate.Dials__c + totalOutboundTasks);

                if (intakeToUpdate.Client__c != null) {
                    addTasksToMap(intakeTasks, intakeToUpdate.Client__c, accountIdToTasks);
                    addTasksToMap(outboundTasks, intakeToUpdate.Client__c, accountIdToOutboundTasks);
                }

                uow.registerDirty(intakeToUpdate);
            }
        }

        if (!accountIdToTasks.isEmpty()) {
            List<Account> accountsToUpdate = [
                SELECT
                    Id,
                    OB_Dials__c,
                    Last_Call_Date_Time__c
                FROM
                    Account
                WHERE
                    Id IN :accountIdToTasks.keySet()
                FOR UPDATE
            ];

            for (Account a : accountsToUpdate) {
                a.Last_Call_Date_Time__c = Datetime.now();

                List<Task> outboundTasks = accountIdToOutboundTasks.get(a.Id);
                Integer totalOutboundTasks = outboundTasks == null ? 0 : outboundTasks.size();
                a.OB_Dials__c = (a.OB_Dials__c == null ? totalOutboundTasks : a.OB_Dials__c + totalOutboundTasks);

                uow.registerDirty(a);
            }
        }

        // Todo: Refactor with fflib factory framework.
        // mmintake_IIntakesSelector selector =  mmintake_IntakesSelector.newInstance();
        // 
        // new mmintake_Intakes(selector.selectById(intakeIdToOutboundTasks.keyset()))
        //     .updateOutboundDialCounts(intakeIdToOutboundTasks, uow);

        uow.commitWork();
    }

    private List<Task> getNewListForContext() {
        if (isAfter() && isInsert()) {
            return (List<Task>) triggerMap.values();
        }
        else if (isAfter() && isUpdate()) {
            return (List<Task>) triggerMap.values();
        }
        return new List<Task>();
    }

    private static void addTaskToMap(Task t, Id key, Map<Id, List<Task>> taskMap) {
        List<Task> tasks = taskMap.get(key);
        if (tasks == null) {
            tasks = new List<Task>();
            taskMap.put(key, tasks);
        }
        if (t != null) {
            tasks.add(t);
        }
    }

    private static void addTasksToMap(List<Task> t, Id key, Map<Id, List<Task>> taskMap) {
        List<Task> tasks = taskMap.get(key);
        if (tasks == null) {
            tasks = new List<Task>();
            taskMap.put(key, tasks);
        }
        if (t != null) {
            tasks.addAll(t);
        }
    }
}