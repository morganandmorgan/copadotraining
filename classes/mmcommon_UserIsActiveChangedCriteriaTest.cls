@isTest
private class mmcommon_UserIsActiveChangedCriteriaTest
{
    @isTest
    private static void newUserMeetsCriteria()
    {
        User newRecord = new User(IsActive = true, id = fflib_IDGenerator.generate(User.SObjectType));
        //User oldRecord = null;

        Test.startTest();

        mmcommon_UserIsActiveChangedCriteria criteria = new mmcommon_UserIsActiveChangedCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.size() == 1);
        System.assertEquals( resultRecords[0].id, newRecord.id );
    }

    @isTest
    private static void newUserFailsCriteria()
    {
        User newRecord = new User(IsActive = false, id = fflib_IDGenerator.generate(User.SObjectType));
        //User oldRecord = null;

        Test.startTest();

        mmcommon_UserIsActiveChangedCriteria criteria = new mmcommon_UserIsActiveChangedCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.isEmpty() );
    }

    @isTest
    private static void changedUsersMeetsCriteria()
    {
        Id userId = fflib_IDGenerator.generate(User.SObjectType);

        User newRecord = new User(IsActive = true, id = userId);
        User oldRecord = new User(IsActive = false, id = userId);

        Test.startTest();

        mmcommon_UserIsActiveChangedCriteria criteria = new mmcommon_UserIsActiveChangedCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        Map<id, User> existingRecordMap = new Map<id, User>();
        existingRecordMap.put( userId, oldRecord );

        criteria.setExistingRecords( existingRecordMap );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.size() == 1);
        System.assertEquals( resultRecords[0].id, newRecord.id );
    }

    @isTest
    private static void changedUsersFailsCriteria()
    {
        Id userId = fflib_IDGenerator.generate(User.SObjectType);

        User newRecord = new User(IsActive = false, id = userId);
        User oldRecord = new User(IsActive = true, id = userId);

        Test.startTest();

        mmcommon_UserIsActiveChangedCriteria criteria = new mmcommon_UserIsActiveChangedCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        Map<id, User> existingRecordMap = new Map<id, User>();
        existingRecordMap.put( userId, oldRecord );

        criteria.setExistingRecords( existingRecordMap );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.isEmpty() );
    }

    @isTest
    private static void unchangedUserFailsCriteria()
    {
        Id userId = fflib_IDGenerator.generate(User.SObjectType);

        User newRecord = new User(IsActive = true, id = userId);
        User oldRecord = new User(IsActive = true, id = userId);

        Test.startTest();

        mmcommon_UserIsActiveChangedCriteria criteria = new mmcommon_UserIsActiveChangedCriteria();

        criteria.setRecordsToEvaluate( new List<User>{ newRecord } );

        Map<id, User> existingRecordMap = new Map<id, User>();
        existingRecordMap.put( userId, oldRecord );

        criteria.setExistingRecords( existingRecordMap );

        list<Sobject> resultRecords = criteria.run();

        Test.stopTest();

        System.assert( resultRecords.isEmpty() );
    }
}