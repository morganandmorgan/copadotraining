public class mmmarketing_ShallowReconcileExecutor
    extends mmmarketing_UpdateCallInfoAbstract
{
    public override void performUpdateIfMatch( mmmarketing_IUpdateCallInfoLogic updateLogic, Marketing_Tracking_Info__c record, mmlib_BaseCallout.CalloutResponse listCallsCalloutResponse, String identifierThatFoundMatch ) {
        updateLogic.updateMTICallRecordShallowReconciliationIfMatch( record, listCallsCalloutResponse, identifierThatFoundMatch, this.uow);
    }
}