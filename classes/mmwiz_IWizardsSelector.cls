public interface mmwiz_IWizardsSelector
{
    List<Wizard__mdt> selectByMasterLabel(set<String> labelSet);
    List<Wizard__mdt> selectByDeveloperName(set<String> developerNamesSet);
    List<Wizard__mdt> selectAll();
    List<Wizard__mdt> selectAllBeingActive();
}