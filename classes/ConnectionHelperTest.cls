@isTest
private class ConnectionHelperTest {

    private static PartnerNetworkConnection buildConnection() {
        Id mockId = SObjectType.PartnerNetworkConnection.getKeyPrefix() + '0'.repeat(15);
        Id createdById = UserInfo.getUserId();
        String connectionAsJson = '{ "Id": "' + mockId + '", "CreatedById": "' + createdById + '", "ConnectionName": "Test Name" }';
        return (PartnerNetworkConnection) JSON.deserialize(connectionAsJson, PartnerNetworkConnection.class);
    }
	
	@isTest
    private static void getConnectionId_Success() {
        PartnerNetworkConnection connection = buildConnection();

        ConnectionHelper.cachedConnections.put(new ConnectionHelper.ConnectionKey(connection.ConnectionName), connection);

        Test.startTest();
        Id returnValue = ConnectionHelper.getConnectionId(connection.ConnectionName);
        Test.stopTest();

        System.assertEquals(connection.Id, returnValue);
    }

    @isTest
    private static void getConnectionId_Failure() {
        PartnerNetworkConnection connection = buildConnection();

        ConnectionHelper.cachedConnections.put(new ConnectionHelper.ConnectionKey(connection.ConnectionName), connection);

        Test.startTest();
        Id returnValue = ConnectionHelper.getConnectionId('Invalid Connection Name');
        Test.stopTest();

        System.assertEquals(null, returnValue);
    }

    @isTest
    private static void getConnectionOwnerId_Success() {
        PartnerNetworkConnection connection = buildConnection();

        ConnectionHelper.cachedConnections.put(new ConnectionHelper.ConnectionKey(connection.ConnectionName), connection);

        Test.startTest();
        Id returnValue = ConnectionHelper.getConnectionOwnerId(connection.ConnectionName);
        Test.stopTest();

        System.assertEquals(connection.CreatedById, returnValue);
    }

    @isTest
    private static void getConnectionOwnerId_Failure() {
        PartnerNetworkConnection connection = buildConnection();

        ConnectionHelper.cachedConnections.put(new ConnectionHelper.ConnectionKey(connection.ConnectionName), connection);

        Test.startTest();
        Id returnValue = ConnectionHelper.getConnectionOwnerId('Invalid Connection Name');
        Test.stopTest();

        System.assertEquals(null, returnValue);
    }
}