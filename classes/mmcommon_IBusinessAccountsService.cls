/**
 *  mmcommon_IBusinessAccountsService
 */
public interface mmcommon_IBusinessAccountsService
{
    List<Account> findTreatmentFacilitiesCloseToLocation( final Location originatingLocation, final String treatmentCenterType );
}