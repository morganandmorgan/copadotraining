/**
 *  mmmarketing_ISourcesSelector
 */
public interface mmmarketing_ISourcesSelector extends mmlib_ISObjectSelector
{
    list<Marketing_Source__c> selectById( Set<Id> idSet );
    list<Marketing_Source__c> selectByName( Set<String> names );
    List<Marketing_Source__c> selectByUtm( Set<String> utmSet );
}