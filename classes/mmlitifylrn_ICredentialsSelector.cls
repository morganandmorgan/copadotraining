public interface mmlitifylrn_ICredentialsSelector extends mmlib_ISObjectSelector
{
    List<mmlitifylrn_ThirdPartyCredential__c> selectById( Set<id> idSet );
    List<mmlitifylrn_ThirdPartyCredential__c> selectByName( set<string> nameSet );
    List<mmlitifylrn_ThirdPartyCredential__c> selectAll();
}