@isTest
public Class TestQuestionnaireCreationClass{
    static testmethod void testQuestionnaireCreatMethod(){

    RecordType acctRcdType = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Person_Account'];

        List<Account> acc = new List<account>{new Account(LastName = 'TestAcc 1', RecordTypeId = acctRcdType.Id),
                new Account(LastName = 'TestAcc 1',Gender__c = 'Female', RecordTypeId = acctRcdType.Id)
        };
        Insert acc;

        List<Intake__c> intk = new List<Intake__c>{ new Intake__c(Client__c = acc[0].Id,Have_you_had_surgery_to_remove_it__c = 'Yes',Litigation__c = 'Mass Tort',Case_Type__c = 'Transvaginal Mesh',Status__c = 'Retainer Received'),
                                                    new Intake__c(Client__c = acc[0].Id,Surgery_to_remove_breast_tissue__c = 'No',Do_you_suffer_from_one_of_the_injuries__c = 'Other',Litigation__c = 'Mass Tort',Case_Type__c = 'Risperdal',Status__c = 'Retainer Received'),
                                                    new Intake__c(Client__c = acc[0].Id,How_old_were_you_at_the_time_of_injury__c = 90,When_did_you_start_using_the_drug__c = date.newinstance(2013, 1, 1),Litigation__c = 'Mass Tort',Case_Type__c = 'Fosamax',Status__c = 'Retainer Received')
        };

        Insert intk;

        List<Questionnaire__c> quesList = new List<Questionnaire__c>{new Questionnaire__c(Intake__c = intk[1].Id )};
        Insert quesList;

        QuestionnaireCreationClass.questionnaireCreation(intk[0].Id);
        QuestionnaireCreationClass.questionnaireCreation(intk[1].Id);
    }
}