public class ConnectionHelper {

    @TestVisible
    private static final Map<ConnectionKey, PartnerNetworkConnection> cachedConnections = new Map<ConnectionKey, PartnerNetworkConnection>();

    public static Id getConnectionId(String connectionName) {

        PartnerNetworkConnection partnerNetCon = getConnection(connectionName);

        if ( partnerNetCon != null ) {
            return partnerNetCon.Id;
        }
        return null;
    }

    public static Id getConnectionOwnerId(String connectionName) {

        PartnerNetworkConnection partnerNetCon = getConnection(connectionName);

        if ( partnerNetCon != null ) {
            return partnerNetCon.CreatedById;
        }
        return null;
    }

    private static PartnerNetworkConnection getConnection(String connectionName) {
        ConnectionKey key = new ConnectionKey(connectionName);
        if (!cachedConnections.containsKey(key)) {
            List<PartnerNetworkConnection> partnerNetConList = [Select Id, CreatedById from PartnerNetworkConnection where ConnectionStatus = 'Accepted' and ConnectionName = :connectionName];
            cachedConnections.put(key, (partnerNetConList.isEmpty() ? null : partnerNetConList.get(0)));
        }
        return cachedConnections.get(key);
    }

    @TestVisible
    private class ConnectionKey {

        private final String key;

        @TestVisible
        private ConnectionKey(String connectionName) {
            key = connectionName == null ? null : connectionName.toLowerCase();
        }

        public Boolean equals(Object o) {
            if (o instanceof ConnectionKey) {
                ConnectionKey that = (ConnectionKey) o;
                return this.key == that.key;
            }
            return false;
        }

        public Integer hashCode() {
            return key == null ? 0 : key.hashCode();
        }
    }
}