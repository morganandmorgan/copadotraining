/*
        Standard data template class for common objects.
        Method calls return objects with a minium amount of data for insert in test classes.
        These object should then be modified for and inserted
*/
@isTest
public class TestUtil {
    private static String PersonAccountRecordType = 'Person_Account';
    public static String DefaultPersonAccountBillingCity = 'Orlando';
    public static String DefaultPersonAccountBillingState = 'FL';
    public static String DefaultPersonAccountBillingZipCode = '33333';

    private static final Date EVENT_DATE = Date.newInstance(2010, 5, 25);
    private static final String SUBJECT_HOME = 'Home';
    private static Integer SOBJECT_ROTATIONAL = 1;
    private static Integer userIncrement = 0;

    private static Id assignableAttorneyAccount {
        get {
            if (assignableAttorneyAccount == null) {
                String attorneyAccountName = 'Morgan & Morgan P.A.';
                List<Account> accts = [SELECT Id FROM Account WHERE Name = :attorneyAccountName ORDER BY CreatedDate ASC LIMIT 1];
                if (accts.isEmpty()) {
                    Account acct = createAccount(attorneyAccountName);
                    Database.insert(acct);
                    assignableAttorneyAccount = acct.Id;
                } else {
                    assignableAttorneyAccount = accts.get(0).Id;
                }
            }
            return assignableAttorneyAccount;
        }
        private set;
    }

    private static Profile investigatorProfile {
        get {
            if (investigatorProfile == null) {
                investigatorProfile = [SELECT Id FROM Profile WHERE Name = 'M&M CCC Investigator'];
            }
            return investigatorProfile;
        }
        set;
    }

    public static Account createAccount(String name, String RTName) {
        //We dont' use a lot of RT for accounts
        Id rtId = RecordTypeUtil.getRecordTypeIDByDevName('Account', RTName);
        return new Account (name = name, recordTypeId = rtId, shippingCountry = 'United States',
                BillingCountry = 'United States');
    }
    public static Account createAccount(String name) {
        return TestUtil.createAccount(name, 'Business_Account');
    }

    public static Account createAccountBusinessMorganAndMorgan(String name) {
        return TestUtil.createAccount(name, 'Morgan_Morgan_Businesses');
    }

    public static Account createNewAccount(String name, String recordTypeName) {
        Id rtId = RecordTypeUtil.getRecordTypeIDByDevName('Account', recordTypeName);
        return new Account (name = name, recordTypeId = rtId, shippingCountry = 'United States',
                BillingStreet = 'test street', BillingCity = 'test city',
                BillingState = 'test state', BillingCountry = 'test country', BillingPostalCode = '55555',
                Language__c = 'English');
    }

    public static Account createPersonAccount(String firstName, String lastName) {
        Id rtId = RecordTypeUtil.getRecordTypeIDByDevName('Account', TestUtil.PersonAccountRecordType);
        return new Account (FirstName = firstName,
                LastName = lastname,
                RecordTypeId = rtId,
                BillingStreet = '123 Main St.',
                BillingCity = TestUtil.DefaultPersonAccountBillingCity,
                BillingState = TestUtil.DefaultPersonAccountBillingState,
                BillingPostalCode = TestUtil.DefaultPersonAccountBillingZipCode,
                BillingCountry = 'United States');
    }

    /* Create Contact Methods */
    public static Contact createContact(String firstName, String lastName, String email, String accountId, String phone, String description) {
        return new Contact(AccountId = accountId, FirstName = firstName, LastName = lastName,
                Title = 'Dude', Department = 'Things', Email = email, Phone = phone,
                Description = description);
    }
    public static Contact createContact(String accountId) {
        // Generate a pseudo random contact info
        String rnd = System.now().getTime() + '-' +
                String.valueOf(Math.round(Math.random() * 100));
        String email = rnd + '_newuser@testorg.com';
        String firstName = 'John' + rnd;
        String lastName = 'Doe';
        String phone = '(555)123-5555';
        String description = '';
        return createContact(firstName, lastName, email, accountId, phone, description);
    }

    public static Contact createAssignableAttorney() {
        Contact c = createContact(assignableAttorneyAccount);
        c.Title = 'Attorney';
        return c;
    }

    /* Create User Methods */
    public static User createUser(String profileID) {
        // Generate a pseudo random user email
        String uniqueKey = System.now().getTime() + '-' +
                String.valueOf(Math.round(Math.random() * 100));
        // Get a new user object from createUser(String, String)
        return TestUtil.createUser(profileID, uniqueKey + '_newuser@testorg.com');
    }
    public static User createUser(String profileID, String Email) {
        // Check if the profile Id is valid
        if (!UserProfileUtil.validProfile(profileID)) {
            throw new InvalidIdException('The ProfileID provided is not a valid Profile Id.');
        }
        // Initialize a new user
        User testUser = new User(
                Alias = 'newUser',
                Email = Email,
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = (Id) profileID,
                FederationIdentifier = Email,
                TimeZoneSidKey = 'America/Los_Angeles',
                //UserName=Email
                Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@acme.com'
        );
        // Return new user object
        return testUser;
    }

    public static User createUser() {
        Integer userNum = userIncrement++;
        return new User(
                IsActive = true,
                FirstName = 'FirstName ' + userNum,
                LastName = 'LastName ' + userNum,
                //Username = 'apex.temp.test.user@sample.com.' + userNum,
                Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@acme.com',
                Email = 'apex.temp.test.user@sample.com.' + userNum,
                Alias = 'alib' + userNum,
                TimeZoneSidKey = 'America/New_York',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                ProfileId = UserInfo.getProfileId(),
                Territory__c = null
        );
    }

    public static User createUser(String territory, String language, String skill, Boolean active) {
        Integer userNum = userIncrement++;
        String email = 'developers' + userNum + '@forthepeople.com';
        User u = new User(IsActive = active,
                FirstName = 'FirstName ' + userNum,
                LastName = 'LastName ' + userNum,
                //Username = email,
                Username = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(256)) + '@acme.com',
                Email = email,
                Alias = 'test_' + userNum,
                TimeZoneSidKey = 'America/New_York',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                ProfileId = UserInfo.getProfileId(),
                Territory__c = territory);

        if (String.isNotEmpty(language)) {
            u.Language__c = language;
        }

        if (String.isNotEmpty(skill)) {
            u.Skill__c = skill;
        }

        return u;
    }

    public static User createActiveInvestigator() {
        User u = createUser();
        u.IsActive = true;
        u.ProfileId = investigatorProfile.Id;
        u.Territory__c = 'Test Territory';
        return u;
    }

    public static User createInactiveInvestigator() {
        User u = createUser();
        u.IsActive = false;
        u.ProfileId = investigatorProfile.Id;
        u.Territory__c = 'Test Territory';
        return u;
    }

    /* Create Lead Methods */
    public static Lead createLead(String firstName, String lastName, String email, String leadstatus, String phone, String description) {
        // return new lead object
        return new Lead(FirstName = firstName, LastName = lastName, Email = email,
                Status = leadstatus, Phone = phone, Description = description);
    }
    public static Lead createLead() {
        // Generate a pseudo random email
        String rnd = System.now().getTime() + '-' +
                String.valueOf(Math.round(Math.random() * 100));
        String email = rnd + '_newuser@testorg.com';
        String firstName = 'John';
        String lastName = 'Doe';
        String leadstatus = 'Open';
        String phone = '5551235555';
        String description = 'Google AdWords';
        //Get a new lead object from createLead(String, String, String, String)
        return createLead(firstname, lastname, email, leadstatus, phone, description);
    }

    /* Create Task Methods */
    // create a task with a whoId (contact, lead, etc)
    public static Task createTask(String whoId, String subject, String status, String priority) {
        return new Task(WhoId = whoId, Subject = subject, Status = status, Priority = priority);
    }

    public static Task createTask() {
        return createTask('Task Subject');
    }

    public static Task createTask(String subject) {
        return createTask(String.valueOf(UserInfo.getUserId()), 'Task Subject');
    }

    public static Task createTask(String whoId, String subject) {
        String status = 'Not Started';
        String priority = 'Normal';
        return createTask(whoId, subject, status, priority);
    }

    public static Task createTask(String whatId, String subject, String blank) {
        String status = 'Not Started';
        String priority = 'Normal';
        return createTask(whatId, subject, status, priority, '');
    }

    public static Task createTask(String whatId, String subject, String status, String priority, String blank) {
        return createTask(whatId, subject, status, priority, blank, System.today());
    }

    public static Task createTask(String whatId, String subject, String status, String priority, String blank, Date activityDate) {
        return new Task(whatId = whatId, Subject = subject, Status = status, Priority = priority, ActivityDate = activityDate, Completed_Date__c = status == 'Completed' ? System.now() : null);
    }

    public static Task createTask(String whoId, String subject, Date activityDate) {
        String status = 'Not Started';
        String priority = 'Normal';
        return createTask(whoId, subject, status, priority, null, activityDate);
    }

    public static litify_pm__Matter__c createMatter(Account account) {
        return createMatter(account, null);
    }

    public static litify_pm__Matter__c createMatter(Account account, litify_pm__Matter_Plan__c matterPlan) {
        return new litify_pm__Matter__c(litify_pm__Client__c = account.Id, litify_pm__Matter_Plan__c = matterPlan == null ? null : matterPlan.Id);
    }

    /* Create Attachment Methods */
    public static Attachment createAttachment(Id parentId) {
        return TestUtil.createAttachment(parentId, 'someName');
    }
    public static Attachment createAttachment(Id parentId, String attachmentName) {
        Blob b = Blob.valueOf('Test Data');
        Attachment attachment = new Attachment();
        attachment.ParentId = parentId;
        attachment.Name = attachmentName;
        attachment.Body = b;
        return attachment;
    }

    /* Custom exception class that can be thrown while executing methods in this class */
    public class InvalidIdException extends Exception {
    }
    /* Create Intake records */
    public static Intake__c createIntake() {
        Account acc = TestUtil.createPersonAccount('name', 'Account');
        return new Intake__c(Client__c = acc.Id, Case_Type__c = 'RealEstate',
                What_was_the_date_of_the_incident__c = Date.today(),
                Caller_First_Name__c = 'Test', Caller_Last_Name__c = 'User',
                Representative_Reason__c = 'Test Reason');
    }

    public static Intake__c createIntake(Account personAccount) {
        Intake__c intake = createIntake();
        intake.Client__c = personAccount.Id;
        return intake;
    }

    public static IncidentInvestigationEvent__c createIncidentInvestigationEvent(Incident__c incident) {
        IncidentInvestigationEvent__c incidentInvestigationEvent = new IncidentInvestigationEvent__c();
        incidentInvestigationEvent.Incident__c = incident.Id;
        incidentInvestigationEvent.StartDateTime__c = Date.today().addDays(-5);
        incidentInvestigationEvent.EndDateTime__c = Date.today().addDays(5);
        incidentInvestigationEvent.Investigation_Status__c = 'Scheduled';
        return incidentInvestigationEvent;
    }

    public static List<IncidentInvestigationEvent__c> createIncidentInvestigationEvents(Incident__c incident, Integer num) {
        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = new List<IncidentInvestigationEvent__c>();

        for (Integer i = 0; i < num; i++) {
            incidentInvestigationEvents.add(new IncidentInvestigationEvent__c(
                    Incident__c = incident.Id,
                    StartDateTime__c = Date.today().addDays(-5),
                    EndDateTime__c = Date.today().addDays(5),
                    Investigation_Status__c = 'Scheduled'
            ));
        }

        return incidentInvestigationEvents;
    }

    public static IntakeInvestigationEvent__c createIntakeInvestigationEvent(Intake__c intake, IncidentInvestigationEvent__c incidentInvestigationEvent) {
        IntakeInvestigationEvent__c intakeInvestigationEvent = new IntakeInvestigationEvent__c();
        intakeInvestigationEvent.Intake__c = intake.Id;
        intakeInvestigationEvent.IncidentInvestigation__c = incidentInvestigationEvent.Id;
        intakeInvestigationEvent.StartDateTime__c = Date.today().addDays(-5);
        intakeInvestigationEvent.EndDateTime__c = Date.today().addDays(5);
        intakeInvestigationEvent.Investigation_Status__c = 'Scheduled';
        return intakeInvestigationEvent;
    }

    /**
     * Creates a list of num intake investigation event records
     * @param intake the intake record to which to associate
     * @param incidentInvestigation the incident investigation event record to which to associate
     * @param num the number of intake investigation event records to create
     * @return the list of intake investigation event records
     */
    public static List<IntakeInvestigationEvent__c> createIntakeInvestigationEvents(Intake__c intake, IncidentInvestigationEvent__c incidentInvestigation, Integer num) {
        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = new List<IntakeInvestigationEvent__c>();

        for (Integer i = 0; i < num; i++) {
            intakeInvestigationEvents.add(new IntakeInvestigationEvent__c(
                    Intake__c = intake.Id,
                    IncidentInvestigation__c = incidentInvestigation.Id,
                    StartDateTime__c = Date.today().addDays(-5),
                    EndDateTime__c = Date.today().addDays(5),
                    Investigation_Status__c = 'Scheduled'
            ));
        }

        return intakeInvestigationEvents;
    }

    /**
     * Creates a list of events for incident investigations
     * @param u the user assigned to the event
     * @param incidentInvestigationEvents the incident investigation event records
     * @return the list of event records
     */
    public static List<Event> createIncidentEvents(User u, List<IncidentInvestigationEvent__c> incidentInvestigationEvents) {
        List<Event> events = new List<Event>();

        for (IncidentInvestigationEvent__c incidentInvestigationEvent : incidentInvestigationEvents) {
            events.add(new Event(
                    Subject = 'Some Incident Event',
                    Type = 'Investigation',
                    StartDateTime = Datetime.newInstance(EVENT_DATE.year(), EVENT_DATE.month(), EVENT_DATE.day(), 18, 0, 0).addDays(-1),
                    EndDateTime = Datetime.newInstance(EVENT_DATE.year(), EVENT_DATE.month(), EVENT_DATE.day(), 1, 0, 0),
                    IsAllDayEvent = false,
                    OwnerId = u.Id,
                    WhatId = incidentInvestigationEvent.Id
            ));
        }

        return events;
    }

    /**
     * Creates a list of events for intake investigations
     * @param u the user assigned to the event
     * @param intakeInvestigationEvents the intake investigation event records
     * @return the list of event records
     */
    public static List<Event> createIntakeEvents(User u, List<IntakeInvestigationEvent__c> intakeInvestigationEvents) {
        List<Event> events = new List<Event>();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent : intakeInvestigationEvents) {
            events.add(new Event(
                    Subject = 'Some Intake Event',
                    Type = 'Investigation',
                    StartDateTime = Datetime.newInstance(EVENT_DATE.year(), EVENT_DATE.month(), EVENT_DATE.day(), 18, 0, 0).addDays(-1),
                    EndDateTime = Datetime.newInstance(EVENT_DATE.year(), EVENT_DATE.month(), EVENT_DATE.day(), 1, 0, 0),
                    IsAllDayEvent = false,
                    OwnerId = u.Id,
                    WhatId = intakeInvestigationEvent.IncidentInvestigation__c
            ));
        }

        return events;
    }

    /**
     * Generates a well formed, fake ID for the provided object type.
     * @param objectType the SObject type to generate a fake ID for
     * @return a well formed, fake ID for the provided object type
     */

    public static Id getFakeId(Schema.SObjectType objectType) {
        return fflib_IDGenerator.generate(objectType);
    }

    public static Event createAllDayEvent(Id ownerId, Date startDate, Date endDate) {
        Event e = new Event(
                Subject = 'Test Event',
                IsAllDayEvent = true,
                StartDateTime = Datetime.newInstance(startDate, Time.newInstance(0, 0, 0, 0)),
                EndDateTime = Datetime.newInstance(endDate, Time.newInstance(0, 0, 0, 0)),
                OwnerId = ownerId
        );
        return e;
    }

    public static Event createEvent() {
        return createEvent(UserInfo.getUserId(), Datetime.now(), Datetime.now().addHours(1));
    }

    public static Event createEvent(Id ownerId, Datetime startDatetime, Datetime endDatetime) {
        Event e = new Event(
                Subject = 'Test Event',
                IsAllDayEvent = false,
                StartDateTime = startDatetime,
                EndDateTime = endDatetime,
                OwnerId = ownerId
        );
        return e;
    }

    public static Holiday createHoliday(String name, Date d) {
        return new Holiday(
                ActivityDate = d,
                IsAllDay = true,
                Name = name
        );
    }

    public static Intake__c createIntake_ScheduleInvestigator() {
        Account acct = new Account(Gender__c = 'Male', LastName = 'Test Account');
        insert acct;

        Intake__c intake = new Intake__c(Case_Type__c = 'Xarelto', When_did_you_start_using_the_drug__c = Date.newInstance(2010, 1, 1), Client__c = acct.Id);
        return intake;
    }

    public static Event createHomeTime(Id ownerId, Datetime startAt, Datetime endAt) {
        Event home = new Event(
                OwnerId = ownerId,
                StartDateTime = DateTime.newInstance(startAt.date(), Time.newInstance(startAt.time().hour(), startAt.time().minute(), 0, 0)),
                EndDateTime = DateTime.newInstance(endAt.date(), Time.newInstance(endAt.time().hour(), endAt.time().minute(), 0, 0)),
                Subject = SUBJECT_HOME);
        return home;
    }

    public static Lawsuit__c createLawsuit() {
        Lawsuit__c lawsuit = new Lawsuit__c();
        lawsuit.External_Id__c = mmlib_Utils.generateGuid();
        return lawsuit;
    }

    public static Lawsuit__c createLawsuit(Id intakeId) {
        Lawsuit__c lawsuit = createLawsuit();
        lawsuit.Intake__c = intakeId;
        return lawsuit;
    }

    public static Incident__c createIncident() {
        Incident__c incident = new Incident__c();
        return incident;
    }

    public static dsfs__DocuSign_Status__c createDocusignStatus() {
        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c();
        docuSignStatus.dsfs__Sender__c = 'Unit Test';
        return docuSignStatus;
    }

    public static litify_pm__Matter_Team_Role__c createMatterTeamMemberRole() {
        litify_pm__Matter_Team_Role__c matterTeamRole = new litify_pm__Matter_Team_Role__c();
        matterTeamRole.Name = 'Principle Attorney';
        return matterTeamRole;
    }

    public static litify_pm__Matter_Team_Member__c createMatterTeamMember(litify_pm__Matter__c matter, User user, litify_pm__Role__c role) {
        litify_pm__Matter_Team_Member__c teamMember = new litify_pm__Matter_Team_Member__c();
        teamMember.litify_pm__Matter__c = matter.Id;
        teamMember.litify_pm__User__c = user.Id;
        return teamMember;
    }

    public static litify_pm__Matter_Team_Member__c createMatterTeamMember(litify_pm__Matter__c matter, User user, litify_pm__Matter_Team_Role__c matterTeamRole) {
        litify_pm__Matter_Team_Member__c teamMember = new litify_pm__Matter_Team_Member__c();
        teamMember.litify_pm__Matter__c = matter.Id;
        teamMember.litify_pm__User__c = user.Id;
        teamMember.litify_pm__Role__c = matterTeamRole.Id;
        return teamMember;
    }

    public static litify_pm__Role__c createRole(litify_pm__Matter__c matter, Account account) {
        litify_pm__Role__c role = new litify_pm__Role__c();
        role.litify_pm__Matter__c = matter.Id;
        role.litify_pm__Party__c = account.Id;
        return role;
    }

    public static litify_pm__Expense__c createExpense(litify_pm__Matter__c matter) {
        litify_pm__Expense__c expense = new litify_pm__Expense__c();
        expense.litify_pm__Matter__c = matter.Id;
        expense.litify_pm__Amount__c = 1.00;
        expense.litify_pm__Date__c = Date.today();
        return expense;
    }

    public static Coverage__c createCoverage() {
        Coverage__c coverage = new Coverage__c();
        return coverage;
    }

    public static litify_pm__Damage__c createDamage(litify_pm__Matter__c matter) {
        litify_pm__Damage__c damage = new litify_pm__Damage__c();
        damage.litify_pm__Matter__c = matter.Id;
        return damage;
    }

    public static Damage_Payment__c createDamagePayment(litify_pm__Matter__c matter, Litify_pm__Damage__c damage) {
        Damage_Payment__c damagePayment = new Damage_Payment__c();
        damagePayment.Matter__c = matter.Id;
        damagePayment.Litify_Damage__c = damage.Id;
        return damagePayment;
    }

    public static litify_pm__Firm__c createFirm(String name) {
        litify_pm__Firm__c firm = new litify_pm__Firm__c();
        firm.Name = name;
        firm.litify_pm__ExternalId__c = 150;
        firm.litify_pm__Is_Available__c = true;
        return firm;
    }

    public static litify_pm__Case_Type__c createCaseType(String name) {
        litify_pm__Case_Type__c caseType = new litify_pm__Case_Type__c();
        caseType.Name = name;
        return caseType;
    }

    public static litify_pm__Case_Type__c createCaseType() {
        return createCaseType('General Injury');
    }

    public static litify_pm__Referral__c createReferral(litify_pm__Case_Type__c caseType, litify_pm__Firm__c firm) {
        litify_pm__Referral__c referral = new litify_pm__Referral__c();
        referral.RecordTypeId = getRecordTypeIdByName('Third Party Outgoing Referral');
        referral.litify_pm__ExternalId__c = 877;
        referral.litify_pm__Case_Type__c = caseType.Id;
        referral.litify_pm__Client_First_Name__c = 'Unit';
        referral.litify_pm__Client_Last_Name__c = 'Test';
        referral.litify_pm__Case_State__c = 'GA Georgia';
        referral.litify_pm__Client_phone__c = '111-222-3333';
        referral.litify_pm__Handling_Firm__c = firm.Id;
        return referral;
    }

    public static Deposit__c createDeposit(litify_pm__Matter__c matter) {
        Deposit__c deposit = new Deposit__c();
        deposit.RecordTypeId = [select Id from RecordType where SobjectType = 'Deposit__c' and DeveloperName = 'Operating' limit 1].Id;
        deposit.Matter__c = matter.Id;
        deposit.Amount__c = 40.00;
        deposit.Source__c = 'Settlement';
        deposit.Check_Date__c = Date.today();
        return deposit;
    }

    /*public static Marketing_Campaign__c createMarketingCampaign() {
        Marketing_Campaign__c marketingCampaign = new Marketing_Campaign__c();

        marketingCampaign.Campaign_Name__c = utmCampaignValue;
        marketingCampaign.utm_campaign__c = utmCampaignValue.toLowerCase();
        marketingCampaign.Source_Campaign_ID__c = sourceCampaignIDValue;
        marketingCampaign.Domain_Value__c = domainValue;
    }*/

    private static String getRecordTypeIdByName(String recordTypeName) {
        List<RecordType> recordTypes = [select Id, Name from RecordType where Name = :recordTypeName];

        if (recordTypes.size() > 0) {
            return recordTypes[0].Id;
        }

        return null;
    }

    public static Fee_Goal__c createFeeGoal() {
        Fee_Goal__c feeGoal = new Fee_Goal__c();
        feeGoal.Employee__c = UserInfo.getUserId();
        feeGoal.OwnerId = UserInfo.getUserId();
        return feeGoal;
    }

    public static Settlement__c createSettlement() {
        Settlement__c settlement = new Settlement__c();
        settlement.Fee_Amount_if_not_percent__c = 100.00;
        return settlement;
    }

    public static Fee_Goal_Allocation__c createFeeGoalAllocation(Fee_Goal__c feeGoal, Settlement__c settlement) {
        Fee_Goal_Allocation__c feeGoalAllocation = new Fee_Goal_Allocation__c();
        feeGoalAllocation.Fee_Goal__c = feeGoal.Id;
        feeGoalAllocation.Settlement__c = settlement.Id;
        return feeGoalAllocation;
    }

    public static Dice_Queue__c createDiceQueue() {
        Dice_Queue__c diceQueue = new Dice_Queue__c();
        return diceQueue;
    }

    public static Marketing_Tracking_Info__c createMarketingTrackingInfo(Intake__c intake) {
        Marketing_Tracking_Info__c marketingTrackingInfo = new Marketing_Tracking_Info__c();
        marketingTrackingInfo.Intake__c = intake.Id;
        return marketingTrackingInfo;
    }
}