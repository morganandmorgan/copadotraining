@isTest
private class mmpsc_AccountSearchInputControllerTest
{
    @isTest
    static void test()
    {
        Lawsuit__c ls = new Lawsuit__c();
        ls.Client_Name__c = 'Tom Petty';
        ls.Id = fflib_IDGenerator.generate(Lawsuit__c.SObjectType);

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmlawsuit_ILawsuitsSelector mockLawsuitsSelector = (mmlawsuit_ILawsuitsSelector) mocks.mock(mmlawsuit_LawsuitsSelector.class);

        mocks.startStubbing();
        mocks.when(mockLawsuitsSelector.selectByIntake(new Set<Id> {ls.Id})).thenReturn(new List<Lawsuit__c> {ls});
        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockLawsuitsSelector);

        mmlawsuit_ILawsuitsSelector sel = mmlawsuit_LawsuitsSelector.newInstance();
        System.debug(JSON.serializePretty(sel.selectByIntake(new Set<Id> {ls.Id})));
    }
}