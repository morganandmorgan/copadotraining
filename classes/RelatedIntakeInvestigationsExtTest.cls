@isTest
private class RelatedIntakeInvestigationsExtTest {

	@TestSetup
    private static void setup() {
        List<SObject> toInsert = new List<SObject>();

        Account client = TestUtil.createPersonAccount('Test', 'Client');
        toInsert.add(client);

        Incident__c incident = new Incident__c();
        toInsert.add(incident);

        Database.insert(toInsert);
        toInsert.clear();

        List<Intake__c> intakes = new List<Intake__c>();
        for (Integer i = 0; i < 3; ++i) {
            intakes.add(new Intake__c(
                    Incident__c = incident.Id,
                    Client__c = client.Id
                ));
        }
        toInsert.addAll((List<SObject>) intakes);

        IncidentInvestigationEvent__c incidentInvestigation = new IncidentInvestigationEvent__c(
                Incident__c = incident.Id,
                EndDateTime__c = Datetime.now().addHours(2),
                StartDateTime__c = Datetime.now().addHours(1),
                Investigation_Status__c = 'Scheduled'
            );
        toInsert.add(incidentInvestigation);

        Database.insert(toInsert);
        toInsert.clear();

        Event calendarEvent = new Event(
                Subject = 'Test Subject',
                WhatId = incidentInvestigation.Id,
                StartDateTime = incidentInvestigation.StartDateTime__c,
                EndDateTime = incidentInvestigation.EndDateTime__c,
                Investigation_Status__c = 'Scheduled',
                Type = ScheduleInvestigatorService.EVENT_TYPE
            );
        toInsert.add(calendarEvent);

        List<IntakeInvestigationEvent__c> intakeInvestigations = new List<IntakeInvestigationEvent__c>();
        for (Intake__c intake : intakes) {
            intakeInvestigations.add(new IntakeInvestigationEvent__c(
                    IncidentInvestigation__c = incidentInvestigation.Id,
                    Intake__c = intake.Id,
                    StartDateTime__c = incidentInvestigation.StartDateTime__c,
                    EndDateTime__c = incidentInvestigation.EndDateTime__c,
                    Investigation_Status__c = 'Scheduled'
                ));
        }
        toInsert.addAll((List<SObject>) intakeInvestigations);

        Database.insert(toInsert);
        toInsert.clear();
    }

    private static List<Intake__c> getIntakes() {
        return [SELECT Id, Client__c FROM Intake__c];
    }

    @isTest
    private static void intakeNotIncludedInInvestigation() {
        List<Intake__c> intakes = getIntakes();

        Intake__c selectedIntake = intakes.get(0);

        List<IncidentInvestigationEvent__c> toDelete = [SELECT Id FROM IncidentInvestigationEvent__c];

        Database.delete(toDelete);

        Test.startTest();
        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assert(controller.HasEvents);

        for (RelatedIntakeInvestigationsExt.InvestigationWrapper wrapper : controller.IntakeInvestigations) {
            System.assertNotEquals(null, wrapper.startDateTime);
            System.assertNotEquals(null, wrapper.investigationStatus);
            System.assertEquals(null, wrapper.Event);
        }
    }

    @isTest
    private static void incidentDoesNotHaveEvent() {
        List<Intake__c> intakes = getIntakes();

        Intake__c selectedIntake = intakes.get(0);

        List<Event> toDelete = [SELECT Id FROM Event];

        Database.delete(toDelete);

        Test.startTest();
        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assert(controller.HasEvents);
        System.assertEquals(toDelete.size(), controller.IntakeInvestigations.size());

        for (RelatedIntakeInvestigationsExt.InvestigationWrapper wrapper : controller.IntakeInvestigations) {
            System.assertNotEquals(null, wrapper.startDateTime);
            System.assertNotEquals(null, wrapper.investigationStatus);
            System.assertEquals(null, wrapper.Event);
        }
    }

    @isTest
    private static void incidentHasEvent() {
        List<Intake__c> intakes = getIntakes();

        Intake__c selectedIntake = intakes.get(0);

        Test.startTest();
        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assert(controller.HasEvents);
    }

//    @isTest
//    private static void hasOtherInvestigations() {
//        List<Intake__c> intakes = getIntakes();
//
//        Intake__c selectedIntake = intakes.get(0);
//        Map<Id, Intake__c> otherIntakes = new Map<Id, Intake__c>(intakes);
//        otherIntakes.remove(selectedIntake.Id);
//
//        Test.startTest();
//        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
//        Test.stopTest();
//
//        System.assertEquals(1, controller.IntakeInvestigations.size());
//        System.assertEquals(selectedIntake.Id, controller.IntakeInvestigations.get(0).intake.Id);
//
//        System.assertEquals(otherIntakes.size(), controller.otherIntakeInvestigations.size());
//        for (RelatedIntakeInvestigationsExt.InvestigationWrapper wrapper : controller.otherIntakeInvestigations) {
//            System.assert(otherIntakes.containsKey(wrapper.intake.Id));
//        }
//    }

    @isTest
    private static void excludeOtherCanceledInvestigations() {
        List<Intake__c> intakes = getIntakes();

        for (List<IntakeInvestigationEvent__c> toCancelList : [SELECT Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c IN :intakes]) {
            for (IntakeInvestigationEvent__c toCancel : toCancelList) {
                toCancel.Investigation_Status__c = 'Canceled';
            }
            Database.update(toCancelList);
        }

        Intake__c selectedIntake = intakes.get(0);

        Test.startTest();
        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assertEquals(1, controller.IntakeInvestigations.size());
        System.assertEquals(selectedIntake.Id, controller.IntakeInvestigations.get(0).intake.Id);

        System.assertEquals(0, controller.otherIntakeInvestigations.size());
    }

    @isTest
    private static void canceledEventsHaveInactiveLink() {
        List<Intake__c> intakes = getIntakes();

        for (List<IntakeInvestigationEvent__c> toCancelList : [SELECT Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c IN :intakes]) {
            for (IntakeInvestigationEvent__c toCancel : toCancelList) {
                toCancel.Investigation_Status__c = 'Canceled';
            }
            Database.update(toCancelList);
        }

        Intake__c selectedIntake = intakes.get(0);

        Test.startTest();
        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assertEquals(1, controller.IntakeInvestigations.size());
        System.assertEquals(false, controller.IntakeInvestigations.get(0).getEventLinkIsActive());
    }

    @isTest
    private static void rescheduledEventsHaveInactiveLink() {
        List<Intake__c> intakes = getIntakes();

        for (List<IntakeInvestigationEvent__c> toRescheduleList : [SELECT Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c IN :intakes]) {
            for (IntakeInvestigationEvent__c toReschedule : toRescheduleList) {
                toReschedule.Investigation_Status__c = 'Rescheduled';
            }
            Database.update(toRescheduleList);
        }

        Intake__c selectedIntake = intakes.get(0);

        Test.startTest();
        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
        Test.stopTest();

        System.assertEquals(1, controller.IntakeInvestigations.size());
        System.assertEquals(false, controller.IntakeInvestigations.get(0).getEventLinkIsActive());
    }

//    @isTest
//    private static void scheduledEventsHaveInactiveLink() {
//        List<Intake__c> intakes = getIntakes();
//
//        for (List<IntakeInvestigationEvent__c> existingRecords : [SELECT Investigation_Status__c FROM IntakeInvestigationEvent__c WHERE Intake__c IN :intakes]) {
//            for (IntakeInvestigationEvent__c existingRecord : existingRecords) {
//                System.assertNotEquals(existingRecord.Investigation_Status__c, 'Canceled');
//                System.assertNotEquals(existingRecord.Investigation_Status__c, 'Rescheduled');
//            }
//        }
//
//        Intake__c selectedIntake = intakes.get(0);
//
//        Test.startTest();
//        RelatedIntakeInvestigationsExt controller = new RelatedIntakeInvestigationsExt(new ApexPages.StandardController(selectedIntake));
//        Test.stopTest();
//
//        System.assertEquals(1, controller.IntakeInvestigations.size());
//        System.assertEquals(true, controller.IntakeInvestigations.get(0).getEventLinkIsActive());
//
//        System.assertEquals(intakes.size() - 1, controller.otherIntakeInvestigations.size());
//        for (RelatedIntakeInvestigationsExt.InvestigationWrapper wrapper : controller.otherIntakeInvestigations) {
//            System.assertEquals(true, wrapper.getEventLinkIsActive());
//        }
//    }
}