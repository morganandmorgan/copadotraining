@isTest
private class InvestigationEventTriggerHandlerTest {
    private static final String VALID_TERRITORY = 'Territory A';
  private static final String INVALID_TERRITORY = 'Territory B';
  private static final Date EVENT_DATE = Date.newInstance(2010, 5, 25);

    private static Integer userIncrement = 0;
    private static InvestigationEventTriggerHandler handler = new InvestigationEventTriggerHandler();

    @testSetup
    static void setup() {
        List<User> users = buildUsers();
        System.runAs(new User(Id = UserInfo.getUserId())) {
            Database.insert(users);
        }

        List<Account> testAccounts = new List<Account>{ TestUtil.createPersonAccount('Client', 'One'),
                                                        TestUtil.createPersonAccount('Injured', 'Party'),
                                                        TestUtil.createPersonAccount('The', 'Caller') };
        insert testAccounts;

        Set<Id> idSet = new Set<Id>();
        for (Account a : testAccounts) {
            idSet.add(a.Id);
        }
        testAccounts = [select Id, PersonContactId from Account where Id in :idSet];

        Incident__c incident = new Incident__c();
        insert incident;

        Intake__c intake = createTestIntake(incident.Id, testAccounts[2].Id, testAccounts[0].Id, testAccounts[1].Id);
        insert intake;

        //List<IncidentInvestigationEvent__c> incidentInvestigationEvents = TestUtil.createIncidentInvestigationEvents(incident, 3);
        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = TestUtil.createIncidentInvestigationEvents(incident, 1);
        insert incidentInvestigationEvents;

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = TestUtil.createIntakeInvestigationEvents(intake, incidentInvestigationEvents[0], 3);
        for (IntakeInvestigationEvent__c iie : intakeInvestigationEvents) {
            iie.Contact__c = testAccounts.get(0).PersonContactId;
        }
        insert intakeInvestigationEvents;

        List<Event> events = TestUtil.createIncidentEvents(users[0], incidentInvestigationEvents);
        //events.addAll(TestUtil.createIntakeEvents(users[0], intakeInvestigationEvents));
        insert events;
    }

    private static User buildUser() {
        Integer userNum = userIncrement++;
        return new User(
            IsActive = true,
            FirstName = 'FirstName ' + userNum,
            LastName = 'LastName ' + userNum,
            Username = 'apex.temp.test.user@sample.com.' + userNum,
            Email = 'apex.temp.test.user@sample.com.' + userNum,
            Alias = 'alib' + userNum,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            Territory__c = VALID_TERRITORY
        );
    }

    private static List<User> buildUsers() {
        List<User> result = new List<User>();

        User inactiveUser = buildUser();
        inactiveUser.IsActive = false;
        inactiveUser.Language__c = 'English;French';
        result.add(inactiveUser);

        User invalidTerritoryUser = buildUser();
        invalidTerritoryUser.Territory__c = INVALID_TERRITORY;
        invalidTerritoryUser.Language__c = 'French;Spanish';
        result.add(invalidTerritoryUser);

        User noLanguageUser = buildUser();
        invalidTerritoryUser.Language__c = null;
        result.add(noLanguageUser);

        User someLanguageUser = buildUser();
        someLanguageUser.Language__c = 'Italian;Spanish';
        result.add(someLanguageUser);

        return result;
    }

    private static Intake__c createTestIntake(Id incidentId, Id callerId, Id clientId, Id injuredPartyId) {
        return new Intake__c(
            Caller__c = callerId,
            Client__c = clientId,
            Incident__c = incidentId,
            Injured_Party__c = injuredPartyId,
            Case_Type__c = 'RealEstate',
            What_was_the_date_of_the_incident__c = Date.today(),
            Caller_First_Name__c = 'Test', Caller_Last_Name__c = 'User',
            Representative_Reason__c = 'Test Reason',
            Catastrophic_Injury__c='Yes'
        );
    }

    private static Incident__c getTestIncident() {
        return [
            SELECT
                Id
            FROM
                Incident__c
        ];
    }

    private static Intake__c getTestIntake() {
        return [
            SELECT
                Id
            FROM
                Intake__c
        ];
    }

    private static List<User> getUsers() {
        return [
            SELECT
                Id
            FROM
                User
            WHERE
                Territory__c = :VALID_TERRITORY
        ];
    }

    private static List<Event> getIncidentEvents() {
        return [
            SELECT
                Id,
                Subject,
                Type,
                WhatId,
                WhoId,
                Who.Type,
                StartDateTime,
                EndDateTime,
                OwnerId,
                Reason_for_Delayed_Signup__c,
                Location,
                Contracts_to_be_Signed__c,
                Investigation_Status__c,
                To_Be_Canceled__c,
                To_Be_Rescheduled__c
            FROM
                Event
            WHERE
                Subject = 'Some Incident Event'

        ];
    }

    private static List<Event> getIntakeEvents() {
        return [
            SELECT
                Id,
                Type,
                WhatId,
                WhoId,
                Who.Type,
                StartDateTime,
                EndDateTime,
                Reason_for_Delayed_Signup__c,
                Location
            FROM
                Event
            WHERE
                Subject = 'Some Intake Event'

        ];
    }

    private static List<IncidentInvestigationEvent__c> getIncidentInvestigationEvents() {
        return [
            SELECT
                OwnerId,
                StartDateTime__c,
                EndDateTime__c,
                Investigation_Status__c,
                Reason_for_Delayed_Signup__c,
                Contracts_to_be_Signed__c
            FROM
                IncidentInvestigationEvent__c
        ];
    }

    private static IncidentInvestigationEvent__c getIncidentInvestigationEventByWhatId(Id whatId) {
        return [
            SELECT
                OwnerId,
                StartDateTime__c,
                EndDateTime__c,
                Investigation_Status__c,
                Reason_for_Delayed_Signup__c,
                Contracts_to_be_Signed__c
            FROM
                IncidentInvestigationEvent__c
            WHERE
                Id = :whatId
            LIMIT 1
        ];
    }

    private static List<IntakeInvestigationEvent__c> getIntakeInvestigationEvents() {
        return [
            SELECT
                OwnerId,
                StartDateTime__c,
                EndDateTime__c,
                Investigation_Status__c,
                Reason_for_Delayed_Signup__c
            FROM
                IntakeInvestigationEvent__c
        ];
    }

    private static void copyToShadowObjects(Event evt) {
        List<SObject> toUpdate = new List<SObject>();

        List<IncidentInvestigationEvent__c> incidentInvestigations = [
            SELECT
                OwnerId, StartDateTime__c, EndDateTime__c, Investigation_Status__c, Reason_for_Delayed_Signup__c,
                (SELECT OwnerId, StartDateTime__c, EndDateTime__c, Investigation_Status__c, Location__c, Catastrophic__c, Contact__c, Intake__r.Catastrophic__c, Intake__r.Client__r.PersonContactId, Reason_for_Delayed_Signup__c
                 FROM IntakeInvestigationEvents__r)
            FROM
                IncidentInvestigationEvent__c
            WHERE
                Id = :evt.WhatId
        ];

        for (IncidentInvestigationEvent__c incidentInvestigation : incidentInvestigations) {
            incidentInvestigation.StartDateTime__c = evt.StartDateTime;
            incidentInvestigation.EndDateTime__c = evt.EndDateTime;
            incidentInvestigation.OwnerId = evt.OwnerId;
            incidentInvestigation.Reason_for_Delayed_Signup__c = evt.Reason_for_Delayed_Signup__c;

            List<IntakeInvestigationEvent__c> childIntakeInvestigations = incidentInvestigation.IntakeInvestigationEvents__r;
              for (IntakeInvestigationEvent__c childIntakeInvestigation : childIntakeInvestigations) {
                  childIntakeInvestigation.StartDateTime__c = evt.StartDateTime;
                childIntakeInvestigation.EndDateTime__c = evt.EndDateTime;
                childIntakeInvestigation.OwnerId = evt.OwnerId;
                childIntakeInvestigation.Location__c = evt.Location;
                childIntakeInvestigation.Catastrophic__c = childIntakeInvestigation.Intake__r.Catastrophic__c;
                childIntakeInvestigation.Reason_for_Delayed_Signup__c = evt.Reason_for_Delayed_Signup__c;
                childIntakeInvestigation.Contact__c = childIntakeInvestigation.Intake__r.Client__r.PersonContactId;
              }
            toUpdate.addAll((List<Sobject>) childIntakeInvestigations);
        }
        toUpdate.addAll((List<Sobject>) incidentInvestigations);

        Database.update(toUpdate);
    }

    @isTest
    static void testDeleteHandler_InvesigationStatusUpdated_Canceled() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);
        evt.To_Be_Canceled__c = true;

        Test.startTest();
        handler.afterDeleteHandler(new List<Event>{ evt });
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, incidentInvestigationEvent.Investigation_Status__c);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, intakeInvestigationEvent.Investigation_Status__c);
        }
    }

    @isTest
    static void testDeleteHandler_InvesigationStatusUpdated_Rescheduled() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);
        evt.To_Be_Rescheduled__c = true;

        Test.startTest();
        handler.afterDeleteHandler(new List<Event>{ evt });
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(InvestigationEventTriggerHandler.RESCHEDULE_STATUS, incidentInvestigationEvent.Investigation_Status__c);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(InvestigationEventTriggerHandler.RESCHEDULE_STATUS, intakeInvestigationEvent.Investigation_Status__c);
        }
    }

//    @isTest
//    static void testDeleteHandler_WhatIdRespected_RecordsUnrelatedToEventAreNotModified() {
//        List<User> users = getUsers();
//
//        List<Account> testAccounts = new List<Account>{ TestUtil.createPersonAccount('Client', 'One') };
//        insert testAccounts;
//        Set<Id> idSet = new Set<Id>();
//        for (Account a : testAccounts) {
//            idSet.add(a.Id);
//        }
//        testAccounts = [select Id, PersonContactId from Account where Id in :idSet];
//
//        Incident__c incident = getTestIncident();
//        Intake__c intake = getTestIntake();
//
//        List<IncidentInvestigationEvent__c> newIncidentInvestigationEvents = TestUtil.createIncidentInvestigationEvents(incident, 3);
//        Database.insert(newIncidentInvestigationEvents);
//        Map<Id, IncidentInvestigationEvent__c> newIncidentInvestigationEventsMap = new Map<Id, IncidentInvestigationEvent__c>(newIncidentInvestigationEvents);
//
//        List<IntakeInvestigationEvent__c> newIntakeInvestigationEvents = TestUtil.createIntakeInvestigationEvents(intake, newIncidentInvestigationEvents.get(0), 3);
//        for (IntakeInvestigationEvent__c iie : newIntakeInvestigationEvents) {
//            iie.Contact__c = testAccounts.get(0).PersonContactId;
//        }
//        Database.insert(newIntakeInvestigationEvents);
//        Map<Id, IntakeInvestigationEvent__c> newIntakeInvestigationEventsMap = new Map<Id, IntakeInvestigationEvent__c>(newIntakeInvestigationEvents);
//
//        List<Event> newEvents = TestUtil.createIncidentEvents(users.get(0), newIncidentInvestigationEvents);
//        Database.insert(newEvents);
//
//        for (Event newEvent : newEvents) {
//            newEvent.To_Be_Canceled__c = true;
//        }
//
//        Test.startTest();
//        handler.afterDeleteHandler(newEvents);
//        Test.stopTest();
//
//        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = getIncidentInvestigationEvents();
//
//        for (IncidentInvestigationEvent__c incidentInvestigationEvent: incidentInvestigationEvents) {
//            if (newIncidentInvestigationEventsMap.containsKey(incidentInvestigationEvent.Id)) {
//                System.assertEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, incidentInvestigationEvent.Investigation_Status__c);
//            }
//            else {
//                System.assertNotEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, incidentInvestigationEvent.Investigation_Status__c);
//            }
//        }
//
//        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();
//
//        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
//            if (newIntakeInvestigationEventsMap.containsKey(intakeInvestigationEvent.Id)) {
//                System.assertEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, intakeInvestigationEvent.Investigation_Status__c);
//            }
//            else {
//                System.assertNotEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, intakeInvestigationEvent.Investigation_Status__c);
//            }
//        }
//    }

    @isTest
    static void testDeleteHandler_WrongEventType() {
        List<User> users = getUsers();

        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);
        evt.Type = 'Not Investigation Type';

        Test.startTest();
        handler.afterDeleteHandler(new List<Event>{ evt });
        Test.stopTest();

        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = getIncidentInvestigationEvents();

        for (IncidentInvestigationEvent__c incidentInvestigationEvent: incidentInvestigationEvents) {
            System.assertNotEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, incidentInvestigationEvent.Investigation_Status__c);
        }

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertNotEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, intakeInvestigationEvent.Investigation_Status__c);
        }
    }

    @isTest
    static void testUpdateHandler_UpdateSuccess_ChangeToStartTime() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);

        copyToShadowObjects(evt);

        Date newStartDate = Date.today().addDays(20);

        evt.StartDateTime = newStartDate;

        Test.startTest();
        handler.afterUpdateHandler(evt, evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(evt.StartDateTime, incidentInvestigationEvent.StartDateTime__c);
        System.assertEquals(evt.EndDateTime, incidentInvestigationEvent.EndDateTime__c);
        System.assertEquals(evt.OwnerId, incidentInvestigationEvent.OwnerId);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(evt.StartDateTime, intakeInvestigationEvent.StartDateTime__c);
            System.assertEquals(evt.EndDateTime, intakeInvestigationEvent.EndDateTime__c);
            System.assertEquals(evt.OwnerId, intakeInvestigationEvent.OwnerId);
        }
    }

    @isTest
    static void testUpdateHandler_UpdateSuccess_ChangeToEndTime() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);

        copyToShadowObjects(evt);

        Date newEndDate = Date.today().addDays(20);

        evt.EndDateTime = newEndDate;

        Test.startTest();
        handler.afterUpdateHandler(evt, evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(evt.StartDateTime, incidentInvestigationEvent.StartDateTime__c);
        System.assertEquals(evt.EndDateTime, incidentInvestigationEvent.EndDateTime__c);
        System.assertEquals(evt.OwnerId, incidentInvestigationEvent.OwnerId);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(evt.StartDateTime, intakeInvestigationEvent.StartDateTime__c);
            System.assertEquals(evt.EndDateTime, intakeInvestigationEvent.EndDateTime__c);
            System.assertEquals(evt.OwnerId, intakeInvestigationEvent.OwnerId);
        }
    }

    @isTest
    static void testUpdateHandler_UpdateSuccess_ChangeToOwner() {
        List<User> users = getUsers();
        User u = users.get(0);

        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);

        copyToShadowObjects(evt);

        evt.OwnerId = u.Id;

        Test.startTest();
        handler.afterUpdateHandler(evt, evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(evt.StartDateTime, incidentInvestigationEvent.StartDateTime__c);
        System.assertEquals(evt.EndDateTime, incidentInvestigationEvent.EndDateTime__c);
        System.assertEquals(evt.OwnerId, incidentInvestigationEvent.OwnerId);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(evt.StartDateTime, intakeInvestigationEvent.StartDateTime__c);
            System.assertEquals(evt.EndDateTime, intakeInvestigationEvent.EndDateTime__c);
            System.assertEquals(evt.OwnerId, intakeInvestigationEvent.OwnerId);
        }
    }

    @isTest
    static void testUpdateHandler_UpdateSuccess_ChangeToDelayReason() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);

        copyToShadowObjects(evt);

        evt.Reason_for_Delayed_Signup__c = 'A Different Signup Reason';

        Test.startTest();
        handler.afterUpdateHandler(evt, evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(evt.StartDateTime, incidentInvestigationEvent.StartDateTime__c);
        System.assertEquals(evt.EndDateTime, incidentInvestigationEvent.EndDateTime__c);
        System.assertEquals(evt.OwnerId, incidentInvestigationEvent.OwnerId);
        System.assertEquals(evt.Reason_for_Delayed_Signup__c, incidentInvestigationEvent.Reason_for_Delayed_Signup__c);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(evt.StartDateTime, intakeInvestigationEvent.StartDateTime__c);
            System.assertEquals(evt.EndDateTime, intakeInvestigationEvent.EndDateTime__c);
            System.assertEquals(evt.OwnerId, intakeInvestigationEvent.OwnerId);
            System.assertEquals(evt.Reason_for_Delayed_Signup__c, intakeInvestigationEvent.Reason_for_Delayed_Signup__c);
        }
    }

 //   @isTest
 //   static void testUpdateHandler_WhatIdRespected_RecordsUnrelatedToEventAreNotModified() {
 //       List<User> users = getUsers();
 //       Date newStartDate = Date.today().addDays(20);
 //       Date newEndDate = Date.today().addDays(40);
 //       User u = users.get(0);
 //
 //       List<Account> testAccounts = new List<Account>{ TestUtil.createPersonAccount('Client', 'One') };
 //       insert testAccounts;
 //       Set<Id> idSet = new Set<Id>();
 //       for (Account a : testAccounts) {
 //           idSet.add(a.Id);
 //       }
 //       testAccounts = [select Id, PersonContactId from Account where Id in :idSet];
 //
 //       // reset owner Id
 //       List<IncidentInvestigationEvent__c> incidentInvestigationEvents = getIncidentInvestigationEvents();
 //       for (IncidentInvestigationEvent__c incidentInvestigationEvent : incidentInvestigationEvents) {
 //           incidentInvestigationEvent.OwnerId = UserInfo.getUserId();
 //       }
 //       Database.update(incidentInvestigationEvents);
 //
 //       List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();
 //       for (IntakeInvestigationEvent__c intakeInvestigationEvent : intakeInvestigationEvents) {
 //           intakeInvestigationEvent.OwnerId = UserInfo.getUserId();
 //       }
 //       Database.update(intakeInvestigationEvents);
 //
 //
 //
 //       Incident__c incident = getTestIncident();
 //       Intake__c intake = getTestIntake();
 //
 //       List<IncidentInvestigationEvent__c> newIncidentInvestigationEvents = TestUtil.createIncidentInvestigationEvents(incident, 3);
 //       Database.insert(newIncidentInvestigationEvents);
 //       Map<Id, IncidentInvestigationEvent__c> newIncidentInvestigationEventsMap = new Map<Id, IncidentInvestigationEvent__c>(newIncidentInvestigationEvents);
 //
 //       List<IntakeInvestigationEvent__c> newIntakeInvestigationEvents = TestUtil.createIntakeInvestigationEvents(intake, newIncidentInvestigationEvents.get(0), 3);
 //       for (IntakeInvestigationEvent__c iie : newIntakeInvestigationEvents) {
 //           iie.Contact__c = testAccounts.get(0).PersonContactId;
 //       }
 //       Database.insert(newIntakeInvestigationEvents);
 //       Map<Id, IntakeInvestigationEvent__c> newIntakeInvestigationEventsMap = new Map<Id, IntakeInvestigationEvent__c>(newIntakeInvestigationEvents);
 //
 //       List<Event> newEvents = TestUtil.createIncidentEvents(u, newIncidentInvestigationEvents);
 //       Database.insert(newEvents);
 //
 //       for (Event newEvent : newEvents) {
 //           newEvent.StartDateTime = newStartDate;
 //           newEvent.EndDateTime = newEndDate;
 //           newEvent.OwnerId = u.Id;
 //       }
 //
 //       Test.startTest();
 //       handler.afterUpdateHandler(newEvents, newEvents);
 //       Test.stopTest();
 //
 //       List<IncidentInvestigationEvent__c> resultIncidentInvestigationEvents = getIncidentInvestigationEvents();
 //
 //       for (IncidentInvestigationEvent__c incidentInvestigationEvent: resultIncidentInvestigationEvents) {
 //           if (newIncidentInvestigationEventsMap.containsKey(incidentInvestigationEvent.Id)) {
 //               System.assertEquals(newStartDate, incidentInvestigationEvent.StartDateTime__c);
 //               System.assertEquals(newEndDate, incidentInvestigationEvent.EndDateTime__c);
 //               System.assertEquals(u.Id, incidentInvestigationEvent.OwnerId);
 //           }
 //           else {
 //               System.assertNotEquals(newStartDate, incidentInvestigationEvent.StartDateTime__c);
 //               System.assertNotEquals(newEndDate, incidentInvestigationEvent.EndDateTime__c);
 //               System.assertNotEquals(u.Id, incidentInvestigationEvent.OwnerId);
 //           }
 //       }
 //
 //       List<IntakeInvestigationEvent__c> resultIntakeInvestigationEvents = getIntakeInvestigationEvents();
 //
 //       for (IntakeInvestigationEvent__c intakeInvestigationEvent: resultIntakeInvestigationEvents) {
 //           if (newIntakeInvestigationEventsMap.containsKey(intakeInvestigationEvent.Id)) {
 //               System.assertEquals(newStartDate, intakeInvestigationEvent.StartDateTime__c);
 //               System.assertEquals(newEndDate, intakeInvestigationEvent.EndDateTime__c);
 //               System.assertEquals(u.Id, intakeInvestigationEvent.OwnerId);
 //           }
 //           else {
 //               System.assertNotEquals(newStartDate, intakeInvestigationEvent.StartDateTime__c);
 //               System.assertNotEquals(newEndDate, intakeInvestigationEvent.EndDateTime__c);
 //               System.assertNotEquals(u.Id, intakeInvestigationEvent.OwnerId);
 //           }
 //       }
 //   }

    @isTest
    static void testUpdateHandler_WrongEventType() {
        List<User> users = getUsers();
        Date newStartDate = Date.today().addDays(20);
        Date newEndDate = Date.today().addDays(40);
        User u = users.get(0);

        // reset owner Id
        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = getIncidentInvestigationEvents();
        for (IncidentInvestigationEvent__c incidentInvestigationEvent : incidentInvestigationEvents) {
            incidentInvestigationEvent.OwnerId = UserInfo.getUserId();
        }
        Database.update(incidentInvestigationEvents);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();
        for (IntakeInvestigationEvent__c intakeInvestigationEvent : intakeInvestigationEvents) {
            intakeInvestigationEvent.OwnerId = UserInfo.getUserId();
        }
        Database.update(intakeInvestigationEvents);

        List<Event> events = getIncidentEvents();
        for (Event evt : events) {
            evt.Type = 'Not Investigation Type';
        }

        Test.startTest();
        handler.afterUpdateHandler(events, events);
        Test.stopTest();

        List<IncidentInvestigationEvent__c> resultIncidentInvestigationEvents = getIncidentInvestigationEvents();

        for (IncidentInvestigationEvent__c incidentInvestigationEvent: resultIncidentInvestigationEvents) {
            System.assertNotEquals(newStartDate, incidentInvestigationEvent.StartDateTime__c);
            System.assertNotEquals(newEndDate, incidentInvestigationEvent.EndDateTime__c);
            System.assertNotEquals(u.Id, incidentInvestigationEvent.OwnerId);
        }

        List<IntakeInvestigationEvent__c> resultIntakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: resultIntakeInvestigationEvents) {
            System.assertNotEquals(newStartDate, intakeInvestigationEvent.StartDateTime__c);
            System.assertNotEquals(newEndDate, intakeInvestigationEvent.EndDateTime__c);
            System.assertNotEquals(u.Id, intakeInvestigationEvent.OwnerId);
        }
    }

    @isTest
    static void AfterUpdate_HasTravelTime_TravelIsMovedWithEvent() {
        Event testEvent = getIncidentEvents()[0];

        Event travelTimeBefore = new Event(StartDateTime = testEvent.StartDateTime.addHours(-1), EndDateTime = testEvent.StartDateTime, Subject = ScheduleInvestigatorService.SUBJECT_TRAVEL, OwnerId = testEvent.ownerId, WhatId = testEvent.WhatId);
        insert travelTimeBefore;

        Event travelTimeAfter = new Event(StartDateTime = testEvent.EndDateTime, EndDateTime = testEvent.EndDateTime.addHours(1), Subject = ScheduleInvestigatorService.SUBJECT_TRAVEL, OwnerId = testEvent.ownerId, WhatId = testEvent.WhatId);
        insert travelTimeAfter;

        Event updatedEvent = new Event(StartDateTime = testEvent.StartDateTime, EndDateTime = testEvent.EndDateTime, Subject = testEvent.Subject, OwnerId = testEvent.OwnerId, WhatId = testEvent.WhatId);

        Test.startTest();
        handler.AfterUpdateHandler(testEvent, updatedEvent);
        Test.stopTest();

        travelTimeBefore = [SELECT StartDateTime, EndDateTime FROM Event WHERE Id = :travelTimeBefore.Id];
        System.assertEquals(updatedEvent.StartDateTime.AddHours(-1), travelTimeBefore.StartDateTime);
        System.assertEquals(updatedEvent.StartDateTime, travelTimeBefore.EndDateTime);

        travelTimeAfter = [SELECT StartDateTime, EndDateTime FROM Event WHERE Id = :travelTimeAfter.Id];
        System.assertEquals(updatedEvent.EndDateTime, travelTimeAfter.StartDateTime);
        System.assertEquals(updatedEvent.EndDateTime.AddHours(1), travelTimeAfter.EndDateTime);

    }

    @isTest
    private static void testUpdateTrigger_Success() {
        List<User> users = getUsers();
        User u = users.get(0);

        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);

        Date newStartDate = Date.today().addDays(20);
        Date newEndDate = Date.today().addDays(30);

        evt.StartDateTime = newStartDate;
        evt.EndDateTime = newEndDate;
        evt.OwnerId = u.Id;

        Test.startTest();
        Database.update(evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(newStartDate, incidentInvestigationEvent.StartDateTime__c);
        System.assertEquals(newEndDate, incidentInvestigationEvent.EndDateTime__c);
        System.assertEquals(u.Id, incidentInvestigationEvent.OwnerId);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(newStartDate, intakeInvestigationEvent.StartDateTime__c);
            System.assertEquals(newEndDate, intakeInvestigationEvent.EndDateTime__c);
            System.assertEquals(u.Id, intakeInvestigationEvent.OwnerId);
        }
    }

    @isTest
    private static void DeleteHandler_DeletesFollowingTravelTime() {
        Event testEvent = getIncidentEvents()[0];

        Event travelTimeBefore = new Event(StartDateTime = testEvent.StartDateTime.addHours(-1), EndDateTime = testEvent.StartDateTime, Subject = ScheduleInvestigatorService.SUBJECT_TRAVEL, OwnerId = testEvent.ownerId, WhatId = testEvent.WhatId);
        insert travelTimeBefore;

        Event travelTimeAfter = new Event(StartDateTime = testEvent.EndDateTime, EndDateTime = testEvent.EndDateTime.addHours(1), Subject = ScheduleInvestigatorService.SUBJECT_TRAVEL, OwnerId = testEvent.ownerId, WhatId = testEvent.WhatId);
        insert travelTimeAfter;

        Test.startTest();
        handler.afterDeleteHandler(testEvent);
        Test.stopTest();

        List<Event> travelTimesFound = [SELECT Id FROM Event WHERE Id IN (:travelTimeBefore.Id, :travelTimeAfter.Id)];

        System.assertEquals(0, travelTimesFound.size());
    }

    @isTest
    private static void testUpdateTrigger_DeleteIfFlaggedForCancel() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);
        evt.To_Be_Canceled__c = true;

        Test.startTest();
        Database.update(evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, incidentInvestigationEvent.Investigation_Status__c);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(InvestigationEventTriggerHandler.CANCEL_STATUS, intakeInvestigationEvent.Investigation_Status__c);
        }

        Map<Id, Event> requeriedEvents = new Map<Id, Event>([SELECT Id FROM Event]);
        System.assert(!requeriedEvents.containsKey(evt.Id));
    }

    @isTest
    private static void testUpdateTrigger_DeleteIfFlaggedForReschedule() {
        List<Event> events = getIncidentEvents();
        Event evt = events.get(0);
        evt.To_Be_Rescheduled__c = true;

        Test.startTest();
        Database.update(evt);
        Test.stopTest();

        IncidentInvestigationEvent__c incidentInvestigationEvent = getIncidentInvestigationEventByWhatId(evt.WhatId);

        System.assertEquals(InvestigationEventTriggerHandler.RESCHEDULE_STATUS, incidentInvestigationEvent.Investigation_Status__c);

        List<IntakeInvestigationEvent__c> intakeInvestigationEvents = getIntakeInvestigationEvents();

        for (IntakeInvestigationEvent__c intakeInvestigationEvent: intakeInvestigationEvents) {
            System.assertEquals(InvestigationEventTriggerHandler.RESCHEDULE_STATUS, intakeInvestigationEvent.Investigation_Status__c);
        }

        Map<Id, Event> requeriedEvents = new Map<Id, Event>([SELECT Id FROM Event]);
        System.assert(!requeriedEvents.containsKey(evt.Id));
    }

    @isTest
    private static void testInsert_CopyContractsToBeSigned() {
        User u = getUsers().get(0);

        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = [SELECT Id, Contracts_to_be_Signed__c FROM IncidentInvestigationEvent__c WHERE Contracts_to_be_Signed__c > 0];
        System.assertNotEquals(0, incidentInvestigationEvents.size(), 'Unexpected test data');

        Database.delete([SELECT Id FROM Event WHERE WhatId IN :incidentInvestigationEvents]);

        List<Event> newEvents = TestUtil.createIncidentEvents(u, incidentInvestigationEvents);
        for (Event newEvent : newEvents) {
            newEvent.Contracts_to_be_Signed__c = null;
        }

        Test.startTest();
        Database.insert(newEvents);
        Test.stopTest();

        Map<Id, Event> requeriedEvents = new Map<Id, Event>(getIncidentEvents());
        Map<Id, IncidentInvestigationEvent__c> incidentInvestigationEventsMap = new Map<Id, IncidentInvestigationEvent__c>(incidentInvestigationEvents);

        for (Event ev : newEvents) {
            Event requeriedEvent = requeriedEvents.get(ev.Id);
            IncidentInvestigationEvent__c iie = incidentInvestigationEventsMap.get(ev.WhatId);
            System.assertEquals(iie.Contracts_to_be_Signed__c, requeriedEvent.Contracts_to_be_Signed__c);
        }
    }

    @isTest
    private static void testUpdate_CopyContractsToBeSigned_WhatIdSet() {
        User u = getUsers().get(0);

        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = [SELECT Id, Contracts_to_be_Signed__c FROM IncidentInvestigationEvent__c WHERE Contracts_to_be_Signed__c > 0];
        System.assertNotEquals(0, incidentInvestigationEvents.size(), 'Unexpected test data');

        Database.delete([SELECT Id FROM Event WHERE WhatId IN :incidentInvestigationEvents]);

        List<Event> newEvents = TestUtil.createIncidentEvents(u, incidentInvestigationEvents);
        List<Id> originalWhatIds = new List<Id>();
        for (Event newEvent : newEvents) {
            originalWhatIds.add(newEvent.WhatId);

            newEvent.WhatId = null;
            newEvent.Contracts_to_be_Signed__c = null;
        }
        Database.insert(newEvents);

        Test.startTest();
        List<Event> toUpdateList = new List<Event>();
        for (Integer i = 0; i < newEvents.size(); ++i) {
            Event newEvent = newEvents.get(i);
            Id selectedWhatId = originalWhatIds.get(i);

            Event toUpdate = new Event(Id = newEvent.Id, WhatId = selectedWhatId);
            toUpdateList.add(toUpdate);
        }
        Database.update(toUpdateList);
        Test.stopTest();

        Map<Id, Event> requeriedEvents = new Map<Id, Event>(getIncidentEvents());
        Map<Id, IncidentInvestigationEvent__c> incidentInvestigationEventsMap = new Map<Id, IncidentInvestigationEvent__c>(incidentInvestigationEvents);

        for (Event ev : toUpdateList) {
            Event requeriedEvent = requeriedEvents.get(ev.Id);
            IncidentInvestigationEvent__c iie = incidentInvestigationEventsMap.get(ev.WhatId);
            System.assertEquals(iie.Contracts_to_be_Signed__c, requeriedEvent.Contracts_to_be_Signed__c);
        }
    }

    @isTest
    private static void testUpdate_CopyContractsToBeSigned_WhatIdRemoved() {
        List<IncidentInvestigationEvent__c> incidentInvestigationEvents = [SELECT Id, Contracts_to_be_Signed__c FROM IncidentInvestigationEvent__c WHERE Contracts_to_be_Signed__c > 0];
        List<Event> eventsToUpdate = [SELECT Id, WhatId FROM Event WHERE Contracts_to_be_Signed__c > 0 AND WhatId IN :incidentInvestigationEvents];
        System.assertNotEquals(0, eventsToUpdate.size(), 'Unexpected test data');

        for (Event ev : eventsToUpdate) {
            ev.WhatId = null;
        }

        Test.startTest();
        Database.update(eventsToUpdate);
        Test.stopTest();

        Map<Id, Event> requeriedEvents = new Map<Id, Event>([SELECT Id, Contracts_to_be_Signed__c FROM Event WHERE Id IN :eventsToUpdate]);

        for (Event ev : eventsToUpdate) {
            Event requeriedEvent = requeriedEvents.get(ev.Id);
            System.assertNotEquals(null, requeriedEvent);
            System.assertEquals(0, requeriedEvent.Contracts_to_be_Signed__c);
        }
    }
}