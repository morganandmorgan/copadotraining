public with sharing class mmdice_AccessLogsSelector
    extends mmlib_SObjectSelector
implements mmdice_IAccessLogsSelector
{
	public static mmdice_IAccessLogsSelector newInstance()
	{
		return (mmdice_IAccessLogsSelector) mm_Application.Selector.newInstance(DICE_AccessLog__c.SObjectType);
	}

	private Schema.sObjectType getSObjectType()
	{
		return DICE_AccessLog__c.SObjectType;
	}

	private List<Schema.SObjectField> getAdditionalSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				DICE_AccessLog__c.AccessedDate__c,
				DICE_AccessLog__c.ItemId__c,
				DICE_AccessLog__c.NavigatorId__c
		};
	}

	public List<DICE_AccessLog__c> selectById(Set<Id> idSet)
	{
		return (List<DICE_AccessLog__c>) selectSObjectsById(idSet);
	}

	public String selectForPeriodicDeletionQuery()
	{
	    return
            newQueryFactory()
                .setCondition(DICE_AccessLog__c.CreatedDate + ' < ' + mmlib_Utils.formatSoqlDatetimeUTC(DateTime.now().addHours(getPurgeLookbackHours())))
                .toSOQL();
	}

	public List<DICE_AccessLog__c> selectAll()
	{
        Boolean assertCRUD = false;
        Boolean enforceFLS = false;
        Boolean includeSelectorFields = true;

		return (List<DICE_AccessLog__c>) Database.query(newQueryFactory(assertCRUD, enforceFLS, includeSelectorFields).toSOQL());
	}

    public List<DICE_AccessLog__c> selectAllOrderedByLastModifiedDescending()
	{
        Boolean assertCRUD = false;
        Boolean enforceFLS = false;
        Boolean includeSelectorFields = true;

		return
            (List<DICE_AccessLog__c>)
            Database.query(
                newQueryFactory(assertCRUD, enforceFLS, includeSelectorFields)
                .setCondition(DICE_AccessLog__c.AccessedDate__c + ' >= ' + mmlib_Utils.formatSoqlDatetimeUTC(DateTime.now().addHours(getQueueNameLookbackHours())))
                .addOrdering(new fflib_QueryFactory.Ordering(DICE_AccessLog__c.LastModifiedDate, fflib_QueryFactory.SortOrder.DESCENDING))
                .toSOQL()
            );
	}

    // ================ Feature Settings ==================
    private Boolean settingsAcquired = false;
    private Integer accessLogPurgeLookbackHours = 6;
    private Integer queueNameLookbackHours = 2;

    private void getSettings()
    {
        if (!settingsAcquired)
        {
            Dice_Settings__c settings = Dice_Settings__c.getInstance();

            if (settings != null)
            {
                accessLogPurgeLookbackHours = Integer.valueOf(settings.Lookback_Hours_for_Purging_Access_Log__c);
                queueNameLookbackHours = Integer.valueOf(settings.Lookback_Hours_for_Task_Queue_Name__c);

                if (accessLogPurgeLookbackHours == null) accessLogPurgeLookbackHours = 6;
                if (queueNameLookbackHours == null) queueNameLookbackHours = 2;
            }

            settingsAcquired = true;
        }
    }

    private Integer getQueueNameLookbackHours()
    {
        getSettings();
        return -1 * queueNameLookbackHours;
    }

    private Integer getPurgeLookbackHours()
    {
        getSettings();
        return -1 * accessLogPurgeLookbackHours;
    }
}