/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBSignUp {
    global PBSignUp() {

    }
    @InvocableMethod(label='Sign Up' description='Sign up the given Intake')
    global static void signUp(List<litify_pm.PBSignUp.ProcessBuilderSignUpWrapper> signUpItems) {

    }
global class ProcessBuilderSignUpWrapper {
    @InvocableVariable( required=false)
    global Id record_id;
    global ProcessBuilderSignUpWrapper() {

    }
}
}
