/**
 *  mmintake_IIntakesSelector
 */
public interface mmintake_IIntakesSelector extends mmlib_ISObjectSelector
{
    list<Intake__c> selectById(Set<Id> idSet);
    list<Intake__c> selectByIdWithCallerClientAndInjuredParty(Set<Id> idSet);
    list<Intake__c> selectByIdWithCallerClientInjuredPartyAndAdditionalFields(Set<Id> idSet, list<Schema.FieldSet> intakeFieldSetlist, Schema.FieldSet relatedEventFieldSet);
    list<Intake__c> selectByClientId(Set<Id> idSet);
    list<Intake__c> selectWithFieldsetByClientId(Schema.FieldSet fs, Set<Id> idSet);
    list<Intake__c> selectByGeneratingAttorneyAndCreatedDateRange( set<id> generatingAttorneyIdSet, Date startDate, Date endDate);
    list<Intake__c> selectByUniqueSubmissionKey(Set<String> uniqueSubmissionKeySet);
    list<Intake__c> selectBySubmissionId(Set<String> submissionIdSet);
    list<Intake__c> selectMostRecentMesoReferral();
    list<Intake__c> selectByCiscoCallId(String ciscoCallId);
}