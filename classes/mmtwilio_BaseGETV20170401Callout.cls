/**
 *  mmtwilio_BaseGETV20170401Callout
 */
public abstract class mmtwilio_BaseGETV20170401Callout
    extends mmlib_BaseGETCallout
{
    public mmtwilio_BaseGETV20170401Callout()
    {
        super();

        getHeaderMap().put('Content-Type','application/json');
    }

    protected override string getHost()
    {
        return 'api.twilio.com';
    }

    protected override string getPath()
    {
        return '/2010-04-01' + (string.isNotBlank( getPathSuffix() ) ? getPathSuffix() : '');
    }

    protected abstract string getPathSuffix();

    protected override HttpResponse executeCallout()
    {
        HttpResponse resp = null;

        TwilioIntegrationRelated__c customSetting = TwilioIntegrationRelated__c.getInstance();

        if ( customSetting != null
            && customSetting.IsTwilioIntegrationEnabled__c == true
            && ( ! mmlib_Utils.org.isSandbox
                || (mmlib_Utils.org.isSandbox && customSetting.IsIntegrationsEnabledInSandboxEnv__c == true)
                )
            )
        {
            resp = super.executeCallout();
        }
        else
        {
            resp = new HttpResponse();

            resp.setBody('{}');
            resp.setStatus('Callouts to Twilio are currently disabled.');
            resp.setStatusCode(203);

            system.debug( resp.getStatus() );
        }

        return resp;
    }
}