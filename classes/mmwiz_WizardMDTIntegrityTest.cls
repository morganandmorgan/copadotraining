@isTest
private class mmwiz_WizardMDTIntegrityTest
{
    @isTest
    static void verifyAllTokensAreFound()
    {
        map<string, map<string, set<string>>> verificationResponses = mmwiz_MDTVerificationLogic.verifyAllWizardMDT();

        if ( verificationResponses.isEmpty() )
        {
            system.assert(true);
        }
        else
        {
            string fullMessage = '\n\n\nThe following inconsistencies were found in the Wizard__mdt records. \n';

            for ( string modelKey : verificationResponses.keySet() )
            {
                for ( string missingType : verificationResponses.get( modelKey ).keySet() )
                {
                    for ( string missingToken : verificationResponses.get( modelKey ).get( missingType ) )
                    {
                        fullMessage += '- Model \'' + modelKey + '\' has a ' + missingType + ' reference to ' + missingToken + '\n';
                    }
                }
            }

            fullMessage += '\nPlease correct the above inconsistencies and re-run this test.';

            system.assert( false, fullMessage );
        }
    }
}