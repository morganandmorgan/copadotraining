@isTest
private class mmdocusign_ClientSignupControllerTest {
	@isTest
	static void test() {
		String env = mmlib_Utils.generateGuid();
		String email = 'arya.stark@winterfell.com';
		String name = 'Arya Stark';
		String cuid = mmlib_Utils.generateGuid();
		String url = 
			'https://winterfell.com/brokenTower.html' +
			'?env=' + EncodingUtil.urlEncode(env, 'UTF-8') + 
			'&email=' + EncodingUtil.urlEncode(email, 'UTF-8') +
			'&name=' + EncodingUtil.urlEncode(name, 'UTF-8') +
			'&cuid=' + EncodingUtil.urlEncode(cuid, 'UTF-8');

		Test.setCurrentPage(new PageReference(url));

		mmdocusign_ClientSignupController.runAsTest = true;

		mmdocusign_ClientSignupController cont = new mmdocusign_ClientSignupController();
		cont.execute();

		Map<String, String> testResult = mmdocusign_ClientSignupController.test_urlParameterMap;

		System.assertEquals(env, testResult.get('env'));
		System.assertEquals(email, testResult.get('email'));
		System.assertEquals(name, testResult.get('name'));
		System.assertEquals(cuid, testResult.get('cuid'));
	} //test

	@isTest
	static void test2() {

		mmdocusign_ClientSignupController.runAsTest = true;

		mmdocusign_ClientSignupController cont = new mmdocusign_ClientSignupController();

		cont.execute(); //missing paramteres test coverage

		String url = 
			'https://winterfell.com/brokenTower.html' +
			'?mid=1';

		Test.setCurrentPage(new PageReference(url));

		cont.execute(); //message display test coverage

		String text = cont.getDisplayText(); //let's get one more line covered

	} //test2
}