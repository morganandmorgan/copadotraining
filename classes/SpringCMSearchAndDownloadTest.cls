@IsTest
public class SpringCMSearchAndDownloadTest {

    private static Account account;
    private static litify_pm__Matter__c matter;
    private static MM_Document__c mmDocument;

    static {
        account = TestUtil.createAccount('Unit Test Account');
        insert account;

        matter = TestUtil.createMatter(account);
        insert matter;

        mmDocument = new MM_Document__c();
        insert mmDocument;
    }

    static testMethod void testSearch() {
        new SpringAdvancedSearch().SpringAdvancedSearch(matter.Id);
    }

    static testMethod void testDownload() {
        new SpringCMDownloadHandler().SpringCMDownloadHandler(mmDocument.Id);
    }
}