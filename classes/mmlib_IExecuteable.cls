/**
 *  mmlib_IExecuteable
 */
public interface mmlib_IExecuteable
{
    void execute();
}