/**
 * FeeGoalAllocation_Domain
 * @description Domain class for Fee Goal Allocation SObject.
 * @author Jeff Watson
 * @date 2/13/2019
 */

public with sharing class FeeGoalAllocation_Domain extends fflib_SObjectDomain {

    private List<Fee_Goal_Allocation__c> feeGoalAllocations;
    private FeeGoal_Service feeGoalService;

    // Ctors
    public FeeGoalAllocation_Domain() {
        super();
        this.feeGoalAllocations = new List<Fee_Goal_Allocation__c>();
        this.feeGoalService = new FeeGoal_Service();
    }

    public FeeGoalAllocation_Domain(List<Fee_Goal_Allocation__c> feeGoalAllocations) {
        super(feeGoalAllocations);
        this.feeGoalAllocations = (List<Fee_Goal_Allocation__c>) records;
        this.feeGoalService = new FeeGoal_Service();
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new FeeGoalAllocation_Domain(records);
        }
    }

    // Trigger Handlers
    public override void onAfterInsert() {
        this.updateFeeGoalCount(feeGoalAllocations);
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        this.updateFeeGoalCount(feeGoalAllocations);
    }

    public override void onAfterDelete() {
        this.updateFeeGoalCount(feeGoalAllocations);
    }

    // Private Methods
    private void updateFeeGoalCount(List<Fee_Goal_Allocation__c> feeGoalAllocations) {
        feeGoalService.updateFeeGoalWithMatterCount(feeGoalAllocations);
    }
}