@isTest
public  class IntakeConvertFlowQueueableTest {

    @isTest
    public static void mainTest() {

        Intake__c intake = TestUtil.createIntake();
        insert intake;

        Set<Id> intakeIds = new Set<Id>();
        intakeIds.add(intake.id);

        IntakeConvertFlowQueueable icfq = new IntakeConvertFlowQueueable(intakeIds,null);

        icfq.execute(null);

    } //mainTest

}//class