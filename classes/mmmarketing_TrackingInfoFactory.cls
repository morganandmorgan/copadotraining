/**
 *  mmmarketing_TrackingInfoFactory
 */
public class mmmarketing_TrackingInfoFactory
    implements mmmarketing_TrackingInfoFactory.ITrackingInfoFactory
{
    public enum TRACKING_INFO_RECORD_TYPE_DEVELOPERS_NAMES
    {
        CALL,
        DIGITAL
    }

    public enum SOURCES
    {
        ADWORDS, FACEBOOK, GOOGLE
    }

    public enum MEDIUMS
    {
        PPC, CPC
    }

    public interface ITrackingInfoFactory
    {
        Marketing_Tracking_Info__c generateNewFromHubSpotInfo( Intake__c newIntake, Map<String, String> hubSpotInfo );
        Marketing_Tracking_Info__c generateNewCall( Intake__c newIntake, String phoneNumber, Datetime phoneCallStartTime );
        ITrackingInfoFactory setNewRecordInstance( Marketing_Tracking_Info__c newRecordInstance);
        void prepareTrackingDetailInfoRecord(String detailTrackingParameter, String detailTrackingValue);
        mmmarketing_TrackingInfoFactory with(Map<SObjectField, String> fieldParameterMap);
        mmmarketing_TrackingInfoFactory setIntake(Intake__c intake);
        Marketing_Tracking_Info__c generate();
    }

    private static final string GCLID_VALUE_API_NAME = Marketing_Tracking_Info__c.GCLID_Value__c.getdescribe().getName();

    // TODO: Change this map to a Custom Metadata Type that can be configured.
    private static final map<String, String> landingPageParameterToMTIFieldMap;

/*
    utm_campaign => Campaign_Value__c
    utm_medium => Medium_Value__c
    utm_term => Term_Value__c
    Domain Name => Domain_Value__c
    Web Page => Page_Value__c
    utm_source => Source_Value__c
    utm_content => <not specified>
    ads_adid "<number>"                               "
    ads_matchtype "p", "e", "b" ???? "phrase", "
    ads_network  "g" "e" "p" => Source_Value__c ???  "add was only displayed on google network"
    ads_creative "<number>"
    ads_keyword "<number>" => Term_Value__c id number
    keyword "<number>" => Term_Value__c    id number
    gclid "<number>" => GCLID_Value__c ??? encrypted way for google to pass everything --- NEED A NEW FIELD

    https://www.forthepeople.com/?ads_cmpid=389431803&ads_adid=25466484603&ads_matchtype=p&ads_network=g&ads_creative=146898164065&utm_term=morgan%20and%20morgan&ads_targetid=kwd-810415765&utm_campaign=&utm_source=adwords&utm_medium=ppc&ttv=2&gclid=CMKSy9re588CFYxahgodVygC8g
*/
    static
    {
        landingPageParameterToMTIFieldMap = new map<String, String>();

        landingPageParameterToMTIFieldMap.put( 'utm_campaign', 'Campaign_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'ads_cmpid', 'Campaign_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'campaign', 'Campaign_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'utm_medium', 'Medium_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'utm_term', 'Term_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'ads_keyword', 'Term_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'keyword', 'Term_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'utm_source', 'Source_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'gclid', 'GCLID_Value__c' );
        landingPageParameterToMTIFieldMap.put( 'ads_adsetid', 'Ad_Set_ID__c' );
        landingPageParameterToMTIFieldMap.put( 'ads_adsid', 'Ad_ID__c' );
    }

    private Map<String, String> hubSpotFieldsMap = new Map<String,String>();
    private mmlib_ISObjectUnitOfWork uow = null;

    private Map<String, Schema.SObjectField> mtiSObjectFieldMap = Marketing_Tracking_Info__c.sObjectType.getDescribe().fields.getMap();

    private static mmmarketing_TrackingInfoFactory.ITrackingInfoFactory marketingTrackingInfoFactoryInstance = null;

    private mmmarketing_TrackingInfoFactory()
    {
        this(null);
    }

    private mmmarketing_TrackingInfoFactory( mmlib_ISObjectUnitOfWork uow )
    {
        this.uow = uow;
    }

    public static mmmarketing_TrackingInfoFactory.ITrackingInfoFactory getInstance()
    {
        return getInstance_Impl(null);
    }

    public static mmmarketing_TrackingInfoFactory.ITrackingInfoFactory getInstance(fflib_ISObjectUnitOfWork uow)
    {
        return getInstance_Impl(uow);
    }

    private static mmmarketing_TrackingInfoFactory.ITrackingInfoFactory getInstance_Impl(fflib_ISObjectUnitOfWork uow)
    {
        if (marketingTrackingInfoFactoryInstance == null)
        {
            marketingTrackingInfoFactoryInstance = new mmmarketing_TrackingInfoFactory((mmlib_ISObjectUnitOfWork) uow);
        }

        return marketingTrackingInfoFactoryInstance;
    }

    private void primeHubspotFieldsMap()
    {
        if (hubSpotFieldsMap.isEmpty())
        {
            for (Hubspot_Field_Mapping__c entry : Hubspot_Field_Mapping__c.getAll().values())
            {
                hubSpotFieldsMap.put(entry.hubspot_field_label__c, entry.intake_field_api_name__c);
            }
        }
    }

    private Marketing_Tracking_Info__c newRecord = null;

    /*
     *  Accessor to the newRecord instance.  Allowing the methods to
     *  access the variable via this method allows a lazy instantition
     *  of the member variable should there actually be a parameter
     *  that can be used by the Marketing_Tracking_Info__c record.
     */
    private Marketing_Tracking_Info__c getNewRecordInstance()
    {
        if ( this.newRecord == null )
        {
            this.newRecord = new Marketing_Tracking_Info__c();
            detailInformationByParamMap.clear();
            detailInformationParamsProcessedSet.clear();
        }

        return this.newRecord;
    }

    private void resetNewRecordInstance()
    {
        this.newRecord = null;
        detailInformationByParamMap.clear();
        detailInformationParamsProcessedSet.clear();
    }

//    private void processCustomFieldDataFromHubSpot( Map<String, String> hubSpotInfo )
//    {
//        primeHubspotFieldsMap();
//
//        String relationshipName = mmlib_Utils.findChildRelationshipName( Intake__c.SObjectType, Marketing_Tracking_Info__c.SObjectType, Marketing_Tracking_Info__c.Intake__c );
//        String hubSpotFieldValue = null;
//        String trackingInfoFieldName = null;
//        Schema.SoapType typ = null;
//        Object objValue = null;
//
//        for (String fieldName : hubSpotInfo.keySet())
//        {
//            String fieldValue = hubSpotInfo.get(fieldName);
//
//            string message = '\n\n\n'
//                + 'fieldName : '+ fieldName + '\n'
//                + 'fieldValue : ' + fieldValue + '\n'
//                + 'hubSpotFieldsMap.containskey( fieldName ) : ' + hubSpotFieldsMap.containskey( fieldName ) + '\n';
//
//            if (hubSpotFieldsMap.containskey( fieldName ))
//            {
//                message += 'hubSpotFieldsMap.get( fieldName ) : ' + hubSpotFieldsMap.get( fieldName ) + '\n';
//                message += 'mtiSObjectFieldMap.containskey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) : ' + mtiSObjectFieldMap.containskey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) + '\n';
//
//                typ = null;
//                objValue = null;
//
//                hubSpotFieldValue = hubSpotFieldsMap.get( fieldName );
//
//                if ( relationshipName.equalsIgnoreCase(hubSpotFieldValue) )
//                {
//                    // process the full landing page url
//                    parseAndProcessAllFieldsOfLandingPageURL( fieldValue );
//                }
//                else if ( hubSpotFieldValue.startsWith( relationshipName ) )
//                {
//                    // process the single field
//
//                    trackingInfoFieldName = hubSpotFieldValue.removeStartIgnoreCase( relationshipName + '.' );
//
//                    if ( mtiSObjectFieldMap.containskey( trackingInfoFieldName.toLowerCase() ) )
//                    {
//                        typ = mtiSObjectFieldMap.get( trackingInfoFieldName.toLowerCase()  ).getDescribe().getSoapType();
//
//                        objValue = mmlib_Utils.convertValToType(typ, fieldValue);
//
//                        getNewRecordInstance().put( trackingInfoFieldName, objValue );
//                    }
//                }
//            }
//            else
//            {
//                message += 'hubSpotFieldsMap.get( fieldName ) : n/a \n';
//                message += 'mtiSObjectFieldMap.containskey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) : n/a \n';
//            }
//
//            message += '\n\n\n';
//            system.debug( message );
//        }
//    }

    private Map<SObjectField, String> convertCustomFieldDataFromHubSpotToSOjbectFieldParameterMap( Map<String, String> hubSpotInfo )
    {
        Map<SObjectField, String> fieldParameterMap = new Map<SObjectField, String>();

        primeHubspotFieldsMap();

        String relationshipName = mmlib_Utils.findChildRelationshipName( Intake__c.SObjectType, Marketing_Tracking_Info__c.SObjectType, Marketing_Tracking_Info__c.Intake__c );
        String hubSpotFieldValue = null;
        String trackingInfoFieldName = null;
        Schema.SoapType typ = null;
        Object objValue = null;

        for (String fieldName : hubSpotInfo.keySet())
        {
            String fieldValue = hubSpotInfo.get(fieldName);

            string message = '\n\n\n'
                + 'fieldName : '+ fieldName + '\n'
                + 'fieldValue : ' + fieldValue + '\n'
                + 'hubSpotFieldsMap.containskey( fieldName ) : ' + hubSpotFieldsMap.containskey( fieldName ) + '\n';

            if (hubSpotFieldsMap.containskey( fieldName ))
            {
                message += 'hubSpotFieldsMap.get( fieldName ) : ' + hubSpotFieldsMap.get( fieldName ) + '\n';
                message += 'mtiSObjectFieldMap.containskey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) : ' + mtiSObjectFieldMap.containskey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) + '\n';

                typ = null;
                objValue = null;

                hubSpotFieldValue = hubSpotFieldsMap.get( fieldName );

                if ( relationshipName.equalsIgnoreCase(hubSpotFieldValue) )
                {
                    // process the full landing page url
                    // parseAndProcessAllFieldsOfLandingPageURL( fieldValue );
                }
                else if ( hubSpotFieldValue.startsWith( relationshipName ) )
                {
                    // process the single field

                    trackingInfoFieldName = hubSpotFieldValue.removeStartIgnoreCase( relationshipName + '.' );

                    if ( mtiSObjectFieldMap.containskey( trackingInfoFieldName.toLowerCase() ) )
                    {
                        fieldParameterMap.put( mtiSObjectFieldMap.get( trackingInfoFieldName.toLowerCase() ), fieldValue);


//                        typ = mtiSObjectFieldMap.get( trackingInfoFieldName.toLowerCase()  ).getDescribe().getSoapType();
//
//                        objValue = mmlib_Utils.convertValToType(typ, fieldValue);
//
//                        getNewRecordInstance().put( trackingInfoFieldName, objValue );
                    }
                }
            }
            else
            {
                message += 'hubSpotFieldsMap.get( fieldName ) : n/a \n';
                message += 'mtiSObjectFieldMap.containskey( hubSpotFieldsMap.get( fieldName ).tolowercase() ) : n/a \n';
            }

            message += '\n\n\n';
            system.debug( message );
        }

        return fieldParameterMap;
    }

    private void parseAndProcessAllFieldsOfLandingPageURL(String landingPageUrl)
    {
        if ( String.isNotBlank( landingPageUrl ) )
        {
            getNewRecordInstance().Raw_Landing_Page_URL__c = landingPageUrl;
        }

        try
        {
            URL theUrl = new URL( landingPageUrl );

            String urlProtocol = theUrl.getProtocol();
            String urlHost = theUrl.getHost();
            String urlPath = theUrl.getPath();
            Integer urlPort = theUrl.getPort();
            String urlRef = theUrl.getRef();
            String urlQuery = theUrl.getQuery();

            //system.debug( '                                       getProtocol() ::: ' + urlProtocol );
            //system.debug( '                                       getHost() ::: ' + urlHost );
            //system.debug( '                                       getPath() ::: ' + urlPath );
            //system.debug( '                                       getPort() ::: ' + urlPort );
            //system.debug( '                                       getRef() ::: ' + urlRef );
            //system.debug( '                                       getQuery() ::: ' + urlQuery );

            getNewRecordInstance().Domain_Value__c = urlHost;
            getNewRecordInstance().Page_Value__c = urlPath;

            String queryDelimiter = '&';
            String queryParamDelimiter = '=';

            List<string> queryParamsList = urlQuery.split(queryDelimiter);
            //system.debug( queryParamsList.size() );

            map<string, string> queryParamMap = new map<string, string>();
            Schema.SoapType typ = null;
            Object objValue = null;

            for ( String queryParam : queryParamsList )
            {
                system.debug( queryParam );
                queryParamMap.put( queryParam.substringBefore( queryParamDelimiter ), queryParam.substringAfter( queryParamDelimiter ) );
            }

            // system.debug( queryParamMap );
            String MTIFieldAPIName = null;

            boolean isGCLIDPresent = false;

            for ( String queryParamKey : queryParamMap.keyset() )
            {
                // Is the queryParamKey part of the mapping to a known Marketing_Tracking_Info__c field API name??
                if ( landingPageParameterToMTIFieldMap.containsKey( queryParamKey ) )
                {
                    MTIFieldAPIName = landingPageParameterToMTIFieldMap.get( queryParamKey ).toLowerCase();

                    // Is the field API Name found in the list actually on the Marketing_Tracking_Information__c object??
                    if ( mtiSObjectFieldMap.containskey( MTIFieldAPIName ) )
                    {
                        // only assign the field value if there is something there to assign.  in other words, don't save NULLs
                        if ( String.isNotBlank( queryParamMap.get( queryParamKey ) ) )
                        {
                            getNewRecordInstance().put( MTIFieldAPIName, decodeString( queryParamMap.get( queryParamKey ) ) );

                            if ( GCLID_VALUE_API_NAME.equalsIgnoreCase( MTIFieldAPIName ) )
                            {
                                isGCLIDPresent = true;
                            }
                        }
                    }
                    else
                    //.... then push the value down to the detail object for now.
                    {
                        prepareTrackingDetailInfoRecord( queryParamKey, queryParamMap.get( queryParamKey ));
                    }
                }
                else
                // ...the queryParamKey was not part of the known Marketing_Tracking_Info__c field API names and
                //      thus we need to create an associated Marketing_Tracking_Detail_Information__c record instead
                {
                    prepareTrackingDetailInfoRecord( queryParamKey, queryParamMap.get( queryParamKey ));
                }
            }

            // In this circumstance, you can default the source to Google
            if ( isGCLIDPresent )
            {
                if (String.isBlank( getNewRecordInstance().Source_Value__c ))
                {
                    getNewRecordInstance().Source_Value__c = SOURCES.GOOGLE.name().toLowerCase();
                }

                if (String.isBlank( getNewRecordInstance().Medium_Value__c ))
                {
                    getNewRecordInstance().Medium_Value__c = MEDIUMS.CPC.name().toLowerCase();
                }

            }
        }
        catch (Exception e)
        {
            system.debug( LoggingLevel.WARN, 'Unable to parse landing page url supplied : \n\n' + e);
        }
    }

    public mmmarketing_TrackingInfoFactory.ITrackingInfoFactory setNewRecordInstance( Marketing_Tracking_Info__c newRecordInstance)
    {
        this.newRecord = newRecordInstance;

        detailInformationByParamMap.clear();
        detailInformationParamsProcessedSet.clear();

        for (Marketing_Tracking_Detail_Information__c detailInformationRecord : newRecordInstance.Marketing_Tracking_Detail_Information__r)
        {
            detailInformationByParamMap.put( detailInformationRecord.Detail_Tracking_Parameter__c, detailInformationRecord );
        }

        return this;
    }

    private map<string, Marketing_Tracking_Detail_Information__c> detailInformationByParamMap = new map<string, Marketing_Tracking_Detail_Information__c>();
    private set<string> detailInformationParamsProcessedSet = new Set<string>();

    public void prepareTrackingDetailInfoRecord(String detailTrackingParameter, String detailTrackingValue)
    {
        if ( string.isNotBlank( detailTrackingParameter )
            && string.isNotBlank( detailTrackingValue )
            && ! detailInformationParamsProcessedSet.contains( detailTrackingParameter )
            )
        {
            Marketing_Tracking_Detail_Information__c trackingDetailInfoRecord = null;

            if ( detailInformationByParamMap.containsKey( detailTrackingParameter ) )
            {
                trackingDetailInfoRecord = detailInformationByParamMap.get( detailTrackingParameter );

                trackingDetailInfoRecord.Detail_Tracking_Value__c = decodeString( detailTrackingValue );

                this.uow.register( trackingDetailInfoRecord );
            }
            else
            {
                trackingDetailInfoRecord = new Marketing_Tracking_Detail_Information__c();

                trackingDetailInfoRecord.Detail_Tracking_Parameter__c = detailTrackingParameter;
                trackingDetailInfoRecord.Detail_Tracking_Value__c = decodeString( detailTrackingValue );

                this.uow.register( trackingDetailInfoRecord, Marketing_Tracking_Detail_Information__c.Marketing_Tracking_Information__c, getNewRecordInstance());
            }

            detailInformationParamsProcessedSet.add( detailTrackingParameter );
        }
    }

    private String decodeString( String input )
    {
        return String.isNotBlank(input)
                ? input.replace('%20',' ').replace('%2B','+').replace('%26','&')
                : input;
    }

    // ==================== Chained Method Pattern ===================================================================

    private Marketing_Tracking_Info__c record = null;

    private Intake__c intake = null;

    private Map<SObjectField, String> fieldParameterMap = new Map<SObjectField, String>();

    public mmmarketing_TrackingInfoFactory with(Map<SObjectField, String> fieldParameterMap)
    {
        this.fieldParameterMap.putAll( fieldParameterMap );
        return this;
    }

    public mmmarketing_TrackingInfoFactory setIntake(Intake__c intake)
    {
        this.intake = intake;
        return this;
    }

    public Marketing_Tracking_Info__c generateNewFromHubSpotInfo( Intake__c newIntake, Map<String, String> hubSpotInfo )
    {
        this.newRecord = null;

        Map<SObjectField, String> fieldParameterMap = new Map<SObjectField, String>();

        fieldParameterMap.putAll( provisionDigitalSpecificFields() );

        fieldParameterMap.putAll( convertCustomFieldDataFromHubSpotToSOjbectFieldParameterMap( hubSpotInfo ) );

        this.with( fieldParameterMap );

        this.setIntake( newIntake );

        return this.generate();
    }

    public Marketing_Tracking_Info__c generateNewCall( Intake__c newIntake, String phoneNumber, Datetime phoneCallStartTime )
    {
        resetNewRecordInstance();

        Map<SObjectField, String> fieldParameterMap = new Map<SObjectField, String>();

        fieldParameterMap.putAll( provisionCallSpecificFields( phoneNumber, phoneCallStartTime ) );

        this.with( fieldParameterMap );

        this.setIntake( newIntake );

        return this.generate();
    }

    public Map<SObjectField, String> provisionCallSpecificFields( String phoneNumber, Datetime phoneCallStartTime )
    {
        Map<SObjectField, String> fieldParameterMap = new Map<SObjectField, String>();

        fieldParameterMap.put( Marketing_Tracking_Info__c.RecordTypeId, mmlib_RecordTypeUtils.getRecordTypeIDByDevName( Marketing_Tracking_Info__c.SObjectType, TRACKING_INFO_RECORD_TYPE_DEVELOPERS_NAMES.CALL.name() ) );
        if ( string.isNotBlank( phoneNumber ) )
        {
            fieldParameterMap.put( Marketing_Tracking_Info__c.Calling_Phone_Number__c, phoneNumber );
        }

        if ( phoneCallStartTime != null )
        {
            // TODO: Fix this so that the addition of the phoneCallStartTime value occurs in the same manner as the rest of the fields
            //  For now, I have left it as a direct value set to the Tracking_Event_Timestamp__c because I am uncertain what happens if the Datetime value is saved to a string. hubSpotFieldsMap
            //  I will have to figure this out later.
            //fieldParameterMap.put( Marketing_Tracking_Info__c.Tracking_Event_Timestamp__c, phoneCallStartTime );
            getNewRecordInstance().Tracking_Event_Timestamp__c = phoneCallStartTime;
        }

        return fieldParameterMap;
    }

    public Map<SObjectField, String> provisionDigitalSpecificFields()
    {
        Map<SObjectField, String> fieldParameterMap = new Map<SObjectField, String>();

        fieldParameterMap.put( Marketing_Tracking_Info__c.RecordTypeId, mmlib_RecordTypeUtils.getRecordTypeIDByDevName( Marketing_Tracking_Info__c.SObjectType, TRACKING_INFO_RECORD_TYPE_DEVELOPERS_NAMES.DIGITAL.name() ) );
        fieldParameterMap.put( Marketing_Tracking_Info__c.Ad_Type__c, 'SiteClick');

        return fieldParameterMap;
    }

    public Marketing_Tracking_Info__c generate()
    {
        if (this.uow == null )
        {
            throw new FactoryException('Factory\'s uow was not populated.');
        }

        if ( fieldParameterMap.containsKey( Marketing_Tracking_Info__c.RecordTypeId ) )
        {
            string recordTypeFieldValue = fieldParameterMap.get( Marketing_Tracking_Info__c.RecordTypeId );

            //if the record type is digital, then
            if ( string.isNotBlank(recordTypeFieldValue)
                &&  recordTypeFieldValue.equalsIgnoreCase( string.valueOf( mmlib_RecordTypeUtils.getRecordTypeIDByDevName( Marketing_Tracking_Info__c.SObjectType, TRACKING_INFO_RECORD_TYPE_DEVELOPERS_NAMES.DIGITAL.name() ) ) )
                )
            {
                fieldParameterMap.putAll( provisionDigitalSpecificFields() );
            }
            else if ( string.isNotBlank(recordTypeFieldValue)
                &&  recordTypeFieldValue.equalsIgnoreCase( string.valueOf( mmlib_RecordTypeUtils.getRecordTypeIDByDevName( Marketing_Tracking_Info__c.SObjectType, TRACKING_INFO_RECORD_TYPE_DEVELOPERS_NAMES.CALL.name() ) ) )
                )
            {
                // TODO: figure this out better.  This is a stop gap for now.
                fieldParameterMap.putAll( provisionCallSpecificFields( null, null ) );
            }
            else
            {
                //the RecordTypeId was provided but it is blank or does not match the above
                // default to digital record type if none is provided.
                fieldParameterMap.putAll( provisionDigitalSpecificFields() );
            }
        }
        else
        {
            // default to digital record type if none is provided.
            fieldParameterMap.putAll( provisionDigitalSpecificFields() );
        }

        string rawLandingPageUrl = null;

        for ( Schema.SObjectField field : fieldParameterMap.keyset() )
        {
            // capture the Raw_Landing_Page_URL__c field value, if provided, for later processing.
            if ( Marketing_Tracking_Info__c.Raw_Landing_Page_URL__c == field )
            {
                rawLandingPageUrl = fieldParameterMap.get( field );
            }

            try
            {
                getNewRecordInstance().put( field, mmlib_Utils.convertValToDisplayType( field.getDescribe().getType(), fieldParameterMap.get(field)));
            }
            catch ( System.SObjectException soe )
            {
                // Provided field map may have SObjectFields for different SObjects.
                // SFDC does not offer a method for determing a SObjectField's SObject.
                // Exception-based programming is the only alternative.
            }
        }

        if ( string.isNotBlank( rawLandingPageUrl ))
        {
            parseAndProcessAllFieldsOfLandingPageURL( rawLandingPageUrl );
        }

        if (intake != null)
        {
            uow.registerRelationship( getNewRecordInstance(), Marketing_Tracking_Info__c.Intake__c, this.intake );
        }

        uow.registerNew( getNewRecordInstance() );

        return getNewRecordInstance();
    }

    public class FactoryException
        extends Exception
    {
        // Intentionally blank.
    }
}