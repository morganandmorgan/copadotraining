public class mmlitifylrn_ReferralsServiceImpl
        implements mmlitifylrn_IReferralsService
{
    private Datetime lastSyncOverride = null;

    public void referToLitify(Set<id> referralIdSet)
    {
        mmlitifylrn_Referrals.newInstance(referralIdSet).postThirdPartyReferralToLitifyInQueueableMode();
    }

    public void syncFromLitify(Datetime lastSync)
    {
        this.lastSyncOverride = lastSync;
        this.syncFromLitify();
    }

    public void syncFromLitify()
    {
        List<mmlitifylrn_ThirdPartyCredential__c> allCredentials = mmlitifylrn_CredentialsSelector.newInstance().selectAll();

        mmlitifylrn_ReferralsSyncQueueable referralsSyncQueueable = null;
        for (mmlitifylrn_ThirdPartyCredential__c credential : allCredentials)
        {
            referralsSyncQueueable = new mmlitifylrn_ReferralsSyncQueueable(credential);
            if(this.lastSyncOverride != null)
            {
                referralsSyncQueueable.setLastSyncOverride(this.lastSyncOverride);
            }

            System.enqueueJob(referralsSyncQueueable);

        }
    }
}