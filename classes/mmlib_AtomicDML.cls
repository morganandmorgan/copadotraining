/**
 *  mmlib_AtomicDML
 */
public class mmlib_AtomicDML
    implements mmlib_IAtomicDmlWorkUnitGettable
{
    public void dmlInsert(List<SObject> objList)
    {
        List<Database.SaveResult> resultList = database.insert( objList, false );

        // Iterate through each returned result
        for (Integer idx = 0; idx < resultList.size(); idx++)
        {
            Database.SaveResult sr = resultList.get(idx);
            SObject so = objList.get(idx);

            workUnitList.add(new mmlib_AtomicDmlWorkUnit(so, sr));

            if ( ! sr.isSuccess() )
            {
                // Operation failed, so get all errors
                for (Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(objList.getSobjectType() + ' fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    public void dmlUpdate(List<SObject> objList)
    {
        list<Database.SaveResult> resultList = database.update( objList, false );

        // Iterate through each returned result
        for (Integer idx = 0; idx < resultList.size(); idx++)
        {
            Database.SaveResult sr = resultList.get(idx);
            SObject so = objList.get(idx);

            workUnitList.add(new mmlib_AtomicDmlWorkUnit(so, sr));

            if ( ! sr.isSuccess() )
            {
                // Operation failed, so get all errors
                for (Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(objList.getSobjectType() + ' fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    public void dmlDelete(List<SObject> objList)
    {
        list<Database.DeleteResult> resultList = database.delete( objList, false );

        // Iterate through each returned result
        for (Integer idx = 0; idx < resultList.size(); idx++)
        {
            Database.DeleteResult sr = resultList.get(idx);
            SObject so = objList.get(idx);

            workUnitList.add(new mmlib_AtomicDmlWorkUnit(so, sr));

            if ( ! sr.isSuccess() )
            {
                // Operation failed, so get all errors
                for (Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(objList.getSobjectType() + ' fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    private List<mmlib_AtomicDmlWorkUnit> workUnitList = new List<mmlib_AtomicDmlWorkUnit>();
    public List<mmlib_AtomicDmlWorkUnit> getWorkUnitList()
    {
        return workUnitList;
    }
}