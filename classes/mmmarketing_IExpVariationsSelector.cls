/**
 *  mmmarketing_IExpVariationsSelector
 */
public interface mmmarketing_IExpVariationsSelector extends mmlib_ISObjectSelector
{
    List<Marketing_Exp_Variation__c> selectById(Set<Id> idSet);
}