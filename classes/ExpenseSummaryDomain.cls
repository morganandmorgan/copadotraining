/**
 * ExpenseSummaryDomain
 * @description Domain class for Expense_Summary__c.
 * @author Jeff Watson
 * @date 1/18/2019
 */

public with sharing class ExpenseSummaryDomain extends fflib_SObjectDomain {

    // Ctor
    public ExpenseSummaryDomain(List<Expense_Summary__c> records) {
        super(records);
    }

    // Trigger Handlers

    // Static Methods
    // Map<Id,Set<Id>> expenseTypesForMatterIds = Map<matterId, Set<litify_pm_Expense_Type__c.id>>
    public static void createExpenseSummaries(Map<Id,Set<Id>> expenseTypesForMatterIds, fflib_SObjectUnitOfWork uow) {

        if(expenseTypesForMatterIds.size() == 0) return;

        List<litify_pm__Expense__c> allExpenses = [SELECT Id, litify_pm__Matter__c, litify_pm__Matter__r.RecordType.Name, litify_pm__ExpenseType2__r.Name, CostType__c, Amount_Remaining_Client__c, Amount_Remaining_to_Recover__c, Amount_Recovered__c FROM litify_pm__Expense__c WHERE litify_pm__Matter__c IN :expenseTypesForMatterIds.keySet() AND Don_t_Recover_This_Cost__c = false AND litify_pm__ExpenseType2__r.Name != 'Soft Cost Recovered' AND litify_pm__ExpenseType2__r.Name != 'Hard Cost Recovered' AND (NOT litify_pm__ExpenseType2__r.Name LIKE '%Write Offs%')];
        if(allExpenses == null || allExpenses.isEmpty()) return;

        Map<String, litify_pm__Expense_Type__c> expenseTypeMapByName = new Map<String, litify_pm__Expense_Type__c>();
        for(litify_pm__Expense_Type__c expenseType : [select Id, Name from litify_pm__Expense_Type__c]) {
            expenseTypeMapByName.put(expenseType.Name, expenseType);
        }

        //Map <matterId, <expsenseTypeId, expense_summary__c>>
        //insteady delete/re-create, lets build a map of the existing expense summaries
        Map<Id,Map<Id,Expense_Summary__c>> expenseSummaryMap = new Map<Id,Map<Id,Expense_Summary__c>>();
        List<Expense_Summary__c> expenseSummaries = [select Id, Matter__c, Expense_Type__c from Expense_Summary__c where Matter__c in :expenseTypesForMatterIds.keySet()];
        for (Expense_Summary__c expenseSummary : expenseSummaries) {
            //uow.registerDeleted(expenseSummary);
            //are we doing all types, or does this expense belong to an affected type?
            if (expenseTypesForMatterIds.get(expenseSummary.matter__c) == null || expenseTypesForMatterIds.get(expenseSummary.matter__c).contains(expenseSummary.expense_type__c)) {
                if ( !expenseSummaryMap.containsKey(expenseSummary.matter__c) ) {
                        expenseSummaryMap.put(expenseSummary.matter__c, new Map<Id,Expense_Summary__c>() );
                }
                expenseSummaryMap.get(expenseSummary.matter__c).put(expenseSummary.expense_type__c, expenseSummary);
            }
        }

        for (Id matterId : expenseTypesForMatterIds.keySet()) {
            List<litify_pm__Expense__c> expensesForThisMatter = new List<litify_pm__Expense__c>();
            for(litify_pm__Expense__c expense : allExpenses) {
                //does this expense belong to the matter we are working on
                if (expense.litify_pm__Matter__c == matterId) {
                    //are we doing all types, or does this expense belong to an affected type?
                    if (expenseTypesForMatterIds.get(matterId) == null || expenseTypesForMatterIds.get(matterId).contains(expense.litify_pm__expenseType2__c)) {
                        expensesForThisMatter.add(expense);
                    }
                }
            }
            //for (Expense_Summary__c es : computeExpenseSummariesByMatter(matterId, expensesForThisMatter, expenseTypeMapByName)) {
            //    uow.registerNew(es);
            //}
            updateExpenseSummariesByMatter(matterId, expensesForThisMatter, expenseTypeMapByName, expenseSummaryMap.get(matterId), uow);
        }
    } //createExpenseSummaries

    private static void updateExpenseSummariesByMatter(Id matterId, List<litify_pm__Expense__c> expenses, Map<String, litify_pm__Expense_Type__c> expenseTypeMapByName, Map<Id, Expense_Summary__c> expenseSummaries, fflib_SObjectUnitOfWork uow) {

        Map<String, Double> expensesMapByType = new Map<String, Double>();
        for (litify_pm__Expense__c expense : expenses) {
            if(expense.CostType__c != null) {
                if (expensesMapByType.get(expense.litify_pm__ExpenseType2__r.Name) == null) {
                    if (expense.litify_pm__Matter__r.RecordType.Name == 'Social Security') {
                        expensesMapByType.put(expense.litify_pm__ExpenseType2__r.Name, expense.Amount_Remaining_Client__c);
                    } else {
                        expensesMapByType.put(expense.litify_pm__ExpenseType2__r.Name, expense.Amount_Remaining_to_Recover__c);
                    }
                } else {
                    if (expense.litify_pm__Matter__r.RecordType.Name == 'Social Security') {
                        expensesMapByType.put(expense.litify_pm__ExpenseType2__r.Name, expensesMapByType.get(expense.litify_pm__ExpenseType2__r.Name) + expense.Amount_Remaining_Client__c);
                    } else {
                        expensesMapByType.put(expense.litify_pm__ExpenseType2__r.Name, expensesMapByType.get(expense.litify_pm__ExpenseType2__r.Name) + expense.Amount_Remaining_to_Recover__c);
                    }
                }
            }
        }

        //List<Expense_Summary__c> expenseSummaries = new List<Expense_Summary__c>();
        for (String expenseType : expensesMapByType.keySet()) {
            Expense_Summary__c expenseSummary;
            //does an expense summary already exist?
            if ( expenseSummaries == null || !expenseSummaries.containsKey(expenseTypeMapByName.get(expenseType).Id) ) {
                expenseSummary = new Expense_Summary__c();
                expenseSummary.Matter__c = matterId;
                expenseSummary.Expense_Type__c = expenseTypeMapByName.get(expenseType).Id;
                expenseSummary.Total_Amount__c = expensesMapByType.get(expenseType);
                uow.registerNew(expenseSummary);
            } else {
                expenseSummary = expenseSummaries.get(expenseTypeMapByName.get(expenseType).Id);
                expenseSummary.Total_Amount__c = expensesMapByType.get(expenseType);
                uow.registerDirty(expenseSummary);
                //remove it from the list, we'll delete anything remaining
                expenseSummaries.remove(expenseTypeMapByName.get(expenseType).Id);
            }
        }

        //delete the summaries that did not need totals
        if (expenseSummaries != null && expenseSummaries.size() != 0) {
            uow.registerDeleted(expenseSummaries.values());
        }

        //return expenseSummaries;
    } //updateExpenseSummariesByMatter

    @AuraEnabled
    public static string recalculateExpenseSummariesForMatter(Id matterId) {

        Map<Id,Set<Id>> matterIds = new Map<Id,Set<Id>>();
        matterIds.put(matterId, null);

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> {litify_pm__Expense__c.getSObjectType(), Expense_Summary__c.getSObjectType()});
        ExpenseSummaryDomain.createExpenseSummaries(matterIds, uow);

        uow.commitWork();
        
        return 'Success';

    } //recalculateExpenseSummariesForMatter

    // IConstructable
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new ExpenseSummaryDomain(records);
        }
    }

} //class