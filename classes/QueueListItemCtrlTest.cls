@isTest
private class QueueListItemCtrlTest {
  static IntakeRepNavigatorWidgetQueue__c queue;
  static {
    queue = new IntakeRepNavigatorWidgetQueue__c(Name = 'Test queue');
    insert queue;


    PageReference PageRef = Page.IntakeRepNavigator_QueueListItem;
    Test.setCurrentPage(PageRef);
    ApexPages.currentPage().getParameters().put('q', queue.id);
  }

  @isTest static void Initialize_PopulatesIntakeColumnsOption() {
    Test.startTest();
    IntakeRepNavigator_QueueListItemCtrl ctrl = new IntakeRepNavigator_QueueListItemCtrl();
    Test.stopTest();

    System.assertNotEquals(0, ctrl.IntakeColumns.size());
  }
}