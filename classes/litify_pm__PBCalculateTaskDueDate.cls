/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PBCalculateTaskDueDate {
    global PBCalculateTaskDueDate() {

    }
    @InvocableMethod(label='Calculate Task Due Date' description='Calculates and sets the given tasks' due dates based on the date given.')
    global static void calculateTaskDueDates(List<litify_pm.PBCalculateTaskDueDate.PBCalculateTaskDueDateWrapper> input) {

    }
global class PBCalculateTaskDueDateWrapper {
    @InvocableVariable( required=false)
    global Date initialDate;
    @InvocableVariable( required=false)
    global Task inputTask;
    global PBCalculateTaskDueDateWrapper() {

    }
}
}
