@isTest
private class mmcommon_AccountsNextCallDateActionTest
{
    @isTest
    static void normalProcess()
    {
        Account acct = TestUtil.createPersonAccount('Test', 'Account');//new Account();
        //acct.Id = fflib_IdGenerator.generate(Account.SObjectType);

        Intake__c intake = TestUtil.createIntake(acct); //new Intake__c();
        //intake.Id = fflib_IdGenerator.generate(Intake__c.SObjectType);
        //intake.Client__c = acct.Id;

        Task tsk = new Task();
        tsk.ReminderDateTime = Date.newInstance(2017, 11, 11);
        tsk.Status = 'Not Started';
        tsk.Type = 'Callback';
        tsk.ActivityDate = Date.today();
        tsk.WhatId = intake.Id;

        insert tsk;
        tsk = [SELECT Id, ReminderDateTime, IsClosed, Status, Type, WhatId, ActivityDate FROM Task WHERE Id = :tsk.Id].get(0);
        System.assertEquals(false, tsk.isClosed, 'Apex is not properly representing the Task.isClosed property.');

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        mmcommon_IAccountsSelector mockAccountsSelector = (mmcommon_AccountsSelector) mocks.mock(mmcommon_AccountsSelector.class);
        mmintake_IIntakesSelector mockIntakesSelector = (mmintake_IntakesSelector) mocks.mock(mmintake_IntakesSelector.class);
        mmcommon_ITasksSelector mockTasksSelector = (mmcommon_TasksSelector) mocks.mock(mmcommon_TasksSelector.class);

        mocks.startStubbing();

        mocks.when(mockAccountsSelector.sObjectType()).thenReturn(Account.SObjectType);
        mocks.when(mockAccountsSelector.selectById(new Set<Id> {acct.Id})).thenReturn(new List<Account> {acct});

        mocks.when(mockIntakesSelector.sObjectType()).thenReturn(Intake__c.SObjectType);
        mocks.when(mockIntakesSelector.selectById(new Set<Id> {intake.Id})).thenReturn(new List<Intake__c> {intake});

        mocks.when(mockTasksSelector.sObjectType()).thenReturn(Task.SObjectType);
        mocks.when(mockTasksSelector.selectOpenTasksByRelatedIdSet(new Set<Id> {intake.Id})).thenReturn(new List<Task> {tsk});
        
        mocks.stopStubbing();

        mm_Application.Selector.setMock(mockAccountsSelector);
        mm_Application.Selector.setMock(mockIntakesSelector);
        mm_Application.Selector.setMock(mockTasksSelector);

        mmcommon_AccountsNextCallDateAction.isTest = true;

        mmcommon_AccountsNextCallDateAction action = new mmcommon_AccountsNextCallDateAction();
        Boolean result = action.setRecordsToActOn(new List<SObject> {tsk}).run();

        System.assertEquals(true, result);
        //System.assertEquals(tsk.ReminderDateTime, acct.NextCallDateTime__c);
    }

    // ----------------------------------------------------------
    //   A more functional test ensuring ReminderDateTime ordering.
    //   Not needed for release, but remains for posterity.
    // ----------------------------------------------------------
    // @isTest
    // static void funcTest()
    // {
    //     Account acct = new Account();
    //     acct.LastName = 'Snow';
    //     acct.NextCallDateTime__c = null;
    //     insert acct;

    //     Intake__c intake = new Intake__c();
    //     intake.Client__c = acct.Id;
    //     insert intake;

    //     Task tsk_sooner = new Task();
    //     tsk_sooner.ReminderDateTime = Date.newInstance(2017, 11, 15);
    //     tsk_sooner.Status = 'Not Started';
    //     tsk_sooner.Type = 'Callback';
    //     tsk_sooner.WhatId = intake.Id;

    //     Task tsk_target = new Task();
    //     tsk_target.ReminderDateTime = Date.newInstance(2017, 11, 17);
    //     tsk_target.Status = 'Not Started';
    //     tsk_target.Type = 'Callback';
    //     tsk_target.WhatId = intake.Id;

    //     Task tsk_distractor = new Task();
    //     tsk_distractor.ReminderDateTime = Date.newInstance(2017, 11, 20);
    //     tsk_distractor.Status = 'Not Started';
    //     tsk_distractor.Type = 'Callback';
    //     tsk_distractor.WhatId = intake.Id;

    //     insert new List<Task> {tsk_sooner, tsk_target, tsk_distractor};

    //     acct.NextCallDateTime__c = null;
    //     update acct;

    //     mmcommon_AccountsNextCallDateAction action = new mmcommon_AccountsNextCallDateAction();
    //     Boolean result = action.setRecordsToActOn(new List<SObject> {tsk_target}).run();

    //     System.assertEquals(true, result);

    //     acct = [select NextCallDateTime__c from Account where Id = :acct.Id].get(0);
    //     System.assertEquals(tsk_sooner.ReminderDateTime, acct.NextCallDateTime__c);
    // }
}