public class InvestigationEventTriggerHandler {

  // todo move to ScheduleInvestigatorService
  public static final String CANCEL_STATUS = 'Canceled';
  public static final String RESCHEDULE_STATUS = 'Rescheduled';
  private static final String EVENT_TYPE_INVESTIGATION = 'Investigation';

  public void beforeInsertHandler(List<Event> newList) {
    copyValuesFromIncidentInvestigationEvent(newList);
  }

  public void beforeUpdateHandler(Map<Id, Event> oldMap, Map<Id, Event> newMap) {
    List<Event> eventsWithChangedIncidentInvestigationEvent = new List<Event>();
    for (Event ev : newMap.values()) {
      Event oldEv = oldMap.get(ev.Id);

      if (ev.WhatId != oldEv.WhatId) {
        if (ev.WhatId == null || ev.WhatId.getSObjectType() != IncidentInvestigationEvent__c.SObjectType) {
          ev.Contracts_to_be_Signed__c = 0;
        }
        else {
          eventsWithChangedIncidentInvestigationEvent.add(ev);
        }
      }
    }

    copyValuesFromIncidentInvestigationEvent(eventsWithChangedIncidentInvestigationEvent);
  }

  private static void copyValuesFromIncidentInvestigationEvent(List<Event> newList) {
    Map<Id, Event> eventByIncidentInvestigationEventIds = filterInvestigationsByWhatId(newList);
    List<IncidentInvestigationEvent__c> incidentInvestigations = getIncidentInvestigations(eventByIncidentInvestigationEventIds.keySet());

    for (IncidentInvestigationEvent__c incidentInvestigation : incidentInvestigations) {
      Event childEvent = eventByIncidentInvestigationEventIds.get(incidentInvestigation.Id);
      childEvent.Contracts_to_be_Signed__c = incidentInvestigation.Contracts_to_be_Signed__c;
    }
  }

  public void afterInsertHandler(List<Event> newList) {
    Map<Id, Event> eventByIncidentInvestigationEventIds = filterInvestigationsByWhatId(newList);
    List<IncidentInvestigationEvent__c> incidentInvestigations = getIncidentInvestigations(eventByIncidentInvestigationEventIds.keySet());

    List<IncidentInvestigationEvent__c> incidentInvestigationsToUpdate = new List<IncidentInvestigationEvent__c>();
    List<IntakeInvestigationEvent__c> intakeInvestigationsToUpdate = new List<IntakeInvestigationEvent__c>();

    for (IncidentInvestigationEvent__c incidentInvestigation : incidentInvestigations) {
      Event ev = eventByIncidentInvestigationEventIds.get(incidentInvestigation.Id);

      if (incidentInvestigation.StartDateTime__c != ev.StartDateTime
          || incidentInvestigation.EndDateTime__c != ev.EndDateTime
          || incidentInvestigation.OwnerId != ev.OwnerId
          || incidentInvestigation.Reason_for_Delayed_Signup__c != ev.Reason_for_Delayed_Signup__c
          || incidentInvestigation.Investigation_Status__c != ev.Investigation_Status__c) {

        incidentInvestigation.StartDateTime__c = ev.StartDateTime;
        incidentInvestigation.EndDateTime__c = ev.EndDateTime;
        incidentInvestigation.OwnerId = ev.OwnerId;
        incidentInvestigation.Reason_for_Delayed_Signup__c = ev.Reason_for_Delayed_Signup__c;
        incidentInvestigation.Investigation_Status__c = ev.Investigation_Status__c;
        incidentInvestigationsToUpdate.add(incidentInvestigation);
      }

      List<IntakeInvestigationEvent__c> childIntakeInvestigations = incidentInvestigation.IntakeInvestigationEvents__r;
      for (IntakeInvestigationEvent__c childIntakeInvestigation : childIntakeInvestigations) {
        if (childIntakeInvestigation.StartDateTime__c != ev.StartDateTime
            || childIntakeInvestigation.EndDateTime__c != ev.EndDateTime
            || childIntakeInvestigation.OwnerId != ev.OwnerId
            || childIntakeInvestigation.Location__c != ev.Location
            || childIntakeInvestigation.Catastrophic__c != childIntakeInvestigation.Intake__r.Catastrophic__c
            || childIntakeInvestigation.Reason_for_Delayed_Signup__c != ev.Reason_for_Delayed_Signup__c
            || childIntakeInvestigation.Contact__c != childIntakeInvestigation.Intake__r.Client__r.PersonContactId
            || childIntakeInvestigation.Investigation_Status__c != ev.Investigation_Status__c) {

          childIntakeInvestigation.StartDateTime__c = ev.StartDateTime;
          childIntakeInvestigation.EndDateTime__c = ev.EndDateTime;
          childIntakeInvestigation.OwnerId = ev.OwnerId;
          childIntakeInvestigation.Location__c = ev.Location;
          childIntakeInvestigation.Catastrophic__c = childIntakeInvestigation.Intake__r.Catastrophic__c;
          childIntakeInvestigation.Reason_for_Delayed_Signup__c = ev.Reason_for_Delayed_Signup__c;
          childIntakeInvestigation.Contact__c = childIntakeInvestigation.Intake__r.Client__r.PersonContactId;
          childIntakeInvestigation.Investigation_Status__c = ev.Investigation_Status__c;
          intakeInvestigationsToUpdate.add(childIntakeInvestigation);
        }
      }
    }

    Database.update(intakeInvestigationsToUpdate);
    Database.update(incidentInvestigationsToUpdate);
  }

  private List<Event> GetTravelTimeRelatedTo(List<Event> events) {
    List<String> travelTimeCriteria = new List<String>();
    for (Event ev : events) {
      String whatId = ev.WhatId == null ? 'null' : '\'' + ev.WhatId + '\'';
      String s = '(OwnerId = \'' + ev.OwnerId + '\' AND WhatId = ' + whatId + ')';
      travelTimeCriteria.add(s);
    }

    String travelTimeSoql = 'SELECT ID, OwnerId, StartDateTime, EndDateTime, WhatId FROM Event WHERE Subject = \'Travel\' AND (' + String.join(travelTimeCriteria, ' OR ') + ')';

    List<Event> travelTimeToUpdate = Database.Query(travelTimeSoql);

    return travelTimeToUpdate;
  }

  // overload usedful for testing
  @TestVisible
  private void afterUpdateHandler(Event oldEvent, Event newEvent) {
    afterUpdateHandler(new List<Event> { oldEvent }, new List<Event> { newEvent });
  }

  public void afterUpdateHandler(List<Event> oldList, List<Event> newList) {
    List<IncidentInvestigationEvent__c> incidentInvestigationsToUpdate = new List<IncidentInvestigationEvent__c>();
    List<IntakeInvestigationEvent__c> intakeInvestigationsToUpdate = new List<IntakeInvestigationEvent__c>();
    List<Event> travelTimesToUpdate = new List<Event>();
    List<Id> triggeredEventsToDelete = new List<Id>();

    List<Event> travelTimes = GetTravelTimeRelatedTo(oldList);
    Map<Id, IncidentInvestigationEvent__c> eventToINCIEMap = mapRelatedIncidentInvestigationEvent(newList);

    for (Integer i = 0; i < newList.size() ; i++) {
      Event oldEvent = oldList[i];
      Event newEvent = newList[i];
      if (newEvent.Type != EVENT_TYPE_INVESTIGATION) {
        continue;
      }

      if (newEvent.WhatId != null) {
        // set INCIE properties
        IncidentInvestigationEvent__c incidentInvestigation = eventToINCIEMap.get(newEvent.id);
        incidentInvestigation.StartDateTime__c = newEvent.StartDateTime;
        incidentInvestigation.EndDateTime__c = newEvent.EndDateTime;
        incidentInvestigation.OwnerId = newEvent.OwnerId;
        incidentInvestigation.Reason_for_Delayed_Signup__c = newEvent.Reason_for_Delayed_Signup__c;
        incidentInvestigation.Investigation_Status__c = newEvent.Investigation_Status__c;
        incidentInvestigationsToUpdate.add(incidentInvestigation);

        // set associated INTIE properties
        for (IntakeInvestigationEvent__c childIntakeInvestigation : incidentInvestigation.IntakeInvestigationEvents__r) {
            childIntakeInvestigation.StartDateTime__c = newEvent.StartDateTime;
            childIntakeInvestigation.EndDateTime__c = newEvent.EndDateTime;
            childIntakeInvestigation.OwnerId = newEvent.OwnerId;
            childIntakeInvestigation.Location__c = newEvent.Location;
            childIntakeInvestigation.Catastrophic__c = childIntakeInvestigation.Intake__r.Catastrophic__c;
            childIntakeInvestigation.Reason_for_Delayed_Signup__c = newEvent.Reason_for_Delayed_Signup__c;
            childIntakeInvestigation.Contact__c = childIntakeInvestigation.Intake__r.Client__r.PersonContactId;
            childIntakeInvestigation.Investigation_Status__c = newEvent.Investigation_Status__c;
            intakeInvestigationsToUpdate.add(childIntakeInvestigation);
        }

        // find related travel time
        for (Event travel : travelTimes) {
          if (travel.OwnerId == oldEvent.OwnerId && travel.WhatId == oldEvent.WhatId) {
            if (travel.StartDateTime == oldEvent.EndDateTime) { // travel after
              travel.StartDateTime = newEvent.EndDateTime;
              travel.EndDateTime = newEvent.EndDateTime.addHours(1);              
              travelTimesToUpdate.add(travel);
            } else if (travel.EndDateTime == oldEvent.StartDateTime) { // travel before
              travel.StartDateTime = newEvent.StartDateTime.addHours(-1);
              travel.EndDateTime = newEvent.StartDateTime;
              travelTimesToUpdate.add(travel);
            }
          }
        }
      }

      if ((newEvent.To_Be_Canceled__c == true && newEvent.To_Be_Canceled__c != oldEvent.To_Be_Canceled__c)
          || (newEvent.To_Be_Rescheduled__c == true && newEvent.To_Be_Rescheduled__c != oldEvent.To_Be_Rescheduled__c)) {
        triggeredEventsToDelete.add(newEvent.Id);
      }
    }

    Database.update(intakeInvestigationsToUpdate);
    Database.update(incidentInvestigationsToUpdate);
    Database.update(travelTimesToUpdate);
    Database.delete(triggeredEventsToDelete);
  }

  @TestVisible
  private void afterDeleteHandler(Event evt) {
    afterDeleteHandler(new List<Event> { evt });
  }
  
  public void afterDeleteHandler(List<Event> oldList) {
    List<Event> travelTimes = GetTravelTimeRelatedTo(oldList);

    Map<Id, IncidentInvestigationEvent__c> eventToINCIEMap = mapRelatedIncidentInvestigationEvent(oldList);


    List<IncidentInvestigationEvent__c> incidentInvestigationsToUpdate = new List<IncidentInvestigationEvent__c>();
    List<IntakeInvestigationEvent__c> intakeInvestigationsToUpdate = new List<IntakeInvestigationEvent__c>();
    List<Event> travelTimesToDelete = new List<Event>(); 

    for (Event ev : oldList) {
      if (ev.Type != EVENT_TYPE_INVESTIGATION) {
        continue;
      }

      IncidentInvestigationEvent__c incidentInvestigation = eventToINCIEMap.get(ev.id);

      if (ev.To_Be_Canceled__c == true && incidentInvestigation.Investigation_Status__c != CANCEL_STATUS) {
        incidentInvestigation.Investigation_Status__c = CANCEL_STATUS;
        incidentInvestigation.Canceled_Rescheduled_By__c = UserInfo.getUserId();
        incidentInvestigationsToUpdate.add(incidentInvestigation);
      }
      else if (ev.To_Be_Rescheduled__c == true && incidentInvestigation.Investigation_Status__c != RESCHEDULE_STATUS) {
        incidentInvestigation.Investigation_Status__c = RESCHEDULE_STATUS;
        incidentInvestigation.Canceled_Rescheduled_By__c = UserInfo.getUserId();
        incidentInvestigationsToUpdate.add(incidentInvestigation);
      }

      List<IntakeInvestigationEvent__c> childIntakeInvestigations = incidentInvestigation.IntakeInvestigationEvents__r;
      for (IntakeInvestigationEvent__c childIntakeInvestigation : childIntakeInvestigations) {
        if (ev.To_Be_Canceled__c == true && childIntakeInvestigation.Investigation_Status__c != CANCEL_STATUS) {
          childIntakeInvestigation.Investigation_Status__c = CANCEL_STATUS;
          intakeInvestigationsToUpdate.add(childIntakeInvestigation);
        }
        else if (ev.To_Be_Rescheduled__c == true && childIntakeInvestigation.Investigation_Status__c != RESCHEDULE_STATUS) {
          childIntakeInvestigation.Investigation_Status__c = RESCHEDULE_STATUS;
          intakeInvestigationsToUpdate.add(childIntakeInvestigation);
        }
      }

      // find related travel time
      for (Event travel : travelTimes) {
        if (travel.OwnerId == ev.OwnerId && travel.WhatId == ev.WhatId) {
          travelTimesToDelete.add(travel);
        }
      }

    }

    Database.update(incidentInvestigationsToUpdate);
    Database.update(intakeInvestigationsToUpdate);
    Database.delete(travelTimesToDelete);
  }

  private static Map<Id, Event> filterInvestigationsByWhatId(List<Event> eventList) {
    Map<Id, Event> eventByIncidentInvestigationEventIds = new Map<Id, Event>();
    for (Event ev : eventList) {
      if (ev.Type == EVENT_TYPE_INVESTIGATION && ev.WhatId != null) {
        eventByIncidentInvestigationEventIds.put(ev.WhatId, ev);
      }
    }
    return eventByIncidentInvestigationEventIds;
  }

  private static Map<Id, IncidentInvestigationEvent__c> mapRelatedIncidentInvestigationEvent(List<Event> events) {
    Map<Id, List<Event>> eventsByWhatId = new Map<Id, List<Event>>();
    for (Event ev : events) {
      if (ev.WhatId != null && ev.WhatId.getSObjectType() == IncidentInvestigationEvent__c.SObjectType) {
        List<Event> mappedEvents = eventsByWhatId.get(ev.WhatId);
        if (mappedEvents == null) {
          mappedEvents = new List<Event>();
          eventsByWhatId.put(ev.WhatId, mappedEvents);
        }
        mappedEvents.add(ev);
      }
    }

    List<IncidentInvestigationEvent__c> iies = getIncidentInvestigations(eventsByWhatId.keySet());

    Map<Id, IncidentInvestigationEvent__c> result = new Map<Id, IncidentInvestigationEvent__c>();
    for(IncidentInvestigationEvent__c iie : iies) {
      List<Event> mappedEvents = eventsByWhatId.get(iie.Id);
      for(Event ev : mappedEvents) {
        result.put(ev.Id, iie);
      }
    }

    return result;
  }

  private static List<IncidentInvestigationEvent__c> getIncidentInvestigations(Set<Id> incidentInvestigationEventIds) {
    List<IncidentInvestigationEvent__c> incidentInvestigations = new List<IncidentInvestigationEvent__c>();
    if (!incidentInvestigationEventIds.isEmpty()) {
      incidentInvestigations.addAll([
          SELECT
            OwnerId, StartDateTime__c, EndDateTime__c, Investigation_Status__c, Reason_for_Delayed_Signup__c, Contracts_to_be_Signed__c,
            (SELECT OwnerId, StartDateTime__c, EndDateTime__c, Investigation_Status__c, Location__c, Catastrophic__c, Contact__c, Intake__r.Catastrophic__c, Intake__r.Client__r.PersonContactId, Reason_for_Delayed_Signup__c
             FROM IntakeInvestigationEvents__r)
          FROM
            IncidentInvestigationEvent__c
          WHERE
            Id IN :incidentInvestigationEventIds
        ]);
    }
    return incidentInvestigations;
  }
}