public class mmwiz_ActionDisplayErrorResponseModel
    extends mmwiz_AbstractActionModel
{
    public mmwiz_ActionDisplayErrorResponseModel()
    {
        setType(mmwiz_ActionDisplayErrorResponseModel.class);
        setIsUIBasedAction(true);
    }
}