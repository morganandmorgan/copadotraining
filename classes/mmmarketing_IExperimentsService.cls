/**
 *  mmmarketing_IExperimentsService
 */
public interface mmmarketing_IExperimentsService
{
    void setupExperimentsAndVariationsForMTIs( list<Marketing_Tracking_Info__c> mtiRecords );
}