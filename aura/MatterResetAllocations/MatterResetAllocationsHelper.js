({
	startResetAllocation : function(component, event) 
    {
		var resetAllocationAction = component.get("c.executeAllocationReset");       	
		var toastErrorHandler = component.find('toastErrorHandler');

	    resetAllocationAction.setParams({
	        'formDescriptionId' : component.get('v.formDescriptionId'),
	        "recordId" : component.get("v.recordId")
	    });

	    resetAllocationAction.setCallback(this, function(response){
	        toastErrorHandler.handleResponse(
	        response, 
	        function(response){
	            var toastEvent = $A.get("e.force:showToast");
	            toastEvent.setParams({
	                "title": 'SUCCESS',
	                "message": 'Reset of allocation has completed',
	                "type": "success"
	            });
	            toastEvent.fire();
	        })

	        // Close the action panel
	        var dismissActionPanel = $A.get("e.force:closeQuickAction");
	        dismissActionPanel.fire();

            //Refresh the page:
            $A.get("e.force:refreshView").fire();
	    })

	    $A.enqueueAction(resetAllocationAction);
	},
})