({
    doInit : function(component, event, helper) {
        helper.getModel(component, event, helper);

        const empApi = component.find("empApi");
        const errorHandler = function (message) {
            console.error("Received error ", JSON.stringify(message));
        };
        empApi.onError(errorHandler);
        helper.subscribe(component, event, helper);
    },

    handleRouteChange : function(component, event, helper) {
        helper.getModel(component, event, helper);
    }
})