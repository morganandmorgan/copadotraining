({
    getModel : function(component, event, helper) {
        var action = component.get("c.getModel");
        action.setParam("matterId", window.location.pathname.split('/')[4]);

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.model", response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete callback');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    sortTasksBy: function(component, field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.model.documents");
        sortAsc = field == sortField ? false: true;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1? 0: (sortAsc?-1:1)*(t2?-1:1);
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.model.documents", records);
    },

    subscribe : function(component, event, helper) {
        const empApi = component.find("empApi");
        var channel = "/topic/DocumentUpdates";
        const replayId = -1;

        const callback = function (message) {
            console.log("Event Received : " + JSON.stringify(message));

            var matterId = window.location.pathname.split('/')[4];
            if(message.data.sobject.Matter__c == matterId && message.data.sobject.Document_Link__c != undefined) {
                helper.getModel(component, event, helper);
                var utilityAPI = component.find("utilitybar");
                utilityAPI.openUtility();
            }
        };

        empApi.subscribe(channel, replayId, callback).then(function(newSubscription) {
            console.log("Subscribed to channel " + channel);
            component.set("v.subscription", newSubscription);
        });
    }
})