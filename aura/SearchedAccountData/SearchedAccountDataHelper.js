({
    doInitHelper: function( component, event ) {
      console.log(component.get("v.accountIdList")+'njvfndfd');
        var filterObject = component.get( 'v.filterObject' );
        if( $A.util.isEmpty(filterObject) ) {

            component.set( 'v.message', 'No records found' );

        } else {

            var count = 0;
            Object.keys(filterObject).forEach(function(key,index) {
                if( !$A.util.isEmpty( filterObject[key] ) ) {

                    count++;
                }
            });
            if( count > 0 ) {
                component.set( 'v.message', 'No records found' );
            }
        }
  },
showIntakeTable : function(component, event) {

  	var recordId = event.getSource().get("v.title");
  	console.log(recordId+'recordId')

  	var wrapperList = component.get("v.paginateList");

    wrapperList.forEach(function(currentValue, index, arr){

     	if(currentValue.recordId == recordId){

     		currentValue.downArrow = true;
     		currentValue.rightArrow = false;

     	}	
    })
    component.set("v.paginateList",wrapperList);
},

hideChildTable : function(component, event) {

  	var recordId = event.getSource().get("v.title");
  	var wrapperList = component.get("v.paginateList");

    wrapperList.forEach(function(currentValue, index, arr){

   	  if(currentValue.recordId == recordId){

   		   currentValue.downArrow = false;
   		   currentValue.rightArrow = true;

   	  }	
   })
    component.set("v.paginateList",wrapperList);
},
updateExistingAccounts : function(component, event) {

	var recordId = event.getSource().get("v.name");
  var wrapperList = component.get("v.paginateList");
  component.set("v.intakeButton",false);
  component.set("v.selectedPersonId",recordId);

  wrapperList.forEach(function(currentValue, index, arr){

      if(currentValue.recordId == recordId){

        if(event.getSource().get('v.checked') == false){

            currentValue.isChecked = false;
            component.set("v.accountButton",false)
        }
        else{
              currentValue.isChecked = true;
              component.set("v.accountButton",true)
            }
        }
        else{
              currentValue.isChecked = false;
        }  
        if(!$A.util.isEmpty(currentValue.intakes)){

 		 	    currentValue.intakes.forEach(function(currentIntake, intakeIndex, intakeArr){
 		 		  currentIntake.isChecked = false;
 		 	})
 		 } 
    })
  component.set("v.paginateList",wrapperList);
},
updateExistingIntakes : function(component, event) {

 		 var recordId = event.getSource().get("v.name");
     console.log(recordId);
 		 var accountRecordId = event.getSource().get("v.title");
     console.log(accountRecordId);
 		 component.set("v.intakeButton",true);
 		 component.set("v.accountButton",false);
 		 component.set("v.selectedPersonId",accountRecordId);
 		 component.set("v.selectedIntakeId",recordId);
 		 
 		 var wrapperList = component.get("v.paginateList");

 		 wrapperList.forEach(function(currentValue, index, arr){
 		 	currentValue.isChecked = false;
 		 	if(!$A.util.isEmpty(currentValue.intakes)){

 		 	 currentValue.intakes.forEach(function(currentIntake, intakeIndex, intakeArr){
 		 		if(currentIntake.intakeRecordId == recordId){

   		 			if(event.getSource().get('v.checked') == false){

   		 				currentIntake.isChecked = false;
   		 				component.set("v.intakeButton",false);
   		 				component.set("v.accountButton",false);
   		 			}
   		 			else{
   		 				currentIntake.isChecked = true;
   		 			}
   		 		}
   		 		else{
   		 			currentIntake.isChecked = false;
   		 		}
 		 	})
 		 }
    })
 		component.set("v.paginateList",wrapperList);
 		console.log(wrapperList);
},
executeFlow : function (component) {

 	component.set("v.flow",true);
  var filterObject = component.get("v.filterObject");
  console.log(component.get("v.IdOfRecord")+'reasonID');
  console.log(component.get("v.selectedPersonId")+'accountId');
  console.log(component.get("v.selectedIntakeId")+'intakeId');
  console.log(filterObject.Id+'filterObject');
	 var inputVariables = [
	 { name : "PersonAccountID", type : "String", value: component.get("v.selectedPersonId")}, 
	 { name : "IntakeID", type : "String", value:component.get("v.selectedIntakeId")},
   {name : "CiscoID",  type : "String", value:filterObject.Id}, 
   {name : "ReasonID",  type : "String",value:component.get("v.IdOfRecord")}
   ];

	// Find the component whose aura:id is "flowData"
	var flow = component.find("flowData");

	// In that component, start your flow. Reference the flow's Unique Name.
	flow.startFlow("Intake_Creator_LiveIVR_2_0",inputVariables);
	
},
pagination : function(component, event) {

  try{

      var self = this;
      var paginationList  = [];
      var pageSize = component.get("v.pageSize");
      component.set("v.accountWrappperList",self.refreshPaginationList(component));
      var acccountWrapperList = component.get("v.accountWrappperList");
      var page = component.get("v.curruntPage");
      var totalPage = component.get("v.totalPage");

      var paginationType = event.getSource().get("v.alternativeText");

      if(paginationType == 'next'){
          component.set("v.curruntPage",parseInt(page)+1);
      }
      if(paginationType == 'prev'){
          component.set("v.curruntPage",parseInt(page)-1);
      }

      //Get currunt page no after calculation
      page = component.get("v.curruntPage");

      //Loop over pagination list and assign new pagination list in tempList
      if(page <= totalPage && page > 0){

          if((page)*pageSize <= acccountWrapperList.length - 1){

              paginationList.push(acccountWrapperList[0])
              for(var i = (page-1)*pageSize+1; i < (page)*pageSize+1; i++ ){
                  paginationList.push(acccountWrapperList[i]);  
              }
          }
          else{
              paginationList.push(acccountWrapperList[0])
              for(var i = (page-1)*pageSize+1; i < acccountWrapperList.length; i++ ){
                  paginationList.push(acccountWrapperList[i]);  
              }
          }
 
      }

      console.log(paginationList);
      component.set("v.paginateList",paginationList);

    }
    catch(e){
      self.showToastHelper( component,e, 'error');
    }
},
insertNewAccount : function(component, event, helper) {
  var self = this;
  try{
      
      var filterObj  = component.get("v.filterObject");
      console.log(filterObj.dnis+'>>>>Dnis value')
      var warningMessage = '';
      Object.keys(filterObj).forEach(function(key,index) {
            if( $A.util.isEmpty( filterObj[key]) &&  key == 'firstName') {

                warningMessage +=  'First Name';
            }
            if( $A.util.isEmpty( filterObj[key]) &&  key ==  'lastName')  {
              if(warningMessage != '')
                warningMessage +=  ', Last Name';
              else
                 warningMessage +=  'Last Name';
            }
            if( $A.util.isEmpty( filterObj[key]) &&  key ==  'email')  {

                if(warningMessage != '')
                warningMessage +=  ', Email';
              else
                 warningMessage +=  'Email';
            }
        });
      if(warningMessage != ''){
           self.showToastHelper( component,'Please fill required field: ' +warningMessage, 'warning');
      }
      else{

            component.set("v.showHideSpnr",true)
            var action = component.get("c.insertNewAccountWithIntake");
            action.setParams({searchData:JSON.stringify(filterObj)});

            action.setCallback(this, function(response) {

              var state = response.getState();

              if (state === "SUCCESS") {

                component.set("v.accountButton",false)
                component.set("v.intakeButton",false)
                component.set("v.showHideSpnr",false)

                var flowObject = JSON.parse(response.getReturnValue());
                console.log(flowObject)
                component.set("v.flowObj",flowObject)
                component.set("v.selectedPersonId",flowObject.accountId);
                component.set("v.selectedIntakeId",flowObject.intakeId);
                var accountListIds = component.get("v.accountIdList");
                accountListIds.push(flowObject.accountId);
                component.set("v.accountIdList",accountListIds);
                self.searchAccountRecords(component,flowObject)
                self.executeFlow(component);
                component.set("v.showHideSpnr",false);
              }
               else if (state === "ERROR") {
                  $A.util.addClass( component.find( 'myspinner' ), 'slds-hide');
                  var errors = response.getError();
                  if (errors) {
                      if (errors[0] && errors[0].message) {
                        self.showToastHelper( component,  errors[0].message, 'error');
                      }
                  } else {
                    self.showToastHelper( component, "Unknown error", 'error');
                  }
                  component.set("v.showHideSpnr",false)
              }
            });
          $A.enqueueAction(action);
      }
  }catch(e){
      self.showToastHelper( component, e, 'error');
      component.set("v.showHideSpnr",false)
   }
},
insertNewIntake : function(component, event, helper) {
  var self = this;
  try{
        var filterObj  = component.get("v.filterObject");
        var accountId = component.get("v.selectedPersonId");
        component.set("v.showHideSpnr",true)
        var action = component.get("c.insertNewIntakeWithExistAccount");
        action.setParams({acccountRecordId : accountId,
          searchData: JSON.stringify(component.get("v.filterObject"))});

         action.setCallback(this, function(response) {
           var state = response.getState();
            if (state === "SUCCESS") {
              component.set("v.accountButton",false)
              component.set("v.intakeButton",false)
              component.set("v.showHideSpnr",false)

              var flowObject = JSON.parse(response.getReturnValue());
              component.set("v.flowObj",flowObject)
              var accountListIds = component.get("v.accountIdList");
              accountListIds.push(flowObject.accountId);
              component.set("v.accountIdList",accountListIds);
              component.set("v.selectedIntakeId",flowObject.intakeId)

              self.searchAccountRecords(component,flowObject)
              self.executeFlow(component);

              component.set("v.showHideSpnr",false);
            }
             else if (state === "ERROR") {
                $A.util.addClass( component.find( 'myspinner' ), 'slds-hide');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                      self.showToastHelper( component, error[0].message, 'error');
                    }
                } else {
                  self.showToastHelper( component, "Unknown error", 'error');
                }
                component.set("v.showHideSpnr",false)
            }
          });
          $A.enqueueAction(action);

        }catch(e){
          self.showToastHelper( component, e, 'error');
          component.set("v.showHideSpnr",false)
        }
    },
     showToastHelper: function( component, message, messageType ) {
        component.find( 'toastComponent' ).showToastMethod( message, messageType );
    },
     searchAccountRecords: function(component,flowObj) {
          var self = this;
          try{
              console.log(flowObj);
               var accountListIds = component.get("v.accountIdList");
               var action = component.get("c.searchAccountRecord");
                  action.setParams({"listOfAccountIds": accountListIds,
                                    "flowObj" :JSON.stringify(flowObj )});
        
                action.setCallback(this, function(response) {

                    var state = response.getState();
                    if (state === "SUCCESS") {
                       component.set("v.showHideSpnr",false);
                       var pageSize = component.get("v.pageSize");
                        var wrapperList = JSON.parse(response.getReturnValue());

                       //Calculate total page and set in attribute
                        var totalPage = Math.ceil((wrapperList.length - 1)/pageSize);
                        component.set("v.totalPage",totalPage);
                        if(totalPage!= 0)
                        component.set("v.curruntPage",1);
                        else
                        component.set("v.curruntPage",0);
                       
                        component.set("v.accountWrappperList", wrapperList);
                        console.log(wrapperList);
                        var paginationList  = [];
                        
                       
                        if(wrapperList.length >= pageSize){
                           for(var i = 0;i< pageSize+1 ; i++ ){
                            paginationList.push(wrapperList[i]);
                           }
                        }else{
                              for(var i = 0;i< wrapperList.length ; i++ ){
                            paginationList.push(wrapperList[i]);
                           }
                        }
                        component.set("v.paginateList",paginationList);

                        wrapperList.forEach(function(currentValue, index, arr){

                          if(!$A.util.isEmpty(currentValue.intakes)){
                             currentValue.intakes.forEach(function(currentIntake, intakeIndex, intakeArr){
                              if(currentIntake.isChecked){
                                  component.set("v.selectedIntakeId",currentIntake.intakeRecordId);
                                  component.set("v.selectedPersonId",currentValue.recordId);
                                  component.set("v.intakeButton",true)
                                }
                              })
                            }
                        })
                    }
                    else if (state === "ERROR") {

                      var errors = response.getError();
                      if (errors) {
                          if (errors[0] && errors[0].message) {
                              alert(errors[0].message)
                             self.showToastHelper( component,errors[0].message, 'error');

                              }
                          } else {
                             self.showToastHelper( component,"Unknown error", 'error');
                          }
                      component.set("v.showHideSpnr",false);
                    }
                });
         
                $A.enqueueAction(action);
           }
        catch(e){
            self.showToastHelper( component,e, 'error');
             component.set("v.showHideSpnr",false);
        }
    },
    refreshPaginationList:function(component){
     var wrapperList = component.get("v.accountWrappperList");

       wrapperList.forEach(function(currentValue, index, arr){
        currentValue.isChecked = false;
        if(!$A.util.isEmpty(currentValue.intakes)){

          currentValue.intakes.forEach(function(currentIntake, intakeIndex, intakeArr){
              currentIntake.isChecked = false;
              component.set("v.intakeButton",false);
              component.set("v.accountButton",false);
          })
        }
      })

       return wrapperList;
    }
})