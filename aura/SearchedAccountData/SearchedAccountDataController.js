({
    doInit: function( component, event, helper ) {
        helper.doInitHelper( component, event );
    },
    showIntakeTable : function(component, event, helper) {
        helper.showIntakeTable(component, event);
    },
    hideChildTable: function(component, event, helper) {
        helper.hideChildTable(component, event);
    },
    hideModel : function(component, event, helper) {
        component.set("v.pupupModel",false);
        component.set("v.flow",false);
    },
    updateExistingAccount: function(component, event, helper) {
       helper.updateExistingAccounts(component, event);
    },
     updateExistingIntake: function(component, event, helper) {
        helper.updateExistingIntakes(component, event);
    },
    executeFlow : function(component, event, helper) {
        helper.executeFlow(component, event);
    },
    pagination : function(component, event, helper) {
        helper.pagination(component, event);
    },
     insertNewAccount : function(component, event, helper) {
        helper.insertNewAccount(component, event);
    },
     insertNewIntake : function(component, event, helper) {
        helper.insertNewIntake(component, event);
    },
    showHideSpinner: function(component, event, helper) {
    },
    searchAccountRecords: function(component, event, helper) {
        component.set("v.intakeButton",false);
        component.set("v.accountButton",false);

        helper.searchAccountRecords(component, null);
    },
    statusChange : function (component, event) {
    if (event.getParam('status') === "FINISHED") {
      component.set("v.flow",false)
    }
  }
})