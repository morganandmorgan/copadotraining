({
    display : function(component, event, helper) {
        component.find("tooltip").getElement().style.display = '';
    },
    displayOut : function(component, event, helper) {
        component.find("tooltip").getElement().style.display = 'none';
    }
})