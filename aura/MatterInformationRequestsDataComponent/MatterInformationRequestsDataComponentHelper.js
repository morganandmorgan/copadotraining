({
    saveInfoRecord: function(component, recordToSave) {
        var self = this;
        recordToSave.Name = recordToSave.Request__c;
        var action = component.get("c.saveRecord");
        action.setParams({
            recordObj: recordToSave
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var recordId = response.getReturnValue();
                recordToSave.Id = recordId;
                component.set("v.record", recordToSave);
                component.set("v.recordSavedState", JSON.stringify(recordToSave));
                component.set("v.checked", false);
                component.set("v.isNewRequest", false);
                self.showToast("dismissible", "Success", "Record saved Successfully", "success", 2000);
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    deleteRecord: function(component, recordToDelete) {
        var self = this;
        var action = component.get("c.deleteRecord");
        action.setParams({
            recordObj: recordToDelete
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var records = component.get("v.allRecords");
                var index;
                records.some(function(element, arr_index, _arr) {
                    if (element.Id == recordToDelete.Id) {
                        index = arr_index;
                        return true;
                    }
                });
                records.splice(index, 1);
                component.set("v.allRecords", records);
                self.showToast("dismissible", "Success", "Record Removed Successfully", "success", 2000);
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    cloneRecord: function(component, recordToSave) {
        var self = this;
        var records = component.get("v.allRecords");
        var recordString = JSON.stringify(component.get("v.record"));
        var record = JSON.parse(recordString);
        var recordIndex;
        records.some(function(element, arr_index, _arr) {
            if (element.Id == record.Id) {
                recordIndex = arr_index;
                return true;
            }
        });       
        record.Master_Id = record.Id + '_' +  Math.random().toString(36).substr(2, 9);
        record.Id = null;
        record.sobjectType = 'litify_pm__Request__c';
        record.litify_pm__Matter__c = component.get("v.parentId");
        record.litify_pm__Date_Received__c = null;
        /*record.Date_Requested__c = new Date().toJSON().slice(0, 10).replace(/ /g, ' ');*/
        record.litify_pm__Date_Requested__c = null;
        record.litify_pm__Comments__c = '';
        record.Status__c = 'Not Started';
        record.litify_pm__Facility_Name__c = '';
        records.splice(recordIndex + 1, 0, record);
        component.set("v.allRecords", records);
        var appEvent = $A.get("e.c:MatterInformationRequestsEvent");
        appEvent.setParam("type", "clone");
        appEvent.fire();
    },
    showToast: function(mode, title, message, type, duration) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "mode": mode,
            "title": title,
            "message": message,
            "type": type,
            "duration": duration
        });
        resultsToast.fire();
    },
})