({
	doInit : function(component, event, helper) {
		var record = component.get("v.record");
		component.set("v.recordSavedState",JSON.stringify(record));
		var roleName = component.get("v.record.litify_pm__Facility_Name__c");
		roleName = $A.util.isEmpty(roleName) ? '' : roleName;
		component.set("v.roleName",roleName);
	},
	save : function(component, event, helper) {
		var record = component.get("v.record");
		if (component.get("v.checked")){
			if (record.litify_pm__Facility__c == ''){
				component.set("v.roleName",'');
			}
		}
		helper.saveInfoRecord(component, record);
	},
	resetChanges : function(component, event, helper) {
		var record = component.get("v.recordSavedState");
		var previousRecord = JSON.parse(record);
		component.set("v.record", previousRecord);
		component.set("v.checked",false);
		var recordIndex;
		var records = component.get("v.allRecords");
		records.some(function(element, arr_index, _arr){
			if (!$A.util.isEmpty(previousRecord.Id)){
				if (element.Id == previousRecord.Id){
					recordIndex = arr_index;
					return true;
				}
			}
			else{
				if (element.Master_Id == previousRecord.Master_Id){
					recordIndex = arr_index;
					return true;
				}
			}			
		});
		records[recordIndex] = previousRecord;
		component.set("v.allRecords",records);		
	},
	clone : function(component, event, helper) {
		helper.cloneRecord(component);
	},
	handleEvent: function(component, event, helper) {
        if (event.getParam('type') === 'clone'){
        	var requestParent = event.getSource().get("v.record.Request__c");
	        var requestChild = component.get("v.record.Request__c");
	        var recordId = component.get("v.record.Id");
	        if (requestChild == requestParent && $A.util.isEmpty(recordId)){
	        	component.set("v.checked",true);
	        }
        } 
        else if (event.getParam('type') === 'newRequest' && $A.util.isEmpty(component.get("v.record.litify_pm__Matter__c"))) {
        	var record = component.get("v.record");
        	record.litify_pm__Matter__c = component.get("v.parentId");
        	component.set("v.isNewRequest", true);
	        component.set("v.checked",true);

        	var payload = event.getParam('payload');
        	if (!$A.util.isEmpty(payload)){
        		var requestList = JSON.parse(payload);
        		if (!$A.util.isEmpty(requestList)){
        			component.set("v.requestList", requestList);
        			record.Request__c = $A.util.isEmpty(requestList[0]) ? '' : requestList[0];
        			record.Name = $A.util.isEmpty(requestList[0]) ? '' : requestList[0];
        		}
        	}
        	component.set("v.record", record); 
			component.set("v.recordSavedState",JSON.stringify(record));
        }

    },
	edit : function(component, event, helper) {

		var self = this;
		var checked = component.get("v.checked");
		component.set("v.checked", component.get("v.checked") == true ?false :true);

	},
	remove : function(component, event, helper) {
		var confirm = window.confirm('Are you sure you would like to delete this record?\nPress OK for yes, or cancel for no.');
		if (confirm){
			var record = component.get("v.record");
			helper.deleteRecord(component, record);
		}
	},
	changeRole : function(component, event, helper) {

		var roleId = event.getSource().get("v.value");
		var roles = component.get("v.roles");
		roles.forEach(function(element){
			if (element.Id == roleId){
				component.set("v.roleName", element.Name__c == 'None' ? '' : element.Name__c);
			}
		});

	},
	increaseSize : function(component, event, helper) {
		var field = event.getSource().getLocalId();
		if (field === 'commentInputText' ){
			$A.util.addClass(component.find("commentInputText"), 'slds-hide');
        	$A.util.removeClass(component.find("commentTextArea") , 'slds-hide');        
        	window.setTimeout(function(){
        		component.find("commentTextArea").focus();
        	},200);
		}
		else if (field === 'requestInputText'){
			$A.util.addClass(component.find("requestInputText"), 'slds-hide');
        	$A.util.removeClass(component.find("requestTextArea") , 'slds-hide');        
        	window.setTimeout(function(){
        		component.find("requestTextArea").focus();
        	},200);
		}
    },    
    decreaseSize : function(component, event, helper) {
    	var field = event.getSource().getLocalId();
    	if (field === 'commentTextArea' ){
			$A.util.removeClass(component.find("commentInputText"), 'slds-hide');
        	$A.util.addClass(component.find("commentTextArea") , 'slds-hide');        
		}
		else if (field === 'requestTextArea'){
			$A.util.removeClass(component.find("requestInputText"), 'slds-hide');
        	$A.util.addClass(component.find("requestTextArea") , 'slds-hide');        
		}
    },
    removeRecord : function(component, event, helper) {
    	var records = component.get("v.allRecords");
    	var record = component.get("v.record");
        var index;
        records.some(function(element, arr_index, _arr) {
        	if(!$A.util.isEmpty(element.Master_Id) && !$A.util.isEmpty(record.Master_Id)){
        		if (element.Master_Id === record.Master_Id ) {
	                index = arr_index;
	                return true;
	            }
        	}            
        });
        records.splice(index, 1);
        component.set("v.allRecords", records);
    },
})