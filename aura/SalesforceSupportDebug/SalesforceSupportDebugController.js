({
  initComponent: function(component, event, helper) {
      let testArea = component.find('auraId').getElement();
      $(testArea).css('background-color', helper.getRandomColor());
      
	component.set('v.arry', [1,2]);
  },
    handleButtonClick: function(component, event, helper) {
        let action = component.get('c.doNothing');
        action.setCallback(this, function(response) {
                    helper.handleButtonClick(component, event);
        });
        $A.enqueueAction(action);
    },
    doNothing: function() {}
})