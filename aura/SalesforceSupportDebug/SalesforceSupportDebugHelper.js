({
	handleButtonClick: function(component, event) {
      let testArea = component.find('auraId').getElement();
      $(testArea).css('background-color', this.getRandomColor());
	},
    getRandomColor: function() {
        var hexNums = '0123456789ABCDEF';
  		var color = '#';
  		for (var i = 0; i < 6; i++) {
    		color += hexNums[Math.floor(Math.random() * 16)];
  		}
  		return color;
    }
})