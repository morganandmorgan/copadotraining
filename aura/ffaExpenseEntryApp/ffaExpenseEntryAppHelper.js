({
    initializeExpenses : function(component, event, helper) {
        var action = component.get("c.getExpenses");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data", response.getReturnValue());
                console.log(component.get("v.data"));
            }
            component.set("v.showSpinner", "false");
        });
        $A.enqueueAction(action);
    },
})