({
    doInit : function (component, event, helper)
    {
        if(component.get('v.sObjectName') == null)
        {
            helper.initializeExpenses(component, helper);
        }
    },

    handleLoad : function (component, event, helper) 
    {
        component.set("v.showSpinner", "false");
    },

    handleSubmit : function (component, event, helper) 
    {
        component.set("v.showSpinner", "true");
    } , 
    handleSuccess : function (component, event, helper) 
    {
        component.set("v.showSpinner", "false");
        component.set("v.saved", true);

        var payload = event.getParams().response;
        component.set("v.myId", payload.id);

        payload.id = '';
    },
    handleError : function (component, event, helper) 
    {
        component.set("v.showSpinner", "false");
        component.set("v.saved", false);
    },

    handleClick: function (component, event, helper) 
    {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.myId")
        });
        navEvt.fire();
    },
    handleNewRecord: function (component, event, helper) 
    {
        component.set("v.saved", false);
    },
    onSave: function ( component, event, helper)
    {

    }
})