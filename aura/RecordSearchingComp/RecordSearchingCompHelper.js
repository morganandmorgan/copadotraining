({
    doInit: function(component, event) {
        let self = this;
        let selectedRecordObject = component.get('v.selectedRecordObject');
        component.set('v.preselectedValue', selectedRecordObject.label);
        //let preselectedValue
        component.set("v.showHideSpnr", true);
        console.log(component.get("v.objectApiName"));
        var action = component.get("c.retrieveRecordSearchData");
        action.setParams({
            objectName: component.get("v.objectApiName"),
            fieldSetName: component.get("v.searchingFieldSetName")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                let returnValue = JSON.parse(result);
                component.set("v.searchFieldSetData", returnValue.searchFieldSetData);
                let objectNameField = returnValue.nameFieldAPIName;
                console.log(objectNameField);
                component.set('v.objectNameField', returnValue.nameFieldAPIName);
                self.handleFormLoadHelper(component, event);
                if(!$A.util.isEmpty(selectedRecordObject.label)) {
                    self.searchRecord(component, event);
                } else {
                    component.set("v.showHideSpnr",false);
                }
            } else {
                component.set("v.showHideSpnr",false);
                self.showServerError(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    searchRecord: function(component, event) {
        var self = this;
        var sortingString = 'Order By '+component.get('v.sortFieldName')+' '+component.get("v.sortDir");
        component.set("v.showHideSpnr",true);
        let validSoFar = self.validateFiltersHelper(component, event);
        if(validSoFar +' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>.');
        if(!validSoFar) {
            component.set("v.showHideSpnr", false);
            return;
        }
        var whereClause = self.createWhereClause(component);
        console.log('where clause___________');
        console.log(whereClause);
        if(!$A.util.isEmpty(whereClause)){
        if(!$A.util.isEmpty(sortingString )) {
            whereClause = whereClause+' '+sortingString;
        }
        
        console.log(whereClause);
        var action = component.get("c.searchedRecords");
        action.setParams({
            objectName: component.get("v.objectApiName"),
            resultFieldSetName:component.get("v.showResultFieldSetName"),
            whereClause: whereClause
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                let headerList = [];
                if(!$A.util.isEmpty(result) && result.length > 0) {
                    headerList = result.splice(0,1);
                }
                console.log(headerList);
                if(!$A.util.isEmpty(headerList) && !$A.util.isEmpty(headerList[0]) && !$A.util.isEmpty(headerList[0].headerList)) {
                    component.set('v.headerList', headerList[0].headerList);
                }
                component.set("v.recordDataList", result);
                let recordPerPage = component.get('v.recordPerPage'),
                    recordDataParentList = [],
                    startIndex = 0;
                while(startIndex <= result.length) {
                    let endIndex = startIndex + recordPerPage >= result.length ? result.length : startIndex + recordPerPage;
                    recordDataParentList.push(result.slice(startIndex, endIndex));
                    startIndex += recordPerPage;
                }
                component.set('v.recordDataParentList', recordDataParentList);
                component.set('v.totalPages', recordDataParentList.length);
                component.set('v.currentPage', 1);
                component.set('v.recordDataListInView', recordDataParentList[0] !== null ? recordDataParentList[0] : []);
                component.set("v.showHideSpnr",false);
            } else {
                component.set("v.showHideSpnr",false);
                self.showServerError(component, response);
            }
        });
        $A.enqueueAction(action);
        //}
        console.log(whereClause+'>>>where clause');
        }else{
            component.set("v.showHideSpnr",false);
        }
    },
    createWhereClause: function(component, event) {
        try{
        let self = this;
        let whereClauseObj = new Object();
        var whereClause = '';
        let replaceValue;
        var fields = component.find('inputFields');
        fields = Array.isArray(fields) ? fields : [fields];
        let combo = component.get('v.customFilter');
        let searchFieldSetData = component.get('v.searchFieldSetData');
        console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        console.log(JSON.stringify(searchFieldSetData));

        let objectNameField = component.get('v.objectNameField');
        let selectedFilterType = component.get('v.selectedFilterType');
        let selectedFilters = [];
        whereClauseObj.logicType = selectedFilterType;
        let preselectedValue = component.get('v.preselectedValue');
        console.log('preselectedValue -> ', preselectedValue);
        if(!$A.util.isEmpty(preselectedValue)) {
            component.set('v.preselectedValue',null);
            console.log(JSON.stringify(searchFieldSetData));
            let nameField = searchFieldSetData.filter(obj => {
                console.log(obj.fieldApiName+' '+objectNameField);
                return obj.fieldApiName === objectNameField
            });
                console.log('<>>>>>>>>>>>><>>>>>>>>>>>>>>>>>');
                console.log(JSON.stringify(nameField));
               if(!$A.util.isEmpty(nameField)){
                if(nameField) {
                    nameField = nameField[0];
                    whereClause = objectNameField + ' LIKE \'' + preselectedValue + '%\'';
                }
                let nameFieldInput = fields.filter(obj => {
                    if( (!$A.util.isEmpty(obj.get('v.fieldName')) && obj.get('v.fieldName') === nameField.fieldApiName)
                    || (!$A.util.isEmpty(obj.get('v.name')) && obj.get('v.name') === nameField.fieldApiName)) {
                        obj.set('v.value', preselectedValue);
                        return;
                    }
                });
            }
        } else {
            if(selectedFilterType === 'Custom') {
                let customLogicInput = component.find('customLogicInput');
                if($A.util.isEmpty(customLogicInput) || $A.util.isEmpty(combo)) {
                    self.showToast(component, 'Error', 'error', 'Please enter your custom logic');
                    return;
                }
                whereClauseObj.customLogic = combo;
                let comboList = [];
                var reg = new RegExp('^([()0-9]|AND|OR|\\s)+$');
                var match = reg.test(combo.toUpperCase());
                if (match) {
                    comboList = combo.match(/\d+/g).map(Number);
                }
                
                var comboToBeReplaced = combo;
                console.log(' comboToBeReplaced:: '+comboToBeReplaced);
                whereClauseObj.logicIndexList = comboList;
                for(let index in searchFieldSetData) {
                    index = parseInt(index);
                    if(comboList.includes(index + 1)) {
                        let dataObject = searchFieldSetData[index];
                        selectedFilters.push(dataObject);
                    }
                }
                whereClauseObj.filterFieldsList = selectedFilters;
                
                //let logicIndexList = whereClauseObj.customLogic.split(' ');
                let logicIndexList = whereClauseObj.customLogic.match(/\d+/g).map(Number);
                let filterIndex = 0;
                var listOfFilterReplaceValues = [];
                for(let index in logicIndexList) {
                    let counterIndex = parseInt(logicIndexList[index]);
                    let fieldObj = whereClauseObj.filterFieldsList[filterIndex];
                        var splitCombo = comboToBeReplaced.split( counterIndex);
                        comboToBeReplaced = splitCombo[1];
                        var stringTobeReplaced = splitCombo[0]+' '+counterIndex;
                        if(!$A.util.isEmpty(fieldObj.dataValue)) {
                            if(fieldObj.dataType == 'reference'){
                                let dataValue = self.fetchStringDataType(fieldObj.dataType) ? ' LIKE \'' + fieldObj.dataValue + '%\'' : ' = ' + fieldObj.dataValue;
                                let referenceField = searchFieldSetData.filter(obj => {
                                    return obj.fieldApiName === fieldObj.fieldApiName
                                });
                                replaceValue = ' '+referenceField[0].relationshipPath + dataValue+' ';
                                listOfFilterReplaceValues.push( stringTobeReplaced.replace( counterIndex, replaceValue ) );
                                
                            }else{
                                let dataValue;
                                if(fieldObj.dataType == 'multipicklist'){
                                    dataValue = self.fetchStringDataType(fieldObj.dataType) ? ' includes (\'' + fieldObj.dataValue + '\')' : ' = ' + fieldObj.dataValue;
                                }else{
                                    dataValue = self.isContainsNameField(fieldObj.fieldLabel) ? ' LIKE \'' +fieldObj.dataValue + '%\'' : self.fetchStringDataType(fieldObj.dataType)? ' =\'' + fieldObj.dataValue + '\'' : ' = ' + fieldObj.dataValue;
                                }
                                
                                replaceValue = ' '+fieldObj.fieldApiName + dataValue+' ';
                                console.log( 'replaceValue '+replaceValue );
                                listOfFilterReplaceValues.push( stringTobeReplaced.replace( counterIndex, replaceValue ) );
                            }
                            //whereClause += replaceValue + ' ';
                        }
                        filterIndex++;
                }
                for( var repl in listOfFilterReplaceValues ) {
                    whereClause += listOfFilterReplaceValues[repl]+' ';
                }
                if(!$A.util.isEmpty(comboToBeReplaced)){
                    whereClause += comboToBeReplaced;
                } 
            } else {
                let filterList = [];
                for(let index in searchFieldSetData) {
                    index = parseInt(index);
                    let dataObject = searchFieldSetData[index];
                    selectedFilters.push(dataObject);
                }
                whereClauseObj.filterFieldsList = selectedFilters;
                
                for(let index in whereClauseObj.filterFieldsList) {
                    index = parseInt(index);
                    let fieldObj = whereClauseObj.filterFieldsList[index];
                    if(!$A.util.isEmpty(fieldObj.dataValue)) {
                        if(fieldObj.dataType == 'reference'){
                            let dataValue = self.fetchStringDataType(fieldObj.dataType) ? ' LIKE \'' + fieldObj.dataValue + '%\'' : ' = ' + fieldObj.dataValue;
                            let referenceField = searchFieldSetData.filter(obj => {
                                return obj.fieldApiName === fieldObj.fieldApiName
                            });

							replaceValue = referenceField[0].relationshipPath + dataValue;
                        }else{
                            let dataValue;
                            if(fieldObj.dataType == 'multipicklist'){
                                dataValue = self.fetchStringDataType(fieldObj.dataType) ? ' includes (\'' + fieldObj.dataValue + '\')' : ' = ' + fieldObj.dataValue;
                            }else{
                                dataValue = self.isContainsNameField(fieldObj.fieldLabel) ? ' LIKE \'' + fieldObj.dataValue + '%\'' :self.fetchStringDataType(fieldObj.dataType)? ' =\'' + fieldObj.dataValue + '\'' : ' = ' + fieldObj.dataValue;
                            }
                            replaceValue = fieldObj.fieldApiName + dataValue;
                        }
                        /* let dataValue = self.fetchStringDataType(fieldObj.dataType) ?' LIKE \'%' + fieldObj.dataValue + '%\'' : ' = ' + fieldObj.dataValue;
						replaceValue = fieldObj.fieldApiName + dataValue;*/ 
                        filterList.push(replaceValue);
                    }
                }
                whereClause = filterList.join(' ' + selectedFilterType + ' ');
            }
        }
        console.log(whereClause);
        return whereClause;
        }catch(e)
        {console.log(e)}
    },
    validateFiltersHelper: function(component, event) {
        let self = this;
        let isContainsExtra = true;
        let isContainsLess = true;
        let combo = component.get('v.customFilter');
        let comboList = [];
        let selectedFilters = component.get('v.searchFieldSetData');
        console.log('selectedFilters: '+JSON.stringify(selectedFilters));
        let num = 0;
        let filterValue = component.get('v.selectedFilterType');
        if (filterValue === 'Custom') {
            var reg = new RegExp('^([()0-9]|AND|OR|\\s)+$');
            var match = reg.test(combo.toUpperCase());
            if (match) {
                for (let index = 0; index < selectedFilters.length ; index++) {
                    console.log(selectedFilters[index].dataType+':::'+selectedFilters[index].dataValue);
                    if(selectedFilters[index].dataType =='boolean' && !selectedFilters[index].dataValue){
                        isContainsExtra = true;
                    }else{
                        if( !$A.util.isEmpty( selectedFilters[index].dataValue )) {
                            if ( !combo.includes((index+1).toString())) {
                                isContainsExtra = false;
                                break;
                            }
                        }
                    }
                }
                comboList = combo.match(/\d+/g).map(Number);
            }
        }
        console.log(comboList);
        if (!$A.util.isEmpty(comboList)) {
            for (let i = 0; i < comboList.length; i++) {
                isContainsLess = false;
                for (let index = 0; index < selectedFilters.length; index++) {
                    if (index+1 === comboList[i]) {
                        if(selectedFilters[index].dataType == 'boolean' && !selectedFilters[index].dataValue){
                            isContainsLess = true;
                            break;
                        }
                        else if( $A.util.isEmpty( selectedFilters[index].dataValue )) {
                            isContainsLess = true;
                            break;
                        }
                    }else if(comboList[i] >  selectedFilters.length || comboList[i]  <0){
                        isContainsLess = true;
                            break;
                    }
                }
                if (isContainsLess === true) {
                    num = comboList[i];
                    break;
                }
            }
        }
        console.log(isContainsLess, isContainsExtra);
        if (isContainsLess === true && num != 0) {
            let message = 'The filter logic references an undefined filter: ' + num + '.';
            self.showToast(component, 'Error', 'error', message);
            return false;
        } else if (!isContainsExtra) {
            let message = 'Some filter conditions might have been defined but not referenced in your filter logic.';
            self.showToast(component, 'Warning', 'warning', message);
            return true;
        }
        self.hideToast(component);
        return true;
    },
    paginateHelper: function(component, event, newPage) {
        component.set('v.currentPage', newPage);
        let recordDataParentList = component.get('v.recordDataParentList');
        console.log(recordDataParentList);
        let recordDataListInView = recordDataParentList[newPage - 1];
        console.log(recordDataListInView);
        component.set('v.recordDataListInView', recordDataListInView);
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.showHideSpnr", false);
            }), 1000
        );
    },
    handleFormLoadHelper: function(component, event) {
        var inputFields = component.find('inputFields');
        for(let index in inputFields) {
            inputFields[index].set('v.value', null);
        }
        component.set("v.showHideSpnr", false);
        console.log('called');
    },
    showToast: function(component, title, type, message) {
        // FLOW SCREEN DOES NOT SUPPORT THE STANDARD SALESFORCE EVENTS BECAUSE THE COMPONENT IS RENDERED A IN VF PAGE
        /*$A.get('e.force:showToast').setParams({
			title: title,
			type: type,
			message: message
		}).fire();*/
        component.set('v.showError', true);
        component.set('v.errorMessage', message);
        component.set('v.errorType', 'slds-theme_' + type);
    },
    hideToast: function(component) {
        component.set('v.showError', false);
        component.set('v.errorMessage', '');
    },
    fetchStringDataType: function(inputType) {
        let stringTypeArr = ['email', 'id', 'multipicklist', 'picklist', 'reference', 'string', 'phone', 'url','address','textarea'];
        return stringTypeArr.includes(inputType);
    },
     isContainsNameField: function(inputType) {
         if(inputType.toUpperCase().includes('NAME')){
             return true;
         }else{
             return false;
         }
    },
    showServerError: function(component, response) {
        component.set("v.showHideSpnr", false);
        var errors = response.getError();
        if (errors && errors[0] && errors[0].message) {
            console.log("Error message: ", errors[0].message);
            let showToast = $A.get('e.force:showToast');
            if(!$A.util.isEmpty(showToast) && showToast != undefined) {
               showToast.setParams({
                    title: 'Error',
                    type: 'Error',
                    message: errors[0].message
                }).fire(); 
            } else {
            	alert('Server error: '+ errors[0].message);    
            }
        } else {
            console.log("Unknown error");
            
            let showToast = $A.get('e.force:showToast');
            if(!$A.util.isEmpty(showToast) && showToast != undefined) {
               showToast.setParams({
                    title: title,
                    type: type,
                    message: message
                }).fire(); 
            } else {
            	alert('Unknown error');
            }
        }
    },
    fireChangeEventHelper: function(component, event) {
        let inputFields = component.find('inputFields');
        inputFields = Array.isArray(inputFields) ? inputFields : [inputFields];
        let defaultValuesObjArr = [];
        for(let index in inputFields) {
            //if(!$A.util.isEmpty(inputFields[index].get('v.value'))) {
            defaultValuesObjArr.push({
                fieldValue: inputFields[index].get('v.value'),
                fieldName: !$A.util.isEmpty(inputFields[index].get('v.fieldName')) ? inputFields[index].get('v.fieldName') : inputFields[index].get('v.name')
            });
            //}
        }
        component.getEvent('recordSearchEvent').setParams({
            eventParamsObj: {
                type: 'searchInProgress',
                fromComponent: 'recordSearch',
                defaultValuesArr: defaultValuesObjArr
            }
        }).fire();
    },
    sortSobjectRecord: function(component, event, sortDetails) {
        let self = this;
        if(!$A.util.isEmpty( sortDetails )){
            component.set("v.sortFieldName", sortDetails.sortFieldName)
            component.set("v.sortDir", sortDetails.sortDir);
            self.searchRecord(component, event);
        }
    },
})