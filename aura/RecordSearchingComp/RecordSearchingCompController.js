({
    doInit : function(component, event, helper) {
        helper.doInit(component, event);
    },
    searchRecord : function(component, event, helper) {
        helper.searchRecord(component, event);
    },
    validateFilters: function(component, event) {   
        helper.validateFiltersHelper(component, event);
    },
    handleFormLoad: function(component, event, helper) {
       //helper.handleFormLoadHelper(component, event);
    },
    paginate: function(component, event, helper) {
        component.set('v.showHideSpnr', true);
        let label = event.getSource().get('v.label');
        let currentPage = component.get('v.currentPage');
        let newPage = label === 'Previous' ? currentPage - 1 : currentPage + 1;
        helper.paginateHelper(component, event, newPage);
    },
    fireChangeEvent: function(component, event, helper) {
        helper.fireChangeEventHelper(component, event);
    },
    selectRecord: function(component, event) {
        let recordDataComponent = component.find('recordDataComponent');
        recordDataComponent.selectRecordFromOuter();
    },
    sortSobjectRecord : function(component, event, helper) {
        let sortingDetails = event.getParam('sortingDeatils');
        helper.sortSobjectRecord(component, event, sortingDetails);
    },
})