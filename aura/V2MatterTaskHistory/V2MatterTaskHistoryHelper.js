/**
 * Created by noah on 2019-01-17.
 */
({
  sortBy : function dynamicSort(property, desc) {
    desc = !!!desc;
    desc = desc ? 1 : -1;
    var sortOrder = 1;

    if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }

    return function (a,b) {
      var result = (a[property] < b[property]) ? desc : (a[property] > b[property]) ? -desc : 0;
      return result * sortOrder;
    }
  }
})