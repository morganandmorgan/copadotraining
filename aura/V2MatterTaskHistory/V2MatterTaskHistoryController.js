/**
 * Created by noah on 2019-01-17.
 */
({
  doInit : function(component, event, helper) {
    const empApi = component.find('empApi');
    empApi.onError($A.getCallback(function(message) {
      console.error('Received error from CometD ', message);
    }));

    empApi.subscribe('/event/Document_Uploaded__e', -1 /* only new events */, $A.getCallback(function(message){
      console.log('Received message ', message);
      if (message.data.payload.MatterId__c == component.get('v.recordId')) {
        $A.enqueueAction(component.get('c.refreshData')); // refresh data
      }
    })).then($A.getCallback(function(newSubscription) {
      console.log('Subscribed to channel ', newSubscription);
    }));

    $A.enqueueAction(component.get('c.refreshData'));
  },

  refreshData: function(component, event, helper) {
    component.set('v.isLoading', true);
    var action = component.get('c.getRecs');
    action.setParams({ matterId: component.get('v.recordId') });
    component.set('v.items', []);

    action.setCallback(this, function(a){
      var model = a.getReturnValue();

      component.set('v.model', model);
      var items = model.items;

      items.forEach(function(x) {
        x.documentType = x.documentType || '';
        x.documentTo = x.documentTo || '';
        x.documentFrom = x.documentFrom || '';

      });

      var seenType = {};
      var uniqueType = [];

      var seenCode = {};
      var documentTypes = [];
      var documentSubtypes = [];

      for (var i = 0; i < items.length; i++) {
        if (items[i].type) {
          if (!seenType[items[i].type]) {
            seenType[items[i].type] = 1;
            uniqueType.push(items[i].type);
          }
        }
        if (items[i].documentType) {
          if (!seenCode[items[i].documentType]) {
            seenCode[items[i].documentType] = 1;
            documentTypes.push(items[i].documentType);
          }
        }
        if (items[i].documentSubtype) {
          if (!seenCode[items[i].documentSubtype]) {
            seenCode[items[i].documentSubtype] = 1;
            documentSubtypes.push(items[i].documentSubtype);
          }
        }
      }

      uniqueType.sort();
      documentTypes.sort();

      component.set('v.optionsType', uniqueType);
      component.set('v.sortBy', 'completedDate');
      // component.set('v.filterType', uniqueType.join(';'));

      component.set('v.documentTypes', documentTypes);
      component.set('v.documentSubtypes', documentSubtypes);
      //component.set('v.filterDocType', '');

      // component.set('v.items', items);
      // component.set('v.allItems', items.slice(0));
      component.set('v.allItems', items);
      // component.set('v.items', items);

      component.set('v.totalCount', model.totalCount);

      $A.enqueueAction(component.get('c.filterItems'));
      component.set('v.isLoading', false);
    });

    $A.enqueueAction(action);
  },
  filterItems: function(component, event, helper) {
    var allItems = component.get('v.allItems');
    var type = component.get('v.filterType');
    type = type == '' ? null : type.split(';');
    var docType = component.get('v.filterDocType');
    var docSubtype = component.get('v.filterDocSubtype');
    if (type && type.every(function(x) { return x != 'Document' && x != 'Email' })) {
      component.find('filterByDocumentType').set('v.disabled', true);
      component.find('filterByDocumentSubtype').set('v.disabled', true);
      docType = null;
      docSubtype = null;
    } else {
      component.find('filterByDocumentType').set('v.disabled', false);
      component.find('filterByDocumentSubtype').set('v.disabled', false);
    }
    var searchStr = (component.get('v.searchStr') || '').toLowerCase();
    if(searchStr == ':clear') {
      $A.enqueueAction(component.get('c.clearFilters'));
    } else {
      let regex = new RegExp(searchStr, "i");
      var items = allItems.filter(function(it) {
        return (!docType || docType == it.documentType)
          && (!docSubtype || docSubtype == it.documentSubtype)
          && (!type || type.includes(it.type))
          && (!searchStr
            || (it.id != undefined && regex.test(it.id))
            || (it.subject != undefined && regex.test(it.subject))
            || (it.assignedTo != undefined && regex.test(it.assignedTo))
            || (it.timeKeeper != undefined && regex.test(it.timeKeeper))
            || (it.description != undefined && regex.test(it.description))
            || (it.type != undefined && regex.test(it.type))
            || (it.detail != undefined && regex.test(it.detail))
            || (it.documentTo != undefined && regex.test(it.documentTo))
            || (it.documentFrom != undefined && regex.test(it.documentFrom))
            || (it.documentType != undefined && regex.test(it.documentType))
            || (it.documentSubtype != undefined && regex.test(it.documentSubtype))
          )
      });

      items.sort(helper.sortBy(component.get('v.sortBy'), component.get("v.sortAsc")));

      let desiredItemCount = component.get('v.desiredItemCount');
      if (desiredItemCount > 0) {
        items = items.slice(0, desiredItemCount);
      }

      component.set('v.items', items);
    }
  },

  clearFilters : function(component, event, helper) {
    component.set("v.filterType", "");
    component.set("v.filterDocType", "");
    component.set("v.filterDocSubtype", "");
    component.set("v.searchStr", "");
    $A.enqueueAction(component.get('c.filterItems'));
  },

  searchStrChanged : function(component, event, helper) {
    // all this complexity in order to create a more responsive experience
    // without throwing onto bg thread typing into the search box slows down
    // to the speed of the c.filterItems function

    let oldTimeout = component.get('v.strChangedTimeout');
    if (oldTimeout) {
      window.clearTimeout(oldTimeout);
    }

    let timeout = window.setTimeout(
      $A.getCallback(function() {
        $A.enqueueAction(component.get('c.filterItems'));
      }), 125);

    component.set('v.strChangedTimeout', timeout);
  },

  openTaskModalForView : function(component, event, helper) {
    component.set("v.taskId", event.target.id);
    component.set("v.showTaskModal", true);
    component.set("v.editMode", false);
  },

  saveTask : function(component, event, helper) {
    component.find("editTask").get("e.recordSave").fire();
    component.set("v.showTaskModal", false);
    $A.enqueueAction(component.get('c.doInit'));
  },

  editTask : function(component, event, helper) {
    component.set("v.editMode", true);
  },

  cancelTaskEdit : function(component, event, helper) {
    component.set("v.editMode", false);
  },

  closeTaskModal : function(component, event, helper) {
    component.set("v.showTaskModal", false);
    component.set("v.editMode", true);
  },

  reverseSort : function(component, event, helper) {
    component.set("v.sortAsc", !component.get("v.sortAsc"));
    $A.enqueueAction(component.get('c.filterItems'));
  },

  setCountTo15 : function(component, event, helper) {
    component.set("v.desiredItemCount", "15");
    // $A.enqueueAction(component.get('c.doInit'));
    $A.enqueueAction(component.get('c.filterItems'));
  },

  setCountTo25 : function(component, event, helper) {
    component.set("v.desiredItemCount", "25");
    // $A.enqueueAction(component.get('c.doInit'));
    $A.enqueueAction(component.get('c.filterItems'));
  },

  setCountTo50 : function(component, event, helper) {
    component.set("v.desiredItemCount", "50");
    // $A.enqueueAction(component.get('c.doInit'));
    $A.enqueueAction(component.get('c.filterItems'));
  },

  setCountTo100 : function(component, event, helper) {
    component.set("v.desiredItemCount", "100");
    // $A.enqueueAction(component.get('c.doInit'));
    $A.enqueueAction(component.get('c.filterItems'));
  },

  setCountToAll : function(component, event, helper) {
    component.set("v.desiredItemCount", "0");
    // $A.enqueueAction(component.get('c.doInit'));
    $A.enqueueAction(component.get('c.filterItems'));
  },

  sortByName: function(component, event, helper) {
    component.set('v.sortBy', 'subject');
    $A.enqueueAction(component.get('c.reverseSort'));
  },
  sortByDate: function(component, event, helper) {
    component.set('v.sortBy', 'completedDate');
    $A.enqueueAction(component.get('c.reverseSort'));
  },
  sortByDocumentType: function(component, event, helper) {
    component.set('v.sortBy', 'documentType');
    $A.enqueueAction(component.get('c.reverseSort'));
  },
  sortByDocumentTo: function(component, event, helper) {
    component.set('v.sortBy', 'documentTo');
    $A.enqueueAction(component.get('c.reverseSort'));
  },
  sortByDocumentFrom: function(component, event, helper) {
    component.set('v.sortBy', 'documentFrom');
    $A.enqueueAction(component.get('c.reverseSort'));
  },
  sortByCreatedBy: function(component, event, helper) {
    component.set('v.sortBy', 'assignedTo');
    $A.enqueueAction(component.get('c.reverseSort'));
  },

  toggleBody : function(component, event, helper) {
    var theId = event.currentTarget && event.currentTarget.dataset && event.currentTarget ? event.currentTarget.dataset.id : null;

    var allItems = component.get('v.items');

    var showBody = false;
    var itemsToExpandContract = allItems;
    if (theId) {
      var item = allItems.find(function(el) { return el.id == theId});
      if (item) {
        itemsToExpandContract = [item];
        showBody = !!!item._isExpanded;
      }
    } else {
      showBody = !component.get("v.showFullBody");
      component.set("v.showFullBody", showBody);
    }

    itemsToExpandContract.forEach(function(i) {
      i._isExpanded = showBody;
    });
    if (showBody) {
      var action = component.get('c.getBodies');
      action.setParams({ids: itemsToExpandContract.map(x => x.id)});
      action.setCallback(this, function (res) {
        var bodies = res.getReturnValue();
        var allItems = component.get('v.items');

        allItems.forEach(function (item) {
          let body = bodies[item.id];
          if (body) {
            item.description = body;
          }
        });
        component.set('v.items', allItems);
      });
      $A.enqueueAction(action);
    } else{
      itemsToExpandContract.forEach(function (i) {
        i.description = null;
      });
      component.set('v.items', allItems);
    }
  },

  editField: function(component, event, helper) {
    var theId = event.currentTarget.dataset.id;
    var theField = event.currentTarget.dataset.field;

    var allItems = component.get('v.items');
    allItems.forEach(function(x) { x._isEditing = false; });
    component.set('v.items', allItems);

    window.setTimeout(
      $A.getCallback(function() {

        var currentItem = allItems.find(function (el) {
          return el.id == theId
        });
        currentItem._isEditing = theField;
        component.set('v.items', allItems);
      }), 1);
  },

  cancelEdit: function(component, event, helper) {
    let theId = event.currentTarget.dataset.id;
    let allItems = component.get('v.items');
    let theItem = allItems.find(function(x) { return x.id == theId; });
    theItem._isEditing = false;
    component.set('v.items', allItems);
  },
  saveEdit: function(component, event, helper) {
    let theId = event.currentTarget.dataset.id;
    let theField = event.currentTarget.dataset.field;
    let allItems = component.get('v.items');
    let theItem = allItems.find(function(x) { return x.id == theId; });
    let inputElement = component.find('inlineEditField');
    let value = inputElement.get('v.value');

    var action = component.get('c.updateField');
    action.setParams({ id: theId, fieldName: theField, value: value });
    action.setCallback(this, function(a) {
      component.set('v.items', allItems);
    });
    $A.enqueueAction(action);
    theItem[theField] = value;
    theItem._isEditing = false;
  },
})