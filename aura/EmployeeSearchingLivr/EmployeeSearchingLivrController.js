({
    searchDataChanged: function(component, event, helper) {
        console.log("searchDataChanged");
        component.set("v.searching",true);
        var lastTimer = component.get("v.searchTimer");
        if (lastTimer) {
            window.clearTimeout(lastTimer);
        }

        var data = {
            "firstName": component.find("dsps_firstName").get("v.value"),
            "lastName": component.find("dsps_lastName").get("v.value"),
            "department": component.find("dsps_department").get("v.value"),
            "tags": component.find("dsps_keyword").get("v.value"),
            "extension": component.find("dsps_extension").get("v.value"),
            "phoneNumber": component.find("dsps_phoneNumber").get("v.value"),
            "location": component.find("dsps_location").get("v.value")
        };

        data = helper.cleanAndValidateData(data);

        var hasSomething = !helper.dataIsEmpty(data)
        if (!hasSomething) {
            component.set("v.searchResults", []);
            component.set('v.searchTimer', false);
            return;
        }

        lastTimer = window.setTimeout(
            $A.getCallback(function() {
                var lt = component.get('v.searchTimer');
                console.log('Got back timer for: ' + lt);
                if (lt != lastTimer) return;

                var action = component.get("c.searchForUsers")
                action.setParams({"data": data});
                action.setCallback(this, function(response) {
                    console.log("Received:\n" + JSON.stringify(response.getReturnValue(), null, 4));
                    var lt = component.get('v.searchTimer');
                    console.log('Got back results for: ' + lt);
                    if (lt != lastTimer) {
                        console.log('Timer not current - returning');
                        return;
                    }

                    if (response.getState() === "SUCCESS") {
                    	component.set("v.searching",false);
                        console.log(response.getReturnValue())
                        component.set("v.searchResults", response.getReturnValue());
                    }
                    else{
                    	component.set("v.searching",false);
                    }
                });
                $A.enqueueAction(action);
            })
        , 1000);
        console.log('New timer: ' + lastTimer);
        component.set('v.searchTimer', lastTimer);
    }
})