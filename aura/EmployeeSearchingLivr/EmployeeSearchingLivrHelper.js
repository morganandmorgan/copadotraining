({
    cleanAndValidateData: function(data) {
        data["firstName"] = mm.lib.string.reduceToAlphaNumerics(data["firstName"]);
        data["lastName"] = mm.lib.string.reduceToAlphaNumerics(data["lastName"]);

        data["phoneNumber"] = mm.lib.string.reduceToDigits(data["phoneNumber"]);
        if (data["phoneNumber"] && data["phoneNumber"].length !== 10) data["phoneNumber"] = '';

        data["extension"] = mm.lib.string.reduceToDigits(data["extension"]);

        return data;
    },
    dataIsEmpty: function(data) {
        for (var k in data) {
            if (data.hasOwnProperty(k) && data[k] && 0 < data[k].length) {
                return false;
            }
        }
        return true;
    }
})