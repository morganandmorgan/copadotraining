({
   hideDivHelper : function(component, event){
        var id =component.find('Div1');
        $A.util.addClass(id , 'hide');
        var id2 =component.find('Div2');
        $A.util.addClass(id2 , 'divWidth');
        component.set("v.isVisible",true);
    }, 
    setDivWidthHelper : function(component, event){
        var id =component.find('Div1');
        $A.util.removeClass(id , 'hide');
        var id2 =component.find('Div2');
        $A.util.removeClass(id2 , 'divWidth');
        component.set("v.isVisible",false);
    }
})