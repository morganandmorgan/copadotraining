({
    doInit: function(component, event, helper) {
        helper.doInitHelper(component, event);
    },
    handleSuccess: function(component, event, helper) {
        /*let recordId = component.set('v.recordId', event.getParam('id'));
        console.log(recordId);*/
        let params = event.getParams();
        let recordId = params.response.id;
        console.log(recordId);
        component.getEvent('recordSearchEvent').setParams({
            eventParamsObj: {
                type: 'searchComplete',
                recordId: recordId
            }
        }).fire();
        helper.hideSpinner(component);
    },
    handleOnActive: function(component, event, helper) {
        if(component.get('v.initComplete')) {
            helper.showSpinner(component);
            helper.handleOnActiveHelper(component, event);
        }
    },
    handleSubmit: function(component, event, helper) {
        helper.showSpinner(component);
    },
    handleError: function(component, event, helper) {
        helper.hideSpinner(component);
    }
})