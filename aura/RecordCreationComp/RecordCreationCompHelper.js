({
    doInitHelper: function(component, event) {
        let self = this;
        self.showSpinner(component);
        let action = component.get('c.retieveFieldSet');
        action.setParams({
            objectName: component.get('v.objectAPIName'),
            fieldSetName: component.get('v.fieldSetName')
        });
        action.setCallback(this, function(response) {
            console.log(response.getState());
            if(response.getState() === 'SUCCESS') {
                let returnValue = response.getReturnValue();
                console.log(returnValue);
                let fieldsList = [];
                for(let index in returnValue) {
                    fieldsList.push({
                        fieldApiName: returnValue[index].fieldApiName
                    });
                }
                component.set('v.fieldsList', fieldsList);
                component.set('v.initComplete', true);
                self.fireTabSelectEvent(component);
                //self.hideSpinner(component);
            } else {
                self.showError(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    showSpinner: function(component) {
        $A.util.removeClass(component.find('spinner'), 'slds-hide');
    },
    hideSpinner: function(component) {
        $A.util.addClass(component.find('spinner'), 'slds-hide');
    },
    showError: function(component, response) {
        let errors = response.getError();
        let errorString = 'Error from server';
        if(errors) {
            errorString = '';
            if(Array.isArray(errors)) {
                for(let index in errors) {
                    errorString += errors[index].message;
                }
            } else {
                errorString = errors;
            }
        }
        $A.get('e.force:showToast').setParams({
            title: 'Error',
            type: 'error',
            message: errorString
        }).fire();
        self.hideSpinner(component);
    },
    handleOnActiveHelper: function(component, event) {
        let self = this;
        let params = event.getParam('arguments');
        if(params) {
            let defaultFieldValues = params.defaultFieldValues;
            console.log('*****', defaultFieldValues);
            let fieldsList = component.get('v.fieldsList');
            for(let fieldIndex in fieldsList) {
                for(let index in defaultFieldValues) {
                    if(defaultFieldValues[index].fieldName === fieldsList[fieldIndex].fieldApiName) {
                        fieldsList[fieldIndex].fieldValue = defaultFieldValues[index].fieldValue;
                        break;
                    }
                }
            }
            component.set('v.fieldsList', fieldsList);
        }
        self.hideSpinner(component);
    },
    fireTabSelectEvent: function(component) {
        component.getEvent('recordSearchEvent').setParams({
            eventParamsObj: {
                type: 'searchInProgress',
                fromComponent: 'recordCreation'
            }
        }).fire();
    }
})