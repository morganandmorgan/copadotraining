/**
 * Created by noah on 2019-01-19.
 */
({
  doInit : function(component, event, helper) {
    let fileExtension = component.get('v.fileExtension');
    fileExtension = (fileExtension || '').toLowerCase()
    var iconName = 'unknown';

    switch (fileExtension) {
      case 'pdf':
        iconName = 'pdf';
        break;
      case 'txt':
        iconName = 'txt';
        break;
      case 'rtf':
        iconName = 'rtf';
        break;
      case 'jpg':
      case 'jpeg':
      case 'gif':
      case 'tif':
      case 'tiff':
      case 'png':
        iconName = 'image';
        break;
      case 'wav':
      case 'mp3':
        iconName = 'audio';
        break;
      case 'avi':
      case 'mov':
      case 'mpg':
      case 'mpeg':
      case 'mp4':
        iconName = 'video';
        break;
      case 'doc':
      case 'docx':
        iconName = 'word';
        break;
      case 'ppt':
        iconName = 'ppt';
        break
      case 'eml':
        iconName = 'eml';
        break;
    }

    component.set('v.iconName', 'doctype:' + iconName);
  }
})