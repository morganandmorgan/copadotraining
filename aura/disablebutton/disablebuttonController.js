({
	closeModal : function(component, event, helper) {
	    var targetPage = window.location.pathname;
	    if(targetPage.includes("new")) {
	        targetPage = targetPage.substr(0, targetPage.length-3);
	        targetPage = targetPage + "list?filterName=Recent";
        }
		window.location.href = 'https://' + window.location.host + targetPage;
	}
})