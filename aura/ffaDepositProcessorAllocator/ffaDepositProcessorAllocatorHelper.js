({
    startDepositAllocation : function(component, event) 
    {
        var action = component.get("c.executeDepositHelper");
        var toastErrorHandler = component.find('toastErrorHandler');

        action.setParam("recordId", component.get("v.recordId"));

        // action.setCallback(this,function(response) {
        //     var state = response.getState();
        //     if (state == 'SUCCESS')
        //     {
        //         var resultsToast = $A.get("e.force:showToast");
        //         resultsToast.setParams({
        //             "title": "Successful!",
        //             "message": "Process was completed successfully."
        //         });
        //         resultsToast.fire();
        //     }
        //     else
        //     {
        //         var resultsToast = $A.get("e.force:showToast");
        //         resultsToast.setParams({
        //             "title": "Failed!",
        //             "message": "Process failed. Please contact a system administrator about this."
        //         });
        //         resultsToast.fire();

        //     }
        //     // Close the action panel
        //     var dismissActionPanel = $A.get("e.force:closeQuickAction");
        //     dismissActionPanel.fire();

        //     //Refresh the page:
        //     $A.get("e.force:refreshView").fire();

        // });
        // $A.enqueueAction(action);

        action.setCallback(this, function(response){
	        toastErrorHandler.handleResponse(
	        response, 
	        function(response){
	            var toastEvent = $A.get("e.force:showToast");
	            toastEvent.setParams({
	                "title": 'SUCCESS',
	                "message": 'Deposit Allocated',
	                "type": "success"
	            });
	            toastEvent.fire();
	        })

	        // Close the action panel
	        var dismissActionPanel = $A.get("e.force:closeQuickAction");
	        dismissActionPanel.fire();

            //Refresh the page:
            $A.get("e.force:refreshView").fire();
	    })

	    $A.enqueueAction(action);
    },
})