({
    accountSelected: function(component, event, helper) {
        helper.sendSelectedAccountIdToServer(component, event);
    },

    divClicked: function(component, event, helper) {
        console.log('handling toggle intakes event.');
        helper.toggleChildRows(component, event);
        console.log('handled toggle intakes event.');
    },

    getAccountData: function(component, event, helper) {
        helper.getAccountsFromServer(component);
    }
})