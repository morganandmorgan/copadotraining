({
    getAccountsFromServer : function(component) {

        var idList = component.get("v.accountIdList");

        if (idList && 0 < idList.length) {
            var action = component.get("c.getAccounts")
            action.setParams({"idList": idList});
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var resp = response.getReturnValue();
                    component.set("v.jsonData", resp);
                    var accountFields = [];
                    if (resp.length) {
                        for (var i = 0; i < resp[0].fieldValues.length; i++) {
                            accountFields.push(resp[0].fieldValues[i].fld);
                        }
                    }
                    component.set("v.accountFields", accountFields);
                }
            });

            $A.enqueueAction(action);
        } else {
            component.set("v.jsonData", []);
            component.set("v.accountFields", []);
        }
    },

    sendSelectedAccountIdToServer: function(component, event) {
        window.location.href = '/' + event.currentTarget.dataset.accountid;
    },

    toggleChildRows: function(component, event) {
        var relatedTo = event.currentTarget.dataset.relatedto;

        console.log('relatedTo: ' + relatedTo);

        this.toggleHidden(document.getElementsByClassName('children_' + relatedTo));
        this.toggleHidden(document.getElementsByClassName('rightArrow_' + relatedTo));
        this.toggleHidden(document.getElementsByClassName('downArrow_' + relatedTo));
    },
    toggleHidden: function(divs) {
        for (var i = 0; i < divs.length; i++) {
            $A.util.toggleClass(divs[i], "mmsswc_Hidden");
        }
    }
})