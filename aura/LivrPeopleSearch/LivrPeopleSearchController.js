({
    hideDiv :function(component, event, helper){
        helper.hideDivHelper(component, event);
    },
    setDivWidth : function(component, event, helper){
        helper.setDivWidthHelper(component, event);
    },
})