({
	destroyToast : function(component,event,helper) {
		helper.destroyToastHelper(component);
	},
	showToast: function( component, event, helper ) {
		var params = event.getParam( 'arguments' ) || event.getParams();
		helper.showToastHelper( component, params.toastMessage, params.toastType );
	}
})