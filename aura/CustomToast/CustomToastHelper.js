({
    destroyToastHelper : function(component) {
        $A.util.addClass( component.find( 'toastDiv' ), 'slds-hide' );
    },
    showToastHelper: function( component, message, type ) { 
        var delay = 5000;
        var showToast = component.get( 'v.showToast' );
        $A.util.removeClass( component.find( 'toastDiv' ), 'slds-hide' );
        component.set( 'v.message', message );
        component.set( 'v.type', type );
        component.set( 'v.title', type. toUpperCase());
        window.setTimeout( function() {
            $A.util.addClass( component.find( 'toastDiv' ), 'slds-hide' );
            component.set( 'v.message', '' );
            component.set( 'v.type', '' );
        },delay);		
    }
})