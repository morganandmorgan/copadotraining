({
	onClick : function(component, event, helper) 
    {
        
    if(component.find("settlementDate").get("v.value")) {
        // send to server
        component.set('v.showSpinner', 'true');
        helper.calculateInterest(component);
    } else {
        component.find("Type").set("v.errors", [{message:"A date is required"}]);
    }

        
	}
})