({
    calculateInterest : function(component) 
    {
        var action = component.get("c.calculateInterest");
        action.setParams({"matterId" : component.get("v.recordId"), 
            "settlementDateString" : new Date(component.get('v.settlementDate')).toJSON()
        });

        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state == 'SUCCESS')
            {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Successful!",
                    "message": "Interest calculated successfully."
                });
                resultsToast.fire();
            }
            else
            {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Failed!",
                    "message": "Process failed. Please contact a system administrator about this."
                });
                resultsToast.fire();

            }
            // Close the action panel
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();

        });
        $A.enqueueAction(action);
    
    }
})