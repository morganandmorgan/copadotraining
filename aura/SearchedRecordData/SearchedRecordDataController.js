({
    selectRecord: function(component, event, helper) {
        let selectedRecordRadio = component.find('selectedRecordRadio');
        selectedRecordRadio = Array.isArray(selectedRecordRadio) ? selectedRecordRadio : [selectedRecordRadio];
        console.log(selectedRecordRadio);
        let eventFired = false;
        for(let index in selectedRecordRadio) {
            if(selectedRecordRadio[index].get('v.checked')) {
                component.getEvent('recordSearchEvent').setParams({
                    eventParamsObj: {
                        type: 'searchComplete',
                        recordId: selectedRecordRadio[index].get('v.value')
                    }
                }).fire();
                eventFired = true;
                break;
            }
        }
        if(!eventFired) {
            alert('Please select a record');
        }
    },
    sortData: function(component, event, helper) {
        let sortFieldName = event.currentTarget.dataset.set;
        console.log('sortFieldName --> ', sortFieldName);
        let recordDataList = helper.sortDataHelper(component, event, component.get('v.recordDataList'), sortFieldName);
        component.set('v.recordDataList', recordDataList);
    },
    sortFromExternal: function(component, event, helper) {
        let sortFieldName = component.get('v.sortFieldName');
        if(!$A.util.isEmpty(sortFieldName)) {
            let recordDataList = helper.sortDataHelper(component, event, component.get('v.recordDataList'), sortFieldName);
            component.set('v.recordDataList', recordDataList);
        }
    }
})