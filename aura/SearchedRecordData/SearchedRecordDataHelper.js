({
    sortDataHelper: function(component, event, arrToSort, sortFieldName) {
        let sortDir = component.get('v.sortDir');
        console.log(sortDir);
        let existingSortFieldName = component.get('v.sortFieldName');
        existingSortFieldName = $A.util.isEmpty(existingSortFieldName) ? sortFieldName : existingSortFieldName;
        console.log(existingSortFieldName);
        component.set('v.sortFieldName', sortFieldName);
        sortDir = existingSortFieldName === sortFieldName ? (sortDir === 'ASC' ? 'DESC' : 'ASC') : 'ASC';
        console.log(sortDir);
        component.set('v.sortDir', sortDir);
        component.getEvent('recordSortingEvent').setParams({
            sortingDeatils: {
                sortFieldName: sortFieldName,
                sortDir : sortDir
            }
        }).fire();
        /*let isAsc = sortDir === 'ASC' ? true : false;
        console.log(isAsc);
        function compare(a, b) {
            let dataA = a.recordData.recordList.filter(obj => {
                return obj.fieldApiName === sortFieldName
            });
            dataA = dataA[0];
            let dataB = b.recordData.recordList.filter(obj => {
                return obj.fieldApiName === sortFieldName
            });
            dataB = dataB[0];
            if (dataA.fieldvalue > dataB.fieldvalue) return isAsc ? 1 : -1;
            if (dataB.fieldvalue > dataA.fieldvalue) return isAsc ? -1 : 1;
            return 0;
        }
        arrToSort.sort(compare);
        console.log(arrToSort);
        return arrToSort;*/
    }
})