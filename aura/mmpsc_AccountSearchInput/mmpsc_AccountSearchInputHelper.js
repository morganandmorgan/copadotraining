({
    cleanAndValidateData: function(data) {
        data["firstName"] = mm.lib.string.reduceToAlphaNumerics(data["firstName"]);
        data["lastName"] = mm.lib.string.reduceToAlphaNumerics(data["lastName"]);

        data["phoneNumber"] = mm.lib.string.reduceToDigits(data["phoneNumber"]);
        if (data["phoneNumber"] && data["phoneNumber"].length !== 10) data["phoneNumber"] = '';

        data["email"] = mm.lib.string.removeWhitespace(data["email"]);

        data["ssn"] = mm.lib.string.reduceToDigits(data["ssn"]);
        if (data["ssn"] && data["ssn"].length !== 4) data["ssn"] = '';

        data["birthDate"] = mm.lib.date.convertToFormattedString(data["birthDate"]);

        return data;
    },
    dataIsEmpty: function(data) {
        for (var k in data) {
            if (data.hasOwnProperty(k) && data[k] && 0 < data[k].length) {
                return false;
            }
        }
        return true;
    },
    getParameterByName: function(name) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    loadInitParams: function(component) {
        if (component.get('v.initComplete')) return;
        component.set('v.initComplete', true);

        var ani = this.getParameterByName('ANI');
        if (ani) {
            component.set("v.phoneNumber", ani);
            this.searchDataChanged(component, true);
    }
    },
    searchDataChanged: function(component, immediate) {
        console.log("searchDataChanged");

        var lastTimer = component.get("v.searchTimer");
        if (lastTimer) {
            window.clearTimeout(lastTimer);
        }

        var data = {
            "firstName": component.get("v.firstName"),
            "lastName": component.get("v.lastName"),
            "phoneNumber": component.get("v.phoneNumber"),
            "email": component.get("v.email"),
            "ssn": component.get("v.ssn"),
            "birthDate": component.get("v.birthDate")
        };

        console.log("data before c&v:\n" + JSON.stringify(data, null, 4));
        data = this.cleanAndValidateData(data);
        console.log("data after c&v:\n" + JSON.stringify(data, null, 4));

        var hasSomething = !this.dataIsEmpty(data)
        console.log("!this.dataIsEmpty(data): " + hasSomething);
        if (!hasSomething) {
            component.set("v.accountIdList", []);
            return;
        }

        lastTimer = window.setTimeout(
            $A.getCallback(function() {
                var lt = component.get('v.searchTimer');
                console.log('Got back data for: ' + lt);
                if (lt != lastTimer) return;

                component.set('v.searching', true);
                console.log("Sending:\n" + JSON.stringify(data, null, 4));
                var action = component.get("c.searchForAccounts")
                action.setParams({"data": data});
                action.setCallback(this, function(response) {
                    component.set('v.searching', false);
                    console.log("Received:\n" + JSON.stringify(response.getReturnValue(), null, 4));
                    if (response.getState() === "SUCCESS") {
                        component.set("v.accountIdList", response.getReturnValue());
                    }
                });
                $A.enqueueAction(action);
            })
            , immediate ? 0 : 1000);
        console.log('New timer: ' + lastTimer);
        component.set('v.searchTimer', lastTimer);
    }
})