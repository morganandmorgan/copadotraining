({
    scriptsLoaded: function(component, event, helper) {
        component.set('v.scriptLoadComplete', true);
        if (component.get('v.scriptLoadComplete') && component.get('v.initRenderComplete')) {
            helper.loadInitParams(component);
        }
    },
    onRender: function(component, event, helper) {
        component.set('v.initRenderComplete', true);
        if (component.get('v.scriptLoadComplete') && component.get('v.initRenderComplete')) {
            helper.loadInitParams(component);
        }
    },
    searchDataChanged: function(component, event, helper) {
        helper.searchDataChanged(component);
                    }
})