({
    reloadData : function(component) {
        var action = component.get('c.getSettings');
        var h = this;
        action.setCallback(this, function(a){
            var ret = a.getReturnValue();
            component.set('v.pageSize', ret.pageSize || 0);

            h.reloadDataSet(component, 'MattersWithoutTasks');
            h.reloadDataSet(component, 'TodaysTasks');
            h.reloadDataSet(component, 'OverdueTasks');
            h.reloadDataSet(component, 'ThisWeekTasks');
        });

        $A.enqueueAction(action);
    },
    reloadDataSet: function(component, type) {
        component.set('v.isLoading' + type, true);
        var action = component.get('c.get' + type);
        action.setCallback(this, function(a) {
            var ret = a.getReturnValue();
            this.resortRecs(component, ret, type);
            component.set('v.isLoading' + type, false);
            component.set('v.arr' + type, ret);
        })
        $A.enqueueAction(action);
    },
    resortRecs: function(component, recs, type) {
        if (recs) {
            recs.sort(function(a, b) {
                if (a.dueDate || a.dueDateTime) {
                    var ax = a.dueDate ? a.dueDate : a.dueDateTime;
                    var ay = b.dueDate ? b.dueDate : b.dueDateTime;
                    var x = component.get('v.isSortAsc' + type) ? 1 : -1;
                    if (ax > ay) return x;
                    if (ax < ay) return -1 * x;
                }
                if (a.matterName > b.matterName) return 1;
                else if (a.matterName < b.matterName) return -1;
    
                else if (a.clientName > b.clientName) return 1;
                else if (a.clientName < b.clientName) return -1;
    
                else if (a.subject > b.subject) return 1;
                else if (a.subject < b.subject) return -1;
    
                return 0;
            });
        }
        return recs;
}
})