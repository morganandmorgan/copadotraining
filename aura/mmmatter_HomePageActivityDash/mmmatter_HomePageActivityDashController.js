({
    doInit : function(component, event, helper) {
        helper.reloadData(component);
    },
    navigateToRec: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var recId = selectedItem.dataset.idd;

        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": recId,
            "slideDevName": "detail"
        });
        sObectEvent.fire();
    },

    nextPageToday: function(component, event, helper) {
        component.set('v.pageNumToday', component.get('v.pageNumToday') + 1);
    },
    prevPageToday: function(component, event, helper) {
        component.set('v.pageNumToday', component.get('v.pageNumToday') - 1);
    },

    resortOverdue: function(component, event, helper) {
        component.set('v.isSortAscOverdueTasks', !component.get('v.isSortAscOverdueTasks'));
        component.set('v.pageNumOverdue', 1);

        var arr = helper.resortRecs(component, component.get('v.arrOverdueTasks'), 'OverdueTasks');
        console.log(JSON.stringify(arr));

        component.set('v.arrOverdueTasks', arr);
    },
    nextPageOverdue: function(component, event, helper) {
        component.set('v.pageNumOverdue', component.get('v.pageNumOverdue') + 1);
    },
    prevPageOverdue: function(component, event, helper) {
        component.set('v.pageNumOverdue', component.get('v.pageNumOverdue') - 1);
    },

    resortUpcoming: function(component, event, helper) {
        component.set('v.isSortAscThisWeekTasks', !component.get('v.isSortAscThisWeekTasks'));
        component.set('v.pageNumUpcoming', 1);

        var arr = helper.resortRecs(component, component.get('v.arrThisWeekTasks'), 'ThisWeekTasks');
        console.log(JSON.stringify(arr));

        component.set('v.arrThisWeekTasks', arr);
    },
    nextPageUpcoming: function(component, event, helper) {
        component.set('v.pageNumUpcoming', component.get('v.pageNumUpcoming') + 1);
    },
    prevPageUpcoming: function(component, event, helper) {
        component.set('v.pageNumUpcoming', component.get('v.pageNumUpcoming') - 1);
    },

    nextPageMatters: function(component, event, helper) {
        component.set('v.pageNumMatters', component.get('v.pageNumMatters') + 1);
    },
    prevPageMatters: function(component, event, helper) {
        component.set('v.pageNumMatters', component.get('v.pageNumMatters') - 1);
    }
})