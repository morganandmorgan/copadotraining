({
    getModel : function(component, event, helper) {
        helper.showSpinner(component);
        var action = component.get("c.getModel");
        action.setParams({ "queueId" : component.get("v.queue_id") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.model", response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete callback');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
            helper.hideSpinner(component);
        });

        $A.enqueueAction(action);
    },

    showSpinner : function(component) {
        component.set("v.spinner", true);
    },

     hideSpinner : function(component) {
        component.set("v.spinner", false);
    }
})