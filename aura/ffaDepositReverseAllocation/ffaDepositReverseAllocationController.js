({
    init : function(component, event, helper) {
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.reverseDate', today);
    },

    onClick : function(component, event, helper){
        component.set('v.showSpinner', 'true');
        helper.startReverseDeposit(component);
	}
})