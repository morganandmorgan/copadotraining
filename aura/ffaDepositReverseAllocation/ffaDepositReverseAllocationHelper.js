({
    startReverseDeposit : function(component, event) 
    {
        var resetAllocationAction = component.get("c.executeDepositReverseHelper");
        var toastErrorHandler = component.find('toastErrorHandler');

        resetAllocationAction.setParams({
            "dateString" : new Date(component.get('v.reverseDate')).toJSON(),
	        "recordId" : component.get("v.recordId")
	    });

        resetAllocationAction.setCallback(this,function(response) {
            toastErrorHandler.handleResponse(
                response, 
                function(response){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": 'SUCCESS',
                        "message": 'Reset of deposit has completed',
                        "type": "success"
                    });
                    toastEvent.fire();
                })
    
                // Close the action panel
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
    
                //Refresh the page:
                $A.get("e.force:refreshView").fire();
            })        
        $A.enqueueAction(resetAllocationAction);
    },
})