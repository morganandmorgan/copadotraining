({
    doInit : function(component, event, helper) {
        var action = component.get('c.getRecs');
        action.setParams({matterId: component.get('v.recordId'), count: component.get('v.desiredItemCount')});
        component.set('v.items', []);

        action.setCallback(this, function(a){
            var model = a.getReturnValue();

            var items = model.items;

            var seenType = {};
            var uniqueType = [];

            var seenCode = {};
            var uniqueCode = [];

            for (var i = 0; i < items.length; i++) {
                if (items[i].type) {
                    if (!seenType[items[i].type]) {
                        seenType[items[i].type] = 1;
                        uniqueType.push(items[i].type);
                    }
                }
                if (items[i].detail) {
                    if (!seenCode[items[i].detail]) {
                        seenCode[items[i].detail] = 1;
                        uniqueCode.push(items[i].detail);
                    }
                }
            }

            uniqueType.sort();
            uniqueCode.sort();

            component.set('v.optionsType', uniqueType);
            //component.set('v.filterType', '');

            component.set('v.optionsCode', uniqueCode);
            //component.set('v.filterCode', '');

            component.set('v.items', items);
            component.set('v.allItems', items.slice(0));

            component.set('v.totalCount', model.totalCount);

            $A.enqueueAction(component.get('c.filterItems'));
            component.set('v.isLoading', false);
        });

        $A.enqueueAction(action);

        component.set('v.isLoading', true);
    },

    filterItems: function(component, event, helper) {
        var items = [];
        var allItems = component.get('v.allItems');
        var type = component.get('v.filterType');
        var code = component.get('v.filterCode');
        var searchStr = (component.get('v.searchStr') != undefined) ? component.get('v.searchStr').toLowerCase() : "";
        if(searchStr == ':clear') {
            $A.enqueueAction(component.get('c.clearFilters'));
        } else {
            for (var i = 0; i < allItems.length; i++) {
                var includeItem = false;

                // Check filters
                if (
                    (!code || code == allItems[i].detail)
                    && (!type || type == allItems[i].type)
                    && (!searchStr
                            || (allItems[i].id != undefined && allItems[i].id.toLowerCase().includes(searchStr))
                            || (allItems[i].subject != undefined && allItems[i].subject.toLowerCase().includes(searchStr))
                            || (allItems[i].assignedTo != undefined && allItems[i].assignedTo.toLowerCase().includes(searchStr))
                            || (allItems[i].timeKeeper != undefined && allItems[i].timeKeeper.toLowerCase().includes(searchStr))
                            || (allItems[i].description != undefined && allItems[i].description.toLowerCase().includes(searchStr))
                            || (allItems[i].type != undefined && allItems[i].type.toLowerCase().includes(searchStr))
                            || (allItems[i].detail != undefined && allItems[i].detail.toLowerCase().includes(searchStr))
                            || (allItems[i].documentTo != undefined && allItems[i].documentTo.toLowerCase().includes(searchStr))
                            || (allItems[i].documentFrom != undefined && allItems[i].documentFrom.toLowerCase().includes(searchStr))
                        )
                   ) { items.push(allItems[i]); }
            }

            items.sort(helper.sortBy("completedDate"));
            if(component.get("v.sortAsc") == false) {
                items.reverse();
            }

            component.set('v.items', items);
        }
    },

    clearFilters : function(component, event, helper) {
        component.set("v.filterType", "");
        component.set("v.filterCode", "");
        component.set("v.searchStr", "");
        $A.enqueueAction(component.get('c.filterItems'));
    },

    searchStrChanged : function(component, event, helper) {
        $A.enqueueAction(component.get('c.filterItems'));
    },

    openTaskModalForView : function(component, event, helper) {
        component.set("v.taskId", event.target.id);
        component.set("v.showTaskModal", true);
        component.set("v.editMode", false);
    },

    saveTask : function(component, event, helper) {
        component.find("editTask").get("e.recordSave").fire();
        component.set("v.showTaskModal", false);
        $A.enqueueAction(component.get('c.doInit'));
    },

    editTask : function(component, event, helper) {
        component.set("v.editMode", true);
    },

    cancelTaskEdit : function(component, event, helper) {
       component.set("v.editMode", false);
   },

   closeTaskModal : function(component, event, helper) {
      component.set("v.showTaskModal", false);
      component.set("v.editMode", true);
   },

   reverseSort : function(component, event, helper) {
        component.set("v.sortAsc", !component.get("v.sortAsc"));
        $A.enqueueAction(component.get('c.filterItems'));
   },

   setCountTo15 : function(component, event, helper) {
       component.set("v.desiredItemCount", "15");
       $A.enqueueAction(component.get('c.doInit'));
       $A.enqueueAction(component.get('c.filterItems'));
  },

   setCountTo25 : function(component, event, helper) {
        component.set("v.desiredItemCount", "25");
        $A.enqueueAction(component.get('c.doInit'));
        $A.enqueueAction(component.get('c.filterItems'));
   },

   setCountTo50 : function(component, event, helper) {
        component.set("v.desiredItemCount", "50");
        $A.enqueueAction(component.get('c.doInit'));
        $A.enqueueAction(component.get('c.filterItems'));
   },

   setCountTo100 : function(component, event, helper) {
           component.set("v.desiredItemCount", "100");
           $A.enqueueAction(component.get('c.doInit'));
           $A.enqueueAction(component.get('c.filterItems'));
      },

   setCountToAll : function(component, event, helper) {
       component.set("v.desiredItemCount", "0");
       $A.enqueueAction(component.get('c.doInit'));
       $A.enqueueAction(component.get('c.filterItems'));
   },

   toggleFullEmailBody : function(component, event, helper) {
       component.set("v.showFullEmailBody", !component.get("v.showFullEmailBody"));
   }
})