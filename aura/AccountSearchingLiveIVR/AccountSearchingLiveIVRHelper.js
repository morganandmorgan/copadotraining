({
    cleanAndValidateData: function(data) {
        data["firstName"] = mm.lib.string.reduceToAlphaNumerics(data["firstName"]);
        data["lastName"] = mm.lib.string.reduceToAlphaNumerics(data["lastName"]);

        data["phoneNumber"] = mm.lib.string.reduceToDigits(data["phoneNumber"]);
        if (data["phoneNumber"] && data["phoneNumber"].length !== 10) data["phoneNumber"] = '';

        data["email"] = mm.lib.string.removeWhitespace(data["email"]);

        data["ssn"] = mm.lib.string.reduceToDigits(data["ssn"]);
        if (data["ssn"] && data["ssn"].length !== 4)
        { 
            data["ssn"] = '';
            }

        data["birthDate"] = mm.lib.date.convertToFormattedString(data["birthDate"]);

        return data;
    },
    dataIsEmpty: function(data) {
        for (var k in data) {
            if (data.hasOwnProperty(k) && data[k] && 0 < data[k].length && k != 'dnis') {
                return false;
            }
        }
        return true;
    },
    getParameterByName: function(name) {
        var self =this;
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var decodedUrl  = self.customDecode(url) ;
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(decodedUrl);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    customDecode:function( decString ) {
    return decodeURIComponent( decodeURIComponent( decString ) );
},
    loadInitParams: function(component) {
        if (component.get('v.initComplete')) return;
        component.set('v.initComplete', true);

        var ani = this.getParameterByName('ANI');
        if (ani) {
            var filterObject = component.get("v.filterObject");
            filterObject.phoneNumber = ani;
            component.set("v.filterObject", filterObject);
            this.searchDataChanged(component, true);
    }
    },
    searchDataChanged: function(component, immediate) {
       var self = this;
        var data = component.get("v.filterObject");
        var Id = this.getParameterByName('ID');
        console.log('Id>>>>>>>>>'+Id);
        if($A.util.isEmpty(Id))
            Id = '';
        data.Id = Id;
            component.set("v.showHideSpnr",true);
            
            var lastTimer = component.get("v.searchTimer");
            if (lastTimer) {
                window.clearTimeout(lastTimer);
            }
            component.set("v.filterObject",data);

            console.log("data before c&v:\n" + JSON.stringify(data, null, 4));
             data = self.cleanAndValidateData(data);
            console.log("data after c&v:\n" + JSON.stringify(data, null, 4));
           
            var hasSomething = !self.dataIsEmpty(data)
            if (!hasSomething) {
                component.set("v.accountIdList", []);
                return;
            }

            lastTimer = window.setTimeout(
                $A.getCallback(function() {
                    var lt = component.get('v.searchTimer');
                    if (lt != lastTimer) return;

                    component.set('v.searching', true);
                    var action = component.get("c.searchForAccounts")
                    action.setParams({"data": data});
                    action.setCallback(self, function(response) {
                        component.set('v.searching', false);
                        var state = response.getState() ;
                        if (response.getState() === "SUCCESS") {
                            console.log(response);
                            component.set("v.accountIdList", response.getReturnValue());
                             component.set("v.tempList", response.getReturnValue());
                            
                            component.set("v.showHideSpnr",false);

                        }
                        else if (state === "ERROR") {
                            $A.util.addClass( component.find( 'myspinner' ), 'slds-hide');
                            var errors = response.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                  self.showToastHelper( component, errors[0].message, 'error');
                                }
                            } else {
                              self.showToastHelper( component, "Unknown error", 'error');
                            }
                            component.set("v.showHideSpnr",false) 
                        }
                    });
                    $A.enqueueAction(action);
                })
                , immediate ? 0 : 1000);
            component.set('v.searchTimer', lastTimer);
       /* }else{
            self.showToastHelper( component, 'Aleast one field required with last name for search record', 'warning');
        }*/
    },
    lookupEvent: function( component, event) {
        try{
            var displayText = event.getParam("displayText");
            var showHidebuttons = event.getParam("showHidebuttons");
            var popupModel = event.getParam("popupModel");
            var IdOfRecord = event.getParam("IdOfRecord");
            console.log('is of record in account searching'+IdOfRecord)
            component.set("v.showHidebuttons",showHidebuttons);
            component.set("v.responseDispalyText",displayText);
            component.set("v.pupupModel",popupModel);
            component.set("v.IdOfRecord",IdOfRecord);
            
            
        }catch(e){
            self.showToastHelper( component, e, 'error');
           
        }
    },
    showToastHelper: function( component, message, messageType ) {
        component.find( 'toastComponent' ).showToastMethod( message, messageType );
    },
    validateSearchObject : function( data ) {

        var count = 0;
        Object.keys(data).forEach(function(key,index) {
                if( (!$A.util.isEmpty( data[key] ))) {
                            count++;
        }});
        if(count > 1){
            return true;
        }
        else{
            return false;
        }
    },
})