({
    scriptsLoaded: function(component, event, helper) {
        component.set('v.scriptLoadComplete', true);
       var filterObject = component.get("v.filterObject");
        var count = 0;
        Object.keys(filterObject).forEach(function(key,index) {
                if( (!$A.util.isEmpty( filterObject[key] ) && ( key != 'dnis' ) && ( key != 'Id' ))) {
                            count++;
                }
        });
        if(count > 0){
            helper.searchDataChanged(component);
        }
    },
    onRender: function(component, event, helper) {
        component.set('v.initRenderComplete', true);
       /* if (component.get('v.scriptLoadComplete') && component.get('v.initRenderComplete')) {
            helper.loadInitParams(component);
        }*/
    },
    hideModel : function(component, event, helper) {

        //'set value false in modelsdls to hide popup model'
        component.set("v.pupupModel",false);
    },
    searchDataChanged: function(component, event, helper) {
        console.log(JSON.stringify(component.get("v.filterObject")))
        helper.searchDataChanged(component);
    },
    lookupEvent: function( component, event, helper ) {
        helper.lookupEvent( component, event );
    },
    
})