({
    fetchRoles: function(component, createNewRecords) {
        var self = this;
        var action = component.get("c.fetchRelatedRole");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var roles = response.getReturnValue();
                roles.unshift({
                    Name__c: 'None',
                    Id: ''
                });

                component.set("v.roleOptions", roles);
                if (createNewRecords) {
                    self.fetchCreateRecordsWithCriteria(component);
                } else {
                    self.fetchRecordsWithCriteria(component);
                }
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    fetchRecordsWithCriteria: function(component) {
        var self = this;
        var action = component.get("c.fetchRecords");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var recordsData = response.getReturnValue();
                if (!$A.util.isEmpty(recordsData)) {                    
                    var rolesList = component.get("v.roleOptions");
                    recordsData.forEach(function(element) {
                        var roleNameAnchor = $A.util.isEmpty(element.litify_pm__Facility_Name__c) ? '' : element.litify_pm__Facility_Name__c;
                        element.litify_pm__Facility_Name__c = roleNameAnchor;
                        if (roleNameAnchor != '<a href="/" target="_self"> </a>' && !$A.util.isEmpty(roleNameAnchor)) {
                            var idStart = roleNameAnchor.indexOf("/") + 1;
                            roleNameAnchor = roleNameAnchor.slice(idStart, roleNameAnchor.indexOf("\"", (idStart + 15 ) ));
                        } 
                        if (!$A.util.isEmpty(rolesList)){
                            rolesList.some(function(rec, arr_index, _arr){                             
                                if (self.convertId(rec.Id.toString()) === self.convertId(roleNameAnchor.toString())){
                                    element.litify_pm__Facility_Name__c = $A.util.isEmpty(rec.Id) ? '' : rec.Name__c; 
                                    return true;
                                }              
                            });
                        } else {
                            element.litify_pm__Facility_Name__c = '';
                        }
                    });
                    component.set("v.records", recordsData);
                    component.set("v.arrowDirection", 'asc');
                    component.set("v.selectedTab", 'None');
                }
            }
            if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    fetchCreateRecordsWithCriteria: function(component) {
        var self = this;
        var action = component.get("c.fetchCreateRecords");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var recordsData = response.getReturnValue();
                if ($A.util.isEmpty(recordsData)) {
                    self.showToast("dismissible", "Attention :", "There are no Records Related to this Matter \n Fulfilling Criteria.", "info", 2000);
                } else {
                    var rolesList = component.get("v.roleOptions");
                    recordsData.forEach(function(element) {
                        var roleNameAnchor = $A.util.isEmpty(element.litify_pm__Facility_Name__c) ? '' : element.litify_pm__Facility_Name__c;
                        element.litify_pm__Facility_Name__c = roleNameAnchor;
                        if (roleNameAnchor != '<a href="/" target="_self"> </a>' && !$A.util.isEmpty(roleNameAnchor)) {
                            var idStart = roleNameAnchor.indexOf("/") + 1;
                            roleNameAnchor = roleNameAnchor.slice(idStart, roleNameAnchor.indexOf("\"", (idStart + 15 ) ));
                        } 
                        if (!$A.util.isEmpty(rolesList)){
                            rolesList.some(function(rec, arr_index, _arr){                             
                                if (self.convertId(rec.Id.toString()) === self.convertId(roleNameAnchor.toString())){
                                    element.litify_pm__Facility_Name__c = $A.util.isEmpty(rec.Id) ? '' : rec.Name__c; 
                                    return true;
                                }              
                            });
                        } else {
                            element.litify_pm__Facility_Name__c = '';
                        }
                    });
                    component.set("v.records", recordsData);
                    component.set("v.arrowDirection", 'asc');
                    component.set("v.selectedTab", 'None');
                }
            }
            if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    checkExisting: function(component) {
        var self = this;
        var action = component.get("c.checkRecordsExist");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var existed = response.getReturnValue();
                if (existed) {
                    existed = window.confirm("Please Click Ok to Refetch Information Requests.");
                    if (existed) {
                        self.fetchRoles(component, true);
                    }
                } else {
                    existed = window.confirm("There Are No Information Requests Exists For this Matter.Please Click Ok to Refetch Information Requests.");
                    if (existed) {
                        self.fetchRoles(component, true);
                    }
                }
            }
            if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    saveAll: function(component, recordsToSave) {
        var self = this;
        var action = component.get("c.saveRecords");
        if (!$A.util.isEmpty(recordsToSave)){
            recordsToSave.forEach(function(element) {
                element.Name = element.Request__c;
            });
        }
        action.setParams({
            recordsList: recordsToSave
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                self.fetchRoles(component, false);
                component.set("v.onEditAll", false);
                self.showToast("dismissible", "Success", "Records are saved Successfully", "success", 2000);
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    editAll: function(component, recordsToSave) {
        var self = this;
        var records = component.find("records");
        if (records[0] == undefined){
            records.set("v.checked", true);
            records.set("v.hideSingleSave", true);
        }
        else{
            records.forEach(function(element) {
                element.set("v.checked", true);
                element.set("v.hideSingleSave", true);
            });
        }
        component.set("v.onEditAll", true);
    },
    resetAll: function(component) {
        var allRecords = component.get("v.records");
        var records = [];
        if (component.find("records")[0] == undefined){
            records.push(component.find("records"));
        }
        else{
            records = component.find("records");
        }
        records.forEach(function(element) {
            var record = element.get("v.recordSavedState");
            var previousRecord = JSON.parse(record);
            element.set("v.record", previousRecord);
            element.set("v.checked",false);
            element.set("v.hideSingleSave",false);
            var recordIndex;
            allRecords.some(function(element, arr_index, _arr){
                if(!$A.util.isEmpty(previousRecord.Id)){
                    if (element.Id == previousRecord.Id){
                        recordIndex = arr_index;
                        return true;
                    }
                }
                else{
                    if (element.Master_Id == previousRecord.Master_Id){
                        recordIndex = arr_index;
                        return true;
                    }
                }                
            });
            allRecords[recordIndex] = previousRecord;
            component.set("v.records",allRecords);                   
        });
        component.set("v.onEditAll", false);        
    },
    sortRecordsByHeader: function(component, event) {
        var self = this;
        var records = component.get("v.records");
        var direction = component.get("v.arrowDirection");
        records.sort(function(a, b) {
            if ($A.util.isEmpty(a[event.target.id]) && $A.util.isEmpty(b[event.target.id])) {
                return 0;
            } else if ($A.util.isEmpty(a[event.target.id])) {
                return direction == 'asc' ? -1 : 1;
            } else if ($A.util.isEmpty(b[event.target.id])) {
                return direction == 'asc' ? 1 : -1;
            } else {
                if (direction == 'asc') {
                    return a[event.target.id].localeCompare(b[event.target.id]);
                } else {
                    return b[event.target.id].localeCompare(a[event.target.id]);
                }
            }
        });
        component.set("v.records", records);
        component.set("v.arrowDirection", direction == 'asc' ? 'desc' : 'asc');
        component.set("v.selectedTab", event.target.id);
    },
    createNewRequest: function(component) {
        var records = component.get("v.records");
        var record = {};
        record.Id = null;
        record.Master_Id = Math.random().toString(36).substr(2, 9);
        record.sobjectType = 'litify_pm__Request__c';
        record.litify_pm__Matter__c = null;
        record.litify_pm__Date_Received__c = null;
        record.litify_pm__Date_Requested__c = null;
        record.litify_pm__Comments__c = null;
        record.Status__c = 'Not Started';
        record.litify_pm__Facility_Name__c = '';
        if ($A.util.isEmpty(records)){
            records = [];
            records.push(record);
        }
        else{
            records.unshift(record);
        }
        component.set("v.records", records);
        var appEvent = $A.get("e.c:MatterInformationRequestsEvent");
        appEvent.setParam("type", "newRequest");
        var payload = component.get("v.allRequests");
        appEvent.setParam("payload", $A.util.isEmpty(payload) ? '' : JSON.stringify(payload));
        appEvent.fire();
    },
    fetchAllRequests: function(component) {
        var self = this;
        var action = component.get("c.fetchCMTRecords");
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set("v.allRequests", response.getReturnValue());
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError();
                var message;
                errors.forEach(function(element) {
                    message = message + '\n' + element.message;
                });
                self.showToast("sticky", "Error", message, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function(mode, title, message, type, duration) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "mode": mode,
            "title": title,
            "message": message,
            "type": type,
            "duration": duration
        });
        resultsToast.fire();
    },
    convertId: function(id) {
        if (id.length === 18) return id;
        // Generate three last digits of the id
        for (let i = 0; i < 3; i++) {
            let f = 0;
            // For every 5-digit block of the given id
            for (let j = 0; j < 5; j++) {
                // Assign the j-th chracter of the i-th 5-digit block to c
                let c = id.charAt(i * 5 + j);
                // Check if c is an uppercase letter
                if (c >= 'A' && c <= 'Z') {
                    // Set a 1 at the character's position in the reversed segment
                    f += 1 << j;
                }
            }
            // Add the calculated character for the current block to the id
            id += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ012345'.charAt(f);
        }
        return id;
    },


    
})