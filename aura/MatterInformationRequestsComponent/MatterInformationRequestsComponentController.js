({
    doInit: function(component, event, helper) {
        helper.fetchAllRequests(component);
        helper.fetchRoles(component, false);
    },
    addRequests: function(component, event, helper) {
    	helper.checkExisting(component);
    },
    addNewRequest: function(component, event, helper) {
        helper.createNewRequest(component);
    },
    sortRecords: function(component, event, helper) {
       	helper.sortRecordsByHeader(component, event);
    },
    edit: function(component, event, helper) {  
        helper.editAll(component);
    },
    save: function(component, event, helper) {
        var recordsToSave = component.get("v.records");
        if (!$A.util.isEmpty(recordsToSave)){
            helper.saveAll(component,recordsToSave);
        }              
    },
    reset: function(component, event, helper) {
        helper.resetAll(component);
    },
})