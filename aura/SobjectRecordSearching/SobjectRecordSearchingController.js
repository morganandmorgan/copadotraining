({
    doInit: function(component, event, helper) {       
        let locationHref = window.location.href;
        let isFlowViewer = locationHref.includes('visual.force.com');
        component.set('v.isFlowViewer', isFlowViewer);
        let recordId = component.get('v.recordId');
        console.log(recordId, ' --> recordId');
        helper.fetchRecordName(component, event, recordId);
        let innerWidth = window.innerWidth;
        component.set('v.modalWidth', Math.ceil(innerWidth * 0.7) + 'px');
        //component.set('v.modalMarginLeft', '-' + Math.ceil(innerWidth * 0.1) + 'px');
        component.set('v.modalMarginLeft', Math.ceil(innerWidth * 0.1) + 'px');
        window.addEventListener('resize', $A.getCallback(function() {
            component.set('v.modalWidth', Math.ceil(innerWidth * 0.7) + 'px');
            component.set('v.modalMarginLeft', Math.ceil(innerWidth * 0.1) + 'px');
        }));
    },
    openRecordSearchModal: function(component) {
        component.set('v.recordSearchActive', true);
        console.log(JSON.stringify(component.get('v.selectedRecordObject')));
    },
    closeRecordSearchModal: function(component) {
        component.set('v.recordSearchActive', false);
        console.log(JSON.stringify(component.get('v.selectedRecordObject')));
    },
    handleDefautlValuesEvent: function(component, event, helper) {
        let eventParamsObj = event.getParam('eventParamsObj');
        if(eventParamsObj.type === 'searchInProgress') {
            if(eventParamsObj.fromComponent === 'recordSearch') {
                component.set('v.defaultValuesArr', eventParamsObj.defaultValuesArr);
            } else if(eventParamsObj.fromComponent === 'recordCreation') {
                helper.handleTabOnActiveHelper(component, event);
            }
        } else if(eventParamsObj.type === 'searchComplete') {
            let recordId = eventParamsObj.recordId;
            helper.fetchRecordName(component, event, recordId);
            $A.get('e.c:RecordIdEvent').setParams({
                eventParamsObj: {
                    recordId: recordId,
                    componentUniqueId: component.get('v.componentUniqueId')
                }
            }).fire();
        }
    },
    handleTabOnActive: function(component, event, helper) {
        helper.handleTabOnActiveHelper(component, event);
    },
    removeSelectedRecord: function(component, event, helper) {
        let selectedRecord =  component.get('v.selectedRecordObject');
        console.log(selectedRecord.recordId);
        console.log(selectedRecord.label);
        selectedRecord.recordId = '';
        selectedRecord.label = '';
        component.set('v.selectedRecordObject', selectedRecord);
        component.set('v.showPillsContainer', false);
    },
    onresize: function(component, event, helper) {
      
    }
})