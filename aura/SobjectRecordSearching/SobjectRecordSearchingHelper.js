({
    handleTabOnActiveHelper: function(component, event) {
        let defaultValuesArr = component.get('v.defaultValuesArr');
        let recordCreationComp = component.find('recordCreationComp');
        recordCreationComp.onActive(defaultValuesArr);
    },
    fetchRecordName: function(component, event, recordId) {
        let action = component.get('c.fetchSelectedRecord');
        action.setParams({
            recordId: recordId,
            objectAPIName: component.get('v.objectAPIName')
        });
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                console.log(component.get('v.objectAPIName'));
                let returnValue = response.getReturnValue();
                console.log(returnValue);
                let selectedRecordObject = JSON.parse(returnValue);
                console.log('<<<<<<<<<<*****************>>>>>>>>>>>>>>> recor did');
                console.log(recordId);
                selectedRecordObject.isSelected = !$A.util.isEmpty(recordId);
                component.set('v.selectedRecordObject', selectedRecordObject);
                component.set('v.recordId', recordId);
                component.set('v.recordSearchActive', false);
            } else {
                alert('Error');
            }
        });
        $A.enqueueAction(action);
    }
})