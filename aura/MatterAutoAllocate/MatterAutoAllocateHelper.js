({
	startAutoAllocation : function(component, event) 
    {
		var autoAllocationAction = component.get("c.executeAutoAllocation");       	
		var toastErrorHandler = component.find('toastErrorHandler');

	    autoAllocationAction.setParams({
	        'formDescriptionId' : component.get('v.formDescriptionId'),
	        "recordId" : component.get("v.recordId")
	    });

	    autoAllocationAction.setCallback(this, function(response){
	        toastErrorHandler.handleResponse(
	        response, 
	        function(response){
	            var toastEvent = $A.get("e.force:showToast");
	            toastEvent.setParams({
	                "title": 'SUCCESS',
	                "message": 'Full Allocation of Negative Expenses and Deposits has completed',
	                "type": "success"
	            });
	            toastEvent.fire();
	        })

	        // Close the action panel
	        var dismissActionPanel = $A.get("e.force:closeQuickAction");
	        dismissActionPanel.fire();

            //Refresh the page:
            $A.get("e.force:refreshView").fire();
	    })

	    $A.enqueueAction(autoAllocationAction);
	},
})