({
	doInit : function(component, event, helper) {
      
        component.set('v.showSpinner', 'true');

		var recalcAction = component.get("c.recalculateExpenseSummariesForMatter");       	
		var toastErrorHandler = component.find('toastErrorHandler');

	    recalcAction.setParams({
	        "matterId" : component.get("v.recordId")
	    });

	    recalcAction.setCallback(this, function(response){
	        toastErrorHandler.handleResponse(
	        response, 
	        function(response) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": 'SUCCESS',
                    "message": 'Expense Summaries Recalculated for Matter.',
                    "type": "success"
                });
                toastEvent.fire();
            })

            component.set('v.showSpinner', 'false');

	        // Close the action panel
	        var dismissActionPanel = $A.get("e.force:closeQuickAction");
	        dismissActionPanel.fire();

            //Refresh the page:
            $A.get("e.force:refreshView").fire();
	    })

	    $A.enqueueAction(recalcAction);

	}, //doInit
})