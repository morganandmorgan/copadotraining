({
	doInit : function( component, event, searchValue ) {
      	try { 

      		var self = this;
	        //Get action From server and set its parameters
			var action = component.get( 'c.returnRecords' );
			
			action.setParams({"questionModel": $A.get("$Label.c.LIVRMetadataName")});

	        //Get result which is return by apex class
			action.setCallback( this, function( response ) {
                
				if( response.getState() === 'SUCCESS' ) {

					var returnVal = JSON.parse( response.getReturnValue());
					component.set( 'v.questionsWrapper', returnVal );
					component.set( 'v.objectLabel', returnVal[0].title );
					var searchText = component.get( "v.searchText")
					if(!$A.util.isEmpty(searchText)){
						self.showLookup( component );
						var filteredData = self.lookupData(component,searchText);
						component.set("v.lookupData",filteredData);
					}
				}
				else{
					self.showToastHelper( component, e, 'error');
				}
			});
			$A.enqueueAction( action );
       	}
      	catch(e){
      		self.showToastHelper( component, e, 'error');
      	}
    },
	filterListHelper: function( component, event, searchValue ) {
      	try{

      		var self = this;
      		var lookupData = component.get( 'v.lookupData' )
        		if(event.which == '40'){

        			for(var i = 0; i < lookupData.length ; i++){

        				console.log(lookupData[i].backgroundClass+'>>>>>>'+i)
        				if(!$A.util.isEmpty(lookupData[i].backgroundClass)){
        					if(i >= 5){
        						document.getElementById('container').scrollTop += 36;
        					}
        					if(i != lookupData.length-1){
	    						lookupData[i].backgroundClass = '';
        						lookupData[i+1].backgroundClass = 'liBackGround';
        					}
        					
        					break;
        				}
        				else{
        					lookupData[i].backgroundClass = '';
        				}
        			}
        			component.set("v.lookupData",lookupData);
        		}
        		else if(event.which == '38'){

        			for(var i = 0; i < lookupData.length ; i++){
        				if(!$A.util.isEmpty(lookupData[i].backgroundClass)){
        					if(i >= 5){
        						document.getElementById('container').scrollTop -= 36;
        					}
        					if(i != 0){
	    						lookupData[i].backgroundClass = '';
        						lookupData[i-1].backgroundClass = 'liBackGround';
        					}
        					break;
        				}
        				else{
        					lookupData[i].backgroundClass = '';
        				}
        			}

        			component.set("v.lookupData",lookupData);
        		}
    			else if(event.which == '13'){

	    			for(var i = 0; i < lookupData.length ; i++){
	    				if(!$A.util.isEmpty(lookupData[i].backgroundClass)){
	    					self.onSelectHelper(component,event,lookupData[i].Label);
	    					break;
	    				}
	    				else{
	    					lookupData[i].backgroundClass = '';
	    				}
	    			}
	    			component.set("v.lookupData",lookupData);
        		}
        		else{

        			var filteredData = self.lookupData(component,searchValue);
		        	component.set("v.lookupData",filteredData);
		       }
		}
		catch(e){
			$A.util.addClass( component.find( 'myspinner' ), 'slds-hide');
		}
	},
	onSelectHelper: function( component, event, label ) {

        //self is a instance of helper to call other helper methods
		var self = this;
        var showHidebuttons;
        var displayText;
        var popupModel;
        var IdOfRecord;
		var lookupData = component.get( 'v.lookupData' );

        //Loop over all record data list and get selected id
		for( var element in lookupData ) {

			if( lookupData[element].Label === label ) {
                IdOfRecord = lookupData[element].IdOfRecord;
				var selectedItem = {
					Label: label,
					actionToken: lookupData[element].actionToken
				}
				component.set( 'v.searchText','')
				component.set( 'v.selectedItem', selectedItem );
				break;
			}
		}
		if(!$A.util.isEmpty(selectedItem.actionToken)){

			if(selectedItem.actionToken.includes('actionFlow') ){
                showHidebuttons = false;
                popupModel = false;

                var action = component.get("c.responseActions");
                action.setParams({"actionToken": selectedItem.actionToken});

                action.setCallback(this, function(response) {
                    var state = response.getState();

                    if (state === "SUCCESS") {
                    	var result = response.getReturnValue();

                        //Fire lookup event
						self.fireEvent( component, displayText,showHidebuttons,popupModel,IdOfRecord );
                    }
                    else if ( state === "ERROR" ) {
                        var errors = response.getError();
                        if ( errors ) {
                            if (errors[0] && errors[0].message) {
                                self.showToastHelper( component, errors[0].message, 'error');
                            }
                        } else {
                        	 	self.showToastHelper( component, 'Unknown error', 'error');
                        }
                    }
                  });

              	$A.enqueueAction(action);

                self.fireEvent( component, '',showHidebuttons,popupModel,IdOfRecord );

            }else{
            	showHidebuttons = true;
            	popupModel =true
                var action = component.get("c.responseActions");
                action.setParams({"actionToken": selectedItem.actionToken});

                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                    	var result = response.getReturnValue();

                        displayText =  result.displayText;

                         //Fire lookup event
						self.fireEvent( component, displayText,showHidebuttons,popupModel,IdOfRecord );
                    }
                    else if ( state === "ERROR" ) {
                        var errors = response.getError();
                        if ( errors ) {
                            if (errors[0] && errors[0].message) {
                                self.showToastHelper( component, errors[0].message, 'error');
                            }
                        }else {
                        	 	self.showToastHelper( component, 'Unknown error', 'error');
                        }
                    }
                    });
              	$A.enqueueAction(action);
			}
		}
		component.set( 'v.preventLookupHide', false );
	},
	fireEvent: function( component, displayText, showHidebuttons, popupModel,IdOfRecord ) {
		component.getEvent( 'lookupEvent' ).setParams({
			"displayText" : displayText,
			"showHidebuttons" : showHidebuttons,
			"popupModel" : popupModel,
			"IdOfRecord": IdOfRecord

		}).fire();
	},
	lookupData: function( component, searchValue ) {
		var questionWrapper = component.get("v.questionsWrapper");
  		var searchList = [];
  		var searchListTemp =[];
  		questionWrapper.forEach(function(element, index) {
  			if(!($A.util.isEmpty(element.serializeContent) || $A.util.isEmpty(element.serializeContent.tags))){
				element.serializeContent.tags.forEach(function(val, index1) {
					if(element.displayText.toUpperCase().includes(searchValue.toUpperCase())|| element.label.toUpperCase().includes(searchValue.toUpperCase()) || val.toUpperCase().includes(searchValue.toUpperCase())){
						var obj = {};					
						obj['backgroundClass'] = '';
	           			obj['Label'] =  element.displayText;
	           			obj['IdOfRecord'] =  element.IdOfRecord;
	           			if(!searchListTemp.includes(element.serializeContent.actionTokens[0]) ) {
	           				obj['actionToken'] =  element.serializeContent.actionTokens[0];
	           				searchList.push(obj);
	           			}
	           			searchListTemp.push(element.serializeContent.actionTokens[0]);
					}
				});
			}
    	});
    	searchList[0].backgroundClass = 'liBackGround';
    	return searchList;
    },
	showLookup: function( component ) {

        //Show lookup component
		$A.util.addClass( component.find( 'lookupDiv' ), 'slds-is-open' );
	},
	hideLookup: function( component ) {

        //Hide lookup component
		$A.util.removeClass( component.find( 'lookupDiv' ), 'slds-is-open' );
		component.set( 'v.preventLookupHide', false );
	},
    showToastHelper: function( component, message, messageType ) {
        component.find( 'toastComponent' ).showToastMethod( message, messageType );
    },
})