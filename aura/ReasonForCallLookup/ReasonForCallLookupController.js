({
	doInit : function( component, event, helper ) {
			helper.doInit( component);
	},
	filterList: function( component, event, helper ) {
        
        //Get search text value and set in attribute
		var searchValue = component.get( 'v.searchText')
        
        //Call helper method to fetc h data from server
		if( searchValue.length > 0 ) {
			helper.showLookup( component );
			helper.filterListHelper( component, event, searchValue );
		} 
		else {
			helper.hideLookup( component );
		}		
	},
	onSelect: function( component, event, helper ) {
        
        //Call helper onseclect method to get id of contact when contact is selected 
		if( !$A.util.isEmpty( event.currentTarget.id ) ) {
			console.log(event.currentTarget.id )
			helper.onSelectHelper( component, event, event.currentTarget.id );
		}
	},
	onClear: function( component, event, helper ) {
        
       	//Helper method fireEvent and hideLookup on on clear
		component.set( 'v.selectedItem', null );
		helper.fireEvent( component, '',true,false );
		helper.hideLookup( component );
	},
	hideLookup: function( component, event, helper ) {
		
		if( !component.get( 'v.preventLookupHide' ) ) {
			helper.hideLookup( component );
		}
	},
	mouseDown: function( component, event, helper ) {
        
        //Set true in attribute when mouse is down
		component.set( 'v.preventLookupHide', true );
	},
	mouseUp: function( component ) {
        
        //Set true in attribute when mouse is Up
		component.set( 'v.preventLookupHide', false );
	}
})