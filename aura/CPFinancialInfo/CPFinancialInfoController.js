({
    init : function(component, event, helper)
    {
        component.set("v.expenseCols", 
        [
            { label: 'Date', fieldName: 'EffDate', type: 'date', sortable: true },
            { label: 'Check No.', fieldName: 'CheckNo', type: 'text', sortable: true },
            { label: 'Invoice', fieldName: 'Invoice', type: 'text', sortable: true },
            { label: 'Cost Code', fieldName: 'CostCode', type: 'text', sortable: true },
            { label: 'Payee', fieldName: 'Payee', type: 'text', sortable: true },
            { label: 'Comment', fieldName: 'Comment', type: 'text', sortable: true} ,
            { label: 'Amount', fieldName: 'Amount', type: 'currency', sortable: true }
        ]);

        component.set("v.trustsCols",
        [
            { label: 'Date', fieldName: 'EffDate', type: 'date', sortable: true },
            { label: 'Check No.', fieldName: 'CheckNo', type: 'text', sortable: true },
            { label: 'Bank', fieldName: 'Bank', type: 'text', sortable: true },
            { label: 'Cost Code', fieldName: 'CostCode', type: 'text', sortable: true },
            { label: 'Payee', fieldName: 'Payee', type: 'text', sortable: true },
            { label: 'Comment', fieldName: 'Comment', type: 'text', sortable: true },
            { label: 'Amount', fieldName: 'Amount', type: 'currency', sortable: true }
        ]);
        
        var recId = component.get("v.recordId");
        var action = component.get("c.GetMatterFinancialInfo");

        action.setParams({ "matterId": recId });
        action.setCallback(this, function(a) { component.set("v.caseFinInfo", a.getReturnValue()); });
        $A.enqueueAction(action);
    },

    sortExpenses : function(component, event, helper)
    {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        component.set('v.expenseSortDir', sortDirection);
        component.set('v.expenseSortedBy', sortedBy);

        var dataCopy = component.get('v.caseFinInfo.CaseExpenseData.CaseExpenses');
        dataCopy.sort((helper.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));

        component.set('v.caseFinInfo.CaseExpenseData.CaseExpenses', dataCopy);
    },

    sortTrusts : function(component, event, helper)
    {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        component.set('v.trustsSortDir', sortDirection);
        component.set('v.trustsSortedBy', sortedBy);

        var dataCopy = component.get('v.caseFinInfo.CaseTrustData.CaseTrusts');
        dataCopy.sort((helper.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));

        component.set('v.caseFinInfo.CaseTrustData.CaseTrusts', dataCopy);
    }
})