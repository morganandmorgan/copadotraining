({
    sortBy : function(field, reverse)
    {
        var key = function (x) { return x[field]; };

        return function (a, b) {
            // Handle Date
            if (field === 'EffDate') {
                var dateA = new Date(key(a) ? key(a) : '');
                var dateB = new Date(key(b) ? key(b) : '');

                return reverse * ((dateA > dateB) - (dateB > dateA));
            }
            // Handle Numeric
            else if (field === 'Amount') {
                var a = key(a) ? key(a) : '';
                var b = key(b) ? key(b) : '';

                return reverse * ((a > b) - (b > a));
            }
            // Handle Text
            else {
                var a = key(a) ? key(a).toLowerCase() : '';
                var b = key(b) ? key(b).toLowerCase() : '';

                return reverse * ((a > b) - (b > a));
            }
        }
    },
})