({
	doInit : function( component, event, searchValue ) {
      	try {
      		var self = this;
      		component.set("v.searching",true) 
      		var filterObj = component.get("v.filterObject");
      		console.log(filterObj.dnis);
	        //Get action From server and set its parameters
			var action = component.get( 'c.intakeDetails' );
			action.setParams({"dnis": filterObj.dnis});

	        //Get result which is return by apex class
			action.setCallback( this, function( response ) {
				var state = response.getState() ;
				if( response.getState() === 'SUCCESS' ) {
					var returnVal = response.getReturnValue();
					component.set("v.intakePicklistData",JSON.parse( returnVal ))  
					component.set("v.intakeFormDetails",self.createIntakeFormObject( component ));
					component.set("v.searching",false) 
				}
				else if (state === "ERROR") {
					var errors = [];
	                errors = response.getError();
	                alert(errors)
	                if (errors) {
	                    if (errors[0] && errors[0].message) {
	                      self.showToastHelper( component, errors[0].message, 'error');
	                    }
	                } else {
	                  self.showToastHelper( component, "Unknown error", 'error');
	                }
	                component.set("v.searching",false) 
            	}
			});
			$A.enqueueAction( action );
       	}
      	catch(e){
      		self.showToastHelper( component, e, 'error');
      	}
    },

    saveIntakeForm : function( component, event ){
    	
		try { 
		  		var self = this;
		  		component.set("v.searching",true) 
		  		var intakeFormDetail = component.get("v.intakeFormDetails");

		        //Get action From server and set its parameters
				var action = component.get( 'c.saveIntake' );
				action.setParams({"fieldParameterMap": intakeFormDetail});

		        //Get result which is return by apex class
				action.setCallback( this, function( response ) {
					var state = response.getState() ;
					if( response.getState() === 'SUCCESS' ) {

						component.set("v.searching",false) 

						var responseOfServer = response.getReturnValue().intakeName;
						self.showToastHelper( component, "Intake "+responseOfServer+" was created", 'success');
					}
					else if (state === "ERROR") {
		                var errors = response.getError();
		                if (errors) {
		                    if (errors[0] && errors[0].message) {
		                      self.showToastHelper( component, errors[0].message, 'error');
		                    }
		                } else {
		                  self.showToastHelper( component, "Unknown error", 'error');
		                }
		                component.set("v.searching",false) 
            		}
				});
				$A.enqueueAction( action );
	   	}
	  	catch(e){
	  		component.set("v.searching",false) 
	  		self.showToastHelper( component, e, 'error');
	  	}
    	
    },

    createIntakeFormObject : function( component ){
		var filterObj = component.get("v.filterObject")
    	var intakeFormDetailsObject = {};

    	intakeFormDetailsObject['litigationSelect'] =  '' ;
        intakeFormDetailsObject['handlingFirmSelect'] = '';
        intakeFormDetailsObject['marketingSourceSelect'] = '';
        intakeFormDetailsObject['languageSelect'] = '';
        intakeFormDetailsObject['textarea-case-details'] = '';
        intakeFormDetailsObject['textarea-generating-source-details'] = '';
        intakeFormDetailsObject['text-input-client-email-address'] =  '';
        intakeFormDetailsObject['clientFirstName'] = '' ;
        intakeFormDetailsObject['clientLastName'] =  '';
        intakeFormDetailsObject['clientPhone'] = filterObj.phoneNumber;
        intakeFormDetailsObject['clientMobile'] = '';
        intakeFormDetailsObject['caseDetails'] = '';
        intakeFormDetailsObject['emailAddress'] = '';
        return intakeFormDetailsObject;
        
    },
    showToastHelper: function( component, message, messageType ) {
        component.find( 'toastComponent' ).showToastMethod( message, messageType );
    },
    validateFields: function( component, event ) {
		return component.find( 'validate' ).reduce(function ( validSoFar, inputCmp ) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get( 'v.validity' ).valid;
        }, true );
	},
})