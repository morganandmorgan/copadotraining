({
	doInit : function( component, event, helper ) {
			helper.doInit( component);
	},
	saveIntakeForm: function( component, event, helper ) {
		if(helper.validateFields(component,event)){
			helper.saveIntakeForm( component);
		}
	},
})